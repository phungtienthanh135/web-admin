<?php

use Illuminate\Database\Seeder;

class AttributeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('attribute')->insert([
            [
                'id' => 0,
                'attribute' => 'index',
                'name' => 'Số thứ tự',],
            [
                'id' => 1,
                'attribute' => 'ticketCode',
                'name' => 'Mã vé'],
            [
                'id' => 2,
                'attribute' => 'numberSeat',
                'name' => 'Số lượng ghế'],
            [
                'id' => 3,
                'attribute' => 'listSeat',
                'name' => 'Danh sách ghế'],
            [
                'id' => 4,
                'attribute' => 'customer',
                'name' => 'Tên hành khách'],
            [
                'id' => 5,
                'attribute' => 'phone',
                'name' => 'Số điện thoại'],
            [
                'id' => 6,
                'attribute' => 'pickUp',
                'name' => 'Điểm lên'],
            [
                'id' => 7,
                'attribute' => 'dropOff',
                'name' => 'Điểm xuống'],
            [
                'id' => 8,
                'attribute' => 'agency',
                'name' => 'Đại lý'],
            [
                'id' => 9,
                'attribute' => 'ticketStatus',
                'name' => 'Trạng thái vé'],
            [
                'id' => 10,
                'attribute' => 'agencyPrice',
                'name' => 'Giá thực thu'],
            [
                'id' => 11,
                'attribute' => 'paidMoney',
                'name' => 'Đã thu'],
            [
                'id' => 12,
                'attribute' => 'unPaidMoney',
                'name' => 'Chưa thu'],
            [
                'id' => 13,
                'attribute' => 'note',
                'name' => 'Ghi chú']
        ]);
    }
}
