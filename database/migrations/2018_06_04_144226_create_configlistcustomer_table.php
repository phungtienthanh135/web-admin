<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfiglistcustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configlistcustomer', function (Blueprint $table) {
            $table->increments('index');
            $table->string('companyId');
            $table->integer('idAttribute');
            $table->integer('status');
            $table->integer('size');
            $table->integer('bold');
            $table->integer('width');
            $table->integer('height');
            $table->string('alias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('configlistcustomer');
    }
}
