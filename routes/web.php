<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*Phần đăng nhập*/

Route::group(['middleware' => ['guest'], 'prefix' => '/'], function () {
    Route::get('/', 'LoginController@index');

    /*Route::get('login', ['as' => 'login', 'uses' => 'LoginController@index']);*/
    Route::get('dang-nhap', ['as' => 'cpanel.login.index', 'uses' => 'LoginController@index']);

    Route::get('dang-nhap-dai-ly', ['as' => 'cpanel.vue.LoginAgency', 'uses' => 'LoginController@agencyLogin']);

    Route::post('dang-nhap', ['as' => 'cpanel.login.checklogin', 'uses' => 'LoginController@CheckLogin']);

    Route::get('quen-mat-khau', 'LoginController@ForgotPasswordDefaul');

    Route::match(['get', 'post'],'quen-mat-khau/{buoc}', 'LoginController@ForgotPassword')->where(['buoc' => '[0-3]']);
});

/*Phần đăng xuất*/
Route::get('dang-xuat', ['as' => 'cpanel.login.logout', 'uses' => 'LoginController@logout']);

Route::get('getViewListPackage',['as'=>'getViewListPackage','uses'=>'PackageServiceController@getViewListPackage']);
Route::post('buyPackage','PackageServiceController@buyPackage');
Route::get('/electronic-contract', function (Request $request) {
    if (!$request->token) {
        return 'token null';
    }
    if (!$request->tripId) {
        return 'trip null';
    }
    return view('cpanel.Vue.ElectronicContract')->with(['tripId' => $request->tripId, 'token' => $request->token]);
});

/*Phần quản lý*/
Route::group(['middleware' => ['checklogin'], 'prefix' => 'cpanel'], function () {
    Route::group(['prefix' => 'ship-new'], function (){
        Route::get('show', 'ShipNewController@show');
        Route::get('getNameFromPhone', 'ShipNewController@getNameFromPhone');
        Route::get('create-goods', 'ShipNewController@createGoods');
        Route::get('update-goods', 'ShipNewController@updateGoods');
        Route::get('create-package', 'ShipNewController@createPackage');
        Route::post('ship-package', 'ShipNewController@shipPackage');
        Route::post('arrive-package', 'ShipNewController@arrivePackage');
        Route::post('deliver-package', 'ShipNewController@deliverPackage');
        Route::get('deleteGoods', 'ShipNewController@deleteGoods');
        Route::get('searchPoint', 'ShipNewController@searchPoint');
        Route::get('updatePackage', 'ShipNewController@updatePackage');
        Route::get('getRoute', 'ShipNewController@getRoute');
        Route::get('returnPackage', 'ShipNewController@returnPackage');

    });
    Route::get('', function(){
        return redirect('cpanel/vue/home');
    });
    /*Trang chủ*/
    Route::group(['prefix' => 'home'], function () {
        Route::get('', ['as' => 'cpanel.home', 'uses' => 'HomeController@index']);
        Route::get('account', ['as' => 'cpanel.account', 'uses' => 'HomeController@account']);
        Route::get('userLog', ['as' => 'cpanel.userLog', 'uses' => 'HomeController@userLog']);
        Route::get('quick-search', 'HomeController@quickSearch');
        Route::get('search-phone', 'HomeController@searchPhone');
    });

    Route::group(['prefix' => 'policy'], function () {
        Route::get('indexPolicy', 'PolicyController@getView');
    });
    /*Vé*/
    Route::group(['prefix' => 'ticket'], function () {
        Route::get('show', 'TicketController@show')->middleware(roleToString([GET_LIST_TICKET]));
        Route::get('showTicketCancel', 'TicketController@showTicketCancel')->middleware(roleToString([GET_LIST_TICKET]));
        Route::get('getListTicketCancelWithTripId', 'TicketController@getListTicketCancelWithTripId')->middleware(roleToString([GET_LIST_TICKET]));

        Route::get('get-list-ticket', 'TicketController@getListTicket')->middleware(roleToString([GET_LIST_TICKET]));
        Route::get('sell', 'TicketController@sell')->middleware(roleToString([SELL_TICKET]));
        Route::get('sell-ver1', 'TicketController@sellVer1')->middleware(roleToString([SELL_TICKET]));
        Route::post('sell', 'TicketController@postSell')->middleware(roleToString([SELL_TICKET]));
        Route::post('flashSell', 'TicketController@flashSell')->middleware(roleToString([SELL_TICKET]));
        Route::post('sell-ticket-goods', 'TicketController@postSellTicketGood')->middleware(roleToString([SELL_TICKET]));
        Route::get('get-price-ticket-goods', 'TicketController@getPriceTicketGoods')->middleware(roleToString([SELL_TICKET]));
        Route::get('printTicket', 'TicketController@printTicket')->middleware(roleToString([GET_LIST_TICKET,SELL_TICKET]));



        Route::post('update', 'TicketController@updateStatusTicket')->middleware(roleToString([UPDATE_TICKET,CANCEL_TICKET,PAYMENT_TICKET]));
        Route::post('updateChecked', 'TicketController@updateCheckTicket')->middleware(roleToString([UPDATE_TICKET,CANCEL_TICKET,PAYMENT_TICKET]));
        Route::post('make-call', 'TicketController@makeCall');
        Route::post('ticket-info', 'TicketController@getTicketInfo');
        Route::get('ticket-history', 'TicketController@getTicketHistory');
        Route::post('transshipment', 'TicketController@addtransshipment');

        Route::get('historyCalling','TicketController@getHistoryCall');
        Route::get('payVnp/{ticketId}','TicketController@payVnp');
    });
    /*Mã khuyến mãi*/
    Route::group(['prefix' => 'coupon'], function () {
        Route::get('show', 'CouponController@show')->middleware(roleToString([GETLIST_PROMOTION]));
        Route::post('add', 'CouponController@postAdd')->middleware(roleToString([CREATE_PROMOTION]));
        Route::post('edit', 'CouponController@postEdit')->middleware(roleToString([UPDATE_PROMOTION]));
        Route::post('delete', 'CouponController@delete')->middleware(roleToString([DELETE_PROMOTION]));
        Route::get('check', 'CouponController@check')->middleware(roleToString([SELL_TICKET]));

    });
    /*Lịch chạy*/
    Route::group(['prefix' => 'schedule'], function () {
        Route::get('show', 'ScheduleController@show')->middleware(roleToString([GET_LIST_SCHEDULE]));

        Route::get('add', 'ScheduleController@getAdd')->middleware(roleToString([CREATE_SCHEDULE]));
        Route::post('add', 'ScheduleController@postAdd')->middleware(roleToString([CREATE_SCHEDULE]));

        Route::get('edit/{id}', 'ScheduleController@getEdit')->middleware(roleToString([UPDATE_SCHEDULE]));
        Route::post('edit/{id}', 'ScheduleController@postEdit')->middleware(roleToString([UPDATE_SCHEDULE]));

        Route::post('delete', 'ScheduleController@delete')->middleware(roleToString([DELETE_SCHEDULE]));
        Route::post('list-date', 'ScheduleController@listDateThisWeek');


    });
    /*Chuyến xe*/
    Route::group(['prefix' => 'trip'], function () {

        Route::get('show', 'TripController@show')->middleware(roleToString([GET_LIST_TRIP_FOR_USER_TRIP]));
        Route::get('getListTrip', 'TripController@getListTrip')->middleware(roleToString([GET_LIST_TRIP_FOR_USER_TRIP]));
        Route::get('lockTrip', 'TripController@updateLockTrip');

        Route::get('search', 'TripController@search');
        Route::get('search-by-tripid', 'TripController@searchByTripId');

        Route::get('control', 'TripController@control')->middleware(roleToString([GET_LIST_OUT_OF_STATION_COMMAND]));
        Route::get('cancel', 'TripController@cancelTrip')->middleware(roleToString([CANCEL_OUT_OF_STATION_COMMAND]));
        Route::post('edit', 'TripController@postEdit')->middleware(roleToString([GET_LIST_TRIP_FOR_USER_TRIP]));
        Route::post('edit-trip', 'TripController@postEditTrip');
        Route::get('start', 'TripController@startTrip')->middleware(roleToString([OUT_OF_STATION_COMMAND]));
        Route::get('open', 'TripController@openStartTrip')->middleware(roleToString([OUT_OF_STATION_COMMAND]));
        Route::get('end-trip/{id}', 'TripController@endTrip');
        Route::get('transfer', 'TripController@transfer')->middleware(roleToString([CREATE_CHANGE_VEHICLE]));
        Route::post('transfer', 'TripController@postTransfer')->middleware(roleToString([CREATE_CHANGE_VEHICLE]));
//        Route::get('transfer', 'TripController@transfer')->middleware(roleToString([CREATE_CHANGE_VEHICLE]));
//        Route::post('transfer', 'TripController@postTransfer')->middleware(roleToString([CREATE_CHANGE_VEHICLE]));
        Route::get('transfer', 'TripController@transfer');
        Route::post('transfer', 'TripController@postTransfer');
        Route::get('log', 'TripController@log')->middleware(roleToString([GET_LIST_TRIP_ACTIVITY]));
        Route::get('log/{TripId}', 'TripController@logdetail')->middleware(roleToString([VIEW_TRIP_ACTIVITY]));

        Route::get('HolidayPrice','TripController@getAddPriceHoliday');
        Route::post('HolidayPrice','TripController@postAddPriceHoliday');

        Route::get('print', 'TripController@printVoidTicket');

        Route::get('view-seat', 'TripController@getSeatMapByTripId');

    });
    /*Tuyến đường*/
    Route::group(['prefix' => 'route'], function () {
        Route::get('show', 'RouteController@show')->middleware(roleToString([GET_LIST_ROUTE]));
        Route::get('search', 'RouteController@search');
        //Route::get('add/{step}', 'RouteController@getadd');
        Route::get('add', 'RouteController@getadd')->middleware(roleToString([CREATE_ROUTE]));
        Route::post('add', 'RouteController@postadd')->middleware(roleToString([CREATE_ROUTE]));

        Route::get('edit/{id}', 'RouteController@getEdit')->middleware(roleToString([UPDATE_ROUTE]));
        Route::post('edit', 'RouteController@postEdit')->middleware(roleToString([UPDATE_ROUTE]));

        Route::post('delete', 'RouteController@delete')->middleware(roleToString([DELETE_ROUTE]));
        Route::get('sort', 'RouteController@sort')->middleware(roleToString([GET_LIST_ROUTE]));
    });
    /*Điểm dừng*/
    Route::group(['prefix' => 'point'], function () {
        Route::get('show', 'PointController@show')->middleware(roleToString([GET_LIST_POINT]));
        Route::get('search', 'PointController@search');


        Route::get('add', 'PointController@getadd')->middleware(roleToString([CREATE_POINT]));
        Route::post('add', 'PointController@postadd')->middleware(roleToString([CREATE_POINT]));

        Route::post('edit', 'PointController@postEdit')->middleware(roleToString([UPDATE_POINT]));
        Route::get('edit', 'PointController@getEdit')->middleware(roleToString([UPDATE_POINT]));

        Route::post('delete', 'PointController@delete')->middleware(roleToString([DELETE_POINT]));
    });
    /*Phương tiện*/
    Route::group(['prefix' => 'vehicle'], function () {
        Route::get('show', 'VehicleController@show')->middleware(roleToString([GET_LIST_VEHICLE]));
        Route::get('add', 'VehicleController@getadd')->middleware(roleToString([CREATE_VEHICLE]));
        Route::post('add', 'VehicleController@postadd')->middleware(roleToString([CREATE_VEHICLE]));
        Route::get('edit/{id}', 'VehicleController@getEdit')->middleware(roleToString([UPDATE_VEHICLE]));
        Route::post('edit', 'VehicleController@postEdit')->middleware(roleToString([UPDATE_VEHICLE]));
        Route::post('delete', 'VehicleController@delete')->middleware(roleToString([DELETE_VEHICLE]));
        Route::get('get-seatmap-by-vehicleid', 'VehicleController@getSeatMapByVehicleId');
        Route::get('locationOnline', 'VehicleController@locationOnline');
        Route::get('getLocationOnlineData', 'VehicleController@getLocationOnlineData');
    });
    /*Loại phương tiện*/
    Route::group(['prefix' => 'vehicle-type'], function () {
        Route::get('show', 'VehicleTypeController@show')->middleware(roleToString([GET_LIST_VEHICLE_TYPE]));
        Route::post('edit', 'VehicleTypeController@edit')->middleware(roleToString([UPDATE_VEHICLE_TYPE]));
        Route::post('add', 'VehicleTypeController@add')->middleware(roleToString([CREATE_VEHICLE_TYPE]));
        Route::post('delete', 'VehicleTypeController@delete')->middleware(roleToString([DELETE_VEHICLE_TYPE]));
    });
    /*Sơ đồ ghế*/
    Route::group(['prefix' => 'seat-map'], function () {

        Route::get('show/{id}', 'SeatMapController@detail');
        Route::get('patternSeatMap', 'SeatMapController@getPaternSeatMap');
        Route::get('show', 'SeatMapController@show')->middleware(roleToString([GET_LIST_SEATMAP]));
        Route::post('add', 'SeatMapController@postAdd')->middleware(roleToString([CREATE_SEATMAP]));
        Route::post('detele', 'SeatMapController@delete')->middleware(roleToString([DELETE_SEATMAP]));
        Route::post('edit', 'SeatMapController@edit')->middleware(roleToString([UPDATE_SEATMAP]));

    });
    /*Khoản mục*/
    Route::group(['prefix' => 'item'], function () {
        Route::get('show', 'ItemController@show')->middleware(roleToString([GET_LIST_ITEMS]));
        Route::post('add', 'ItemController@postAdd')->middleware(roleToString([CREATE_ITEMS]));
        Route::post('edit', 'ItemController@postEdit')->middleware(roleToString([UPDATE_ITEMS]));
        Route::post('delete', 'ItemController@delete')->middleware(roleToString([DELETE_ITEMS]));

    });
    /*Tiền tệ*/
    Route::group(['prefix' => 'Currency'], function () {
        Route::get('show', 'CurrencyController@show');
        Route::post('add', 'CurrencyController@postAdd');
        Route::post('edit', 'CurrencyController@postEdit');
        Route::post('delete', 'CurrencyController@delete');

    });
    /*Khách hàng*/
    Route::group(['prefix' => 'customer'], function () {
        Route::get('show', 'CustomerController@show')->middleware(roleToString([GET_LIST_CUSTOMER]));
        Route::get('policyCustomer', 'CustomerController@getViewPolicy')->middleware(roleToString([GET_LIST_CUSTOMER]));
        Route::get('ajax-get-info-by-phonenumber', 'CustomerController@getInfoByPhoneNumber');
        Route::get('feedback', 'FeedbackController@show')->middleware(roleToString([GET_LIST_FEEDBACK]));
        Route::post('addFeedback', 'FeedbackController@postAdd')->middleware(roleToString([CREATE_FEEDBACK]));
        Route::get('notification', 'NotificationController@customer');

    });
    /*Người dùng - nhân viên*/
    Route::group(['prefix' => 'user'], function () {
        Route::get('show', 'UserController@show')->middleware(roleToString([GET_LIST_EMPLOYEE]));
        Route::get('add', 'UserController@add')->middleware(roleToString([CREATE_NEW_EMPLOYEE]));
        Route::post('add', 'UserController@postadd')->middleware(roleToString([CREATE_NEW_EMPLOYEE]));
        Route::get('edit/{id}', 'UserController@getEdit')->middleware(roleToString([UPDATE_EMPLOYEE]));
        Route::post('edit', 'UserController@postEdit')->middleware(roleToString([UPDATE_EMPLOYEE]));
        Route::post('changePassword', 'UserController@changePassword');

        Route::post('delete', 'UserController@delete')->middleware(roleToString([DELETE_EMPLOYEE]));
        Route::post('lock', 'UserController@lock')->middleware(roleToString([DELETE_EMPLOYEE]));
        Route::get('notification', 'NotificationController@user');


    });

    Route::group(['prefix'=>'notification'],function (){
        Route::get('notify','NotificationController@notify');
    });

    /*Phân quyền*/
    Route::group(['prefix' => 'group-permission'], function () {
        Route::get('show', 'GroupPermissionController@show')->middleware(roleToString([GET_LIST_GROUP_PERMISSION]));
        Route::get('add', 'GroupPermissionController@getadd')->middleware(roleToString([CREATE_GROUP_PERMISSION]));
        Route::post('add', 'GroupPermissionController@postadd')->middleware(roleToString([CREATE_GROUP_PERMISSION]));
        Route::post('update', 'GroupPermissionController@updatePermission')->middleware(roleToString([UPDATE_GROUP_PERMISSION]));
        Route::post('delete', 'GroupPermissionController@delete')->middleware(roleToString([DELETE_GROUP_PERMISSION]));
        Route::post('get-permission-by-id', 'GroupPermissionController@getPermissionByGroupId');
    });

    /*Thu chi*/
    Route::group(['prefix' => 'balance-sheet'], function () {
        Route::get('income', 'BalanceSheetController@income')->middleware(roleToString([GET_LIST_RECEIPT_AND_PAYMENT]));
        Route::get('expenses', 'BalanceSheetController@expenses')->middleware(roleToString([GET_LIST_RECEIPT_AND_PAYMENT]));
//        Route::get('report', 'BalanceSheetController@report')->middleware(roleToString([REPORT_RECEIPT_AND_PAYMENT]));
        Route::post('add', 'BalanceSheetController@add')->middleware(roleToString([CREATE_RECEIPT_AND_PAYMENT]));
        Route::post('add-multil', 'BalanceSheetController@addMultil')->middleware(roleToString([CREATE_RECEIPT_AND_PAYMENT]));
        Route::post('delete', 'BalanceSheetController@delete')->middleware(roleToString([DELETE_RECEIPT_AND_PAYMENT]));
    });
    /*Báo cáo*/
    Route::group(['prefix' => 'report'], function () {
//        Route::get('reconciliation', 'ReportController@reconciliation');
//        Route::get('reconciliation-for-user', 'ReportController@reconciliationForUser');
        Route::get('point', 'ReportController@getPoint');
        Route::get('customers', 'ReportController@getCustomer');
        Route::get('profit', 'ReportController@profit');
        Route::get('balance-report', 'ReportController@balanceReport');
        Route::get('detail', 'ReportController@detail');
        Route::get('summary-date', 'ReportController@summaryByDate');
        Route::get('summary-month', 'ReportController@summaryByMonth');
//        Route::get('listAgency','ReportController@listAgency');
        Route::get('accountantReport','ReportController@accountantReport');
        Route::get('messageReport','ReportController@messageReport');
        Route::post('topUp','ReportController@topUp');
        Route::get('debtorsReport','ReportController@debtorsReport');
        Route::get('depreciationReport','ReportController@depreciationReport');
        Route::get('online-report','ReportController@onlineReport');
        Route::get('chartByMonth','ReportController@chartByMonth');
        Route::get('chartByMonthAll','ReportController@chartByMonthAll');
        Route::get('order-online', 'TicketController@orderOnline' )->middleware(roleToString([GET_LIST_TICKET]));
    });
    /*Hệ thống*/
    Route::group(['prefix' => 'system'], function () {
        Route::get('config', 'SystemController@config')->middleware(roleToString([CONFIG_COMPANY_INFO]));
        Route::post('config', 'SystemController@postConfig')->middleware(roleToString([CONFIG_COMPANY_INFO]));
        Route::get('uploadImg', 'SystemController@postImg')->middleware(roleToString([CONFIG_COMPANY_INFO]));
        Route::get('info', 'SystemController@info')->middleware(roleToString([CONFIG_COMPANY_INFO]));
        Route::get('user-activity', 'SystemController@UserActivity')->middleware(roleToString([CONFIG_COMPANY_INFO]));
        Route::post('upload-image', 'SystemController@postUpload');
        Route::get('list-phone', 'SystemController@listPhoneNumber')->middleware(roleToString([CONFIG_COMPANY_INFO]));
        Route::post('add-or-update-phone', 'SystemController@addOrUpdatePhoneNumber')->middleware(roleToString([CONFIG_COMPANY_INFO]));
        Route::post('delete-phone', 'SystemController@deletePhone')->middleware(roleToString([CONFIG_COMPANY_INFO]));
        Route::get('settingListCustomer', 'settingListCustomerController@setup');
        Route::get('updateSLC', 'settingListCustomerController@updateSetting');
        Route::get('settingPrintTicket', 'settingPrintTicketController@setup');
        Route::get('updatePT', 'settingPrintTicketController@updateSetting');
        Route::get('settingShip', 'settingShipController@setup');
        Route::get('updateS', 'settingShipController@updateSetting');
        Route::post('setContentMessage', 'SystemController@setContentMessage');
        Route::get('getContentsMessage', 'SystemController@getContentsMessage');
    });
    /*Đại lý*/
    Route::group(['middleware' => [roleToString([GET_LIST_AGENCY])], 'prefix' => 'agency'], function () {
        Route::get('show', 'AgencyController@show');
        Route::post('add', 'AgencyController@postAdd');
        Route::post('updateOrDelete', 'AgencyController@updateOrDelete');
        Route::get('list-agency', 'AgencyController@getListAgency');
        Route::get('levelAgency','AgencyController@getViewLevelAgency');
        Route::get('listLevelAgency','AgencyController@getListLevelAgency');
        Route::post('addLevelAgency','AgencyController@postAddLevelAgency');
        Route::post('editLevelAgency','AgencyController@postEditLevelAgency');
        Route::get('policyAgency','AgencyController@getViewPolicyAgency');

        Route::get('listPolicyAgency','AgencyController@getListPolicyAgency');
        Route::post('addPolicyAgency','AgencyController@postAddPolicyAgency');
        Route::post('editPolicyAgency','AgencyController@postUpdatePolicyAgency');
    });
    /*Gửi đồ*/
    Route::group(['prefix' => 'ship'], function (){
        Route::post('sell-ticket-goods','ShipController@postSellTicketGood')->middleware(roleToString([SELL_TICKET]));
        Route::post('update', 'ShipController@updateStatusTicket')->middleware(roleToString([UPDATE_TICKET,CANCEL_TICKET,PAYMENT_TICKET]));
        Route::get('get-price-ticket-goods', 'ShipController@getPriceTicketGoods')->middleware(roleToString([SELL_TICKET]));
        Route::get('sell', 'ShipController@sell')->middleware(roleToString([SELL_TICKET]));
        Route::get('print/{ticketId}', 'ShipController@printTicket')->middleware(roleToString([GET_LIST_TICKET,SELL_TICKET]));
        Route::get('show', ['as' => 'cpanel.ship.show', 'uses' => 'ShipController@show']);
        Route::get('postGoodInTrip', 'ShipController@postGoodInTrip');
        Route::get('listTypeShip', 'ShipController@typeShip');
        Route::get('listType', 'ShipController@typeShipNew');
        Route::post('add', 'ShipController@addTypeShip');
        Route::post('repair', 'ShipController@repairTypeShip');
        Route::post('delete', 'ShipController@deleteTypeShip');
        Route::post('price-ship', 'ShipController@getPriceShip');

    });
    Route::group(['prefix'=>'department'],function(){
        Route::get('show',['as'=>'department.show','uses'=>'DepartmentController@show']);
        Route::post('postAdd','DepartmentController@postAdd');
        Route::post('postEdit','DepartmentController@postEdit');
        Route::get('delete/{departmentID}','DepartmentController@delete');
    });
    Route::group(['prefix' => 'help'], function () {
        Route::get('{helpname}', 'HelpController@show');
    });
    Route::group(['prefix' => 'php'], function () {
        Route::get('call', 'Controller@callPhpServer');
    });
//    Route::get('vue',function (){
//        return view('cpanel.Vue.index');
//    });
    Route::group(['prefix' => 'vue'], function () {
//        Route::get('', function () {
//            return view('cpanel.Vue.index');
//        });
        Route::get('/{any}', function () {
            return view('cpanel.Vue.index');
        })->where(['any'=>'.*']);
    });
});