/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import HighchartsVue from 'highcharts-vue';
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import VueClockPicker from '@pencilpix/vue2-clock-picker';
import {VueEditor} from 'vue2-editor';
import supperLoading from './components/Template/SupperLoading.vue';
import loading from './components/Template/Loading.vue';
import DatePicker from './components/Template/DatePicker.vue';
import DatePicker2 from './components/Template/DatePicker2.vue';
import DatePickerRanger from './components/Template/DatePickerRanger.vue';
import UploadImage from './components/Template/UploadImage.vue';
// import UploadImageEl from './components/Template/UploadImageEl.vue';
import Multiselect from 'vue-multiselect';
import barcode from 'vue-barcode';
import loadmore from './components/Template/LoadMore.vue';
import count from './components/Template/Count.vue';
import selectUser from './components/Template/SelectUser.vue';
import selectPoint from './components/Template/SelectPoint.vue';
import selectItem from './components/Template/SelectItem.vue';
import select2User from './components/Template/Select2User.vue';
import Promotion from './components/Template/Promotion.vue';
import SelectAgency from './components/Template/SelectAgency.vue';
import CountDown from './components/Helper/CountDown.vue';
import InputNumber from './components/Helper/InputNumber.vue';
import ElInputNumber from './components/Helper/ElInputNumber.vue';
import draggable from 'vuedraggable';
import VueQRCodeComponent from 'vue-qrcode-component';
import Helper from './Helper';
import ElementUI from 'element-ui';
import store from './store';
import router from './router.js';
import App from './components/App.vue';
import lang from './Lang/index.js';
import ESelectUser from './components/User/ESelectUser.vue';
// import Vue from 'vue';
// import Vuelidate from 'vuelidate';
// Vue.use(Vuelidate);

require('./bootstrap');
require('./func');
require('./data');
require('./API');

window.Vue = require('vue');
Vue.config.productionTip = false;

Vue.use(HighchartsVue);

import VueQrcodeReader from 'vue-qrcode-reader';
Vue.use(VueQrcodeReader);

import VueSignaturePad from 'vue-signature-pad';
Vue.use(VueSignaturePad);

Vue.component('ClockPicker',VueClockPicker);
Vue.component('VueEditor',VueEditor);
Vue.component('supperloading',supperLoading);
Vue.component('loading',loading);
Vue.component('datepicker',DatePicker);
Vue.component('datepicker2',DatePicker2);
Vue.component('datepicker-ranger',DatePickerRanger);
Vue.component('uploadimage',UploadImage);
// Vue.component('uploadimage2',UploadImageEl);
Vue.component('multiselect', Multiselect);
Vue.component('barcode', barcode);
Vue.component('loadmore', loadmore);
Vue.component('count', count);
Vue.component('selectuser', selectUser);
Vue.component('selectpoint', selectPoint);
Vue.component('selectitem', selectItem);
Vue.component('select2user', select2User);
Vue.component('Promotion', Promotion);
Vue.component('SelectAgency', SelectAgency);
Vue.component('CountDown', CountDown);
Vue.component('InputNumber', InputNumber);
Vue.component('InputNumber2', ElInputNumber);
Vue.component('draggable', draggable);
Vue.component('qr-code', VueQRCodeComponent);
Vue.component('e-select-user', ESelectUser);

Helper.create(Vue);

Vue.use(require('vue-shortkey'));

Vue.use(ElementUI);


import {firebaseInit} from "./firebaseDatabase.js"
import {fireStoreInit} from "./firestoreDatabase.js"

var vm = (async function () {
    await Promise.all([firebaseInit(),fireStoreInit()]).then(value=>{

    });
    return new Vue({
        el: '#app',
        router : router,
        store : store,
        i18n : lang,
        render: renderComponent => renderComponent(App)
    })
})();
export default vm;
