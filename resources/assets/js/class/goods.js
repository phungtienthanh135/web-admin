import store from '../store';
import lang from '../Lang/index';

export class GoodsStatus {
    constructor (goodsStatus){
        this.status = goodsStatus!==undefined&&goodsStatus!==null?goodsStatus:null;
    }
    label (){
        return GoodsStatus.properties()[this.status]?GoodsStatus.properties()[this.status] : '';
    }
    static properties () {
        return {
            0 : "Đã hủy",
            1 : "Đã nhận hàng",
            2 : "Đã Gom",
            3 : "Đã lên xe",
            4 : "Đã xuống xe",
            5 : "Đã trả",
        };
    }
}

export class GoodsType {
    constructor (goodsType){
        this.id = goodsType&&goodsType.goodsTypeId ? goodsType.goodsTypeId : null;
        this.name = goodsType&&goodsType.goodsTypeName ? goodsType.goodsTypeName : '';
        this.price = goodsType&&goodsType.goodsTypePrice ? goodsType.goodsTypePrice : 0;
        this.usePriceTable = goodsType&&goodsType.usePriceTable ? goodsType.usePriceTable : false;
        this.priceTable = goodsType&&goodsType.priceTable ? goodsType.priceTable : [];
    }

    save (success,error){
        store.commit('LOAD','SAVE_GOODS_TYPE');
        let params = {
            goodsTypeName : this.name,
            goodsTypePrice : this.price,
            usePriceTable : this.usePriceTable,
        };
        if(this.id!==null){
            params.goodsTypeId = this.id;
        }

        window.sendJson({
            url : window.urlDBD(this.id?window.API.goodsTypeUpdate:window.API.goodsTypeCreate),
            data : params,
            success :function (result) {
                store.commit('UNLOAD','SAVE_GOODS_TYPE');
                if(typeof success == "function"){
                    success(result);
                }
            },
            functionIfError : function (err) {
                store.commit('UNLOAD','SAVE_GOODS_TYPE');
                if(typeof error == "function"){
                    error(err);
                }
            }
        });
    }
    remove(success,error){
        if(!this.id){
            return false;
        }
        let cancelReason = window.prompt('Tại sao bạn muốn xóa?');
        if(cancelReason!==null){
            store.commit('LOAD','DELETE_GOODS_TYPE');
            window.sendJson({
                url : window.urlDBD(window.API.goodsTypeDelete),
                data : {
                    goodsTypeId : this.id,
                    cancelReason : cancelReason,
                },
                success :function (result) {
                    store.commit('UNLOAD','DELETE_GOODS_TYPE');
                    if(typeof success == "function"){
                        success(result);
                    }
                },
                functionIfError : function (err) {
                    store.commit('UNLOAD','DELETE_GOODS_TYPE');
                    if(typeof error == "function"){
                        error(err);
                    }
                }
            });
        }
    }
}

export class ListGoodsType {
    constructor (){
        this.name = '';
        this.page = 0;
        this.count = 50;
        this.list = [];
    }

    getList(beforeLoad,success,error){
        let self = this;
        store.commit('LOAD','GET_LIST_GOODS_TYPE');
        let params = {
            page : this.page,
            count : this.count
        };
        if(this.name){
            params.name = this.name;
        }
        if(typeof beforeLoad == "function"){
            beforeLoad();
        }
        sendJson({
            url : urlDBD(API.goodsTypeList),
            data : params,
            success : function(rs){
                store.commit('UNLOAD','GET_LIST_GOODS_TYPE');
                self.setList(rs.results.list);
                if(typeof success == "function"){
                    success(rs);
                }
            },
            functionIfError : function(rs){
                store.commit('UNLOAD','GET_LIST_GOODS_TYPE');
                if(typeof error == "function"){
                    error(rs);
                }
            }
        });
    }

    setList(_list){
        let list = [];
        $.each(_list,function (k,value) {
            list.push(new GoodsType(value));
        });
        this.list = list;
    }
}

export class Goods {
    constructor(goods) {
        this.id = goods && goods.goodsId ? goods.goodsId : null;
        this.no = goods ? goods.goodsNo : '';
        this.code = goods ? goods.goodsCode : '';
        this.name = goods ? goods.goodsName : '';
        this.quantity = goods ? goods.quantity : 1;
        this.value = goods ? goods.value : 0;
        this.notice = goods ? goods.notice : '';

        this.price = goods ? goods.price : 0;//cuoc phi
        this.extraPrice = goods ? goods.extraPrice : 0;//cuoc phi thu them
        this.extraPriceNote = goods ? goods.extraPriceNote : '';//ly do thu them
        this.paid = goods ? goods.paid : 0;//tien da tra

        this.updateUserInfoTimeline = goods ? goods.updateUserInfoTimeline : [];

        this.sendSMS = goods ? goods.sendSMS : false;
        this.sendSMSSender = goods ? goods.sendSMSSender : false;
        this.sendDate = goods ? new Date(goods.sendDate).customFormat('#DD#-#MM#-#YYYY#') : new Date().customFormat('#DD#-#MM#-#YYYY#');
        this.status = new GoodsStatus(goods ? goods.goodsStatus : null);
        this.goodsType = goods && goods.goodsTypeId ? new GoodsType({
            goodsTypeId: goods.goodsTypeId,
            goodsTypeName: goods.goodsTypeName
        }) : null;
        this.packageInfo = {
            id: goods && goods.packageId ? goods.packageId : null,
            name: goods && goods.packageName ? goods.packageName : null,
            no: goods && goods.packageNo ? goods.packageNo : null,
        };
        this.from = {
            fullName: goods ? goods.sender : '',
            phoneNumber: goods ? goods.senderPhone : '',
            image: goods ? goods.imageURL : '',
            address: goods ? goods.pickUpPoint : '',
            point: null
        };
        if (goods && goods.pickPointId) {
            this.from.point = {
                id: goods ? goods.pickPointId : '',
                name: goods ? goods.pickPointName : '',
                address: goods ? goods.pickUpPoint : ''
            };
        }
        this.to = {
            fullName: goods ? goods.receiver : '',
            phoneNumber: goods ? goods.receiverPhone : '',
            image: goods ? goods.receiverImageURL : '',
            address: goods ? goods.dropOffPoint : '',
            point: null
        };
        if (goods && goods.dropPointId) {
            this.to.point = {
                id: goods ? goods.dropPointId : null,
                name: goods ? goods.dropPointName : '',
                address: goods ? goods.dropOffPoint : '',
            };
        }
        this.route = {
            id: goods && goods.routeId ? goods.routeId : null,
            name: goods && goods.routeName ? goods.routeName : null
        };
        this.trip = {
            id: goods && goods.tripId ? goods.tripId : null,
            numberPlate: goods && goods.tripNumberPlate ? goods.tripNumberPlate : null,
            startDate: goods && goods.tripStartDate ? goods.tripStartDate : null,
            startTime: goods && goods.tripStartTime ? goods.tripStartTime : null
        };
        this.senderPayment = goods&&goods.paid>=this.totalPrice()?true:false;
        this.printAfterSave = false;
        this.printTemAfterSave = false;
    }

    setPrice (value){
        this.price = parseInt(value)||0;
        this.setPaid();
    }
    setExtraPrice (value){
        this.extraPrice = parseInt(value)||0;
        this.setPaid();
    }
    setQuantity (value){
        this.quantity = parseInt(value)||0;
        this.setPaid();
    }
    setPaid (value){
        this.paid = value?parseInt(value):(this.senderPayment ? this.totalPrice() : 0);
    }

    setSenderPayment(value){
        this.senderPayment = value?true:false;
        this.setPaid();
    }
    totalPrice (){
        try{
            let price = (parseInt(this.price)+parseInt(this.extraPrice))*parseInt(this.quantity);
            return price;
        }catch (e) {
            return 0;
            console.warn(e);
        }
    }

    setGoodsType(goodsType) {
        if (goodsType) {
            this.goodsType = goodsType;
            this.name = this.name ||this.goodsType.name;
            this.setPrice(this.goodsType.price);
        }else{
            this.goodsType = null;
        }
    }

    setFromPoint (point){
        if(point){
            this.from.point = {
                id: point.id,
                name: point.name,
                address: point.address
            };
            this.from.address = point.address;
        }else {
            this.from.point = null;
        }
    }
    setToPoint (point){
        if(point){
            this.to.point = {
                id: point.id,
                name: point.name,
                address: point.address
            };
            this.to.address = point.address;
        }else {
            this.to.point = null;
        }
    }

    getParamForCreateListGoods (){
        let data =  {
            goodsName: this.name,
            price: this.price,
            quantity: this.quantity,
            extraPrice: this.extraPrice,
            totalPrice: this.totalPrice(),
            paid: this.paid,
            goodsValue: this.value,
            notice: this.notice,
            imageURL: this.from.image||this.to.image
        }
        if(this.goodsType!==null){
            data.goodsTypeId = this.goodsType.id;
        }
        return data;
    }

    getConfigGood(page = 0, count = 50) {
        let self = this;
        let param = { page, count };
        let result = [];
        sendJson({
            url : urlDBD(API.smsConfigDisplayList),
            data : {
                page: 0,
                count: 50,
            },
            async : false,
            success : function (res) {
                $.each(res.results.listMessage, function (c, config) {
                    if (config.messageType == 9){
                        result.push(config)
                    }
                });
            },
            functionIfError : function (res) {
                self.unload('getSettingSmsDisplay');
            }
        });
        return result;
    }

    letSendSMS(goods, receiverPhone, senderPhone, sendSMSSender, sendSMS){
        let self = this
        let goodsName = ''
        let goodsTypeName = ''
        let price = 0
        let totalPrice = 0
        let unpaid = 0
        let paid = 0
        let extraPrice = 0
        let quantity = 0
        let extraPriceNote = ''
        let goodsValue = 0
            goodsName += (goods.goodsName ? goods.goodsName + '' : '')
            goodsTypeName += (goods.goodsTypeName ? goods.goodsTypeName  + '': '')
            extraPriceNote += (goods.extraPriceNote? goods.extraPriceNote + '' : '' )
            price +=goods.price ? parseInt(goods.price) : 0
            totalPrice += goods.totalPrice ? parseInt(goods.totalPrice) : 0
            unpaid += goods.unpaid ? parseInt(goods.unpaid) : 0
            paid += goods.paid ? parseInt(goods.paid) : 0
            extraPrice += goods.extraPrice ?  parseInt(goods.extraPrice)  : 0
            quantity += goods.quantity ? parseInt(goods.quantity) : 0
            goodsValue += goods.goodsValue ? parseInt(goods.goodsValue) : 0

        price = price.toString()
        totalPrice = totalPrice.toString()
        unpaid = unpaid.toString()
        paid = paid.toString()
        extraPrice = extraPrice.toString()
        quantity = quantity.toString()
        goodsValue = goodsValue.toString()

        let config = (self.getConfigGood())[0].nativeContent
        let a = config.replace('${goodsPackageCode}', goods.goodsPackageCode ? goods.goodsPackageCode + ' ' : '')
        let b = a.replace('${goodsName}',goodsName)
        let bb = b.replace('${goodsTypeName}', goodsTypeName)
        let c = bb.replace('${pickPointName}', goods.pickPointName ? goods.pickPointName  + ' ': '')
        let d = c.replace('${dropPointName}', goods.dropPointName ? goods.dropPointName  + ' ' : '')
        let dd = d.replace('${pickUpPoint}', goods.pickUpPoint ? goods.pickUpPoint  + ' ' : '')
        let ddd = dd.replace('${dropOffPoint}', goods.dropOffPoint ? goods.dropOffPoint  + ' ' : '')
        let ee = ddd.replace('${sender}',goods.sender ? goods.sender  + ' ': '')
        let eee = ee.replace('${senderPhone}', goods.senderPhone ? goods.senderPhone  + ' ' : '')
        let gg = eee.replace('${receiver}', goods.receiver ? goods.receiver  + ' ': '')
        let h = gg.replace('${receiverPhone}',goods.receiverPhone ? goods.receiverPhone  + ' ' : '')
        let hh = h.replace('${driverPhonenumber}',goods.trip && goods.trip.drivers && goods.trip.drivers.length > 0 && goods.trip.drivers[0].phoneNumber ? goods.trip.drivers[0].phoneNumber  + ' ' : '')
        let hhh = hh.replace('${vehiclePhonenumber}',goods.trip && goods.trip.vehicle && goods.trip.vehicle.phoneNumber ? goods.trip.vehicle.phoneNumber  + ' ' : '')
        let ii = hhh.replace('${price}', price)
        let jj = ii.replace('${totalPrice}',totalPrice)
        let kk = jj.replace('${unpaid}',unpaid)
        let ll = kk.replace('${paid}', paid)
        let m = ll.replace('${extraPrice}', extraPrice)
        let l = m.replace('${extraPriceNote}',extraPriceNote)
        let i = l.replace('${quantity}',quantity)
        let j = i.replace('${goodsValue}',goodsValue)
        let messageReplace = j.replace('${sendDate}',goods.sendDate ? goods.sendDate  + ' ' : '')


        let data = {
            message: messageReplace,
        }
        if(receiverPhone && sendSMS || senderPhone && sendSMSSender){
            data.phoneNumber = []
            if(receiverPhone && sendSMS){
                data.phoneNumber.push(receiverPhone)
            }
            if(senderPhone && sendSMSSender){
                data.phoneNumber.push(senderPhone)
            }
        }
        window.sendJson({
            url : window.urlDBD(window.API.goodSendSMS),
            data : data,
            success : function (res) {
                notify("Gửi tin nhắn thành công");
            },
            error : function (err) {
                notify("Không gửi được tin nhắn, vui lòng kiểm tra lại số điện thoại");
                console.warn(err);
            }
        });
    }

    save (success,error){
        let self = this
        store.commit('LOAD','SAVE_GOODS');
        let params = {
            goodsCode : self.code,
            goodsValue : self.value,
            goodsName : self.name,
            quantity :self.quantity,
            price : self.price,
            extraPrice : self.extraPrice,
            extraPriceNote : self.extraPriceNote,
            totalPrice : self.totalPrice(),
            paid:self.paid,
            unpaid:self.totalPrice() - self.paid,
            sender : self.from.fullName,
            senderPhone: self.from.phoneNumber,
            pickUpPoint : self.from.address,
            imageURL:self.from.image,
            receiver: self.to.fullName,
            receiverPhone :self.to.phoneNumber,
            dropOffPoint : self.to.address,
            receiverImageURL:self.to.image,
            sendSMS : self.sendSMS,
            sendSMSSender : self.sendSMSSender,
            notice : self.notice,
        };
        params.sendDate = window.func.dateToMS(self.sendDate);
        if(self.goodsType!==null){
            params.goodsTypeId = self.goodsType.id;
        }
        if(self.from.point!==null){
            params.pickPointId = self.from.point.id;
        }
        if(self.to.point!==null){
            params.dropPointId = self.to.point.id;
        }
        if(self.trip.id!==null){
            params.tripId = self.trip.id;
        }
        if(self.id!==null){
            params.goodsId = self.id;
        }
        window.sendJson({
            url : window.urlDBD(this.id?window.API.goodsUpdate:window.API.goodsCreate),
            data : params,
            success :function (result) {
                if(self.sendSMSSender && self.from.phoneNumber || self.sendSMS && self.to.phoneNumber){
                    self.letSendSMS(result.results.goods, self.to.phoneNumber, self.from.phoneNumber,self.sendSMSSender,self.sendSMS)
                }
                store.commit('UNLOAD','SAVE_GOODS');
                if(typeof success == "function"){
                    success(result);
                }
            },
            functionIfError : function (err) {
                store.commit('UNLOAD','SAVE_GOODS');
                if(typeof error == "function"){
                    error(err);
                }
            }
        });
    }
    remove(success,error){
        let self = this;
        if(!this.id){
            return false;
        }
        let cancelReason = window.prompt('Tại sao bạn muốn xóa?');
        if(cancelReason!==null){
            store.commit('LOAD','DELETE_GOODS');
            window.sendJson({
                url : window.urlDBD(window.API.goodsDelete),
                data : {
                    goodsId : self.id,
                    cancelReason : cancelReason
                },
                success :function (result) {
                    store.commit('UNLOAD','DELETE_GOODS');
                    if(typeof success == "function"){
                        success(result);
                    }
                },
                functionIfError : function (err) {
                    store.commit('UNLOAD','DELETE_GOODS');
                    if(typeof error == "function"){
                        error(err);
                    }
                }
            });
        }
    }
}
export class CreateListGoods {
    constructor() {
        this.code = '';
        this.sendDate = new Date().customFormat('#DD#-#MM#-#YYYY#');
        this.sendSMS = false;
        this.sendSMSSender = false;
        this.from = {
            fullName: '',
            phoneNumber: '',
            image:'',
            address: '',
            point: null
        };
        this.to = {
            fullName: '',
            phoneNumber: '',
            image: '',
            address: '',
            point: null
        };
        this.trip = {
            id: null,
            numberPlate:''
        };

        this.senderPayment = false;
        this.printAfterSave = false;
        this.printTemAfterSave = false;
        this.goodsList = [new Goods()];
        this.agency = null;
    }
    addGoods() {
        let g = new Goods();
        g.setSenderPayment(this.senderPayment);
        this.goodsList.push(g);
    }
    removeGoods(key){
        this.goodsList.splice(key,1);
    }
    setSenderPayment(value){
        let self = this;
        self.senderPayment = value?true:false;
        $.each(self.goodsList,function (g,goods) {
            goods.setSenderPayment(value);
        });
    }

    setFromPoint (point){
        if(point){
            this.from.point = {
                id: point.id,
                name: point.name,
                address: point.address
            };
            this.from.address = point.address;
        }else {
            this.from.point = null;
        }
    }
    setToPoint (point){
        if(point){
            this.to.point = {
                id: point.id,
                name: point.name,
                address: point.address
            };
            this.to.address = point.address;
        }else {
            this.to.point = null;
        }
    }
    getConfigGood(page = 0, count = 50) {
        let self = this;
        let param = { page, count };
        let result = [];
        sendJson({
            url : urlDBD(API.smsConfigDisplayList),
            data : {
                page: 0,
                count: 50,
            },
            async : false,
            success : function (res) {
                $.each(res.results.listMessage, function (c, config) {
                    if (config.messageType == 9){
                        result.push(config)
                    }
                });
            },
            functionIfError : function (res) {
                self.unload('getSettingSmsDisplay');
            }
        });
        return result;
    }
    callApiSendSms (param) {
        window.sendJson({
            url : window.urlDBD(window.API.goodV2SendSms),
            data : param,
            success : function (res) {
                notify("Gửi tin nhắn thành công");
            },
            error : function (err) {
                console.warn(err);
            }
        });
    }
    letSendSMS(goods, receiverPhone, senderPhone, sendSMSSender, sendSMS){
        let self = this;
        $.each(goods, function (g, good) {
            let goodsName = ''
            let goodsTypeName = ''
            let price = 0
            let totalPrice = 0
            let unpaid = 0
            let paid = 0
            let extraPrice = 0
            let quantity = 0
            let extraPriceNote = ''
            let goodsValue = 0
            for(let i = 0; i < good.goodsList.length; i++ ) {
                if (good.goodsList.length == h) {
                    goodsName += (good.goodsList[i].goodsName ? good.goodsList[i].goodsName + '' : '')
                    goodsTypeName += (good.goodsList[i].goodsTypeName ? good.goodsList[i].goodsTypeName  + '': '')
                    extraPriceNote += (good.goodsList[i].extraPriceNote? good.goodsList[i].extraPriceNote + '' : '' )
                } else {
                    goodsName += (good.goodsList[i].goodsName ? good.goodsList[i].goodsName + ',' : '')
                    goodsTypeName += (good.goodsList[i].goodsTypeName ? good.goodsList[i].goodsTypeName  + ',': '')
                    extraPriceNote += (good.goodsList[i].extraPriceNote? good.goodsList[i].extraPriceNote + ',' : '' )
                }
                price += good.goodsList[i].price ? parseInt(good.goodsList[i].price) : 0
                totalPrice += good.goodsList[i].totalPrice ? parseInt(good.goodsList[i].totalPrice) : 0
                unpaid += good.goodsList[i].unpaid ? parseInt(good.goodsList[i].unpaid) : 0
                paid += good.goodsList[i].paid ? parseInt(good.goodsList[i].paid) : 0
                extraPrice += good.goodsList[i].extraPrice ?  parseInt(good.goodsList[i].extraPrice)  : 0
                quantity += good.goodsList[i].quantity ? parseInt(good.goodsList[i].quantity) : 0
                goodsValue += good.goodsList[i].goodsValue ? parseInt(good.goodsList[i].goodsValue) : 0
            }
            price = price.toString()
            totalPrice = totalPrice.toString()
            unpaid = unpaid.toString()
            paid = paid.toString()
            extraPrice = extraPrice.toString()
            quantity = quantity.toString()
            goodsValue = goodsValue.toString()

            let config = (self.getConfigGood())[0].nativeContent
            let a = config.replace('${goodsPackageCode}', good.goodsPackageCode ? good.goodsPackageCode + ' ' : '')
            let b = a.replace('${goodsName}',goodsName)
            let bb = b.replace('${goodsTypeName}', goodsTypeName)
            let c = bb.replace('${pickPointName}', good.goodsList[0].pickPointName ? good.goodsList[0].pickPointName  + ' ': '')
            let d = c.replace('${dropPointName}', good.goodsList[0].dropPointName ? good.goodsList[0].dropPointName  + ' ' : '')
            let dd = d.replace('${pickUpPoint}', good.goodsList[0].pickUpPoint ? good.goodsList[0].pickUpPoint  + ' ' : '')
            let ddd = dd.replace('${dropOffPoint}', good.goodsList[0].dropOffPoint ? good.goodsList[0].dropOffPoint  + ' ' : '')
            let ee = ddd.replace('${sender}',good.goodsList[0].sender ? good.goodsList[0].sender  + ' ': '')
            let eee = ee.replace('${senderPhone}', good.goodsList[0].senderPhone ? good.goodsList[0].senderPhone  + ' ' : '')
            let gg = eee.replace('${receiver}', good.goodsList[0].receiver ? good.goodsList[0].receiver  + ' ': '')
            let h = gg.replace('${receiverPhone}',good.goodsList[0].receiverPhone ? good.goodsList[0].receiverPhone  + ' ' : '')
            let hh = h.replace('${driverPhonenumber}',good.trip && good.trip.drivers && good.trip.drivers.length > 0 && good.trip.drivers[0].phoneNumber ? good.trip.drivers[0].phoneNumber  + ' ' : '')
            let hhh = hh.replace('${vehiclePhonenumber}',good.trip && good.trip.vehicle && good.trip.vehicle.phoneNumber ? good.trip.vehicle.phoneNumber  + ' ' : '')
            let ii = hhh.replace('${price}', price)
            let jj = ii.replace('${totalPrice}',totalPrice)
            let kk = jj.replace('${unpaid}',unpaid)
            let ll = kk.replace('${paid}', paid)
            let m = ll.replace('${extraPrice}', extraPrice)
            let l = m.replace('${extraPriceNote}', extraPriceNote)
            let lll = l.replace('${roomAddress}', window.userInfo.userInfo.room.address)
            let i = lll.replace('${quantity}',quantity)
            let j = i.replace('${goodsValue}',goodsValue)
            let messageReplace = j.replace('${sendDate}',good.goodsList[0].sendDate ? good.goodsList[0].sendDate  + ' ' : '')

            // let data = {
            //     message: messageReplace,
            // }
            // if(receiverPhone && sendSMS || senderPhone && sendSMSSender){
            //     data.phoneNumber = []
            //     if(receiverPhone && sendSMS){
            //         data.phoneNumber.push(receiverPhone)
            //     }
            //     if(senderPhone && sendSMSSender){
            //         data.phoneNumber.push(senderPhone)
            //     }
            // }
           const message = messageReplace;
           if(good.goodsList.length === 0) {
            return;
           }
           if(receiverPhone && sendSMS) {
                const param = {
                    message: message,
                    type: 1,
                    goodsId: good.goodsList[0].goodsId
                }
                self.callApiSendSms(param);
           }
           if(senderPhone && sendSMSSender) {
                const param = {
                    message: message,
                    type: 0,
                    goodsId: good.goodsList[0].goodsId
                }
                self.callApiSendSms(param);
           }
        })
    }

    save (success,error){
        let self = this;
        store.commit('LOAD','SAVE_GOODS');
        let params = {
            goodsCode : self.code,
            sender : self.from.fullName,
            senderPhone: self.from.phoneNumber,
            pickUpPoint : self.from.address,
            receiver: self.to.fullName,
            receiverPhone :self.to.phoneNumber,
            dropOffPoint : self.to.address,
            sendSMS : self.sendSMS,
            sendSMSSender : self.sendSMSSender,
            goodsDetails : [],
            agency: self.agency ? { id: self.agency.id} : null
        };
        params.sendDate = window.func.dateToMS(self.sendDate);
        if(self.from.point!==null){
            params.pickPointId = self.from.point.id;
        }
        if(self.to.point!==null){
            params.dropPointId = self.to.point.id;
        }
        if(self.trip.id!==null){
            params.tripId = self.trip.id;
        }
        $.each(self.goodsList,function (g,goods) {
            params.goodsDetails.push(goods.getParamForCreateListGoods());
        })

        window.sendJson({
            url : window.urlDBD(API.goodsCreateMultiple),
            data : params,
            success :function (result) {
                store.commit('UNLOAD','SAVE_GOODS');
                if(typeof success == "function"){
                    success(result);
                }
                if(self.sendSMSSender && self.from.phoneNumber || self.sendSMS && self.to.phoneNumber){
                    self.letSendSMS(result.results.goods, self.to.phoneNumber, self.from.phoneNumber,self.sendSMSSender,self.sendSMS)
                }
            },
            functionIfError : function (err) {
                store.commit('UNLOAD','SAVE_GOODS');
                if(typeof error == "function"){
                    error(err);
                }
            }
        });
    }
}
export class ListGoods {
    constructor (listGoods){
        let list = [];
        $.each(listGoods,function (k,value) {
            list.push(new Goods(value));
        });
        this.list = list;
    }
    getList (){
        return this.list;
    }
}