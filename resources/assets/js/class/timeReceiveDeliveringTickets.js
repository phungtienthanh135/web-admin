import { formatMiliseconds } from "./function";

export class DayBeforeWarning {
    static properties (){
        return [
            {value : 0,label:'cùng ngày'},
            {value : 1,label:'ngày hôm trước'},
            {value : 2,label:'2 ngày hôm trước'},
            {value : 3,label:'3 ngày hôm trước'},
            {value : 4,label:'4 ngày hôm trước'},
            {value : 5,label:'5 ngày hôm trước'},
        ];
    }
}

export class TimeReceiveDeliveringTickets {
    constructor(props){
        this.id = props&&props.id ? props.id : null;
        this.timeStartTrip = props&&props.timeStartTrip ? formatMiliseconds(props.timeStartTrip,'#hhhh#:#mm#') : '';
        this.id = props&&props.id ? props.id : null;
        this.id = props&&props.id ? props.id : null;
    }
}