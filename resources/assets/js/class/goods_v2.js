import filters from '../Helper/Filters';
import store from "../store/index";
import {ElectronicTicket} from "./reportElectronicTicket";

export class TypeOfGoodsExtraFee {
    constructor(prop) {
        this.value = prop !== null && prop !== undefined ? prop : 0;
    }

    label() {
        try {
            return TypeOfGoodsExtraFee.properties()[this.value].label;
        } catch (e) {
            return '';
        }
    }

    static properties() {
        return [
            {label: 'Bảo hiểm', value: 0},
            {label: 'Phụ phí', value: 1},
            {label: 'Dịch vụ', value: 2},
        ];
    }

    static enum() {
        return {
            INSURRANCE: 0,
            SURCHARGE: 1,
            SERVICE: 2
        };
    }
}

export class GoodsType {
    constructor(prop) {
        this.value = prop !== null && prop !== undefined ? prop : 0;
    }

    label() {
        try {
            return GoodsType.properties()[this.value].label;
        } catch (e) {
            return '';
        }
    }

    static properties() {
        return [
            {label: 'Hàng hóa', value: 0, type: true},
            {label: 'Tiền', value: 1, type: true},
            {label: 'COD', value: 2, type: false},
        ];
    }

    static enum() {
        return {
            GOODS: 0,
            MONEY: 1,
            COD: 2,
        };
    }
}


export class GoodsPaymentMethod {
    constructor(prop) {
        this.value = prop !== null && prop !== undefined ? prop : 1;
    }

    static properties() {
        return [
            {label: 'Tiền mặt', value: 1},
        ];
    }

    static enum() {
        return {
            MONEY: 1,
        };
    }
}

export class AutoField {
    constructor(prop) {
        this.value = prop !== null && prop !== undefined ? prop : 0;
    }

    getLabel() {
        try {
            return AutoField.properties()[this.value].label;
        } catch (e) {
            return '';
        }
    }

    static properties() {
        return {
            value: {label: 'Giá trị hàng', value: 'value', code: '${value}', unit: 'VNĐ'},
            weight: {label: 'Khối lượng', value: 'weight', code: '${weight}', unit: 'KG'},
            originalFee: {label: 'Phí loại hình vận chuyển', value: 'originalFee', code: '${originalFee}', unit: 'VNĐ'},
            totalFee: {label: 'Toàn bộ phí', value: 'totalFee', code: '${totalFee}', unit: 'VNĐ'},
            quantity: {label: 'Số lượng', value: 'quantity', code: '${quantity}', unit: 'đơn vị'},
            cod: {label: 'Tiền thu hộ', value: 'cod', code: '${cod}', unit: 'VNĐ'},
        };
    }

    static enum() {
        // value - giá trị mặt hàng
        // weight - khối lượng
        // originalFee - phí loại hình vận chuyển
        // totalFee - toàn bộ phí
        // quantity - só lượng
        // cod - tiền thu hộ
        return {
            INSURRANCE: 0,
            SURCHARGE: 1,
            SERVICE: 2
        };
    }
}

//gói giá
export class Criteria {
    constructor(props) {
        this.id = props&&props.id ? props.id : null;
        this.name = props&&props.name ? props.name : '';
        this.criteriaField = props&&props.criteriaField ? props.criteriaField : '';
        this.usingThreshold = props&&(props.upperThreshold||props.lowerThreshold) ?true : false;
        this.upperThreshold = props && props.upperThreshold ? props.upperThreshold : null;
        this.lowerThreshold = props && props.lowerThreshold ? props.lowerThreshold : null;
        this.formula = props&&props.formula ? props.formula : '';
    }
}

//danh sách gói giá
export class Criterias {
    constructor(listCriteria) {
        this.list = [];
        if (listCriteria) {
            this.setList(listCriteria);
        }
    }

    setList(list) {
        let self = this;
        this.list = [];
        $.each(list, function (i, item) {
            self.list.push(item instanceof Criteria ? item : new Criteria(item));
        });
    }
}

// phu phi
export class ExtraFee {
    constructor(props) {
        this.id = props && props.id ? props.id : null;
        this.active = props && props.active ? props.active : true;
        this.name = props && props.name ? props.name : '';
        this.type = new TypeOfGoodsExtraFee(props && props.type ? props.type : null);
        this.goodsType = new GoodsType(props && props.goodsType ? props.goodsType : null);
        this.required = props && props.required ? props.required : false;
        this.usingExistedField = props && props.usingExistedField ? props.usingExistedField : false;
        this.criteria = [];
        this.setCriteria(props && props.criteria ? props.criteria : []);
        this.criteriaField = this.criteria.length>0&&this.criteria[0].criteriaField?this.criteria[0].criteriaField:'';
        this.appliedCriterion = props && props.appliedCriterion ? props.appliedCriterion : null;
        this.allowChangeAppliedCirteria = true;
    }

    getExtraFee() {
        let result = {
            id: this.id,
            name: this.name,
        };
        if (!this.usingExistedField) {
            result.appliedCriterion = this.appliedCriterion;
        }
        return result;
    }

    setCriteria(list) {
        let self = this;
        this.criteria = [];
        $.each(list, function (i, item) {
            self.criteria.push(item instanceof Criteria ? item : new Criteria(item));
        });
    }

    save (options){
        let self = this;
        let param = {
            name : self.name,
            type : self.type.value,
            goodsType: self.goodsType.value,
            required : self.required,
            usingExistedField : self.usingExistedField,
            criteria : [],
        }
        if(self.criteria.length>0){
            $.each(self.criteria,function (i,item) {
                let crit = {
                    name : item.name,
                    criteriaField : self.criteriaField,
                    upperThreshold : item.upperThreshold?item.upperThreshold:null,
                    lowerThreshold : item.lowerThreshold?item.lowerThreshold:null,
                    formula : item.formula
                }
                if(item.id){
                    crit.id = item.id;
                }
                param.criteria.push(crit);
            });
        }
        if(self.id){
            param.id = self.id;
        }
        if (self.id && options && options.remove) {
            param.active = false;
        }
        if(options&&typeof options.before === 'function'){
            options.before();
        }
        window.sendJson({
            url : urlDBD(API.goodsV2ExtraFee),
            data : param,
            success : function (data) {
                if(options&&typeof options.success === 'function'){
                    options.success(data);
                }
            },
            functionIfError : function (err) {
                if(options&&typeof options.error === 'function'){
                    options.error(err);
                }
            }
        });
    }
}

// danh sach phu phi
export class ExtraFees {
    constructor(props) {
        this.page = 0;
        this.count = 30;
        this.loadMore = false;
        this.loading = false;
        this.goodsType = null;
        this.required = null;
        this.ids = [];
        this.list = [];
    }

    getList(options) { // number page : tăng hoặc giảm page
        let self = this;
        if (options && !isNaN(options.numberPage)) {
            self.page += parseInt(options.numberPage);
        } else {
            self.page = 0;
        }

        let params = {
            page: self.page,
            count: self.count
        };
        if (self.keyword) {
            params.numberPlate = self.keyword;
        }
        if (self.goodsType !== null) {
            params.goodsType = self.goodsType;
        }
        if (self.required !== null) {
            params.required = self.required;
        }
        if (self.ids && self.ids.length > 0) {
            params.ids = self.ids;
        }
        self.loading = true;
        self.loadMore = false;
        if (options && typeof options.before === 'function') {
            options.before();
        }
        window.sendParam({
            url: window.urlDBD(window.API.goodsV2ExtraFee),
            data: params,
            type: "get",
            success: function (data) {
                self.loadMore = true;
                if (data.results.types.length < self.count) {
                    self.loadMore = false;
                }
                self.loading = false;
                if (self.page <= 0) {
                    self.setList(data.results.types);
                } else {
                    self.setList(data.results.types, {push: true});
                }
                if (options && typeof options.success === 'function') {
                    options.success(data);
                }
            },
            functionIfError: function (data) {
                self.loading = false;
                if (options && typeof options.error === 'function') {
                    options.error(data);
                }
            }
        });
    }

    setList(list, options) {
        let self = this;
        if (!(options && options.push)) {
            self.list = [];
        }
        $.each(list, function (i, item) {
            self.list.push(item instanceof ExtraFee ? item : new ExtraFee(item));
        });
    }
}


export class PointPricingGoodsFee {
    constructor(props) {
        this.name = props && props.name ? props.name : '';
        this.receivedPoints = props && props.receivedPoints ? props.receivedPoints : [];
        this.deliveryPoints = props && props.deliveryPoints ? props.deliveryPoints : [];

        this.criteria = [];
        this.setCriteria(props && props.criteria ? props.criteria : []);
        this.criteriaField = this.criteria.length > 0 && this.criteria[0].criteriaField ? this.criteria[0].criteriaField : '';
    }

    setCriteria(list) {
        let self = this;
        this.criteria = [];
        $.each(list, function (i, item) {
            self.criteria.push(item instanceof Criteria ? item : new Criteria(item));
        });
    }
}

// bang gia
export class GoodsFee {
    constructor(props) {
        this.id = props && props.id ? props.id : null;
        this.name = props && props.name ? props.name : '';
        this.active = props && props.active ? props.active : true;
        this.transportType = props && props.transportType ? props.transportType : null;
        this.goodsType = new GoodsType(props && props.goodsType ? props.goodsType : null);
        this.required = props && props.required ? props.required : false;
        this.usingExistedField = props && props.usingExistedField ? props.usingExistedField : false;
        this.pointPricing = new PointPricingGoodsFee(props && props.pointPricing ? props.pointPricing : null);
        this.appliedCriterion = null;
    }

    setGoodsType(data) {
        this.goodsType = data instanceof GoodsType ? data : new GoodsType(data);
    }

    save(options) {
        let self = this;
        let param = {
            name: self.name,
            goodsType: self.goodsType.value,
            required: self.required,
            usingExistedField: self.usingExistedField,
            transportType: self.transportType,
            pointPricing: {
                name: self.pointPricing.name,
                receivedPointIds: self.pointPricing.receivedPoints.map(({id}) => id),
                receivedPoints: self.pointPricing.receivedPoints,
                deliveryPointIds: self.pointPricing.deliveryPoints.map(({id}) => id),
                deliveryPoints: self.pointPricing.deliveryPoints,
                criteria: []
            },
        }
        if (self.pointPricing.criteria.length > 0) {
            $.each(self.pointPricing.criteria, function (i, item) {
                let crit = {
                    name: item.name,
                    criteriaField: self.pointPricing.criteriaField,
                    upperThreshold: item.upperThreshold ? item.upperThreshold : null,
                    lowerThreshold: item.lowerThreshold ? item.lowerThreshold : null,
                    formula: item.formula
                }
                if (item.id) {
                    crit.id = item.id;
                }
                param.pointPricing.criteria.push(crit);
            });
        }
        if (self.id) {
            param.id = self.id;
        }
        if (self.id && options && options.remove) {
            param.active = false;
        }
        if (options && typeof options.before === 'function') {
            options.before();
        }
        window.sendJson({
            url: urlDBD(API.goodsV2Fee),
            data: param,
            success: function (data) {
                if (options && typeof options.success === 'function') {
                    options.success(data);
                }
            },
            functionIfError: function (err) {
                if (options && typeof options.error === 'function') {
                    options.error(err);
                }
            }
        })
    }
}

// danh sach bang gia
export class GoodsFees {
    constructor() {
        this.page = 0;
        this.count = 30;
        this.loadMore = false;
        this.loading = false;
        this.goodsType = null;
        this.transportTypeId = null;
        this.receivedPointId = null;
        this.deliveryPointId = null;
        this.ids = [];
        this.list = [];
    }

    getList(options) { // number page : tăng hoặc giảm page
        let self = this;
        if (options && !isNaN(options.numberPage)) {
            self.page += parseInt(options.numberPage);
        } else {
            self.page = 0;
        }

        let params = {
            page: self.page,
            count: self.count
        };
        // if (self.keyword) {
        //     params.numberPlate = self.keyword;
        // }
        if (self.goodsType !== null) {
            params.goodsType = self.goodsType;
        }
        if (self.transportTypeId !== null) {
            params.transportTypeId = self.transportTypeId;
        }
        if (self.receivedPointId !== null) {
            params.receivedPointId = self.receivedPointId;
        }
        if (self.deliveryPointId !== null) {
            params.deliveryPointId = self.deliveryPointId;
        }
        if (this.ids && this.ids.length > 0) {
            params.ids = this.ids;
        }
        self.loading = true;
        self.loadMore = false;
        if (options && typeof options.before === 'function') {
            options.before();
        }

        window.sendParam({
            url: window.urlDBD(window.API.goodsV2Fee),
            data: params,
            type: "get",
            success: function (data) {
                self.loadMore = true;
                if (data.results.types.length < self.count) {
                    self.loadMore = false;  
                }
                self.loading = false;
                if (self.page <= 0) {
                    self.setList(data.results.types);
                } else {
                    self.setList(data.results.types, {push: true});
                }
                if (options && typeof options.success === 'function') {
                    options.success(data);
                }
            },
            functionIfError: function (data) {
                self.loading = false;
                if (options && typeof options.error === 'function') {
                    options.error(data);
                }
            }
        });
    }

    setList(list, options) {
        let self = this;
        if (!(options && options.push)) {
            self.list = [];
        }
        $.each(list, function (i, item) {
            self.list.push(item instanceof GoodsFee ? item : new GoodsFee(item));
        });
    }
}

//phuong thuc nhan va tra hang
export class CollectionAnDeliveryType {
    constructor(props) {
        this.value = props ? props : 0;
    }

    typeAllowed(CollectionOrDelivery) {
        if (CollectionOrDelivery === 'Collection') {
            let valueAllowed = store.state.CompanyConfig.companyConfig.CONFIG.VALID_GOODS_COLLECTION_METHODS;
            if (valueAllowed) {
                $.each(valueAllowed.value)
            }
        } else {
            let valueAllowed = store.state.CompanyConfig.companyConfig.CONFIG.VALID_GOODS_DELIVERY_METHODS;

        }
    }

    static properties() {
        return [
            {label: "Tại bưu cục", value: 0},
            {label: "Tận nhà", value: 1},
            {label: "Dọc đường", value: 2},
        ]
    }

    static enum() {
        return {
            ROOM: 0,
            HOME: 1,
            THE_WAY: 2,
        }
    }
}

export class Collection {
    constructor(props, options) {
        let date = new Date();
        let dateStr = `${date.getFullYear()}${date.getMonth() + 1}${date.getDate()}`
        this.address = props && props.address ? props.address : '';
        this.phhoneNumber = props && props.phhoneNumber ? props.phhoneNumber : '';
        this.regionInfo = props && props.regionInfo ? props.regionInfo : null;
        this.workShiftInfo = props && props.workShiftInfo ? props.workShiftInfo : null;
        // this.date = (props && props.date ? props.date : new Date().customFormat('#YYYY##MM##DD#')).toString();
        this.date = (props && props.date ? props.date : dateStr).toString();
        this.time = props && props.time ? (typeof props.time === 'string' ? props.time : filters.customDate(props.time, '#hhhh#:#mm#', true)) : dateStr;
        this.courier = props && props.courier ? props.courier : null;
    }

    getTimeMiliseconds (){
        return window.func.timeToMs(this.time);
    }

    getCourier(){
        let self = this;
        if(!self.courier){
            return null
        }
        return {
            id : self.courier.id,
            userName : self.courier.userName,
            fullName : self.courier.fullName,
            email : self.courier.email,
            phoneNumber : self.courier.phoneNumber
        }
    }
}

//trang thai don hang
export class GoodsStatus {
    constructor(props) {
        this.value = props ? props : GoodsStatus.enum().CANCELED;
    }

    label() {
        return GoodsStatus.properties()[this.value].label;
    }

    color() {
        return GoodsStatus.properties()[this.value].color;
    }

    static properties() {
        return [
            {label: "Đã hủy", value: 0, name: 'CANCELED', color: '#CFD3DB'},
            {label: "Yêu cầu nhận hàng", value: 1, name: 'REQUEST', color: '#5C9CFE'},
            {label: "Đã nhận hàng (chờ phân xe)", value: 2, name: 'REQUEST_SUCCESS', color: '#007bff'},
            {label: "Trên xe vận chuyển", value: 3, name: 'ON_VEHICLE', color: '#084388'},
            {label: "Tại điểm giao hàng", value: 4, name: 'ON_ROOM_DELIVERY', color: '#F39B13'},
            {label: "Đang giao hàng", value: 5, name: 'DELIVERING', color: '#6979F8'},
            {label: "Đã giao hàng", value: 6, name: 'DELIVERY_SUCCESS', color: '#00C48C'},
            {label: "Giao hàng thất bại", value: 7, name: 'DELIVERY_ERROR', color: '#E86966'},
        ]
    }

    static enum() {
        return {
            CANCELED: 0,
            REQUEST: 1,
            REQUEST_SUCCESS: 2,
            ON_VEHICLE: 3,
            ON_ROOM_DELIVERY: 4,
            DELIVERING: 5,
            DELIVERY_SUCCESS: 6,
            DELIVERY_ERROR: 7,
        }
    }

    getStatusByValue(value) {
        let data = GoodsStatus.properties().filter( _ => _.value == value)
        return  data ? data[0] : {label: '', color: ''}
    }
}

export class UserOfGoods {
    constructor(props, options) {
        this.phoneNumber = props && props.phoneNumber ? props.phoneNumber : '';
        this.name = props && props.name ? props.name : '';
        this.citizenId = props && props.citizenId ? props.citizenId : '';
        this.sendSMS = props && props.sendSMS ? props.sendSMS : false;
    }
}

export class Dimensions {
    constructor(props, options) {
        this.weight = props && props.weight ? Number(props.weight) : 0;
        this.convertFromVolume = props && props.convertFromVolume ? Number(props.convertFromVolume) : false;
        this.length = props && props.length ? Number(props.length) : 1;
        this.width = props && props.width ? Number(props.width) : 1;
        this.height = props && props.height ? Number(props.height) : 1;
    }
}

//chi tiet hang
export class GoodsDetail {
    constructor(props, options) {
        this.name = props && props.name ? props.name : '';
        this.code = props && props.code ? props.code : null;
        this.value = props && props.value ? props.value : 0;
        this.originalFee = props && props.originalFee ? props.originalFee : 0;
        this.totalFeeInput = props && props.totalFee ? props.totalFee : 0;
        this.quantity = props && props.quantity ? props.quantity : 1;
        this.note = props && props.note ? props.note : '';
        this.type = props&&props.type&&props.type instanceof GoodsFee ? props.type : new GoodsFee(props && props.type ? props.type : null);
        
        
        if(!this.type.id){
            this.totalFeeInput = props && props.originalFee ? props.originalFee : 0;    
        }

        this.appliedCriterion = props && props.appliedCriterion ? props.appliedCriterion : null;
        this.allowChangeCriteria = true;
        this.imageUrls = props && props.imageUrls ? props.imageUrls : [];
        this.loadingExtraFeeRequired = false;
        this.extraFees = [];
        this.setExtraFees(props && props.extraFees ? props.extraFees : []);
        this.dimensions = new Dimensions(props && props.dimensions ? props.dimensions : null);
    }

    getDetail() {
        let result = {
            name: this.name,
            code: this.code,
            value: this.value,
            quantity: this.quantity,
            note: this.note,
            type: null,
            imageUrls: this.imageUrls,
            dimensions: this.dimensions,
            extraFees: [],
            totalFee: this.totalFee(),
            originalFee: Number(this.originalFee)
        };

        // if (!this.appliedCriterion || !this.appliedCriterion.id) {
        //     result.adjustFee = Number(this.totalFeeInput)
        // }
        if (this.type !== null && this.type.id !== null){
            result.type = {
                id: this.type ? this.type.id : null,
                name: this.name ? this.type.name : '',
            }
            result.extraFees = this.getExtraFees()
        }
        if(!this.type.usingExistedField && this.type.id !== null){
            result.type.priceInfo = {appliedCriterion : this.appliedCriterion};
        }

        return result;
    }

    getExtraFees() {
        let arr = [];
        $.each(this.extraFees, function (e, extra) {
            let etf = extra.getExtraFee();
            etf.priority = e;
            arr.push(etf);
        });
        return arr;
    }

    setType(props) {
        this.type = props instanceof GoodsFee ? props : new GoodsFee(props);

        if(props != null) {
            this.setAppliedCriteria();
            this.getExtraFeeRequired();
        }
    }

    getExtraFeeRequired() {
        let self = this;
        if (this.type && this.type.id) {
            let extraFees = new ExtraFees();
            extraFees.required = true;
            extraFees.goodsType = this.type.goodsType.value;
            extraFees.getList({
                before: function () {
                    self.loadingExtraFeeRequired = true;
                },
                success: function () {
                    self.loadingExtraFeeRequired = false;
                    self.setExtraFees(extraFees.list);
                    self.setDefaultExtraFeeAppliedCriteria();
                },
                error: function () {
                    self.loadingExtraFeeRequired = false;
                }
            })
        } else {
            self.setExtraFees([]);
        }
    }

    setDefaultExtraFeeAppliedCriteria() {
        let self = this;
        $.each(self.extraFees, function (e, extraFee) {
            self.setAppliedCriteriaExtraFeeByIndex(null, e);
        })
    }

    setExtraFeeByIndex(props, index) {
        if (props) {
            this.extraFees[index] = _.cloneDeep(props);
        } else {
            this.extraFees[index] = new ExtraFee();
        }
        this.setAppliedCriteriaExtraFeeByIndex(null, index);
    }

    totalExtraFees() {
        let total = 0;
        $.each(this.getCalculateExtraFee(), function (i, e) {
            total += Number(e);
        })
        return total;
    }

    setAppliedCriteriaExtraFeeByIndex(props, index) {
        let self = this;
        let cri = null;
        this.extraFees[index].allowChangeAppliedCirteria = true;
        if (props) {
            cri = props;
        } else {
            let criteriaList = this.extraFees[index].criteria;
            let criteriaField = this.extraFees[index].criteriaField;
            cri = self.getAppliedCriteria(criteriaList, criteriaField);
            this.extraFees[index].allowChangeAppliedCirteria = cri ? false : true;
        }
        this.extraFees[index].appliedCriterion = cri instanceof Criteria ? cri : (cri == null ? null : new Criteria(cri));
        // this.setOriginalFee();
    }

    getAppliedCriteria(criteriaList, criteriaField) {
        let self = this;
        let cri = null;
        if (self.criteriaFieldValue(criteriaField) !== undefined) {
            $.each(criteriaList, function (c, cr) {
                if (!cr.lowerThreshold && !cr.upperThreshold) {
                    cri = cr;
                    return false;
                }
                if (!cr.lowerThreshold && cr.upperThreshold && self.criteriaFieldValue(criteriaField) < cr.upperThreshold) {
                    cri = cr;
                    return false;
                }
                if (!cr.upperThreshold && cr.lowerThreshold && self.criteriaFieldValue(criteriaField) > cr.lowerThreshold) {
                    cri = cr;
                    return false;
                }
                if (!(parseFloat(cr.lowerThreshold) > self.criteriaFieldValue(criteriaField) ||
                    parseFloat(cr.upperThreshold) < self.criteriaFieldValue(criteriaField))) {
                    cri = cr;
                    return false;
                }
            });
        }
        return cri
    }

    setExtraFees(props) {
        let arr = []
        $.each(props, function (p, prop) {
            arr.push(prop instanceof ExtraFee ? prop : new ExtraFee(prop));
        })
        this.extraFees = arr;
    }

    addExtraFee() {
        this.extraFees.push(new ExtraFee());
    }

    removeExtraFee(index) {
        this.extraFees.splice(index, 1);
    }

    getCalculateExtraFee() {
        let self = this;
        let results = [];
        let value = self.criteriaFieldValue('value');
        let weight = self.criteriaFieldValue('weight');
        let quantity = self.criteriaFieldValue('quantity');
        let originalFee = self.criteriaFieldValue('originalFee');
        let totalFee = _.cloneDeep(originalFee);
        $.each(self.extraFees, function (e, extraFee) {
            if (!extraFee.appliedCriterion || !extraFee.appliedCriterion.id) {
                results.push(0);
                return;
            }
            let formula = extraFee.appliedCriterion.formula;
            let propertiesAutoField = AutoField.properties();
            $.each(propertiesAutoField, function (p, pro) {
                formula = formula.replaceAll(pro.code, pro.value);
            });
            let fee = eval(formula);
            totalFee += parseFloat(fee);
            results.push(fee);
        })

        return results;
    }

    setWeight(props) {
        if (props != null) {
            this.dimensions.weight = props;
        }

        if(this.type.id != null) {
            this.setAppliedCriteria();
        }
        
    }

    setWidth(props) {
        this.dimensions.width = props !== null && props !== undefined && props !== '' ? props : 1;
        this.calculateWeight();
    }

    setHeight(props) {
        this.dimensions.height = props !== null && props !== undefined && props !== '' ? props : 1;
        this.calculateWeight();
    }

    setLength(props) {
        this.dimensions.length = props !== null && props !== undefined && props !== '' ? props : 1;
        this.calculateWeight();
    }

    calculateWeight() {
        if (store.state.CompanyConfig.companyConfig.CONFIG.SIZE_WEIGHT_CONVERSION.value) {
            let width = this.dimensions.width;
            let height = this.dimensions.height;
            let length = this.dimensions.length;
            let formula = store.state.CompanyConfig.companyConfig.CONFIG.SIZE_WEIGHT_CONVERSION.value;
            formula = formula.replaceAll('${width}', 'width');
            formula = formula.replaceAll('${height}', 'height');
            formula = formula.replaceAll('${length}', 'length');
            this.setWeight(eval(formula));
        }
    }

    setQuantity(props) {
        
        // this.quantity = props || 1
        this.quantity = props
        if(this.type.id != null) {
            this.setAppliedCriteria();
        }
    }

    setValue(props) {
        this.value = props||0;
        if(this.type.id != null) {
            this.setAppliedCriteria();
        }
    }

    setAppliedCriteria(props) {
        let self = this;
        let cri = null;
        if (props) {
            cri = props;
            this.allowChangeCriteria = true;
        } else {
            this.allowChangeCriteria = true;
            if (this.type) {
                let criteriaList = this.type.pointPricing.criteria;
                let criteriaField = this.type.pointPricing.criteriaField;
                cri = self.getAppliedCriteria(criteriaList, criteriaField);
                this.allowChangeCriteria = cri ? false : true;
            }
        }
        this.appliedCriterion = cri instanceof Criteria ? cri : (cri == null ? null : new Criteria(cri));
        this.setOriginalFee();
    }

    calcOriginalFee() {
        let value = this.criteriaFieldValue('value');
        let weight = this.criteriaFieldValue('weight');
        let quantity = this.criteriaFieldValue('quantity');
        let totalFee = this.criteriaFieldValue('totalFee');
        let formula = this.appliedCriterion.formula;
        let propertiesAutoField = AutoField.properties();
        $.each(propertiesAutoField, function (p, pro) {
            formula = formula.replaceAll(pro.code, pro.value);
        });
        return eval(formula);
    }

    setOriginalFee() {
        if (!this.appliedCriterion || !this.appliedCriterion.id) {
            this.originalFee = 0;
            return;
        }

        this.originalFee = this.calcOriginalFee()
        this.setDefaultExtraFeeAppliedCriteria();
    }

    totalFee() {
        if (this.type == null || this.type.id == null){
            return parseInt(this.totalFeeInput) * this.quantity +  Number(this.totalExtraFees())
        } else {
            return Number(this.originalFee) * this.quantity +  Number(this.totalExtraFees())
        }
    }

    criteriaFieldValue(field) {
        if (field === 'weight') {
            return this.dimensions.weight;
        }
        if (this[field] !== undefined) {
            return this[field];
        }
        return undefined;
    }
}

//hang (thong tin hang)
export class Goods {
    constructor(props, options) {
        this.id = props && props.id ? props.id : null;
        this.companyId = props && props.companyId ? props.companyId : null;
        this.collectionId = props && props.collectionId ? props.collectionId : null;
        this.code = props && props.code ? props.code : null;
        this.packageCode = props && props.packageCode ? props.packageCode : null;
        this.transportType = props && props.transportType ? props.transportType : null;
        this.status = new GoodsStatus(props && props.status ? props.status : null);
        this.receivedPoint = props && props.receivedPoint ? props.receivedPoint : null;
        this.deliveryPoint = props && props.deliveryPoint ? props.deliveryPoint : null;
        this.office = props && props.office ? props.office : null;
        this.transportTrip = props && props.transportTrip ? props.transportTrip : null;
        this.paid = props && props.paid ? props.paid : 0;
        this.totalFee = props && props.totalFee ? props.totalFee : 0;
        this.paymentType = props && props.paymentType ? props.paymentType : 1;
        this.cod = props && props.cod ? props.cod : 0;
        this.codFee = new ExtraFee(props && props.codFeeInfo ? props.codFeeInfo : null);
        this.useCod = props && props.cod ? true : false;
        this.payments = props && props.payments ? props.payments : [];
        this.note = props && props.note ? props.note : '';
        this.goodsType = new GoodsType(props && props.goodsType ? props.goodsType : null);
        this.type = props && props.type ? props.type : 0;
        this.sender = new UserOfGoods(props && props.sender ? props.sender : null);
        this.receiver = new UserOfGoods(props && props.receiver ? props.receiver : null);
        this.collectionType = new CollectionAnDeliveryType(props && props.collectionType ? props.collectionType : null);
        this.collection = new Collection(props && props.collection ? props.collection : null);
        this.delivery = new Collection(props && props.delivery ? props.delivery : null);
        this.deliveryType = new CollectionAnDeliveryType(props && props.deliveryType ? props.deliveryType : null);
        this.details = [];
        this.detailsOld = [];
        this.detailsResponse = props && props.details ? props.details : [];
        this.locations = props && props.locations ? props.locations : [];
        this.setDetails(props && props.details ? props.details : []);
        this.loadingGetGoodsFeesByIds = false;
        this.loadingGetExtraFeesByIds = false;
        this.debounceCodFee = null;
        this.log = new GoodsLog(props&&props.id?props.id : null);
        this.scanNumber = props&&props.scanNumber ? props.scanNumber : 0
        this.updatedDate = props&&props.updatedDate ? props.updatedDate : ''
        this.updatedTime = props&&props.updatedTime ? props.updatedTime : ''
        this.createdTime = props&&props.createdTime ? props.createdTime : ''
        
    }

    callDeleteGoods(row) {

    }

    totalGoodsFee() {
        let total = 0;
        $.each(this.details, function (d, detail) {
            total += parseInt(detail.totalFee());
        });
        return total;
    }

    getTotalFee() {
        this.totalFee = this.totalGoodsFee() + this.calculateCodFee();
        return this.totalFee;
    }

    setDetails(list) {
        let self = this;
        this.details = [];
        $.each(list, function (i, item) {
            self.details.push(item instanceof GoodsDetail ? item : new GoodsDetail(item));
        });
    }

    getDetails() {
        let details = [];
        $.each(this.details, function (d, detail) {
            details.push(detail.getDetail());
        });
        return details;
    }

    getDetailByResponse() {
        let self = this;
        let typeIds = {};
        let extraFeeIds = {};
        $.each(this.detailsResponse, function (d, detail) {
            if (detail.type && detail.type.id) {
                typeIds[detail.type.id] = d;
            }
            $.each(detail.extraFees, function (e, extraFee) {
                if (extraFee.id && !extraFeeIds[extraFee.id]) {
                    extraFeeIds[extraFee.id] = true;
                }
            });
        });
        let goodsFees = new GoodsFees();
        goodsFees.ids = Object.keys(typeIds);
        goodsFees.getList({
            before: function () {
                self.loadingGetGoodsFeesByIds = true;
            },
            success: function (data) {
                self.loadingGetGoodsFeesByIds = false;
            },
            error: function (error) {
                self.loadingGetGoodsFeesByIds = false;
            }
        });
        let extraFees = new ExtraFees();
        extraFees.ids = Object.keys(extraFeeIds);
        extraFees.getList({
            before: function () {
                self.loadingGetExtraFeesByIds = true;
            },
            success: function (data) {
                self.loadingGetExtraFeesByIds = false;
            },
            error: function (error) {
                self.loadingGetExtraFeesByIds = false;
            }
        });
        let itv = setInterval(function () {
            if (!self.loadingGetExtraFeesByIds && !self.loadingGetGoodsFeesByIds) {
                self.setDetailByResponse(goodsFees.list, extraFees.list);
                clearInterval(itv);
            }
        }, 200);
    }

    setDetailByResponse(listGoodsFees, listExtraFees) {
        let self = this;
        let details = [];
        $.each(self.detailsResponse, function (d, dt) {
            let detail = {
                name: dt.name,
                code: dt.code,
                value: dt.value,
                originalFee: dt.originalFee,
                quantity: dt.quantity,
                note: dt.note,
                appliedCriterion: dt.type && dt.type.priceInfo && dt.type.priceInfo.appliedCriterion ? dt.type.priceInfo.appliedCriterion : null,
                allowChangeCriteria: true,
                imageUrls: dt.imageUrls,
                extraFees: [],
                type: null,
                dimensions: dt.dimensions
            }
            $.each(listGoodsFees, function (t, type) {
                if (dt.type&&type.id === dt.type.id) {
                    detail.type = type;
                    return false;
                }
            });
            $.each(dt.extraFees, function (e, extraFee) {
                let efee = null;
                $.each(listExtraFees, function (index, data) {
                    if (data.id === extraFee.id) {
                        efee = data;
                        return false;
                    }
                });
                efee.appliedCriterion = extraFee.appliedCriterion;
                efee.allowChangeAppliedCirteria = !extraFee.usingExistedField;
                detail.extraFees.push(efee);
            });
            details.push(detail);
        });
        this.setDetails(details);
    }

    getReceivedPoint (){
        if(!this.receivedPoint){
            return null;
        }
        let self = this;
        return {
            id: self.receivedPoint.id,
            name: self.receivedPoint.name,
            province: self.receivedPoint.province,
            city: self.receivedPoint.city,
            district: self.receivedPoint.district,
            phoneNumber: self.receivedPoint.phoneNumber,
            address: self.receivedPoint.address
        }
    }

    getDeliveryPoint (){
        if(!this.deliveryPoint){
            return null;
        }
        let self = this;
        return {
            id: self.deliveryPoint.id,
            name: self.deliveryPoint.name,
            province: self.deliveryPoint.province,
            city: self.deliveryPoint.city,
            district: self.deliveryPoint.district,
            phoneNumber: self.deliveryPoint.phoneNumber,
            address: self.deliveryPoint.address
        }
    }

    saveCollection(options) {
        let self = this;
        let param = {
            transportType: self.transportType,
            goodsType: self.goodsType.value,
            collection: {
                address : self.collection.address,
                phoneNumber : self.collection.phoneNumber,
                date : self.collection.date,
                time : self.collection.getTimeMiliseconds(),
                courier : self.collection.getCourier(),
                regionInfo : self.collection.regionInfo,
                workShiftInfo : self.collection.workShiftInfo,
            },
            collectionType : 1,
            receivedPoint : self.getReceivedPoint(),
            deliveryPoint : self.getDeliveryPoint(),
            sender : self.sender,
            receiver : self.receiver,
            office : self.office,
            note : self.note,
        }
        if (self.id) {
            param.id = self.id;
        }
        if (self.id && options && options.remove) {
            param.active = false;
        }
        if (options && typeof options.before === 'function') {
            options.before();
        }
        window.sendJson({
            url: urlDBD(API.goodsV2Collection),
            data: param,
            success: function (data) {
                if (options && typeof options.success === 'function') {
                    options.success(data);
                }
            },
            functionIfError: function (err) {
                if (options && typeof options.error === 'function') {
                    options.error(err);
                }
            }
        })
    }

    setGoodsType(props) {
        this.goodsType.value = props;
        if (this.detailsOld && this.detailsOld.length > 0) {
            let swap = this.details;
            this.details = _.cloneDeep(this.detailsOld);
            this.detailsOld = swap;
        } else {
            this.detailsOld = _.cloneDeep(this.details);
            this.details = [];
            this.details.push(new GoodsDetail());
        }
    }

    setCod(props) {
        let self = this;
        this.cod = props;
        if (this.debounceCodFee) {
            clearTimeout(this.debounceCodFee);
        }
        this.debounceCodFee = setTimeout(function () {
            self.getCodFee();
            clearTimeout(this.debounceCodFee);
        }, 500)
    }

    getCodFee() {
        let self = this;
        let extraFees = new ExtraFees();
        extraFees.goodsType = GoodsType.enum().COD;
        extraFees.getList({
            before: function () {

            },
            success: function () {
                let fee = _.first(extraFees.list);
                if (fee) {
                    let criteriaList = fee.criteria;
                    let criteriaField = fee.criteriaField;
                    let cri = self.getAppliedCriteria(criteriaList, criteriaField);
                    fee.appliedCriterion = cri instanceof Criteria ? cri : (cri == null ? null : new Criteria(cri));
                    self.codFee = fee;
                }
            }
        })
    }

    getAppliedCriteria(criteriaList, criteriaField) {
        let self = this;
        let cri = null;
        if (self.criteriaFieldValue(criteriaField) !== undefined) {
            $.each(criteriaList, function (c, cr) {
                if (!cr.lowerThreshold && !cr.upperThreshold) {
                    cri = cr;
                    return false;
                }
                if (!cr.lowerThreshold && cr.upperThreshold && self.criteriaFieldValue(criteriaField) < cr.upperThreshold) {
                    cri = cr;
                    return false;
                }
                if (!cr.upperThreshold && cr.lowerThreshold && self.criteriaFieldValue(criteriaField) > cr.lowerThreshold) {
                    cri = cr;
                    return false;
                }
                if (!(parseFloat(cr.lowerThreshold) > self.criteriaFieldValue(criteriaField) ||
                    parseFloat(cr.upperThreshold) < self.criteriaFieldValue(criteriaField))) {
                    cri = cr;
                    return false;
                }
            });
        }
        return cri
    }

    calculateCodFee() {
        if (!this.useCod || !this.codFee || !this.codFee.id || !this.codFee.appliedCriterion || !this.codFee.appliedCriterion.id) {
            return 0;
        }
        let value = this.criteriaFieldValue('value');
        let weight = this.criteriaFieldValue('weight');
        let quantity = this.criteriaFieldValue('quantity');
        let totalFee = this.criteriaFieldValue('totalFee');
        let cod = this.criteriaFieldValue('cod');
        let formula = this.codFee.appliedCriterion.formula;
        let propertiesAutoField = AutoField.properties();
        $.each(propertiesAutoField, function (p, pro) {
            formula = formula.replaceAll(pro.code, pro.value);
        });
        return eval(formula);
    }

    criteriaFieldValue(field) {
        if (field === 'totalFee') {
            return this.totalGoodsFee();
        }
        if (this[field] !== undefined) {
            return this[field];
        }
        return undefined;
    }

    saveGoods(options,id) {
        let self = this;
        let param = {
            packageCode: self.packageCode,
            transportType: self.transportType,
            goodsType: self.goodsType.value,
            type: self.goodsType.value,
            collection: {
                address: self.collection.address,
                phoneNumber: self.collection.phoneNumber,
                date: self.collection.date,
                time: self.collection.getTimeMiliseconds(),
                courier: self.collection.getCourier(),
                regionInfo: self.collection.regionInfo,
                workShiftInfo: self.collection.workShiftInfo,
            },
            collectionType: self.collectionType.value,
            delivery: {
                address: self.delivery.address,
                phoneNumber: self.delivery.phoneNumber,
                date: self.delivery.date,
                time: self.delivery.getTimeMiliseconds(),
                courier: self.delivery.getCourier(),
                regionInfo: self.delivery.regionInfo,
                workShiftInfo: self.delivery.workShiftInfo,
            },
            deliveryType: self.deliveryType.value,
            receivedPoint: self.getReceivedPoint(),
            deliveryPoint: self.getDeliveryPoint(),
            sender: self.sender,
            receiver: self.receiver,
            office: self.office,
            note: self.note,
            details: self.getDetails(),
            paid: self.paid,
            cod: 0,
            paymentType: self.paymentType,
        }
        if (id){
            param.transportTrip = {
                id: id
            }
        }
        if (this.useCod) {
            param.cod = self.cod ? self.cod : 0;
            if(self.codFee.id){
                param.codFeeInfo = self.codFee.getExtraFee();
            }
        }
        if (self.id) {
            param.id = self.id;
        }
        if (self.code) {
            param.code = self.code;
        }
        if (self.id && options && options.remove) {
            param.active = false;
        }
        if (options && typeof options.before === 'function') {
            options.before();
        }
        window.sendJson({
            url: urlDBD(self.id ? API.goodsV2Update : API.goodsV2Create),
            data: param,
            success: function (data) {
                if (options && typeof options.success === 'function') {
                    options.success(data);
                }
            },
            functionIfError: function (err) {
                if (options && typeof options.error === 'function') {
                    options.error(err);
                }
            }
        })
    }
}

export class GoodsCollectionList {
    constructor() {
        this.page = 0;
        this.count = 30;
        this.loadMore = false;
        this.loading = false;
        this.date = new Date().customFormat('#YYYY##MM##DD#');
        this.code = '';
        this.region = null;
        this.courier = null;
        this.workShift = null;
        this.goodsStatus = null;
        this.list = [];
    }

    setFilter(value, field) {
        this[field] = value;
        return this.getList();
    }

    getList(options) { // number page : tăng hoặc giảm page
        let self = this;
        if (options && !isNaN(options.numberPage)) {
            self.page += parseInt(options.numberPage);
        } else {
            self.page = 0;
        }

        let params = {
            page: self.page,
            count: self.count,
            date: self.date,
            code: self.code,
        };
        // if (self.keyword) {
        //     params.numberPlate = self.keyword;
        // }
        if (self.goodsType !== null && self.goodsType !== null) {
            params.goodsType = self.goodsType;
        }
        if(self.courier){
            params.courierId = self.courier.id;
        }
        if(self.region){
            params.regionId = self.region.id;
        }
        if (self.goodsStatus !== null && self.goodsStatus !== undefined) {
            params.goodsStatus = self.goodsStatus;
        }
        self.loading = true;
        self.loadMore = false;

        return window.sendParam({
            url: window.urlDBD(window.API.goodsV2Collection),
            data: params,
            type: "get",
            success: function (data) {
                self.loadMore = true;
                if (data.results.collectionRequests.length < self.count) {
                    self.loadMore = false;
                }
                self.loading = false;
                if (self.page <= 0) {
                    self.setList(data.results.collectionRequests);
                } else {
                    self.setList(data.results.collectionRequests, {push: true});
                }
            },
            functionIfError: function (data) {
                self.loading = false;
            }
        });
    }

    setList(list, options) {
        let self = this;
        if (!(options && options.push)) {
            self.list = [];
        }
        $.each(list, function (i, item) {
            self.list.push(item instanceof Goods ? item : new Goods(item));
        });
    }
}

var date = new Date();
//danh sach hang
export class GoodsList {
    constructor() {
        this.page = 0;
        this.count = 100;
        this.loadMore = false;
        this.loading = false;
        this.date = [new Date( new Date().getTime() - 7 * 60 * 60 * 24 * 1000  ).customFormat('#YYYY##MM##DD#'), new Date().customFormat('#YYYY##MM##DD#')];
        this.code = '';
        this.phoneNumber = '';
        this.region = null;
        this.courier = null;
        this.workShift = null;
        this.goodsStatus = [];
        this.receivedPoint = null;
        this.deliveryPoint = null;
        this.list = [];
        this.list2 = [];
        this.officeSelected = null;
        this.receiverSelected = null;
        this.route = null;
        this.trip = null
    }

    getListGoodsInTrip (trip, listPoint) {
        let self = this;
        self.loading = true;
        let data = {
            tripId: trip.tripId,
            date: [trip.date]
        }
        if (trip.tripId === "-1") {
            notify(self.$t('notify.this-trip-has-no-tickets-yet-has-not-been-created-yet'), "error");
            return false;
        }
        sendParam({
            url: urlDBD(API.goodsV2List),
            data: data,
            type: 'get',
            success: function (data) {
                self.setListBeforeGet(data.results.goods, listPoint);
                self.loading = false;
            },
            functionIfError: function (res) {
                self.loading = false;
            }
        })
    }

    setListBeforeGet (data, listPoint){
        let self = this;
        self.list = []
        self.list2 = []
        $.each(listPoint,function (v,item) {
            self.list[v] = []
            $.each(data,function (v2,item2) {
                if (item2.deliveryPoint&&item2.deliveryPoint.address == item.address) {
                    self.list[v].push(new Goods(item2));
                }
            })
            if (self.list[v].length == 0) {
                self.list[v] = null
            }
        });
        let a = data
        $.each(listPoint,function (v,item) {
            let b = []
            $.each(a,function (v2,item2) {
                if (item2.deliveryPoint&&item2.deliveryPoint.address !== item.address || item2.deliveryPoint == null) {
                    b.push(new Goods(item2));
                }
            })
            a = b
        });
        self.list2 = a
    }

    setFilter(value, field) {
        this[field] = value;
        return this.getList();
    }

    getList(options) { // number page : tăng hoặc giảm page
        let self = this;
        if (options && !isNaN(options.numberPage)) {
            self.page += parseInt(options.numberPage);
            if(!self.loadMore){
                return false;
            }
        } else {
            self.page = 0;
        }

        let params = {
            page: self.page,
            count: self.count,
            date: self.date,
            status: self.goodsStatus,
        };
        if (self.code) {
            params.code = self.code;
        }

        if (self.officeSelected){
            params.currentOfficeId = self.officeSelected.id
        }

        if (self.receiverSelected) {
            params.collectCourierId = self.receiverSelected.id
        }

        if (self.route) {
            params.routeId = self.route.routeId
        }

        if (self.trip) {
            params.tripId = self.trip.tripId
        }

        if (self.phoneNumber) {
            params.phoneNumber = self.phoneNumber;
        }

        if (self.goodsType !== null && self.goodsType !== null) {
            params.goodsType = self.goodsType;
        }
        if (self.courier) {
            params.courierId = self.courier.id;
        }
        if (self.region) {
            params.regionId = self.region.id;
        }

        let defaultStatus = [0, 1, 2, 3, 4, 5, 6, 7];

        if (!self.goodsStatus || self.goodsStatus.length == 0) {
            params.status = defaultStatus;
        }

        let goodsStatusIsArray = self.goodsStatus instanceof Array;
        if( !goodsStatusIsArray ) {
            params.status = (self.goodsStatus == "") ? defaultStatus : [self.goodsStatus];
        }
        // ..............................................
        if(options) {
            if (options.code) {
                params.code = options.code;
            }

            if (options.officeSelected){
                params.currentOfficeId = options.officeSelected.id
            }

            if (options.receiverSelected) {
                params.collectCourierId = options.receiverSelected.id
            }

            if (options.route) {
                params.routeId = options.route.routeId
            }

            if (options.trip) {
                params.tripId = options.trip.tripId
            }
            if (options.date) {
                params.date = options.date
            }
            if (options.phoneNumber) {
                params.phoneNumber = options.phoneNumber;
            }
            if (options.goodsType !== null && options.goodsType !== null) {
                params.goodsType = options.goodsType;
            }
            if (options.courier) {
                params.courierId = options.courier.id;
            }
            if (options.region) {
                params.regionId = options.region.id;
            }
            if (options.goodsStatus === undefined || options.goodsStatus === null || options.goodsStatus.length == 0) {
                params.status = defaultStatus;
            }
            if( !(options.goodsStatus instanceof Array) ) {
                params.status = (options.goodsStatus === "") ? defaultStatus : [options.goodsStatus];
            }
        }

        self.loading = true;
        self.loadMore = false;
        return window.sendParam({
            url: window.urlDBD(window.API.goodsV2List),
            data: params,
            type: "get",
            success: function (data) {
                self.loadMore = true;
                if (data.results.goods.length < self.count) {
                    self.loadMore = false;
                }

                let res = data.results.goods;
                
                res = res.sort((value1, value2) => {
                    return value2.createdTime - value1.createdTime
                })

                self.loading = false;
                if (self.page <= 0) {
                    self.setList(res);
                } else {
                    self.setList(res, {push: true});
                }

                if(options&&typeof options.success === 'function'){
                    options.success(res);
                }
            },
            functionIfError: function (data) {
                self.loading = false;
            }
        });
    }

    setList(list, options) {
        let self = this;
        if (!(options && options.push)) {
            self.list = [];
        }
        $.each(list, function (i, item) {
            self.list.push(item instanceof Goods ? item : new Goods(item));
        });
    }
}

export class GoodsLog{
    constructor(id) {
        this.loading = false;
        this.logs = [];
        this.id = id;
    }
    getLogs (){
        let self = this;
        if(!self.id){
            this.logs = [];
            return false;
        }
        self.loading = true;
        window.sendParam({
            url: window.urlDBD(window.API.goodsV2Log),
            data: {id : self.id},
            type: "get",
            success: function (data) {
                self.logs = data.results.logs.reverse();
                self.loading = false;
            },
            functionIfError: function (data) {
                self.loading = false;
            }
        });
    }
}

