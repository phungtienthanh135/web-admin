export class RouteShort {
    constructor(prop){
        this.id = prop&&prop.id?prop.id:null;
        this.name = prop&&prop.name?prop.name:'';
        this.shortName = prop&&prop.shortName?prop.shortName:'';
    }
}

export class Routes {
    constructor (props){
        this.keyword = props&&props.keyword ? props.keyword : '';
        this.page = props&&props.page ? props.page : 0;
        this.count = props&&props.count ? props.count : 30;
        this.list = [];
        this.loading = false;
        this.loadMore = true;
    }

    getList (numberPage,options){ // number page : tăng hoặc giảm page
        let self = this;
        if(isNaN(numberPage)){
            self.page=0;
        }else{
            self.page+= parseInt(numberPage);
        }

        let params = {
            page : self.page,
            count : self.count,
            companyId : userInfo.userInfo.companyId
        };
        if(self.keyword){
            params.routeName = self.keyword;
        }
        if(options && typeof options.beforeSend === 'function'){
            options.beforeSend();
        }
        self.loading = true;
        self.loadMore = false;
        window.sendJson({
            url : window.urlDBD(window.API.routeList),
            data : params,
            success : function (data) {
                self.loadMore = true;
                if(data.results.result.length<self.count){
                    self.loadMore = false;
                }
                self.loading = false;
                if(options && typeof options.sendSuccess === 'function'){
                    options.sendSuccess(data);
                }
                self.setListBeforeGet(data.results.result);
            },
            functionIfError : function (data) {
                self.loading = false;
                if(options && typeof options.sendError === 'function'){
                    options.sendError(data);
                }
            }
        });
    }

    setListBeforeGet (props){
        let self = this;
        if(this.page<=0){
            this.list = [];
        }
        $.each(props,function (p,prop) {
            self.list.push(prop);
        });
    }
}