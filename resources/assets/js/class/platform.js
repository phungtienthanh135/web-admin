import lang from '../Lang/index';

export class Platform {
    constructor(prop){
        this.value = Number.isInteger(prop)&&prop>0&&prop<6 ? prop : 1 ;
    }

    label(){
        return Platform.properties()[this.value].label;
    }

    static properties (){
        return {
            1 : {
                label : lang.t('website-management'),
                value : 1
            },
            2 : {
                label : lang.t('online'),
                value : 2
            },
            3 : {
                label : 'IOS',
                value : 3
            },
            4 : {
                label : 'Android',
                value : 4
            },
            5 : {
                label : lang.t('menu.agency'),
                value : 5
            }
        };
    }
    static ENUM (){
        return {
            ADMIN : 1,
            ONLINE : 2,
            IOS : 3,
            ANDROID : 4,
            AGENCY : 5
        };
    }
}
export class ListPlatform {
    constructor(listPl){
        let list = [];
        $.each(listPl?listPl:[],function (key,value) {
            list.push(new Platform(value));
        });
        this.list = list;
    }
    toStringLabel(){
        let arrName = [];
        $.each(this.list,function (key,value) {
            arrName.push(value.label);
        });
        return arrName.join(', ');
    }
}