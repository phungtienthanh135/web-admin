import _ from 'lodash';

export class RegionDepartment {
    constructor(props){
        let value = parseInt(props?props : 0);
        this.value = value>=0&&value<3?value:0;
    }
    static properties(){
        return {
                0: {value: 0, label: "Giao vé"},
                1: {value: 1, label: "Trung chuyển"},
                2: {value: 2, label: "Giao hàng"},
        };
    }

    static ENUM() {
        return {
            TICKET_TRANSPORT: 0,
            TRANSSHIPMENT: 1,
            GOODS_TRANSPORT: 2,
        };
    }
}

export class Region {
    constructor(props){
        this.id = props&&props.id ? props.id : null;
        this.name = props&&props.name ? props.name : '';
        this.province = props&&props.province ? props.province : '';
        this.department = props&&props.department ? props.department : 0;
        this.addresses = props&&props.addresses ? props.addresses : [];
        this.createdTime = props&&props.createdTime ? props.createdTime : null;
    }

    removeAddress (index){
        this.addresses = _.pullAt(this.addresses, [index]);
    }

    pushAddress (){
        this.addresses.push('');
    }
}