import {UserShort} from "./user";

export class PointTypeOfTicket {
    constructor (prop){
        let type = isNaN(prop)?0:parseInt(prop)
        this.value = type<0||type>3?0:type;
    }

    static properties (){
        return {
            0 : {
                value : 0,
                label : 'Tại bến'
            },
            1 : {
                value : 1,
                label : 'Tận nhà'
            },
            2 : {
                value : 2,
                label : 'Dọc đường'
            },
            3 : {
                value : 3,
                label : 'Trung chuyển'
            }
        };
    }
    static ENUM (){
        return {
            POINT : 0,
            HOME : 1,
            AWAY : 2,
            TRANSSHIPMENT : 3
        };
    }
}

export class PointOfTicket {
    constructor(prop){
        this.type = new PointOfTicket(prop&&prop.pointType ? prop.pointType :null);
        this.id = prop&&prop.id ? prop.id :null;
        this.name = prop&&prop.name ? prop.name :'';
        this.address = prop&&prop.address ? prop.address :'';
        this.lat = prop&&prop.latitude ? prop.latitude :null;
        this.lng = prop&&prop.longitude ? prop.longitude :null;
        this.transshipmentId = prop&&prop.transshipmentId ? prop.transshipmentId :null;
        this.transshipmentDriver = new UserShort(prop&&prop.transshipmentDriver?prop.transshipmentDriver:null);
    }
}