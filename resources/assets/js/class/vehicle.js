export class Vehicle {
    constructor(props) {
        this.id = props && props.vehicleId ?props.vehicleId : null;
        this.name = props && props.vehicleName ?props.vehicleName : '';
        this.numberPlate = props && props.numberPlate ?props.numberPlate : '';
        this.seatMap = props && props.seatMap ?props.seatMap : null;
        this.phoneNumber = props && props.phoneNumber ?props.phoneNumber.split(',') : [];
        this.chasisNumber = props && props.chasisNumber ? props.chasisNumber : ''
        this.companyId = props && props.companyId ? props.companyId : ''
        this.createdDate = props && props.createdDate ? props.createdDate : ''
        this.customField = props && props.customField ? props.customField : ''
        this.description = props && props.description ? props.description : ''
        this.engineNumber = props && props.engineNumber ? props.engineNumber : ''
        this.estimatedFuelConsumption = props && props.estimatedFuelConsumption ? props.estimatedFuelConsumption : ''
        this.gpsId = props && props.gpsId ? props.gpsId : ''
        this.insuranceEndTime = props && props.insuranceEndTime ? props.insuranceEndTime : ''
        this.insuranceName = props && props.insuranceName ? props.insuranceName : ''
        this.insurancePhone = props && props.insurancePhone ? props.insurancePhone : ''
        this.isActive = props && props.isActive ? props.isActive : ''
        this.latitude = props && props.latitude ? props.latitude : ''
        this.listImage = props && props.listImage ? props.listImage : ''
        this.listUtilityVehicleIds = props && props.listUtilityVehicleIds ? props.listUtilityVehicleIds : ''
        this.listUtilityVehicles = props && props.listUtilityVehicles ? props.listUtilityVehicles : ''
        this.longitude = props && props.longitude ? props.longitude : ''
        this.numberOfSeats = props && props.numberOfSeats ? props.numberOfSeats : ''
        this.numberPlate = props && props.numberPlate ? props.numberPlate : ''
        this.origin = props && props.origin ? props.origin : ''
        this.originPrice = props && props.originPrice ? props.originPrice : ''
        this.productedYear = props && props.productedYear ? props.productedYear : ''
        this.reduceTime = props && props.reduceTime ? props.reduceTime : ''
        this.registrationDeadline = props && props.registrationDeadline ? props.registrationDeadline : ''
        this.remain = props && props.remain ? props.remain : ''
        this.seatMap = props && props.seatMap ? props.seatMap : ''
        this.seatMapId = props && props.seatMapId ? props.seatMapId : ''
        this.seatMapType = props && props.seatMapType ? props.seatMapType : ''
        this.startDayUsing = props && props.startDayUsing ? props.startDayUsing : ''
        this.totalReducetion = props && props.totalReducetion ? props.totalReducetion : ''
        this.userId = props && props.userId ? props.userId : ''
        this.vehicleId = props && props.vehicleId ? props.vehicleId : ''
        this.vehicleName = props && props.vehicleName ? props.vehicleName : ''
        this.vehicleOwnerId = props && props.vehicleOwnerId ? props.vehicleOwnerId : ''
        this.vehicleStatus = props && props.vehicleStatus ? props.vehicleStatus : ''
        this.vehicleType = props && props.vehicleType ? props.vehicleType : ''
        this.vehicleTypeName = props && props.vehicleTypeName ? props.vehicleTypeName : ''
    }
}

export class Vehicles {
    constructor (props){
        this.keyword = props&&props.keyword ? props.keyword : '';
        this.page = props&&props.page ? props.page : 0;
        this.count = props&&props.count ? props.count : 30;
        this.list = [];
        this.loading = false;
        this.loadMore = true;
    }

    getList (numberPage){ // number page : tăng hoặc giảm page
        let self = this;
        if(isNaN(numberPage)){
            self.page=0;
        }else{
            self.page+= parseInt(numberPage);
        }

        let params = {
            page : self.page,
            count : self.count
        };
        if(self.keyword){
            params.numberPlate = self.keyword;
        }
        self.loading = true;
        self.loadMore = false;
        window.sendJson({
            url : window.urlDBD(window.API.vehicleList),
            data : params,
            success : function (data) {
                self.loadMore = true;
                if(data.results.vehicles.length<self.count){
                    self.loadMore = false;
                }
                self.loading = false;
                self.setListBeforeGet(data.results.vehicles);
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }

    setListBeforeGet (vehicles){
        let self = this;
        if(this.page<=0){
            this.list = [];
        }
        $.each(vehicles,function (v,vehicle) {
            self.list.push(new Vehicle(vehicle));
        });
    }
}