export class PaymentMethods {
    constructor(prop){
        this.value = !isNaN(prop)&&parseInt(prop)>0&&parseInt(prop)<4 ? parseInt(prop) : 2 ;
    }

    label(){
        return this.value?PaymentMethods.properties()[this.value].label:'';
    }
    set (prop){
        this.value = !isNaN(prop)&&parseInt(prop)>0&&parseInt(prop)<4 ? parseInt(prop) : 2;
    }
    static properties(){
        return {
            // 1:{label : 'Thẻ VISA/Master Card',value : 1},
            2:{label : 'Thẻ ATM Nội địa',value : 2},
            // 3:{label : 'Ví điện tử',value : 3}
        };
    }
    static ENUM(){
        return {
            INTERNATIONAL_CARD : 1,//thẻ quốc tế
            LOCAL_CARD : 2, //thẻ nội địa
            WALLET : 3 // ví điện tử
        };
    }
}

export class LocalCard {
    constructor (props){
        this.bankingName = props&&props.bankingName?props.bankingName:''; // tên ngân hàng
        this.bankingNumber = props&&props.bankingNumber?props.bankingNumber:'';//số thẻ
        this.cardHolder = props&&props.cardHolder?props.cardHolder:'';//chủ thẻ
    }
}
export class InternationalCard {
    constructor (props){
        this.bankingName = props&&props.bankingName?props.bankingName:''; // tên ngân hàng
        this.bankingNumber = props&&props.bankingNumber?props.bankingNumber:'';//số thẻ
        this.cardHolder = props&&props.cardHolder?props.cardHolder:'';//chủ thẻ
    }
}
export class Wallet {
    constructor (props){
        this.bankingName = props&&props.bankingName?props.bankingName:''; // tên ngân hàng
        this.bankingNumber = props&&props.bankingNumber?props.bankingNumber:'';//số thẻ
        this.cardHolder = props&&props.cardHolder?props.cardHolder:'';//chủ thẻ
    }
}

export class Banking {
    constructor(props){
        this.method = new PaymentMethods(props&&props.method ? props.method:null);
        switch (this.method.value) {
            case PaymentMethods.ENUM().INTERNATIONAL_CARD :
                this.infomation = new InternationalCard(props&&props.infomation?props.infomation:null);
                break;
            case PaymentMethods.ENUM().LOCAL_CARD :
                this.infomation = new LocalCard(props&&props.infomation?props.infomation:null);
                break;
            case PaymentMethods.ENUM().WALLET :
                this.infomation = new Wallet(props&&props.infomation?props.infomation:null);
                break;
        }
    }
}