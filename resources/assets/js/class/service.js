
export class Service {
    constructor(props) {
        this.listImage = props&&props.listImage ? props.listImage : [];
        this.companyId = props&&props.companyId ? props.companyId : '';
        this.id = props&&props.id ? props.id : '';
        this.sourceCoupon = props&&props.sourceCoupon ? props.sourceCoupon : {};
        this.unit = props&&props.unit ? props.unit : '';
        this.price = props&&props.price ? props.price : '';
        this.name = props&&props.name ? props.name : '';
        this.description = props&&props.description ? props.description : '';
        this.lowerCaseName = props&&props.lowerCaseName ? props.lowerCaseName : '';
        this.loading = false;
    }


    sendDeleteService (service,call) {
        let self = this;
        self.loading = true;
        sendJson({
            url : window.urlDBD(window.API.deleteService),
            type: 'POST',
            data : {
                name: service.name,
                id:  service.id,
                status: 0
            },
            success : function (data) {
                self.loading = false;
                call();
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }

    sendEditService (call){
        let self = this;
        self.loading = true;
        let couponList = [];
        $.each(self.sourceCoupon, function (c, coupon) {
            let propertyValues = Object.values(coupon);
            couponList.push(propertyValues);
        })
        const entries = new Map(couponList);

        const obj = Object.fromEntries(entries);
        let data = {
            id: self.id,
            unit: self.unit,
            price: self.price,
            name: self.name,
            description: self.description,
            // lowerCaseName: self.name.toLowerCase(),
            listImage: self.listImage,
            sourceCoupon: obj
        };
        sendJson({
            url : window.urlDBD(window.API.updateService),
            type: 'post',
            data : data,
            success : function (data) {
                self.loading = false;
                call();
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }

    sendAddService (call){
        let self = this;
        self.loading = true;
        let couponList = [];
        $.each(self.sourceCoupon, function (c, coupon) {
            let propertyValues = Object.values(coupon);
            couponList.push(propertyValues);
        })
        const entries = new Map(couponList);

        const obj = Object.fromEntries(entries);

        let data = {
            unit: self.unit,
            price: self.price,
            name: self.name,
            description: self.description,
            // lowerCaseName: self.name.toLowerCase(),
            listImage: self.listImage,
            sourceCoupon: obj
        };
        sendJson({
            url : window.urlDBD(window.API.createService),
            type: 'post',
            data : data,
            success : function (data) {
                self.loading = false;
                call();
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }
}

export class ListService {
    constructor (props){
        this.page = props&&props.page ? props.page : 0;
        this.count = props&&props.count ? props.count : 0;
        this.list = [];
        this.loading = false;
    }

    getList (){
        let self = this;
        self.loading = true;
        let params = {
            page: self.page,
            count: self.count,
            companyId: window.userInfo.userInfo.companyId
        };
        sendJson({
            url : window.urlDBD(window.API.getListService),
            type: 'post',
            data : params,
            success : function (data) {
                self.loading = false;
                self.setListBeforeGet(data.results.services);
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }

    setListBeforeGet (data){
        let self = this;
        self.list = [];
        $.each(data,function (v,item) {
            self.list.push(new Service(item));
        });
    }
}

export class OrderService {
    constructor(props) {
        this.serviceOrderCode = props&&props.serviceOrderCode ? props.serviceOrderCode : '#'+ new Date().getTime().toString().slice(0,9);
        this.serviceId = props&&props.serviceId ? props.serviceId : '';
        this.id = props&&props.id ? props.id : '';
        this.totalPrice = props&&props.totalPrice ? props.totalPrice : 0;
        this.serviceAmount = props&&props.serviceAmount ? props.serviceAmount : '';
        this.buyerName = props&&props.buyerName ? props.buyerName : '';
        this.buyerPhone = props&&props.buyerPhone ? props.buyerPhone : '';
        this.createdDate = props&&props.createdDate ? props.createdDate : '';
        this.notice = props&&props.notice ? props.notice : '';
        this.service = props&&props.service ? props.service : {};
        this.emailInfo = props&&props.emailInfo ? props.emailInfo : {};
        this.serviceUsingTime = props&&props.serviceUsingTime ? props.serviceUsingTime : new Date().getTime();
        this.loading = false;
    }


    sendDeleteOrderService (order,call) {
        let self = this;
        self.loading = true;
        sendJson({
            url : window.urlDBD(window.API.deleteOrderService),
            type: 'POST',
            data : {
                id:  order.id
            },
            success : function (data) {
                self.loading = false;
                call();
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }

    sendEditOrderService (call){
        let self = this;
        self.loading = true;
        let data = {
            id: self.id,
            serviceOrderCode: self.serviceOrderCode,
            serviceId: self.serviceId,
            totalPrice: self.totalPrice,
            serviceAmount: self.serviceAmount,
            buyerName: self.buyerName,
            buyerPhone: self.buyerPhone,
            notice: self.notice,
            serviceUsingTime: self.serviceUsingTime,
            emailInfo:{
                isSendEmail: true,
                customerEmail:self.emailInfo.customerEmail,
                subject :'Thông báo đặt thành công dịch vụ',
                format:'text/html',
                from:'july140791@gmail.com',
                content:'Quý khách đã đặt dịch vụ thành công'
            }
        };
        sendJson({
            url : window.urlDBD(window.API.updateOrderService),
            type: 'post',
            data : data,
            success : function (data) {
                self.loading = false;
                call();
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }

    sendAddOrderService (call){
        let self = this;
        self.loading = true;
        let data = {
            serviceOrderCode: self.serviceOrderCode,
            serviceId: self.serviceId,
            totalPrice: self.totalPrice,
            serviceAmount: self.serviceAmount,
            buyerName: self.buyerName,
            buyerPhone: self.buyerPhone,
            notice: self.notice,
            serviceUsingTime: self.serviceUsingTime,
            emailInfo:{
                isSendEmail: true,
                customerEmail:self.emailInfo.customerEmail,
                subject :'Thông báo đặt thành công dịch vụ',
                format:'text/html',
                from:'kythuat@anvui.vn',
                content:'Quý khách ' + self.buyerName +
                    ' đã đặt dịch vụ thành công'
            }
        };
        sendJson({
            url : window.urlDBD(window.API.createOrderService),
            type: 'post',
            data : data,
            success : function (data) {
                self.loading = false;
                call();
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }
}

export class ListOrderService {
    constructor (props){
        this.page = props&&props.page ? props.page : 0;
        this.count = props&&props.count ? props.count : 0;
        this.list = [];
        this.loading = false;
    }

    getList (){
        let self = this;
        self.loading = true;
        let params = {
            page: self.page,
            count: self.count,
            companyId: window.userInfo.userInfo.companyId
        };
        sendJson({
            url : window.urlDBD(window.API.getListOrderService),
            type: 'post',
            data : params,
            success : function (data) {
                self.loading = false;
                let list = []
                $.each(data.results.services, function (s, se) {
                    if(se.status !== 0){
                        list.push(se);
                    }
                })
                self.setListBeforeGet(list);
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }

    setListBeforeGet (data){
        let self = this;
        self.list = [];
        $.each(data,function (v,item) {
            self.list.push(new OrderService(item));
        });
    }
}

