
export class ListTransfer {
    constructor(props) {
        this.keyword = props&&props.keyword ? props.keyword : '';
        this.route = props&&props.route ? props.route : null;
        this.ticketCode = props&&props.ticketCode ? props.ticketCode : '';
        this.getInTimePlan = props&&props.getInTimePlan ? props.getInTimePlan : '';
        this.createdDate = props&&props.createdDate ? props.createdDate : '';
        this.getInTimePlanInt = props&&props.getInTimePlanInt ? props.getInTimePlanInt : '';
        this.fullName = props&&props.fullName ? props.fullName : '';
        this.phoneNumber = props&&props.phoneNumber ? props.phoneNumber : '';
        this.routeInfo = props&&props.routeInfo ? props.routeInfo : '';
        this.transferStatus = props&&props.transferStatus ? props.transferStatus : '';
        this.note = props&&props.note ? props.note : '';
        this.collectionTransactions = props&&props.collectionTransactions ? props.collectionTransactions : null;
        this.seller = props&&props.seller ? props.seller : '';
        this.pointUp = props&&props.pointUp ? props.pointUp : '';
        this.pointDown = props&&props.pointDown ? props.pointDown : '';
        this.tripId = props&&props.tripId ? props.tripId : '';
        this.seat = props&&props.seat ? props.seat : '';
        this.ticketId = props&&props.ticketId ? props.ticketId : '';
        this.agencyPrice = props&&props.agencyPrice ? props.agencyPrice : '';
        this.page = props&&props.page ? props.page : 0;
        this.count = props&&props.count ? props.count : 30;
        this.list = [];
        this.loading = false;
        this.loadMore = true;
    }

    confirm(data,call){
        let self = this;
        self.loading = true
        let params = [{
                        transferStatus : 1,
                        ticketId : data.ticketId,
                        ticketStatus: 3,
                        paidMoney : data.agencyPrice
                    }];
        let url = window.API.ticketUpdate;

        window.sendJson({
            url : window.urlDBD(url),
            data : params,
            success :function (result) {
                if(typeof(call) == 'function'){
                    call();
                }
                self.loading = false;
            },
            functionIfError : function (err) {
                self.loading= false;
                if(data&&typeof data.error === 'function'){
                    data.error(err);
                }
            }
        });
    }
}
var date = new Date();
export class ListTransfers {
    constructor (props){
        this.keyword = props&&props.keyword ? props.keyword : '';
        this.route = props&&props.route ? props.route : null;
        /*this.startDate = props&&props.startDate ? props.startDate : '';
        this.endDate = props&&props.endDate ? props.endDate : '';*/
        this.rangeDate = [new Date(date.getFullYear(), date.getMonth(), 1).customFormat('#YYYY##MM##DD#'), new Date().customFormat('#YYYY##MM##DD#')];
        this.phoneNumber = props&&props.phoneNumber ? props.phoneNumber : '';
        this.transferStatus = props&&props.transferStatus ? props.transferStatus : 0;
        this.ticketCode = props&&props.ticketCode ? props.ticketCode : '';
        this.page = props&&props.page ? props.page : 0;
        this.count = props&&props.count ? props.count : 30;
        this.list = [];
        this.loading = false;
    }

    getListTransfers (){
        let self = this;
        self.loading = true;
        let params = {
        };
        if(!self.rangeDate){
            params.startDate = new Date(date.getFullYear(), date.getMonth(), 1).customFormat('#YYYY##MM##DD#');
            params.endDate = new Date().customFormat('#YYYY##MM##DD#');
        }
        if(self.rangeDate){
            params.startDate = self.rangeDate[0];
            params.endDate = self.rangeDate[1];
        }
        if(self.route){
            params.routeIds = [];
            /*self.route.forEach(( elem ) => {
                params.routeIds.push(elem.routeId);
            });*/
            $.each(self.route,function (v,item) {
                params.routeIds.push(item.routeId);
            });
        }
        if(self.transferStatus){
            params.transferStatus = self.transferStatus;
        }
        if(self.phoneNumber){
            params.phoneNumber = self.phoneNumber;
        }
        if(self.ticketCode){
            params.ticketCode = self.ticketCode;
        }
        sendParam({
            url : window.urlDBD(window.API.getTicketTransferConfirm),
            type: 'get',
            data : params,
            success : function (data) {
                self.loading = false;
                self.setListBeforeGet(data.results.listTicket);
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
        }

    setListBeforeGet (data){
        let self = this;
        if(this.page<=0){
            this.list = [];
        }
        $.each(data,function (v,item) {
            self.list.push(new ListTransfer(item));
        });
    }
}

