
export class Permission {
    constructor(props) {
        this.agencies = props&&props.agencies ? props.agencies : null;
        this.staffs = props&&props.staffs ? props.staffs : null;
        this.rooms = props&&props.rooms ? props.rooms : null;
        this.routeInfo = props&&props.routeInfo ? props.routeInfo : null;
        this.id = props&&props.id ? props.id : '';
        this.levelsOfAgency = props&&props.levelsOfAgency ? props.levelsOfAgency : '';
        this.loading = false;
    }


    sendDeletePermission (permission,call) {
        let self = this;
        self.loading = true;
        sendJson({
            url : window.urlDBD(window.API.deletePermissionSellTicket),
            type: 'POST',
            data : {
              id:  permission.id
            },
            success : function (data) {
                self.loading = false;
                call();
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }

    sendEditPermission (call){
        let self = this;
        self.loading = true;
        let data = [];
        $.each(self.routeInfo,function (t,route) {
            let a = {
                routeInfo : {
                    id : route.routeId
                },
                levelsOfAgency: [],
                agencies: [],
                staffs : [],
                rooms : []
            }
            if (self.agencies) {
                $.each(self.agencies,function (t,agencie) {
                    a.agencies.push({id : agencie.id});
                });
            }
            if (self.levelsOfAgency) {
                $.each(self.levelsOfAgency,function (t,level) {
                    a.levelsOfAgency.push({id : level.levelAgencyId});
                });
            }
            if (self.staffs) {
                $.each(self.staffs,function (t,staff) {
                    a.staffs.push({id : staff.id});
                });
            }
            if (self.rooms) {
                $.each(self.rooms,function (t,room) {
                    a.rooms.push({id : room.id});
                });
            }
            data.push(a);
        })

        sendJson({
            url : window.urlDBD(window.API.createPermissionSellTicket),
            type: 'post',
            data : data,
            success : function (data) {
                self.loading = false;
                call();
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }

    sendAddPermission (call){
        let self = this;
        self.loading = true;
        let data = [];
        $.each(self.routeInfo,function (t,route) {
            let a = {
                routeInfo : {
                    id : route.routeId
                },
                levelsOfAgency: [],
                agencies: [],
                staffs : [],
                rooms : []
            }
            if (self.agencies) {
                $.each(self.agencies,function (t,agencie) {
                    a.agencies.push({id : agencie.id});
                });
            }
            if (self.levelsOfAgency) {
                $.each(self.levelsOfAgency,function (t,level) {
                    a.levelsOfAgency.push({id : level.levelAgencyId});
                });
            }
            if (self.staffs) {
                $.each(self.staffs,function (t,staff) {
                    a.staffs.push({id : staff.id});
                });
            }
            if (self.rooms) {
                $.each(self.rooms,function (t,room) {
                    a.rooms.push({id : room.id});
                });
            }
            data.push(a);
        })

        sendJson({
            url : window.urlDBD(window.API.createPermissionSellTicket),
            type: 'post',
            data : data,
            success : function (data) {
                self.loading = false;
                call();
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }
}

export class ListPermission {
    constructor (props){
        this.page = props&&props.page ? props.page : 0;
        this.count = props&&props.count ? props.count : 30;
        this.list = [];
        this.loading = false;
    }

    getList (options){
        let self = this;
        self.loading = true;
        let params = {};
        sendParam({
            url : window.urlDBD(window.API.getListPermissionSellTicket),
            type: 'get',
            data : params,
            success : function (data) {
                self.loading = false;
                self.setListBeforeGet(data.results.userByRoutes);
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }

    setListBeforeGet (data){
        let self = this;
        self.list = [];
        $.each(data,function (v,item) {
            self.list.push(new Permission(item));
        });
    }
}

