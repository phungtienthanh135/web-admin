function countEmptySeat(data, startPoint, endPoint) {
    var mask = createMask(startPoint, endPoint - 1)
    // console.log('mask: ' + mask);

    return data.filter(dat => checkEmptySeat(dat, mask)).length;
}

function checkEmptySeat(data, mask) {

    var empty = true;
    // console.log(data);
    for (var i = 0; i < data.length; i++) {
        // console.log('i: ' + i);
        // console.log('mask index: ' + (data.length - i - 1));

        if (mask[data.length - i - 1] == undefined) {
            // console.log('no mask, continue')
            continue;
        }
        // console.log('data: ' + data[i]);
        // console.log('mask: ' + mask[data.length - i - 1]);

        empty = empty && (data[i] & mask[data.length - i - 1]) == 0;
        // console.log((data[i] & mask[data.length - i - 1]));
    }
    // console.log('empty: ' + empty)
    return empty;
}

function createMask(start, end) {
    var startByte = Math.floor(start / 8);
    var endByte = Math.floor(end / 8);
    var startBit = start % 8;
    var endBit = end % 8;

    var bytes = [];

    for (var i = 0; i < startByte; i++) {
        bytes = bytes.concat(0);
    }

    if (startByte === endByte) {
        bytes = bytes.concat(getMaskByte(startBit, endBit));
    } else {
        bytes = bytes.concat(getMaskByte(startBit, 7));

        for (var i = startByte + 1; i < endByte; i++) {
            bytes = bytes.concat(0xff);
        }

        bytes = bytes.concat(getMaskByte(0, endBit));
    }

    return bytes;
}

function getMaskByte(start, end) {
    end = end > 7 ? 7 : end;
    var mask = 0;
    for (var i = start; i <= end; i++) {
        mask += 1 << i;
    }

    return mask;
}

module.exports = countEmptySeat;