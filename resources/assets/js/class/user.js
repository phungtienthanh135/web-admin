import {SeatMap} from "./seatmap";
import {PricePolicy} from "./price_policy";
import lang from "../Lang";

export class UserType{
    constructor(_type){
        let type = !isNaN(_type)?parseInt(_type):0;
        this.value = type>=0&&type<=7? type:0;
    }
    label(){
        return UserType.properties()[this.value].label;
    }
    static DRIVER(){
        return 2;
    }
    static properties (){
        let arr = [
            {
                value : 0,
                label : "Chưa xác định",
                employees : false
            },
            {
                value : 1,
                label : 'Khách hàng',
                employees : false
            },
            {
                value : 2,
                label : 'Tài xế',
                employees : true
            },
            {
                value : 3,
                label : 'Phụ xe',
                employees : true
            },
            {
                value : 4,
                label : 'Kế toán',
                employees : true
            },
            {
                value : 5,
                label : 'Nhân viên hành chính',
                employees : true
            },
            {
                value : 6,
                label : 'Thanh tra',
                employees : true
            },
            {
                value : 7,
                label : 'Chủ nhà xe',
                employees : false
            }
        ];
        return arr;
    }
}

export class User {

}

export class UserShort {
    constructor(prop){
        this.id = prop&&prop.id?prop.id:null;
        this.fullName = prop&&prop.fullName?prop.fullName:'';
        this.phoneNumber = prop&&prop.phoneNumber?prop.phoneNumber:'';
        this.email = prop&&prop.email?prop.email:'';
        this.avatar = prop&&prop.avatar?prop.avatar:'';
    }
}

export class AgencyShort {
    constructor(prop){
        this.id = prop&&prop.id?prop.id:null;
        this.fullName = prop&&prop.fullName?prop.fullName:'';
        this.phoneNumber = prop&&prop.phoneNumber?prop.phoneNumber:'';
        this.email = prop&&prop.email?prop.email:'';
        this.avatar = prop&&prop.avatar?prop.avatar:'';
    }
}

export class Users {
    constructor (props){
        this.keyword = props&&props.keyword ? props.keyword : '';
        this.page = props&&props.page ? props.page : 0;
        this.count = props&&props.count ? props.count : 30;
        this.listUserType = props&&props.listUserType ? props.listUserType : [2,3,4,5,6];
        this.userStatus = props&&props.userStatus ? props.userStatus : undefined;
        this.list = [];
        this.loading = false;
        this.loadMore = true;
        this.getAgency = props&&props.getAgency?true:false;
    }

    getList (numberPage, callback){ // number page : tăng hoặc giảm page
        let self = this;
        if(isNaN(numberPage)){
            self.page=0;
        }else{
            self.page+= parseInt(numberPage);
        }

        let params = {
            page : self.page,
            count : self.count
        };
        if(self.keyword){
            params.fullName = self.keyword;
        }
        if(self.listUserType&&self.listUserType.length>0&&!self.getAgency){
            params.listUserType = self.listUserType;
        }
        if(self.userStatus&&self.userStatus.length>0){
            params.userStatus = self.userStatus;
        }
        if(self.getAgency){
            params.agencyCompanyId = window.userInfo.userInfo.companyId;
        }
        self.loading = true;
        self.loadMore = false;
        window.sendJson({
            url : window.urlDBD(window.API.userListJson),
            data : params,
            success : function (data) {
                self.loadMore = true;
                if(data.results.result.length<self.count){
                    self.loadMore = false;
                }
                self.loading = false;
                self.setListBeforeGet(data.results.result);
                if (typeof callback === "function") {
                    callback(data);
                }
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }

    setListBeforeGet (props){
        let self = this;
        if(this.page<=0){
            this.list = [];
        }
        $.each(props,function (p,prop) {
            self.list.push(prop);
        });
    }
}

export class LoginSecurityStatus{
    constructor(prop){
        this.value = prop||'DENIED';
    }

    label(){
        return LoginSecurityStatus.properties()[this.value].label;
    }
    color(){
        return LoginSecurityStatus.properties()[this.value].color;
    }

    static properties (){
        return {
            PENDING : {
                label : "Chờ xử lý",
                value : "PENDING",
                color : '#F39B13'
            },
            DENIED : {
                label : "Từ chối",
                value : "DENIED",
                color : '#E62020'
            },
            ALLOWED : {
                label : "Đã duyệt",
                value : "ALLOWED",
                color : '#00C48C'
            },
        };
    }
    static ENUM (){
        return {
            PENDING : "PENDING",
            DENIED : "DENIED",
            ALLOWED : "ALLOWED"
        };
    }
}

export class LoginSecurity {
    constructor(props){
        this.id = props&&props.id?props.id:null;
        this.companyId = props&&props.companyId?props.companyId:null;
        this.user = new UserShort(props&&props.userInfo?props.userInfo:null);
        this.accessCode = props&&props.accessCode?props.accessCode:null;
        this.status = new LoginSecurityStatus(props&&props.status?props.status:null);
        this.note = props&&props.note?props.note:null;
        this.createdDate = props&&props.createdDate?new Date(props.createdDate).customFormat('#hhhh#:#mm# - #DD#/#MM#/#YYYY#'):new Date().customFormat('#hhhh#:#mm# - #DD#/#MM#/#YYYY#');
    }

    save (obj){
        let self = this;
        if(typeof obj==='object'&&typeof obj.beforeSend ==='function'){
            obj.beforeSend();
        }
        self.loading = true;
        window.sendJson({
            url : window.urlDBD(window.API.accessCodeUpdate),
            data : {
                id : this.id,
                status : this.status.value
            },
            success : function (data) {
                self.loading = false;
                if(typeof obj==='object'&&typeof obj.success ==='function'){
                    obj.success(data.results);
                }
            },
            functionIfError : function (data) {
                self.loading = false;
                if(typeof obj==='object'&&typeof obj.error ==='function'){
                    obj.error();
                }
            }
        });
    }
}

export class LoginSecurities {
    constructor (props){
        this.keyword = props&&props.keyword ? props.keyword : '';
        this.user = props&&props.user ? props.user : null;
        this.status = props&&props.status ? props.status : null;
        this.page = props&&props.page ? props.page : 0;
        this.count = props&&props.count ? props.count : 30;
        this.list = [];
        this.loading = false;
        this.loadMore = true;
    }

    getList (data){ // number page : tăng hoặc giảm page
        let self = this;
        if(!data||isNaN(data.page)){
            self.page=0;
        }else{
            self.page+= parseInt(data.page);
        }

        let params = {
            page : self.page,
            count : self.count
        };
        if(self.keyword){
            params.name = self.keyword;
        }
        if(self.user){
            params.userId = self.user.id;
        }
        if(self.status){
            params.status = self.status;
        }
        self.loading = true;
        self.loadMore = false;
        window.sendJson({
            url : window.urlDBD(window.API.accessCodeList),
            data : params,
            success : function (data) {
                self.loadMore = true;
                if(data.results.data.length<self.count){
                    self.loadMore = false;
                }
                self.loading = false;
                self.setListBeforeGet(data.results.data);
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }
    setListBeforeGet(data){
        let self = this;
        if(this.page<=0){
            this.list = [];
        }
        $.each(data,function (v,item) {
            self.list.push(new LoginSecurity(item));
        });
    }
}
