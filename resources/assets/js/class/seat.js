export class SeatType {
    constructor(_type){
        let type = !isNaN(_type)?parseInt(_type):7;
        this.value = type>=0&&type<=7? type:7;
    }

    label(){
        return SeatType.properties()[this.value].label;
    }

    static properties(){
        return {
            1 : {
                label : 'Cửa',
                value : 1
            },
            2 : {
                label : 'Ghế tài',
                value : 2
            },
            3 : {
                label : 'Ghế ngồi',
                value : 3
            },
            4 : {
                label : 'Giường nằm',
                value : 4
            },
            5 : {
                label : 'WC',
                value : 5
            },
            6 : {
                label : 'Ghế phụ',
                value : 6
            },
            7 : {
                label : 'Lối đi',
                value : 7
            }
        };
    }

    static ENUM (){
        return {
            DOOR : 1,
            DRIVER : 2,
            SEAT : 3,
            BED : 4,
            WC : 5,
            ASSISTANT : 6,
            WAY : 7
        };
    }
}

export class Seat {
    constructor (seat){
        this.seatId = seat?seat.seatId||'':'';
        this.floor = seat?seat.floor||0:0;
        this.row = seat?seat.row||0:0;
        this.column = seat?seat.column||0:0;
        this.seatType = new SeatType(seat?seat.seatType:null);
        this.extraPrice = seat?seat.extraPrice||0:0;
    }

    getParamsForAPI (){
        return {
            seatId: this.seatId,
            floor: this.floor,
            row: this.row,
            column: this.column,
            seatType: this.seatType.value,
            extraPrice: this.extraPrice
        };
    }
}

export class DisplaySeatMode {
    static properties() {
        return [
            // {value: 0, label: 'Hiển thị ghế rút gọn'},
            {value: 1, label: 'Hiển thị ghế chi tiết'},
            {value: 2, label: 'Hiển thị tất cả các vé'}
        ];
    }

    static Enum() {
        return {
            MINI: 0,
            DETAIL: 1,
            ALL: 2
        };
    }
}