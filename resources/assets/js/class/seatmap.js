import {Seat,SeatType} from "./seat";

export class SeatMap {
    constructor(props) {
        this.id = props && props.seatMapId ?props.seatMapId : null;
        this.name = props && props.seatMapName ?props.seatMapName : '';
        this.row = props && props.numberOfRows ?props.numberOfRows : 0;
        this.column = props && props.numberOfColumns ?props.numberOfColumns : 0;
        this.floor = props && props.numberOfFloors ?props.numberOfFloors : 0;
        this.seatList = props && props.seatList ?props.seatList : null;
        this.seatMapFake = null;
        this.fakeSeatMap();
    }
    fakeSeatMap (){
        let data = [];
        for(let f=0;f<this.floor;f++){
            let listRow = [];
            for( let r=0;r<this.row;r++){
                let listCol = [];
                for( let c=0;c<this.column;c++){
                    listCol.push(new Seat({floor: f+1,row : r+1,column:c+1}));
                }
                listRow.push(listCol);
            }
            data.push(listRow);
        }
        if(this.seatList){
            $.each(this.seatList,function (k,v) {//v là seat
                try{
                    if(data[v.floor-1][v.row-1][v.column-1]!==undefined){
                        data[v.floor-1][v.row-1][v.column-1]=new Seat(v);
                    }// các thông sô tầng hàng cột bắt đầu từ 1, mapdata từ 0 nên phải trừ 1
                }catch(e){
                    console.log('ticket error' + JSON.stringify(v));
                }
            });
        }
        this.seatMapFake = data;
        return data;
    }

    getParamsForAPI (){
        let data = {
            seatMapId : this.id,
            seatMapName : this.name,
            numberOfFloors : this.floor,
            numberOfColumns : this.column,
            numberOfRows : this.row,
            seatList : []
        };
        $.each(this.seatMapFake,function (f,floor) {
            $.each(floor,function (r,row) {
                $.each(row,function (c,column) {
                    if(column.seatType.value!== SeatType.ENUM().WAY){
                        data.seatList.push(column.getParamsForAPI());
                    }
                });
            });
        });
        return data;
    }
}

export class SeatMaps {
    constructor (props){
        this.keyword = props&&props.keyword ? props.keyword : '';
        this.page = props&&props.page ? props.page : 0;
        this.count = props&&props.count ? props.count : 30;
        this.list = [];
        this.loading = false;
        this.loadMore = true;
    }

    getList (numberPage){ // number page : tăng hoặc giảm page
        let self = this;
        if(isNaN(numberPage)){
            self.page=0;
        }else{
            self.page+= parseInt(numberPage);
        }

        let params = {
            page : self.page,
            count : self.count
        };
        if(self.keyword){
            params.name = self.keyword;
        }
        self.loading = true;
        self.loadMore = false;
        window.sendJson({
            url : window.urlDBD(window.API.seatMapList),
            data : params,
            success : function (data) {
                self.loadMore = true;
                if(data.results.seatMaps.length<self.count){
                    self.loadMore = false;
                }
                self.loading = false;
                self.setListBeforeGet(data.results.seatMaps);
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }

    setListBeforeGet (data){
        let self = this;
        if(this.page<=0){
            this.list = [];
        }
        $.each(data,function (v,item) {
            self.list.push(new SeatMap(item));
        });
    }
}