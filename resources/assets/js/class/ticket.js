import {PointOfTicket} from "./point";
import {RouteShort} from "./route";
import {Seat} from "./seat";
import {UserShort} from "./user";

export function calculatePriceTicket(ticket) {
    // trong ticket yêu cầu có :
    // originPrice : giá của chặng trong tuyến;
    // extraPrice : giá công thêm của ghế ;
    // childrenTicketRatio : tỉ lệ giá người lớn và giá trẻ em;
    let isAdult = !ticket.isChild;
    ticket.priceSeatAndExtra = ticket.originPrice + ticket.extraPrice;
    //isAdult là người lớn,
    ticket.priceAfterAdult = isAdult == false && ticket.childrenTicketRatio !== undefined ? _.cloneDeep(ticket.priceSeatAndExtra) * ticket.childrenTicketRatio : _.cloneDeep(ticket.priceSeatAndExtra);// tính giá trẻ em
    // tính hoa hồng đại lý
    ticket.priceCommission=0;
    if(ticket.policy.commission){
        let cms = ticket.policy.commission;
        if(cms.commissionType===2){
            ticket.priceCommission = ticket.priceAfterAdult * cms.commissionValue;
        }else{
            ticket.priceCommission = _.cloneDeep(cms.commissionValue);
        }
    }
    ticket.priceAfterCommission = _.cloneDeep(ticket.priceAfterAdult) - ticket.priceCommission;

    ticket.pricePromotion = 0;
    if(ticket.policy.listPromotion){
        let tkPromotionPrice = 0;
        $.each(ticket.policy.listPromotion,function (p,prm) {
            if(prm.price>-1){
                tkPromotionPrice += _.cloneDeep(prm.price);
            }
            if(prm.percent>-1){
                tkPromotionPrice += prm.percent * ticket.priceAfterCommission;//priceAfterAdult
            }
        });
        ticket.pricePromotion = tkPromotionPrice;
    }

    ticket.priceTransshipmentUp = 0;
    ticket.priceTransshipmentDown = 0;
    if(ticket.pointUp.pointType == 3){
        if(ticket.transshipmentInPoint&&ticket.transshipmentInPoint.transshipmentPrice){
            ticket.priceTransshipmentUp = _.cloneDeep(ticket.transshipmentInPoint.transshipmentPrice);
        }
    }
    if(ticket.pointDown.pointType == 3){
        if(ticket.transshipmentOffPoint&&ticket.transshipmentOffPoint.transshipmentPrice){
            ticket.priceTransshipmentDown = _.cloneDeep(ticket.transshipmentOffPoint.transshipmentPrice);
        }
    }
    ticket.priceAddition = 0;
    if(ticket.policy.addition){
        let adt = ticket.policy.addition;
        if(adt.type==1){
            ticket.priceAddition = adt.amount*adt.mode;
        }else{
            ticket.priceAddition = adt.amount*adt.mode*ticket.priceAfterCommission;//priceAfterAdult
        }
    }
    ticket.priceSurcharges = 0;
    if (ticket.surcharges) {
        $.each(ticket.surcharges, function (s, su) {
            ticket.priceSurcharges += su.mode * su.price;
        });
    }

    ticket.totalPrice = ticket.priceSurcharges + parseFloat(ticket.priceAfterAdult) - parseFloat(ticket.priceCommission) - parseFloat(ticket.pricePromotion) + parseFloat(ticket.priceTransshipmentUp) + parseFloat(ticket.priceTransshipmentDown) + parseFloat(ticket.priceAddition);

    return ticket;
}

export class TicketStatus {
    constructor(status,overTime,gotIntoTrip,isCompleted){
        this.status = status||0;
        this.overTime = overTime!==undefined?overTime:0;
        this.gotIntoTrip = gotIntoTrip!==undefined?gotIntoTrip:false;
        this.isCompleted = isCompleted!==undefined?isCompleted:0;
        if(!(status && status === 2 && overTime&&(overTime===0||overTime>(new Date().getTime())))){ // hết hạn giữ chỗ
            this.status = 1;
        }
        if(gotIntoTrip){
            this.status = 4;
        }
        if(isCompleted){
            this.status = 5;
        }
    }
    label (){
        return TicketStatus.properties()[this.status]?TicketStatus.properties()[this.status].label : '';
    }
    background (){
        return TicketStatus.properties()[this.status]?TicketStatus.properties()[this.status].background : '';
    }
    color (){
        return TicketStatus.properties()[this.status]?TicketStatus.properties()[this.status].color : '';
    }
    mode (){
        return TicketStatus.properties()[this.status]?TicketStatus.properties()[this.status].mode : '';
    }
    static properties(){
        return {
            0 : {
                label : 'Đã hủy',
                background : 'bg-success',
                color : 'text-success',
                mode : 'cancel'
            },
            1 : {
                label : 'Hết hạn giữ chỗ',
                background : 'bg-huy',
                color : 'text-danger',
                mode : 'book'
            },
            2 : {
                label : 'Giữ chỗ',
                background : 'bg-giucho',
                color : 'text-giucho',
                mode : 'book'
            },
            3 : {
                label : 'Đã thanh toán',
                background : 'bg-success',
                color : 'text-success',
                mode : 'pay'
            },
            4 : {
                label : 'Đã lên xe',
                background : 'bg-success',
                color : 'text-success',
                mode : 'book'
            },
            5 : {
                label : 'Đã hoàn thành',
                background : 'bg-success',
                color : 'text-success',
                mode : 'book'
            },
            7 : {
                label : 'Giữ chỗ ưu tiên',
                background : 'bg-giucho',
                color : 'text-giucho',
                mode : 'book'
            }
        };
    }
}

export class DeliverStatus {
    static properties() {
        return {
            '-1': {
                value: -1,
                label: 'Đặt vé'
            },
            0: {
                value: 0,
                label: 'Phân công'
            },
            1: {
                value: 1,
                label: 'Đang giao'
            },
            2: {
                value: 2,
                label: 'Giao thành công'
            },
            3: {
                value: 3,
                label: 'Giao không thành công'
            }
        };
    }
}

export class Ticket {
    constructor (prop){
        this.id = prop?prop.ticketId:null;
        this.code = prop?prop.ticketCode:null;
        this.fullName = prop?prop.fullName:'';
        this.phoneNumber = prop?prop.phoneNumber:'';
        this.email = prop?prop.email:'';
        this.birthday = prop?prop.birthday:'';
        this.isChild = prop?prop.isChild:'';
        this.pointUp = new PointOfTicket(prop&&prop.pointUp?prop.pointUp:null);
        this.pointDown = new PointOfTicket(prop&&prop.pointDown?prop.pointDown:null);
        this.ticketStatus = new TicketStatus(prop?prop.ticketStatus:null,prop?prop.overTime:null,prop?prop.gotIntoTrip:null,prop?prop.isCompleted:null);
        this.route = new RouteShort(prop.routeInfo);
        this.trip = prop.tripId;
        this.seat = new Seat(prop?prop.seat:null);
        this.seller = new UserShort(prop?prop.seller:null);
    }
}