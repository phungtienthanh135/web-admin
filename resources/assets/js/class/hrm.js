// human resource manage
import store from '../store';
import filter from '../Helper/Filters';
import lang from '../Lang/index';

export class HRM {
    static properties(){
        return {
            0 : {
                label : 'Khen thưởng, kỷ luật',
                value : 'RoP'
            },
            1 : {
                label : 'Quá trình làm việc',
                value : 'RoP'
            },
            2 : {
                label : 'Sức khỏe',
                value : 'Health'
            },
            3 : {
                label : 'Vi phạm giao thông',
                value : 'TrafficViolation'
            },
            4 : {
                label : 'Đào tạo, tập huấn',
                value : 'TrainingCourse'
            },
        };
    }
}

export class RoP {//reward or punishment - thưởng hoặc phạt
    constructor(prop){
        this.id = prop&&prop.id? prop.id : null;
        this.userId = prop&&prop.userId? prop.userId : window.userInfo.userInfo.id;
        this.content = prop&&prop.content? prop.content : '';
        this.isAward = prop&&prop.isAward? 1 : 0;
        this.createdDate = prop&&prop.createdDate? prop.createdDate : 0;
    }

    labelAward (){
        return RoP.isAwardProperties()[this.isAward].label;
    }

    static isAwardProperties (){
        return {
            0 : {
                value : 0,
                label : 'Phạt',
            },
            1 : {
                value : 1,
                label : 'Thưởng',
            },
        };
    }

    save (success,error){
        store.commit('LOAD','SAVE_RoP');
        let params = {
            userId : this.userId,
            content : this.content,
            isAward : this.isAward?true:false
        };
        if(this.id!==null){
            params.id = this.id;
        }

        window.sendJson({
            url : window.urlDBD(this.id?window.API.hrmRewardOrPunishmentUpdate:window.API.hrmRewardOrPunishmentCreate),
            data : params,
            success :function (result) {
                store.commit('UNLOAD','SAVE_RoP');
                if(typeof success == "function"){
                    success(result);
                }
            },
            functionIfError : function (err) {
                store.commit('UNLOAD','SAVE_RoP');
                if(typeof error == "function"){
                    error(err);
                }
            }
        });
    }

    remove (success,error){
        if(!this.id){
            return false;
        }
        let cancelReason = window.prompt('Tại sao bạn muốn xóa?');
        if(cancelReason!==null){
            store.commit('LOAD','DELETE_RoP');
            window.sendJson({
                url : window.urlDBD(window.API.hrmRewardOrPunishmentDelete),
                data : {
                    id : this.id,
                    cancelReason : cancelReason,
                },
                success :function (result) {
                    store.commit('UNLOAD','DELETE_RoP');
                    if(typeof success == "function"){
                        success(result);
                    }
                },
                functionIfError : function (err) {
                    store.commit('UNLOAD','DELETE_RoP');
                    if(typeof error == "function"){
                        error(err);
                    }
                }
            });
        }
    }
}

export class ListRoP {
    constructor (prop){
        this.query = '';
        this.page = 0;
        this.count = 50;
        this.userId = prop&&prop.userId ? prop.userId : null;
        this.list = [];
    }

    getList(beforeLoad,success,error){
        let self = this;
        store.commit('LOAD','GET_LIST_RoP');
        let params = {
            page : this.page,
            count : this.count
        };
        if(this.userId){
            params.userId = this.userId;
        }
        if(this.query){
            params.query = this.query;
        }
        if(typeof beforeLoad == "function"){
            beforeLoad();
        }
        window.sendJson({
            url : window.urlDBD(window.API.hrmRewardOrPunishmentList),
            data : params,
            success : function(rs){
                store.commit('UNLOAD','GET_LIST_RoP');
                self.setList(rs.results.data);
                if(typeof success == "function"){
                    success(rs);
                }
            },
            functionIfError : function(rs){
                store.commit('UNLOAD','GET_LIST_RoP');
                if(typeof error == "function"){
                    error(rs);
                }
            }
        });
    }

    setList(_list){
        let list = [];
        $.each(_list,function (k,value) {
            list.push(new RoP(value));
        });
        this.list = list;
    }
}


export class Health {//reward or punishment - thưởng hoặc phạt
    constructor(prop){
        this.id = prop&&prop.id? prop.id : null;
        this.userId = prop&&prop.userId? prop.userId : window.userInfo.userInfo.id;
        this.hospitalName = prop&&prop.hospitalName? prop.hospitalName : '';
        this.result = prop&&prop.result? prop.result : '';
        this.date = prop&&prop.date? filter.customDate(prop.date,'#DD#-#MM#-#YYYY#') : new Date().customFormat('#DD#-#MM#-#YYYY#');
    }

    save (success,error){
        store.commit('LOAD','SAVE_RoP');
        let params = {
            userId : this.userId,
            hospitalName : this.hospitalName,
            result : this.result,
            date : window.func.dateToMS(this.date)
        };
        if(this.id!==null){
            params.id = this.id;
        }

        window.sendJson({
            url : window.urlDBD(this.id?window.API.hrmHealthCheckUpdate:window.API.hrmHealthCheckCreate),
            data : params,
            success :function (result) {
                store.commit('UNLOAD','SAVE_RoP');
                if(typeof success == "function"){
                    success(result);
                }
            },
            functionIfError : function (err) {
                store.commit('UNLOAD','SAVE_RoP');
                if(typeof error == "function"){
                    error(err);
                }
            }
        });
    }

    remove (success,error){
        if(!this.id){
            return false;
        }
        let cancelReason = window.prompt('Tại sao bạn muốn xóa?');
        if(cancelReason!==null){
            store.commit('LOAD','DELETE_RoP');
            window.sendJson({
                url : window.urlDBD(window.API.hrmHealthCheckDelete),
                data : {
                    id : this.id,
                    cancelReason : cancelReason
                },
                success :function (result) {
                    store.commit('UNLOAD','DELETE_RoP');
                    if(typeof success == "function"){
                        success(result);
                    }
                },
                functionIfError : function (err) {
                    store.commit('UNLOAD','DELETE_RoP');
                    if(typeof error == "function"){
                        error(err);
                    }
                }
            });
        }
    }
}

export class ListHealth {
    constructor (prop){
        this.query = '';
        this.page = 0;
        this.count = 50;
        this.userId = prop&&prop.userId ? prop.userId : null;
        this.list = [];
    }

    getList(beforeLoad,success,error){
        let self = this;
        store.commit('LOAD','GET_LIST_RoP');
        let params = {
            page : this.page,
            count : this.count
        };
        if(this.userId){
            params.userId = this.userId;
        }
        if(this.query){
            params.query = this.query;
        }
        if(typeof beforeLoad == "function"){
            beforeLoad();
        }
        window.sendJson({
            url : window.urlDBD(window.API.hrmHealthCheckList),
            data : params,
            success : function(rs){
                store.commit('UNLOAD','GET_LIST_RoP');
                self.setList(rs.results.data);
                if(typeof success == "function"){
                    success(rs);
                }
            },
            functionIfError : function(rs){
                store.commit('UNLOAD','GET_LIST_RoP');
                if(typeof error == "function"){
                    error(rs);
                }
            }
        });
    }

    setList(_list){
        let list = [];
        $.each(_list,function (k,value) {
            list.push(new Health(value));
        });
        this.list = list;
    }
}

export class TrainingCourse {//tập huấn
    constructor(prop){
        this.id = prop&&prop.id? prop.id : null;
        this.userId = prop&&prop.userId? prop.userId : window.userInfo.userInfo.id;
        this.name = prop&&prop.name? prop.name : '';
        this.host = prop&&prop.host? prop.host : '';
        this.startDate = prop&&prop.startDate? filter.customDate(prop.startDate,'#DD#-#MM#-#YYYY#') : new Date().customFormat('#DD#-#MM#-#YYYY#');
        this.endDate = prop&&prop.endDate? filter.customDate(prop.endDate,'#DD#-#MM#-#YYYY#') : new Date().customFormat('#DD#-#MM#-#YYYY#');
        this.expiredDate = prop&&prop.expiredDate? filter.customDate(prop.expiredDate,'#DD#-#MM#-#YYYY#') : new Date().customFormat('#DD#-#MM#-#YYYY#');
    }

    save (success,error){
        store.commit('LOAD','SAVE_TRAINING');
        let params = {
            userId : this.userId,
            name : this.name,
            host : this.host,
            startDate : window.func.dateToMS(this.startDate),
            endDate : window.func.dateToMS(this.endDate),
            expiredDate : window.func.dateToMS(this.expiredDate)
        };
        if(this.id!==null){
            params.id = this.id;
        }

        window.sendJson({
            url : window.urlDBD(this.id?window.API.hrmTrainingCourseUpdate:window.API.hrmTrainingCourseCreate),
            data : params,
            success :function (result) {
                store.commit('UNLOAD','SAVE_TRAINING');
                if(typeof success == "function"){
                    success(result);
                }
            },
            functionIfError : function (err) {
                store.commit('UNLOAD','SAVE_TRAINING');
                if(typeof error == "function"){
                    error(err);
                }
            }
        });
    }

    remove (success,error){
        if(!this.id){
            return false;
        }
        let cancelReason = window.prompt('Tại sao bạn muốn xóa?');
        if(cancelReason!==null){
            store.commit('LOAD','DELETE_TRAINING');
            window.sendJson({
                url : window.urlDBD(window.API.hrmTrainingCourseDelete),
                data : {
                    id : this.id,
                    cancelReason : cancelReason
                },
                success :function (result) {
                    store.commit('UNLOAD','DELETE_TRAINING');
                    if(typeof success == "function"){
                        success(result);
                    }
                },
                functionIfError : function (err) {
                    store.commit('UNLOAD','DELETE_TRAINING');
                    if(typeof error == "function"){
                        error(err);
                    }
                }
            });
        }
    }
}

export class ListTrainingCourse {
    constructor (prop){
        this.query = '';
        this.page = 0;
        this.count = 50;
        this.userId = prop&&prop.userId ? prop.userId : null;
        this.list = [];
    }

    getList(beforeLoad,success,error){
        let self = this;
        store.commit('LOAD','GET_LIST_TRAINING');
        let params = {
            page : this.page,
            count : this.count
        };
        if(this.userId){
            params.userId = this.userId;
        }
        if(this.query){
            params.query = this.query;
        }
        if(typeof beforeLoad == "function"){
            beforeLoad();
        }
        window.sendJson({
            url : window.urlDBD(window.API.hrmTrainingCourseList),
            data : params,
            success : function(rs){
                store.commit('UNLOAD','GET_LIST_TRAINING');
                self.setList(rs.results.data);
                if(typeof success == "function"){
                    success(rs);
                }
            },
            functionIfError : function(rs){
                store.commit('UNLOAD','GET_LIST_TRAINING');
                if(typeof error == "function"){
                    error(rs);
                }
            }
        });
    }

    setList(_list){
        let list = [];
        $.each(_list,function (k,value) {
            list.push(new TrainingCourse(value));
        });
        this.list = list;
    }
}

export class TrafficViolation {//Vi phạm ATGT
    constructor(prop){
        this.id = prop&&prop.id? prop.id : null;
        this.userId = prop&&prop.userId? prop.userId : window.userInfo.userInfo.id;
        this.violation = prop&&prop.violation? prop.violation : '';
        this.condemnation = prop&&prop.condemnation? prop.condemnation : '';
        this.note = prop&&prop.note? prop.note : '';
        this.date = prop&&prop.date? filter.customDate(prop.date,'#DD#-#MM#-#YYYY#') : new Date().customFormat('#DD#-#MM#-#YYYY#');
    }

    save (success,error){
        store.commit('LOAD','SAVE_TRAFFIC_VIOLATION');
        let params = {
            userId : this.userId,
            violation : this.violation,
            condemnation : this.condemnation,
            note : this.note,
            date : window.func.dateToMS(this.date)
        };
        if(this.id!==null){
            params.id = this.id;
        }

        window.sendJson({
            url : window.urlDBD(this.id?window.API.hrmTrafficViolationUpdate:window.API.hrmTrafficViolationCreate),
            data : params,
            success :function (result) {
                store.commit('UNLOAD','SAVE_TRAFFIC_VIOLATION');
                if(typeof success == "function"){
                    success(result);
                }
            },
            functionIfError : function (err) {
                store.commit('UNLOAD','SAVE_TRAFFIC_VIOLATION');
                if(typeof error == "function"){
                    error(err);
                }
            }
        });
    }

    remove (success,error){
        if(!this.id){
            return false;
        }
        let cancelReason = window.prompt('Tại sao bạn muốn xóa?');
        if(cancelReason!==null){
            store.commit('LOAD','DELETE_TRAFFIC_VIOLATION');
            window.sendJson({
                url : window.urlDBD(window.API.hrmTrafficViolationDelete),
                data : {
                    id : this.id,
                    cancelReason : cancelReason
                },
                success :function (result) {
                    store.commit('UNLOAD','DELETE_TRAFFIC_VIOLATION');
                    if(typeof success == "function"){
                        success(result);
                    }
                },
                functionIfError : function (err) {
                    store.commit('UNLOAD','DELETE_TRAFFIC_VIOLATION');
                    if(typeof error == "function"){
                        error(err);
                    }
                }
            });
        }
    }
}

export class ListTrafficViolation {
    constructor (prop){
        this.query = '';
        this.page = 0;
        this.count = 50;
        this.userId = prop&&prop.userId ? prop.userId : null;
        this.list = [];
    }

    getList(beforeLoad,success,error){
        let self = this;
        store.commit('LOAD','GET_TRAFFIC_VIOLATION');
        let params = {
            page : this.page,
            count : this.count
        };
        if(this.userId){
            params.userId = this.userId;
        }
        if(this.query){
            params.query = this.query;
        }
        if(typeof beforeLoad == "function"){
            beforeLoad();
        }
        window.sendJson({
            url : window.urlDBD(window.API.hrmTrafficViolationList),
            data : params,
            success : function(rs){
                store.commit('UNLOAD','GET_TRAFFIC_VIOLATION');
                self.setList(rs.results.data);
                if(typeof success == "function"){
                    success(rs);
                }
            },
            functionIfError : function(rs){
                store.commit('UNLOAD','GET_TRAFFIC_VIOLATION');
                if(typeof error == "function"){
                    error(rs);
                }
            }
        });
    }

    setList(_list){
        let list = [];
        $.each(_list,function (k,value) {
            list.push(new TrafficViolation(value));
        });
        this.list = list;
    }
}

export class WorkProcess {//Quá trình làm việc
    constructor(prop){
        this.id = prop&&prop.id? prop.id : null;
        this.userId = prop&&prop.userId? prop.userId : window.userInfo.userInfo.id;
        this.licenseLevel = prop&&prop.licenseLevel? prop.licenseLevel : '';
        this.contactType = prop&&prop.contactType? prop.contactType : '';
        this.vehicleControlling = {
            vehicleType : prop&&prop.vehicleControlling&&prop.vehicleControlling.vehicleType? prop.vehicleControlling.vehicleType : '',
            load : prop&&prop.vehicleControlling&&prop.vehicleControlling.load? prop.vehicleControlling.load : ''
        };
        this.date = prop&&prop.date? filter.customDate(prop.date,'#DD#-#MM#-#YYYY#') : new Date().customFormat('#DD#-#MM#-#YYYY#');
    }

    save (success,error){
        store.commit('LOAD','SAVE_WORK');
        let params = {
            userId : this.userId,
            licenseLevel : this.licenseLevel,
            contactType : this.contactType,
            vehicleControlling : {
                vehicleType  :this.vehicleControlling.vehicleType,
                load  :this.vehicleControlling.load
            },
            date : window.func.dateToMS(this.date)
        };
        if(this.id!==null){
            params.id = this.id;
        }

        window.sendJson({
            url : window.urlDBD(this.id?window.API.hrmWorkProcessUpdate:window.API.hrmWorkProcessCreate),
            data : params,
            success :function (result) {
                store.commit('UNLOAD','SAVE_WORK');
                if(typeof success == "function"){
                    success(result);
                }
            },
            functionIfError : function (err) {
                store.commit('UNLOAD','SAVE_WORK');
                if(typeof error == "function"){
                    error(err);
                }
            }
        });
    }

    remove (success,error){
        if(!this.id){
            return false;
        }
        let cancelReason = window.prompt('Tại sao bạn muốn xóa?');
        if(cancelReason!==null){
            store.commit('LOAD','DELETE_WORK');
            window.sendJson({
                url : window.urlDBD(window.API.hrmWorkProcessDelete),
                data : {
                    id : this.id,
                    cancelReason : cancelReason
                },
                success :function (result) {
                    store.commit('UNLOAD','DELETE_WORK');
                    if(typeof success == "function"){
                        success(result);
                    }
                },
                functionIfError : function (err) {
                    store.commit('UNLOAD','DELETE_WORK');
                    if(typeof error == "function"){
                        error(err);
                    }
                }
            });
        }
    }
}

export class ListWorkProcess {
    constructor (prop){
        this.query = '';
        this.page = 0;
        this.count = 50;
        this.userId = prop&&prop.userId ? prop.userId : null;
        this.list = [];
    }

    getList(beforeLoad,success,error){
        let self = this;
        store.commit('LOAD','GET_WORKS');
        let params = {
            page : this.page,
            count : this.count
        };
        if(this.userId){
            params.userId = this.userId;
        }
        if(this.query){
            params.query = this.query;
        }
        if(typeof beforeLoad == "function"){
            beforeLoad();
        }
        window.sendJson({
            url : window.urlDBD(window.API.hrmWorkProcessList),
            data : params,
            success : function(rs){
                store.commit('UNLOAD','GET_WORKS');
                self.setList(rs.results.data);
                if(typeof success == "function"){
                    success(rs);
                }
            },
            functionIfError : function(rs){
                store.commit('UNLOAD','GET_WORKS');
                if(typeof error == "function"){
                    error(rs);
                }
            }
        });
    }

    setList(_list){
        let list = [];
        $.each(_list,function (k,value) {
            list.push(new WorkProcess(value));
        });
        this.list = list;
    }
}