
export class ElectronicTicket {
    constructor(props) {
        this.route = props&&props.route ? props.route : null;
        this.ticketCode = props&&props.ticketCode ? props.ticketCode : '';
        this.getInTimePlan = props&&props.getInTimePlan ? props.getInTimePlan : '';
        this.createdDate = props&&props.createdDate ? props.createdDate : '';
        this.getInTimePlanInt = props&&props.getInTimePlanInt ? props.getInTimePlanInt : '';
        this.fullName = props&&props.fullName ? props.fullName : '';
        this.phoneNumber = props&&props.phoneNumber ? props.phoneNumber : '';
        this.routeInfo = props&&props.routeInfo ? props.routeInfo : '';
        this.transferStatus = props&&props.transferStatus ? props.transferStatus : '';
        this.cashier = props&&props.cashier ? props.cashier : '';
        this.tripInfo = props&&props.tripInfo ? props.tripInfo : '';
        this.email = props&&props.email ? props.email : '';
        this.tripId = props&&props.tripId ? props.tripId : '';
        this.routeId = props&&props.routeId ? props.routeId : '';
        this.pointUp = props&&props.pointUp ? props.pointUp : '';
        this.pointDown = props&&props.pointDown ? props.pointDown : '';
        this.ticketStatus = props&&props.ticketStatus ? props.ticketStatus : '';
        this.room = props&&props.room ? props.room : '';
        this.invoiceInfo = props&&props.invoiceInfo ? props.invoiceInfo : '';
        this.seller = props&&props.seller ? props.seller : '';
        this.seat = props&&props.seat ? props.seat : '';
        this.floor = props&&props.floor ? props.floor : '';
        this.ticketId = props&&props.ticketId ? props.ticketId : '';
        this.agencyPrice = props&&props.agencyPrice ? props.agencyPrice : '';
        this.page = props&&props.page ? props.page : 0;
        this.count = props&&props.count ? props.count : 30;
        this.loading = false;
    }

    sendInvoice(data, call){
        let self = this;
        self.loading = true
        let params = [data.ticketId];
        let url = window.API.sendInvoice;

        window.sendJson({
            url : window.urlDBD(url),
            data : params,
            success :function (result) {
                self.loading = false;
                if(typeof(call) == 'function'){
                    call();
                }
            },
            functionIfError : function (err) {
                self.loading= false;
                if(data&&typeof data.error === 'function'){
                    data.error(err);
                }
            }
        });
    }
    sendMail(data){
        let self = this;
        self.loading = true
        let params = [
                {
                    ticketId : data
                }
            ];
        let url = window.API.ticketUpdate;

        window.sendJson({
            url : window.urlDBD(url),
            data : params,
            success :function (result) {
                self.loading = false;
            },
            functionIfError : function (err) {
                self.loading= false;
                if(data&&typeof data.error === 'function'){
                    data.error(err);
                }
            }
        });
    }
}
var date = new Date();
export class ListElectronicTicket {
    constructor (props){
        this.keyword = props&&props.keyword ? props.keyword : '';
        this.route = props&&props.route ? props.route : null;
        /*this.startDate = props&&props.startDate ? props.startDate : '';
        this.endDate = props&&props.endDate ? props.endDate : '';*/
        this.rangeDate = [new Date(date.getFullYear(), date.getMonth(), 1).customFormat('#YYYY##MM##DD#'), new Date().customFormat('#YYYY##MM##DD#')];
        this.phoneNumber = props&&props.phoneNumber ? props.phoneNumber : '';
        this.requireInvoiceStatuses = props&&props.requireInvoiceStatuses ? props.requireInvoiceStatuses : [1];
        this.ticketCode = props&&props.ticketCode ? props.ticketCode : '';
        this.page = props&&props.page ? props.page : 0;
        this.count = props&&props.count ? props.count : 30;
        this.list = [];
        this.loading = false;
    }

    getList (){
        console.log('get')
        let self = this;
        self.loading = true;
        let params = {
        };
        if(!self.rangeDate){
            params.startDate = new Date(date.getFullYear(), date.getMonth(), 1).customFormat('#YYYY##MM##DD#');
            params.endDate = new Date().customFormat('#YYYY##MM##DD#');
        }
        if(self.rangeDate){
            params.startDate = self.rangeDate[0];
            params.endDate = self.rangeDate[1];
        }
        if(self.route){
            params.routeIds = [];
            /*self.route.forEach(( elem ) => {
                params.routeIds.push(elem.routeId);
            });*/
            $.each(self.route,function (v,item) {
                params.routeIds.push(item.routeId);
            });
        }
        if(self.requireInvoiceStatuses){
            params.requireInvoiceStatuses = [];
            /*self.route.forEach(( elem ) => {
                params.routeIds.push(elem.routeId);
            });*/
            $.each(self.requireInvoiceStatuses,function (v,item) {
                params.requireInvoiceStatuses.push(item);
            });
        }
        sendParam({
            url : window.urlDBD(window.API.getListTicketInvoice),
            type: 'get',
            data : params,
            success : function (data) {
                self.loading = false;
                self.setListBeforeGet(data.results.listTicket);
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }

    setListBeforeGet (data){
        let self = this;
        if(this.page<=0){
            this.list = [];
        }
        $.each(data,function (v,item) {
            self.list.push(new ElectronicTicket(item));
        });
    }
}

