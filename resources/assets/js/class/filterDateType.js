export class FilterDateType {
    
    static properties(){
        return [
            {
                key: 1,
                label: 'Ngày tạo'
            },
            {
                key: 2,
                label: 'Ngày đi'
            },
            {
                key: 3,
                label: 'Ngày thanh toán'
            },
        ];
    }
}