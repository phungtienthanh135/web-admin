export class DelivaryStatus {
    
    static properties(){
        return {
                "-1": {value: -1, label: "Đặt vé"},
                0: {value: 0, label: "Phân công"},
                1: {value: 1, label: "Đang giao"},
                2: {value: 2, label: "Giao thành công"},
                3: {value: 3, label: "Giao không thành công"},
        };
    }
}