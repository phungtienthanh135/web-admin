
import {CollectionAnDeliveryType, GoodsFees} from "./goods_v2";
import _ from "lodash";
import {DisplaySeatMode} from './seat';

window.$= require('jquery');

export class PaymentType {
    static properties(){
        return {
            1 : {value : 1, label : 'Thanh toán trực tuyến (online)'},
            2 : {value : 2, label : 'Thanh toán trên xe'},
            3 : {value : 3, label : 'Thanh toán tiền mặt tại quầy'},
            6 : {value : 6, label : 'Chuyển khoản'},
        };
    }
}

export class ItemDefault {
    static properties(){
        return {
            1 : {value : 1, label : 'Thanh toán trực tuyến (online)'},
            2 : {value : 2, label : 'Thanh toán trên xe'},
            3 : {value : 3, label : 'Thanh toán tiền mặt tại quầy'},
            6 : {value : 6, label : 'Chuyển khoản'},
        };
    }
}

export class CompanyConfig {
    constructor() {
        this.loading = false;
        this.CONFIG = {
            DELIVERY_TICKET: new DELIVERY_TICKET(),
            COMPANY_NAME: new COMPANY_NAME(),
            COMPANY_CALL_NAME: new COMPANY_CALL_NAME(),
            BUSINESSREGISTRATION: new BUSINESSREGISTRATION(),
            SAMPLE_SIGNATURE: new SAMPLE_SIGNATURE(),
            SMSBRANDNAME: new SMSBRANDNAME(),
            NAMEOWNER: new NAMEOWNER(),
            PHONENUMBER_OWNER: new PHONENUMBER_OWNER(),
            EMAIL_OWNER: new EMAIL_OWNER(),
            TAX_CODE: new TAX_CODE(),
            ADDRESS_CONTRACT_OWNER: new ADDRESS_CONTRACT_OWNER(),
            ACCOUNT_BANK_INFO_OWNER: new ACCOUNT_BANK_INFO_OWNER(),
            POSITION_OWNER: new POSITION_OWNER(),
            ONLY_PRINT_TICKET_WHEN_BOUGTH: new ONLY_PRINT_TICKET_WHEN_BOUGTH(),
            LOCK_ACTION_AFTER_DEPARTED: new LOCK_ACTION_AFTER_DEPARTED(),
            SELECT_ROOM_AFTER_LOGIN: new SELECT_ROOM_AFTER_LOGIN(),
            SURCHARGE_CATEGORY: new SURCHARGE_CATEGORY(),
            // USE_NEWEST_INTERFACE: new USE_NEWEST_INTERFACE(),
            USE_MANAGE_PERMISSION_SELL_TICKET: new USE_MANAGE_PERMISSION_SELL_TICKET(),
            NUMBER_OF_PRINT_TICKET: new NUMBER_OF_PRINT_TICKET(),
            SIZE_WEIGHT_CONVERSION: new SIZE_WEIGHT_CONVERSION(),
            VALID_GOODS_COLLECTION_METHODS: new VALID_GOODS_COLLECTION_METHODS(),
            VALID_GOODS_DELIVERY_METHODS: new VALID_GOODS_DELIVERY_METHODS(),
            DEFAULT_PAYMENT_TYPE: new DEFAULT_PAYMENT_TYPE(),
            DEFAULT_ITEM: new DEFAULT_ITEM(),
            DEBIT_DRIVER_ITEM: new DEBIT_DRIVER_ITEM(),
            REVENUE_DRIVER_ITEM: new REVENUE_DRIVER_ITEM(),
            DISPLAY_SEAT_MODE: new DISPLAY_SEAT_MODE(),
            DISPLAY_NEW_LIST_TRIP: new DISPLAY_NEW_LIST_TRIP(),
            DISPLAY_TRIP_DETAIL: new DISPLAY_TRIP_DETAIL(),
            DISPLAY_WAY: new DISPLAY_WAY(),
            DISPLAY_TRIP_CODE: new DISPLAY_TRIP_CODE(),
            DISPLAY_TIME_AT_FIRST_POINT: new DISPLAY_TIME_AT_FIRST_POINT(),
            MERGE_TICKET_PRINT: new MERGE_TICKET_PRINT(),
            SYNC_CONFIG_SELL_FORM: new SYNC_CONFIG_SELL_FORM(),
            CONFIRM_TRANFER: new CONFIRM_TRANFER (),
            TRANSHIPMENT_BY_DRIVER: new TRANSHIPMENT_BY_DRIVER (),
            TIME_CANCEL_TICKET_CONFIRM_TRANSFER_BEFORE_START_TRIP: new TIME_CANCEL_TICKET_CONFIRM_TRANSFER_BEFORE_START_TRIP (),
            AUTO_CANCEL_TICKET: new AUTO_CANCEL_TICKET (),
            EDIT_AGENCY_AFTER_PAID: new EDIT_AGENCY_AFTER_PAID (),
            AUTO_SENT_SMS: new AUTO_SENT_SMS (),
            TIME_START_GET_DATA: new TIME_START_GET_DATA (),
            HIDE_PENDING_TICKET_WHEN_PAY: new HIDE_PENDING_TICKET_WHEN_PAY (),
            SHOWNUMBERSOLDSEAT: new SHOWNUMBERSOLDSEAT (),
            HIDE_FIELD_IN_SELL_TICKET: new HIDE_FIELD_IN_SELL_TICKET (),
            HIDE_FIELD_IN_GOOD_IN_TRIP: new HIDE_FIELD_IN_GOOD_IN_TRIP (),
            HIDE_FIELD_IN_MANAGE_GOOD: new HIDE_FIELD_IN_MANAGE_GOOD (),
            HIDE_FIELD_TRANSSHIPMENT_BY_TICKET: new HIDE_FIELD_TRANSSHIPMENT_BY_TICKET(),
            USE_SHORT_GOODS_FORM: new USE_SHORT_GOODS_FORM(),
            USE_GET_GOODS_BY_ROOM: new USE_GET_GOODS_BY_ROOM(),
            DEFAULT_DELIVERY_TYPE: new DEFAULT_DELIVERY_TYPE(),
            DEFAULT_TRANSPORT_TYPE: new DEFAULT_TRANSPORT_TYPE(),
            DEFAULT_TABLE_PRICE: new DEFAULT_TABLE_PRICE(),
            HIDE_FIELD_TRANSSHIPMENT_BY_TRIP: new HIDE_FIELD_TRANSSHIPMENT_BY_TRIP(),
            ABLE_CHANGE_RECEIVED_DATE: new ABLE_CHANGE_RECEIVED_DATE(),
            // EMAIL_ADDRESS: new EMAIL_ADDRESS(),
            // EMAIL_TITLE_SEND: new EMAIL_TITLE_SEND(),
            // EMAIL_CONTENT_SEND: new EMAIL_CONTENT_SEND(),
        };
    }

    get() {
        let self = this;
        this.loading = true;
        window.sendParam({
            url: window.urlDBD(window.API.companyConfigList),
            type: 'GET',
            success: function (data) {
                self.loading = false;
                self.set(data.results.companyconfigurations);
            }
        });
    }

    set(configs) {
        let self = this;
        if (configs && configs.length > 0) {
            configs.forEach(function (config, c) {
                try {
                    self.CONFIG[config.name].set(config);
                } catch (e) {
                    self.CONFIG[config.name] = new BASE_CONFIG(config);
                }
            });
        }
    }

    update(options) {
        let self = this;
        let params = [];
        window.$.each(this.CONFIG, function (key, value) {
            let data = {
                active: value.activeUpdate,
                value: value.getValueUpdate(),
                label: value.label,
                name: value.name
            }
            if (value.id) {
                data.id = value.id;
            }
            params.push(data);
        });
        if (options && typeof options.before === 'function') {
            options.before();
        }
        window.sendJson({
            url: window.urlDBD(window.API.companyConfigUpdate),
            data: params,
            success: function (data) {
                if (options && typeof options.success === 'function') {
                    options.success(data);
                }
            },
            functionIfError(err) {
                if (options && typeof options.error === 'function') {
                    options.error(err);
                }
            }
        });
    }
}

export class BASE_CONFIG {
    constructor(props) {
        this.value = null;
        this.valueUpdate = null;
        this.active = false;
        this.activeUpdate = _.cloneDeep(this.active);
        this.name = null;
        this.label = '';
        this.display = false;
        this.set(props);
        this.dom = 'checkbox';
    }

    set(props) {
        this.id = props && props.id ? props.id : null;
        this.setValue(props ? props.value : null);
        this.active = props && props.active ? props.active : false;
        this.activeUpdate = _.cloneDeep(this.active);
        this.name = props && props.name ? props.name : null;
        if (!this.label && props && props.label) {
            this.label = props.label;
        }

        this.display = props && props.display ? props.display : false;
    }

    setValue(value) {
        try {
            this.value = JSON.parse(value);
            this.valueUpdate = JSON.parse(value);
        }catch (e) {
            this.value = value;
            this.valueUpdate = value;
        }

    }
    getValueUpdate(){
        return typeof this.valueUpdate==="boolean"||typeof this.valueUpdate==="string"?this.valueUpdate:JSON.stringify(this.valueUpdate);
    }

    getValue() {
        return this.value
    }
}

export class DELIVERY_TICKET extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Giao vé';
        this.name = 'DELIVERY_TICKET';
    }
}

export class ONLY_PRINT_TICKET_WHEN_BOUGTH extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Chỉ in vé sau khi thanh toán';
        this.name = 'ONLY_PRINT_TICKET_WHEN_BOUGTH';
    }
}

export class LOCK_ACTION_AFTER_DEPARTED extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Cho phép thao tác sau khi xuất bến';
        this.name = 'LOCK_ACTION_AFTER_DEPARTED';
    }
}

export class COMPANY_NAME extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Tên doanh nghiệp đầy đủ ';
        this.name = 'COMPANY_NAME';
    }
}

export class COMPANY_CALL_NAME extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Tên Doanh Nghiệp Viết Tắt hoặc thường gọi';
        this.name = 'COMPANY_CALL_NAME';
    }
}

export class BUSINESSREGISTRATION extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Upload đăng ký kinh doanh';
        this.name = 'BUSINESSREGISTRATION';
    }
}

export class SMSBRANDNAME extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='SMS brandname mong muốn';
        this.name = 'SMSBRANDNAME';
    }
}

export class SAMPLE_SIGNATURE extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Chữ ký mẫu ( lưu dạng ảnh)';
        this.name = 'SAMPLE_SIGNATURE';
    }
}

export class NAMEOWNER extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Tên chủ doanh nghiệp (Người đứng tên hợp đồng điện tử)';
        this.name = 'NAMEOWNER';
    }
}

export class PHONENUMBER_OWNER extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Số điện thoại';
        this.name = 'PHONENUMBER_OWNER';
    }
}

export class EMAIL_OWNER extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Email';
        this.name = 'EMAIL_OWNER';
    }
}

export class TAX_CODE extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Mã số thuế';
        this.name = 'TAX_CODE';
    }
}

export class ADDRESS_CONTRACT_OWNER extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Địa chỉ';
        this.name = 'ADDRESS_CONTRACT_OWNER';
    }
}

export class ACCOUNT_BANK_INFO_OWNER extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Thông tin tài khoản';
        this.name = 'ACCOUNT_BANK_INFO_OWNER';
    }
}

export class POSITION_OWNER extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Chức vụ';
        this.name = 'POSITION_OWNER';
    }
}

export class SELECT_ROOM_AFTER_LOGIN extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Chọn phòng vé sau khi đăng nhập';
        this.name = 'SELECT_ROOM_AFTER_LOGIN';
    }
}

export class SURCHARGE_CATEGORY extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='danh mục phụ thu';
        this.name = 'SURCHARGE_CATEGORY';
    }
}

// bỏ ko dùng nữa
// export class USE_NEWEST_INTERFACE extends BASE_CONFIG {
//     constructor(props) {
//         super(props);
//         this.label='Sử dụng giao diện mới nhất';
//         this.name = 'USE_NEWEST_INTERFACE';
//     }
// }

export class USE_MANAGE_PERMISSION_SELL_TICKET extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Sử dụng chức năng phân quyền theo tuyến';
        this.name = 'USE_MANAGE_PERMISSION_SELL_TICKET';
    }
}

export class DISPLAY_NEW_LIST_TRIP extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Hiển thị danh sách chuyến mới';
        this.name = 'DISPLAY_NEW_LIST_TRIP';
    }
}
export class DISPLAY_TIME_AT_FIRST_POINT extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Hiển thị giờ tại điểm xuất phát';
        this.name = 'DISPLAY_TIME_AT_FIRST_POINT';
    }
}
export class DISPLAY_TRIP_CODE extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Hiển thị mã chuyến';
        this.name = 'DISPLAY_TRIP_CODE';
    }
}
export class DISPLAY_TRIP_DETAIL extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Hiển thị chi tiết chuyến';
        this.name = 'DISPLAY_TRIP_DETAIL';
    }
}
export class DISPLAY_WAY extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Hiển thị lối đi';
        this.name = 'DISPLAY_WAY';
    }
}
export class MERGE_TICKET_PRINT extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Gộp vé khi in';
        this.name = 'MERGE_TICKET_PRINT';
    }
}

export class SYNC_CONFIG_SELL_FORM extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.name = "SYNC_CONFIG_SELL_FORM";
        this.label = "Sử dụng cấu hình cho tất cả";
    }
}

export class CONFIRM_TRANFER extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.name = "CONFIRM_TRANFER";
        this.label = "Xác nhận chuyển khoản";
    }
}
export class AUTO_CANCEL_TICKET extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.name = "AUTO_CANCEL_TICKET";
        this.label = "Sử dụng tự động hủy vé";
    }
}

export class EDIT_AGENCY_AFTER_PAID extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.name = "EDIT_AGENCY_AFTER_PAID";
        this.label = "Mở gán đại lý vé đã thanh toán";
    }
}

export class AUTO_SENT_SMS extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.name = "AUTO_SENT_SMS";
        this.label = "Mặc định gửi tin nhắn sau khi đặt vé";
    }
}

export class TIME_START_GET_DATA extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.name = "TIME_START_GET_DATA";
        this.label = "Thời gian bắt đầu lấy dữ liệu";
    }
}

export class HIDE_PENDING_TICKET_WHEN_PAY extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.name = "HIDE_PENDING_TICKET_WHEN_PAY";
        this.label = "Ẩn nút chờ xử lý với vé đã thanh toán";
    }
}

export class SHOWNUMBERSOLDSEAT extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.name = "SHOWNUMBERSOLDSEAT";
        this.label = "Hiện thị số ghế đã bán trong danh sách chuyến";
    }
}

export class TRANSHIPMENT_BY_DRIVER extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.setValue(props&&props.value ? props.value : true);
        this.name = "TRANSHIPMENT_BY_DRIVER";
        this.label = "Chỉ hiển thị thông tin khách trung chuyển cho tài xế được phân";
    }
}

export class NUMBER_OF_PRINT_TICKET extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.dom = 'text';
        this.label='Số lần được phép in vé';
        this.name = 'NUMBER_OF_PRINT_TICKET';
    }
}
export class TIME_CANCEL_TICKET_CONFIRM_TRANSFER_BEFORE_START_TRIP extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.dom = 'text';
        this.name = "TIME_CANCEL_TICKET_CONFIRM_TRANSFER_BEFORE_START_TRIP";
        this.label = "Huỷ vé chờ xác nhận chuyển khoản trước giờ xe xuất bến (phút)";
    }

    setValue(value) {
        let minute = (value/60000)||0
        let valueUpdate = parseInt(minute);
        this.value = value;
        this.valueUpdate = valueUpdate;
    }

    getValueUpdate(){
        return JSON.stringify(parseInt(this.valueUpdate)*60000);
    }
}

export class EMAIL_ADDRESS extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.dom = 'text-tag';
        this.name = "EMAIL_ADDRESS";
        this.label = "Địa chỉ email";
    }
}

export class EMAIL_TITLE_SEND extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.dom = 'text';
        this.name = "EMAIL_TITLE_SEND";
        this.label = "Tiêu đề email";
    }
}

export class EMAIL_CONTENT_SEND extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.dom = 'text';
        this.domType = 'textarea';
        this.name = "EMAIL_CONTENT_SEND";
        this.label = "Nội dung email";
    }
}

export class SIZE_WEIGHT_CONVERSION extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Công thức chuyển đổi kích thước - trọng lượng';
        this.name = 'SIZE_WEIGHT_CONVERSION';
        this.dom = 'text';
        this.options = [
            {key: '${width}: Chiều rộng', value: '{width}'},
            {key: '${length}: Chiều dài', value: '{length}'},
            {key: '${height}: Chiều cao', value: '{height}'},
        ];
        this.note = '${width}: Chiều rộng,  ${length} : Chiều dài,  ${height} : Chiều cao';
    }
}

export class HIDE_FIELD_IN_GOOD_IN_TRIP extends BASE_CONFIG {
    constructor(props) {
        super(props);
        if(!props||!props.value) {
            this.setValue([]);
        };
        this.label = 'Ẩn trường trong hàng hóa';
        this.dom = 'select';
        this.multiple = true;
        this.name = 'HIDE_FIELD_IN_GOOD_IN_TRIP';
        this.options = [
            {
                label: 'Số lượng',
                value: 'quantity'
            },
            {
                label: 'Cước',
                value: 'totalPrice'
            },
            {
                label: 'Đã thu',
                value: 'paid'
            },
            {
                label: 'Chưa thu',
                value: 'unpaid'
            },
            {
                label: 'Người gửi',
                value: 'senderPhone'
            },
            {
                label: 'Người nhận',
                value: 'receiverPhone'
            },
            {
                label: 'NV nhận',
                value: 'updateUserInfoTimeline'
            },
            {
                label: 'Điểm đến',
                value: 'pickUpPoint'
            },
            {
                label: 'Điểm xuống',
                value: 'dropOffPoint'
            },
            {
                label: 'Ghi chú',
                value: 'notice'
            }
        ]
    }
}
export class ABLE_CHANGE_RECEIVED_DATE extends BASE_CONFIG {
    constructor(props) {
        super(props);
        if(!props||!props.value) {
            this.setValue([]);
        };

        this.label = 'Cho phép thay đổi ngày nhận hàng'
        this.name = 'ABLE_CHANGE_RECEIVED_DATE'
        this.valueUpdate = false
    }
}

export class USE_SHORT_GOODS_FORM extends BASE_CONFIG {
    constructor(props) {
        super(props);
        if(!props||!props.value) {
            this.setValue([]);
        };

        this.label = 'Sử dụng form hàng hoá thu gọn'
        this.name = 'USE_SHORT_GOODS_FORM'
        this.valueUpdate = false
    }
}

export class USE_GET_GOODS_BY_ROOM extends BASE_CONFIG {
    constructor(props) {
        super(props);
        if(!props||!props.value) {
            this.setValue([]);
        };

        this.label = 'Sử dụng lọc hàng hóa theo văn phòng'
        this.name = 'USE_GET_GOODS_BY_ROOM'
        this.valueUpdate = false
    }
}

export class DEFAULT_DELIVERY_TYPE extends BASE_CONFIG {
    constructor(props) {
        super(props);
        if(!props||!props.value) {
            this.setValue([]);
        };

        this.label = 'Chọn hình thức nhận hàng mặc định'
        this.name = 'DEFAULT_DELIVERY_TYPE'
        this.multiple = false
        this.options = CollectionAnDeliveryType.properties();

    }
}

export class DEFAULT_TRANSPORT_TYPE extends BASE_CONFIG {
    constructor(props) {
        super(props);
        if(!props||!props.value) {
            this.setValue([]);
        };

        this.label = 'Chọn gói chuyển phát mặc định'
        this.name = 'DEFAULT_TRANSPORT_TYPE'
        this.multiple = false
    }
}

export class DEFAULT_TABLE_PRICE extends BASE_CONFIG {
    constructor(props) {
        super(props);
        if(!props||!props.value) {
            this.setValue([]);
        };

        this.label = 'Chọn bảng giá mặc định'
        this.name = 'DEFAULT_TABLE_PRICE'
        // let goodsFees = new GoodsFees()
        // this.options = goodsFees.getList()
    }
}


export class HIDE_FIELD_TRANSSHIPMENT_BY_TICKET extends BASE_CONFIG {
    constructor(props) {
        super(props);
        if(!props||!props.value) {
            this.setValue([]);
        };
        this.label = 'Ẩn trường trong trung chuyển theo vé';
        this.dom = 'select';
        this.multiple = true;
        this.name = 'HIDE_FIELD_TRANSSHIPMENT_BY_TICKET';
        this.options = [
            {
                label: 'Khách hàng',
                value: 'fullName'
            },
            {
                label: 'Ngày đặt',
                value: 'createDate'
            },
            {
                label: 'DS ghế',
                value: 'seatId'
            },
            {
                label: 'Địa chỉ đón khách',
                value: 'pointUpAddress'
            },
            {
                label: 'Tài xế đón khách',
                value: 'pointUpDriver'
            },
            {
                label: 'Địa chỉ trả khách',
                value: 'pointDownAddress'
            },
            {
                label: 'Tài xế trả khách',
                value: 'pointDownDriver'
            },
            {
                label: 'Thời gian khởi hành',
                value: 'createdDateInt'
            },
            {
                label: 'Nhân viên',
                value: 'seller'
            },
            {
                label: 'Giá/Trạng thái vé',
                value: 'status'
            },
            {
                label: 'Ghi chú',
                value: 'note'
            },
            {
                label: 'Khu vực đón khách',
                value: 'pointUpArea'
            },
            {
                label: 'Khu vực trả khách',
                value: 'pointDownArea'
            },
            {
                label: 'BKS',
                value: 'numberPlate'
            },
        ]
    }
}

export class HIDE_FIELD_TRANSSHIPMENT_BY_TRIP extends BASE_CONFIG {
    constructor(props) {
        super(props);
        if(!props||!props.value) {
            this.setValue([]);
        };
        this.label = 'Ẩn trường trong trung chuyển theo chuyến';
        this.dom = 'select';
        this.multiple = true;
        this.name = 'HIDE_FIELD_TRANSSHIPMENT_BY_TRIP';
        this.options = [
            {
                label: 'Mã vé',
                value: 'ticketCode'
            },
            {
                label: 'Hành khách',
                value: 'fullName'
            },
            {
                label: 'SDT',
                value: 'phoneNumber'
            },
            {
                label: 'DS ghế',
                value: 'seat'
            },
            {
                label: 'Điểm lên',
                value: 'pointUp'
            },
            {
                label: 'Điểm trả',
                value: 'pointDown'
            },
            {
                label: 'Tài xế đón',
                value: 'pointUpDriver'
            },
            {
                label: 'Tài xế trả',
                value: 'pointDownDriver'
            },
            {
                label: 'Giờ đặt',
                value: 'createdDate'
            },
            {
                label: 'Thành tiền',
                value: 'agencyPrice'
            },
            {
                label: 'Người đặt',
                value: 'seller'
            },
            {
                label: 'Ghi chú',
                value: 'note'
            },
            {
                label: 'Khu vực đón khách',
                value: 'pointUpArea'
            },
            {
                label: 'Khu vực trả khách',
                value: 'pointDownArea'
            },
            {
                label: 'BKS',
                value: 'numberPlate'
            },
        ]
    }
}

export class HIDE_FIELD_IN_SELL_TICKET extends BASE_CONFIG {
    constructor(props) {
        super(props);
        if(!props||!props.value){
            this.setValue([])
        }
        this.label = 'Ẩn trường trong bán vé 02';
        this.dom = 'select';
        this.name = "HIDE_FIELD_IN_SELL_TICKET";
        this.multiple = true;
        this.options = [
            {
                label : 'Phụ thu',
                value : 'phu_thu'
            },
            {
                label : 'Ngày sinh',
                value : 'ngay_sinh'
            },
            {
                label : 'Email',
                value : 'email'
            },
            {
                label : 'Đại lý',
                value : 'dai_ly'
            },
            {
                label : 'Ghi chú',
                value : 'ghi_chu'
            },
            {
                label : 'Mã liên kết',
                value : 'ma_lien_ket'
            },
            {
                label : 'Cơ sở ưu đãi',
                value : 'co_so_uu_dai'
            },
            {
                label : 'Xuất vé điện tử',
                value : 'xuat_ve_dien_tu'
            },
            {
                label : 'Đặt hộ',
                value : 'dat_ho'
            },
        ]
    }
}

export class HIDE_FIELD_IN_MANAGE_GOOD extends BASE_CONFIG {
    constructor(props) {
        super(props);
        if(!props||!props.value){
            this.setValue([])
        }
        this.label = 'Ẩn trường trong quản lý đơn hàng';
        this.dom = 'select';
        this.name = "HIDE_FIELD_IN_MANAGE_GOOD";
        this.multiple = true;
        this.options = [
            {
                label : 'Tên hàng',
                value : 'ten_hang'
            },
            {
                label : 'Giá trị hàng',
                value : 'gia_tri_hang'
            },
            {
                label : 'Khối lượng',
                value : 'khoi_luong'
            },
            {
                label : 'Số lượng',
                value : 'so_luong'
            },
            {
                label : 'Gói',
                value : 'goi'
            },
            {
                label : 'Giá cước',
                value : 'gia_cuoc'
            },
            {
                label : 'Phụ phí',
                value : 'phu_phi'
            },
            {
                label : 'Tên cước',
                value : 'ten_cuoc'
            },
            {
                label : 'Ảnh',
                value : 'anh'
            },
        ]
    }
}

export class VALID_GOODS_COLLECTION_METHODS extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label = 'Các phương thức nhận hàng';
        this.dom = 'select';
        this.multiple = true;
        this.options = CollectionAnDeliveryType.properties();
    }
}

export class VALID_GOODS_DELIVERY_METHODS extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.dom = 'select';
        this.label = 'Các phương thức giao hàng';
        this.multiple = true;
        this.options = CollectionAnDeliveryType.properties();
    }
}
export class DEFAULT_PAYMENT_TYPE extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Hình thức thanh toán mặc định';
        this.name = 'DEFAULT_PAYMENT_TYPE';
        this.dom = 'select';
        this.multiple = false;
        this.options = PaymentType.properties();
    }
}

export class DEFAULT_ITEM extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Khoản mục mặc định';
        this.name = 'DEFAULT_ITEM';
        this.dom = 'select';
        this.multiple = false;
    }
}

export class DEBIT_DRIVER_ITEM extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Khoản mục gán nợ tài xế mặc định';
        this.name = 'DEBIT_DRIVER_ITEM';
        this.dom = 'select';
        this.multiple = false;
    }
}

export class REVENUE_DRIVER_ITEM extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Khoản mục thu chi mặc định';
        this.name = 'REVENUE_DRIVER_ITEM';
        this.dom = 'select';
        this.multiple = false;
    }
}

export class DISPLAY_SEAT_MODE extends BASE_CONFIG {
    constructor(props) {
        super(props);
        this.label='Hiển thị thông tin ghế';
        this.name = 'DISPLAY_SEAT_MODE';
        this.dom = 'select';
        this.multiple = false;
        this.options = DisplaySeatMode.properties();
    }
}
