import lang from '../Lang/index';
import store from '../store';
import filter from '../Helper/Filters';
import {ListPlatform} from './platform';

export class TripStatus {
    constructor(status) {
        this.status = status!==undefined&&status!==null&&!isNaN(status)&&status<4&&status>=0?status:0;
    }

    label (){
        return TripStatus.listStatus()[this.status].label;
    }

    static listStatus (){
        return {
            0: {
                label: 'Đã hủy',
                value: 0
            },
            1: {
                label: 'Chưa xuất bến',
                value: 1
            },
            2: {
                label: 'Đã xuất bến',
                value: 2
            },
            3: {
                label: 'Đã kết thúc',
                value: 3
            }
        };
    }

}

export class TripType {
    static Enum(){
        return {
            NORMAL : 1,//mặc định
            CONTRACT : 2,//hơp đồng
            TAXI : 3,// taxi
            CONTRACT_WITHOUT_TICKET : 4,// đặt toàn bộ xe
        }
    }
}
export class TripTransshipmentType {
    static Enum(){
        return {
            PICK_UP : 1,//Đón
            DROP_OFF : 2,//Trả
        }
    }
}

export class TripLogType {
    constructor(logType){
        this.type = !isNaN(logType)&&logType<9&&logType>=0?logType:0;
    }
    label (){
        return TripLogType.properties()[this.type].label;
    }
    static properties(){
        return {
            1: {
                label : 'Tạo chuyến',
            },
            2: {
                label : 'Cập nhật chuyến'
            },
            3: {
                label : 'Xuất bến'
            },
            4: {
                label : 'Mở xuất bến'
            },
            5: {
                label : 'Hủy chuyến'
            },
            6: {
                label : 'Khôi phục'
            },
            7: {
                label : 'Tự động xuất bến'
            },
            8: {
                label : 'In phơi'
            }
        };
    }
}
export class TripLog {
    constructor(log){
        this.field = log.field;
        this.value = log.value;
    }

    label(){
        return lang.t(this.field);
    }
    toString(){
        return lang.t(this.field)+ ": " + this.getValueString();
    }
    getValueString(){
        let str = '';
        switch (this.field) {
            case 'tripStatus' :
                str = new TripStatus(this.value).label();
                break;
            case 'startTime':
                str = filter.customDate(this.value,'#hhhh#:#mm#',true);
                break;
            case 'drivers':
                let nameDri = [];
                $.each(this.value,function (key,value) {
                    nameDri.push(value.fullName);
                });
                str = nameDri.join(', ');
                break;
            case 'assistants':
                let nameAss = [];
                $.each(this.value,function (key,value) {
                    nameAss.push(value.fullName);
                });
                str = nameAss.join(', ');
                break;
            case 'listLockTrip':
                str = new ListPlatform(this.value).toStringLabel();
                break;
            case 'vehicle':
                str = this.value && this.value.numberPlate ? this.value.numberPlate : '';
                break;
            case 'room':
                str = this.value && this.value.name ? this.value.name : '';
                break;
        }
        return str;
    }
}
export class TripLogs {
    constructor(log){
        this.date = log.createdDate;
        this.fullName = log.createdUserName||'';
        this.type = new TripLogType(log.logType);
        let logsN = [];
        $.each(log.logs,function (key,value) {
            logsN.push(new TripLog(value));
        });
        this.logs = logsN;
    }
    logsToString (){
        let str = '';
        $.each(this.logs,function (key,value) {
            str = str+'- '+value.toString()+'</br>';
        });
        return str;
    }
}
export class Trip {
    constructor(trip){
        this.id = trip.tripId||null;
        this.startTime = trip.startTime||0;
        this.logs = [];
    }

    setLogs (_logs){
        let newLogs = [];
        $.each(_logs?_logs:[],function (key,value) {
            newLogs.push(new TripLogs(value));
        });
        this.logs = newLogs;
    }

    getLogs (success,error){
        let self = this;
        if(!this.id){
            return false;
        }
        store.commit('LOAD','TRIP_GET_LOGS');
        window.sendJson({
            url : window.urlDBD(window.API.tripLogs),
            data : {
                id : self.id
            },
            success :function (result) {
                self.setLogs(result.results.logs);
                store.commit('UNLOAD','TRIP_GET_LOGS');
                if(typeof success == "function"){
                    success(result);
                }
            },
            functionIfError : function (err) {
                store.commit('UNLOAD','TRIP_GET_LOGS');
                if(typeof error == "function"){
                    error(err);
                }
            }
        });
    }
}
