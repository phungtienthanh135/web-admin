import {Banking} from "./banking";

export class AuthorizeSellTicketTimeType{
    constructor(prop){
        this.value = prop||0;
    }
    label(){
        return AuthorizeSellTicketTimeType.properties()[this.value].label;
    }
    static properties (){
        return {
            0 : {
                label:'Vô thời hạn'
            },
            1 : {
                label:'Có thời hạn'
            }
        };
    }
}

export class AuthorizeSellTicketTime {
    constructor(prop){
        this.type = new AuthorizeSellTicketTimeType(prop&&!isNaN(prop.type)?prop.type:null);
        this.startDate = prop&&prop.startDate? new Date(prop.startDate).customFormat('#DD#-#MM#-#YYYY#') : new Date().customFormat('#DD#-#MM#-#YYYY#');
        this.endDate = prop&&prop.endDate? new Date(prop.endDate).customFormat('#DD#-#MM#-#YYYY#') : new Date().customFormat('#DD#-#MM#-#YYYY#');
    }
}
export class AuthorizeSellTicket {
    constructor (){
        this.exist =  false;
        this.acceptTerms =  false; // đồng ý điểu khoản
        this.commission = 12;
        this.time = new AuthorizeSellTicketTime();
        this.banking = new Banking();
        this.allRoute = false;
        this.routes = null;
        this.loading =false;
    }
    get (obj){
        let self = this;
        if(typeof obj==='object'&&typeof obj.beforeSend ==='function'){
            obj.beforeSend();
        }
        self.loading = true;
        window.sendJson({
            url : window.urlDBD(window.API.authorizeSellTicketGet),
            type : 'GET',
            success : function (data) {
                self.loading = false;
                self.set(data.results);

                if(typeof obj==='object'&&typeof obj.success ==='function'){
                    obj.success(data.results);
                }
            },
            functionIfError : function (data) {
                self.loading = false;
                if(typeof obj==='object'&&typeof obj.error ==='function'){
                    obj.error();
                }
            }
        });
    }

    set (prop){
        if(prop&&prop.result){
            this.exist =  prop.hasResult?true:false;
            this.acceptTerms =  prop.hasResult?true:false; // đồng ý điểu khoản
            this.commission = !isNaN(prop.result.commission)?prop.result.commission*100:12;
            let t = {
                type : prop.result.hasValidDate?1:0,
                startDate : prop.result.allowedFrom?prop.result.allowedFrom:null,
                endDate : prop.result.allowedTo?prop.result.allowedTo:null,
            };
            this.time = new AuthorizeSellTicketTime(t);
            this.banking = new Banking({method : null,infomation : {
                    bankingName : prop.result.accountInfo.accountType?prop.result.accountInfo.accountType:null,
                    bankingNumber : prop.result.accountInfo.accountNumber?prop.result.accountInfo.accountNumber:null,
                    cardHolder : prop.result.accountInfo.accountHolder?prop.result.accountInfo.accountHolder:null,
                }});
            this.allowedRoutes = prop.result&&prop.result.allowedRoutes?true:false;
            let routes = [];
            $.each(prop.result.routes||[],function (r,route) {
                routes.push({routeId : route.id,routeName : route.name});
            })
            this.routes = routes;
        }
    }

    save (obj){
        let self = this;
        if(isNaN(this.commission)||parseInt(this.commission)<10){// hoa hồng từ 10% trở lên
            notify("Hoa hồng tối thiểu là 10%, vui lòng nhập lại","error");
            return false;
        }
        if(!this.banking.infomation.bankingNumber||!this.banking.infomation.bankingName||!this.banking.infomation.cardHolder){
            notify("Vui lòng nhập đầy đủ thông tin thẻ","error");
            return false;
        }
        if(typeof obj==='object'&&typeof obj.beforeSend ==='function'){
            obj.beforeSend();
        }
        let param = {
            allow : true,
            hasValidDate : self.time.type.value?true:false,
            allowedFrom: window.func.dateToMS(self.time.startDate),
            allowedTo: window.func.dateToMS(self.time.endDate),
            commission: this.commission/100,
            accountInfo: {
                accountNumber: this.banking.infomation.bankingNumber,
                accountType: this.banking.infomation.bankingName,
                accountHolder: this.banking.infomation.cardHolder
            },
            allowedRoutes : null
        };
        if(this.routes&&this.routes.length>0){
            let rs = [];
            $.each(this.routes,function (r,route) {
                rs.push(route.routeId);
            });
            param.allowedRoutes = rs;
        }
        self.loading = true;
        window.sendJson({
            url : window.urlDBD(window.API.authorizeSellTicketCreate),
            data : param,
            success : function (data) {
                self.loading = false;
                if(typeof obj==='object'&&typeof obj.success ==='function'){
                    obj.success(data.results);
                }
            },
            functionIfError : function (data) {
                self.loading = false;
                if(typeof obj==='object'&&typeof obj.error ==='function'){
                    obj.error();
                }
            }
        });
    }
}