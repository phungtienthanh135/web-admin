import {SeatMap} from "./seatmap";
import store from "../store";

export class PricePolicy {
    constructor(props) {
        this.id = props && props.id ?props.id : null;
        this.route = props && props.route ?props.route : null;
        this.seatMap = props && props.seatMap ?props.seatMap : null;
        this.startDate = props && props.startDate ?props.startDate.toString() : new Date().customFormat('#YYYY##MM##DD#'); // yyyyMMdd
        this.endDate = props && props.endDate&&props.endDate.toString().length==8 ?props.endDate.toString() : '';// yyyyMMdd
        this.hasEndDate = props && props.endDate && props.endDate>0 ? true : false;
        this.loading = false;
    }

    setRoute (data){
        let self = this;
        if(this.route){
            self.loading= true;
            window.sendJson({
                url : window.urlDBD(window.API.routeView),
                data : {
                    id :self.route.routeId
                },
                success :function (result) {
                    self.loading= false;
                    self.route = result.results.route;
                    if(data&&typeof data.success === 'function'){
                        data.success(result);
                    }
                },
                functionIfError : function (err) {
                    self.loading= false;
                    if(data&&typeof data.error === 'function'){
                        data.error(err);
                    }
                }
            });
        }
    }

    save(data,create){
        let self = this;
        if(!this.route||!this.route.routeId){
            notify("Vui lòng chọn tuyến","error");
            return false;
        }
        if(!this.seatMap){
            notify("Vui lòng chọn sơ đồ","error");
            return false;
        }
        let params = {
            routeInfo : {
                id : this.route.routeId
            },
            startDate : this.startDate,
            endDate : this.hasEndDate?this.endDate:-1,
            seatMap : this.seatMap.getParamsForAPI()
        };
        let url = window.API.pricePolicyCreate;
        if(this.id&&!create){
            url = window.API.pricePolicyUpdate;
            params.id = this.id;
        }

        if(create){

        }

        this.loading= true;
        if(data&&typeof data.beforeSend === 'function'){
            data.beforeSend();
        }

        window.sendJson({
            url : window.urlDBD(url),
            data : params,
            success :function (result) {
                self.loading= false;
                if(data&&typeof data.success === 'function'){
                    data.success(result);
                }
            },
            functionIfError : function (err) {
                self.loading= false;
                if(data&&typeof data.error === 'function'){
                    data.error(err);
                }
            }
        });
    }
    remove(data){
        let self = this;
        if(!this.id){
            return false;
        }
        let params = {
            id : this.id
        };
        if(data&&typeof data.beforeSend === 'function'){
            data.beforeSend();
        }
        this.loading= true;
        window.sendJson({
            url : window.urlDBD(window.API.pricePolicyDelete),
            data : params,
            success :function (result) {
                self.loading= false;
                if(data&&typeof data.success === 'function'){
                    data.success(result);
                }
            },
            functionIfError : function (err) {
                self.loading= false;
                if(data&&typeof data.error === 'function'){
                    data.error(err);
                }
            }
        });
    }
}

export class PricePolicies {
    constructor (props){
        this.keyword = props&&props.keyword ? props.keyword : '';
        this.route = props&&props.route ? props.route : null;
        this.seatMap = props&&props.seatMap ? props.seatMap : null;
        this.date = props&&props.date ? props.date : '';
        this.page = props&&props.page ? props.page : 0;
        this.count = props&&props.count ? props.count : 30;
        this.list = [];
        this.loading = false;
        this.loadMore = true;
    }

    getList (data){ // number page : tăng hoặc giảm page
        let self = this;
        if(!data||isNaN(data.page)){
            self.page=0;
        }else{
            self.page+= parseInt(data.page);
        }

        let params = {
            page : self.page,
            count : self.count
        };
        if(self.keyword){
            params.name = self.keyword;
        }
        if(self.route){
            params.routeId = self.route.routeId;
        }
        if(self.seatMap){
            params.seatMapId = self.seatMap.id;
        }
        if(self.date){
            params.date = self.date;
        }
        self.loading = true;
        self.loadMore = false;
        setTimeout(function () {
            window.sendJson({
                url : window.urlDBD(window.API.pricePolicyList),
                data : params,
                success : function (data) {
                    self.loadMore = true;
                    if(data.results.pricePolicies.length<self.count){
                        self.loadMore = false;
                    }
                    self.loading = false;
                    self.setListBeforeGet(data.results.pricePolicies);
                },
                functionIfError : function (data) {
                    self.loading = false;
                }
            });
        },data&&data.delay?data.delay:0);
    }

    setListBeforeGet (data){
        let self = this;
        if(this.page<=0){
            this.list = [];
        }
        $.each(data,function (v,item) {
            item.route = {
                routeId : item.routeInfo.id,
                routeName : item.routeInfo.name
            };
            item.seatMap = new SeatMap(item.seatMap);
            self.list.push(new PricePolicy(item));
        });
    }
}