
export class Report {
    constructor(props) {
        this.id = props&&props.id ? props.id : '';
        this.name = props&&props.name ? props.name : '';
        this.description = props&&props.description ? props.description : '';
        this.path = props&&props.path ? props.path : '';
        this.permittedUsers = props&&props.permittedUsers ? props.permittedUsers : null;
        this.page = props&&props.page ? props.page : 0;
        this.count = props&&props.count ? props.count : 30;
        this.loading = false;
    }

    sendEditReport (listPermittedUser,reportEdit,call) {
        let self = this;
        self.loading = true;
        let x = []
        if(listPermittedUser) {
            for (let i = 0; i < listPermittedUser.length; i++)
            {
                let a = {}
                a.id = listPermittedUser[i].id;
                a.userName = listPermittedUser[i].userName;
                a.fullName = listPermittedUser[i].fullName;
                a.phoneNumber = listPermittedUser[i].phoneNumber;
                a.email = listPermittedUser[i].email;
                a.avatar = listPermittedUser[i].avatar;
                a.licenceLevel = listPermittedUser[i].licenceLevel;
                a.licenceCode = listPermittedUser[i].licenceCode;
                a.licenceExpired = listPermittedUser[i].licenceExpired;
                a.birthday = listPermittedUser[i].birthday;
                a.office = listPermittedUser[i].office ? listPermittedUser[i].office : null;
                x.push(a);
            }
        }
        let data = {
            id: reportEdit.id,
            path : reportEdit.path,
            permittedUsers: x,
            name : reportEdit.name,
            description : reportEdit.description
        };
        sendJson({
            url : window.urlDBD(window.API.listManageReport),
            type: 'post',
            data : data,
            success : function (data) {
                self.loading = false;
                /*call();*/
                /* self.setListBeforeGet(data.results.listReport);*/
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }

    sendDeleteReport (report,call) {
        let self = this;
        self.loading = true;
        sendParam({
            url : window.urlDBD(window.API.listManageReport)+'?id='+report.id,
            type: 'DELETE',
            success : function (data) {
                self.loading = false;
                call();
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }


    sendAddReport (listPermittedUser,call){
        let self = this;
        self.loading = true;
        let x = []
        if(listPermittedUser) {
            for (let i = 0; i < listPermittedUser.length; i++)
            {
                let a = {}
                a.id = listPermittedUser[i].id;
                a.userName = listPermittedUser[i].userName;
                a.fullName = listPermittedUser[i].fullName;
                a.phoneNumber = listPermittedUser[i].phoneNumber;
                a.email = listPermittedUser[i].email;
                a.avatar = listPermittedUser[i].avatar;
                a.licenceLevel = listPermittedUser[i].licenceLevel;
                a.licenceCode = listPermittedUser[i].licenceCode;
                a.licenceExpired = listPermittedUser[i].licenceExpired;
                a.birthday = listPermittedUser[i].birthday;
                a.office = listPermittedUser[i].office ? listPermittedUser[i].office : null;
                x.push(a);
            }
        }
        let data = {
            path : self.url,
            permittedUsers: x,
            name : self.name,
            description : self.description
        };
        sendJson({
            url : window.urlDBD(window.API.listManageReport),
            type: 'post',
            data : data,
            success : function (data) {
                self.loading = false;
                call();
                /* self.setListBeforeGet(data.results.listReport);*/
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }
}

export class ListReport {
    constructor (props){
        this.page = props&&props.page ? props.page : 0;
        this.permittedUserId = props&&props.permittedUserId ? props.permittedUserId : '';
        this.count = props&&props.count ? props.count : 30;
        this.description = props&&props.description ? props.description : '';
        this.name = props&&props.name ? props.name : '';
        this.list = [];
        this.loading = false;
    }

    getList (options){
        let self = this;
        self.loading = true;
        let params = {};
        if(window.userInfo.userInfo.userType !== 7 && options&&options.id){
            params.permittedUserId = options.id;
        }
        sendParam({
            url : window.urlDBD(window.API.listManageReport),
            type: 'get',
            data : params,
            success : function (data) {
                self.loading = false;
                self.setListBeforeGet(data.results.rs);
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }
    getListFiler (id){
        let self = this;
        self.loading = true;
        let params = {};
        if(id){
            params.permittedUserId = id;
        }
        sendParam({
            url : window.urlDBD(window.API.listManageReport),
            type: 'get',
            data : params,
            success : function (data) {
                self.loading = false;
                self.setListBeforeGet(data.results.rs);
            },
            functionIfError : function (data) {
                self.loading = false;
            }
        });
    }



    setListBeforeGet (data){
        let self = this;
        if(this.page<=0){
            this.list = [];
        }
        $.each(data,function (v,item) {
            self.list.push(new Report(item));
        });
    }
}

