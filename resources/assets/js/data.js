window.localData = {
    goodsStatus : {
        0 : "Đã hủy",
        1 : "Mới tạo",
        2 : "Đã gom",
        3 : "Đã lên xe",
        4 : "Đã xuống xe",
        5 : "Đã trả"
    },
    ticketStatus : {
        0 : 'Đã hủy',
        1 : 'Trống',
        2 : 'Giữ chỗ có thời hạn',
        3 :'Đã thanh toán',
        4 : 'Đã lên xe',
        5 : 'Đã xuống xe',
        6 : '',
        7 : 'Giữ chỗ ưu tiên',
    },
};
window.NO_SEAT = 'NO SEAT';
// UNKNOWN(100), Vui lòng kiểm tra kết nối mạng và thử lại
// NULL_OR_EMPTY(101), Dữ liệu không thể trống
// MISSING(102), Hãy cung cấp đủ thông tin
// INVALID(103), Dữ liệu không hợp lệ
// ALREADY_EXIST(104), Dữ liệu đã tồn tại
// NOT_EXIST(105), Dữ liệu không tồn tại
// ERROR_SAVE(106),
//     NOT_HAVE_PERMISSION(107), Bạn KHÔNG  có quyền để thực hiện chức năng này
// TOKEN_EXPIRED(108), Phiên đăng nhập hết hạn vui lòng đăng nhập lại
// DUPLICATE(109), Dữ liệu bị trùng
// IMAGE_NULL(110), Ảnh không thể trống
// PRICE_NOT_MATCH(111), Giá không hợp lệ
// EXCEED_DEBT_AGENCY(112), Hết hạn mức công nợ
// SEAT_BOOKED(113), Vị trí bạn ngồi đã có người đặt trước
// COMPANY_LOCKED(114), Nhà xe đã bị khóa
// ROUTE_LOCKED(115), Tuyến đã bị khóa, vui lòng liên hệ quản lý
// TRIP_LOCKED(116), Chuyến đã bị khóa, vui lòng liên hệ quản lý
// TRIP_STATUS_INVALID(117), Trạng thái của chuyến xe không hợp lệ
// BOOK_ONLINE_LOCKED(118), Chức năng đặt vé online đã bị khóa
// TICKET_EXIST(119), Vé đã tồn tại
// POINT_ORDER_INVALID(120), Thứ tự của điểm không hợp lệ
// POINT_DO_NOT_SELL_TICKET(121), Điểm dừng này không bán vé
// PASSWORD_NOT_MATCH(122), Mật khẩu không chính xác
// EXCEED_VERIFY_CODE(123) Đã tồn tại mã xác minh
window.ErrorMessage = {
    USR_ERROR_000000000 : 'Phiên làm việc của bạn đã hết hạn, vui lòng đăng nhập lại',
    USR_ERROR_000000001 : 'Bạn không có quyền',

    GOODS_NULL : 'mã vận đơn hoặc mã gom ko tồn tại',
    GOODS_NAME_NULL_OR_EMPTY :'tên hàng trống',
    GOODS_USER_NULL_OR_EMPTY : '',
    GOODS_NAME_EXISTED :'tên hàng trùng',
    PRICE_NULL_OR_EMPTY : 'thiếu thông tin giá',
    GOODS_PRICE_INVALID : 'tổng tiền, tiền đã trả và chưa trả sai',
    GOODS_CATEGORY_NULL_OR_EMPTY : 'loại hàng không tồn tại',
    GOODS_ID_NULL : 'thiếu Mã hàng',
    GOODS_INFORMATION_MISSING : 'thiếu thông tin khi nhập hàng',
    GOODS_ID_INVALID : '',
    GOODS_CODE_INVALID :'',
    GOODS_LIST_INVALID :'danh sách mã vận đơn hoặc mã gom sai',
    GOODS_PACKAGE_CODE :'danh sách mã vận đơn hoặc mã gom sai',
    TRIP_NOT_START :'vui lòng chọn chuyến đã bắt đầu chạy',
    TC_000001 : 'Chuyến đã hoàn thành hoặc bị hủy',

    TICKET_000045 : "Hết hạn mức công nợ đại lý",
    100 : 'Vui lòng kiểm tra kết nối mạng và thử lại',
    101 : 'Dữ liệu không thể trống',
    102 :'Hãy cung cấp đủ thông tin',
    103 :'Dữ liệu không hợp lệ',
    104 :'Dữ liệu đã tồn tại',
    105 :'Dữ liệu không tồn tại',
    106 : '',
    107 :'Bạn KHÔNG  có quyền để thực hiện chức năng này',
    108 :'Phiên đăng nhập hết hạn vui lòng đăng nhập lại',
    109 :'Dữ liệu bị trùng',
    110 :'Ảnh không thể trống',
    111 :'Giá không hợp lệ',
    112 :'Hết hạn mức công nợ',
    113 :'Vị trí bạn ngồi đã có người đặt trước',
    114 :'Nhà xe đã bị khóa',
    115 :'Tuyến đã bị khóa, vui lòng liên hệ quản lý',
    116 :'Chuyến đã bị khóa, vui lòng liên hệ quản lý',
    117 :'Trạng thái của chuyến xe không hợp lệ',
    118 :'Chức năng đặt vé online đã bị khóa',
    119 :'Vé đã tồn tại',
    120 :'Thứ tự của điểm không hợp lệ',
    121 :'Điểm dừng này không bán vé',
    122 :'Mật khẩu không chính xác',
    123 :'Đã tồn tại mã xác minh',
    124 :'Điểm bán vé không hỗ trợ đón trả tận nhà. Vui lòng chọn điểm đón trả khách',
    128 :'Chuyến này đã hết giờ giữ chỗ, vui lòng thanh toán',
    417 :'Đã hết số lần in vé'
};

window.configShow = {
    printTicket : {
        name : 'Cấu hình in vé',
        sample : {},//dữ liệu mẫu 1 vé,
        style : '',
        obj :{
            logo : {
                name : 'logo',
                label : '',
                show : true,
                style : ''
            },
            ticketCode : {
                name : 'Mã',
                style : '',
                show : true
            },
            routeName : {
                name : 'Tuyến',
                style : '',
                show : true
            },
            fullName : {
                name : 'Họ Tên',
                style : '',
                show : true
            },
            phoneNumber : {
                name : 'Số điện thoại',
                style : '',
                show : true
            },
            listSeatId : {
                name : 'Ghế',
                style : '',
                show : true
            },
            startTime : {
                name : 'Khởi hành',
                style : '',
                show : true
            },
            agencyPrice : {
                name : 'Giá vé',
                style : '',
                show : true
            },
            pickUp : {
                name : 'Lên',
                style : '',
                show : true
            },
            dropOff : {
                name : 'Xuống',
                style : '',
                show : true
            }
        }
    },
    printBill : {
        name : 'Cấu hình in phiếu thu',
        sampleData : {},
        obj : {
            logo : {
                name : 'Logo',
                label : '',
                show : true,
                styles : []
            },
            companyName : {
                name : 'Tên công ty',
                label : '',
                show : true,
                styles : []
            },
            companyAddress : {
                name : 'Địa chỉ công ty',
                label : '',
                show : true,
                styles : []
            },
            title : {
                name : 'Tiêu đề',
                label : 'PHIẾU THU',
                show : true,
                styles : []
            },
            sender : {
                name : 'Tên người nộp',
                label : '',
                show : true,
                styles : []
            },
            senderPhone : {
                name : 'SDT người nộp',
                label : '',
                show : true,
                styles : []
            },
            receiver : {
                name : 'Tên người nhận',
                label : '',
                show : true,
                styles : []
            },
            receiverPhone : {
                name : 'Tên người nhận',
                label : '',
                show : true,
                styles : []
            },
            amount : {
                name : 'Số tiền',
                label : '',
                show : true,
                styles : []
            },
            reason : {
                name : 'Lí do thu tiền',
                label : '',
                show : true,
                styles : []
            },
            note : {
                name : 'Ghi chú',
                label : '',
                show : true,
                styles : []
            },
            date : {
                name : 'Ngày tháng',
                label : '',
                show : true,
                styles : []
            }
        }
    }
};
window.cssStyle = {
    'width' : {
        attr : 'width',
        name : 'Chiều rộng',
        tag : 'input',
        placeholder : 'px ,rem ...'
    },
    'line-height' : {
        attr : 'line-height',
        name : 'Độ cao dòng',
        tag : 'input',
        placeholder : 'px ,rem ...'
    },
    'height' : {
        attr : 'height',
        name : 'Chiều cao',
        tag : 'input',
        placeholder : 'px , rem ...'
    },
    'padding' : {
        attr : 'padding',
        name : 'padding',
        tag : 'input',
        placeholder : 'px , rem ...'
    },
    'padding-left' : {
        attr : 'padding-left',
        name : 'Căn lề trái',
        tag : 'input',
        placeholder : 'px , rem ...'
    },
    'padding-right' : {
        attr : 'padding-right',
        name : 'Căn lề phải',
        tag : 'input',
        placeholder : 'px , rem ...'
    },
    'padding-top' : {
        attr : 'padding-top',
        name : 'Căn lề trên',
        tag : 'input',
        placeholder : 'px , rem ...'
    },
    'padding-bottom' : {
        attr : 'padding-bottom',
        name : 'Căn lề dưới',
        tag : 'input',
        placeholder : 'px , rem ...'
    },
    'text-align' : {
        attr : 'text-align',
        name: 'căn lề',
        tag : 'select',
        options : {
            left : 'Trái',
            center : "Giữa",
            right : "Phải"
        }
    },
    'float': {
        attr : 'float',
        name : 'Vị trí',
        tag : 'select',
        options : {
            left : 'Trái',
            right : 'Phải'
        }
    },
    'font-weight': {
        attr : 'font-weight',
        name : 'Độ đậm chữ',
        tag : 'select',
        options : {
            bold : 'Đậm',
            normal : 'Bình thường',
            '300' : '3',
            '600' : '6',
            '900' : '9',
        }
    },
    'font-size' :  {
        attr : 'font-size',
        name : 'Cỡ chữ',
        tag : 'select',
        options : {
            '0.8rem' : '8',
            '1rem' : '10',
            '1.2rem' : '12',
            '1.4rem' : '14',
            '1.6rem' : '16',
            '1.8rem' : '18',
            '2.0rem' : '20',
            '2.4rem' : '24',
            '2.8rem' : '28',
            '3.6rem' : '36',
            '4.8rem' : '48'
        }
    },
    'color' : {
        attr : 'color',
        name : 'Màu chữ',
        tag : 'color',
        placeholder : ''
    },
    'background' : {
        attr : 'background',
        name : 'Màu nền',
        tag : 'color',
        placeholder : ''
    },
    'text-align' : {
        attr : 'text-align',
        name : 'Căn lề chữ',
        tag : 'select',
        options : {
            'left' : 'trái',
            'center' : 'giữa',
            'right' : 'phải',
        }
    },
    'font-family' : {
        attr : 'font-family',
        name : 'font chữ',
        tag : 'select',
        options : {
            'Arial' : 'Arial',
            'Helvetica' : 'Helvetica',
            'sans-serif' : 'sans-serif',
            'Times New Roman' : 'Times New Roman',
            'Times' : 'Times',
            'Fira Sans Extra Condensed' : 'Fira Sans Extra Condensed',
        }
    },
};
window.DisplaySetting = {
    PRINT_TICKET :'PRINT_TICKET',
    PRINT_TICKET_TEMP :'PRINT_TICKET_TEMP',
    PRINT_LIST_TICKET :'PRINT_LIST_TICKET',
    PRINT_TICKET_CONTRACT :'PRINT_TICKET_CONTRACT',
    PRINT_LIST_GOODS :'PRINT_LIST_GOODS',
    PRINT_LIST_PAYMENT :'PRINT_LIST_PAYMENT',
    PRINT_LIST_PAYMENT_CHI :'PRINT_LIST_PAYMENT_CHI',
    PRINT_STATISTICAL_GOODS_EMPLOYEE :'PRINT_STATISTICAL_GOODS_EMPLOYEE',
    PRINT_SEAT_MAP :'PRINT_SEAT_MAP',
    PRINT_SEAT_MAP_HEADER :'PRINT_SEAT_MAP_HEADER',
    PRINT_SEAT_MAP_FOOTER :'PRINT_SEAT_MAP_FOOTER',
    PRINT_GOODS_TEM :'PRINT_GOODS_TEM',
    PRINT_GOODS :'PRINT_GOODS',
    PRINT_FREIGHT_ORDER: 'PRINT_FREIGHT_ORDER',
    REPORT_DETAIL: 'REPORT_DETAIL',
    EMAIL_TEMPLATE: 'EMAIL_TEMPLATE',
    REPORT_TRIP: 'REPORT_TRIP',
    TICKET_EMAIL_TEMPLATE: 'TICKET_EMAIL_TEMPLATE'
}
window.StateCode = {
    '+84' : '0',
    '+62' : '0',
    '+1' : '0',
    '+856' : '0',
    '+855' : '0',
    '+66' : '0',
    '+86' : '0',
    '+65' : '0',
}
