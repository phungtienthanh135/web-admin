//CONFIG ở vue/index.php
window.urlDBD = function (path) {
    if(typeof path === "string"){
        return CONFIG.url+path;
    }
    return CONFIG.url;
};
window.urlRP = function (path) {
    if(typeof path === "string"){
        return CONFIG.urlReport+path;
    }
    return CONFIG.urlReport;
};
window.urlCF = function (path) {
    if(typeof path === "string"){
        return CONFIG.urlCloudFunction+path;
    }
    return CONFIG.urlCloudFunction;
}

window.sendJson = function (dataReq) {
    if (typeof dataReq.headers === 'undefined') {
        dataReq.headers={DOBODY6969 : userInfo.token.tokenKey};
    }
    if(dataReq.type===undefined){
        dataReq.type="post";
    }
    dataReq.contentType = "application/json; charset=utf-8";
    if (typeof dataReq.data === "object") {
        dataReq.data = JSON.stringify(dataReq.data);
    }
    if(typeof dataReq.dataType==="undefined"){
        dataReq.dataType = "json";
    }
    if (typeof dataReq.error !== "function") {
        dataReq.error = function (result) {
            try{
                var err;
                if(typeof dataReq.functionIfError === "function"){
                    err = dataReq.functionIfError(JSON.parse(result.responseText));
                }
                if(result &&result.responseText){
                    err = JSON.parse(result.responseText);
                    console.log(err);
                    if(err.results.error.code == 108){
                        window.location.replace(window.location.origin + '/dang-xuat?check=true');
                    }
                    var errMsg = err.results.error.code||err.results.error.message;//message
                    console.log(errMsg)
                    if (typeof ErrorMessage[errMsg] !== "undefined") {
                        notify(ErrorMessage[errMsg],"error");
                    } else {
                        notify("có lỗi " + errMsg,"error");
                    }
                }else{
                    notify("có lỗi xảy ra! vui lòng xem lại kết nối mạng!","error");
                }
            }catch(e){
                console.log(e);
                if(typeof dataReq.functionIfError === "function"){
                    dataReq.functionIfError(result);
                }
                notify("có lỗi xảy ra! vui lòng xem lại kết nối mạng!","error");
            }
        }
    }
    return $.ajax(dataReq);
};
window.sendParam = function (dataReq) {
    if (typeof dataReq.headers === 'undefined') {
        dataReq.headers={DOBODY6969 : userInfo.token.tokenKey};
    }
    if(dataReq.type===undefined){
        dataReq.type="post";
    }
    if(typeof dataReq.dataType==="undefined"){
        dataReq.dataType = "json";
    }
    if (typeof dataReq.error !== "function") {
        dataReq.error = function (result) {
            try {
                var err;
                if(typeof dataReq.functionIfError === "function"){
                    err = dataReq.functionIfError(result);
                }
                if(typeof result!=='undefined' &&typeof result.responseText!=='undefined'){
                    err = JSON.parse(result.responseText);
                    if(err.results.error.code == 108){
                        window.location.replace(window.location.origin + '/dang-xuat?check=true');
                    }
                    var errMsg = err.results.error.code||err.results.error.message;//message
                    if (typeof ErrorMessage[errMsg] !== "undefined") {
                        notify(ErrorMessage[errMsg],"error");
                    } else {
                        notify("có lỗi " + errMsg,"error");
                    }
                }else{
                    notify("có lỗi xảy ra! vui lòng xem lại kết nối mạng!","error");
                }
            }catch (e){
                console.log(e);
                if(typeof dataReq.functionIfError === "function"){
                    dataReq.functionIfError(result);
                }
                notify("có lỗi xảy ra! vui lòng xem lại kết nối mạng!","error");
            }

        }
    }
    return $.ajax(dataReq);
};

window.API = {
    //agency
    agencySearchByKeyword :'user/agency/search_by_keyword',
    searchByPhone : 'company_customer/find_info_by_phone_number',
    searchByPhoneNumber : 'company_customer/searchByPhoneNumber',
    quickSearch : 'ticket/searchByKeyword',

    companyCustomerList : 'company_customer/getList',
    companyCustomerUpdateDistrust : 'company_customer/updateDistrust',

    //point
    pointList : 'point/getList',
    pointCreate : 'point/create',
    pointUpdate : 'point/update',
    pointDelete : 'point/delete',
    pointDetail : 'point/get',
    pointProvinceDistrict : 'point/get_province_district',

    // route
    routeList : 'route/getList',
    routeListAll : 'route/get-all',
    routeCreate : 'route/create',
    routeUpdate : 'route/update',
    routeDelete : 'route/delete',
    routeOrder : 'route/order',
    routeView : 'route/view',

    // future route ticket price
    createFuturePrice: 'plan/create-ticket-price',
    getFuturePrice: 'plan/list-ticket-price',
    deleteFuturePrice: 'plan/delete-ticket-price',

    // goods
    goodsList : 'goods/v2/list',
    goodsListByUser : 'goods/v2/get',
    getGoodsList : 'goods/v2/report',
    goodsCreate : 'goods/v2/create',
    goodsCreateMultiple : '/goods/v2/create-multi',
    goodsUpdate : 'goods/v2/update',
    revertGoodsStatus : 'goods/v2/delete-trip-info',
    goodsDelete :'goods/v2/delete',
    goodsReportByUser : 'goods/v2/report-by-user',
    goodsSearchByPhone : 'goods/v2/search-by-phone',

    // packageGoods
    createGoodsPackage : 'goods/v2/create-package',
    updateGoodsPackage : 'goods/v2/update-package-goods',
    arriveGoodsPackage : 'goods/v2/package-arrive',
    goodsHistory : '/goods/logs',
    // goodsType
    goodsTypeList :'goods_type/list',
    goodsTypeCreate : 'goods_type/create',
    goodsTypeUpdate : 'goods_type/update',
    goodsTypeDelete : 'goods_type/delete',

    // trip
    tripList : 'web/find-schedule-like-for-admin-new',
    tripViewDetailSeatMap : 'trip/view_detail_seat_map_in_trip',
    tripViewDetail : 'trip/view-detail',
    tripView : 'trip/view',
    statisticByPointAll : 'statistictrip/get',//lấy chốt chuyến
    statisticByPointCreateAll : 'statistictrip/createByAdmin',
    tripOpenStart :'trip/open',
    tripStart : 'trip/start-trip',
    tripGetInDate : 'trip/getInDate',
    tripGetInDates : 'trip/getInDates',
    tripRefreshFirebase : 'trip/refresh',
    tripGetPricePolicy : 'trip/getPricePolicy',
    tripManage : 'trip/manage',
    tripLogs : 'trip/logs',
    tripPrint : 'trip/print',
    econtractCreate : 'econtract/create',
    updateContractRepresentation: 'trip/updateContractRepresentation',
    tripOptimize : 'ticket/trip-optimize',
    contractGetNumber : 'econtract/megabiz/contractnum',
    contractExport : 'econtract/export',
    getTripInDateRange: 'trip/getInDates',

    // bill
    billList : 'bill/getList',
    billCreate : 'bill/create',
    billCreateMultiple : 'bill/createMultiple',
    billUpdate : 'bill/update',
    billDelete : 'bill/delete',

    // bill prepare
    createBillPrepare: 'prepare/bill/create',
    getListBillPrepare: 'prepare/bill/getList',
    deleteBillPrepare: 'prepare/bill/delete',
    updateBillPrepare: 'prepare/bill/update',

    // service
    getListService: 'service/get-list',
    createService: 'service/create',
    updateService: 'service/update',
    deleteService: 'service/update',
    getListOrderService: 'service/order/get-list',
    updateOrderService: 'service/order/update',
    deleteOrderService: 'service/order/delete',
    createOrderService: 'service/order/create',

    // item : khoản mục
    itemList : 'items/getlist',
    itemCreate : 'items/create',
    itemUpdate : 'items/update',
    itemDelete : 'items/delete',

    // user
    userList : 'web_admin/getlist',
    userListJson : 'user/getlist',
    userDetail : 'web_admin/getuserinfo',
    userChangePassword : 'user/changepassword',
    userLock : 'user/lock',
    userDelete : 'user/delete',
    userStaffRegister : 'user/staff-register',
    userUpdateInfo : 'user/updateinfo',
    userV2StaffCreate : 'v2/user/staff/create',
    driverLicenseExpired: 'user/license/get-list-expired',
    userBirthday: 'user/birthday',
    userLoginHistory: 'user/logging-history',
    driversFree: 'user/getFreeDriver',

    accessCodeCreate : '/access-code/create',
    accessCodeUpdate : '/access-code/update',
    accessCodeList : '/access-code',
    // seatMap
    seatMapList : 'seatmap/getList',
    seatMapCreate : 'seatmap/create',
    seatMapUpdate : 'seatmap/update',
    seatMapDelete : 'seatmap/delete',
    // agency
    agencyListWebAdmin : 'web_admin/get-list-agency',
    agencyCreate : 'user/create_agency',
    agencyUpdate : 'user/update-agency',
    // Level Agency
    levelAgencyList : 'level_agency/get_list',
    levelAgencyCreate : 'level_agency/create',
    levelAgencyUpdate : 'level_agency/update',
    levelAgencyDelete : 'level_agency/delete',

    // Policy Agency
    policyAgencyList : 'agency_price_policy/get_list',
    policyAgencyCreate : 'agency_price_policy/create',
    policyAgencyUpdate : 'agency_price_policy/update',
    // Price Policy
    pricePolicyList : 'price_policy/getList',
    pricePolicyCreate : 'price_policy/create',
    pricePolicyUpdate : 'price_policy/update',
    pricePolicyDelete : 'price_policy/delete',

    // vehicle
    vehicleView: 'vehicle/view',
    vehicleList : 'vehicle/getList',
    vehicleCreate : 'vehicle/create',
    vehicleUpdate : 'vehicle/update',
    vehicleDelete : 'vehicle/delete',
    vehicleGetListExpired : 'vehicle/get-list-expired',
    vehiclesFree: 'vehicle/getFreeVehicle',
    // vehicle owner
    vehicleOwnerGetList: 'vehicle_owner/getList',
    vehicleOwnerCreate: 'vehicle_owner/create',
    vehicleOwnerDelete: 'vehicle_owner/delete',
    vehicleOwnerUpdate: 'vehicle_owner/update',
    // vehicleType
    vehicleTypeList : 'vehicletype/getlist',

    // vehicle utility
    getVehicleUtility: 'utility/vehicle/get-list',
    
    //planForTrip (schedule)
    planForTripCreate : 'planfortrip/create',
    planForTripList : 'planfortrip/getList',
    // planForTripSearch : 'trip/admin-search',
    planForTripSearch : 'planfortrip/search',
    planForTripSearchToArrange : 'planfortrip/searchToArrange',
    planForTripUpdate : 'planfortrip/update',
    planForTripDelete : 'planfortrip/delete',

    // notification
    NotificationPopup : 'popup/get-list',
    NotificationPopupCreate : 'popup/create',
    NotificationPopupUpdate : 'popup/update',
    NotificationPopupDelete : 'popup/delete',

    // promotion
    promotionList :'promotion/getlist',
    promotionCreate :'promotion/create',
    promotionUpdate :'promotion/update',
    promotionDelete :'promotion/delete',
    promotionCheck :'promotion/check',

    //additionPrice
    AdditionPriceList : 'addition_price/get-list',
    AdditionPriceCreate : 'addition_price/create',
    AdditionPriceUpdate : 'addition_price/update',
    AdditionPriceDelete : 'addition_price/delete',

    //ticket
    ticketCreate :'general_ticket/create',
    getTicketTransferConfirm : 'ticket/getListTransferConfirm',
    getListTicketInvoice : 'ticket/getListTicketInvoice',
    sendInvoice : 'ticket/sendInvoice',
    // ticketUpdate :'general_ticket/updateInfo',

    ticketBook :'ticket/book',
    ticketGetForTrip :'ticket/getForAdmin',
    ticketUpdate : 'ticket/update',
    ticketTransfer : 'ticket/transfer',
    ticketSwap : 'ticket/swap',
    ticketCancel :'ticket/cancel',
    ticketHistorySeat : 'ticket/viewLogBySeat',
    ticketHistory : 'ticket/viewLog',
    ticketUpdateCheckedOnline : 'ticket/updateCheckedTicketOnline',
    ticketCheckInDay : 'ticket/check-in-day',
    ticketPrintHistory: 'ticket/print',
    ticketPrintTemHistory: 'ticket/printTem',
    ticketTransshipmentList: 'ticket/getListTransshipment',
    ticketTransshipment: 'ticket/transshipment',
    ticketDeliveryList: 'ticket/getDelivery',
    ticketDeliveryUpdate: 'ticket/updateDelivery',
    ticketSendSMS: '/ticket/sendSMS',
    goodSendSMS: '/sms/send',
    goodV2SendSms: '/goods/v2/send-sms',
    getListPermissionSellTicket: 'user_by_route/getList',
    createPermissionSellTicket: 'user_by_route/create',
    deletePermissionSellTicket: 'user_by_route/delete',

    merchantList : 'merchant',
    emailHtml2pfdSend: 'email/html2pdf/send',

    cancelTicketPolicyList : 'cancelticketpolicy/getList',
    cancelTicketPolicyCreate : 'cancelticketpolicy/create',
    cancelTicketPolicyUpdate : 'cancelticketpolicy/update',
    cancelTicketPolicyDelete : 'cancelticketpolicy/delete',

    countTicketByPoint: '/avq_api/trip/count-ticket-by-point',

    // telecom
    telecomList :'branch_telecom/get-list',
    telecomCreate :'branch_telecom/create',
    telecomUpdate :'branch_telecom/update',
    telecomDelete :'branch_telecom/delete',
    telecomCompanyList :'telecom/list',
    telecomCompanyCreate :'telecom/create',

    //report
    reportByTrips : 'report/getByTrip',
    reportByTransshipment : '/report/getTransshipmentReport',
    reportByTripsCreate : 'trip/createReport',
    reportByListTripsCreate : 'trip/createListReport', // chốt thu chi chuyến chốt nhiều chuyến
    reportByListTripsGet: '/report/trip',
    reportCashBook : 'accumulated_cash/get',
    reportFuel : 'consumption',
    reportAgency : 'report/getReconciliationAgency',
    reportDetail : 'v2/report/create-report-detail',
    reportOnlinePayment : 'report/onlinePayment',
    reportMessage : 'report/sms',
    reportSMSDetail: 'smsInfo/getList',
    reportCustomer : 'report/customer',
    reportCashByUser: 'v2/report/cash-report',
    revenueMonthly : 'report/revenueSummary',
    listManageReport : 'custom_report',
    displaySettingCreate : 'display-settings/create',
    displaySettingGet : 'display-settings/get',
    systemSettingUpdate : 'company/update',
    systemSettingGet : 'company/view',
    reportGoodsByEmployees : 'goods/v3/report/user',

    permissionList : 'role/getList',
    permissionCreate : 'role/create',
    permissionUpdate : 'role/update',
    permissionDelete : 'role/delete',
    permissionGrantForUser : 'role/grantForUser',

    servicePackageGetFunction : 'service_package/getServiceFunction',

    epaySmsMegaV  : 'ePay/getSMSMegaV',
    epayCreateTransactionSMSMegaV : 'ePay/createTransactionSMSMegaV',
    epaySmsVnpay : 'sms/transaction',

    //custumer type

    customerTypeCreate: 'company_customer_type/create',
    customerTypeUpdate: 'company_customer_type/update',
    customerTypeList: 'company_customer_type/getList',
    customerTypeDelete: 'company_customer_type/delete',

    // cai dat hien thi tin nhan

    smsConfigDisplayList: 'message/getList',
    smsConfigDisplayCreate: 'message/create',
    smsConfigDisplayUpdate: 'message/update',
    smsConfigDisplayDelete: 'message/delete',


    agencyView : '/agency/view',

    authorizeSellTicketGet : '/company/sanve/policy',
    authorizeSellTicketCreate : '/company/sanve',

    //hrm
    hrmRewardOrPunishmentList : 'hrm/reward_punishment/get-list',
    hrmRewardOrPunishmentCreate : 'hrm/reward_punishment/create',
    hrmRewardOrPunishmentUpdate : 'hrm/reward_punishment/update',
    hrmRewardOrPunishmentDelete : 'hrm/reward_punishment/delete',

    hrmHealthCheckList : 'hrm/health-certification/get-list',
    hrmHealthCheckCreate : 'hrm/health-certification/create',
    hrmHealthCheckUpdate : 'hrm/health-certification/update',
    hrmHealthCheckDelete : 'hrm/health-certification/delete',

    hrmTrafficViolationList : 'hrm/traffic-violation/get-list', // vi phạm giao thông
    hrmTrafficViolationCreate : 'hrm/traffic-violation/create',
    hrmTrafficViolationUpdate : 'hrm/traffic-violation/update',
    hrmTrafficViolationDelete : 'hrm/traffic-violation/delete',

    hrmTrainingCourseList : 'hrm/training-course/get-list',
    hrmTrainingCourseCreate : 'hrm/training-course/create',
    hrmTrainingCourseUpdate : 'hrm/training-course/update',
    hrmTrainingCourseDelete : 'hrm/training-course/delete',

    hrmWorkProcessList : 'hrm/work-process/get-list',
    hrmWorkProcessCreate : 'hrm/work-process/create',
    hrmWorkProcessUpdate : 'hrm/work-process/update',
    hrmWorkProcessDelete : 'hrm/work-process/delete',

    vnpQrPay : 'vnpay/qr/pay',

    //company region
    regionCreate: 'company-region/create',
    regionGet: 'company-region/get',
    regionDelete: 'company-region/delete',
    regionUpdate: 'company-region/update',

    // region price
    regionPriceCreate: 'company-region-price/create',
    regionPriceDelete: 'company-region-price/delete',
    regionPriceUpdate: 'company-region-price/update',
    //Work Shift
    workShiftCreate: "shift/create",
    workShiftGet: "shift/get",
    workShiftDelete: "shift/delete",
    workShiftUpdate: "shift/update",

    //WorkTime
    scheduleOfStaffGet: "schedule_of_staff/getList",
    scheduleOfStaffCreate: "schedule_of_staff/create",
    scheduleOfStaffCreateMulti: "schedule_of_staff/createMulti",
    scheduleOfStaffUpdate: "schedule_of_staff/createMulti",
    scheduleOfStaffDelete: "schedule_of_staff/delete",

    timeReceiveDeliveringTicketsCreate: "time_receive_delivering_tickets/create",
    timeReceiveDeliveringTicketsCreateMultiple: "time_receive_delivering_tickets/createMulti",
    timeReceiveDeliveringTicketsList: "time_receive_delivering_tickets/getList",
    timeReceiveDeliveringTicketsDelete: "time_receive_delivering_tickets/delete",
    timeReceiveDeliveringTicketsDeleteMultiple: "time_receive_delivering_tickets/deleteMulti",
    timeReceiveDeliveringTicketsUpdate: "time_receive_delivering_tickets/update",
    timeReceiveDeliveringTicketsUpdateMultiple: "time_receive_delivering_tickets/updateMulti",

    timeTransshipmentWarning: '/config_transshipment_time/get',
    //Delivery
    deliveryGet: "ticket/getDelivery",
    ticketDeliveryUpdate: "ticket/updateDelivery",


    surchargeCategoryList: "surcharge_category/getList",
    surchargeCategoryCreate: "surcharge_category/create",
    surchargeCategoryUpdate: "surcharge_category/update",
    surchargeCategoryDelete: "surcharge_category/delete",

    companyConfigList: 'company_config/getList',
    companyConfigUpdate: 'company_config/update',

    ///goods v3 server -> v2 client
    goodsV2TransportType: 'goods/v3/transport-type',
    goodsV2ExtraFee: 'goods/v3/extra-fee',
    goodsV2Fee: 'goods/v3/fee',
    goodsV2Collection: 'goods/v3/collection',
    goodsV2CollectionAssign: '/goods/v3/collection/assign',
    goodsV2Manage: 'goods/v3/manage',
    goodsV2Deliver: 'goods/v3/deliver',
    goodsV2List: 'goods/v3/get',
    goodsV2Create: 'goods/v3/create',
    goodsV2Update: 'goods/v3/update',
    goodsV2SearchCustomer: 'goods/v3/customer',
    goodsV2Log: 'goods/v3/log',

    //GOODs_warehouse
    goodsWareHouseCreate: 'goods_warehouse/create',
    goodsWareHouseUpdate: 'goods_warehouse/update',
    goodsWareHouseGet: 'goods_warehouse/getList',
    goodsWareHouseDelete: 'goods_warehouse/delete',

    goodsV2GetList: 'goods/v3/get',

    // Config limit invoice
    deleteConfigLimitInvoice: 'config_limit_invoice/delete',
    updateConfigLimitInvoice: 'config_limit_invoice/update',
    createConfigLimitInvoice: 'config_limit_invoice/create',
    getConfigLimitInvoiceList: 'config_limit_invoice/getList',

    //Ingredient report
    getIngredientReport: 'gasoline/getList',
    createIngredientItem: 'gasoline/create',
    deleteIngredientItem: 'gasoline/delete',
    updateIngredientItem: 'gasoline/update',

    //E-wallet report
    getEWalletReport: 'etransaction/etransaction-report',
    getEContractDetail: 'econtract/getList',
    getTicketPackageList: 'ticket_package/getList',
    buyTicketPackage: 'ticket_package/buy',

    // get ticket list by phone number
    getTicketByPhone: 'ticket/getListTicketByPhoneNumber',

    // send message by list phone
    sendSMSbyListPhone: 'sms/send',
    sendSmsCampaign: 'sms/create-campaign-ads',

    // ADMIN notify
    getListAdminNotify: 'systemNotification',
    
    // APP device
    createDevice: 'app-device/create',
    getDeviceList: 'app-device/get-list',
    deleteDevice: 'app-device/delete',
};
