export default {
    url : function (val) {
        return window.location.origin + '/' + val;
    },
    lang : function (key) {

    },
    caps: function(value) {
        value = value.toString();
        return value.charAt(0).toUpperCase() + value.slice(1)
    },
    uppercase: function(v) {
        return v.toUpperCase();
    },
    phoneNumber:function (str) {
        if(typeof str==='undefined' || str === null || str.toString().length<7 ){
            return str;
        }
        var str1 = str.substr(-3, 3);
        var str2 = str.substr(-6, 3);
        var str3 = str.substring(0, str.length - 6);
        return str3 + ' ' + str2 + ' ' + str1;
    },
//     ONEPAY(1),
// PAYOO(2),
//     VNPAY(3),
//     FPT_SMS(4),
//     VMG_SMS(5),
//     TRUE_MONEY(6),
//     MOMO(7),
//     INCOM(8),
//     MEGAPAY(9),
//     VIETTEL_SMS(10),
//     TRI_ANH(11),
//     ZALO_PAY(12),
//     VNPAY_QR(13),
//     ITS_SMS(14),
//     FPT_INVOICE(15),
//     VNPAY_SPOS(16)
    checkPaymentGate : function(status){
        if (status) {
            switch (status) {
                case 1: {
                    return 'ONEPAY';
                }
                case 2: {
                    return 'PAYOO';
                }
                case 3: {
                    return 'VNPAY';
                }
                case 4: {
                    return 'FPT_SMS';
                }
                case 5: {
                    return 'VMG_SMS';
                }
                case 6: {
                    return 'TRUE_MONEY';
                }
                case 7: {
                    return 'MOMO';
                }
                case 8: {
                    return 'INCOM';
                }
                case 9: {
                    return 'MEGAPAY';
                }
                case 10: {
                    return 'VIETTEL_SMS';
                }
                case 11: {
                    return 'TRI_ANH';
                }
                case 12: {
                    return 'ZALO_PAY';
                }
                case 13: {
                    return 'VNPAY_QR';
                }
                case 14: {
                    return 'ITS_SMS';
                }
                case 15: {
                    return 'FPT_INVOICE';
                }
                case 16: {
                    return 'VNPAY_SPOS';
                }
                default: {
                    return 'Không xác định';
                }
            }
        } else {
            return '';
        }

    },
    checkTicketStatus : function(vehicleType){
        if (vehicleType) {
            switch (vehicleType) {
                case 0: {
                    return 'Hủy';
                }
                case 1: {
                    return 'Rỗng';
                }
                case 2: {
                    return 'Đang giữ chỗ';
                }
                case 3: {
                    return 'Đã thanh toán';
                }
                case 4: {
                    return 'Đã lên xe';
                }
                case 5: {
                    return 'Đã hoàn thành';
                }
                case 6: {
                    return 'Quá giờ giữ chỗ';
                }
                case 7: {
                    return 'Giữ chỗ ưu tiên';
                }
            }
        } else {
            return '';
        }

    },
    checkVehicleType : function(vehicleType){
        if (vehicleType) {
            switch (vehicleType) {
                case 0: {
                    return 'Giường nằm';
                }
                case 1: {
                    return 'Ghế ngồi';
                }
                case 2: {
                    return 'Ghế ngồi limousine';
                }
                case 3: {
                    return 'Giường nằm limousine';
                }
                case 4: {
                    return 'Phòng VIP (cabin)';
                }
            }
        } else {
            return '';
        }

    },
    checkTransStatus : function(tripStatus){
        if (tripStatus) {
            switch (tripStatus) {
                case 1: {
                    return 'Đang thực hiện';
                }
                case 2: {
                    return 'Thành công';
                }
                case 3: {
                    return 'Thất bại';
                }
            }
        } else {
            return '';
        }

    },
    checkTripStatus : function(tripStatus){
        if (tripStatus) {
            switch (tripStatus) {
                case 1: {
                    return 'Chưa xuất bến';
                }
                case 2: {
                    return 'Đã xuất bến';
                }
                case 3: {
                    return 'Đã hoàn thành';
                }
                case 4: {
                    return 'Hủy';
                }
            }
        } else {
            return '';
        }

    },
    checkDateInWeek: function (dateString) {
        date = dateString.toString();
        var str1 = date.substr(0, 4);
        var str2 = date.substr(4,2);
        var str3 = date.substr(6,2);
        date =  str2 + '/' + str3 + '/' + str1;
        var ms = new Date(date).getTime(); // cộng timezone việt nam
        var date = new Date(ms).getDay();
        switch (date) {
            case 1: {
                return 'Thứ 2';
            }
            case 2: {
                return 'Thứ 3';
            }
            case 3: {
                return 'Thứ 4';
            }
            case 4: {
                return 'Thứ 5';
            }
            case 5: {
                return 'Thứ 6';
            }
            case 6: {
                return 'Thứ 7';
            }
            case 7: {
                return 'Chủ Nhật';
            }
            default: {
                return '';
            }
        }
    },
    customDate : function(miliseconds,formatString,is){
        if(is){// nếu true thì miliseconds là múi giờ +0 , nếu múi giờ + 0 thì thay đổi miliseconds
            miliseconds -=- new Date(miliseconds).getTimezoneOffset()*60000;
        }
        var dateFm = new Date(miliseconds);
        var YYYY,YY,MMMM,MMM,MM,M,DDDD,DDD,DD,D,hhhh,hhh,hh,h,mm,m,ss,s,ampm,AMPM,dMod,th;
        YY = ((YYYY=dateFm.getFullYear())+"").slice(-2);
        MM = (M=dateFm.getMonth()+1)<10?('0'+M):M;
        MMM = (MMMM=["January","February","March","April","May","June","July","August","September","October","November","December"][M-1]).substring(0,3);
        DD = (D=dateFm.getDate())<10?('0'+D) :D;
        DDD = (DDDD=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][dateFm.getDay()]).substring(0,3);
        th=(D>=10&&D<=20)?'th':((dMod=D%10)==1)?'st':(dMod==2)?'nd':(dMod==3)?'rd':'th';
        formatString = formatString.replace("#YYYY#",YYYY).replace("#YY#",YY).replace("#MMMM#",MMMM).replace("#MMM#",MMM).replace("#MM#",MM).replace("#M#",M).replace("#DDDD#",DDDD).replace("#DDD#",DDD).replace("#DD#",DD).replace("#D#",D).replace("#th#",th);
        h=(hhh=dateFm.getHours());
        if (h==0) h=24;
        if (h>12) h-=12;
        hh = h<10?('0'+h):h;
        hhhh = hhh<10?('0'+hhh):hhh;
        AMPM=(ampm=hhh<12?'am':'pm').toUpperCase();
        mm=(m=dateFm.getMinutes())<10?('0'+m):m;
        ss=(s=dateFm.getSeconds())<10?('0'+s):s ;
        return formatString.replace("#hhhh#",hhhh).replace("#hhh#",hhh).replace("#hh#",hh).replace("#h#",h).replace("#mm#",mm).replace("#m#",m).replace("#ss#",ss).replace("#s#",s).replace("#ampm#",ampm).replace("#AMPM#",AMPM);
    },
    strToDate : function (str,type) {
        if(typeof str==='undefined' || str.toString().length!==8||str===''||str===null){
            return str;
        }
        str = str.toString();
        var str1 = str.substr(0, 4);
        var str2 = str.substr(4,2);
        var str3 = str.substr(6,2);
        if(type){
            let rs = '';
            switch (type) {
                case 'dayName' :
                    let day = new Date(str2 + '/' + str3 + '/' + str1).getDay();
                    if(day == 0){
                        rs = "Chủ nhật";
                    }else{
                        rs = 'Thứ '+(day+1);
                    }
                    break;

                default : break
            }
            return rs;
        }
        return str3 + '-' + str2 + '-' + str1;
    },
    dayName : function(milisecond){

    },
    intToTime : function (str) {
        if(!str){
            return str;
        }
        var H = Math.floor(parseInt(str)/3600000);
        var m = Math.floor(parseInt(str)%3600000/60000);
        return (H<10? '0'+H : H)+ ':'+ (m<10? '0'+m : m);
    },
    getValueWithKey : function (key,array) {
        if(typeof array!=="object"||typeof array[key]==="undefined"){
            return "";
        }
        if(typeof array[key] === "object"){
            return array[key].toString();
        }else{
            return array[key];
        }
    },
    number:function (num) {
        let result = num;
        try {
            result =num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        }catch (e) {
        }
        return result;
    },
    cutString :function (str,numChar,startOrEnd) {
        if(str){
            str.toString();
            if(str.length>numChar){
                if(startOrEnd==='end'){
                    str=str.substr(str.length - numChar);
                }else{
                    str=str.substring(0,numChar);
                }
                str+='...';
            }
        }
        return str||'';
    },
    goodsStatus : function (status) {
        if(parseInt(status)!=="NaN"&&parseInt(status)>=0&&parseInt(status)<=5){
            return localData.goodsStatus[status];
        }else{
            return "chưa xác định";
        }
    },
    removeUnicode : function(str) {
        str= str.toLowerCase();
        str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
        str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
        str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
        str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
        str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
        str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
        str= str.replace(/đ/g,"d");
        str= str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,"-");

        str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
        str= str.replace(/^\-+|\-+$/g,"");

        return str;
    },
    differenceDate : function(date) {
        let currentDate = moment([moment().format('YYYY'), moment().format('MM') - 1, moment().format('DD')]);
        let dateNew =  moment([moment(date).format('YYYY'), moment(date).format('MM') - 1, moment(date).format('DD')]);
        return dateNew.diff(currentDate, 'days'); 
    },
    stringBr : function (str) {
        return str.replace(/\n/g, "<br />");
    },
    dateStrToMs : function(date){
        date = date.toString();
        var str1 = date.substr(0, 4);
        var str2 = date.substr(4,2);
        var str3 = date.substr(6,2);
        date =  str2 + '/' + str3 + '/' + str1;
        var ms = new Date(date).getTime(); // cộng timezone việt nam
        return ms;
    },
}