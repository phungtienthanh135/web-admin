export default {
    data : function () {
        return {
            userInfo : (window.userInfo||null),
        };
    },
    methods : {
        excel : function (element,fileName) {
            setTimeout(function () {
                let tableEP = $(element).tableExport({
                    headings: true,                    // (Boolean), display table headings (th/td elements) in the <thead>
                    footers: true,                     // (Boolean), display table footers (th/td elements) in the <tfoot>
                    formats: ["xlsx"],    // (String[]), filetypes for the export
                    filename: (fileName||'')+new Date().customFormat('#hhhh#h-#mm#p-#DD#-#MM#-#YYYY#'),// (id, String), filename for the downloaded file
                    bootstrap: false,                   // (Boolean), style buttons using bootstrap
                    exportButtons: false
                });
                let getEp = tableEP.getExportData()
                let exportData = getEp[Object.keys(getEp)[0]]['xlsx'];

                tableEP.export2file(exportData.data, exportData.mimeType, exportData.filename, exportData.fileExtension);
                tableEP.remove();
            },200);
        }
    },
}