export default {
    userInfo : userInfo,
    isAgency : function(){
        if(this.userInfo.userInfo.listAgency.length>0){
            return true;
        }
        return false;
    },
    agencyInfo : function(){
        if(!this.isAgency()){
            return null;
        }
        var self = this;
        var data = {
            id : this.userInfo.userInfo.id,
            fullName : this.userInfo.userInfo.fullName,
            debtLimit: this.userInfo.userInfo.debtLimit,
        };
        $.each(self.userInfo.userInfo.listAgency,function (a,agc) {
            if(self.userInfo.userInfo.companyId === agc.companyId){
                data.level = agc.level;
                data.debtLimit = agc.debtLimit;
            }
        });
        return data;
    },
    getUserInfo: function () {
        var data = {
            id: this.userInfo.userInfo.id,
            fullName: this.userInfo.userInfo.fullName,
            phoneNumber: this.userInfo.userInfo.phoneNumber,
            companyId: this.userInfo.userInfo.companyId
        };
        return data;
    },
    isAdmin : function(){
        if(this.userInfo.userInfo.userType==7){
            return true;
        }
        return false;
    },
    roles : {
        //resource +'_'+ operations
        ADDITION_PRICE_CREATE: false,
        ADDITION_PRICE_DELETE: false,
        ADDITION_PRICE_GET_LIST: false,
        ADDITION_PRICE_UPDATE: false,
        AGENCY_CREATE: false,
        AGENCY_DELETE: false,
        AGENCY_GET_LIST: false,
        AGENCY_UPDATE: false,
        CONFIGURATION_SYSTEM_UPDATE: false,
        CUSTOMER_GET_LIST: false,
        CUSTOMER_TYPE_CREATE: false,
        CUSTOMER_TYPE_DELETE: false,
        CUSTOMER_TYPE_GET_LIST: false,
        CUSTOMER_TYPE_UPDATE: false,
        EMPLOYEE_CREATE: false,
        EMPLOYEE_DELETE: false,
        EMPLOYEE_FOLLOW_TRIP: false,
        EMPLOYEE_GET_LIST: false,
        EMPLOYEE_GRANT_PERMISSION: false,
        EMPLOYEE_UPDATE: false,
        GOODS_COLLECT: false,
        GOODS_CREATE: false,
        GOODS_DELETE: false,
        GOODS_GET_LIST: false,
        GOODS_OUT_OF_TRIP: false,
        GOODS_PUSH_TO_TRIP: false,
        GOODS_TYPE_CREATE: false,
        GOODS_TYPE_DELETE: false,
        GOODS_TYPE_GET_LIST: false,
        GOODS_TYPE_UPDATE: false,
        GOODS_UPDATE: false,
        PERMISSION_CREATE: false,
        PERMISSION_DELETE: false,
        PERMISSION_GET_LIST: false,
        PERMISSION_UPDATE: false,
        POINT_CREATE: false,
        POINT_DELETE: false,
        POINT_GET_LIST: false,
        POINT_UPDATE: false,
        PROMOTION_CREATE: false,
        PROMOTION_DELETE: false,
        PROMOTION_GET_LIST: false,
        PROMOTION_UPDATE: false,
        RECEIPT_AND_PAYMENT_CREATE: false,
        RECEIPT_AND_PAYMENT_DELETE: false,
        RECEIPT_AND_PAYMENT_GET_LIST: false,
        RECEIPT_AND_PAYMENT_UPDATE: false,
        REPORT_DEPRECIATION_ASSET: false,
        REPORT_ONLINE_PAYMENT: false,
        REPORT_RECONCILIATION_AGENCY: false,
        REPORT_REVENUE_DETAIL: false,
        REPORT_REVENUE_BY_STAFF: false,
        REPORT_REVENUE_SUMMARY: false,
        REPORT_SUMMARY_CUSTOMER: false,
        REPORT_SUMMARY_SMS: false,
        REPORT_DRIVER_REPORT : false,
        REPORT_TRANSSHIPMENT_REPORT : false,
        REPORT_GOODS_BY_STAFF : false,
        REPORT_CUSTOMER_REPORT : false,
        REPORT_CASH_REPORT : false,
        REPORT_FUEL_CONSUMPTION_REPORT : false,
        ROUTE_CREATE: false,
        ROUTE_DELETE: false,
        ROUTE_GET_LIST: false,
        ROUTE_LOCK: false,
        ROUTE_UPDATE: false,
        SCHEDULE_CREATE: false,
        SCHEDULE_DELETE: false,
        SCHEDULE_GET_LIST: false,
        SCHEDULE_UPDATE: false,
        SEAT_MAP_CREATE: false,
        SEAT_MAP_DELETE: false,
        SEAT_MAP_GET_LIST: false,
        SEAT_MAP_UPDATE: false,
        SMS_PATTERN_CREATE: false,
        SMS_PATTERN_DELETE: false,
        SMS_PATTERN_GET_LIST: false,
        SMS_PATTERN_UPDATE: false,
        SWITCHBOARD: false,
        SYSTEM_MASTER_GRANT_SERVICE: false,
        TICKET_CREATE: false,
        TICKET_DELETE: false,
        TICKET_GET_LIST: false,
        TICKET_UPDATE: false,
        TICKET_PRINT: false,
        TICKET_PAYMENT: false,
        TICKET_EXPORT_INVOICE: false,
        TICKET_DELETE_INVOICE: false,
        TRIP_ARRANGE: false,
        TRIP_DELETE: false,
        TRIP_GET_LIST: false,
        TRIP_LOCK: false,
        TRIP_OPEN_STARTED: false,
        TRIP_START: false,
        VEHICLE_CREATE: false,
        VEHICLE_DELETE: false,
        VEHICLE_GET_LIST: false,
        VEHICLE_TYPE_CREATE: false,
        VEHICLE_TYPE_DELETE: false,
        VEHICLE_TYPE_GET_LIST: false,
        VEHICLE_TYPE_UPDATE: false,
        VEHICLE_UPDATE: false,
        DISPLAY_SETTINGS : false,
        CANCEL_TICKET_POLICY_GET_LIST : false,
        CANCEL_TICKET_POLICY_CREATE : false,
        CANCEL_TICKET_POLICY_UPDATE : false,
        CANCEL_TICKET_POLICY_DELETE : false,
        CANCEL_TICKET_POLICY_NOT_APPLY_POLICY : false,
        POP_UP_CREATE : false,
        POP_UP_UPDATE : false,
        POP_UP_DELETE : false,
        ACCESS_CODE_ACCESS_CODE_ADMIN : false,
        COMPANY_REGION_CREATE : false,
        COMPANY_REGION_UPDATE : false,
        COMPANY_REGION_DELETE : false,
        COMPANY_REGION_GET_LIST : false,
        WORK_SHIFT_CREATE : false,
        WORK_SHIFT_UPDATE : false,
        WORK_SHIFT_DELETE : false,
        WORK_SHIFT_GET_LIST : false,
        WORK_SCHEDULE_CREATE : false,
        WORK_SCHEDULE_UPDATE : false,
        WORK_SCHEDULE_DELETE : false,
        WORK_SCHEDULE_GET_LIST : false,
        TICKET_DELIVERY_GET_LIST : false,
        TICKET_DELIVERY_ASSIGN_TO_DRIVER : false,
        TICKET_DELIVERY_UPDATE : false,
        LEVEL_OF_AGENCY_GET_LIST : false,
        LEVEL_OF_AGENCY_CREATE : false,
        LEVEL_OF_AGENCY_UPDATE : false,
        LEVEL_OF_AGENCY_DELETE : false,
        AGENCY_PRICE_POLICY_GET_LIST : false,
        AGENCY_PRICE_POLICY_CREATE : false,
        AGENCY_PRICE_POLICY_UPDATE : false,
        AGENCY_PRICE_POLICY_DELETE : false,
        USER_BY_ROUTE_GET_LIST : false,
        USER_BY_ROUTE_CREATE : false,
        USER_BY_ROUTE_UPDATE : false,
        USER_BY_ROUTE_DELETE : false,
        TIME_RECEIVE_DELIVER_TICKET_GET_LIST : false,
        TIME_RECEIVE_DELIVER_TICKET_CREATE : false,
        TIME_RECEIVE_DELIVER_TICKET_UPDATE : false,
        TIME_RECEIVE_DELIVER_TICKET_DELETE : false,
    },
    rolesName : {
        operations : {
            // General operation
            CREATE : "Tạo",
            UPDATE : 'Cập nhật',
            GET_LIST : "Lấy danh sách",
            DELETE : 'Xóa',
            // For Employee
            GRANT_PERMISSION  :'Nhóm quyền',
            FOLLOW_TRIP : "Tài xế phụ xe", // follow trip
            // For ticket
            TRANSFER : 'Chuyển đổi',
            CREATE_DISCOUNTED : 'Tạo vé ưu đãi',
            // For goods
            COLLECT :'Gom',
            PUSH_TO_TRIP :'Đẩy vào chuyến',
            OUT_OF_TRIP :'Xuống khỏi chuyến',
            // For trip
            ARRANGE :'Điểu hành',
            START :'Xuất bến',
            OPEN_STARTED :"Mở xuất bến",
            LOCK :'Khóa',
            // For report
            REVENUE_DETAIL : 'Chi tiết',
            REVENUE_BY_STAFF : 'Theo nhân viên',
            REVENUE_SUMMARY : 'Doanh thu tháng',
            RECONCILIATION_AGENCY :'Đại lý',
            SUMMARY_CUSTOMER : 'Khách hàng',
            ONLINE_PAYMENT :' Thanh toán online',
            SUMMARY_SMS :' Tin nhắn',
            DEPRECIATION_ASSET : 'Khấu hao tài sản',
            // Operation for system anvui
            PAYMENT : 'Cho phép ấn thanh toán vé',
            NOT_APPLY_POLICY : 'Chỉnh sửa giá hủy vé',
            GRANT_SERVICE : 'Nhóm dịch vụ',
            PRINT : 'In',
            EXPORT_INVOICE: 'Cho phép xuất vé điện tử',
            DELETE_INVOICE: 'Cho phép hủy vé điện tử',
            ACCESS_CODE_ADMIN : 'Bảo mật đăng nhập',
            DRIVER_REPORT : 'Báo cáo tài xế',
            TRANSSHIPMENT_REPORT : 'Báo cáo trung chuyển',
            GOODS_BY_STAFF : 'Hàng hóa theo nhân viên',
            CUSTOMER_REPORT : 'Báo cáo khách hàng',
            CASH_REPORT : 'Báo cáo sổ quỹ tiền mặt',
            FUEL_CONSUMPTION_REPORT : 'Báo cáo tiêu hao nhiên liệu',
            ASSIGN_TO_DRIVER : 'Gán cho tài xế',
        },
        resources : {
            CONFIGURATION_SYSTEM : 'Cấu hình hệ thống',
            TIME_RECEIVE_DELIVER_TICKET: 'Thời gian nhận chung chuyển vé',
            POINT : 'Điểm dừng',
            ROUTE : "Tuyến",
            SCHEDULE : "Lịch",
            TRIP : "Chuyến",
            TICKET: "Vé",
            TICKET_DELIVER: "Giao vé",
            SEAT_MAP : "Sơ đồ ghế",
            VEHICLE :"Phương tiện",
            VEHICLE_TYPE:"Loại phương tiện",
            EMPLOYEE:"Nhân viên",
            PERMISSION:"Nhóm quyền",
            ADDITION_PRICE :"Chính sách giá",
            SMS_PATTERN : "Tin nhắn",
            AGENCY :'Đại lý',
            PROMOTION :'Khuyến mãi',
            REPORT :'Báo cáo',

            GOODS :'Hàng hóa',
            GOODS_TYPE : 'Loại hàng',
            ITEMS :'',
            RECEIPT_AND_PAYMENT :'Phiếu thu chi',
            CUSTOMER :'Khách hàng',
            CUSTOMER_TYPE :'Loại khách hàng',
            SWITCHBOARD : 'Tổng đài',
            SYSTEM_MASTER : 'Hệ thống',
            DISPLAY_SETTINGS : 'Cấu hình hiển thị',
            CANCEL_TICKET_POLICY : 'Chính sách hủy vé',
            POP_UP : 'Popup thông báo',
            ACCESS_CODE : 'Duyệt đăng nhập',
            COMPANY_REGION : 'Quản lý khu vực',
            WORK_SHIFT : 'Quản lý ca',
            WORK_SCHEDULE : 'Quản lý lịch làm việc của nhân viên',
            TICKET_DELIVERY : 'Giao vé',
            SURCHARGE : 'Danh mục phụ thu',
            LEVEL_OF_AGENCY : 'Cấp đại lý',
            AGENCY_PRICE_POLICY : 'Chính sách giá đại lý',
            USER_BY_ROUTE : 'Bán vé theo tuyến'
        }
    },
    pointTypes : {
        1 : 'Điểm dừng lớn',
        2 : 'Điểm dừng nhỏ',
        3 : 'Trạm thu phí',
        4 : 'Nhà hàng',
        5 : 'Điểm dừng nhanh',
        7 : 'Điểm trung chuyển',
        8 : 'Phòng vé'
    },
    pointTypeStyle :{
        0 : {
            'font-weight' : 'normal',
            color: 'rgb(35, 39, 49)',
            icon: 'fa fa-bus',
            label: 'Điểm đón tại bến'
        },
        1 : {
            'font-weight' : 'bold',
            color: '#B80DA6',
            icon: 'fa fa-home',
            label: 'Tận nhà'
        },
        2 : {
            'font-weight' : 'bold',
            color: '#3f51b5',
            icon: 'fa fa-map-marker',
            label: 'Dọc đường'
        },
        3 : {
            'font-weight' : 'bold',
            color: 'rgb(35, 39, 49)',
            icon: 'fa fa-car',
            label: 'Trung chuyển'
        },
    },
    licenceLevels : {
        B1 : 'Hạng B1',
        B2 : 'Hạng B2',
        c : 'Hạng C',
        D : 'Hạng D',
        E : 'Hạng E',
    },
    userType : {
        1 : 'Khách hàng',
        2 : 'Tài xế',
        3 : 'Phụ xe',
        4 : 'Kế toán',
        5 : 'Nhân viên hành chính',
        6 : 'Thanh tra',
        7 : 'Chủ nhà xe',
    },
    paymentTypes : {
        1 : 'Thanh toán trực tuyến (online)',
        2: 'Thanh toán trên xe',
        3 : 'Thanh toán tiền mặt tại quầy',
        6 : 'Chuyển khoản',
    },
    vehicleTypes : {
        0 : "Giường nằm",
        1 : "Ghế ngồi",
        2 : "Ghế ngồi limousine",
        3 : "Giường nằm limousine",
        4 : "Phòng VIP (cabin)",
    },
    callStatus : {
        0 : {
            name : "Chưa gọi",
            background : 'inherit',
            color : '#000'
        },
        1 : {
            name : "Phòng vé đã gọi",
            background : '#3f51b5',
            color : '#fff'
        },
        2 : {
            name : "Phòng vé gọi không nghe",
            background : '#ffb300',
            color : '#fff'
        },
        3 : {
            name : "Tài xế đã gọi",
            background : '#4caf50',
            color : '#fff'
        },
        4 : {
            name : "Tài xế gọi không nghe",
            background : '#f44336',
            color : '#fff'
        },
        5 : {
            name : "Số điện thoại không đúng",
            background : '#cc0066',
            color : '#fff'
        },
        6 : {
            name : "Đã gọi cho tài xế",
            background : '#00ccff',
            color : '#fff'
        },
        7 : {
            name : "Thuê bao không gọi được",
            background : '#b042f5',
            color : '#fff'
        },
        8 : {
            name : "Tài xế báo hủy",
            background : '#212121',
            color : '#fff'
        },
        9 : {
            name : "Đã nhận tin",
            background : '#15bfa0',
            color : '#fff'
        },
        10 : {
            name : "Đã nhận tin trung chuyển",
            background : '#c2951b',
            color : '#fff'
        }
    },
    platforms : [
        {value : 1,label : 'Admin'},
        {value : 2,label : 'Web'},
        {value : 3,label : 'IOS'},
        {value : 4,label : 'Android'},
        {value : 5,label : 'Partner'}
    ],
    ticketSample : null,
    ticketsMove : [],
    load : function (data) {
        this.$store.commit('LOAD',data);
    },
    unload : function (data) {
        this.$store.commit('UNLOAD',data);
    },
    changeLocal : function (data) {
        this.$root.$emit('changeLocal',data);
    },
    calculatePriceTicket : function (ticket) {
        // trong ticket yêu cầu có :
        // originPrice : giá của chặng trong tuyến;
        // extraPrice : giá công thêm của ghế ;
        // childrenTicketRatio : tỉ lệ giá người lớn và giá trẻ em;
        ticket.priceSeatAndExtra = ticket.originPrice + ticket.extraPrice;
        //isAdult là người lớn,
        ticket.priceAfterAdult = ticket.isAdult==false&&ticket.childrenTicketRatio ? _.cloneDeep(ticket.priceSeatAndExtra)*ticket.childrenTicketRatio : _.cloneDeep(ticket.priceSeatAndExtra);// tính giá trẻ em
        // tính hoa hồng đại lý
        ticket.priceCommission=0;
        if(ticket.policy.commission){
            let cms = ticket.policy.commission;
            if(cms.commissionType===2){
                ticket.priceCommission = ticket.priceAfterAdult * cms.commissionValue;
            }else{
                ticket.priceCommission = _.cloneDeep(cms.commissionValue);
            }
        }
        ticket.priceAfterCommission = _.cloneDeep(ticket.priceAfterAdult) - ticket.priceCommission;

        ticket.pricePromotion = 0;
        if(ticket.policy.promotion){
            let prm = ticket.policy.promotion;
            if(prm.price>-1){
                ticket.pricePromotion = _.cloneDeep(prm.price);
            }
            if(prm.percent>-1){
                ticket.pricePromotion = prm.percent * ticket.priceAfterCommission;//priceAfterAdult
            }
        }

        ticket.priceTransshipmentUp = 0;
        ticket.priceTransshipmentDown = 0;
        if(ticket.pointUp.pointType == 3){
            if(ticket.transshipmentInPoint&&ticket.transshipmentInPoint.transshipmentPrice){
                ticket.priceTransshipmentUp = _.cloneDeep(ticket.transshipmentInPoint.transshipmentPrice);
            }
        }
        if(ticket.pointDown.pointType == 3){
            if(ticket.transshipmentOffPoint&&ticket.transshipmentOffPoint.transshipmentPrice){
                ticket.priceTransshipmentDown = _.cloneDeep(ticket.transshipmentOffPoint.transshipmentPrice);
            }
        }
        ticket.priceAddition = 0;
        if(ticket.policy.addition){
            let adt = ticket.policy.addition;
            if(adt.type==1){
                ticket.priceAddition = adt.amount*adt.mode;
            }else{
                ticket.priceAddition = adt.amount*adt.mode*ticket.priceAfterCommission;//priceAfterAdult
            }
        }

        ticket.totalPrice = parseFloat(ticket.priceAfterAdult)-parseFloat(ticket.priceCommission)-parseFloat(ticket.pricePromotion)+parseFloat(ticket.priceTransshipmentUp)+parseFloat(ticket.priceTransshipmentDown)+parseFloat(ticket.priceAddition);

        return ticket;
    },
    differenceDate : function(date) {
        let currentDate = moment([moment().format('YYYY'), moment().format('MM') - 1, moment().format('DD')]);
        let dateNew =  moment([moment(date).format('YYYY'), moment(date).format('MM') - 1, moment(date).format('DD')]);
        return dateNew.diff(currentDate, 'days');
    }
}
