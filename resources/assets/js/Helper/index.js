import Mixins from './Mixins';
import Filters from './Filters';
import Prototypes from './Prototype';
import Directive from './Directive';

export default {
    create : function (Vue) {
        if(window.userInfo&&window.userInfo.userInfo.roles){
            let roles = Prototypes.roles;
            window.userInfo.userInfo.roles.forEach(function (value,key) {
                if(!value||!value.listPermission){
                    return false;
                }
                value.listPermission.forEach(function(groupPermission,gpms){
                    if(groupPermission.operations){
                        groupPermission.operations.forEach(function(permission,pms){
                            let key = groupPermission.resource +'_'+ permission;
                            roles[key] = true;
                        });
                    }else{
                        let key = groupPermission.resource;
                        roles[key] = true;
                    }
                });
            });
            Prototypes.roles = roles;
        }

        Vue.mixin(Mixins);

        Object.keys(Filters).forEach(function (key,value) {
            Vue.filter(key,Filters[key]);
        });

        Object.keys(Prototypes).forEach(function (p,pro) {
            Vue.prototype[p] = Prototypes[p];
        });

        Object.keys(Directive).forEach(function (directive,d) {
            Vue.directive(directive,Directive[directive]);
        });
    }
}
