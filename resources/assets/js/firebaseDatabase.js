import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/messaging';
import 'firebase/auth';
import 'firebase/database';

export async function firebaseInit(){
    firebase.initializeApp(CONFIG.firebase);
    // firebase.app().functions();
    
    function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
    }
    await new Promise((resolve,reject)=> {
        if(!firebase.auth.currentUser){
            // firebase.auth().signInWithEmailAndPassword(CONFIG.userNameFirebase, CONFIG.passwordFirebase)
            firebase.auth().signInWithEmailAndPassword(`connectfirebase${getRandomInt(10)}@anvui.vn`, 'Anvui@2015')
                .then(function (firebaseUser) {
                    resolve();
                })
                .catch(function (error) {
                    console.log('connect firebase error', error);
                    reject();
                });
        }
    });
    window.firebaseDatabase = firebase.database();
    window.firebaseMessage = firebase.messaging.isSupported() ? firebase.messaging() : null;
}
