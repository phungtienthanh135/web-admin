import Vue from 'vue';
import Router from 'vue-router';
// Views
import Home from './components/Home/Home.vue';

import RoomWork from './components/Point/RoomWork.vue';

import Goods from './components/Goods/Goods.vue';
import GoodsByUser from './components/Report/GoodsByUser.vue';
import GoodsCancel from './components/Goods/GoodsCancel.vue';

import Goods02 from './components/Goods02/Goods';
import InitPrice from './components/Goods02/InitPrice/InitPrice';
import Operating from './components/Goods02/Operating';
import CollectionRequest from './components/Goods02/CollectionRequest';
import TransportType from './components/Goods02/TransportType/TransportType';
import InitPriceExtraFee from './components/Goods02/InitPrice/ExtraFee';
import InitPriceGoods from './components/Goods02/InitPrice/Goods';
import InitPriceMoney from './components/Goods02/InitPrice/Money';
import ManageGoods from './components/Goods02/ManageGoods/ManageGoods';

// import Customer from './components/Customer/Customer.vue';
import CustomerType from './components/CustomerType/CustomerType.vue';

import GoodsType from './components/GoodsType/GoodsType.vue';

import AddGoodsType from './components/GoodsType/AddGoodsType.vue';
import UpdateGoodsType from './components/GoodsType/UpdateGoodsType.vue';
import Ticket06 from './components/Ticket/Ticket06/Ticket.vue';
import Ticket07 from './components/Ticket/Ticket07/Ticket.vue';
import Ticket09 from './components/Ticket/Ticket09/Ticket.vue';

import PlanForTrip from './components/PlanForTrip/PlanForTrip.vue';
import AddPlanForTrip from './components/PlanForTrip/AddPlanForTrip.vue';
import StatisticByPoint from './components/Trip/StatisticByPoint.vue';
import LevelAgency from './components/LevelAgency/LevelAgency.vue';

import Route from './components/Route/Route.vue';
import AddRoute from './components/Route/AddRoute.vue';
import OrderRoute from './components/Route/OrderRoute.vue';

import Vehicle from './components/Vehicle/Vehicle.vue';
import VehicleOwner from './components/VehicleOwner/VehicleOwner.vue';

import Point from './components/Point/Point.vue';

import Promotion from './components/Policy/Promotion/Promotion.vue';
import AdditionPrice from './components/Policy/AdditionPrice/AdditionPrice.vue';
import CancelTicketPolicy from './components/Policy/CancelTicket/CancelTicket.vue';
import PolicyAgency from './components/Policy/PolicyAgency/PolicyAgency.vue';
import PricePolicy from './components/Policy/PricePolicy/PricePolicy.vue';

import SeatMap from './components/SeatMap/SeatMap.vue';
import LockSeat from './components/SeatMap/LockSeat.vue';

import Item from './components/Item/Item.vue';

import BillCreate from './components/Bill/BillCreate.vue';
import Bill from './components/Bill/Bill.vue';
import BillInTrip from './components/Bill/BillInTrip.vue';
import BillInTrip2 from './components/Bill/BillInTrip2.vue';
import BillInTripForDriver from './components/Bill/BillInTripForDriver.vue';

import NotificationPopup from './components/Notification/Popup.vue';

import TripsDriverReport from './components/Report/TripsDriver.vue';
import TransshipmentReport from './components/Report/Transshipment.vue';
import ReportCountTripDriver from './components/Report/ReportCountTripDriver.vue';

import SystemSetting from './components/Setting/SystemSetting.vue';
import SystemInfoSetting from './components/Setting/SystemInfoSetting.vue';

import DisplaySetting from './components/Setting/DisplaySetting.vue';

import DisplaySettingBillIn from './components/DisplaySetting/BillIn.vue';
import DisplaySettingBillOut from './components/DisplaySetting/BillOut.vue';
import DisplaySettingPrintTicket from './components/DisplaySetting/Ticket.vue';
import DisplaySettingPrintHeader from './components/DisplaySetting/Header.vue';
import DisplaySettingPrintListTicket from './components/DisplaySetting/ListTicket.vue';

import PrintConfigTicket from './components/PrintConfig/Ticket.vue';
import TicketEmail from './components/PrintConfig/TicketEmail.vue';
import PrintConfigTableTickets from './components/PrintConfig/TableTickets.vue';
import PrintConfigFreightOrder from './components/PrintConfig/FreightOrder.vue';
import PrintConfigTicketContract from './components/PrintConfig/TicketContract.vue';
import PrintConfigTicketTemp from './components/PrintConfig/TicketTemp.vue';
import PrintConfigTablePayment from './components/PrintConfig/TablePayment.vue';
import PrintConfigTablePaymentChi from './components/PrintConfig/TablePaymentChi.vue';
import PrintConfigStatisticalGoodsByUser from './components/PrintConfig/StatisticalGoodsByUser.vue';
import PrintConfigSeatMap from './components/PrintConfig/PrintConfigSeatMap.vue';
import PrintConfigGoodsTem from './components/PrintConfig/GoodsTem.vue';
import PrintConfigGoods from './components/PrintConfig/Goods.vue';
import PrintConfigReportDetail from './components/PrintConfig/ReportDetail.vue';
import PrintConfigMailTemplate from './components/PrintConfig/MailTemplate.vue';
import PrintConfigReportTrip from './components/PrintConfig/ReportTrip.vue';

import CashBook from './components/Report/CashBook.vue';
import Fuel from './components/Report/Fuel.vue';
import ReportByAgency from './components/Report/ReportByAgency.vue';
import ReportDetail from './components/Report/ReportDetailPrint.vue';
import ReportDetail02 from './components/Report/ReportDetailPrint02.vue';
import ReportOnlinePayment from './components/Report/ReportOnlinePayment.vue';
import ReportMessage from './components/Report/ReportMessage.vue';
import ReportGoods from './components/Report/ReportGoods.vue';
import ReportCashByUser from './components/Report/ReportCashByUser.vue';
import ReportCashByUser02 from './components/Report/ReportCashByUser02.vue';
import ReportDataStudio from './components/Report/ReportDataStudio.vue';
import AssignTransshipmentDriverForTicket from './components/Report/AssignTransshipmentDriverForTicket.vue';
import AssignStaffDeliver from './components/Report/AssignStaffDeliver.vue';
import ReportTripPrint from './components/Report/ReportTrip.vue';
import ReportAV from './components/Report/ReportAV50.vue';


import SmsConfigDisplay from './components/SmsConfig/SmsDisplay.vue';

import GroupPermission from './components/GroupPermission/GroupPermision.vue';

import User from './components/User/User.vue';
import UserLoginHistory from './components/User/LoginHistory.vue';
import Agency from './components/Agency/Agency.vue';
import LoginSecurity from './components/User/LoginSecurity.vue';

import ManageListTrip from './components/Trip/ManageListTrip.vue';
import ManageListTripNew from './components/Trip/ManageListTripNew.vue';
import ManageListTripTable from './components/Trip/ManageListTripTable.vue';
import ManageListTripTwoRoute from './components/Trip/ManageListTripTwoRoute.vue';

import TelecomNumber from './components/Telecom/TelecomNumber.vue';

import AuthorizeSellTicket from './components/AuthorizeSellTicket/AuthorizeSellTicket.vue';

import error from './components/Template/404.vue';
import CustomerFeedbackHistory from './components/Template/CustomerFeedbackHistory.vue';

import Region from './components/Region/Region';
import RegionbyLocation from './components/Region/RegionbyLocation';
import WorkShift from './components/WorkShift/WorkShift';
import ScheduleOfStaff from './components/ScheduleOfStaff/ScheduleOfStaff';
import WarningDeliveringTickets from './components/WarningDeliveringTickets/WarningDeliveringTickets';
import AssignTicketShipping from './components/AssignTicketShipping/AssignTicketShipping';
import SurchargeCategory from './components/SurchargeCategory/SurchargeCategory';
import GoodsSetting from './components/Goods02/GoodsSetting';
import Coordination from './components/Goods02/Coordination';
import FormManagerConfirmTransfer from './components/ConfirmTransfer/FormManagerConfirmTransfer';
import ImportAndExportGoods from './components/Goods02/ImportAndExportGoods';
import ReceiveGoodsInHome from './components/Goods02/ReceiveGoodsInHome';
import Ret from './components/Report/ReportElectronicTicket';
import LimitInvoice from './components/LimitInvoice/LimitInvoice.vue';
import MenuManageReport from './components/Report/MenuManageReport.vue';
import IngredientReport from './components/Ingredient/IngredientReport.vue';
import ReportGoodsByEmployees from './components/Report/ReportGoodsByEmployees.vue';
import ManagePermissionSellTicket from './components/ManageTicket/ManagePermissionSellTicket.vue';
import ReportGood02 from './components/Goods02/ReportGood02.vue';
import ReportListPassengersRoute from './components/Ticket/Ticket07/ReportListPassengersRoute.vue';
import TripStatistic from './components/Report/TripStatistic.vue';
import ListTripCancer from './components/Trip/ListTripCancer.vue';
import SynthesisReport from './components/Report/SynthesisReport.vue';
import ReportsByVehicle from './components/Report/ReportsByVehicle.vue';
import ReportByRoute from './components/Report/ReportByRoute.vue';
import MySaleReport from './components/Report/MySaleReport.vue';
import MyBillRequest from './components/Bill/MyBillRequest.vue';
import ReportCustomer2 from './components/Report/ReportCustomer.vue';
import SanVeContrast from './components/Contrast/SanVeContrast';
import ReportContract from './components/Report/ReportContract.vue';
import ReportSynthetictTickerAndGoodByDate from './components/Report/ReportSynthetictTickerAndGoodByDate.vue';
import ReportTicketByStaffInMonth from './components/Report/ReportTicketByStaffInMonth.vue';
import ReportSyntheticInOneDay from './components/Report/ReportSyntheticInOneDay.vue';
import ReportRoomTicketByPointType from './components/Report/ReportRoomTicketByPointType.vue';
import ReportTicketOnline from './components/Report/ReportTicketOnline.vue';
import ReportIncomeByPhoneNumberCustomer from './components/Report/ReportIncomeByPhoneNumberCustomer.vue';
import ManageService from './components/Service/ManageService.vue';
import ManageSoldService from './components/Service/ManageSoldService.vue';
import ReportIncomeByDrivers from './components/Report/ReportIncomeByDrivers.vue';
import ReportDetailPassengerTicket from './components/Report/ReportDetailPassengerTicket.vue';
import ReportIncomeVehicleContract from './components/Report/ReportIncomeVehicleContract.vue';
import ReportTD1 from './components/Report/ReportTD1.vue';
import ReportTD2 from './components/Report/ReportTD2.vue';
import ManageApp from './components/AppDevice/ManageApp.vue';

import {CompanyConfig} from "./class/company_config";
Vue.use(Router);

const router = new Router({
    routes: [
        { path: '/Home', name: 'Home', component: Home },
        {path: '/room-work', name: 'RoomWork', component: RoomWork},

        { path: '/Goods', name: 'Goods', component: Goods},
        { path: '/goods-cancel', name: 'GoodsCancel', component: GoodsCancel},
        { path: '/GoodsByUser', name: 'GoodsByUser', component: GoodsByUser},

        {path: '/goods-02', name: 'Goods02', component: Goods02},
        {
            path: '/goods-02/init-price/',
            // name: 'InitPrice',
            component: InitPrice,
            children: [
                {
                    path: '',
                    name: 'InitPriceGoods',
                    component: InitPriceGoods
                },
                {
                    path: 'money',
                    name: 'InitPriceMoney',
                    component: InitPriceMoney
                },
                {
                    path: 'extra-fee',
                    name: 'InitPriceExtraFee',
                    component: InitPriceExtraFee
                }
            ]
        },
        {path: '/goods-02/operating', name: 'Operating', component: Operating},
        {path: '/goods-02/collection-request', name: 'CollectionRequest', component: CollectionRequest},
        {path: '/goods-02/transport-type', name: 'TransportType', component: TransportType},
        {path: '/goods-02/manage-goods', name: 'ManageGoods', component: ManageGoods},

        // { path: '/customer', name: 'Customer', component: Customer},
        { path: '/customerType', name: 'CustomerType', component: CustomerType},

        { path: '/goods-type', name: 'GoodsType', component: GoodsType},
        { path: '/add-goods-type', name: 'AddGoodsType', component: AddGoodsType},
        { path: '/update-goods-type/:goodsTypeId', name: 'UpdateGoodsType', component: UpdateGoodsType},

        // { path: '/ticket-version-04', redirect: { name: 'Ticket06' }},
        // { path: '/ticket-05', redirect: { name: 'Ticket06' }},
        { path: '/ticket-06', name: 'Ticket06', component: Ticket06},
        {path: '/ticket-07', name: 'Ticket07', component: Ticket07},
        {path: '/ticket-09', name: 'Ticket09', component: Ticket09},

        { path: '/plan-for-trip', name: 'PlanForTrip', component: PlanForTrip},
        { path: '/add-plan-for-trip', name: 'AddPlanForTrip', component: AddPlanForTrip},
        { path: '/statistic-by-point', name: 'StatisticByPoint', component: StatisticByPoint},
        { path: '/level-agency', name: 'LevelAgency', component: LevelAgency},

        { path: '/route', name: 'Route', component: Route},
        { path: '/add-route', name: 'AddRoute', component: AddRoute},
        { path: '/order-route', name: 'OrderRoute', component: OrderRoute},

        { path: '/vehicle', name: 'Vehicle', component: Vehicle},
        { path: '/VehicleOwner', name: 'VehicleOwner', component: VehicleOwner},

        { path: '/point', name: 'Point', component: Point},

        { path: '/promotion', name: 'Promotion', component: Promotion},
        { path: '/addition-price', name: 'AdditionPrice', component: AdditionPrice},
        { path: '/cancel-ticket-policy', name: 'CancelTicketPolicy', component: CancelTicketPolicy},
        { path: '/policy-agency', name: 'PolicyAgency', component: PolicyAgency},
        { path: '/price-policy', name: 'PricePolicy', component: PricePolicy},

        { path: '/seat-map', name: 'SeatMap', component: SeatMap},
        { path: '/lock-seat', name: 'LockSeat', component: LockSeat},

        { path: '/item', name: 'Item', component: Item},

        { path: '/bill', name: 'Bill', component: Bill},
        { path: '/bill-create', name: 'BillCreate', component: BillCreate},
        { path: '/bill-in-trip', name: 'BillInTrip', component: BillInTrip},
        { path: '/bill-in-trip-2', name: 'BillInTrip2', component: BillInTrip2},
        { path: '/bill-in-trip-for-driver', name: 'BillInTripForDriver', component: BillInTripForDriver},

        { path: '/notification-popup', name: 'NotificationPopup', component: NotificationPopup},

        { path: '/trips-driver-report', name: 'TripsDriverReport', component: TripsDriverReport},
        { path: '/transshipment-report', name: 'TransshipmentReport', component: TransshipmentReport},
        { path: '/report-count-trip-driver', name: 'ReportCountTripDriver', component: ReportCountTripDriver},

        { path: '/system-setting', name: 'SystemSetting', component: SystemSetting},
        { path: '/system-info-setting', name: 'SystemInfoSetting', component: SystemInfoSetting},

        { path: '/display-setting', name: 'DisplaySetting', component: DisplaySetting},

        { path: '/display-setting-bill-in', name: 'DisplaySettingBillIn', component: DisplaySettingBillIn},
        { path: '/display-setting-bill-out', name: 'DisplaySettingBillOut', component: DisplaySettingBillOut},
        { path: '/display-setting-print-ticket', name: 'DisplaySettingPrintTicket', component: DisplaySettingPrintTicket},
        { path: '/display-setting-print-header', name: 'DisplaySettingPrintHeader', component: DisplaySettingPrintHeader},
        { path: '/display-setting-print-list-ticket', name: 'DisplaySettingPrintListTicket', component: DisplaySettingPrintListTicket},

        { path: '/print-config-ticket', name: 'PrintConfigTicket', component: PrintConfigTicket},
        { path: '/config-ticket-email', name: 'TicketEmail', component: TicketEmail},
        { path: '/print-config-table-tickets', name: 'PrintConfigTableTickets', component: PrintConfigTableTickets},
        { path: '/print-config-freight-order', name: 'PrintConfigFreightOrder', component: PrintConfigFreightOrder},
        { path: '/print-config-ticket-contract', name: 'PrintConfigTicketContract', component: PrintConfigTicketContract},
        { path: '/print-config-ticket-temp', name: 'PrintConfigTicketTemp', component: PrintConfigTicketTemp},
        { path: '/print-config-payment', name: 'PrintConfigTablePayment', component: PrintConfigTablePayment},
        { path: '/print-config-payment-chi', name: 'PrintConfigTablePaymentChi', component: PrintConfigTablePaymentChi},
        { path: '/print-config-statistical-goods-by-user', name: 'PrintConfigStatisticalGoodsByUser', component: PrintConfigStatisticalGoodsByUser},
        { path: '/print-config-seat-map', name: 'PrintConfigSeatMap', component: PrintConfigSeatMap},
        { path: '/print-config-goods-tem', name: 'PrintConfigGoodsTem', component: PrintConfigGoodsTem},
        { path: '/print-config-goods', name: 'PrintConfigGoods', component: PrintConfigGoods},
        { path: '/print-config-report-detail', name: 'PrintConfigReportDetail', component: PrintConfigReportDetail},
        { path: '/print-config-mail-template', name: 'PrintConfigMailTemplate', component: PrintConfigMailTemplate},
        { path: '/print-config-report-trip', name: 'PrintConfigReportTrip', component: PrintConfigReportTrip},

        { path: '/sms-config-display', name: 'SmsConfigDisplay', component: SmsConfigDisplay},

        { path: '/cash-book', name: 'CashBook', component: CashBook},
        { path: '/fuel', name: 'Fuel', component: Fuel},
        { path: '/report-by-agency', name: 'ReportByAgency', component: ReportByAgency},
        { path: '/report-detail', name: 'ReportDetail', component: ReportDetail},
        {path: '/report-detail-02', name: 'ReportDetail02', component: ReportDetail02},
        { path: '/report-online-payment', name: 'ReportOnlinePayment', component: ReportOnlinePayment},
        { path: '/report-message', name: 'ReportMessage', component: ReportMessage},
        { path: '/report-goods', name: 'ReportGoods', component: ReportGoods},
        { path: '/report-cash-by-user', name: 'ReportCashByUser', component: ReportCashByUser},
        { path: '/report-cash-by-user-02', name: 'ReportCashByUser02', component: ReportCashByUser02},
        { path: '/report-data-studio', name: 'ReportDataStudio', component: ReportDataStudio},
        { path: '/report-trip-print', name: 'ReportTripPrint', component: ReportTripPrint},
        {
            path: '/assign-transshipment-driver-for-ticket',
            name: 'AssignTransshipmentDriverForTicket',
            component: AssignTransshipmentDriverForTicket
        },
        {path: '/assign-staff-deliver', name: 'AssignStaffDeliver', component: AssignStaffDeliver},

        { path: '/group-permission', name: 'GroupPermission', component: GroupPermission},

        { path: '/user', name: 'User', component: User},
        { path: '/user-login-history', name: 'UserLoginHistory', component: UserLoginHistory},
        { path: '/agency', name: 'Agency', component: Agency},
        { path: '/login-security', name: 'LoginSecurity', component: LoginSecurity},

        { path: '/manage-list-trip', name: 'ManageListTrip', component: ManageListTrip},
        { path: '/manage-list-trip-new', name: 'ManageListTripNew', component: ManageListTripNew},
        { path: '/manage-list-trip-table', name: 'ManageListTripTable', component: ManageListTripTable},
        {path: '/manage-list-trip-two-route', name: 'ManageListTripTwoRoute', component: ManageListTripTwoRoute},

        { path: '/authorize-sell-ticket', name: 'AuthorizeSellTicket', component: AuthorizeSellTicket},

        { path: '/telecom-number', name: 'TelecomNumber', component: TelecomNumber},
        { path: '/region', name: 'Region', component: Region},
        { path: '/RegionbyLocation', name: 'RegionbyLocation', component: RegionbyLocation},

        { path: '/work-shift', name: 'WorkShift', component: WorkShift},

        {path: '/schedule-of-staff', name: 'ScheduleOfStaff', component: ScheduleOfStaff},

        {path: '/assign-ticket-shipping', name: 'AssignTicketShipping', component: AssignTicketShipping},

        {path: '/surcharge-category', name: 'SurchargeCategory', component: SurchargeCategory},

        { path: '/warning-delivering-tickets', name: 'WarningDeliveringTickets', component: WarningDeliveringTickets},

        {path: '/goods-setting', name: 'GoodsSetting', component: GoodsSetting},

        {path: '/coordination-request', name: 'CoordinationRequest', component: Coordination},
        {path: '/contrast/sanve', name: 'SanVeContrast', component: SanVeContrast},
        

        {path: '/import-and-export-to-warehouse', name: 'ImportAndExportGoods', component: ImportAndExportGoods},
        {path: '/receive-goods-in-home', name: 'ReceiveGoodsInHome', component: ReceiveGoodsInHome},
        {path: '/form-manager-confirm-transfer', name: 'FormManagerConfirmTransfer', component: FormManagerConfirmTransfer},
        {path: '/custom-limit-invoice', name: 'CustomLimitInvoice', component: LimitInvoice},
        {path: '/manage-ingredient', name: 'IngredientReport', component: IngredientReport},
        {path: '/ret', name: 'Ret', component: Ret},
        {path: '/menu-manage-report', name: 'MenuManageReport', component: MenuManageReport},
        {path: '/report-goods-by-employees', name: 'ReportGoodsByEmployees', component: ReportGoodsByEmployees},
        {path: '/manage-permission-sell-ticket', name: 'ManagePermissionSellTicket', component: ManagePermissionSellTicket},
        {path: '/ReportGood02', name: 'ReportGood02', component: ReportGood02},
        {path: '/trip-statistic', name: 'TripStatistic', component: TripStatistic},
        {path: '/report-list-passengers-route', name: 'ReportListPassengersRoute', component: ReportListPassengersRoute},
        {path: '/list-trip-cancer', name: 'ListTripCancer', component: ListTripCancer},
        {path: '/synthesis-report', name: 'SynthesisReport', component: SynthesisReport},
        {path: '/reports-by-vehicle', name: 'ReportsByVehicle', component: ReportsByVehicle},
        {path: '/report-by-route', name: 'ReportByRoute', component: ReportByRoute},
        {path: '/MySaleReport', name: 'MySaleReport', component: MySaleReport},
        {path: '/MyBillRequest', name: 'MyBillRequest', component: MyBillRequest},
        {path: '/ReportCustomer', name: 'ReportCustomer', component: ReportCustomer2},
        {path: '/report-contract', name: 'ReportContract', component: ReportContract},
        {path: '/ReportSynthetictTickerAndGoodByDate', name: 'ReportSynthetictTickerAndGoodByDate', component: ReportSynthetictTickerAndGoodByDate},
        {path: '/ReportTicketByStaffInMonth', name: 'ReportTicketByStaffInMonth', component: ReportTicketByStaffInMonth},
        {path: '/ReportSyntheticInOneDay', name: 'ReportSyntheticInOneDay', component: ReportSyntheticInOneDay},

        {path: '/ReportRoomTicketByPointType', name: 'ReportRoomTicketByPointType', component: ReportRoomTicketByPointType},
        { path : '/CustomerFeedbackHistory', name: 'CustomerFeedbackHistory', component : CustomerFeedbackHistory },
        { path : '/ReportTicketOnline', name: 'ReportTicketOnline', component : ReportTicketOnline },
        { path : '/ReportIncomeByPhoneNumberCustomer', name: 'ReportIncomeByPhoneNumberCustomer', component : ReportIncomeByPhoneNumberCustomer },
        { path : '/manage-service', name: 'ManageService', component : ManageService },
        { path : '/manage-sold-service', name: 'ManageSoldService', component : ManageSoldService },
        { path : '/report-income-by-drivers', name: 'ReportIncomeByDrivers', component : ReportIncomeByDrivers },
        { path : '/report-detail-passenger-ticket', name: 'ReportDetailPassengerTicket', component : ReportDetailPassengerTicket },
        { path : '/ReportIncomeVehicleContract', name: 'ReportIncomeVehicleContract', component : ReportIncomeVehicleContract },
        { path : '/ReportTD1', name: 'ReportTD1', component : ReportTD1 },
        { path : '/ReportTD2', name: 'ReportTD2', component : ReportTD2 },
        { path : '/ManageApp', name: 'ManageApp', component : ManageApp },
        { path : '/ReportAV', name: 'ReportAV', component : ReportAV },
        { path : '*',component : error },
    ],
    mode :'history',
    base :'/cpanel/vue/'
});


router.beforeEach(function (to, from, next) {
    let skips = ['RoomWork', 'SystemSetting'];
    if (skips.includes(to.name)) {
        return next();
    }
    ;
    let history = localStorage.getItem('room') || null;
    if (history) {
        history = JSON.parse(history);
    }
    let companyConfig = new CompanyConfig();
    companyConfig.set(window.userInfo.companyconfigurations);
    let room = history && history.companyId && history.companyId === window.userInfo.userInfo.companyId && history.tokenKey === window.userInfo.token.tokenKey ? history : null;
    let config = _.cloneDeep(companyConfig.CONFIG.SELECT_ROOM_AFTER_LOGIN);
    if (config.value && (!room || !room.id)) {
        return next({name: 'RoomWork'});
    }
    return next();
});

export default router;
