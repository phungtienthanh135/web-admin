window.language = {
    'vi':{
        name : "Tiếng Việt",
        img : '',
        replace : {
            CREATE : "Tạo",
            UPDATE : 'Sửa',
            DELETE : 'Xóa',
            SEARCH : "Lọc",
            LIST : 'Danh Sách',

            VEHICLE : "PHƯƠNG TIỆN",

            TICKET : "Vé",
            SELL_TICKET : "Bán Vé",

            GOODS : "Hàng Hóa",
            GOODS_TYPE : "Loại Hàng Hóa",

            TIME : "Giờ",
            START_TIME :'Giờ Bắt Đầu',
            END_TIME : "Giờ Kết Thúc",
            DATE : "Ngày",
            START_DATE : "Ngày Bắt Đầu",
            END_DATE : "Ngày Kết Thúc",

            ROUTE : "Tuyến",

            TRIP : "Chuyến",

            PRINT : "In",


        }
    }
};