import app from '../app';

export default {
    SET_LANG: function(state, payload) {
        app.$i18n.locale = payload;
    },
    LOAD : function (state,payload) {
        state.supperLoading.push(payload);
    },
    UNLOAD : function (state,payload) {
        state.supperLoading.remove(payload);
    },
    SET_TICKET_SAMPLE : function (state,ticket) {
        state.ticketSample = ticket;
    },
    SET_AGENCY_DEBT_LIMIT : function (state,payload) {
        state.agencyDebtLimit = payload;
    },
    SET_SELL_FORM : function (state,payload) {
        if(payload){
            state.sellForm=payload;
            func.setConfigWeb({sellForm:payload});
        }else{
            let history = func.getConfigWeb('sellForm');
            state.sellForm = history || 'Ticket07';
        }
    },
    MERGE_TICKETS_PRINT : function (state,payload) {
        if(payload!==undefined&&payload!==null){
            state.mergeTicketsWhenPrint = payload;
            func.setConfigWeb({mergeTicketsWhenPrint:payload});
        }else{
            var flag = func.getConfigWeb('mergeTicketsWhenPrint')||false;
            state.mergeTicketsWhenPrint = flag;
        }

    },
    SHOW_DATEPICKER : function (state,payload) {
        if(payload!==undefined&&payload!==null){
            state.showDatepicker = payload;
            func.setConfigWeb({showDatepicker:payload});
        }else{
            var flag = func.getConfigWeb('showDatepicker')||false;
            state.showDatepicker = flag;
        }

    },
    CONFIG_DATA : function (state,payload) {
        let data = JSON.parse(localStorage.getItem('configData')||'{}');
        data = payload!==null&&typeof payload === "object" ? payload : data;
        $.each(data,function (key,value) {
            state.config_data[key] = value;
        });
        localStorage.setItem('configData',JSON.stringify(state.config_data));
    },
    SET_SHOW_TICKETS_OR_SEAT_MAP : function (state,payload) {
        state.showTicketsOrSeatMap = payload||false;
    },
    SET_MERCHANTS : function (state,payload) {
        state.merchants = payload;
    },
    SET_SMS_CONFIGS : function (state,payload) {
        state.smsConfigs = payload;
    },
    SET_SMS_GOOD_CONFIG : function (state,payload) {
        state.smsGoodConfig = payload;
    },
    SET_ROUTES : function (state,payload) {
        state.routes = payload;
    },
    SET_ROOM: function (state, payload) {
        if (payload && payload.id && payload.name) {
            var data = {
                id: payload.id,
                name: payload.name,
                address: payload.address ? payload.address : '',
                phoneNumber: payload.phoneNumber ? payload.phoneNumber : '',
                pointTypes: payload.pointTypes ? payload.pointTypes : '',
                pointType: 8,
                companyId: payload.companyId ? payload.companyId : null,
                tokenKey : window.userInfo.token.tokenKey,
            };
            localStorage.setItem('room', JSON.stringify(data));
            state.room = data;
        } else {
            var history = localStorage.getItem('room') || null;
            if (history) {
                history = JSON.parse(history);
            }
            history = history && history.companyId && history.companyId === window.userInfo.userInfo.companyId&&history.tokenKey === window.userInfo.token.tokenKey ? history : null;
            state.room = history;
        }
    }
}