import Vue from 'vue';
import Vuex from 'vuex';
import State from './state.js';
import Getters from './getters';
import Mutations from './mutations';
import Actions from './actions';

import TicketVersion04 from './modules/ticketVersion04';
import Ticket05 from './modules/Ticket05';
import Ticket06 from './modules/Ticket06';
import Ticket07 from './modules/Ticket07';
import Ticket09 from './modules/Ticket09';
import Ticket09_2 from './modules/Ticket09_2';
import CompanyConfig from './modules/company_config';
import ManageListTrip from './modules/ManageListTrip';
import ManageGoods from './modules/ManageGoods';
import LockSeat from './modules/lockSeat';

Vue.use(Vuex);

export default new Vuex.Store({
    state : State,
    getters : Getters,
    mutations : Mutations,
    actions : Actions,
    modules : {
        TicketVersion04 : TicketVersion04,
        Ticket05 : Ticket05,
        Ticket06 : Ticket06,
        Ticket07: Ticket07,
        Ticket09: Ticket09,
        Ticket09_2: Ticket09_2,
        CompanyConfig: CompanyConfig,
        ManageListTrip: ManageListTrip,
        ManageGoods: ManageGoods,
        LockSeat: LockSeat
    }
});