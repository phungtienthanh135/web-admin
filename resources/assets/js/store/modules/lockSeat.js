
export default {
    namespaced: true,
    state: {
        lockSeatState: null,
    },
    getters: {

    },
    mutations: {
        SET_LOCK_SEAT_CONFIG: function(state, config) {
            state.lockSeatState = config;
        }
    },
    actions: {
        INIT_LOCK_SEAT_CONFIG: function(context) {
            context.dispatch('GET_LOCK_SEAT_CONFIG');
        },
        GET_LOCK_SEAT_CONFIG: function(context){
            firebaseDatabase.ref().child("LOCK_SEAT").child(userInfo.userInfo.companyId).on("value", function(snapshot) {
                context.commit("SET_LOCK_SEAT_CONFIG", snapshot.val());
            });
        },
        UPDATE_LOCK_SEAT_CONFIG: function(context, config) {
            firebaseDatabase.ref().child("LOCK_SEAT").child(userInfo.userInfo.companyId).set(config);
        }
    }

}