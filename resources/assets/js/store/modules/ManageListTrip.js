import filters from '../../Helper/Filters.js';

export default {
    state: { 
        view: {
            driver: null,
            vehicle: null
        },
        viewConfig: {
            showVehicle: true,
            showDrivers: true,
            showSeat: true,
            showStartTime: true,
            showShortTimeRange: false,
            showTimeNoTrip: true,
            showSoldSeat: false,
            showNote: false
        },
        tripSelected: null,
        tripEditor: {
            listLockTrip: [],
            code: null,
            note: null,
            startTimeReality: null,
            vehicle: null,
            drivers: [],
            assistants: [],
            tripStatus: null,
            color: null,
            //for add trip
            route: null,
            // timeRange: 0
        }
    },
    mutations: { 
        SET_CONFIGS: (state, payload) => {
            Object.keys(payload).forEach((key) => {
                state.viewConfig[key] = payload[key]
            })
        },
        TOGGLE_VIEW_CONFIG: (state, conf) => { state.viewConfig[conf] = !state.viewConfig[conf] },
        SET_VIEW_CONFIG_FROM_LOCAL_STORAGE: (state) => {  
            if(localStorage.getItem('ManageListTrip.viewConfig') != null) {
                try {
                    let localConfig = JSON.parse( localStorage.getItem('ManageListTrip.viewConfig') )
                    
                    Object.keys(state.viewConfig).forEach((key) => {
                        if(localConfig[key] !== undefined) {
                            state.viewConfig[key] = localConfig[key]
                        }
                    })
                    
                } catch(error) {
                    console.log('Error Config: ' + error)
                }
            } 
        },
        SET_DRIVER_VIEWED: (state, driver) => { 
            state.view.driver = driver 
        },
        SET_VEHICLE_VIEWED: (state, vehicle) => { 
            state.view.vehicle = vehicle 
        },
        RESET_TRIP_EDITOR: (state) => {
            state.tripEditor.listLockTrip = [];
            state.tripEditor.code = null;
            state.tripEditor.color = null;
            state.tripEditor.note = null;
            state.tripEditor.startTimeReality = null;
            state.tripEditor.vehicle = null;
            state.tripEditor.drivers = [];
            state.tripEditor.assistants = [];
            state.tripEditor.tripStatus = null;
            state.tripEditor.route = null;

            state.tripSelected = null
        },
        SET_EDITOR_VEHICLE: (state, payload) => {
            state.tripEditor.vehicle = payload
        },
        SET_TRIP_SELECTED: (state, trip) => { 
            if(trip) {
                state.tripEditor.listLockTrip = trip.listLockTrip ? _.cloneDeep(trip.listLockTrip) : []
                state.tripEditor.code = _.cloneDeep(trip.code) == null ? trip.route.nameShort : _.cloneDeep(trip.code);
                state.tripEditor.note = trip.note ? _.cloneDeep(trip.note) : null
                state.tripEditor.startTimeReality = trip.startTimeReality ? filters.customDate(trip.startTimeReality,'#hhhh#:#mm#',true) : null;
                state.tripEditor.vehicle = trip.vehicle ? _.cloneDeep(trip.vehicle) : null
                state.tripEditor.drivers = trip.drivers ? _.cloneDeep(trip.drivers) : null
                state.tripEditor.assistants = trip.assistants ? _.cloneDeep(trip.assistants) : null
                state.tripEditor.tripStatus = _.cloneDeep(trip.tripStatus) || null
                state.tripEditor.color = _.cloneDeep(trip.color) || null

                state.tripSelected = trip 
            }
        },
        SET_TRIP_SELECTED_STATUS: (state, status) => { 
            state.tripSelected.tripStatus = status
        }
    },
    actions: {
        
    },
    getters: { 
        
    }
}