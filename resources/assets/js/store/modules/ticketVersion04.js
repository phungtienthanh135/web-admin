export default {
    namespaced : true,
    state : {
        showAllTicket : false,
    },
    getters : {
        showAllTicket : state => state.showAllTicket,
    },
    mutations : {
        changeShowAllTicket :function (state) {
            state.showAllTicket=!state.showAllTicket;
        }
    },
    actions :{

    }
}