export default {
    state: { 
        goodsesSelected: [],
        viewConfig: {
            REQUEST: true,
            REQUEST_SUCCESS: true,
            ON_VEHICLE: true,
            ON_ROOM_DELIVERY: true,
            DELIVERING: true,
            DELIVERY_SUCCESS: true,
            DELIVERY_ERROR: true,
            CANCELED: true,
            SHOW_BARCODE_FORM: false,
            SHOW_GOODS_BY_WAREHOUSE: false,
            SHOW_GOODS_GROUP_FORM: false
        },
        modal: {
            wareHouse: false,
            assignTrip: false,
            assignDriver: false,
            cancelGoods: false,
            successGoods: false,
            viewGoods: false,
        },
        goodsListFilter: {
            page: 0,
            count: 100,
            date: null,
            code: null,
            phoneNumber: null,
            receivedPoint: null,
            deliveryPoint: null,
            loadMore: true
        },
        goodsDelivering: null,
        manageAction: null,
        goodsViewing: null,
        listGoods: [],
        loading: false,
        goodsFormater: null
    },
    actions: {
        updateGoods (context, payload) {
            let params = payload

            params.codes = context.state.goodsesSelected.map(value => value.code)
            context.state.loading = true

            context.commit('TOOGLE_LOADING', true)
            sendJson({
                url: urlDBD(API.goodsV2Manage),
                data: params,
                success: (data) => {
                    context.commit('TOOGLE_MANAGE_GOODS_ACTION', context.state.manageAction) 
                    context.commit('SET_GOODSES_SELECTED_EMPTY') 
                    context.dispatch('getListGoods', {pushGoods: false})
                    context.commit('TOOGLE_LOADING', false)

                    notify('cập nhật thành công !','success');
                }
            })
        },
        
        updateGoodsDeliver (context, payload) {
            let params = payload
            params.id = context.state.goodsDelivering.id
            
            context.state.loading = true

            sendJson({
                url: urlDBD(API.goodsV2Deliver),
                data: params,
                success: (res) => {
                    context.commit('TOOGLE_MANAGE_GOODS_ACTION', context.state.manageAction) 
                    context.commit('SET_GOODSES_SELECTED_EMPTY')
                    context.dispatch('getListGoods', {pushGoods: false})
                    context.commit('TOOGLE_LOADING', false)

                    notify('cập nhật thành công !','success');
                },
                functionIfError: (err) => {
                    context.state.loading = false;
                }
            });
        },

        getListGoods ( context, payload ) {
            let params = context.getters.getFilterForGetGoods

            context.commit('TOOGLE_LOADING', true)

            sendParam({
                url: window.urlDBD(window.API.goodsV2List),
                data: params,
                type: "GET",
                success: (data) => {
                    if (data.results.goods.length < params.count) {
                        context.state.loadMore = false;
                    } 
                    let result = data.results.goods;

                    result = result.sort((value1, value2) => {
                        return value2.createdTime - value1.createdTime
                    })
                    
                    let listGoods = []
                    listGoods = result.map(item => item instanceof context.state.goodsFormater ? item : new context.state.goodsFormater(item))

                    if(payload && typeof payload.pushGoods !== 'undefined') {
                        if(payload.pushGoods) {
                            context.state.listGoods.push(...listGoods)
                        } else {
                            context.state.listGoods = listGoods
                        }
                    } else {
                        context.state.listGoods.push(...listGoods)
                    }
                    

                    if (payload && typeof payload.success === 'function') {
                        payload.success(context)
                    }
                    context.commit('TOOGLE_LOADING', false)
                },
                functionIfError: (data) => {
                    context.commit('TOOGLE_LOADING', false)
                }
            });

        },

        searchAndImportGoodsBarCode (context, payload) {
            let goodsFormater = context.state.goodsFormater

            let params = {
                code: payload
            }

            sendParam({
                url: window.urlDBD(window.API.goodsV2List),
                data: params,
                type: "GET",
                success: (data) => {
                    let result = data.results.goods;

                    if(result.length > 0) {
                        context.commit('SET_GOODSES_SELECTED', new goodsFormater(result[0]))
                        context.commit('UPDATE_NUMBER_SCAN_GOODS',{goods: result[0], numberScan: 1 })
                    }
                    
                }
            })
        }
    },
    getters: {
        getFilterForGetGoods (state) {
            if( !state.goodsListFilter.date ) {
                state.goodsListFilter.date = [new Date( new Date().getTime() - 7 * 60 * 60 * 24 * 1000  ).customFormat('#YYYY##MM##DD#'), new Date().customFormat('#YYYY##MM##DD#')]
            }

            let filter = state.goodsListFilter
            let params = {}
            Object.keys(filter).forEach( key => {
                let val = filter[key]
                if(val !== null) {
                    if(key == 'deliveryPoint' || key == 'receivedPoint') {
                        params[key] = val['id']
                    } else {
                        params[key] = val
                    }
                }
            })

            delete params.loadMore

            return params
        }
    },
    mutations: { 
        SET_GOODS_DELIVERING: (state, payload) => {
            state.goodsDelivering = payload
        },

        SET_GOODS_FORMATER: (state, payload) => {
            state.goodsFormater = payload
        },

        SET_GOODS_FILTER: (state, payload) => {
            if(payload) {
                Object.entries(payload).forEach(value => {
                    state.goodsListFilter[value[0]] = value[1]
                })
            }
            
        },
        TOOGLE_LOADING: (state, payload) => {
            state.loading = payload
        },
        TOOGLE_GOODS_VIEWING: (state, payload) => {
            if(!payload) {
                state.modal.viewGoods = false
                state.goodsViewing = null
            } else {
                state.modal.viewGoods = true
                state.goodsViewing = payload
            }
        },

        TOOGLE_MANAGE_GOODS_ACTION: (state, payload) => {
            if(payload == 'UpdateStatus') {
                state.manageAction = state.manageAction != 'UpdateStatus' ? 'UpdateStatus' : null
            }
            if(payload == 'IOWareHouse') {
                state.modal.wareHouse = !state.modal.wareHouse
                state.manageAction = state.modal.wareHouse ? 'IOWareHouse' : null
            }

            if(payload == 'AssignTrip') {
                state.modal.assignTrip = !state.modal.assignTrip
                state.manageAction = state.modal.assignTrip ? 'AssignTrip' : null
            }

            if(payload == 'AssignDriver') {
                state.modal.assignDriver = !state.modal.assignDriver
                state.manageAction = state.modal.assignDriver ? 'AssignDriver' : null
            }

            if(payload == 'CancelGoods') {
                state.modal.cancelGoods = !state.modal.cancelGoods
                state.manageAction = state.modal.cancelGoods ? 'CancelGoods' : null
            }

            if(payload == 'SuccessGoods') {
                state.modal.successGoods = !state.modal.successGoods
                state.manageAction = state.modal.successGoods ? 'SuccessGoods' : null
            }
        },

        TOOGLE_TAB: (state, payload) => {
            if(payload == 'SHOW_BARCODE_FORM') {
                state.viewConfig.SHOW_BARCODE_FORM = !state.viewConfig.SHOW_BARCODE_FORM
                state.viewConfig.SHOW_GOODS_GROUP_FORM = false
            }

            if(payload == 'SHOW_GOODS_GROUP_FORM') {
                state.viewConfig.SHOW_BARCODE_FORM = false
                state.viewConfig.SHOW_GOODS_GROUP_FORM = !state.viewConfig.SHOW_GOODS_GROUP_FORM
            }
        },

        SET_CONFIGS: (state, payload) => {
            Object.keys(payload).forEach((key) => {
                state.viewConfig[key] = payload[key]
            })
        },

        SET_VIEW_CONFIG_FROM_LOCAL_STORAGE: (state) => {  
            if(localStorage.getItem('ManageGoodsv2.viewConfig') != null) {
                try {
                    let localConfig = JSON.parse( localStorage.getItem('ManageGoodsv2.viewConfig') )
                    Object.keys(state.viewConfig).forEach((key) => {
                        if(localConfig[key] !== undefined) {
                            state.viewConfig[key] = localConfig[key]
                        }
                    })
                } catch(error) {
                    console.log('Error Config: ' + error)
                }
            } 
        },

        SET_GOODSES_SELECTED: (state, goods) => {
            if(!Array.isArray(goods)) {
                let goodsSlected = state.goodsesSelected.filter(_ => _.id == goods.id)
                let hasGoods = goodsSlected[0] ? true : false
                
                if(!hasGoods) {
                    state.goodsesSelected.push(goods)
                }
            } else {
                if(goods.length == 0) {
                    state.goodsesSelected = []
                    return 
                }
                goods.forEach((value) => {
                    let goodsFilter = state.goodsesSelected.filter(_ => _.id == value.id)

                    if(!goodsFilter[0]) {
                        state.goodsesSelected.push(value)
                    }
                })
            }
        },

        UPDATE_NUMBER_SCAN_GOODS: (state, payload) => {
            let goods = payload.goods
            let numberScan = payload.numberScan

            state.goodsesSelected.forEach(value => {
                if(value.code == goods.code) {
                    value.scanNumber += numberScan
                }
            })
        },
        
        SET_GOODSES_SELECTED_EMPTY: (state, goods) => {
            state.goodsesSelected = []
        },

        UNSET_GOODS_SELECTED: (state, goodsId) => {
            let goodsesSelected = state.goodsesSelected.filter(goods => goods.id != goodsId) 
            state.goodsesSelected = goodsesSelected
        },
    }
}