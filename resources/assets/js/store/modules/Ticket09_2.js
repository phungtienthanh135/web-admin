import {orderTickets} from '../LibraryTicket06.js';
import countEmptySeat from '../../class/countEmptySeat';

export default {
    namespaced: true,
    state: {
        routes: [],
        date: '',
        time: '',
        useFilterTime: false,
        route: null,
        startPoint: null,
        endPoint: null,
        trips: [],
        trip: null,
        rt_trip : [],
        indexPointInRoute: null,
        rt_trips: [],
        floorActive: 0,
        ticketsSelected: [],
        ticketsSelectedType: 0, // 0 là bán vé ,1 là cập nhật
        lastPushTicketsSelected: null,
        ticketsMove: [],
        showWay: false,
        showNewFormTicket: false,
        ticketScroll: null,
        scrolled: false,
        showAllTicketInSeat: false,
        showDetailTrips: false,
        showListTripsItemV2: false,
        showRouteInTripItem: false,
        addressUpTicketNotSeat: null,
        addressDownTicketNotSeat: null,
        lockedSeat: null,
        buyAllTicket: false,
        loadSeats: [],
        showFormSellTicket: false,
        loadSeatmap: false,
        newSeatMap: [],
        listLoadOneTicket: [],
    },
    getters: {
        findAddressTicketNotSeat: function (state) {
            var data = {
                up: [],
                down: []
            };
            try {
                var trip = _.cloneDeep(state.trip);
                $.each(trip.ticketsNotSeat, function (t, tk) {
                    if (!data.up.includes(tk.pointUp.address)) {
                        data.up.push(tk.pointUp.address);
                    }
                    if (!data.down.includes(tk.pointDown.address)) {
                        data.down.push(tk.pointDown.address);
                    }
                });
                return data;
            } catch (e) {
                return data;
            }
        },
        totalTicketPaid: function (state) {
            var count = 0
            try {
                var trip = _.cloneDeep(state.trip);
                $.each(trip.ticketsInSeatMap, function (t, ticketsInfo) {
                    if(ticketsInfo.paidMoney == ticketsInfo.agencyPrice) {
                        count ++;
                    }
                });
                $.each(trip.ticketsNotSeat, function (t, ticketsInfo) {
                    if(ticketsInfo.paidMoney == ticketsInfo.agencyPrice) {
                        count ++;
                    }
                });
                return count;
            } catch (e) {
                return count;
            }
        },
        totalMoneyTrip: function (state) {
            var totalMoney = {
                totalPaidMoney: 0,
                totalAgencyPrice: 0
            };
            try {
                var trip = _.cloneDeep(state.trip);
                $.each(trip.ticketsInSeatMap, function (t, ticketsInfo) {
                    totalMoney.totalPaidMoney += parseInt(ticketsInfo.paidMoney);
                    totalMoney.totalAgencyPrice += parseInt(ticketsInfo.agencyPrice);
                });
                $.each(trip.ticketsNotSeat, function (t, ticketsInfo) {
                    totalMoney.totalPaidMoney += parseInt(ticketsInfo.paidMoney);
                    totalMoney.totalAgencyPrice += parseInt(ticketsInfo.agencyPrice);
                });
                return totalMoney;
            } catch (e) {
                return totalMoney;
            }
        },
        mapRoutes: function (state) {
            var map = {};
            var routes = state.routes;
            console.log('indexPointInRoute2',routes)
            $.each(routes, function (r, route) {
                var data = {
                    index: r,
                    mapPoints: {}
                };
                $.each(route.listPoint, function (p, point) {
                    data.mapPoints[point.id] = {
                        index: p
                    };
                });
                map[route.routeId] = data;
            });
            return map;
        },
        pointsInRoute: function (state) {
            var list = [];
            var mapId = {};
            var routes = state.routes;
            var route = state.route;
            if (route) {
                $.each(route.listPoint, function (p, point) {
                    if (mapId[point.id] == undefined) {
                        mapId[point.id] = list.length;
                        list.push(point);
                    }
                });
            } else {
                $.each(routes, function (r, rou) {
                    $.each(rou.listPoint, function (p, point) {
                        if (mapId[point.id] == undefined) {
                            mapId[point.id] = list.length;
                            list.push(point);
                        }
                    });
                });
            }

            return {
                points: list,
                map: mapId
            };
        },
        routesInTrips: function (state) {
            var trips = state.trips;
            var mapRoute = {};
            var route = [];
            $.each(trips, function (t, trip) {
                if (!mapRoute[trip.route.id]) {
                    mapRoute[trip.route.id] = true;
                    route.push(trip.route);
                }
            });
            return route;
        },
        allowPastDaysToSellTicket: function (state) {
            var currentDate = moment([moment().format('YYYY'), moment().format('MM') - 1, moment().format('DD')]);
            var dateNew = moment([moment(func.dateToMS(state.date)).format('YYYY'), moment(func.dateToMS(state.date)).format('MM') - 1, moment(func.dateToMS(state.date)).format('DD')]);
            var num = dateNew.diff(currentDate, 'days');
            return num + userInfo.pastDaysToSellTicket > 0 ? true : false;
        },
        lockTripAfterStart: function (state) {
            let trip = state.trip;
            var time = userInfo.timeLockTripAfterStart;
            if (trip && time != null && trip.tripStatus == 2 && time >= 0 && trip.timeStartTrip > 0 && (trip.timeStartTrip + time < new Date().getTime() + 60000)) {
                return false;
            }
            return true;
        },
        limitTime: function (state) {
            let startHour = state.time - window.userInfo.tripSearchDuration;
            let endHour = state.time + window.userInfo.tripSearchDuration;
            if (startHour < 0 || startHour > 24) {
                startHour = 0;
            }
            if (endHour < 0 || endHour > 24) {
                endHour = 24;
            }

            return {
                start: startHour * 60 * 60 * 1000,
                end: endHour == 24 ? endHour * 60 * 60 * 1000 - 1 : endHour * 60 * 60 * 1000
            };
        }
    },
    mutations: {
        SET_DATE: function (state, date) {
            state.date = date;
            window.func.setHistory({date2: date});
        },
        SET_TIME: function (state, _time) {
            _time = parseInt(_time);
            state.time = _time > -1 && _time < 25 ? _time : 0;
        },
        SET_USE_FILTER_TIME: function (state, payload) {
            state.useFilterTime = payload;
        },
        LOAD_ONE_TICKET : function (state, payload) {
            state.listLoadOneTicket.push(payload);
        },
        CANCEL_LOAD_ONE_TICKET: function (state) {
            state.listLoadOneTicket.shift();
        },
        SET_ROUTE: function (state, _route) {
            state.route = _route;
        },
        SET_START_POINT: function (state, point) {
            state.startPoint = point;
        },
        SET_END_POINT: function (state, point) {
            state.endPoint = point;
        },
        LOAD_CHANGE_SEATMAP: function (state, payload) {
            state.newSeatMap = payload;
        },
        LOAD_TICKET_INFORM: function (state, seats) {
            state.ticketsSelected = [];
            state.loadSeats = seats;
        },
        UN_LOAD_TICKET_INFORM: function (state) {
            console.log('UN_LOAD_TICKET_INFORM')
            state.loadSeats = [];
        },
        SET_ROUTES: function (state, routes) {
            state.routes = routes;
        },
        SET_TRIP_INFO: function () {

        },
        SET_TRIP: function (state, _tripId) {
            if(state.trip&&state.trip.tripId!==_tripId){
                state.ticketsSelected = [];
            }
            state.trip = null;
            if (_tripId) {// khi load ra thì rơi vào TH này, click thì rơi vào TH sau
                $.each(state.trips, function (t, trip) {
                    if (trip.tripId == _tripId) {
                        state.trip = trip;
                        return false;
                    }
                });
            }
            if (state.route && state.date) {
                if (state.trip !== null) {
                    // this.getPolicyTrip();
                    func.setHistory({
                        tripId2: state.trip.tripId,
                        routeId2: state.route ? state.route.routeId : null,
                        date2: state.date
                    });
                } else {
                    func.setHistory({routeId2: state.route ? state.route.routeId : null, date2: state.date});
                }
            }
            // khi thay chuyến thì tầng về 0
            state.floorActive = 0;
        },
        SHOW_FORM_SELL: function (state, option) {
            state.showFormSellTicket = option;
        },
        SET_TRIPS: function (state, trips) {
            state.trips = trips;
        },
        SET_RT_TICKETS_INFO  :function(state, payload){
            state.trip._ticketsInfo = payload
        },
        SET_RT_TICKETS_CANCELED  :function(state, payload){
            state.trip._ticketsCanceled = payload
        },
        SET_RT_TICKETS_TRANSFER  :function(state, payload){
            state.trip._ticketsTransfer = payload
        },
        CHANGE_TRIP_DETAIL: function (state, payload) {
            try {
                state.trip.payed = payload.payed;
                state.trip.ticketsTransfer = payload.ticketsTransfer;
                state.trip.ticketsInSeatMap = payload.ticketsInSeatMap;
                state.trip.ticketsInfo = payload.ticketsInSeatMap;
                state.trip.ticketsEndTime = payload.ticketsEndTime;
                state.trip.ticketsCanceled = payload.ticketsCanceled;
                state.trip.ticketsNotSeat = payload.ticketsNotSeat;
                state.trip.seatMapFake = payload.seatMapFake;
                if (!state.scrolled && state.ticketScroll) {
                    state.scrolled = true;
                }
            } catch (e) {
                console.warn(payload, e);
            }
        },
        CHANGE_TRIP_INFO: function (state, payload) {
            try {
                state.trips[payload.index].startTimeReality = payload.data.startTimeReality;
                state.trips[payload.index].tripStatus = payload.data.tripStatus;
                state.trips[payload.index].note = payload.data.note;
                state.trips[payload.index].code = payload.data.code;
                state.trips[payload.index].color = payload.data.color||state.trips[payload.index].color;
                state.trips[payload.index].tripType = payload.data.tripType;
                state.trips[payload.index].transshipmentTypes = payload.data.transshipmentTypes;

                if (payload.data.vehicle) {
                    state.trips[payload.index].vehicle = payload.data.vehicle;
                }
                state.trips[payload.index].drivers = payload.data.drivers;
                state.trips[payload.index].assistants = payload.data.assistants;
                state.trips[payload.index].totalSeat = payload.data.totalSeat;
                state.trips[payload.index].timeStartTrip = payload.data.timeStartTrip;
                state.trips[payload.index].contractRepresentation = payload.data.contractRepresentation;
                state.trips[payload.index].goods = payload.data.goods;
                state.trips[payload.index].totalGoods = payload.data.totalGoods;
                state.trips[payload.index].seatMap = payload.data.seatMap;
                if (payload.data.vehicle) {
                    state.trips[payload.index].vehicle = payload.data.vehicle;
                    if (state.trips[payload.index].vehicle.id != payload.data.vehicle.id) {
                        state.trips[payload.index].seatMapFake = payload.orderTicket.seatMapFake;
                    }
                }
                // try {
                //     state.trips[payload.index].location = {
                //         lat: payload.data.location.latitude,
                //         lng: payload.data.location.longitude
                //     };
                // } catch (e1) {
                //     state.trips[payload.index].location = null;
                // }
                state.trips[payload.index].payed = false;
                let seatData = payload.data.seatData.map(function(dataS){
                    let binary = [];
                    for (let i = 0; i < dataS.length; i++) {
                        binary.push(dataS.charCodeAt(i));
                    }
                    return binary;
                });
                state.trips[payload.index].totalEmptySeat = countEmptySeat(seatData,state.trips[payload.index].pointUp.order,state.trips[payload.index].pointDown.order);
                // if (state.trip && state.trips[payload.index].tripId == state.trip.tripId) {
                //     if (!state.scrolled && state.ticketScroll) {
                //         state.scrolled = true;
                //     }
                // }
            } catch (e) {
                console.warn(payload, e);
            }
        },
        CHANGE_TRIP_GOODS: function (state, payload) {
            try {
                state.trips[payload.index].goods = payload.data;
            } catch (e) {
                console.warn(payload, e);
            }
        },
        LOAD_SEATMAP: function (state, option) {
            state.loadSeatmap = option;
        },
        CHANGE_TRIP_PRINTED_TICKETS: function (state, payload) {
            try {
                state.trips[payload.index]._printedTickets = payload.data;
            } catch (e) {
                console.warn(payload, e);
            }
        },
        CHANGE_TRIP_LOCKED_SEAT: function (state, payload) {
            try {
                state.trips[payload.index]._lockedSeat = payload.data;
            } catch (e) {
                console.warn(payload, e);
            }
        },
        SET_RT_TRIPS: function (state, trips) {
            state.rt_trips = trips;
        },
        SET_POLICY_TRIP: function (state, policy) {
            if (state.trip) {
                state.trip.commissions = policy.commissions ? policy.commissions : null;
                state.trip.promotions = policy.promotions ? policy.promotions : null;
                state.trip.additions = policy.additionPrice ? policy.additionPrice : null;
                state.trip.cancelTicketPolicy = policy.cancelTicketPolicy ? policy.cancelTicketPolicy : null;
            }
        },
        SET_FLOOR_ACTIVE: function (state, floor) {
            state.floorActive = floor;
        },
        PUSH_SELL_TICKET: function (state, ticket) {
            if (state.ticketsSelectedType == 1) {
                state.ticketsSelected = [];
            }
            state.ticketsSelectedType = 0;
            state.ticketsSelected.push(ticket);
        },
        UN_PUSH_SELL_TICKET: function (state, ticket) {
            var index = null;
            state.ticketsSelected.forEach(function (tk, t) {
                if (tk.seat.seatId == ticket.seat.seatId) {
                    index = t;
                    return false;
                }
            });
            if (index !== null) {
                state.ticketsSelected.splice(index, 1);
            }
        },
        PUSH_TICKET_SELECTED_SAME_POINTTYPE: function (state, setTicket) {
            let ticket = setTicket.ticket
            if(setTicket.option == 'pointUp' ){
                var tksSlt = state.ticketsSelectedType == 0 ? [] : state.ticketsSelected;
                state.ticketsSelected = [];
                state.ticketsSelectedType = 1;
                tksSlt.push(ticket)
                var status = func.checkTicketStatus(ticket);
                if (status.mode === 'endTime') {
                    $.each(state.trip.ticketsEndTime, function (t, tk) {
                        if (tk.pointUp.pointType === ticket.pointUp.pointType) {
                            tksSlt.push(tk);
                        }
                    });
                } else if (status.mode === 'cancel') {
                    $.each(state.trip.ticketsCanceled, function (t, tk) {
                        if (tk.pointUp.pointType === ticket.pointUp.pointType) {
                            tksSlt.push(tk);
                        }
                    });
                } else {
                    $.each(state.trip.ticketsInSeatMap, function (t, tk) {
                        if (tk.pointUp.pointType === ticket.pointUp.pointType) {
                            tksSlt.push(tk);
                        }
                    });
                    $.each(state.trip.ticketsNotSeat, function (t, tk) {
                        if (tk.pointUp.pointType === ticket.pointUp.pointType) {
                            tksSlt.push(tk);
                        }
                    });
                }
                state.ticketsSelected = tksSlt;
            }
            if(setTicket.option == 'pointDown' ){
                var tksSlt = state.ticketsSelectedType == 0 ? [] : state.ticketsSelected;
                state.ticketsSelected = [];
                state.ticketsSelectedType = 1;
                tksSlt.push(ticket)
                var status = func.checkTicketStatus(ticket);
                if (status.mode === 'endTime') {
                    $.each(state.trip.ticketsEndTime, function (t, tk) {
                        if (tk.pointDown.pointType === ticket.pointDown.pointType) {
                            tksSlt.push(tk);
                        }
                    });
                } else if (status.mode === 'cancel') {
                    $.each(state.trip.ticketsCanceled, function (t, tk) {
                        if (tk.pointDown.pointType === ticket.pointDown.pointType) {
                            tksSlt.push(tk);
                        }
                    });
                } else {
                    $.each(state.trip.ticketsInSeatMap, function (t, tk) {
                        if (tk.pointDown.pointType === ticket.pointDown.pointType) {
                            tksSlt.push(tk);
                        }
                    });
                    $.each(state.trip.ticketsNotSeat, function (t, tk) {
                        if (tk.pointDown.pointType === ticket.pointDown.pointType) {
                            tksSlt.push(tk);
                        }
                    });
                }
                state.ticketsSelected = tksSlt;
            }

        },
        PUSH_TICKET_SELECTED_SAME_ADDRESS: function (state, setTicket) {
            let ticket = setTicket.ticket
            if(setTicket.option == 'pointUp' ){
                var tksSlt = state.ticketsSelectedType == 0 ? [] : state.ticketsSelected;
                state.ticketsSelected = [];
                state.ticketsSelectedType = 1;
                tksSlt.push(ticket)
                var status = func.checkTicketStatus(ticket);
                if (status.mode === 'endTime') {
                    $.each(state.trip.ticketsEndTime, function (t, tk) {
                        if (tk.pointUp.id === ticket.pointUp.id) {
                            tksSlt.push(tk);
                        }
                    });
                } else if (status.mode === 'cancel') {
                    $.each(state.trip.ticketsCanceled, function (t, tk) {
                        if (tk.pointUp.id === ticket.pointUp.id) {
                            tksSlt.push(tk);
                        }
                    });
                } else {
                    $.each(state.trip.ticketsInSeatMap, function (t, tk) {
                        if (tk.pointUp.id === ticket.pointUp.id) {
                            tksSlt.push(tk);
                        }
                    });
                    $.each(state.trip.ticketsNotSeat, function (t, tk) {
                        if (tk.pointUp.id === ticket.pointUp.id) {
                            tksSlt.push(tk);
                        }
                    });
                }
                state.ticketsSelected = tksSlt;
            }
            if(setTicket.option == 'pointDown' ){
                var tksSlt = state.ticketsSelectedType == 0 ? [] : state.ticketsSelected;
                state.ticketsSelected = [];
                state.ticketsSelectedType = 1;
                tksSlt.push(ticket)
                var status = func.checkTicketStatus(ticket);
                if (status.mode === 'endTime') {
                    $.each(state.trip.ticketsEndTime, function (t, tk) {
                        if (tk.pointDown.id === ticket.pointDown.id) {
                            tksSlt.push(tk);
                        }
                    });
                } else if (status.mode === 'cancel') {
                    $.each(state.trip.ticketsCanceled, function (t, tk) {
                        if (tk.pointDown.id === ticket.pointDown.id) {
                            tksSlt.push(tk);
                        }
                    });
                } else {
                    $.each(state.trip.ticketsInSeatMap, function (t, tk) {
                        if (tk.pointDown.id === ticket.pointDown.id) {
                            tksSlt.push(tk);
                        }
                    });
                    $.each(state.trip.ticketsNotSeat, function (t, tk) {
                        if (tk.pointDown.id === ticket.pointDown.id) {
                            tksSlt.push(tk);
                        }
                    });
                }
                state.ticketsSelected = tksSlt;
            }
        },
        PUSH_UPDATE_TICKET: function (state, ticket) {
            var listTicketCode = [];
            if (state.ticketsSelectedType == 1 && state.ticketsSelected.length > 0) {
                $.each(state.ticketsSelected, function (t, tk) {
                    if (!listTicketCode.includes(tk.ticketCode)) {
                        listTicketCode.push(tk.ticketCode);
                    }
                });
            }
            if (state.ticketsSelectedType == 0 || state.ticketsSelected.length == 0 || listTicketCode.length == 0 || !listTicketCode.includes(ticket.ticketCode)) {
                var tksSlt = state.ticketsSelectedType == 0 ? [] : state.ticketsSelected;
                state.ticketsSelected = [];
                state.ticketsSelectedType = 1;
                var status = func.checkTicketStatus(ticket);
                if (status.mode === 'endTime') {
                    $.each(state.trip.ticketsEndTime, function (t, tk) {
                        if (tk.ticketCode === ticket.ticketCode) {
                            tksSlt.push(tk);
                        }
                    });
                } else if (status.mode === 'cancel') {
                    $.each(state.trip.ticketsCanceled, function (t, tk) {
                        if (tk.ticketCode === ticket.ticketCode) {
                            tksSlt.push(tk);
                        }
                    });
                } else {
                    $.each(state.trip.ticketsInSeatMap, function (t, tk) {
                        if (tk.ticketCode === ticket.ticketCode) {
                            tksSlt.push(tk);
                        }
                    });
                    $.each(state.trip.ticketsNotSeat, function (t, tk) {
                        if (tk.ticketCode === ticket.ticketCode) {
                            tksSlt.push(tk);
                        }
                    });
                }
                state.ticketsSelected = tksSlt;
            } else {
                state.ticketsSelected.push(ticket);
            }
        },
        UN_PUSH_UPDATE_TICKET: function (state, ticket) {
            var index = null;
            state.ticketsSelected.forEach(function (tk, t) {
                if (tk.ticketId == ticket.ticketId) {
                    index = t;
                    return false;
                }
            });
            if (index !== null) {
                state.ticketsSelected.splice(index, 1);
            }
        },
        UN_PUSH_WITH_INDEX: function (state, index) {
            state.ticketsSelected.splice(index, 1);
        },
        UN_SELECTED: function (state) {
            state.ticketsSelected = [];
        },
        SET_TICKETS_SELECTED: function (state, tickets) {
            state.ticketsSelected = tickets;
        },
        SET_TICKETS_MOVE: function (state) {
            console.log('SET_TICKETS_MOVE',state.ticketsSelected)
            state.ticketsMove = _.cloneDeep(state.ticketsSelected);
            state.ticketsSelected = [];
        },
        CANCEL_MOVE_TICKET: function (state) {
            state.ticketsMove = [];
        },
        CANCEL_FIRST_MOVE_TICKET: function (state) {
            state.ticketsMove.shift();
            if(state.ticketsMove.length > 0 ? false : true){
                state.checkTicketPaid = false
            }
        },
        SET_SHOW_WAY: function (state, payload) {
            state.showWay = payload;
            window.func.setConfigWeb({showWay: payload});
        },
        SET_SHOW_NEW_FORM_TICKET: function (state, payload) {
            state.showNewFormTicket = payload;
            window.func.setConfigWeb({showNewFormTicket: payload});
        },
        SET_SHOW_ALL_TICKET_IN_SEAT: function (state, payload) {
            state.showAllTicketInSeat = payload;
            state.ticketsSelected = [];
            window.func.setConfigWeb({showAllTicketInSeat: payload});
        },
        SET_SHOW_DETAIL_TRIPS: function (state, payload) {
            state.showDetailTrips = payload;
            window.func.setConfigWeb({showDetailTrips: payload});
        },
        SET_SHOW_LIST_TRIP_V2: function (state, payload) {
            state.showListTripsItemV2 = payload;
            window.func.setConfigWeb({showListTripsItemV2: payload});
        },
        SET_SHOW_ROUTE_NAME_IN_TRIP_ITEM: function (state, payload) {
            state.showRouteInTripItem = payload;
            window.func.setConfigWeb({showRouteInTripItem: payload});
        },
        SET_TICKET_SCROLL: function (state, ticket) {
            state.ticketScroll = ticket;
            state.scrolled = false;
        },
        SET_ADDRESS_UP_TICKET_NOT_SEAT: function (state, address) {
            state.addressUpTicketNotSeat = address;
        },
        SET_ADDRESS_DOWN_TICKET_NOT_SEAT: function (state, address) {
            state.addressDownTicketNotSeat = address;
        },
        SET_LOCKED_SEAT: function (state, payload) {
            state.lockedSeat = payload;
        },
        SET_BUY_ALL_TICKET: function(state, status) {
            state.buyAllTicket = status;
        }
    },
    actions: {
        INIT: function (context) {
            var showWay = window.func.getConfigWeb('showWay') == null ? true : window.func.getConfigWeb('showWay');
            context.commit('SET_SHOW_WAY', showWay);

            var showNewFormTicket = window.func.getConfigWeb('showNewFormTicket') == null ? true : window.func.getConfigWeb('showNewFormTicket');
            context.commit('SET_SHOW_NEW_FORM_TICKET', showNewFormTicket);

            var showAllTicketInSeat = window.func.getConfigWeb('showAllTicketInSeat') == null ? false : window.func.getConfigWeb('showAllTicketInSeat');
            context.commit('SET_SHOW_ALL_TICKET_IN_SEAT', showAllTicketInSeat);

            var showDetailTrips = window.func.getConfigWeb('showDetailTrips') == null ? false : window.func.getConfigWeb('showDetailTrips');
            context.commit('SET_SHOW_DETAIL_TRIPS', showDetailTrips);

            var showListTripsItemV2 = window.func.getConfigWeb('showListTripsItemV2') == null ? false : window.func.getConfigWeb('showListTripsItemV2');
            context.commit('SET_SHOW_LIST_TRIP_V2', showListTripsItemV2);

            var showRouteInTripItem = window.func.getConfigWeb('showRouteInTripItem') == null ? false : window.func.getConfigWeb('showRouteInTripItem');
            context.commit('SET_SHOW_ROUTE_NAME_IN_TRIP_ITEM', showRouteInTripItem);

            var date = window.func.getHistory('date2') || new Date().customFormat('#DD#-#MM#-#YYYY#');
            context.commit("SET_DATE", date);
            context.commit("LOAD", "GET_ROUTE", {root: true});

            window.sendJson({
                url: window.urlDBD(window.API.routeList),
                data: {
                    page: 0,
                    count: 100,
                    companyId : window.userInfo.userInfo.companyId
                },
                success: function (data) {
                    context.commit('SET_ROUTES', data.results.result);
                    context.dispatch('SET_POINT_AND_ROUTE_HISTORY');
                    context.dispatch("GET_TRIPS");
                    context.commit("UNLOAD", "GET_ROUTE", {root: true});
                },
                functionIfError: function () {
                    context.commit("UNLOAD", "GET_ROUTE", {root: true});
                }
            });
        },
        SET_POINT_AND_ROUTE_HISTORY: function (context) {
            var hStartPointId = window.func.getHistory('startPointId2');
            var hEndPointId = window.func.getHistory('endPointId2');
            context.commit('SET_START_POINT', context.getters.pointsInRoute.map[hStartPointId] !== undefined ? context.getters.pointsInRoute.points[context.getters.pointsInRoute.map[hStartPointId]] : null);
            context.commit('SET_END_POINT', context.getters.pointsInRoute.map[hEndPointId] !== undefined ? context.getters.pointsInRoute.points[context.getters.pointsInRoute.map[hEndPointId]] : null);

            var routeId = window.func.getHistory("routeId2");
            var route = _.first($.grep(context.state.routes, function (routeInfo, r) {
                return routeInfo.routeId === routeId;
            })) || null;
            context.commit('SET_ROUTE', route);
            if (route) {
                if (!context.state.startPoint) {
                    context.commit('SET_START_POINT', route.listPoint[_.first(route.indexPointDisplay)] || _.first(route.listPoint));
                }
                if (!context.state.endPoint) {
                    context.commit('SET_END_POINT', route.listPoint[_.last(route.indexPointDisplay)] || _.last(route.listPoint));
                }
                var indexStartPoint = context.getters.mapRoutes[route.routeId].mapPoints[context.state.startPoint.id];
                var indexEndPoint = context.getters.mapRoutes[route.routeId].mapPoints[context.state.startPoint.id];
                if (indexStartPoint && indexEndPoint && indexStartPoint > indexEndPoint) {
                    context.dispatch('SWAP_POINT');
                }
            }

        },
        LOAD_SEATMAP: function (context, option){
            context.commit('LOAD_SEATMAP', option);
        },
        SWAP_POINT: function (context) {
            var startPoint = _.cloneDeep(context.state.startPoint);
            context.commit('SET_START_POINT', context.state.endPoint);
            context.commit('SET_END_POINT', startPoint);
            context.dispatch('GET_TRIPS');
        },
        SET_DATE: function (context, date) {
            context.commit('UN_SELECTED');
            context.commit('SET_DATE', date);
            context.dispatch('GET_TRIPS');
        },
        SET_TIME: function (context, time) {
            context.commit('SET_TIME', time);
            context.dispatch('GET_TRIPS');
        },
        SET_USE_FILTER_TIME: function (context, payload) {
            context.commit('SET_USE_FILTER_TIME', payload);
            context.dispatch('GET_TRIPS');
        },
        LOAD_CHANGE_SEATMAP: function (context, tickets) {
            let listIds = []
            if(tickets && tickets.length > 0){
                $.each(tickets, function (t, ticket) {
                    listIds.push(ticket.seat.seatId)
                })
            }
            context.commit('LOAD_CHANGE_SEATMAP', listIds);
        },
        SET_ROUTE: function (context, _routeId) {
            context.commit('UN_SELECTED');
            var route = _.first($.grep(context.state.routes, function (routeInfo, r) {
                return routeInfo.routeId === _routeId;
            })) || null;
            context.commit('SET_ROUTE', route);
            window.func.setHistory({routeId2: route ? route.routeId : null});
            if (route) {
                var indexStartPoint = false && context.state.startPoint && context.getters.mapRoutes[route.routeId].mapPoints[context.state.startPoint.id] !== undefined ? context.getters.mapRoutes[route.routeId].mapPoints[context.state.startPoint.id].index : null;
                var indexEndPoint = false && context.state.endPoint && context.getters.mapRoutes[route.routeId].mapPoints[context.state.endPoint.id] !== undefined ? context.getters.mapRoutes[route.routeId].mapPoints[context.state.endPoint.id].index : null;
                var get = indexStartPoint == null || indexEndPoint == null;
                if (indexStartPoint == null) {// trường hợp không có điểm bắt đầu trong tuyến
                    context.commit('SET_START_POINT', route.listPoint[_.first(route.indexPointDisplay)] || _.first(route.listPoint));
                    indexStartPoint = route.listPoint[_.first(route.indexPointDisplay)] ? _.first(route.indexPointDisplay) : 0;
                }
                if (indexEndPoint == null) {
                    context.commit('SET_END_POINT', route.listPoint[_.last(route.indexPointDisplay)] || _.last(route.listPoint));
                    indexEndPoint = route.listPoint[_.last(route.indexPointDisplay)] ? _.last(route.indexPointDisplay) : route.listPoint.length - 1;
                }
                if (indexStartPoint > indexEndPoint) {
                    context.dispatch('SWAP_POINT');
                } else if (get) {
                    context.dispatch('GET_TRIPS');
                }
            }else{
                context.dispatch('GET_TRIPS');
            }
        },
        SET_START_POINT: function (context, point) {
            context.commit('UN_SELECTED');
            context.commit('SET_START_POINT', point);
            context.dispatch('GET_TRIPS');
        },
        SET_END_POINT: function (context, point) {
            context.commit('UN_SELECTED');
            context.commit('SET_END_POINT', point);
            context.dispatch('GET_TRIPS');
        },
        LOAD_TICKET_INFORM: function (context, tickets) {
            console.log('LOAD_TICKET_INFORM')
            let seats = []
            $.each(tickets, function (t, ticket) {
                seats.push(ticket.seat.seatId)
            })
            context.commit('LOAD_TICKET_INFORM', seats);
        },
        GET_TRIPS: function (context) {
            if (!context.state.startPoint || !context.state.endPoint || !context.state.date) {
                return false;
            }
            context.commit("SET_TRIPS", []);
            context.state.trip = null;
            var params = {
                page: 0,
                count: 300,
                timeZone: 7,
                sortSelections: [
                    {fieldName: "startTime", ascDirection: true}
                ],
                companyId: window.userInfo.userInfo.companyId,
                // routeId: context.state.route.routeId,
                date: window.func.dateStr(context.state.date),
                startPoint: context.state.startPoint.id,
                endPoint: context.state.endPoint.id,
                searchPointOption: 1,
                source: 1,
                platform: 1,
                newVersion : true
            };
            if(context.state.route){
                params.routeId = context.state.route.routeId;
            }
            try{
                if(
                    (context.state.route&&
                        context.rootState.CompanyConfig.companyConfig.CONFIG.SYNC_CONFIG_SELL_FORM.value&&
                        context.rootState.CompanyConfig.companyConfig.CONFIG.DISPLAY_TIME_AT_FIRST_POINT.value)||
                    context.rootState.config_data.showStartTimeReality){
                    params.startPoint = _.first(context.state.route.listPoint).id;
                    params.endPoint = _.last(context.state.route.listPoint).id;
                }
            }catch(e){

            }
            if(context.rootState)
                if (context.state.useFilterTime) {
                    params.startTimeLimit = context.getters.limitTime.start;
                    params.endTimeLimit = context.getters.limitTime.end;
                }
            window.func.setHistory({
                routeId2: context.state.route ? context.state.route.routeId : null,
                date2: context.state.date,
                startPointId2: context.state.startPoint.id,
                endPointId2: context.state.endPoint.id
            });
            context.commit("LOAD", "GET_TRIPS", {root: true});
            window.sendJson({
                url: window.urlDBD(window.API.planForTripSearch),
                data: params,
                success: function (data) {
                    let newTrips = [];
                    $.each(data.results.trips,function (t,trip) {
                        if(context.getters.mapRoutes[trip.route.id] !== undefined){
                            newTrips.push(trip);
                        }
                    })
                    $.each(newTrips, function (t, trip) {
                        trip.timeAndEmtyseat = window.func.msToTime(trip.startTime) + ' - ' + trip.totalEmptySeat + ' Ghế trống';
                    });
                    context.dispatch("SET_TRIPS", newTrips);
                    context.dispatch("SET_TRIP");
                    context.commit("UNLOAD", "GET_TRIPS", {root: true});
                },
                functionIfError: function (data) {
                    context.commit("UNLOAD", "GET_TRIPS", {root: true});
                }
            });
        },
        SHOW_FORM_SELL : function (context, option) {
            context.commit("SHOW_FORM_SELL", option)
        },
        SET_TRIPS: function (context, _trips) {
            var listTrip = [];
            $.each(_trips, function (t, trip) {
                var tr = trip;
                tr.seatMapFake = null;
                listTrip.push(tr);
            });
            context.commit('SET_TRIPS', listTrip);
            $.each(context.state.rt_trips, function (i, e) {
                if(typeof e === "function"){
                    e();
                }else{
                    e.off();
                }
            });
            context.commit('SET_RT_TRIPS', []);
            var rt_trips = [];
            $.each(context.state.trips, function (i, e) {
                var cnn = firestoreDatabase.collection("trip").doc(e.tripId)
                    .onSnapshot(function(doc) {
                        let snap = doc.data();
                        try {
                            let data = _.cloneDeep(snap);
                            var orderTicket = orderTickets(data.seatMap,
                                data._ticketsInfo||[],
                                data._ticketsCanceled||[],
                                data._printedTickets||[],
                                {
                                    totalSeat: data.totalSeat,
                                    indexStartPoint: context.getters.mapRoutes[context.state.trip.route.id].mapPoints[context.state.startPoint.id].index,
                                    indexEndPoint: context.getters.mapRoutes[context.state.trip.route.id].mapPoints[context.state.endPoint.id].index,
                                    indexPointInRoute: context.getters.mapRoutes[context.state.trip.route.id].mapPoints
                                });
                            context.commit('CHANGE_TRIP_INFO', {index: i, data: data, orderTicket: orderTicket})
                            context.dispatch('SET_RT_TRIP');
                        } catch (exception) {
                            console.warn(snap, e.tripId, exception);
                            // self.refreshTripToFirebase(e.tripId);
                        }
                    });
                // var cnn = firebaseDatabase.ref().child('trips').child(e.tripId).child('tripInfo');
                // cnn.on('value', function (snapshot) {
                //     let snap = _.cloneDeep(snapshot.val());
                //     try {
                //         let data = _.cloneDeep(snap);
                //         context.commit('CHANGE_TRIP_INFO', {index: i, data: data})
                //     } catch (exception) {
                //         console.warn(snap, e.tripId, exception);
                //         // self.refreshTripToFirebase(e.tripId);
                //     }
                // });
                // var cnn1 = firebaseDatabase.ref().child('trips').child(e.tripId).child('goods');
                // cnn1.on('value', function (snapshot) {
                //     let snap = _.cloneDeep(snapshot.val());
                //     try {
                //         let data = _.cloneDeep(snap);
                //         context.commit('CHANGE_TRIP_GOODS', {index: i, data: data})
                //     } catch (exception) {
                //         console.warn(snap, e.tripId, exception);
                //         // self.refreshTripToFirebase(e.tripId);
                //     }
                // });
                var cnn2 = firebaseDatabase.ref().child('trips').child(e.tripId).child('printedTickets');
                cnn2.on('value', function (snapshot) {
                    let snap = _.cloneDeep(snapshot.val());
                    try {
                        let data = _.cloneDeep(snap);
                        context.commit('CHANGE_TRIP_PRINTED_TICKETS', {index: i, data: data})
                    } catch (exception) {
                        console.warn(snap, e.tripId, exception);
                        // self.refreshTripToFirebase(e.tripId);
                    }
                });
                var cnn3 = firebaseDatabase.ref().child('trips').child(e.tripId).child('lockedSeat');
                cnn3.on('value', function (snapshot) {
                    let snap = _.cloneDeep(snapshot.val());
                    try {
                        let data = _.cloneDeep(snap);
                        context.commit('CHANGE_TRIP_LOCKED_SEAT', {index: i, data: data})
                    } catch (exception) {
                        console.warn(snap, e.tripId, exception);
                        // self.refreshTripToFirebase(e.tripId);
                    }
                });
                rt_trips.push(cnn);
                // rt_trips.push(cnn1);
                rt_trips.push(cnn2);
                rt_trips.push(cnn3);
            });
            context.commit('SET_RT_TRIPS', rt_trips);
        },
        SET_TRIP: function (context, _tripId) {
            console.log('SET_TRIP')
            $.each(context.state.rt_trip,function (r,rt) {
                rt();
            });
            context.state.rt_trip = [];
            let tid = _tripId;
            let hs_tripId = func.getHistory('tripId2') || null;
            if (!_tripId && hs_tripId) {
                $.each(context.state.trips, function (t, trip) {
                    if (trip.tripId == hs_tripId && (!context.state.route||context.state.route.routeId === trip.route.id)) {
                        tid = hs_tripId;
                        return false;
                    }
                });

            }
            if(!tid){
                try{
                    let currentTime = func.timeToMs(new Date().customFormat('#hhhh#:#mm#'))
                    let tripNearTime = context.state.trips.find(trip => trip.startTime > currentTime && (!context.state.route||context.state.route.routeId === trip.route.id))
                    let date = parseFloat(window.func.dateStr(context.state.date))
                    let currentDate = parseFloat(new Date().customFormat('#YYYY##MM##DD#'));
                    tid = tripNearTime && date==currentDate ? tripNearTime.tripId : null;

                }catch (e) {
                    console.warn(e);
                }
            }
            context.commit('SET_TRIP', tid);
            if(context.state.trip){
                let rt_ticketsInfo = firestoreDatabase.collection("trip").doc(context.state.trip.tripId).collection('ticketsInfo')
                    .onSnapshot(function (querySnapshot) {
                        let tickets = [];
                        querySnapshot.forEach(function(doc) {
                            tickets.push(doc.data());
                        });
                        context.commit('SET_RT_TICKETS_INFO',tickets);
                        context.dispatch('SET_RT_TRIP');
                    });
                context.state.rt_trip.push(rt_ticketsInfo);
                let rt_ticketsCanceled = firestoreDatabase.collection("trip").doc(context.state.trip.tripId).collection('ticketsCanceled')
                    .onSnapshot(function (querySnapshot) {
                        let tickets = [];
                        querySnapshot.forEach(function(doc) {
                            tickets.push(doc.data());
                        });
                        context.commit('SET_RT_TICKETS_CANCELED',tickets);
                        context.dispatch('SET_RT_TRIP');
                    });
                context.state.rt_trip.push(rt_ticketsCanceled);
                let rt_ticketsTransfer = firestoreDatabase.collection("trip").doc(context.state.trip.tripId).collection('ticketsTransfer')
                    .onSnapshot(function (querySnapshot) {
                        let tickets = [];
                        querySnapshot.forEach(function(doc) {
                            tickets.push(doc.data());
                        });
                        context.commit('SET_RT_TICKETS_TRANSFER',tickets);
                        context.dispatch('SET_RT_TRIP');
                    });
                context.state.rt_trip.push(rt_ticketsTransfer);
                // var rt_trip = firebaseDatabase.ref().child('trips').child(context.state.trip.tripId);
                // rt_trip.on('value', function (snapshot) {
                //     let snap = _.cloneDeep(snapshot.val());
                //
                // });
                // context.state.rt_trip = rt_trip;
            }
            context.commit('SET_ADDRESS_UP_TICKET_NOT_SEAT', null);
            context.commit('SET_ADDRESS_UP_TICKET_NOT_SEAT', null);
            if (tid) {
                context.dispatch('SET_POLICY_TRIP');
            }
        },
        SET_RT_TRIP : function(context){
            try {
                var orderTicket = orderTickets(context.state.trip.seatMap,
                    context.state.trip._ticketsInfo||[],
                    context.state.trip._ticketsCanceled||[],
                    context.state.trip._printedTickets||[],
                    {
                        totalSeat: context.state.trip.totalSeat,
                        indexStartPoint: context.getters.mapRoutes[context.state.trip.route.id].mapPoints[context.state.startPoint.id].index,
                        indexEndPoint: context.getters.mapRoutes[context.state.trip.route.id].mapPoints[context.state.endPoint.id].index,
                        indexPointInRoute: context.getters.mapRoutes[context.state.trip.route.id].mapPoints
                    });
                if (context.state._lockedSeat && context.state.ticketsSelectedType == 0 && context.state.ticketsSelected) {
                    var newTicketsSelected = [];
                    $.each(context.state.ticketsSelected, function (t, ticket) {
                        let push = true;
                        $.each(context.state._lockedSeat, function (ls, lockS) {
                            if (new Date().getTime() < lockS.time + 1200000 && userInfo.userInfo.id !== lockS.user.id) {// không phải là người đang click
                                $.each(lockS.listSeat, function (s, seat) {
                                    if (seat.seatId === ticket.seat.seatId && seat.seatType === ticket.seat.seatType) {
                                        push = false;
                                        return false;
                                    }
                                });
                                if (!push) {
                                    return false;
                                }
                            }
                        })
                        if (push) {
                            newTicketsSelected.push(ticket);
                        }
                    });
                    if (context.state.ticketsSelected.length != newTicketsSelected.length) {
                        notify("Có người đang đặt vé ghế bạn chọn. vui lòng chon ghế khác")
                    }
                    if( context.state.ticketsSelected.length !== newTicketsSelected.length){
                        context.commit('SET_TICKETS_SELECTED', newTicketsSelected);
                    }
                }
                context.commit('CHANGE_TRIP_DETAIL', orderTicket)
            } catch (exception) {
                console.warn(snap, context.state.trip.tripId, exception);
                // self.refreshTripToFirebase(e.tripId);
            }
        },
        SET_POLICY_TRIP: function (context) {
            if (context.state.trip) {
                context.commit("LOAD", "SET_POLICY_TRIP", {root: true});
                sendJson({
                    url: urlDBD(API.tripGetPricePolicy),
                    data: {
                        tripId: context.state.trip.tripId,
                        startDate: func.dateToMS(context.state.date),
                        startTime: context.state.trip.startTimeReality,
                        pointUpId: context.state.startPoint.id,
                        pointDownId: context.state.endPoint.id
                    },
                    success: function (data) {
                        context.commit("SET_POLICY_TRIP", data.results.pricePolicy);
                        context.commit("UNLOAD", "SET_POLICY_TRIP", {root: true});
                    },
                    error: function (err) {
                        context.commit("UNLOAD", "SET_POLICY_TRIP", {root: true});
                    }
                })
            }
        },
        PUSH_SELL_TICKET: function (context, ticket) {
            context.commit('PUSH_SELL_TICKET', ticket);
            context.dispatch('LOCK_SEAT');
        },
        UN_PUSH_SELL_TICKET: function (context, ticket) {
            context.commit('UN_PUSH_SELL_TICKET', ticket);
            context.dispatch('LOCK_SEAT');
        },
        PUSH_UPDATE_TICKET: function (context, ticket) {
            context.commit('PUSH_UPDATE_TICKET', ticket);
        },
        PUSH_TICKET_SELECTED_SAME_ADDRESS: function (context, ticket) {
            context.commit('PUSH_TICKET_SELECTED_SAME_ADDRESS', ticket);
        },
        PUSH_TICKET_SELECTED_SAME_POINTTYPE: function (context, ticket) {
            context.commit('PUSH_TICKET_SELECTED_SAME_POINTTYPE', ticket);
        },
        UN_PUSH_UPDATE_TICKET: function (context, ticket) {
            context.commit('UN_PUSH_UPDATE_TICKET', ticket);
        },
        UN_LOAD_TICKET_INFORM: function (context) {
            console.log('UN_LOAD_TICKET_INFORM')
            context.commit('UN_LOAD_TICKET_INFORM');
        },
        UN_SELECTED: function (context) {
            context.commit('UN_SELECTED');
            context.dispatch('UN_LOCK_SEAT');
        },
        SET_TICKETS_MOVE: function (context) {
            context.commit('SET_TICKETS_MOVE');
        },
        DO_MOVE_TICKET: function (context, seatInfo) {
            let listIds = seatInfo.seatId
            if(seatInfo.ticketSwap){
                let ticketA = _.cloneDeep(context.state.ticketsMove[0]);
                let ticketB = _.cloneDeep(seatInfo.ticketSwap);
                context.commit("LOAD_ONE_TICKET",listIds);
                context.commit('CANCEL_FIRST_MOVE_TICKET');
                sendJson({
                    url: urlDBD(API.ticketSwap),
                    data: [ticketA.ticketId,ticketB.ticketId],
                    success: function (data) {
                        firebaseDatabase.ref().child('trips').child(ticketA.tripId).child('printedTickets').child(ticketA.ticketId).remove();
                        firebaseDatabase.ref().child('trips').child(ticketB.tripId).child('printedTickets').child(ticketB.ticketId).remove();
                        notify("Đã chuyển", 'success');
                        context.commit("CANCEL_LOAD_ONE_TICKET");
                    },
                    functionIfError: function (err) {
                        context.commit("CANCEL_LOAD_ONE_TICKET");
                    }
                })
            }else{
                context.commit("LOAD_ONE_TICKET",listIds);
                let fb = {
                    tripId: context.state.ticketsMove[0].tripId,
                    ticketId: context.state.ticketsMove[0].ticketId,
                }
                let data = {
                    tripId: context.state.trip.tripId,
                    ticketId: context.state.ticketsMove[0].ticketId,
                    seat: {
                        seatId: seatInfo.seatId,
                        seatType: seatInfo.seatType
                    }
                };
                if (context.state.ticketsMove[0].routeInfo.id !== context.state.trip.route.id) {
                    data.pointUp = context.state.startPoint;
                    if (context.state.ticketsMove[0].pointUp.pointType == 1 || context.state.ticketsMove[0].pointUp.pointType == 2) {
                        data.pointUp.pointType = context.state.ticketsMove[0].pointUp.pointType;
                        data.pointUp.address = context.state.ticketsMove[0].pointUp.address;
                    }
                    data.pointDown = context.state.endPoint;
                    if (context.state.ticketsMove[0].pointDown.pointType == 1 || context.state.ticketsMove[0].pointDown.pointType == 2) {
                        data.pointDown.pointType = context.state.ticketsMove[0].pointDown.pointType;
                        data.pointDown.address = context.state.ticketsMove[0].pointDown.address;
                    }
                }
                if (context.state.ticketsMove[0].ticketStatus == 0 || context.state.ticketsMove[0].ticketStatus == 2) {
                    data.ticketStatus = 7;
                }
                context.commit('CANCEL_FIRST_MOVE_TICKET');
                // context.commit("LOAD", "MOVE_SEAT" + seatInfo.seatId, {root: true});
                sendJson({
                    url: urlDBD(API.ticketTransfer),
                    data: [data],
                    success: function (data) {
                        firebaseDatabase.ref().child('trips').child(fb.tripId).child('printedTickets').child(fb.ticketId).remove();
                        // notify("Đang xử lý...", 'info');
                        context.commit("CANCEL_LOAD_ONE_TICKET");
                    },
                    functionIfError: function (err) {
                        context.commit("CANCEL_LOAD_ONE_TICKET");
                    }
                })
            }
        },
        CHECK_TICKET: function (context, payload) {
            let params = [];
            $.each(context.state.ticketsSelected, function (t, ticket) {
                let tk = {
                    ticketId: ticket.ticketId,
                    callStatus: payload,
                }
                if (ticket.ticketStatus == 2) {
                    tk.ticketStatus = 7;
                    tk.overTime = 0;
                }
                params.push(tk);
            });
            context.commit("LOAD", "CHECK_TICKET", {root: true});
            sendJson({
                url: urlDBD(API.ticketUpdate),
                data: params,
                success: function (data) {
                    context.commit("UNLOAD", "CHECK_TICKET", {root: true});
                },
                functionIfError: function () {
                    context.commit("UNLOAD", "CHECK_TICKET", {root: true});
                }
            })
        },
        DELETE_TICKET_ONLINE_FIREBASE: function (context, payload) {
            $.each(payload.listTicketId, function (t, ticketId) {
                firebaseDatabase.ref().child('ticketOnlines').child(payload.routeInfo.id).child(ticketId).remove();
            })
        },
        LOCK_SEAT: function (context, payload) {
            if (context.state.lockedSeat) {
                context.dispatch('UN_LOCK_SEAT');
            }
            let listSeat = [];
            $.each(context.state.ticketsSelected, function (t, ticket) {
                listSeat.push(ticket.seat);
            })
            let data = {
                user: {
                    id: userInfo.userInfo.id,
                    fullName: userInfo.userInfo.fullName
                },
                startPointId: context.state.startPoint.id,
                endPointId: context.state.endPoint.id,
                listSeat: listSeat,
                tripId: context.state.trip.tripId,
                time: new Date().getTime(),
            }
            firebaseDatabase.ref().child('trips').child(context.state.trip.tripId).child('lockedSeat').child(userInfo.userInfo.id).set(data);
            context.commit('SET_LOCKED_SEAT', data);
        },
        UN_LOCK_SEAT: function (context, userId) {
            if (context.state.lockedSeat) {
                firebaseDatabase.ref().child('trips').child(context.state.lockedSeat.tripId).child('lockedSeat').child(userId || userInfo.userInfo.id).remove();
                context.commit('SET_LOCKED_SEAT', null);
            } else {
                firebaseDatabase.ref().child('trips').child(context.state.trip.tripId).child('lockedSeat').child(userId || userInfo.userInfo.id).remove();
            }
        },
        REFRESH_TRIP: function (context, _tripId) {
            let self = this;
            context.dispatch('SET_POLICY_TRIP');
            context.commit("LOAD", 'refreshTripToFirebase', {root: true});
            sendJson({
                url: urlDBD(API.tripRefreshFirebase),
                data: {
                    tripId: _tripId,
                },
                success: function (data) {
                    // notify("refresh thành công",'success');
                    context.commit("UNLOAD", 'refreshTripToFirebase', {root: true});
                },
                functionIfError: function (err) {
                    context.commit("UNLOAD", 'refreshTripToFirebase', {root: true});
                }
            })
        },
    }
}
