import { makeSeatMap,orderTickets } from '../library.js';

export default {
    namespaced : true,
    state : {
        loading : {
            getRoutes : {
                loading : true,
                error : false
            }
        },
        routes : [],
        date : '',
        route : null,
        startPoint : null,
        endPoint : null,
        trips : [],
        trip :null,
        indexPointInRoute : null,
        rt_trips : [],
        floorActive : 0,
        ticketsSelected : [],
        ticketsSelectedType : 0, // 0 là bán vé ,1 là cập nhật
        lastPushTicketsSelected : null,
        ticketsMove : [],
        showWay : false,
        ticketScroll : null,
        scrolled : true,
        showAllTicketInSeat : false,
        showDetailTrips : false,
        addressUpTicketNotSeat : null,
        addressDownTicketNotSeat : null,
        lockedSeat : null,
    },
    getters : {
        findAddressTicketNotSeat : function (state) {
            var data = {
                up : [],
                down : []
            };
            try {
                var trip = _.cloneDeep(state.trip);
                $.each(trip.ticketsNotSeat,function (t,tk) {
                    if(!data.up.includes(tk.pointUp.address)){
                        data.up.push(tk.pointUp.address);
                    }
                    if(!data.down.includes(tk.pointDown.address)){
                        data.down.push(tk.pointDown.address);
                    }
                });
                return data;
            }catch (e) {
                return data;
            }
        },
        totalMoneyTrip : function (state) {
            var totalMoney = {
                totalPaidMoney: 0,
                totalAgencyPrice: 0
            };
            try{
                var trip = _.cloneDeep(state.trip);
                $.each(trip.ticketsInSeatMap,function (t,ticketsInfo) {
                    totalMoney.totalPaidMoney += ticketsInfo.paidMoney;
                    totalMoney.totalAgencyPrice += ticketsInfo.agencyPrice;
                });
                $.each(trip.ticketsNotSeat,function (t,ticketsInfo) {
                    totalMoney.totalPaidMoney += ticketsInfo.paidMoney;
                    totalMoney.totalAgencyPrice += ticketsInfo.agencyPrice;
                });
                return totalMoney;
            }catch (e) {
                return totalMoney;
            }
        },
        allowPastDaysToSellTicket : function (state) {
            var currentDate = moment([moment().format('YYYY'), moment().format('MM') - 1, moment().format('DD')]);
            var dateNew =  moment([moment(func.dateToMS(state.date)).format('YYYY'), moment(func.dateToMS(state.date)).format('MM') - 1, moment(func.dateToMS(state.date)).format('DD')]);
            var num = dateNew.diff(currentDate, 'days');
            return num+userInfo.pastDaysToSellTicket>0?true:false;
        },
        lockTripAfterStart : function (state) {
            var trip = state.trip;
            var time = userInfo.timeLockTripAfterStart;
            if(trip&&trip.tripStatus==2&&time!=null&&time>=0&&trip.timeStartTrip>0&&(trip.timeStartTrip+time<new Date().getTime())){
                return false;
            }
            return true;
        }
    },
    mutations : {
        SET_DATE : function (state,date) {
            state.date=date;
            window.func.setHistory({date:date});
        },
        SET_ROUTE : function (state,_routeId) {
            var routeId = window.func.getHistory("routeId");
            routeId = _routeId ? _routeId : routeId;
            if(state.routes.length > 0){
                if(routeId === null){
                    state.route = _.first(state.routes);
                }else{
                    state.route = _.first(state.routes);
                    window.$.each(state.routes,function (r,route) {
                        if(route.routeId===routeId){
                            state.route = route;
                        }
                    });
                }
            }else{
                state.route = null;
            }
            if(state.route){
                window.func.setHistory({routeId:state.route.routeId});
                var indexPointInRoute = {};
                window.$.each(state.route.listPoint,function (p,point) {
                    indexPointInRoute[point.id] = p;
                });
                state.indexPointInRoute = indexPointInRoute;
            }else{
                state.indexPointInRoute = null;
            }
        },
        SET_POINT: function(state){
            var hStartPointId=window.func.getHistory('startPointId');
            var hEndPointId=window.func.getHistory('endPointId');
            var indexStartPoint = state.route&&state.route.indexPointDisplay&&state.route.listPoint.length>_.first(state.route.indexPointDisplay)?_.first(state.route.indexPointDisplay):0;
            var indexEndPoint = state.route&&state.route.indexPointDisplay&&state.route.listPoint.length>_.last(state.route.indexPointDisplay)?_.last(state.route.indexPointDisplay):state.route.listPoint.length-1;
            state.startPoint = state.indexPointInRoute[hStartPointId] ? state.route.listPoint[state.indexPointInRoute[hStartPointId]] : state.route.listPoint[indexStartPoint];
            state.endPoint = state.indexPointInRoute[hEndPointId] ? state.route.listPoint[state.indexPointInRoute[hEndPointId]] : state.route.listPoint[indexEndPoint];
        },
        SET_START_POINT: function(state,point){
            state.startPoint = point;
        },
        SET_END_POINT: function(state,point){
            state.endPoint = point;
        },
        SET_ROUTES : function (state,routes) {
            state.routes = routes;
        },
        SET_TRIPS : function (state,trips) {
            state.trips = [];
            $.each(trips ,function (t,trip) {
                trip.seatMapFake = null;
                state.trips.push(trip);
            });
            $.each(state.rt_trips, function (i, e) {
                e.off();
            });
            state.rt_trips = [];
            $.each(state.trips, function (i, e) {
                let cnn = firebaseDatabase.ref().child('trips').child(e.tripId);
                cnn.on('value', function (snapshot) {
                    let data = snapshot.val();
                    try{
                        e.startTimeReality = data.tripInfo.startTimeReality;
                        e.tripStatus = data.tripInfo.tripStatus;
                        e.note = data.tripInfo.note;
                        if(data.tripInfo.vehicle){
                            e.vehicle = data.tripInfo.vehicle;
                        }
                        e.drivers = data.tripInfo.drivers;
                        e.assistants = data.tripInfo.assistants;
                        e.totalSeat = data.tripInfo.totalSeat;
                        e.seatMap = data.tripDetail.seatMap;
                        e.timeStartTrip = data.tripInfo.timeStartTrip;
                        e._ticketsInfo = data.tripDetail.ticketsInfo||[];
                        e._ticketsCanceled = data.tripDetail.ticketsCanceled||[];
                        e._printedTickets = data.printedTickets;
                        e._lockedSeat = data.lockedSeat;
                        try{
                            e.location = {
                                lat : data.location.latitude,
                                lng : data.location.longitude
                            }
                        }catch(e){
                            e.location = null;
                        }
                        let orderTicket = orderTickets(e.seatMap,e._ticketsInfo,e._ticketsCanceled,e._printedTickets,{
                            totalSeat : e.totalSeat,
                            indexStartPoint : state.indexPointInRoute[state.startPoint.id],
                            indexEndPoint : state.indexPointInRoute[state.endPoint.id],
                            indexPointInRoute :state.indexPointInRoute
                        });
                        e.ticketsInSeatMap = orderTicket.ticketsInSeatMap;
                        e.ticketsInfo = orderTicket.ticketsInSeatMap;
                        e.ticketsEndTime = orderTicket.ticketsEndTime;
                        e.ticketsCanceled = orderTicket.ticketsCanceled;
                        e.ticketsNotSeat = orderTicket.ticketsNotSeat;
                        e.seatMapFake = orderTicket.seatMapFake;
                        e.totalEmptySeat = orderTicket.totalEmptySeat;
                        if(state.trip&&e.tripId == state.trip.tripId){
                            if(!state.scrolled&&state.ticketScroll){
                                state.scrolled = true;
                            }
                            // self.selected.trip = e;
                            // self.scrollToTicket();
                            // commit("UNLOAD","realTimeTrip",{root:true});
                            // self.unload("realTimeTrip");
                            if(data.lockedSeat&&state.ticketsSelectedType==0&&state.ticketsSelected){
                                var newTicketsSelected = [];
                                $.each(state.ticketsSelected,function (t,ticket) {
                                    let push = true;
                                    $.each(data.lockedSeat,function (ls,lockS) {
                                        if(new Date().getTime()<lockS.time+1200000&&userInfo.userInfo.id!==lockS.user.id){// không phải là người đang click
                                            $.each(lockS.listSeat,function (s,seat) {
                                                if(seat.seatId===ticket.seat.seatId&&seat.seatType === ticket.seat.seatType){
                                                    push = false;
                                                    return false;
                                                }
                                            });
                                            if(!push){
                                                return false;
                                            }
                                        }
                                    })
                                    if(push){
                                        newTicketsSelected.push(ticket);
                                    }
                                });
                                if(state.ticketsSelected.length != newTicketsSelected.length){
                                    notify("Có người đang đặt vé ghế bạn chọn. vui lòng chon ghế khác")
                                }
                                state.ticketsSelected = newTicketsSelected;
                            }
                        }
                    }catch(exception){
                        console.warn(data,e.tripId,exception);
                        // self.refreshTripToFirebase(e.tripId);
                    }
                });
                state.rt_trips.push(cnn);
            });
        },
        UPDATE_SET_TRIPS : function(state){
            $.each(state.trips, function (i, e) {
                let orderTicket = orderTickets(e.seatMap, e._ticketsInfo, e._ticketsCanceled, e._printedTickets, {
                    totalSeat: e.totalSeat,
                    indexStartPoint: state.indexPointInRoute[state.startPoint.id],
                    indexEndPoint: state.indexPointInRoute[state.endPoint.id],
                    indexPointInRoute :state.indexPointInRoute
                });
                e.ticketsInSeatMap = orderTicket.ticketsInSeatMap;
                e.ticketsInfo = orderTicket.ticketsInSeatMap;
                e.ticketsEndTime = orderTicket.ticketsEndTime;
                e.ticketsCanceled = orderTicket.ticketsCanceled;
                e.ticketsNotSeat = orderTicket.ticketsNotSeat;
                e.seatMapFake = orderTicket.seatMapFake;
                e.totalEmptySeat = orderTicket.totalEmptySeat;
            });
        },
        SET_TRIP : function(state,_tripId){
            state.trip = null;
            if(!_tripId){// khi load ra thì rơi vào TH này, click thì rơi vào TH sau
                let tripId = func.getHistory('tripId');
                if(state.trips.length > 0){
                    if(!tripId){
                        state.trip = null; // _.cloneDeep(self.trips[0])
                    }else{
                        state.trip = null;
                        $.each(state.trips,function (t,trip) {
                            if(tripId===trip.tripId){
                                state.trip = trip;
                                return false;
                            }
                        });
                    }
                }else {
                    state.trip = null;
                }
            }else{
                $.each(state.trips,function (t,trip) {
                    if(trip.tripId==_tripId){
                        state.trip = trip;
                        return false;
                    }
                });
            }
            if(state.route&&state.date){
                if(state.trip!==null){
                    // this.getPolicyTrip();
                    func.setHistory({tripId : state.trip.tripId,routeId : state.route.routeId,date:state.date});
                }else{
                    func.setHistory({routeId : state.route.routeId,date:state.date});
                }
            }
            // khi thay chuyến thì tầng về 0
            state.floorActive = 0;
            state.ticketsSelected = [];
        },
        SET_POLICY_TRIP : function(state,policy){
            state.trip.commissions = policy.commissions ? policy.commissions : null;
            state.trip.promotions = policy.promotions ? policy.promotions : null;
            state.trip.additions = policy.additionPrice ? policy.additionPrice : null;
            state.trip.cancelTicketPolicy = policy.cancelTicketPolicy ? policy.cancelTicketPolicy : null;
        },
        SET_FLOOR_ACTIVE : function (state,floor) {
            state.floorActive = floor;
        },
        PUSH_SELL_TICKET : function (state, ticket) {
            if(state.ticketsSelectedType==1){
                state.ticketsSelected = [];
            }
            state.ticketsSelectedType = 0;
            state.ticketsSelected.push(ticket);
        },
        UN_PUSH_SELL_TICKET : function (state,ticket) {
            var index = null;
            state.ticketsSelected.forEach(function (tk,t) {
                if(tk.seat.seatId == ticket.seat.seatId){
                    index = t;
                    return false;
                }
            });
            if(index!==null){
                state.ticketsSelected.splice(index,1);
            }
        },
        PUSH_UPDATE_TICKET : function (state,ticket) {
            var listTicketCode = [];
            if(state.ticketsSelectedType==1&&state.ticketsSelected.length>0){
                $.each(state.ticketsSelected,function (t,tk) {
                    if(!listTicketCode.includes(tk.ticketCode)){
                        listTicketCode.push(tk.ticketCode);
                    }
                });
            }
            if(state.ticketsSelectedType==0||state.ticketsSelected.length==0||listTicketCode.length==0||!listTicketCode.includes(ticket.ticketCode)){
                var tksSlt = state.ticketsSelectedType==0?[]:state.ticketsSelected;
                state.ticketsSelected = [];
                state.ticketsSelectedType = 1;
                var status = func.checkTicketStatus(ticket);
                if(status.mode === 'endTime'){
                    $.each(state.trip.ticketsEndTime,function (t,tk) {
                        if(tk.ticketCode===ticket.ticketCode){
                            tksSlt.push(tk);
                        }
                    });
                }else if(status.mode === 'cancel'){
                    $.each(state.trip.ticketsCanceled,function (t,tk) {
                        if(tk.ticketCode===ticket.ticketCode){
                            tksSlt.push(tk);
                        }
                    });
                }else{
                    $.each(state.trip.ticketsInSeatMap,function (t,tk) {
                        if(tk.ticketCode===ticket.ticketCode){
                            tksSlt.push(tk);
                        }
                    });
                    $.each(state.trip.ticketsNotSeat,function (t,tk) {
                        if(tk.ticketCode===ticket.ticketCode){
                            tksSlt.push(tk);
                        }
                    });
                }

                state.ticketsSelected = tksSlt;
            }else{
                state.ticketsSelected.push(ticket);
            }
        },
        UN_PUSH_UPDATE_TICKET : function (state,ticket) {
            var index = null;
            state.ticketsSelected.forEach(function (tk,t) {
                if(tk.ticketId == ticket.ticketId){
                    index = t;
                    return false;
                }
            });
            if(index!==null){
                state.ticketsSelected.splice(index,1);
            }
        },
        UN_SELECTED : function (state) {
            state.ticketsSelected = [];
        },
        SET_TICKETS_MOVE : function (state) {
            state.ticketsMove = _.cloneDeep(state.ticketsSelected);
            state.ticketsSelected = [];
        },
        CANCEL_MOVE_TICKET : function (state) {
            state.ticketsMove = [];
        },
        CANCEL_FIRST_MOVE_TICKET : function (state) {
            state.ticketsMove.shift();
        },
        SET_SHOW_WAY : function (state,payload) {
            state.showWay = payload;
            window.func.setConfigWeb({showWay:payload});
        },
        SET_SHOW_ALL_TICKET_IN_SEAT : function (state,payload) {
            state.showAllTicketInSeat = payload;
            state.ticketsSelected = [];
            window.func.setConfigWeb({showAllTicketInSeat:payload});
        },
        SET_SHOW_DETAIL_TRIPS : function (state,payload) {
            state.showDetailTrips = payload;
            window.func.setConfigWeb({showDetailTrips:payload});
        },
        SET_TICKET_SCROLL : function (state,ticket) {
            state.ticketScroll = ticket;
            state.scrolled = false;
        },
        SET_ADDRESS_UP_TICKET_NOT_SEAT : function(state,address){
            state.addressUpTicketNotSeat = address;
        },
        SET_ADDRESS_DOWN_TICKET_NOT_SEAT : function(state,address){
            state.addressDownTicketNotSeat = address;
        },
        SET_LOCKED_SEAT : function (state,payload) {
            state.lockedSeat = payload;
        }
    },
    actions :{
        INIT : function(context){
            var showWay = window.func.getConfigWeb('showWay')==null ? true : window.func.getConfigWeb('showWay');
            context.commit('SET_SHOW_WAY',showWay);

            var showAllTicketInSeat = window.func.getConfigWeb('showAllTicketInSeat')==null ? false : window.func.getConfigWeb('showAllTicketInSeat');
            context.commit('SET_SHOW_ALL_TICKET_IN_SEAT',showAllTicketInSeat);

            var showDetailTrips = window.func.getConfigWeb('showDetailTrips')==null ? false : window.func.getConfigWeb('showDetailTrips');
            context.commit('SET_SHOW_DETAIL_TRIPS',showDetailTrips);

            var date = window.func.getHistory('date')||new Date().customFormat('#DD#-#MM#-#YYYY#');
            context.commit("SET_DATE",date);
            context.commit("LOAD","GET_ROUTE",{root:true});
            window.sendJson({
                url : window.urlDBD(window.API.routeList),
                data : {
                    page : 0,
                    count : 100
                },
                success : function (data) {
                    context.commit('SET_ROUTES',data.results.result);
                    context.commit('SET_ROUTE');
                    context.commit('SET_POINT');
                    context.dispatch("GET_TRIPS");
                    context.commit("UNLOAD","GET_ROUTE",{root:true});
                },
                functionIfError :function () {
                    context.commit("UNLOAD","GET_ROUTE",{root:true});
                }
            });
        },
        SET_DATE : function(context,date){
            context.commit('SET_DATE',date);
            context.dispatch('GET_TRIPS');
        },
        SET_ROUTE : function (context,routeId) {
            context.commit('SET_ROUTE',routeId);
            context.commit('SET_POINT');
            context.dispatch('GET_TRIPS');
        },
        SET_START_POINT: function(context,point){
            context.commit('SET_START_POINT',point);
            context.commit('UPDATE_SET_TRIPS');
        },
        SET_END_POINT: function(context,point){
            context.commit('SET_END_POINT',point);
            context.commit('UPDATE_SET_TRIPS');
        },
        GET_TRIPS : function (context) {
            if(!context.state.route||!context.state.startPoint||!context.state.endPoint||!context.state.date){
                return false;
            }
            context.commit("SET_TRIPS",[]);
            context.state.trip=null;
            var params = {
                page: 0,
                count: window.func.count50(),
                timeZone: 7,
                sortSelections : [
                    {fieldName: "startTime", ascDirection: true}
                ],
                companyId: window.userInfo.userInfo.companyId,
                routeId: context.state.route.routeId,
                date: window.func.dateStr(context.state.date),
                startPoint: context.state.startPoint.id,
                endPoint: context.state.endPoint.id,
                searchPointOption: 1,
                source : 1,
                platform: 1
            };
            window.func.setHistory({routeId:context.state.route.routeId,date:context.state.date});
            context.commit("LOAD","GET_TRIPS",{root:true});
            window.sendJson({
                url : window.urlDBD(window.API.planForTripSearch),
                data : params,
                success : function (data) {
                    context.commit("SET_TRIPS",data.results.trips);
                    context.dispatch("SET_TRIP");
                    context.commit("UNLOAD","GET_TRIPS",{root:true});
                },
                functionIfError:function (data) {
                    context.commit("UNLOAD","GET_TRIPS",{root:true});
                }
            });
        },
        SET_TRIP : function(context,tripId){
            context.commit('SET_TRIP',tripId);
            context.commit('SET_ADDRESS_UP_TICKET_NOT_SEAT',null);
            context.commit('SET_ADDRESS_UP_TICKET_NOT_SEAT',null);
            context.dispatch('SET_POLICY_TRIP');
        },
        SET_POLICY_TRIP : function(context){
            if(context.state.trip){
                context.commit("LOAD","SET_POLICY_TRIP",{root:true});
                sendJson({
                    url : urlDBD(API.tripGetPricePolicy),
                    data : {
                        tripId : context.state.trip.tripId,
                        startDate : func.dateToMS(context.state.date),
                        startTime : context.state.trip.startTimeReality,
                        pointUpId : context.state.startPoint.id,
                        pointDownId : context.state.endPoint.id
                    },
                    success : function (data) {
                        context.commit("SET_POLICY_TRIP",data.results.pricePolicy);
                        context.commit("UNLOAD","SET_POLICY_TRIP",{root:true});
                    },
                    error : function (err) {
                        context.commit("UNLOAD","SET_POLICY_TRIP",{root:true});
                    }
                })
            }
        },
        PUSH_SELL_TICKET : function (context, ticket) {
            context.commit('PUSH_SELL_TICKET', ticket);
        },
        UN_PUSH_SELL_TICKET : function (context,ticket) {
            context.commit('UN_PUSH_SELL_TICKET', ticket);
        },
        PUSH_UPDATE_TICKET : function (context,ticket) {
            context.commit('PUSH_UPDATE_TICKET', ticket);
        },
        UN_PUSH_UPDATE_TICKET : function (context,ticket) {
            context.commit('UN_PUSH_UPDATE_TICKET', ticket);
        },
        UN_SELECTED : function (context) {
            context.commit('UN_SELECTED');
        },
        SET_TICKETS_MOVE : function (context) {
            context.commit('SET_TICKETS_MOVE');
        },
        DO_MOVE_TICKET: function (context,seatInfo) {
            let data = {
                tripId : context.state.trip.tripId,
                ticketId : context.state.ticketsMove[0].ticketId,
                seat : {
                    seatId : seatInfo.seatId,
                    seatType : seatInfo.seatType
                }
            };
            if(context.state.ticketsMove[0].routeInfo.id!==context.state.route.routeId){
                data.pointUp = context.state.startPoint;
                if(context.state.ticketsMove[0].pointUp.pointType==1||context.state.ticketsMove[0].pointUp.pointType==2){
                    data.pointUp.pointType=context.state.ticketsMove[0].pointUp.pointType;
                    data.pointUp.address=context.state.ticketsMove[0].pointUp.address;
                }
                data.pointDown = context.state.endPoint;
                if(context.state.ticketsMove[0].pointDown.pointType==1||context.state.ticketsMove[0].pointDown.pointType==2){
                    data.pointDown.pointType=context.state.ticketsMove[0].pointDown.pointType;
                    data.pointDown.address=context.state.ticketsMove[0].pointDown.address;
                }
            }
            if(context.state.ticketsMove[0].ticketStatus==0||context.state.ticketsMove[0].ticketStatus==2){
                data.ticketStatus = 7;
            }
            context.commit("LOAD","MOVE_SEAT"+seatInfo.seatId,{root:true});
            sendJson({
                url : urlDBD(API.ticketTransfer),
                data : [data],
                success : function (data) {
                    context.commit('CANCEL_FIRST_MOVE_TICKET');
                    notify("Đang xử lý...",'info');
                    context.commit("UNLOAD","MOVE_SEAT"+seatInfo.seatId,{root:true});
                },
                functionIfError : function (err) {
                    context.commit("UNLOAD","MOVE_SEAT"+seatInfo.seatId,{root:true});
                }
            })
        },
        CHECK_TICKET : function (context,payload) {
            let params = [];
            $.each(context.state.ticketsSelected,function (t,ticket) {
                let tk= {
                    ticketId : ticket.ticketId,
                    callStatus : payload,
                }
                if(ticket.ticketStatus == 2){
                    tk.ticketStatus = 7;
                    tk.overTime=0;
                }
                params.push(tk);
            });
            context.commit("LOAD","CHECK_TICKET",{root:true});
            sendJson({
                url : urlDBD(API.ticketUpdate),
                data: params,
                success : function (data) {
                    context.commit("UNLOAD","CHECK_TICKET",{root:true});
                },
                functionIfError : function () {
                    context.commit("UNLOAD","CHECK_TICKET",{root:true});
                }
            })
        },
        DELETE_TICKET_ONLINE_FIREBASE : function (context,payload) {
            sendJson({
                url : urlDBD(API.ticketUpdateCheckedOnline),
                data : payload
            });
        },
        LOCK_SEAT : function (context,payload) {
            if(context.state.lockedSeat){
                context.dispatch('UN_LOCK_SEAT');
            }
            let listSeat = [];
            $.each(context.state.ticketsSelected,function (t,ticket) {
                listSeat.push(ticket.seat);
            })
            let data = {
                user : {
                    id : userInfo.userInfo.id,
                    fullName : userInfo.userInfo.fullName
                },
                startPointId : context.state.startPoint.id,
                endPointId : context.state.endPoint.id,
                listSeat : listSeat,
                tripId : context.state.trip.tripId,
                time : new Date().getTime(),
            }
            firebaseDatabase.ref().child('trips').child(context.state.trip.tripId).child('lockedSeat').child(userInfo.userInfo.id).set(data);
            context.commit('SET_LOCKED_SEAT',data);
        },
        UN_LOCK_SEAT : function (context) {
            if(context.state.lockedSeat){
                firebaseDatabase.ref().child('trips').child(context.state.lockedSeat.tripId).child('lockedSeat').child(userInfo.userInfo.id).remove();
                context.commit('SET_LOCKED_SEAT',null);
            }else{
                firebaseDatabase.ref().child('trips').child(context.state.trip.tripId).child('lockedSeat').child(userInfo.userInfo.id).remove();
            }
        }
    }
}