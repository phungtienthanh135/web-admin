import {CompanyConfig} from "../../class/company_config";

export default {
    state: {
        companyConfig: new CompanyConfig(),
        companyConfigData: []
    },
    getters: {
        mapCompanyConfig: function (state) {
            let result = {};
            $.each(state.companyConfigData, function (c, config) {
                result[config.name] = c;
            });
            return result;
        }
    },
    mutations: {
        SET_COMPANY_CONFIG: function (state, payload) {
            state.companyConfigData = payload;
        }
    },
    actions: {
        INIT_COMPANY_CONFIG: async function (context) {
            await context.dispatch('GET_COMPANY_CONFIG');
            context.state.companyConfig.get();
        },
        GET_COMPANY_CONFIG: function (context) {
            return new Promise((resolve,reject)=>{
                window.sendParam({
                    url: window.urlDBD(window.API.companyConfigList),
                    type: 'GET',
                    success: function (data) {
                        context.commit("SET_COMPANY_CONFIG", data.results.companyconfigurations);
                        resolve()
                    }
                });
            })
        },
        UPDATE_COMPANY_CONFIG: function (context, payload) {
            let params = [];
            $.each(payload.params, function (key, value) {
                params.push({
                    id: value.id,
                    active: value.active,
                    value: value.value
                });
            });
            context.commit("LOAD", "UPDATE_COMPANY_CONFIG");
            window.sendJson({
                url: window.urlDBD(window.API.companyConfigUpdate),
                data: params,
                success: function (data) {
                    context.commit("UNLOAD", "UPDATE_COMPANY_CONFIG");
                    context.commit("SET_COMPANY_CONFIG", payload.params);
                    if (payload && typeof payload.success === "function") {
                        payload.success(data);
                    }
                },
                functionIfError(err) {
                    context.commit("UNLOAD", "UPDATE_COMPANY_CONFIG");
                }
            });
        }
    }
}
