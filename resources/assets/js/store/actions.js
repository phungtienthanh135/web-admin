export default {
    INTT : async function(context,payload){
        console.log('init')
        await context.dispatch('INIT_COMPANY_CONFIG');
        context.commit('MERGE_TICKETS_PRINT');
        context.commit('SET_SELL_FORM');
        if (window.userInfo.isUsingSearchingTripByTimeRange) {
            context.commit('Ticket06/SET_TIME', parseInt(new Date().customFormat('#hhhh#')));
            context.commit('Ticket07/SET_TIME', parseInt(new Date().customFormat('#hhhh#')));
        }
        context.commit('SHOW_DATEPICKER');
        context.commit('CONFIG_DATA');
        context.commit('SET_ROOM');
        context.dispatch('GET_MERCHANTS');
        context.dispatch('GET_SMS_CONFIGS');
        context.dispatch('GET_ROUTES');
        context.dispatch('LockSeat/INIT_LOCK_SEAT_CONFIG');
    },
    setLang : function({commit}, payload) {
        commit('SET_LANG', payload);
    },
    SET_AGENCY_DEBT_LIMIT : function (context,payload) {
        setTimeout(function () {
            window.sendJson({
                url : window.urlDBD(window.API.agencyView),
                data : {
                    id : window.userInfo.userInfo.id
                },
                success : function (data) {
                    context.commit('SET_AGENCY_DEBT_LIMIT',data.results.agencyInfo.debtLimit-data.results.agencyInfo.currentDebt);
                }
            });
        },1000);
    },
    GET_MERCHANTS : function (context,payload) {
        window.sendParam({
            url : window.urlDBD(window.API.merchantList),
            data : {
                companyId : window.userInfo.userInfo.companyId
            },
            type : 'get',
            success : function (res) {
                context.commit('SET_MERCHANTS',res.results.merchant);
            },
            error : function (err) {
                console.warn(err);
            }
        });
    },
    GET_SMS_CONFIGS : function (context,payload) {
        window.sendJson({
            url : window.urlDBD(window.API.smsConfigDisplayList),
            data : {
                page : 0,
                count: 30
            },
            success : function (res) {
                let data2 = []
                $.each(res.results.listMessage, function (c, config) {
                    if (config.messageType != 9 &&
                        config.messageType < 1000000000 ) {
                        data2.push(config);
                    }
                });
                context.commit('SET_SMS_CONFIGS',data2);
                let data = []
                $.each(res.results.listMessage, function (c, config) {
                    if (config.messageType == 9 ||
                        config.messageType > 1000000000 ) {
                        data.push(config);
                    }
                });
                context.commit('SET_SMS_GOOD_CONFIG',data);
            },
            error : function (err) {
                console.warn(err);
            }
        });
    },
    SEND_SMS : function(context,payload){
        let data = {
            messageType: payload.messageType,
            ticketIds : []
        }
        $.each(payload.tickets,function (t,ticket) {
            data.ticketIds.push(ticket.ticketId);
        })
        window.sendJson({
            url : window.urlDBD(window.API.ticketSendSMS),
            data : data,
            success : function (res) {
                notify("Gửi tin nhắn thành công");
            },
            error : function (err) {
                console.warn(err);
            }
        });
    },
    GET_ROUTES : function (context) {
        let params = {
            page : 0,
            count : 100,
            companyId : userInfo.userInfo.companyId
        };
        window.sendJson({
            url : window.urlDBD(window.API.routeList),
            data : params,
            success : function (data) {
                context.commit('SET_ROUTES',data.results.result);
            },
            error : function (err) {
                console.warn(err);
            }
        });
    }
}
