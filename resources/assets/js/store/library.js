function makeSeatDefault (floor,row,column) {
    let data = {
        id : func.makeId(),
        floor : floor,
        column : column,
        row : row,
        seatId : func.makeId(),
        seatType :7 ,
        seatStatus : 1,
        extraPrice:0,
    }
    return data;
}

export function makeSeatMap(_seatMap){
	if(!_seatMap){
        return [];
    }
    let data = [];
    for(let f=0;f<_seatMap.numberOfFloors;f++){
        let listRow = [];
        for( let r=0;r<_seatMap.numberOfRows;r++){
            let listCol = [];
            for( let c=0;c<_seatMap.numberOfColumns;c++){
                listCol.push(makeSeatDefault(f+1,r+1,c+1));
            }
            listRow.push(listCol);
        }
        data.push(listRow);
    }
    if(_seatMap.seatList){
        $.each(_seatMap.seatList,function (k,v) {//v là seat
            try{
                if(data[v.floor-1][v.row-1][v.column-1]!==undefined){
                    let s = _.cloneDeep(v);
                    s.id=func.makeId();
                    data[v.floor-1][v.row-1][v.column-1]=_.cloneDeep(s);
                }// các thông sô tầng hàng cột bắt đầu từ 1, mapdata từ 0 nên phải trừ 1
            }catch(e){
                console.log('ticket error' + JSON.stringify(v));
            }
        });
    }
    return data;
}

export function orderTickets(_seatMap,_ticketsInfo,_ticketsCanceled,_printedTickets,_moreData) {
    let ticketsOrderStep1 = [];// vé sắp xếp sau bước 1: tạo thành mảng vé dựa theo điểm xuất phát
    let ticketsOrderStep2 = [];//sắp xếp vé thành danh sách vé
    let ticketsInSeatMap = []; // các vé trên sơ đồ ghế
    let ticketsEndTime = []; // các vé hết hạn
    let ticketsCanceled = []; // các vé đã hủy
    let ticketsNotSeat = [];//các vé không chỗ ngồi
    if(_ticketsCanceled){
        $.each(_ticketsCanceled,function (t,ticket) {
            ticketsCanceled.push(ticket);
        });
    }
    // sắp xếp theo sdt
    var ticketsSortPhone = [];
    try{
        if(_ticketsInfo){
            ticketsSortPhone = Object.values(_ticketsInfo).sort(function (a,b) {
                return (a.phoneNumber > b.phoneNumber ? 1: (a.phoneNumber < b.phoneNumber ?-1:0));
            });
        }
    }catch(e){
        ticketsSortPhone = _ticketsInfo;
        console.warn(e);
    }
    $.each(ticketsSortPhone,function (t,ticket) {
        try{
            let tk = _.cloneDeep(ticket);
            if(_printedTickets&&_printedTickets[ticket.ticketId]){
                tk.printed = _printedTickets[ticket.ticketId];
            }
            let mode = checkTicketStatus(ticket).mode;
            if(mode=="endTime"){//hết hạn giữ chỗ;
                ticketsEndTime.push(_.cloneDeep(tk));
                return;
            }
            if(mode == "cancel"){
                ticketsCanceled.push(_.cloneDeep(tk));
                return;
            }
            if(tk.seat.seatType==0){
                ticketsNotSeat.push(tk);
                return;
            }
            let key = _moreData.indexPointInRoute[ticket.pointUp.id];
            if(ticketsOrderStep1[key]){
                ticketsOrderStep1[key].push(tk);
            }else{
                ticketsOrderStep1[key] = [tk];
            }
        }catch(e){
            alert("lỗi trên vé , Mã vé : " + ticket.ticketCode);
            console.warn(ticket,e);
        }

    });
    $.each(ticketsOrderStep1,function (p,ticketsInPoint) {/// không  nên dùng concat luôn vì có thể index sẽ bị lỗi
        $.each(ticketsInPoint,function(t,ticket){
            ticketsOrderStep2.push(ticket);
        });
    });// sắp xếp vé xong
    let seatMap = makeSeatMap(_seatMap);
    let seatHasTicketInStartToEnd = {};
    // đẩy vé vào trong ghế , nếu không tồn tại vị trí ghế (không có hoặc k phải ghế thì đẩy vào danh sách không ghế ngược lại cho vào danh sách vé)
    $.each(ticketsOrderStep2,function (t,ticket) {
        try {
            if((seatMap[ticket.seat.floor-1][ticket.seat.row-1][ticket.seat.column-1].seatType!==3&&seatMap[ticket.seat.floor-1][ticket.seat.row-1][ticket.seat.column-1].seatType!==4)||seatMap[ticket.seat.floor-1][ticket.seat.row-1][ticket.seat.column-1].seatId!==ticket.seat.seatId){// không phải ghế hoặc giường
                ticketsNotSeat.push(ticket);
                return;
            }
            // đẩy vé vào trong ghế
            if(seatMap[ticket.seat.floor-1][ticket.seat.row-1][ticket.seat.column-1].tickets){
                seatMap[ticket.seat.floor-1][ticket.seat.row-1][ticket.seat.column-1].tickets.push(ticket);
            }else{
                seatMap[ticket.seat.floor-1][ticket.seat.row-1][ticket.seat.column-1].tickets=[ticket];
            }
            if(!(_moreData.indexPointInRoute[ticket.pointUp.id]>(_moreData.indexEndPoint-1)||_moreData.indexPointInRoute[ticket.pointDown.id]-1<_moreData.indexStartPoint)){
                let keyS = ticket.seat.floor+'_'+ticket.seat.row+'_'+ticket.seat.column;
                seatHasTicketInStartToEnd[keyS] = true;
            }
            ticketsInSeatMap.push(ticket);
        }catch(e){// trường hợp k có seatmap vị trí này
            ticketsNotSeat.push(ticket);
        }
    });

    return {
        ticketsInSeatMap : ticketsInSeatMap,
        ticketsEndTime : ticketsEndTime,
        ticketsCanceled : ticketsCanceled,
        ticketsNotSeat : ticketsNotSeat,
        seatMapFake : seatMap,
        totalEmptySeat : (_moreData.totalSeat - Object.keys(seatHasTicketInStartToEnd).length)
    }
};

export function checkTicketStatus(ticketInfo) {
    var data = {
            status: "Trống",
            bg: "",
            color: "",
            seatStatus: "Trống",
            seatBg: "",
            seatColor: "",
            type : "empty",
            mode : 'book',
        };
    if(ticketInfo===undefined||ticketInfo ===null){
        return data;
    }
    if(ticketInfo.gotIntoTrip){
        data.status = "Đã lên xe";
        data.bg = "bg-lenxe";
        data.color = "text-lenxe";
        data.seatStatus = "Đã lên xe";
        data.seatBg = "bg-lenxe";
        data.seatColor = "text-lenxe";
        data.type = "upToTrip"
        return data;
    }
    switch (ticketInfo.ticketStatus){
        case 0 :
        case 1 :
            data.status = "Đã hủy";
            data.bg = "bg-huy";
            data.color = "text-danger";
            data.seatStatus = "Trống";
            data.seatBg = "";
            data.seatColor = "";
            data.type = "empty";
            data.mode='cancel';
            break;
        case 2 :
            if(ticketInfo.overTime===0||ticketInfo.overTime>(new Date().getTime())){
                data.status = "Giữ chỗ";
                data.bg = "bg-giucho";
                data.color = "text-giucho";
                data.seatStatus = "Giữ chỗ";
                data.seatBg = "bg-giucho";
                data.seatColor = "text-giucho";
                data.type = "buy";
            }else {
                data.status = "Hết hạn giữ chỗ";
                data.bg = "bg-huy";
                data.color = "text-danger";
                data.seatStatus = "Hết hạn giữ chỗ";
                data.seatBg = "";
                data.seatColor = "";
                data.type = "empty";
                data.mode="endTime";
            }
            break;
        case 3 :
            data.status = "Đã thanh toán";
            data.bg = "bg-thanhtoan";
            data.color = "text-success";
            data.seatStatus = "Đã thanh toán";
            data.seatBg = "bg-thanhtoan";
            data.seatColor = "text-thanhtoan";
            data.type = "pay";
            break;
        case 4 :
            data.status = "Đã lên xe";
            data.bg = "bg-success";
            data.color = "text-success";
            data.seatStatus = "Đã lên xe";
            data.seatBg = "bg-success";
            data.seatColor = "text-success";
            data.type = "upToTrip";
            break;
        case 5 :
            data.status = "Đã hoàn thành";
            data.bg = "bg-success";
            data.color = "text-success";
            data.seatStatus = "Hoàn thành chuyến đi";
            data.seatBg = "bg-success";
            data.seatColor = "text-success";
            data.type = "complete"
            break;
        case 6 :
            break;
        case 7 :
            data.status = "Giữ chỗ ưu tiên";
            data.bg = "bg-giucho";
            data.color = "text-giucho";
            data.seatStatus = "Giữ chỗ ưu tiên";
            data.seatBg = "bg-giucho";
            data.seatColor = "text-giucho";
            data.type = "buy";
            break;

        default : {
            break;
        }
    }
    return data;
}