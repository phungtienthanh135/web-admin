export default {
    supperLoading : [],
    ticketSample : null,
    agencyDebtLimit : 0,
    sellForm : 'Ticket05',
    mergeTicketsWhenPrint : false,
    showTicketsOrSeatMap : false,
    showDatepicker : false,
    merchants : [],
    smsConfigs : [],
    smsGoodConfig:[],
    routes : [],
    config_data : {
        paymentTypeDefault : '3',
        showStartTimeReality : false,
    },
    room: null
}