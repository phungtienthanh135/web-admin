import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
// import 'firebase/database';

export async function fireStoreInit(){
    const firestore = await firebase.initializeApp(CONFIG.firestore, "secondary");
    window.firestoreDatabase = await firestore.firestore()
    await window.firestoreDatabase.settings({
        timestampsInSnapshots: true
    });
}
