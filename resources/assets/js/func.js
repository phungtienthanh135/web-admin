window.func = {
    count : function () {
        return 500;
    },
    count50 : function(){
        return 50;
    },
    countSearch : function () {
        return 30;
    },
    countMax : function () {
        return 1000;
    },
    setHistory : function (data) {
        var history = JSON.parse(sessionStorage.getItem('history')||'{}');
        $.each(data,function (k,v) {
            history[k] = v;
        });
        sessionStorage.setItem('history',JSON.stringify(history));
    },
    getHistory : function (key) {
        var history = JSON.parse(sessionStorage.getItem('history')||'{}');
        try{
            return history[key];
        }catch(e){
            return null;
        }
    },
    getAllHistory : function () {
        var history = JSON.parse(sessionStorage.getItem('history')||'{}');
        return history;
    },
    removeHistory : function(...other){
        var history = JSON.parse(sessionStorage.getItem('history')||'{}');
        $.each(other,function(i,key){
            try{
                delete history[key];
            }catch(e){
                console.log(key);
            }
        });
        sessionStorage.setItem('history',JSON.stringify(history));
    },
    setConfigWeb : function (data) {
        var history = JSON.parse(localStorage.getItem('configWeb')||'{}');
        $.each(data,function (key,value) {
            history[key]=value;
        });
        localStorage.setItem('configWeb',JSON.stringify(history));
    },
    getConfigWeb : function (key) {
        var history = JSON.parse(localStorage.getItem('configWeb')||'{}');
        try{
            return history[key];
        }catch(e){
            return null;
        }
    },
    dateToMS: function (date,time) {
        if(!date&&!time){
            return 0;
        }
        date = date.split('-');
        var ms = date[1] + '/' + date[0] + '/' + date[2];
        if(time){
            if(time =='end'){
                ms += ' 23:59:00';
            }else {
                ms += ' '+time+':00';
            }
        }
        ms = new Date(ms).getTime();
        return ms;
    },
    dateStr : function (date) {
        date = date.split('-');
        return date[2]+ '' + date[1] + '' + date[0];
    },
    dateStrToMs : function(date){
        date = date.toString();
        var str1 = date.substr(0, 4);
        var str2 = date.substr(4,2);
        var str3 = date.substr(6,2);
        date =  str2 + '/' + str3 + '/' + str1;
        var ms = new Date(date).getTime(); // cộng timezone việt nam
        return ms;
    },
    DateYMD : function (date) {
        date = date.split('-');
        date = date[2] + date[1]  + date[0];
        return date;
    },
    dateStartToEnd : function (date1,date2) {
        var dataReturn = {start : 0 , end:1};
        var ms1 = this.dateToMS(date1);
        var ms2 = this.dateToMS(date2);
        if(ms1 >ms2){
            dataReturn.start = ms2;
            dataReturn.end = this.dateToMS(date1,'end');
        }else {
            dataReturn.start = ms1;
            dataReturn.end = this.dateToMS(date2,'end');
        }
        return dataReturn;
    },
    timeToMs : function (time) {
        time = time.split(':');
        time = (parseInt(time[0]*60)+parseInt(time[1]))*60000;
        return time;
    },
    msToTime : function (time) {
        var hour = new Date(time).getUTCHours()
        var minute = new Date(time).getMinutes()
        if (minute < 10){
            minute = '0' + minute;
        }
        var retureTime = hour + ':' + minute
        return retureTime;
    },
    dateBeforeToday:function (numberDay) {
        var d = new Date();
        d.setDate(d.getDate()-numberDay)
        return d;
    },
    removeItemNullInArray: function(arr) {
        newArr = {};
        if (typeof arr === 'object') {
            $.each(arr, function (k, v) {
                if (v !== '' && v !== null && typeof v !== 'undefined') {
                    newArr[k] = v;
                }
            });
            return newArr;
        }
        return arr;
    },
    isNumber : function (str) {
        var reg = /^\d+$/;
        return reg.test(str);
    },
    strToNumber : function(str){
        var number = 0;
        switch (str) {
            case true : number = 1;
                break;
            case '':number = 0;
                break;
            default :
                if(isNaN(str)){
                    number = str.toString().replace(/\D/g, '');
                }else{
                    number = str;
                }
                break;

        }
        return number||0;
    },
    defaultLoad :function () {
        return {
            loading : false,
            error :false
        };
    },
    beforeLoad : function () {
        return {
            loading : true,
            error :false
        };
    },
    loadSuccess : function () {
        return {
            loading : false,
            error :false,
        }
    },
    loadError : function () {
        return {
            loading : true,
            error :true,
        }
    },
    makeId :function() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 9; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    },
    removeUnicode : function(str) {
        try{
            str= str.toLowerCase();
            str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
            str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
            str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
            str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
            str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
            str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
            str= str.replace(/đ/g,"d");
            str= str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,"-");
    
            str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
            str= str.replace(/^\-+|\-+$/g,"");
    
            return str;
        }catch(e){
            return str;
        }
    },
    checkTicketStatus : function (ticketInfo) {
        var data = {
                status: "Trống",
                bg: "",
                color: "",
                seatStatus: "Trống",
                seatBg: "",
                seatColor: "",
                type : "empty",
                mode : 'book',
            };
        if(ticketInfo===undefined||ticketInfo ===null){
            return data;
        }
        if(ticketInfo.gotIntoTrip){
            data.status = "Đã lên xe";
            data.bg = "bg-lenxe";
            data.color = "text-lenxe";
            data.seatStatus = "Đã lên xe";
            data.seatBg = "bg-lenxe";
            data.seatColor = "text-lenxe";
            data.type = "upToTrip"
            return data;
        }
        switch (ticketInfo.ticketStatus){
            case 0 :
            case 1 :
                data.status = "Đã hủy";
                data.bg = "bg-huy";
                data.color = "text-danger";
                data.seatStatus = "Trống";
                data.seatBg = "";
                data.seatColor = "";
                data.type = "empty";
                data.mode='cancel';
                break;
            case 2 :
                if(ticketInfo.overTime===0||ticketInfo.overTime>(new Date().getTime())){
                    data.status = "Giữ chỗ";
                    data.bg = "bg-giucho";
                    data.color = "text-giucho";
                    data.seatStatus = "Giữ chỗ";
                    data.seatBg = "bg-giucho";
                    data.seatColor = "text-giucho";
                    data.type = "buy";
                }else {
                    data.status = "Hết hạn giữ chỗ";
                    data.bg = "bg-huy";
                    data.color = "text-danger";
                    data.seatStatus = "Hết hạn giữ chỗ";
                    data.seatBg = "";
                    data.seatColor = "";
                    data.type = "empty";
                    data.mode="endTime";
                }
                break;
            case 3 :
                data.status = "Đã thanh toán";
                data.bg = "bg-thanhtoan";
                data.color = "text-success";
                data.seatStatus = "Đã thanh toán";
                data.seatBg = "bg-thanhtoan";
                data.seatColor = "text-thanhtoan";
                data.type = "pay";
                break;
            case 4 :
                data.status = "Đã lên xe";
                data.bg = "bg-success";
                data.color = "text-success";
                data.seatStatus = "Đã lên xe";
                data.seatBg = "bg-success";
                data.seatColor = "text-success";
                data.type = "upToTrip";
                break;
            case 5 :
                data.status = "Đã hoàn thành";
                data.bg = "bg-success";
                data.color = "text-success";
                data.seatStatus = "Hoàn thành chuyến đi";
                data.seatBg = "bg-success";
                data.seatColor = "text-success";
                data.type = "complete"
                break;
            case 6 :
                break;
            case 7 :
                data.status = "Giữ chỗ ưu tiên";
                data.bg = "bg-giucho";
                data.color = "text-giucho";
                data.seatStatus = "Giữ chỗ ưu tiên";
                data.seatBg = "bg-giucho";
                data.seatColor = "text-giucho";
                data.type = "buy";
                break;

            default : {
                break;
            }
        }
        return data;
    },
    initDragScrolling: function(dom) {
        const sliders = document.querySelectorAll(dom);
        let isDown = false;
        let startX;
        let scrollLeft;

        sliders.forEach((slider) => {
            slider.addEventListener('mousedown', (e) => {
                isDown = true;
                slider.classList.add('active');
                startX = e.pageX - slider.offsetLeft;
                scrollLeft = slider.scrollLeft;
            });
            slider.addEventListener('mouseleave', () => {
                isDown = false;
                slider.classList.remove('active');
            });
            slider.addEventListener('mouseup', () => {
                isDown = false;
                slider.classList.remove('active');
            });
            slider.addEventListener('mousemove', (e) => {
                if (!isDown) return;
                e.preventDefault();
                slider.classList.add('active');
                const x = e.pageX - slider.offsetLeft;
                const walk = (x - startX) * 3; //scroll-fast
                slider.scrollLeft = scrollLeft - walk;
            });
        })
    },
    getAddressUpDownTicket : function(ticketInfo) {
        if(ticketInfo===undefined||ticketInfo===null){
            return {up :"",down :""}
        }
        var addressdrop = '', addresspick = '';
        if (ticketInfo.getInPoint != undefined) {
            if (typeof ticketInfo.getInPoint != 'undefined' || typeof ticketInfo['dropAlongTheWayAddress'] != 'undefined' ||
                typeof ticketInfo['dropOffAddress'] != 'undefined') {
                if (typeof ticketInfo['dropAlongTheWayAddress'] != 'undefined' && ticketInfo['dropAlongTheWayAddress'] != '') {
                    addressdrop = ticketInfo['dropAlongTheWayAddress'];
                } else {
                    if (typeof ticketInfo['dropOffAddress'] != 'undefined' && ticketInfo['dropOffAddress'] != '') {
                        addressdrop = ticketInfo['dropOffAddress'];
                    } else {
                        addressdrop = ticketInfo['getOffPoint']['pointName'];

                    }
                }
                if (addressdrop.split(', ').length >= 2) {
                    addressdrop = addressdrop.split(', ', 2).toString();
                }
                if (typeof ticketInfo['dropAlongTheWayAddress'] != 'undefined' && ticketInfo['dropAlongTheWayAddress'] != '')
                    addressdrop = '<strong>' + addressdrop + '</strong>';
                if (typeof ticketInfo['alongTheWayAddress'] != 'undefined' && ticketInfo['alongTheWayAddress'] != '') {
                    addresspick = ticketInfo['alongTheWayAddress'];
                } else {

                    if (typeof ticketInfo['pickUpAddress'] != 'undefined' && ticketInfo['pickUpAddress'] != '') {
                        addresspick = ticketInfo['pickUpAddress'];
                    }
                    else {
                        addresspick = ticketInfo['getInPoint']['pointName'];
                    }
                }
                if (addresspick.split(', ').length >= 2) {
                    addresspick = addresspick.split(', ', 2).toString();
                }

                if (typeof ticketInfo['alongTheWayAddress'] != 'undefined' && ticketInfo['alongTheWayAddress'] != '') {
                    addresspick = '<strong>' + addresspick + '</strong>';
                }
            }
        }
        return {up: addresspick, down: addressdrop}
    },
    formatMoney : function(amount, decimalCount = 0, decimal = ".", thousands = ",") {
      try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
      } catch (e) {
        console.log(e)
      }
    },
    dynamicSort: function(property){
      var sortOrder = 1;
      if(property[0] === "-") {
          sortOrder = -1;
          property = property.substr(1);
      }
      return function (a,b) {
          var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
          return result * sortOrder;
      }
    },

    DocSo3ChuSo: function (baso)
    {
        var ChuSo=new Array(" không "," một "," hai "," ba "," bốn "," năm "," sáu "," bảy "," tám "," chín ");
        var Tien=new Array( "", " nghìn", " triệu", " tỷ", " nghìn tỷ", " triệu tỷ");
        var tram;
        var chuc;
        var donvi;
        var KetQua="";
        tram=parseInt(baso/100);
        chuc=parseInt((baso%100)/10);
        donvi=baso%10;
        if(tram==0 && chuc==0 && donvi==0) return "";
        if(tram!=0)
        {
            KetQua += ChuSo[tram] + " trăm ";
            if ((chuc == 0) && (donvi != 0)) KetQua += " linh ";
        }
        if ((chuc != 0) && (chuc != 1))
        {
                KetQua += ChuSo[chuc] + " mươi";
                if ((chuc == 0) && (donvi != 0)) KetQua = KetQua + " linh ";
        }
        if (chuc == 1) KetQua += " mười ";
        switch (donvi)
        {
            case 1:
                if ((chuc != 0) && (chuc != 1))
                {
                    KetQua += " mốt ";
                }
                else
                {
                    KetQua += ChuSo[donvi];
                }
                break;
            case 5:
                if (chuc == 0)
                {
                    KetQua += ChuSo[donvi];
                }
                else
                {
                    KetQua += " lăm ";
                }
                break;
            default:
                if (donvi != 0)
                {
                    KetQua += ChuSo[donvi];
                }
                break;
            }
        return KetQua;
    },
    DocTienBangChu: function(SoTien)
    {
        var Tien=new Array( "", " nghìn", " triệu", " tỷ", " nghìn tỷ", " triệu tỷ");
        var lan=0;
        var i=0;
        var so=0;
        var KetQua="";
        var tmp="";
        var ViTri = new Array();
        if(SoTien<0) return "Số tiền âm !";
        if(SoTien==0) return "Không đồng !";
        if(SoTien>0)
        {
            so=SoTien;
        }
        else
        {
            so = -SoTien;
        }
        if (SoTien > 8999999999999999)
        {
            //SoTien = 0;
            return "Số quá lớn!";
        }
        ViTri[5] = Math.floor(so / 1000000000000000);
        if(isNaN(ViTri[5]))
            ViTri[5] = "0";
        so = so - parseFloat(ViTri[5].toString()) * 1000000000000000;
        ViTri[4] = Math.floor(so / 1000000000000);
         if(isNaN(ViTri[4]))
            ViTri[4] = "0";
        so = so - parseFloat(ViTri[4].toString()) * 1000000000000;
        ViTri[3] = Math.floor(so / 1000000000);
         if(isNaN(ViTri[3]))
            ViTri[3] = "0";
        so = so - parseFloat(ViTri[3].toString()) * 1000000000;
        ViTri[2] = parseInt(so / 1000000);
         if(isNaN(ViTri[2]))
            ViTri[2] = "0";
        ViTri[1] = parseInt((so % 1000000) / 1000);
         if(isNaN(ViTri[1]))
            ViTri[1] = "0";
        ViTri[0] = parseInt(so % 1000);
      if(isNaN(ViTri[0]))
            ViTri[0] = "0";
        if (ViTri[5] > 0)
        {
            lan = 5;
        }
        else if (ViTri[4] > 0)
        {
            lan = 4;
        }
        else if (ViTri[3] > 0)
        {
            lan = 3;
        }
        else if (ViTri[2] > 0)
        {
            lan = 2;
        }
        else if (ViTri[1] > 0)
        {
            lan = 1;
        }
        else
        {
            lan = 0;
        }
        for (i = lan; i >= 0; i--)
        {
           tmp = func.DocSo3ChuSo(ViTri[i]);
           KetQua += tmp;
           if (ViTri[i] > 0) KetQua += Tien[i];
           if ((i > 0) && (tmp.length > 0)) KetQua += ',';//&& (!string.IsNullOrEmpty(tmp))
        }
       if (KetQua.substring(KetQua.length - 1) == ',')
       {
            KetQua = KetQua.substring(0, KetQua.length - 1);
       }
       KetQua = KetQua.substring(1,2).toUpperCase()+ KetQua.substring(2);
       return KetQua;//.substring(0, 1);//.toUpperCase();// + KetQua.substring(1);
    },
    unDuplicateTicket: function(listTickets) {
        let res = [];

        let isExist = (arr, x) =>
        {
            for(let i = 0; i < arr.length; i++) {
                let condition = arr[i].pointDown.address == x.pointDown.address
                    && arr[i].pointUp.address == x.pointUp.address
                    && arr[i].ticketCode == x.ticketCode
                    && arr[i].phoneNumber == x.phoneNumber;

                if (condition)
                    return {index: i, status: true};
            }
            return {index: i, status: false};
        }


        listTickets.forEach(elm => {
            let element = _.cloneDeep(elm);
            let checker = isExist(res, element);
            if(!checker.status)
            {
                res.push(element);
            } else {
                element.listSeatId.forEach(elem => res[checker.index]['listSeatId'].push(elem) );
                let totalAgency = res[checker.index]['agencyPrice'] + element.agencyPrice;
                let totalPaid = res[checker.index]['paidMoney'] + element.paidMoney;

                res[checker.index]['agencyPrice'] = totalAgency;
                res[checker.index]['paidMoney'] = totalPaid;
            }
        });

        return res;

    },
}