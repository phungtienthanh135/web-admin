import Vue from 'vue';
import VueI18n from 'vue-i18n';
import viMessage from './vi.js';
import enMessage from './en.js';

import viUiElement from 'element-ui/lib/locale/lang/vi.js';
import enUiElement from 'element-ui/lib/locale/lang/en.js';
import ElementLocale from 'element-ui/lib/locale';


Vue.use(VueI18n);

const messages = {
    vi: {
        ...viMessage,
        ...viUiElement
    },
    en: {
        ...enMessage,
        ...enUiElement
    },
}
let history = JSON.parse(localStorage.getItem('configWeb')||'{}');
let langDefault = history.lang||'vi';
const lang = new VueI18n({
    locale: langDefault, // set locale
    messages,
    fallbackLocale: "vi",
});
ElementLocale.i18n((key, value) => lang.t(key, value))

export default lang;