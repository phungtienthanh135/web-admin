@extends('errors.layout')
@section('title', 'Hệ thống đang nâng cấp')

@section('code','500')
@section('message','Xin lỗi Quý khách! AN VUI đang nâng cấp dữ liệu mời Quý khách quay lại trong vài phút tới!')

@push('javascipt')
    @if(request()->has('debug'))
        <script>
            console.log('{{ $exception->getMessage()}}');
        </script>
    @endif
@endpush
