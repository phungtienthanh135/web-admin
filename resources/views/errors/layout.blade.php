<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lỗi - @yield('title')</title>
    <link rel="shortcut icon" href="/public/favicon.png">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <style>
        body{
            padding-top: 100px;
            background: url(/public/images/background/login-3.jpg) no-repeat;
            background-size: cover;
        }
        .jumbotron{
            background-color: rgba(238, 238, 238, 0.77);
        }
    </style>
</head>

<body>

<a href=""></a>
<div class="container">
    <div class="jumbotron">
        <h1>@yield('code')</h1>

        <h2>@yield('message')</h2>
        <p>
            <a class="btn btn-lg btn-primary" href="{{url()->previous()}}" role="button">Quay lại &raquo;</a>
        </p>
    </div>
    @stack('javascipt')
</div> <!-- /container -->

</body>
</html>




