@extends('cpanel.template.layout')
@section('title', 'Danh sách điểm')

@section('content')

    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Danh sách điểm</h3></div>
            </div>
        </div>
        <div class="khung_lich_ve">
            <div class="date_picker">
                <div class="innerLR">
                    <div class="row-fluid">
                        <form action="{{action('PointController@show')}}">
                            <div class="span6">
                                <label>Tìm điểm</label>
                                <input class="w_full" type="text" name="pointName" id="pointName"
                                       value="{{request('pointName')}}">
                            </div>
                            <div class="span2">
                                <label>&nbsp;</label>
                                <button class="btn btn-info btn-flat-full" id="search">TÌM ĐIỂM
                                </button>
                            </div>
                            @if(hasAnyRole(CREATE_POINT))
                                <div class="span2">
                                    <label>&nbsp;</label>
                                    <a href="{{action('PointController@getadd')}}" style="border-radius:0px"
                                       class="span12 btn btn-warning" id="add">
                                        THÊM
                                        MỚI ĐIỂM
                                    </a>
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid bg_light m_top_10">
            <div class="widget widget-4 bg_light">
                <div class="widget-body">
                    <table class="table table-hover table-vertical-center">
                        <thead>
                        <tr>
                            <!-- <th>Mã điểm</th> -->
                            <th>Tên điểm</th>
                            <th>Địa chỉ</th>
                            <th>Ngày tạo</th>
                            <th>Loại</th>
                            <th>Tùy chọn</th>
                        </tr>
                        </thead>
                        <tbody>

                        @forelse($result as $row)
                            <tr>
                            <!-- <td>{{$row['pointId']}}</td> -->
                                <td>{{str_limit($row['pointName'],40)}}</td>
                                <td>{{$row['address']}}</td>
                                <td>@dateFormat($row['createDate'])</td>
                                <td>@php
                                        switch ($row['pointType'])
                                        {
                                            case '1':
                                            {
                                             echo 'Điểm dừng lớn';
                                             break;
                                            }
                                            case '2':
                                            {
                                             echo 'Điểm dừng nhỏ';

                                             break;
                                            }
                                            case '3':
                                            {
                                              echo 'Trạm thu phí';
                                             break;
                                            }
                                            case '4':
                                            {
                                                echo 'Nhà hàng';
                                             break;
                                            }
                                            case '5':
                                            {
                                                echo 'Điểm dừng nhanh';

                                             break;
                                            }
                                            case '7':
                                                {
                                                 echo 'Điểm trung chuyển';

                                                  break;
                                                            }
                                            default:
                                                echo 'Chưa xác định';
                                            break;
                                        }
                                    @endphp</td>
                                <td>
                                    <a href="javascript:void(0)" tabindex="0"
                                       class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                        <ul class="onclick-menu-content">
                                            <li>
                                                <button onclick="showInfo(this)">Chi tiết
                                                </button>
                                            </li>
                                            @if(hasAnyRole(UPDATE_POINT))
                                                <li>
                                                    <button onclick="window.location.href='{{action('PointController@getEdit',['data'=>$row])}}'">
                                                        Sửa điểm
                                                    </button>
                                                </li>
                                            @endif
                                            {{--không cho phép xóa lung tung--}}
                                            {{--@if(hasAnyRole(DELETE_POINT))--}}
                                                {{--<li>--}}
                                                    {{--<button data-toggle="modal" data-target="#modal_delete"--}}
                                                            {{--value="{{$row['pointId']}}"--}}
                                                            {{--onclick="$('#delete_point').val($(this).val())">Xóa--}}
                                                    {{--</button>--}}
                                                {{--</li>--}}
                                            {{--@endif--}}
                                        </ul>
                                    </a>
                                </td>
                            </tr>
                            <!--hiển thị chi tiết-->
                            <tr class="info" style="display: none">
                                <td colspan="6">
                                    <form action="{{action('PointController@getEdit')}}">
                                        <table class="tttk no_last" cellspacing="20" cellpadding="4">
                                            <tbody>
                                            <tr>
                                                <td><b>MÃ ĐIỂM</b></td>
                                                <td>{{$row['pointId']}}
                                                </td>
                                                <td class="right"><b>TÊN ĐIỂM</b></td>
                                                <td class="left">{{str_limit($row['pointName'],50)}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><b>ĐỊA CHỈ</b></td>
                                                <td>{{$row['address']}}
                                                </td>
                                                <td class="right"><b>ID NGƯỜI DÙNG</b></td>
                                                <td class="left">{{@$row['userId']}}</td>
                                            </tr>
                                            <tr>
                                                <td><b>LOẠI</b></td>
                                                <td>@php
                                                        switch ($row['pointType'])
                                                        {
                                                            case '1':
                                                            {
                                                             echo 'Điểm dừng lớn';
                                                             break;
                                                            }
                                                            case '2':
                                                            {
                                                             echo 'Điểm dừng nhỏ';

                                                             break;
                                                            }
                                                            case '3':
                                                            {
                                                              echo 'Trạm thu phí';
                                                             break;
                                                            }
                                                            case '4':
                                                            {
                                                                echo 'Nhà hàng';
                                                             break;
                                                            }
                                                            case '5':
                                                            {
                                                                echo 'Điểm dừng nhanh';

                                                             break;
                                                            }
                                                            case '7':
                                                            {
                                                                echo 'Điểm trung chuyển';

                                                             break;
                                                            }
                                                            default:
                                                                echo 'Chưa xác định';
                                                            break;
                                                        }
                                                    @endphp</td>
                                                <td class="right"><b>NGÀY TẠO</b></td>
                                                <td>{{date('d/m/Y',$row['createDate']/1000)}}</td>
                                            </tr>
                                            <tr>
                                                <td><b>KINH ĐỘ</b></td>
                                                <td>{{$row['longitude']}}</td>
                                                <td class="right"><b>VĨ ĐỘ</b></td>
                                                <td>{{$row['latitude']}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td class="center" colspan="6">Hiện tại không có dữ liệu</td>
                            </tr>
                        @endforelse

                        </tbody>
                    </table>
                    @include('cpanel.template.pagination-without-number',['page'=>$page])
                </div>
            </div>
        </div>
    </div>

    {{--không cho phép người dùng xóa lung tung--}}


    {{--@if(hasAnyRole(DELETE_POINT))--}}
        {{--<div class="modal hide fade" id="modal_delete" style="width: 250px; margin-left: -10%;margin-top: 10%;">--}}
            {{--<div class="modal-body center">--}}
                {{--<p>Bạn có chắc muốn xoá?</p>--}}
            {{--</div>--}}
            {{--<form action="{{action('PointController@delete')}}" method="post">--}}
                {{--<div class="modal-footer">--}}
                    {{--<input type="hidden" name="_token" value="{{csrf_token()}}">--}}
                    {{--<input type="hidden" name="pointId" value="" id="delete_point">--}}
                    {{--<a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>--}}
                    {{--<button class="btn btn-primary btn_dongy">ĐỒNG Ý</button>--}}
                {{--</div>--}}
            {{--</form>--}}
        {{--</div>--}}
    {{--@endif--}}
    <!-- End Content -->

@endsection