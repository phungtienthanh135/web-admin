@extends('cpanel.template.layout')
@section('title', 'Sửa mới điểm')
@push('activeMenu')
    activeMenu('{{action('PointController@show',[],true)}}');
@endpush
@section('content')
    <style>
        #infowindow-content .title {
            font-weight: bold;
        }

        #infowindow-content {
            display: none;
        }

        #map #infowindow-content {
            display: inline;
        }
    </style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Sửa điểm</h3></div>
            </div>
        </div>
            <div class="widget widget-4 bg_light">
                <div class="widget-body">
                    <div class="innerLR">
                        <div class="row-fluid">
                            <div class="row-fluid bg_light">
                                <div class="row-fluid">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            <div class="span6"> <?php //dev($data);?>
                                <form action="" method="post" class="form-horizontal frm_themmoi modal_tao_lich_chay_xe">
                                    <div class="control-group">
                                        <label style="width: 70px" class="control-label">Địa chỉ<strong style="color: red;float: left;">*</strong></label>
                                        <div style="margin-left: 80px"class="controls">
                                            <input id="pac-input" type="text"
                                                   placeholder="Nhập địa chỉ để tìm kiếm" value="{{$data['address']}}" class="span12" name="address"/>
                                        </div>
                                    </div>

                                    <input type="hidden" value="{{$data['pointId']}}" name="pointId">
                                    <div class="control-group">
                                        <label style="width: 70px"class="control-label">Tên điểm<strong style="color: red;float: left;">*</strong></label>
                                        <div style="margin-left: 80px"class="controls">
                                            <input type="text" class="span12" name="pointName" id="pointName" value="{{$data['pointName']}}"/>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label style="width: 70px"class="control-label">Kinh độ</label>
                                        <div style="margin-left: 80px"class="controls">
                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <input readonly class="span10" value="{{$data['longitude']}}" name="longitude" type="text" id="txtKinhDo">
                                                </div>
                                                <div class="span6 form-inline">
                                                    <label style="padding-top: 5px;float: left">Vĩ độ</label>
                                                    <input readonly style="margin-left:3px;float: left;" class="span10" value="{{$data['latitude']}}" name="latitude" type="text" id="txtViDo">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" style="width: 70px">Tỉnh thành <strong style="color: red;float: left;">*</strong> </label>
                                        <div style="margin-left: 80px" class="controls ">
                                            <select id="provinceName" type="text" class="span12" name="province" style="width: 100%"
                                                    value="{{$data['province']}}" required>
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label style="width: 75px" class="control-label">Quận huyện <strong style="color: red;float: left;">*</strong> </label>
                                        <div style="margin-left: 80px" class="controls">
                                            <select id="districtName" type="text" class="span12 " name="district" style="width: 100%"
                                                    value="{{$data['district']}}" required>
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label style="width: 70px" class="control-label">Loại</label>
                                        <div style="margin-left: 80px"class="controls">
                                            <select class="span12" name="pointType">
                                                @if($data['pointType']!=7)
                                                <option {{$data['pointType']==1?'selected':''}} value="1">Điểm dừng lớn</option>
                                                <option {{$data['pointType']==2?'selected':''}} value="2">Điểm dừng nhỏ</option>
                                                <option {{$data['pointType']==3?'selected':''}} value="3">Trạm thu phí</option>
                                                <option {{$data['pointType']==4?'selected':''}} value="4">Nhà hàng</option>
                                                <option {{$data['pointType']==5?'selected':''}} value="5">Điểm dừng nhanh</option>
                                                 @else
                                                    <option {{$data['pointType']==7?'selected':''}} value="7">Điểm trung chuyển</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label style="width: 70px" class="control-label">&nbsp;</label>
                                        <div style="margin-left: 80px" class="controls">
                                            <input type="checkbox" id="isReceivedGoods" name="isReceivedGoods" {{@$data['isReceivedGoods']==true?'checked':''}} value="true" class="day" title="diểm trả hàng">
                                            <label for="isReceivedGoods">Là Điểm trả hàng</label>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label style="width: 70px" class="control-label">SDT</label>
                                        <div style="margin-left: 80px" class="controls">
                                            <input type="text" id="phoneNumber" name="phoneNumber" value="{{@$data['phoneNumber']}}">
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label">&nbsp;</label>
                                        <div style="margin-left: 80px"class="controls">
                                            <div class="span6">
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <button class="btn btn-warning btn-flat-full">SỬA ĐIỂM</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="span5">
                                <div id="map" style="width:100%;height:450px"></div>
                                <div id="infowindow-content">
                                    <img src="" width="16" height="16" id="place-icon">
                                    <span id="place-name"  class="title"></span><br>
                                    <span id="place-address"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var dataProvince;
        var provinceName='';
        (init());
        function init(){

                    dataProvince =@php echo json_encode($result) @endphp;
                    var html='';

                    for (var i = 0; i < dataProvince.length; i++) {
                        html += "<option style='margin: 0' data-id='" + dataProvince[i].id + "'>" + dataProvince[i].provinceName + "</option>";
                     }
                    $('#provinceName').append(html);
                    $('#provinceName').attr('value','@php echo $data['province'];@endphp');
                    $('#districtName').attr('value','@php echo $data['district'];@endphp');

                    if('@php echo $data['province'];@endphp'!='' ){
                        provinceName= $('#provinceName').val();
                        var trace; console.log( provinceName);
                        for(var i=0;i<dataProvince.length; i++)
                            if(dataProvince[i].provinceName== provinceName) trace=i;
                        $('#districtName').html('');
                        $('#districtName').append('<option value=""></option>');
                        var html='';
                        var dataDistrict=dataProvince[trace].listDistrict;
                        for(var i=0;i<dataDistrict.length; i++)
                            html+="<option style='margin: 0' data-id='" + dataDistrict[i].districtId + "'>" + dataDistrict[i].districtName + "</option>";
                        $('#districtName').append(html);
                       if('@php echo $data['district'];@endphp' !='') $('#districtName').attr('value','@php echo $data['district'];@endphp');
                    }
        }
        $('#provinceName').select2();
         $('#districtName').select2();

        $('#provinceName').bind('change',function(){
            $('#provinceName').attr('value',$('#provinceName option:selected').val());
            $('#provinceName').attr('data-id',$('#provinceName option:selected').data('id'));
            provinceName= $('#provinceName').val();
            var trace;
            for(var i=0;i<dataProvince.length; i++)
                if(dataProvince[i].provinceName== provinceName) trace=i;
            $('#districtName').html('');
            var html='<option>Chọn quận huyện</option>';
            var dataDistrict=dataProvince[trace].listDistrict;
            for(var i=0;i<dataDistrict.length; i++)
                html+="<option style='margin: 0' data-id='" + dataDistrict[i].districtId + "'>" + dataDistrict[i].districtName + "</option>";
            $('#districtName').append(html);

        });
        $('#districtName').bind('change',function() {
            $('#districtName').attr('value', $('#districtName option:selected').val());
            $('#districtName').attr('data-id', $('#districtName option:selected').data('id'));
        });
        $('#districtName').click(function () {
            if(provinceName=='') Message('Thông báo','Hãy chọn tỉnh thành trước');
        });
        $('#addEndPoint').click(function () {
            $('#addEndPoint').attr('type','button');
            if( !$('#provinceName').attr('value'))  message+='Tỉnh thành';
            if( !$('#districtName').attr('value'))  message+=',Quận huyện';

        });

        var LatLngDefault = {lat: {{$data['latitude']}}, lng: {{$data['longitude']}}};
        function initMap(){
            var input = document.getElementById('pac-input');

            var autocompleteAddress = new google.maps.places.Autocomplete(input);
            map = new google.maps.Map(document.getElementById('map'), {zoom: 10,center: LatLngDefault});
            marker = new google.maps.Marker({position:LatLngDefault,map:map,draggable :true,animation: google.maps.Animation.BOUNCE});

//            infowindow = new google.maps.InfoWindow();
//            var infowindowContent = document.getElementById('infowindow-content');
//            infowindow.setContent(infowindowContent);

            autocompleteAddress.setComponentRestrictions({'country':['vn']});
            autocompleteAddress.addListener('place_changed',function(){
                var latlngPlace = autocompleteAddress.getPlace();
                changeLatLngDefault(latlngPlace.geometry.location.lat(),latlngPlace.geometry.location.lng());
                changeMarker();
            });


            marker.addListener('drag',function(){
                changeLatLngDefault(marker.getPosition().lat(),marker.getPosition().lng());
            });

            changeLatLngDefault(LatLngDefault.lat,LatLngDefault.lng);
        }

        function changeMarker(){
            marker.setPosition(LatLngDefault);
            map.setCenter(LatLngDefault);
        }

        function changeLatLngDefault(lat,lng){
            LatLngDefault = { lat : lat , lng : lng};
            $('#txtViDo').val(lat);
            $('#txtKinhDo').val(lng);
        }

        marker.addListener('drag', function () {
            marker.setAnimation(null);
            $('#txtKinhDo').val(marker.getPosition().lng());
            $('#txtViDo').val(marker.getPosition().lat());
        });
        marker.addListener('dragend', function () {
            geocodeLatLng(geocoder, map, infowindow);
        });

    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key={!! API_GOOGLE_KEY !!}&libraries=places&callback=initMap&language=vn"
            type="text/javascript"></script>

@endsection