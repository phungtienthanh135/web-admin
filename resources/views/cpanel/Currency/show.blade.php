
@extends('cpanel.template.layout')
@section('title', 'Danh mục tiền tệ')
@section('content')

    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Danh mục tiền tệ</h3></div>
            </div>
        </div>

        <div class="innerLR">
            <div class="row-fluid">
                <div class="control-box">
                    <h4>Tìm tiền</h4>
                    <div class="row-fluid">
                        <div class="span5">
                            <label for="cbb_LoaiTien">Loại tiền</label>
                            <input type="text" class="span12" name="type" id="cbb_LoaiTien"/>
                        </div>

                        <div class="span2">
                            <label class="separator hidden-phone" for="add"></label>
                            <button class="btn btn-info btn-flat-full hidden-phone" id="search">TÌM LOẠI TIỀN</button>
                            <button class="btn btn-info btn-flat visible-phone" id="search">TÌM LOẠI TIỀN</button>
                        </div>

                        <div class="span2">
                            <label class="separator hidden-phone" for="add"></label>
                            <button data-toggle="modal" data-target="#add_currency" class="btn btn-warning btn-flat-full hidden-phone" id="add">THÊM LOẠI TIỀN</button>
                            <button data-toggle="modal" data-target="#add_currency" class="btn btn-warning btn-flat visible-phone" id="add">THÊM LOẠI TIỀN</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Loại tiền</th>
                        <th>Tỷ giá</th>
                        <th>Mặc định</th>
                        <th>Ghi chú</th>
                        <th>Tuỳ chọn</th>
                    </tr>
                    </thead>
                    <tbody>

                        @forelse($result as $row)
                            <tr>
                                <td>{{$row['foreignCurrencyType']}}</td>
                                <td>@moneyFormat($row['exchangeRate'])</td>
                                <td>{{$row['isActive']}}</td>
                                <td>{{@$row['note']}}</td>
                                <td class="center">
                                    <a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                        <ul class="onclick-menu-content">
                                            <li>
                                                <button data-toggle="modal" data-target="#moadel_edit"
                                                        onclick="">
                                                    Sửa
                                                </button>
                                            </li>
                                            <li>
                                                <button data-toggle="modal" data-target="#modal_delete"
                                                        onclick="$('#id_delete').val('{{$row['foreignCurrencyId']}}')">Xóa
                                                </button>
                                            </li>
                                        </ul>
                                    </a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="center" colspan="4">Hiện không có dữ liệu</td>
                            </tr>
                        @endforelse

                    </tbody>
                </table>
            </div>
            <div class="separator bottom"></div>
            <div class="row-fluid" style="display: none;" id="info">

            </div>

            <div class="clearfix"></div>
            <div class="modal modal-custom hide fade" id="add_currency">
                <div class="modal-header center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>THÊM GIAO DỊCH</h3>
                </div>
                <form action="{{action('CurrencyController@postAdd')}}" class="form-horizontal row-fluid" method="post">
                    <div style="max-height: 500px;" class="modal-body">

                        <div class="control-group">
                            <label class="control-label" for="txtForeignCurrencyType">Loại Tiền tệ</label>
                            <div class="controls">
                                <input value="{{old('foreignCurrencyType')}}" class="span12" type="text" name="foreignCurrencyType" id="txtForeignCurrencyType">
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="txtExchangeRate">Tỷ giá</label>
                            <div class="controls">
                                <input value="{{old('exchangeRate')}}" class="span12" type="text" name="exchangeRate" id="txtExchangeRate">
                            </div>
                        </div>

                       {{-- <div class="control-group">
                            <label class="control-label" for="txtMacDinh">Mặc định</label>
                            <div class="controls">
                                <input value="1" class="span12" type="text" name="" id="txtMacDinh">
                            </div>
                        </div>--}}


                        <div class="control-group">
                            <label class="control-label" for="txtNote">Ghi Chú</label>
                            <div class="controls">
                                <textarea class="span12" id="txtNote" rows="2" name="note"></textarea>
                            </div>
                        </div>
    
                        <div class="control-group">
                            <div class="controls">
                                <div class="row-fluid">
                                    <div class="span9">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <button class="btn btn-warning btn-flat-full">THÊM MỚI</button>
                                    </div>
                                    <div class="span3">
                                        <a data-dismiss="modal" class="btn btn-default btn-flat no_border bg_light">HUỶ</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                    </div>
                </form>
            </div>

            <div class="modal hide fade" id="modal_delete" style="width: 250px; margin-left: -10%;margin-top: 10%;">
                <div class="modal-body center">

                    <p>Bạn có chắc muốn xoá?</p>
                </div>
                <form action="{{action('CurrencyController@delete')}}" method="post">
                    <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="foreignCurrencyId" value="" id="id_delete">
                        <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                        <button class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">

    </script>
    <!-- End Content -->

@endsection