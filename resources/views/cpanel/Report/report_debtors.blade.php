@php
    session_start();
@endphp
@extends('cpanel.template.layout')
@section('title', 'Báo cáo công nợ Đại lí')
@section('content')
    <style>
        #tb_report{
            margin-top: 50px;
        }
    </style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Báo cáo công nợ Đại lí</h3>
                </div>
                <div class="pull-right span4 t_align_r">
                    {{--<div class="row-fluid">--}}
                        {{--<div class="input-append">--}}
                            {{--<button onclick="showinfo(this,'chart')"--}}
                                    {{--class="btn btn-default btn-flat btn-report"><i--}}
                                        {{--class="iconic-chart"></i> Biểu đồ--}}
                            {{--</button>--}}
                            {{--<button onclick="showinfo(this,'table')"--}}
                                    {{--class="btn-activate btn btn-default btn-flat btn-report"><i--}}
                                        {{--class="icon-list-alt"></i> Chi tiết--}}
                            {{--</button>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i
                                        class="icon-print"></i> In
                            </button>
                            <div class="btn-group export-btn">
                                <button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất
                                    file <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="excel" data-fileName="bao_cao">Excel</a></li>
                                    <li><a id="pdf" data-fileName="bao_cao">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div >
            <div class="row-fluid">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Điều kiện lọc</h4>
                    </div>
                    <form action="">
                        @if(empty(session('userLogin')['userInfo']['listAgency']))
                            <div class="row-fluid">
                                <div class="span3">
                                    <label for="cbb_NguonThu">Nguồn thu</label>
                                    <select name="agencyId" id="cbb_NguonThu"
                                            value="{{ is_null(request('agencyId')) ?"":request('agencyId') }}">
                                        <option value="">Tất cả</option>
                                        <option selected>{{ is_null(request('tenNguonThu')) ?"Tất cả":request('tenNguonThu') }}</option>
                                    </select>
                                    <input type="hidden" id="tenNguonThu" name="tenNguonThu">
                                </div>
                                <div class="span3">
                                    <label for="cbb_route">Tuyến</label>
                                    <select name="routeId" id="cbb_route">
                                        <option value="">Tất cả</option>
                                        @foreach($listRoute as $route)
                                            <option {{request('routeId')==$route['routeId']?'selected':''}} value="{{$route['routeId']}}">{{$route['routeName']}}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" id="route" name="route">
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span3">
                                    <label for="txtStartDate">Từ ngày</label>
                                    <div class="input-append">
                                        <input class="span11" autocomplete="off" value="{{request('startDate',date('d-m-Y',time()))}}"
                                               id="txtStartDate" name="startDate" type="text" readonly>
                                    </div>
                                </div>

                                <div class="span3">
                                    <label for="txtEndDate">Đến ngày</label>
                                    <div class="input-append">
                                        <input  class="span11" autocomplete="off" value="{{request('endDate',date('d-m-Y'))}}"
                                               id="txtEndDate" name="endDate" type="text" readonly>
                                    </div>
                                </div>
                                <div class="span3 " style="padding-right: 30px">
                                    <label class="separator hidden-phone" for="add"></label>
                                    <button class="btn btn-info btn-flat-full hidden-phone">LỌC</button>
                                    <button class="btn btn-info btn-flat visible-phone">LỌC</button>
                                </div>
                            </div>
                        @endif
                    </form>
                </div>

                @php
                    $totalTicket=0;
                    $totalPrice=0;
                    $totalPaidMoney=0;
                    $totalUnPaidMoney=0;

                    $listMoney = [
                        'price',
                        'paidMoney',
                        'unPaidMoney'
                    ];

                    $dateFormat = [
                        'time'
                    ];

                    if( !isset($results['code']) && count($results)>0)
                    foreach($results as $result):
                    foreach($result as $row):
                        $totalTicket += 1;
                        $totalPrice += (float)($row['price']);
                        $totalPaidMoney += (float)($row['paidMoney']);
                        $totalUnPaidMoney += (float)($row['unPaidMoney']);
                        endforeach;
                    endforeach;
                   $_SESSION['totalprice']=$totalPrice;
                @endphp
                <div class="row-fluid bg_light" id="tb_report">
                    <table class=" tb_report table table-striped   ">
                        <thead>
                        {{--<tr class="total">--}}
                            {{--<td class="center ">{{$totalTicket}}</td>--}}
                            {{--<td class="center">@moneyFormat($totalPrice)</td>--}}
                            {{--<td class="center">@moneyFormat($totalPaidMoney)</td>--}}
                            {{--<td class="center">@moneyFormat($totalUnPaidMoney)</td>--}}
                            {{--<td class="center"></td>--}}
                            {{--<td class="center"></td>--}}
                            {{--<td class="center"></td>--}}
                            {{--<td class="center"></td>--}}
                            {{--<td class="center"></td>--}}
                            {{--<td class="center"></td>--}}
                            {{--<td class="center"></td>--}}
                            {{--<td class="center"></td>--}}
                        {{--</tr>--}}
                        <tr>
                            {{--<th class="center">Mã vé</th>--}}
                            <th class="center">Số vé</th>
                            {{--<th class="center">Tuyến</th>--}}
                            {{--<th class="center">Điểm khởi hành</th>--}}
                            {{--<th class="center">HK</th>--}}
                            {{--<th class="center">SĐT</th>--}}
                            <th class="center ">Thành tiền</th>
                            <th class="center ">Đã thu</th>
                            <th class="center ">Còn lại</th>
                            <th class="center">Đại lí</th>
                            <th class="center">Mã đại lí</th>
                            <th class="center"></th>
                            <th class="center"></th>
                            <th class="center"></th>
                            <th class="center"></th>
                            <th class="center"></th>
                            <th class="center"></th>
                            {{--<th class="center">Ngày</th>--}}
                            {{--<th class="center">Người bán</th>--}}
                            {{--<th class="center">Người thu</th>--}}
                            {{--<th class="center">Ghi chú</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @if( !isset($results['code'])  && count($results)>0)
                            @foreach($results as $key=>$result)
                                @php
                                    $number=0;
                                    $priceRow=0;
                                    $paidMoney=0;
                                    $unPaidMoney=0;
                                    $_SESSION['tml']='';
                                @endphp
                                @foreach($result as $row)
                                    @if(isset($row['ticketId']))
                                        @php
                                            $number += (float)@$row['number'];
                                            $priceRow += (float)(@$row['price']);
                                            $paidMoney += (float)(@$row['paidMoney']);
                                            $unPaidMoney += (float)(@$row['unPaidMoney']);
                                            $sourceName= @$row['sourceName'];
                                            $sourceId=$key;
                                        $_SESSION['tml']=$_SESSION['tml'].(
                                       '<tr class="hide '.$key.'"><td class="center">'.@$row['ticketId'].'</td>'.
                                       '<td class="center">'.number_format(@$row['price']).'</td>'.
                                       '<td class="center">'.number_format(@$row['paidMoney']).'</td>'.
                                       '<td class="center">'.number_format(@$row['unPaidMoney']).'</td>'.
                                       '<td class="center">'.@$row['number'].'</td>'.
                                       '<td class="center">'.@$row['inPoint'].'</td>'.
                                       '<td class="center">'.@$row['nameGuest'].'</td>'.
                                       '<td class="center">'.@$row['phoneNumber'].'</td>'.
                                       '<td class="center">'.date('d-m-Y',@$row['time']/1000).'</td>'.
                                       '<td class="center">'.@$row['sellerName'].'</td>'.
                                       '<td class="center">'.@$row['cashier'].'</td>'.
                                       '<td class="center">'.@$row['note'].'</td></tr>');
                                        @endphp
                                    @endif
                                @endforeach
                                @if(count($result) >0)
                                    <tr class="item" data-key="{{$key}}">
                                        <td class="center ">{{count($result)}}</td>
                                        <td class="center">{{number_format($priceRow)}}</td>
                                        <td class="center ">{{number_format($paidMoney)}}</td>
                                        <td class="center">{{number_format($unPaidMoney)}}</td>
                                        <td class="center">{{$sourceName}}</td>
                                        <td class="center">{{$key}}</td>
                                        <td class="center"></td>
                                        <td class="center"></td>
                                        <td class="center"></td>
                                        <td class="center"></td>
                                        <td class="center"></td>
                                        <td class="center"></td>
                                    </tr>
                                    <tr class=" hide {{$key}}" style="color: #0b2c89">
                                        <th class="center">Mã vé</th>
                                        <th class="center">Thành tiền</th>
                                        <th class="center">Đã thu</th>
                                        <th class="center">Còn lại</th>
                                        <th class="center">Số ghế</th>
                                        <th class="center ">Khởi hành</th>
                                        <th class="center">HK</th>
                                        <th class="center">SĐT</th>
                                        <th class="center">Ngày</th>
                                        <th class="center">Người bán</th>
                                        <th class="center">Người thu</th>
                                        <th class="center">Ghi chú</th>

                                    </tr>
                                    @php echo $_SESSION['tml']; @endphp
                                @endif
                            @endforeach

                            <tr class="total">
                                <th class="center">TỔNG</th>
                                <td class="center">@moneyFormat($totalPrice)</td>
                                <td class="center">@moneyFormat($totalPaidMoney)</td>
                                <td class="center">@moneyFormat($totalUnPaidMoney)</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                @include('cpanel.template.pagination-without-number',['page'=>$page])
                <div class="row-fluid" id="chart_report">
                    <div class="widget-body">
                        <div id="cong_no" style="height: 250px"></div>
                    </div>
                </div>
            </div>

            @include('cpanel.Print.print')
        </div>
    </div>
    <script src="/public/javascript/printThis.js" type="text/javascript"></script>
    <script type="text/javascript">

        setTimeout(function () {
            $('#chart_report').hide();
        }, 1000);
        $('#cbb_NguonThu').change(function () {
            $('#tenNguonThu').val($(this).children(":selected").text());
            $('#cbb_NguonThu').attr('value', $(this).children(":selected").attr('value'));
        });
        $('#cbb_route').change(function () {
            $('#route').val($(this).children(":selected").text());
            $('#routeId').attr('value', $(this).children(":selected").attr('value'));
        });

        $(document).ready(function () {
            $('tbody > tr.item').click(function () {
                var key = $(this).data('key');
                $('.' + key).toggleClass("hide");
            });
            $('.userList').select2();

            $('#cbb_NguonThu').select2({
                dropdownCss: {'z-index': '999999'},
//                containerCss: {'width': '273px'},
                minimumInputLength: 0,
                placeholder: "Chọn đại lý",
                ajax: {
                    url: "{{action('AgencyController@getListAgency')}}",
                    delay: 1000,
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            keyword: params.term
                        };
                        return query;
                    },
                    processResults: function (data) {
                        var r = [];
                        r.push({text: "Tất cả", id: "-1"});
                        $.map(data, function (item) {
                            if (data.length > 0) {
                                r.push({text: item.fullName, id: item.userId})
                            }
                        });
                        return {
                            results: r
                        };
                    },
                    cache: true,
                }
            });

        });


        function showinfo(oject, type) {
            switch (type) {
                case 'table': {
                    $('#tb_report').show();
                    $('.pager').show();
                    $('#chart_report').hide();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');


                    break;
                }
                case 'chart': {
                    $('#tb_report').hide();
                    $('.pager').hide();
                    $('#chart_report').show();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');

                    break;
                }
                default: {
                    break;
                }
            }
        }

        function printDiv(divName) {
            if ($('#tb_report').is(":visible")) {
                $('#' + divName).printThis({
                    pageTitle: '&nbsp;'
                });
            }
        }
    </script>
    <!-- End Content -->
    <script>

        // charts data

        charts_data = {
            data: {
                @if( !isset($results['code'])  && count($results)>0)
                d1: [{{$_SESSION['totalprice']}}],
                d2: [  @foreach($results as $result)
                        @foreach($result as $row)
                          [{{@$row['time']}}, {{@$row['price']}}]
                    , @endforeach  @endforeach]
                @endif
            }

        };
    </script>
    <script src="/public/javascript/jquery.printPage.js" type="text/javascript"></script>
    <script src="/public/javascript/printThis.js" type="text/javascript"></script>
    <script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
    <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>
    <!-- Charts Helper Demo Script -->
    <script src="/public/javascript/chart/charts.helper.js"></script>
    <!-- Charts Page Demo Script -->
    <script src="/public/javascript/chart/charts.js"></script>
    <!--  Flot (Charts) JS -->
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.selection.js"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.orderBars.js"
            type="text/javascript"></script>
    <!-- Add thư viện pdf -->
    <script src="https://github.com/niklasvh/html2canvas/releases/download/v0.5.0-beta4/html2canvas.min.js"></script>
    <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
@endsection