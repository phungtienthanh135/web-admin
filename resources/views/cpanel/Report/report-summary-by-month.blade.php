@extends('cpanel.template.layout')
@section('title', 'Báo cáo doanh thu tổng hợp theo tháng')
@section('content')
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Báo cáo tổng hợp theo tháng</h3>
                </div>
                <div class="pull-right span4 t_align_r">
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="showinfo(this,'chart')"
                                    class="btn btn-default btn-flat btn-report"><i
                                        class="iconic-chart"></i> Biểu đồ
                            </button>
                            <button onclick="showinfo(this,'table')"
                                    class="btn-activate btn btn-default btn-flat btn-report"><i
                                        class="icon-list-alt"></i> Chi tiết
                            </button>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i
                                        class="icon-print"></i> In
                            </button>
                            <div class="btn-group export-btn">
                                <button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất file <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="excel" data-fileName="bao_cao">Excel</a></li>
                                    <li><a id="pdf" data-fileName="bao_cao">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="innerLR">
            <div class="row-fluid">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Điều kiện lọc</h4>
                    </div>
                    <div class="row-fluid">
                        <form action="">
                            <div class="span2">
                                <label for="cbb_Thang">Tháng</label>
                                <select class="span12" name="month" id="cbb_Thang">
                                    <option value="all">Tất cả</option>
                                    @for($i=1;$i<13;$i++)
                                        <option {{request('month',getdate()['mon'])==$i?'selected':''}}  value="{{$i}}">
                                            Tháng {{$i}}</option>
                                    @endfor
                                </select>
                            </div>

                            <div class="span2">
                                <label for="txtDenNgay">Năm</label>
                                <input class="span12" type="number" name="year" id="txtDenNgay"
                                       value="{{request('year',getdate()['year'])}}">
                            </div>

                            <div class="span3">
                                <label for="cbb_Xe">Biển xe</label>

                                <select name="numberPlate" id="cbb_Xe">
                                    <option value="">Tất cả</option>
                                    @foreach($listVehicle as $Vehicle)
                                        <option {{request('numberPlate')==$Vehicle['numberPlate']?'selected':''}}  value="{{$Vehicle['numberPlate']}}">{{$Vehicle['numberPlate']}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="span3">
                                <label for="cbb_Tuyen">Tuyến</label>

                                <select name="routeId" id="cbb_Tuyen">
                                    <option value="">Tất cả</option>
                                    @foreach($listRoute as $route)
                                        <option {{request('routeId')==$route['routeId']?'selected':''}}  value="{{$route['routeId']}}">{{$route['routeName']}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="span2">
                                <label class="separator hidden-phone" for="add"></label>
                                <button class="btn btn-info btn-flat-full hidden-phone" id="search">LỌC</button>
                                <button class="btn btn-info btn-flat visible-phone" id="search">LỌC</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <div class="row-fluid" id="tb_report">

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="center">Tháng</th>
                        <th>Số vé</th>
                        <th>Doanh thu</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                    $totalTicket=0;
                    $totalRevenue=0;
                    @endphp
                    @if(request('month')!='all')
                        <tr>
                            <td class="center">{{$result['month']}}</td>
                            <td>{{$result['totalTicket']}}</td>
                            <td>@moneyFormat($result['totalRevenue'])</td>
                        </tr>

                        @php
                            $totalTicket+=$result['totalTicket'];
                            $totalRevenue+=$result['totalRevenue'];
                        @endphp
                    @else
                        @foreach($result as $row)
                            <tr>
                                <td class="center">{{$row['month']}}</td>
                                <td>{{$row['totalTicket']}}</td>
                                <td>@moneyFormat($row['totalRevenue'])</td>
                            </tr>
                            @php
                                $totalTicket+=$row['totalTicket'];
                                $totalRevenue+=$row['totalRevenue'];
                            @endphp
                        @endforeach
                    @endif
                    <tr class="total">
                        <th class="center">TỔNG DOANH THU</th>
                        <td>{{$totalTicket}}</td>
                        <td>@moneyFormat($totalRevenue)</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="row-fluid" id="chart_report">
                <div class="widget-body">
                    <div id="chart_by_date" style="height: 250px;"></div>
                </div>
            </div>
        </div>
    </div>
    @include('cpanel.Print.print')
    <script type="text/javascript">
        setTimeout(function () {
            $('#chart_report').hide()
        }, 1000);

        function showinfo(oject, type) {
            switch (type) {
                case 'table': {
                    $('#tb_report').show();
                    $('#chart_report').hide();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');


                    break;
                }
                case 'chart': {
                    $('#tb_report').hide();
                    $('#chart_report').show();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');

                    break;
                }
                default: {
                    break;
                }
            }
        }

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            $('#template_report_content').html(printContents);

            document.body.innerHTML = $('#template_report').html();

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
    <!-- End Content -->
    @if(request('month')=='all')
    <script>

        // charts data
        charts_data = {
            data: {
                d1: [@foreach($result as $row)[{{$row['month']}}, {{$row['totalTicket']}}], @endforeach ],
                d2: [@foreach($result as $row)[{{$row['month']}}, {{$row['totalRevenue']}}], @endforeach ]
            }

        };
    </script>
    <!-- Table Export To File -->
    <script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
    <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>
    <!-- Charts Helper Demo Script -->
    <script src="/public/javascript/chart/charts.helper.js"></script>

    <!-- Charts Page Demo Script -->
    <script src="/public/javascript/chart/charts.js"></script>
    <!--  Flot (Charts) JS -->
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.selection.js"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.orderBars.js" type="text/javascript"></script>
    <!-- End Content -->
    <!-- Add thư viện pdf -->
    <script src="https://github.com/niklasvh/html2canvas/releases/download/v0.5.0-beta4/html2canvas.min.js"></script>
    <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
    @else
        <script>
            $('#chart_report').html('<h5 class="center">Lọc tháng tất cả để xem biểu đồ</h5>');
            $('#template_report_title').text('An Vui - Báo cáo tổng hợp theo tháng');
        </script>
    @endif
@endsection