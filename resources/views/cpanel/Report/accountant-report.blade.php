@extends('cpanel.template.layout')
@section('title', 'Báo cáo doanh thu chi tiết theo ngày')
@section('content')
    <style>
        #tb_report {
            margin-top: 50px;
            margin-left: 5px;
            font-size: 11.5px;
        }
    </style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Báo cáo kế toán</h3>
                </div>
                <div class="pull-right span4 t_align_r">
                    {{--<div class="row-fluid">--}}
                        {{--<div class="input-append">--}}
                            {{--<button onclick="showinfo(this,'chart')"--}}
                                    {{--class="btn btn-default btn-flat btn-report"><i--}}
                                        {{--class="iconic-chart"></i> Biểu đồ--}}
                            {{--</button>--}}
                            {{--<button onclick="showinfo(this,'table')"--}}
                                    {{--class="btn-activate btn btn-default btn-flat btn-report"><i--}}
                                        {{--class="icon-list-alt"></i> Chi tiết--}}
                            {{--</button>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i
                                        class="icon-print"></i> In
                            </button>
                            <div class="btn-group export-btn">
                                <button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất
                                    file <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="excel" data-fileName="bao_cao">Excel</a></li>
                                    <li><a id="pdf" data-fileName="bao_cao">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div >
            <div class="row-fluid">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Điều kiện lọc</h4>
                    </div>
                    <form action="">
                            <div class="row-fluid">
                                <div class="span4">
                                    <label for="txtStartDate">Từ ngày</label>
                                    <div class="input-append">
                                        <input autocomplete="off" value="{{request('startDate',date('d-m-Y',time()))}}"
                                               id="txtStartDate" name="startDate" type="text" readonly>
                                    </div>
                                </div>

                                <div class="span4">
                                    <label for="txtEndDate">Đến ngày</label>
                                    <div class="input-append">
                                        <input autocomplete="off" value="{{request('endDate',date('d-m-Y'))}}"
                                               id="txtEndDate" name="endDate" type="text" readonly>
                                    </div>
                                </div>

                                <div class="span4">
                                    <div class="span3">
                                        <label class="separator hidden-phone" for="add"></label>
                                        <button class="btn btn-info btn-flat-full" id="search">LỌC</button>
                                    </div>
                                </div>
                            </div>
                    </form>

                </div>
            </div>
            <div class="row-fluid bg_light table-responsive" id="tb_report">
                <table class="table table-striped   ">
                    <thead>

                    {{--<tr style="display: none">--}}
                        {{--<td colspan="9" align="center">--}}
                            {{--<h3>BÁO CÁO DOANH THU BÁN VÉ</h3>--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                    {{--<tr style="display: none">--}}
                        {{--<td colspan="9">--}}
                            {{--<h6>Từ ngày: {{request('startDate',date('d/m/Y',time()))}} Đến--}}
                                {{--ngày: {{request('endDate',date('d/m/Y',time()))}}</h6>--}}
                        {{--</td>--}}
                    {{--</tr>--}}

                    {{--@php--}}
                        {{--$totalTicket=0;--}}
                        {{--$totalPrice=0;--}}
                        {{--$totalIntoMoney=0;--}}
                        {{--$totalCashMoney = 0;--}}
                        {{--$tongTienThieu=0;--}}

                        {{--$listMoney = [--}}
                            {{--'price'--}}
                        {{--];--}}

                        {{--foreach($result as $row) :--}}
                            {{--//$row['intoMoney']=$row['number']!=0?$row['number']*$row['price']:$row['price'];--}}
                            {{--$totalTicket+=$row['number'];--}}
                            {{--$tongTienThieu+=$row['price'] - $row['intoMoney'];--}}
                            {{--$totalPrice+=$row['price'];--}}
                            {{--$totalIntoMoney+=$row['intoMoney'];--}}
                            {{--$tmpTotalCashMoney = $row['paymentType']==3?$row['intoMoney']:0;--}}
                            {{--$totalCashMoney += $tmpTotalCashMoney;--}}
                        {{--endforeach;--}}

                    {{--@endphp--}}
                    {{--<tr class="total">--}}
                        {{--<th colspan="{{count(REPORT_ROW)-3}}">TỔNG DOANH THU</th>--}}
                        {{--<td></td>--}}
                        {{--<th class="center">{{$totalTicket}}</th>--}}
                        {{--<td>@moneyFormat($totalIntoMoney)</td>--}}
                        {{--<td>@moneyFormat($totalPrice)</td>--}}
                        {{--<td>@moneyFormat($tongTienThieu)</td>--}}

                    {{--</tr>--}}
                    <tr>
                        <th>Hiển thị trên sổ</th>
                        <th>Hình thức bán hàng</th>
                        <th>Phương thức thanh toán</th>
                        <th>Kiêm phiếu xuất kho</th>
                        <th>Lập kèm hóa đơn</th>
                        <th>Đã lập hóa đơn</th>
                        <th>Ngày hạch toán (*)</th>
                        <th>Ngày chứng từ (*)</th>
                        <th>Số chứng từ (*)</th>
                        <th>Mã khách hàng(*)</th>
                        <th>Tên khách hàng(*)</th>
                        <th>Mã đại lý</th>
                        <th>Tên đại lý</th>
                        <th>Địa chỉ</th>
                        <th>Mã số thuế</th>
                        <th>Diễn giải</th>
                        <th>Nộp vào TK</th>
                        <th>NV bán hàng</th>
                        <th>Loại tiền</th>
                        <th>Tỷ giá</th>
                        <th>Công trình</th>
                        <th>Mã hàng (*)</th>
                        <th>Tên hàng</th>
                        <th>Hàng khuyến mại</th>
                        <th>TK Tiền/Chi phí/Nợ (*)</th>
                        <th>TK Doanh thu/Có (*)</th>
                        <th>ĐVT</th>
                        <th>Số lượng</th>
                        <th>Đơn giá sau thuế</th>
                        <th>Đơn giá</th>
                        <th>Thành tiền</th>
                        <th>Thành tiền quy đổi</th>
                        <th>Tỷ lệ CK (%)</th>
                        <th>Tiền chiết khấu</th>
                        <th>Tiền chiết khấu quy đổi</th>
                        <th>TK chiết khấu</th>
                        <th>Giá tính thuế XK</th>
                        <th>% thuế XK</th>
                        <th>Tiền thuế XK</th>
                        <th>TK thuế XK</th>
                        <th>% thuế GTGT</th>
                        <th>Tiền thuế GTGT</th>
                        <th>Tiền thuế GTGT quy đổi</th>
                        <th>TK thuế GTGT</th>
                        <th>HH không TH trên tờ khai thuế GTGT</th>
                        <th>Kho</th>
                        <th>TK giá vốn</th>
                        <th>TK Kho</th>
                        <th>Đơn giá vốn</th>
                        <th>Tiền vốn</th>
                        <th>Hàng hóa giữ hộ/bán hộ</th>
                        <th>Trường mở rông chung 1</th>
                        <th>Trường mở rông chung 2</th>
                        <th>Trường mở rông chung 3</th>
                        <th>Trường mở rông chung 4</th>
                        <th>Trường mở rông chung 5</th>
                        <th>Trường mở rông chung 6</th>
                        <th>Trường mở rông chung 7</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($report as $row)
                        <tr>
                            <td></td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>@dateFormat($row['startDate'])</td>
                            <td>@dateFormat($row['startDate'])</td>
                            <td>{{$row['documentId']}}</td>
                            <td>{{$row['customerId']}}</td>
                            <td>{{$row['customerName']}}</td>
                            <td>{{@$row['agencyId']}}</td>
                            <td>{{@$row['agencyName']}}</td>
                            <td>{{@$row['address']}}</td>
                            <td></td>
                            <td>{{@$row['note']}}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>{{@$row['trip']}}</td>
                            <td>{{!empty($row['booking']) ? $row['booking'] : 'BOOKING'}}</td>
                            <td>{{!empty($row['booking']) ? $row['booking'] : 'BOOKING'}}</td>
                            <td></td>
                            <td>131</td>
                            <td>5111</td>
                            <td></td>
                            <td>{{@$row['numberOfSeats']}}</td>
                            <td></td>
                            <td>{{@$row['price']}}</td>
                            <td>{{@($row['numberOfSeats']*$row['price'])}}</td>
                            <td></td>
                            <td>{{@$row['commission']*100}}</td>
                            <td>{{@($row['price']*$row['commission'])}}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>{{@$row['routeName']}}</td>
                            <td data-timeformat="{!! $row['startTime'] !!}"></td>
                            <td>{{@$row['code']}}</td>
                            <td>{{@$row['pickUpAddress']}}</td>
                            <td>{{@$row['numberOfSeats']}}</td>
                            <td>{{@$row['price']}}</td>
                            <td>{{@$row['extraPrice']}}</td>
                        </tr>

                    @endforeach

                    {{--<tr class="total">--}}
                        {{--<th colspan="{{count(REPORT_ROW)-3}}">TỔNG DOANH THU</th>--}}
                        {{--<td></td>--}}
                        {{--<th class="center">{{$totalTicket}}</th>--}}
                        {{--<td>@moneyFormat($totalIntoMoney)</td>--}}
                        {{--<td>@moneyFormat($totalPrice)</td>--}}
                        {{--<td>@moneyFormat($tongTienThieu)</td>--}}

                    {{--</tr>--}}
                    </tbody>
                    {{--{!! dev($report,false) !!}--}}
                </table>
            </div>
            <div class="row-fluid" id="chart_report">
                <div class="widget-body">
                    <div id="chart_by_date" style="height: 250px;"></div>
                </div>
            </div>
        </div>

        @include('cpanel.Print.print')
    </div>

    <script type="text/javascript">
        $("#content").attr("style", "width:100%");
        $("#menu").attr("style", "margin-left:-400px");
        $("#ht_btn_close_open_menu").html('<button onClick="open_menu()" type="button" class="btn-navbar"> <div class="icon_open_menu"></div> </button>');

        setTimeout(function () {
            $('#chart_report').hide();
        }, 1000);

        $(document).ready(function () {
            $('.userList').select2();
            $('[data-timeformat]').each(function (i,e) {
                var d= new Date($(e).data('timeformat'));
                var txt = d.getHours() + "h";
                if(d.getMinutes()<10){
                    txt=txt+"0"+d.getMinutes();
                }else{txt+=d.getMinutes();}
                $(e).text(txt);
            })
        });


        function showinfo(oject, type) {
            switch (type) {
                case 'table': {
                    $('#tb_report').show();
                    $('#chart_report').hide();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');


                    break;
                }
                case 'chart': {
                    $('#tb_report').hide();
                    $('#chart_report').show();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');

                    break;
                }
                default: {
                    break;
                }
            }
        }

        function printDiv(divName) {
            if ($('#tb_report').is(":visible")) {
                $('#' + divName).printThis({
                    pageTitle: '&nbsp;'
                });
            }
        }
    </script>
    <!-- End Content -->
    <script>

        // charts data
        charts_data = {
            data: {
                d2: [ ]
            }

        };
    </script>
    <script src="/public/javascript/jquery.printPage.js" type="text/javascript"></script>
    <script src="/public/javascript/printThis.js" type="text/javascript"></script>
    <!-- Table Export To File -->
    <script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
    <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>
    <!-- Charts Helper Demo Script -->
    <script src="/public/javascript/chart/charts.helper.js"></script>
    <!-- Charts Page Demo Script -->
    <script src="/public/javascript/chart/charts.js"></script>
    <!--  Flot (Charts) JS -->
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.selection.js"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.orderBars.js" type="text/javascript"></script>
    <!-- Add thư viện pdf -->
    <script src="https://github.com/niklasvh/html2canvas/releases/download/v0.5.0-beta4/html2canvas.min.js"></script>
    <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
@endsection