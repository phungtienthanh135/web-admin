@extends('cpanel.template.layout')
@section('title', 'Báo cáo công nợ với An Vui')
@section('content')
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Báo cáo doanh thu online</h3>
                </div>


                <div class="pull-right span4 t_align_r">
                    <div class="row-fluid">
                        <div class="input-append">
                        {{--<button onclick="showinfo(this,'chart')" class=" btn btn-default btn-flat btn-report"><i--}}
                        {{--class="iconic-chart"></i> Biểu đồ--}}
                        {{--</button>--}}
                        <!--  <button onclick="showinfo(this,'table')"
                                 class="btn-activate btn btn-default btn-flat btn-report"><i
                                     class="icon-list-alt"></i> Chi tiết
                         </button> -->
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i
                                        class="icon-print"></i> In
                            </button>

                            <div class="btn-group export-btn">
                                <button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất
                                    file <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="excel" data-fileName="bao_cao_doanh_thu_online">Excel</a></li>
                                    <li><a id="pdf">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <div>
            <div class="row-fluid" style="background: #dedfe1;">

            </div>
            <div class="row-fluid" id="tb_report">

                <table class="table table-striped">
                    @php
                        $totalMoney = 0;
                        foreach($result as $row)
                        {
                            $totalMoney += $row['totalMoney'];
                        }
                    @endphp
                    <thead>
                    <tr class="total">
                        <td colspan="5">TỔNG</td>
                        <td class="center">@moneyFormat($totalMoney/100)</td>
                        <td class="center"></td>
                    </tr>
                    <tr>
                        <th class="center">STT</th>
                        <th class="center">Mã vé</th>
                        <th class="center">Tài khoản</th>
                        <th class="center">Tên khách</th>
                        <th class="center">Tiền (VNĐ)</th>
                        <th class="center">Loại</th>
                        <th class="center">Ngày thanh toán</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($count = 1)
                    @foreach($result as $row)
                        {{--@php(dev($row['tickets'][0]['ticketCode'], false))--}}
                        <tr>
                            <td class="center">{{$count}}</td>
                            @if(count($row['tickets']) > 1)
                                <td class="center">{{'('.@$row['tickets'][0]['ticketCode'].') ('.@$row['tickets'][1]['ticketCode'].')'}}</td>
                            @else
                                <td class="center">{{@$row['tickets'][0]['ticketCode']}}</td>
                            @endif
                            <td class="center">{{@$row['merchantId']}}</td>
                            <td class="center">{{$row['tickets'][0]['fullName']}}</td>
                            <td class="center">{{$row['totalMoney']/100}}</td>
                            @php
                                if(!isset($row['scope'])) {
                                    $row['scope'] = 0;
                                }
                            @endphp
                            <td class="center">{{$row['scope'] == 1 ? 'Quốc tế' : 'Nội địa'}}</td>
                            <td class="center">@dateFormat($row['createdDate'])</td>
                        </tr>
                        @php($count++)
                    @endforeach
                    </tbody>
                    <tr class="total">
                        <td colspan="5">TỔNG</td>
                        <td class="center">@moneyFormat($totalMoney/100)</td>
                        <td class="center"></td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
    @include('cpanel.Print.print')
    <script type="text/javascript">

    </script>
    <!-- Table Export To File -->
    <script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
    <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>
    <link rel="stylesheet" href="/public/javascript/wickedpicker/wickedpicker.css">
    <script type="text/javascript" src="/public/javascript/wickedpicker/wickedpicker.js"></script>
    <script>

        $(function () {

            $('#cbb_listDriverId,#cbb_listAssistantId').select2();
            $('.btnUpdate').click(function () {
                var scheduleId = $(this).parents('tr').data('scheduleid'),
                    dateRun = $(this).parents('tr').data('starttime');
                $('#txtScheduleId').val(scheduleId);
                $('#txtDateRun').val(dateRun);
            });

            $('.btnUpdateTrip').click(function () {
                var scheduleId = $(this).parents('tr').data('scheduleid'),
                    day = $(this).parents('tr').data('day'),
                    dateRun = $(this).parents('tr').data('starttime'),
                    time = $(this).parents('tr').data('starttimeformat');
                //Cau hinh wickedpicker
                var options = {
                    now: time,
                    twentyFour: true,
                    upArrow: 'wickedpicker__controls__control-up',
                    downArrow: 'wickedpicker__controls__control-down',
                    close: 'wickedpicker__close',
                    hoverState: 'hover-state',
                    title: 'Chọn thời gian xuất bến',
                    clearable: false
                };

                $('#txt_newDate').wickedpicker(options);
                $('#scheduleId').val(scheduleId);
                $('#dateRun').val(dateRun);
                $('#day').val(day);
            });

        });

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;

        }
    </script>
    <!-- Add thư viện pdf -->
    <script src="https://github.com/niklasvh/html2canvas/releases/download/v0.5.0-beta4/html2canvas.min.js"></script>
    <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
    <!--  Flot (Charts) JS -->
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.selection.js"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.orderBars.js" type="text/javascript"></script>
    <!-- End Content -->

@endsection