@extends('cpanel.template.layout')
@section('title', 'Báo cáo kết quả theo chuyến')
@section('content')
    <style>
        #tb_report {
            margin-top: 50px;
            margin-left: 10px;
        }
        .border{
            border: 1px solid !important;
        }
        table td{
            max-width: 200px;
        }
    </style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Báo cáo theo điểm</h3>
                </div>
                <div class="pull-right span4 t_align_r">
                    <div class="row-fluid">
                        <div class="input-append">
                        {{--<button onclick="showinfo(this,'chart')" class=" btn btn-default btn-flat btn-report"><i--}}
                        {{--class="iconic-chart"></i> Biểu đồ--}}
                        {{--</button>--}}
                        <!--  <button onclick="showinfo(this,'table')"
                                    class="btn-activate btn btn-default btn-flat btn-report"><i
                                        class="icon-list-alt"></i> Chi tiết
                            </button> -->
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i
                                        class="icon-print"></i> In
                            </button>

                            <div class="btn-group export-btn">
                                <button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất
                                    file <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="excel" data-fileName="bao_cao_theo_diem">Excel</a></li>
                                    <li><a id="pdf">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <div>
            <div class="row-fluid filterProfit" style="background: #dedfe1;">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Điều kiện lọc</h4>
                    </div>
                    <form action="" method="get">

                        <div class="row-fluid">
                            <div class="span3">
                                <label for="cbb_Tuyen">Tuyến</label>
                                <select name="routeId" id="cbb_Tuyen">
                                    <option value="">Tất cả</option>
                                    @foreach($listRoute as $row)
                                        <option {{request('routeId') !=null &&request('routeId')==$row['routeId']?'selected':''}} value="{{$row['routeId']}}">{{$row['routeName']}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="span3">
                                <label for="txtStartDate">Từ ngày</label>
                                <div class="input-append">
                                    <input value="{{request('startDate',date('d-m-Y'))}}" class="span11"
                                           id="txtStartDate" name="startDate" type="text" readonly>
                                </div>
                            </div>

                            <div class="span3">
                                <label for="txtEndDate">Đến ngày</label>
                                <div class="input-append">
                                    <input value="{{request('endDate',date('d-m-Y'))}}" id="txtEndDate" name="endDate"
                                           class="span11" type="text" readonly>
                                </div>
                            </div>
                            <div class="span3">
                                <label class="separator hidden-phone" for="add"></label>
                                <button class="btn btn-info btn-flat-full hidden-phone" id="search">LỌC</button>
                                <button class="btn btn-info btn-flat visible-phone" id="search">LỌC</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="row-fluid" style=" margin-left: 15px;">
                <h4 style="margin-top: 20px;">Chọn nội dung hiển thị</h4>
                <select id="changeContent">
                    <option value="0">Theo tiền( VNĐ )</option>
                    <option value="1">Theo số lượng ghế</option>
                </select>
            </div>
            <div class="row-fluid bg_light" id="tb_report">

                <table class="table table table-striped  ">
                    <thead>
                    @if(count($result)>0)
                        @php $tong=0;
                        for($i=0;$i<count($result);$i++)
                          $tong+=$result[$i]['totalMoney'];
                        @endphp
                        <tr class="total">
                            <th class="border">
                                Tổng :{{number_format($tong)}}
                            </th>
                            @for($i=0;$i<count($route['listPoint']);$i++)
                                @php $tongcot=0;@endphp
                                @for($j=0;$j<count($route['listPoint']);$j++)
                                    @php $tongcot+=$result[$i+$j*count($route['listPoint'])]['totalMoney']; @endphp
                                @endfor
                                <td class="center border">{{number_format($tongcot)}}</td>
                            @endfor
                        </tr>
                    @endif
                    <tr>
                        <th class="disable_td border"></th>
                        @foreach($route['listPoint'] as $row)
                            <th class="center border">{{$row['pointName']}}</th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @php $dem=0;@endphp
                    @foreach($route['listPoint'] as $row)
                        <tr>
                            <th class="center border" style="    width: 120px;">{{$row['pointName']}}</th>
                            @for($i=$dem;$i<($dem+count($route['listPoint']));$i++)
                                <td class="center border">
                                    {{number_format($result[$i]['totalMoney'])}}
                                </td>
                            @endfor
                            @php $dem+=count($route['listPoint']);@endphp
                        </tr>
                    @endforeach
                    <tr class="total">
                        <th class="border">
                            Tổng: {{isset($tong)?number_format($tong):''}}
                        </th>
                        @for($i=0;$i<count($route['listPoint']);$i++)
                            @php $tongcot=0;@endphp
                            @for($j=0;$j<count($route['listPoint']);$j++)
                                @php $tongcot+=$result[$i+$j*count($route['listPoint'])]['totalMoney']; @endphp
                            @endfor
                            <td class="center border">{{number_format($tongcot)}}</td>
                        @endfor
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="row-fluid" id="chart_report">
                <div class="widget-body">
                    <div id="cong_no" style="height: 250px;"></div>
                </div>
            </div>
        </div>

    </div>
    @include('cpanel.Print.print')
    <script type="text/javascript">

    </script>
    <script>
        var listPoint ={!! json_encode($route['listPoint']) !!};
        var result ={!! json_encode($result) !!};
        $(document).ready(function () {
            $('body').on('change', '#changeContent', function () {
                var checkcontent = parseInt($(this).val());
                var dem = 0, html = '', htmltong = '', tong = 0;
                $.each(listPoint, function (k, v) {
                    html += ' <tr><th class="center border" style="    width: 120px;">' + v.pointName + '</th>';
                    for (var i = dem; i < (dem + listPoint.length); i++) {
                        if (checkcontent) {
                            html += ' <td class="center border">' + $.number(result[i].totalSeat) + '</td>';
                            tong += result[i].totalSeat;
                        } else {
                            html += ' <td class="center border">' + $.number(result[i].totalMoney) + '</td>';
                            tong += result[i].totalMoney;
                        }
                    }
                    html += '</tr>';
                    dem += listPoint.length;
                });
                htmltong += '<th>Tổng:' + $.number(tong) + '</th>';
                for (var i = 0; i < listPoint.length; i++) {
                    var tongcot = 0;
                    for (var j = 0; j < listPoint.length; j++)
                        if (checkcontent) tongcot += result[i + j * listPoint.length].totalSeat;
                        else tongcot += result[i + j * listPoint.length].totalMoney;
                    htmltong += '<td class="center border">' + $.number(tongcot) + '</td>';
                }
                $('table tbody').html(html + '<tr class="total">' + htmltong + '</tr>');
                $($('table thead tr')[0]).html(htmltong);
            });
        });

        $(function () {

            function printDiv(divName) {
                if ($('#tb_report').is(":visible")) {
                    $('#' + divName).printThis({
                        pageTitle: '&nbsp;'
                    });
                }
            }
        });
    </script>
    <script src="/public/javascript/jquery.printPage.js" type="text/javascript"></script>
    <script src="/public/javascript/printThis.js" type="text/javascript"></script>
    <script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
    <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>
    <!-- Add thư viện pdf -->
    <script src="https://github.com/niklasvh/html2canvas/releases/download/v0.5.0-beta4/html2canvas.min.js"></script>
    <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
    <!--  Flot (Charts) JS -->
    <!-- Table Export To File -->
    <link rel="stylesheet" href="/public/javascript/wickedpicker/wickedpicker.css">
    <script type="text/javascript" src="/public/javascript/wickedpicker/wickedpicker.js"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.selection.js"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.orderBars.js" type="text/javascript"></script>
    <!-- End Content -->

@endsection