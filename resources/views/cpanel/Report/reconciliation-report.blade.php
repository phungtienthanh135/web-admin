@extends('cpanel.template.layout')
@section('title', 'Báo cáo công nợ với An Vui')
@section('content')
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Báo cáo công nợ với An Vui</h3>
            </div>


                <div class="pull-right span4 t_align_r">
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="showinfo(this,'chart')" class=" btn btn-default btn-flat btn-report"><i
                                        class="iconic-chart"></i> Biểu đồ
                            </button>
                            <button onclick="showinfo(this,'table')"
                                    class="btn-activate btn btn-default btn-flat btn-report"><i
                                        class="icon-list-alt"></i> Chi tiết
                            </button>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i
                                        class="icon-print"></i> In
                            </button>

                            <div class="btn-group export-btn">
                                <button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất file <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="excel" data-fileName="bao_cao_cong_no">Excel</a></li>
                                    <li><a id="pdf">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <div class="innerLR">
            <div class="row-fluid">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Điều kiện lọc</h4>
                    </div>
                    <form action="" method="get">
                        <div class="row-fluid">
                            <div class="span3">
                                <label for="txtStartDate">Từ ngày</label>
                                <div class="input-append">
                                    <input value="{{request('startDate',date('d-m-Y',strtotime('-1 month')))}}"
                                           id="txtStartDate" name="startDate" type="text" readonly>
                                </div>
                            </div>

                            <div class="span3">
                                <label for="txtEndDate">Đến ngày</label>
                                <div class="input-append">
                                    <input value="{{request('endDate',date('d-m-Y'))}}" id="txtEndDate" name="endDate" type="text" readonly>
                                </div>
                            </div>


                            <div class="span3">
                                <label class="separator hidden-phone" for="add"></label>
                                <button class="btn btn-info btn-flat-full hidden-phone" id="search">LỌC</button>
                                <button class="btn btn-info btn-flat visible-phone" id="search">LỌC</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <div class="row-fluid" id="tb_report">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Chuyến xe</th>
                        {{--<th>An Vui bán</th>
                        <th>Vận Tải bán</th>--}}
                        <th>An Vui thu</th>
                        <th>Vận Tải thu</th>
                        <th>Hoa hồng An Vui</th>
                        <th>Tiền hoa hồng cho an vui</th>
                    </tr>
                    </thead>
                    <tbody>

                        @php
                            $i=1;
                            $anvuiMoney=0;
                            $assitantsMoney=0;
                            $commissionMoney=0;

                        @endphp

                        @forelse($result as $row)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{ $row['ticketId'] }}</td>
                                {{--<td>@moneyFormat($row['anvuiMoney'])</td>
                                <td>@moneyFormat($row['assitantsMoney'])</td>--}}
                                <td>@moneyFormat($row['anvuiMoney'])</td>
                                <td>@moneyFormat($row['assitantsMoney'])</td>
                                <td>{{ $row['commission']*100 }}%</td>
                                <td>@moneyFormat($row['commissionMoney'])</td>
                                @php
                                    $anvuiMoney+=$row['anvuiMoney'];
                                $assitantsMoney+=$row['assitantsMoney'];
                                $commissionMoney+=$row['commissionMoney'];
                                @endphp
                            </tr>
                            @empty
                            <tr>
                                <td class="center" colspan="6">Hiện tại không có dữ liệu</td>
                            </tr>
                        @endforelse
                        <tr class="total">
                            <td colspan="2">TỔNG DOANH THU</td>
                            {{--<td>400.000</td>
                            <td style="color: #333">450.000đ</td>--}}
                            <td>@moneyFormat($anvuiMoney)</td>
                            <td style="color: #333">@moneyFormat($assitantsMoney)</td>
                            <td style="color: #333"></td>
                            <td>@moneyFormat($commissionMoney)</td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="row-fluid" id="chart_report">
                <div class="widget-body">
                    <div id="cong_no" style="height: 250px;"></div>
                </div>
            </div>
        </div>

    </div>
    @include('cpanel.Print.print')
    <script type="text/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                $('#chart_report').hide();
            }, 1);
        });


        function showinfo(oject, type) {
            switch (type) {
                case 'table': {
                    $('#tb_report').show();
                    $('#chart_report').hide();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');


                    break;
                }
                case 'chart': {
                    $('#tb_report').hide();
                    $('#chart_report').show();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');

                    break;
                }
                default: {
                    break;
                }
            }
        }

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            $('#template_report_content').html(printContents);

            document.body.innerHTML = $('#template_report').html();

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>

    <script>
            // charts data
            charts_data = {
                data: {
                    d1: [@foreach($result as $row)[{{$row['createDate']}}, {{$row['assitantsMoney']}}], @endforeach ],
                    d2: [@foreach($result as $row)[{{$row['createDate']}}, {{$row['anvuiMoney']}}], @endforeach ]
                }

            };
    </script>
    <!-- Table Export To File -->
    <script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
    <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>

    <!-- Charts Helper Demo Script -->
    <script src="/public/javascript/chart/charts.helper.js"></script>

    <!-- Charts Page Demo Script -->
    <script src="/public/javascript/chart/charts.js"></script>
    <!--  Flot (Charts) JS -->
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.selection.js"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.orderBars.js" type="text/javascript"></script>
    <!-- End Content -->

@endsection