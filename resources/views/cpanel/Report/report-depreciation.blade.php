@extends('cpanel.template.layout')
@section('title', 'Báo cáo khấu hao tài sản')
@section('content')
    <style>
        #tb_report{
            margin-top: 50px;
            margin-left: 10px;
        }
        table td{
            max-width: 200px;
        }
    </style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Báo cáo khấu hao tài sản</h3>
                </div>
                <div class="pull-right span4 t_align_r">
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i
                                        class="icon-print"></i> In
                            </button>
                            <div class="btn-group export-btn">
                                <button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất
                                    file <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="excel" data-fileName="bao_cao">Excel</a></li>
                                    <li><a id="pdf" data-fileName="bao_cao">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div >
            <div class="row-fluid">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Tìm kiếm</h4>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">
                            <label for="txtCode">Mã tài sản</label>
                            <div class="input-append">
                                <input id="txtCode" type="text">
                            </div>
                        </div>
                        <div class="span3">
                            <label for="txtName">Tên tài sản</label>
                            <div class="input-append">
                                <input id="txtName" type="text">
                            </div>
                        </div>
                        <div class="span3">
                            <label for="txtDate">Ngày đưa vào sử dụng</label>
                            <div class="input-append">
                                <input autocomplete="off" id="txtDate" name="endDate" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                @php
                    $sumAsset=0;
                    $totalPrice=0;
                    $remainPrice=0;
                    $depreciationPrice=0;

                    if( !isset($results['code']) && count($results)>0)
                    foreach($results as $row):
                        $sumAsset ++;
                        $totalPrice += (float)($row['originalPrice']);
                        $remainPrice += (float)($row['depreciationRemain']);
                        $depreciationPrice += (float)($row['depreciationPrice']);
                    endforeach;
                @endphp
                <div class="row-fluid bg_light" id="tb_report">
                    <table class=" tb_report table table-striped   ">
                        <thead>
                        <tr class="total">
                            <th colspan="3">TỔNG</th>
                            <td class="right">@moneyFormat($totalPrice)</td>
                            <td class="right">@moneyFormat($remainPrice)</td>
                            <td class="right">@moneyFormat($depreciationPrice)</td>
                            <td></td>
                        </tr>
                        <tr>
                            <th class="center">STT</th>
                            <th class="center">Mã tài sản</th>
                            <th class="center">Tên</th>
                            <th class="center ">Tổng tài sản</th>
                            <th class="center ">Khấu hao</th>
                            <th class="center ">Còn lại</th>
                            <th class="center ">Ngày đưa vào sử dụng</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if( !isset($results['code']) && count($results)>0)
                            @foreach($results as $key=>$row)
                                <tr class="item">
                                    <td class="center ">{{$key +1}}</td>
                                    <td>{{@$row['assetId']}}</td>
                                    <td>{{@$row['assetName']}}</td>
                                    <td class="right">{{number_format(@$row['originalPrice'])}}</td>
                                    <td class="right">{{number_format(@$row['depreciationPrice'])}}</td>
                                    <td class="right">{{number_format(@$row['depreciationRemain'])}}</td>
                                    <td class="center">{{@$row['startDayUsing']}}</td>
                                </tr>
                            @endforeach
                            <tr class="total">
                                <th colspan="3">TỔNG</th>
                                <td class="right">@moneyFormat($totalPrice)</td>
                                <td class="right">@moneyFormat($remainPrice)</td>
                                <td class="right">@moneyFormat($depreciationPrice)</td>
                                <td></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>

            @include('cpanel.Print.print')
        </div>
    </div>
    <script src="/public/javascript/printThis.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtCode').change(function () {
                $('#txtName').val('');
                $('#txtDate').val('');

                var listTr = document.getElementsByClassName('item');
                for (var i = 0; i < listTr.length; i++){
                    var tr = listTr[i];$(tr).show();
                    for (var j = 0; j < tr.childNodes.length; j++) {
                        if ($('#txtCode').val() != '') {
                            if ($(tr.childNodes[j]).text() != '' && ($(tr.childNodes[j]).text() == $('#txtCode').val())) {
                                $(tr).show();
                                j = tr.childNodes.length;
                            }
                            else $(tr).hide();
                        }
                    }
                }
            });
            $('#txtName').change(function () {
                $('#txtCode').val('');
                $('#txtDate').val('');

                var listTr = document.getElementsByClassName('item');
                for (var i = 0; i < listTr.length; i++){
                    var tr = listTr[i];$(tr).show();
                    for (var j = 0; j < tr.childNodes.length; j++) {
                        if ($('#txtName').val() != '') {
                            if ($(tr.childNodes[j]).text() != '' && ($(tr.childNodes[j]).text() == $('#txtName').val())) {
                                $(tr).show();
                                j = tr.childNodes.length;
                            }
                            else $(tr).hide();
                        }
                    }
                }
            });
            $('#txtDate').change(function () {
                $('#txtName').val('');
                $('#txtCode').val('');

                var listTr = document.getElementsByClassName('item');
                for (var i = 0; i < listTr.length; i++){
                    var tr = listTr[i];$(tr).show();
                    for (var j = 0; j < tr.childNodes.length; j++) {
                        if ($('#txtDate').val() != '') {
                            if ($(tr.childNodes[j]).text() != '' && ($(tr.childNodes[j]).text() == $('#txtDate').val())) {
                                $(tr).show();
                                j = tr.childNodes.length;
                            }
                            else $(tr).hide();
                        }
                    }
                }
            });

        });

        function showinfo(oject, type) {
            switch (type) {
                case 'table': {
                    $('#tb_report').show();
                    $('.pager').show();
                    $('#chart_report').hide();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');


                    break;
                }
                case 'chart': {
                    $('#tb_report').hide();
                    $('.pager').hide();
                    $('#chart_report').show();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');

                    break;
                }
                default: {
                    break;
                }
            }
        }

        function printDiv(divName) {
            if ($('#tb_report').is(":visible")) {
                $('#' + divName).printThis({
                    pageTitle: '&nbsp;'
                });
            }
        }
    </script>
    <!-- End Content -->
    <script>

    </script>
    <script src="/public/javascript/jquery.printPage.js" type="text/javascript"></script>
    <script src="/public/javascript/printThis.js" type="text/javascript"></script>
    <script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
    <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>
    <!-- Add thư viện pdf -->
    <script src="https://github.com/niklasvh/html2canvas/releases/download/v0.5.0-beta4/html2canvas.min.js"></script>
    <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
@endsection