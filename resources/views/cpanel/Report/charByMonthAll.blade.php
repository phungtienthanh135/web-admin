@extends('cpanel.template.layout')
@section('title', 'Báo cáo doanh thu tháng')
@section('content')
    <style>
        #tb_report{
            margin-top: 50px;
        }
        .xAxis .tickLabel{
            font-weight: bold;
            text-align: left !important;
        }
        .yAxis .tickLabel{
            font-weight: bold;
        }
        input[type="radio"]:checked + label{ font-weight: bold}
        .lds-roller {
            display: inline-block;
            position: relative;
            width: 64px;
            height: 64px;
        }
        .lds-roller div {
            animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            transform-origin: 32px 32px;
        }
        .lds-roller div:after {
            content: " ";
            display: block;
            position: absolute;
            width: 6px;
            height: 6px;
            border-radius: 50%;
            background: #000;
            margin: -3px 0 0 -3px;
        }
        .lds-roller div:nth-child(1) {
            animation-delay: -0.036s;
        }
        .lds-roller div:nth-child(1):after {
            top: 50px;
            left: 50px;
        }
        .lds-roller div:nth-child(2) {
            animation-delay: -0.072s;
        }
        .lds-roller div:nth-child(2):after {
            top: 54px;
            left: 45px;
        }
        .lds-roller div:nth-child(3) {
            animation-delay: -0.108s;
        }
        .lds-roller div:nth-child(3):after {
            top: 57px;
            left: 39px;
        }
        .lds-roller div:nth-child(4) {
            animation-delay: -0.144s;
        }
        .lds-roller div:nth-child(4):after {
            top: 58px;
            left: 32px;
        }
        .lds-roller div:nth-child(5) {
            animation-delay: -0.18s;
        }
        .lds-roller div:nth-child(5):after {
            top: 57px;
            left: 25px;
        }
        .lds-roller div:nth-child(6) {
            animation-delay: -0.216s;
        }
        .lds-roller div:nth-child(6):after {
            top: 54px;
            left: 19px;
        }
        .lds-roller div:nth-child(7) {
            animation-delay: -0.252s;
        }
        .lds-roller div:nth-child(7):after {
            top: 50px;
            left: 14px;
        }
        .lds-roller div:nth-child(8) {
            animation-delay: -0.288s;
        }
        .lds-roller div:nth-child(8):after {
            top: 45px;
            left: 10px;
        }
        @keyframes lds-roller {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

    </style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3 id="headerName">Báo cáo doanh thu Tháng</h3>
                </div>
                {{--<div class="pull-right span4 t_align_r">--}}
                {{--<div class="row-fluid">--}}
                {{--<div class="input-append">--}}
                {{--<button onclick="showinfo(this,'chart')"--}}
                {{--class="btn btn-default btn-flat btn-report"><i--}}
                {{--class="iconic-chart"></i> Biểu đồ--}}
                {{--</button>--}}
                {{--<button onclick="showinfo(this,'table')"--}}
                {{--class="btn-activate btn btn-default btn-flat btn-report"><i--}}
                {{--class="icon-list-alt"></i> Chi tiết--}}
                {{--</button>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row-fluid">--}}
                {{--<div class="input-append">--}}
                {{--<button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i--}}
                {{--class="icon-print"></i> In--}}
                {{--</button>--}}
                {{--<div class="btn-group export-btn">--}}
                {{--<button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất--}}
                {{--file <span class="caret"></span></button>--}}
                {{--<ul class="dropdown-menu">--}}
                {{--<li><a id="excel" data-fileName="bao_cao">Excel</a></li>--}}
                {{--<li><a id="pdf" data-fileName="bao_cao">PDF</a></li>--}}
                {{--</ul>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>


        <div>
            <div class="row-fluid filterDetail">
                <div class="control-box" style="padding: 10px; margin-bottom: 20px;margin-top:0">
                    {{--<div class="row-fluid">--}}
                    {{--<h4>Điều kiện lọc</h4>--}}
                    {{--</div>--}}
                    <input type="radio" id="numberTicket" checked name="chartType" class="radio"><label for="numberTicket" style="display: inline">Số Vé</label> &nbsp; &nbsp;
                    <input type="radio" id="numberMoney" name="chartType" class="radio"><label for="numberMoney" style="display: inline">Doanh thu</label> &nbsp; &nbsp; &nbsp; &nbsp;
                    <select name="" id="txtMonth" style="margin: 0">
                        <option value="1">Tháng 1</option>
                        <option value="2">Tháng 2</option>
                        <option value="3">Tháng 3</option>
                        <option value="4">Tháng 4</option>
                        <option value="5">Tháng 5</option>
                        <option value="6">Tháng 6</option>
                        <option value="7">Tháng 7</option>
                        <option value="8">Tháng 8</option>
                        <option value="9">Tháng 9</option>
                        <option value="10">Tháng 10</option>
                        <option value="11">Tháng 11</option>
                        <option value="12">Tháng 12</option>
                    </select>
                    <select name="" id="txtYear" style="margin: 0">
                    </select>
                    <button class="btn btn-primary" id="SearchReport" onclick="return getReport();" style="height: 29px;margin-bottom: -1px;border-radius: 0;border: none;">Lọc</button>
                </div>
            </div>
            <div class="row-fluid" id="chart_report">
                <div class="widget-body">
                    <div id="chart" style="height: 350px;"></div>
                </div>
                <br>
                <div class="text-center TotalTicket"></div>
            </div>

        </div>
        <div class="clearfix"></div>
        <div class="row-fluid" style="margin-top: 30px">
            <div class="span6">
                <div id="report-source-ticket" style="width: 100%;height: 300px;">

                </div>
                <h3 class="text-center">BIỂU ĐỒ VÉ.</h3>
                <div class="text-center" id="flot-memo"></div>
            </div>
            <div class="span6">
                <div id="report-souce-money" style="width: 100%;height: 300px;">

                </div>
                <h3 class="text-center">BIỂU ĐỒ DOANH THU.</h3>
            </div>
        </div>
    </div>
    {{--<script src="/public/javascript/jquery.printPage.js" type="text/javascript"></script>--}}
    {{--<script src="/public/javascript/printThis.js" type="text/javascript"></script>--}}
    {{--<script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>--}}
    {{--<script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>--}}
    <!-- Charts Helper Demo Script -->
    {{--<script src="/public/javascript/chart/charts.helper.js"></script>--}}
    <!-- Charts Page Demo Script -->
    {{--<script src="/public/javascript/chart/charts.js"></script>--}}
    <!--  Flot (Charts) JS -->
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.selection.js"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.orderBars.js" type="text/javascript"></script>
    <!-- Add thư viện pdf -->
    {{--<script src="https://github.com/niklasvh/html2canvas/releases/download/v0.5.0-beta4/html2canvas.min.js"></script>--}}
    {{--<script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>--}}
    <script>
        $(document).ready(function () {
            dataReport =[];
            setYear();
            getReport();
            $('body').on('change','#txtMonth,#txtYear',function () {
                $('#SearchReport').prop('disabled',false);
            });
            $('input[type=radio][name=chartType]').change(function() {
                setChart(dataReport.ticket,dataReport.money);
            });
        });
        function setYear() {
            month = new Date().getMonth()+1;
            $('#txtMonth').val(month);
            year = new Date().getFullYear();
            html = '';
            if(year>2017){
                for(var i = 2017;i<=year+2;i++){
                    selected = i===year ? "selected":'';
                    html += '<option '+selected+' value="'+i+'">Năm '+i+'</option>';
                }
            }else{
                html = '<option value="2017">Năm 2017</option>';
            }
            $('#txtYear').html(html);
        }

        function getReport() {
            $('#SearchReport').prop('disabled',true);
            $('#chart').html('<div style="text-align: center"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>');
            $('.TotalTicket').html('');
            SendAjaxWithJson({
                dataType: "json",
                type : 'post',
                url: urlDBD("report/revenuaSummary"),
                headers : {
                    'DOBODY6969':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ2IjowLCJkIjp7InVpZCI6IkFETTExMDk3Nzg4NTI0MTQ2MjIiLCJmdWxsTmFtZSI6IkFkbWluIHdlYiIsImF2YXRhciI6Imh0dHBzOi8vc3RvcmFnZS5nb29nbGVhcGlzLmNvbS9kb2JvZHktZ29ub3cuYXBwc3BvdC5jb20vZGVmYXVsdC9pbWdwc2hfZnVsbHNpemUucG5nIn0sImlhdCI6MTQ5MjQ5MjA3NX0.PLipjLQLBZ-vfIWOFw1QAcGLPAXxAjpy4pRTPUozBpw',
                },
                contentType: 'application/json',
                data: {
                    month:$('#txtMonth').val(),
                    year : $('#txtYear').val(),
                },
                success : function(data){
                    if(data.code ===200){
                        dataReport = makeData(data.results.summaryReport);
                        console.log(dataReport);
                        setChart(dataReport.ticket,dataReport.money);
                        buildChartSource(dataReport.sourceTotal.ticket,'#report-source-ticket');
                        buildChartSource(dataReport.sourceTotal.money,'#report-souce-money');
                        $("#report-source-ticket").showMemo();
                    }else{
                        notyMessage("Lỗi khi lấy dữ liệu",'error');
                    }
                    console.log(data);
                },
                error : function(data){
                    $('#chart').html('<div style="text-align: center">Không lấy được dữ liệu</div>');
                    console.log(data);
                }
            });
        }

        function makeData(data) {
            totalTicket = 0;
            totalMoney = 0;
            dataRp = {
                ticket : [],
                money : [],
                sourceTotal : {
                    ticket : {admin:0,web:0,ios : 0,android : 0},
                    money : {admin:0,web:0,ios : 0,android : 0}
                },
                source :[]
            };
            $.each(data,function (k,v) {
                year = v.date.toString().substring(0, 4);
                month = parseInt(v.date.toString().substring(4, 6))-1;
                day = v.date.toString().substring(6, 8);
                miliseconds = gd(year,month,day)
                dataRp.ticket.push([miliseconds,v.totalSeat]);
                dataRp.money.push([gd(year,month,day),v.totalMoney]);
                dataRp.source[miliseconds]=v.sourceTicket;
                totalTicket +=v.totalSeat;
                totalMoney += v.totalMoney;
                if(typeof v.sourceTicket!=='undefined'){
                    dataRp.sourceTotal.ticket.admin += v.sourceTicket[1].totalSeat;
                    dataRp.sourceTotal.ticket.web += v.sourceTicket[2].totalSeat;
                    dataRp.sourceTotal.ticket.ios += v.sourceTicket[3].totalSeat;
                    dataRp.sourceTotal.ticket.android += v.sourceTicket[4].totalSeat;

                    dataRp.sourceTotal.money.admin += v.sourceTicket[1].totalMoney;
                    dataRp.sourceTotal.money.web += v.sourceTicket[2].totalMoney;
                    dataRp.sourceTotal.money.ios += v.sourceTicket[3].totalMoney;
                    dataRp.sourceTotal.money.android += v.sourceTicket[4].totalMoney;
                }
            });
            $('.TotalTicket').html('<span style="padding:10px;border:2px solid #084388;font-size: 20px;color:#fff;background: #4f6c8d">Tổng số vé : <strong>'+$.number(totalTicket)+" (Vé)</strong> . Tổng số tiền : <strong>" + moneyFormat(totalMoney)+'</strong></span>');
            return dataRp;
        }

        function setChart(dataNumberTicket,dataTotalMoney) {
            // var dataset = [
            //     { label: "Số Vé", data: dataNumberTicket},
            //     { label: "Số Tiền", data: dataTotalMoney, yaxis: 2},
            // ];
            var options = {
                series: {
                    bars: {
                        show: true,
                        barWidth : 36000000,
                        align: "center",
                        order:1
                    },
                },
                xaxis: {
                    mode: "time",
                    tickSize: [1, "day"],
                    tickLength: 0,
                    align:'center',
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 10,
                },
                yaxes: [{
                    axisLabel: "Số vé",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 3,
//                    tickFormatter: function (v, axis) {
//                        return v+ 'Vé';
//                    }
                },
                    // {
                    //     position: "right",
                    //     axisLabel: "Change(%)",
                    //     axisLabelUseCanvas: true,
                    //     axisLabelFontSizePixels: 12,
                    //     axisLabelFontFamily: 'Verdana, Arial',
                    //     axisLabelPadding: 3
                    // }
                ],
                legend: {
                    noColumns: 0,
                    labelBoxBorderColor: "#000000",
                    position: "nw"
                },
                grid: {
                    hoverable: true,
                    borderWidth: 2,
                    borderColor: "#633200",
                    backgroundColor: { colors: ["#ffffff", "#EDF5FF"] }
                },
                colors: ["#084388", "red"],
            };
            var dataset = [];
            if($('#numberTicket').is(':checked')){
                dataset = [
                    { label: "Số Vé", data: dataNumberTicket},
                ];
            }else{
                dataset = [
                    { label: "Số Tiền", data: dataTotalMoney},
                ];
            }
            $.plot($("#chart"), dataset, options);
            $("#chart").UseTooltip();

        }

        function buildChartSource(dataIn,element) {
            var dataCircleChart =  [
                {label: "Admin : "+ $.number(dataIn.admin), data: dataIn.admin, color: "#005CDE" },
                { label: "Web : "+ $.number(dataIn.web), data: dataIn.web, color: "#00A36A" },
                { label: "IOS : "+ $.number(dataIn.ios), data: dataIn.ios, color: "#7D0096" },
                { label: "Android : "+ $.number(dataIn.android), data: dataIn.android, color: "#992B00" },
            ];
            var options = {
                series: {
                    pie: {
                        show: true,
                        // innerRadius: 0.5,
                        // label: {
                        //     show: true
                        // }
                    }
                }
            };
            $.plot($(element),dataCircleChart,options);
        }

        function gd(year, month, day) {
            return new Date(year, month, day).getTime();
        }

        var previousPoint = null, previousLabel = null;
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        $.fn.UseTooltip = function () {
            $(this).bind("plothover", function (event, pos, item) {
                if (item) {
                    if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                        previousPoint = item.dataIndex;
                        previousLabel = item.series.label;
                        $("#tooltip").remove();

                        var x = item.datapoint[0];
                        var y = item.datapoint[1];

                        var color = item.series.color;
                        var month = new Date(x).getMonth();

                        if (item.seriesIndex == 0) {
                            if($('#numberTicket').is(':checked')) {
                                showTooltip(item.pageX, item.pageY, color, "<strong>" + item.series.label + "</strong> : <strong>" + y + "</strong>(Vé).<br>Admin : " + dataReport.source[x][1].totalSeat + "<br>Web : " + dataReport.source[x][2].totalSeat + "<br>IOS : " + dataReport.source[x][3].totalSeat + "<br>Android : " + dataReport.source[x][4].totalSeat);
                            }else{
                                showTooltip(item.pageX, item.pageY, color, "<strong>" + item.series.label + "</strong> : <strong>" + $.number(y) + "</strong></strong>(VND). <br>Admin : "+$.number(dataReport.source[x][1].totalMoney) + "<br>Web : "+$.number(dataReport.source[x][2].totalMoney) + "<br>IOS : "+$.number(dataReport.source[x][3].totalMoney) + "<br>Android : "+$.number(dataReport.source[x][4].totalMoney));
                            }
                        } else {
                            showTooltip(item.pageX, item.pageY, color, "<strong>" + item.series.label + "</strong> : <strong>" + $.number(y) + "</strong></strong>(VND). <br>Admin : "+dataReport.source[x][1].totalMoney + "<br>Web : "+dataReport.source[x][2].totalMoney + "<br>IOS : "+dataReport.source[x][3].totalMoney + "<br>Android : "+dataReport.source[x][4].totalMoney);
                        }
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            });
        };

        function showTooltip(x, y, color, contents) {
            $('<div id="tooltip">' + contents + '</div>').css({
                position: 'absolute',
                display: 'none',
                top: y - 40,
                left: x - 120,
                border: '2px solid ' + color,
                padding: '3px',
                'font-size': '9px',
                'border-radius': '5px',
                'background-color': '#fff',
                'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                opacity: 0.9
            }).appendTo("body").fadeIn(200);
        }
        $.fn.showMemo = function () {
            $(this).bind("plothover", function (event, pos, item) {
                if (!item) { return; }
                console.log(item.series.data);
                var html = [];
                var percent = parseFloat(item.series.percent).toFixed(2);

                html.push('<div style="border:1px solid grey;background-color:"',
                    item.series.color,
                    "\">",
                    "<span style=\"color:white\">",
                    item.series.label,
                    " : ",
                    $.formatNumber(item.series.data[0][1], { format: "#,###", locale: "us" }),
                    " (", percent, "%)",
                    "</span>",
                    "</div>");
                $("#flot-memo").html(html.join(''));
            });
        }
    </script>
@endsection