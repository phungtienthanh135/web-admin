@extends('cpanel.template.layout')
@section('title', 'Báo cáo công nợ với An Vui')
@section('content')
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Báo cáo tổng hợp thu chi</h3>
                </div>


                <div class="pull-right span4 t_align_r">
                    <div class="row-fluid">
                        <div class="input-append">
                            {{--<button onclick="showinfo(this,'chart')" class=" btn btn-default btn-flat btn-report"><i--}}
                            {{--class="iconic-chart"></i> Biểu đồ--}}
                            {{--</button>--}}
                            <button onclick="showinfo(this,'table')"
                                    class="btn-activate btn btn-default btn-flat btn-report"><i
                                        class="icon-list-alt"></i> Chi tiết
                            </button>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i
                                        class="icon-print"></i> In
                            </button>

                            <div class="btn-group export-btn">
                                <button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất
                                    file <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="excel" data-fileName="bao_cao_cong_no">Excel</a></li>
                                    <li><a id="pdf">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <div >
            <div class="row-fluid">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Điều kiện lọc</h4>
                    </div>
                    <form action="" method="get">

                        <div class="row-fluid">
                            <div class="span3">
                                <label for="txtStartDate">Từ ngày</label>
                                <div class="input-append">
                                    <input value="{{request('startDate',date('d-m-Y'))}}"
                                           id="txtStartDate" name="startDate" type="text" readonly>
                                </div>
                            </div>

                            <div class="span3">
                                <label for="txtEndDate">Đến ngày</label>
                                <div class="input-append">
                                    <input value="{{request('endDate',date('d-m-Y'))}}" id="txtEndDate" name="endDate"
                                           type="text" readonly>
                                </div>
                            </div>

                            <div class="span3">
                                <label for="itemId">Khoản mục</label>
                                <div class="">
                                    <select name="itemId" id="itemId">
                                        <option value>Tất cả</option>
                                        @foreach($getItem as $item)
                                            <option {{request('itemId') == $item['itemId'] ? 'selected' : ''}} value="{{$item['itemId']}}">{{$item['itemName']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="span3">
                                <label class="separator hidden-phone" for="add"></label>
                                <button class="btn btn-info btn-flat-full hidden-phone" id="search">LỌC</button>
                                <button class="btn btn-info btn-flat visible-phone" id="search">LỌC</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <div class="row-fluid" id="tb_report">
                @php
                    $countPayment = 0;
                    $count = 1;
                    $totalReceipt = 0;
                    $totalPayment = 0;
                    foreach($result as $value) {
                        $totalReceipt += $value['totalReceipt'];
                        $totalPayment += $value['totalPayment'];
                    }
                @endphp
                <table class="table table-striped">
                    <thead>
                        <tr class="total">
                            <td>TỔNG</td>
                            <td class="center">@moneyFormat($totalReceipt)</td>
                            <td class="center">@moneyFormat($totalPayment)</td>
                        </tr>
                        <tr>
                            <th class="center">Ngày</th>
                            <th class="center">Tổng thu</th>
                            <th class="center">Tổng chi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($result as $key=>$value)
                            <tr>
                                <td class="center">{{date('d/m/Y', strtotime($key))}}</td>
                                <td class="center">@moneyFormat($value['totalReceipt'])</td>
                                <td class="center">@moneyFormat($value['totalPayment'])</td>
                            </tr>
                            <tr class="info" id="chitiet_{{$key}}" style="display: none">
                                <td colspan="3">
                                    <table class="tttk">
                                        @php
                                            $totalAmountSpentIn = 0; //tong thu
                                            $totalAmountSpentOut = 0; //tong chi
                                            foreach($value['listReceiptAndPayment'] as $val)
                                            {
                                                if($val['type'] == 1)
                                                {
                                                    $totalAmountSpentIn += $val['amountSpent'];
                                                }
                                                else {
                                                    $totalAmountSpentOut += $val['amountSpent'];
                                                }
                                            }
                                        @endphp
                                        <thead>
                                            <tr class="total">
                                                <td colspan="5">TỔNG</td>
                                                <td class="center">@moneyFormat($totalAmountSpentIn)</td>
                                                <td class="center">@moneyFormat($totalAmountSpentOut)</td>
                                            </tr>
                                            <tr>
                                                <th class="center">STT</th>
                                                <th class="center">Ngày</th>
                                                <th class="center">Số phiếu</th>
                                                <th class="center">Người lập</th>
                                                <th class="center">Lý do</th>
                                                <th class="center">Thu</th>
                                                <th class="center">Chi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($value['listReceiptAndPayment']))
                                            @foreach($value['listReceiptAndPayment'] as $item)
                                                <tr>
                                                    <td class="center">{{$count++}}</td>
                                                    <td class="center">@dateFormat($item['date'])</td>
                                                    <td class="center">{{$item['numberOfDocuments']}}</td>
                                                    <td class="center">{{@$item['userName']}}</td>
                                                    <td class="center">{{$item['reason']}}</td>
                                                    <td class="center">
                                                        @if($item['type'] == 1)
                                                            @moneyFormat($item['amountSpent'])
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td class="center">
                                                        @if($item['type'] == 2)
                                                            @moneyFormat($item['amountSpent'])
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <tr class="total">
                                                <td colspan="5">TỔNG</td>
                                                <td class="center">@moneyFormat($totalAmountSpentIn)</td>
                                                <td class="center">@moneyFormat($totalAmountSpentOut)</td>
                                            </tr>
                                        @else
                                            <tr>
                                                <td class="center" colspan="7">Hiện không có dữ liệu</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            @php $countPayment++; @endphp
                        @endforeach
                        <tr class="total">
                            <td>TỔNG</td>
                            <td class="center">@moneyFormat($totalReceipt)</td>
                            <td class="center">@moneyFormat($totalPayment)</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    @include('cpanel.Print.print')
    <script type="text/javascript">

    </script>
    <!-- Table Export To File -->
    <script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
    <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>
    <link rel="stylesheet" href="/public/javascript/wickedpicker/wickedpicker.css">
    <script type="text/javascript" src="/public/javascript/wickedpicker/wickedpicker.js"></script>
    <script>

        $(function () {

            $('#cbb_listDriverId,#cbb_listAssistantId').select2();
            $('.btnUpdate').click(function () {
                var scheduleId = $(this).parents('tr').data('scheduleid'),
                    dateRun = $(this).parents('tr').data('starttime');
                $('#txtScheduleId').val(scheduleId);
                $('#txtDateRun').val(dateRun);
            });

            $('.btnUpdateTrip').click(function () {
                var scheduleId = $(this).parents('tr').data('scheduleid'),
                    day = $(this).parents('tr').data('day'),
                    dateRun = $(this).parents('tr').data('starttime'),
                    time = $(this).parents('tr').data('starttimeformat');
                //Cau hinh wickedpicker
                var options = {
                    now: time,
                    twentyFour: true,
                    upArrow: 'wickedpicker__controls__control-up',
                    downArrow: 'wickedpicker__controls__control-down',
                    close: 'wickedpicker__close',
                    hoverState: 'hover-state',
                    title: 'Chọn thời gian xuất bến',
                    clearable: false
                };

                $('#txt_newDate').wickedpicker(options);
                $('#scheduleId').val(scheduleId);
                $('#dateRun').val(dateRun);
                $('#day').val(day);
            });

        });

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;

        }
    </script>
    <!-- Add thư viện pdf -->
    <script src="https://github.com/niklasvh/html2canvas/releases/download/v0.5.0-beta4/html2canvas.min.js"></script>
    <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
    <!--  Flot (Charts) JS -->
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.selection.js"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.orderBars.js" type="text/javascript"></script>
    <!-- End Content -->

@endsection