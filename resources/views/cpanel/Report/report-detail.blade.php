@extends('cpanel.template.layout')
@section('title', 'Báo cáo doanh thu chi tiết theo ngày')
@section('content')
    <style>
        #tb_report {
            margin-top: 50px;
            font-size: 11.5px;
        }
      .select2-container--open,.select2-selection__rendered,.select2-container--default,select{
          width: 100% !important;
      }
        table td {
            max-width: 120px;
        }

        .lds-roller {
            display: inline-block;
            position: relative;
            width: 64px;
            height: 64px;
        }

        .lds-roller div {
            animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            transform-origin: 32px 32px;
        }

        .lds-roller div:after {
            content: " ";
            display: block;
            position: absolute;
            width: 6px;
            height: 6px;
            border-radius: 50%;
            background: #000;
            margin: -3px 0 0 -3px;
        }

        .lds-roller div:nth-child(1) {
            animation-delay: -0.036s;
        }

        .lds-roller div:nth-child(1):after {
            top: 50px;
            left: 50px;
        }

        .lds-roller div:nth-child(2) {
            animation-delay: -0.072s;
        }

        .lds-roller div:nth-child(2):after {
            top: 54px;
            left: 45px;
        }

        .lds-roller div:nth-child(3) {
            animation-delay: -0.108s;
        }

        .lds-roller div:nth-child(3):after {
            top: 57px;
            left: 39px;
        }

        .lds-roller div:nth-child(4) {
            animation-delay: -0.144s;
        }

        .lds-roller div:nth-child(4):after {
            top: 58px;
            left: 32px;
        }

        .lds-roller div:nth-child(5) {
            animation-delay: -0.18s;
        }

        .lds-roller div:nth-child(5):after {
            top: 57px;
            left: 25px;
        }

        .lds-roller div:nth-child(6) {
            animation-delay: -0.216s;
        }

        .lds-roller div:nth-child(6):after {
            top: 54px;
            left: 19px;
        }

        .lds-roller div:nth-child(7) {
            animation-delay: -0.252s;
        }

        .lds-roller div:nth-child(7):after {
            top: 50px;
            left: 14px;
        }

        .lds-roller div:nth-child(8) {
            animation-delay: -0.288s;
        }

        .lds-roller div:nth-child(8):after {
            top: 45px;
            left: 10px;
        }

        @keyframes lds-roller {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

    </style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3 id="headerName">Báo cáo doanh thu chi tiết</h3>
                </div>
                <div class="pull-right span4 t_align_r">
                    {{--<div class="row-fluid">--}}
                    {{--<div class="input-append">--}}
                    {{--<button onclick="showinfo(this,'chart')"--}}
                    {{--class="btn btn-default btn-flat btn-report"><i--}}
                    {{--class="iconic-chart"></i> Biểu đồ--}}
                    {{--</button>--}}
                    {{--<button onclick="showinfo(this,'table')"--}}
                    {{--class="btn-activate btn btn-default btn-flat btn-report"><i--}}
                    {{--class="icon-list-alt"></i> Chi tiết--}}
                    {{--</button>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i
                                        class="icon-print"></i> In
                            </button>
                            <div class="btn-group export-btn">
                                <button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất
                                    file <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="excel" data-fileName="bao_cao">Excel</a></li>
                                    <li><a id="pdf" data-fileName="bao_cao">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div>
            <div class="row-fluid filterDetail">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Điều kiện lọc</h4>
                    </div>
                    <form action="">
                        @if(empty(session('userLogin')['userInfo']['listAgency']))
                            <div class="row-fluid">
                                <div class="span2">
                                    <label for="dateType">Thời gian</label>
                                    <select name="sourceId" id="dateType">
                                        <option value="2">Ngày Đi</option>
                                        <option value="1">Ngày Tạo</option>
                                    </select>
                                </div>
                                {{--<div class="span2">--}}
                                    {{--<label for="paymentType">Kiểu thanh toán</label>--}}
                                    {{--<select name="" id="paymentType">--}}
                                        {{--<option value="1">Tại quầy</option>--}}
                                        {{--<option value="2">Online</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                                {{--<div class="span2">--}}
                                    {{--<label for="ticketStatus">Trạng thái vé</label>--}}
                                    {{--<select name="" id="ticketStatus">--}}
                                        {{--<option value="0">Đã hủy</option>--}}
                                        {{--<option value="1">Giữ chỗ</option>--}}
                                        {{--<option value="7">Thanh toán</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            </div>
                            <div class="row-fluid">
                                <div class="span2">
                                    <label for="cbb_NguonThu">Nguồn thu</label>
                                    <select name="sourceId" id="cbb_NguonThu">
                                    </select>
                                    <input type="hidden" id="tenNguonThu" name="tenNguonThu">
                                </div>
                                <div class="span2">
                                    <label for="cbb_Xe">Biển xe</label>
                                    <select name="numberPlate" id="cbb_Xe">
                                        <option value="">Tất cả</option>
                                        @foreach($listVehicle as $Vehicle)
                                            <option {{request('numberPlate')==$Vehicle['numberPlate']?'selected':''}}  value="{{$Vehicle['numberPlate']}}">{{$Vehicle['numberPlate']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if(hasAnyRole(GET_LIST_EMPLOYEE))
                                    <div class="span2">

                                        <label for="driverId">Lái xe</label>
                                        <select name="driverId" id="driverId" class="userList">
                                            <option value="">Tất cả</option>
                                            @foreach($listDriver as $list)
                                                <option {{request('driverId')==$list['userId']?'selected':''}} value="{{$list['userId']}}">{{$list['fullName']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
                                @if(hasAnyRole(GET_LIST_EMPLOYEE))
                                    <div class="span2">

                                        <label for="assistantId">Phụ xe</label>
                                        <select name="assistantId" id="assistantId" class="userList">
                                            <option value="">Tất cả</option>
                                            @foreach($listAssistant as $list)
                                                <option {{request('assistantId')==$list['userId']?'selected':''}} value="{{$list['userId']}}">{{$list['fullName']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
                                <div class="span2">
                                    <label for="cbb_Tuyen">Tuyến</label>
                                    <select name="routeId" id="cbb_Tuyen">
                                        <option value="">Tất cả</option>
                                        @foreach($listRoute as $route)
                                            <option {{request('routeId')==$route['routeId']?'selected':''}} value="{{$route['routeId']}}">{{$route['routeName']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="span2">
                                    <label for="sdtNguoiMua">SDT Khách Hàng</label>
                                    <input type="text" id="customerPhone" style="width:-webkit-fill-available" class="form-control">
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span2">
                                    <label for="departmentId">Phòng ban</label>
                                    <select name="departmentId" id="departmentId">
                                        <option value="">Tất cả</option>
                                        @foreach($listDepartment as $department)
                                            <option {{request('departmentId')==$department['departmentId']?'selected':''}} value="{{$department['departmentId']}}">{{$department['departmentName']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="span2">
                                    <label for="cashierUserId">Người thu</label>
                                    <select name="cashierUserId" id="cashierUserId">
                                        <option value="">Tất cả</option>
                                        @foreach($cashierUser as $cashier)
                                            <option {{request('cashierUserId')==$cashier['userId']?'selected':''}} value="{{$cashier['userId']}}">{{$cashier['fullName']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if(hasAnyRole(GET_LIST_EMPLOYEE))
                                    <div class="span2">
                                        <label for="creatorId">Người bán</label>
                                        <select name="creatorId" id="creatorId" class="userList">
                                            <option value="">Tất cả</option>
                                            <option value="ADM1109778852414622">Admin Web</option>
                                            {{--@foreach($listAllUser as $user)--}}
                                            {{--<option {{request('creatorId')==$user['userId']?'selected':''}} value="{{$user['userId']}}">{{$user['fullName']}}</option>--}}
                                            {{--@endforeach--}}
                                        </select>
                                    </div>
                                @endif
                                <div class="span2">
                                    <label for="txtStartDate">Từ ngày</label>
                                    <div class="input-append">
                                        <input class="span11" autocomplete="off"
                                               value="{{request('startDate',date('d-m-Y',time()))}}"
                                               id="txtStartDate" name="startDate" type="text" readonly>
                                    </div>
                                </div>

                                <div class="span2">
                                    <label for="txtEndDate">Đến ngày</label>
                                    <div class="input-append">
                                        <input class="span11" autocomplete="off"
                                               value="{{request('endDate',date('d-m-Y'))}}"
                                               id="txtEndDate" name="endDate" type="text" readonly>
                                    </div>
                                </div>
                                <div class="span2 pull-right">
                                    <label class="separator hidden-phone" for="add"></label>
                                    <button class="btn btn-info btn-flat-full hidden-phone searchReport">LỌC</button>
                                    <button class="btn btn-info btn-flat visible-phone searchReport">LỌC</button>
                                </div>
                            </div>
                        @else
                            <div class="row-fluid">
                                <div class="span4">
                                    <label for="txtStartDate">Từ ngày</label>
                                    <div class="input-append">
                                        <input autocomplete="off" value="{{request('startDate',date('d-m-Y',time()))}}"
                                               id="txtStartDate" name="startDate" type="text" readonly>
                                    </div>
                                </div>

                                <div class="span4">
                                    <label for="txtEndDate">Đến ngày</label>
                                    <div class="input-append">
                                        <input autocomplete="off" value="{{request('endDate',date('d-m-Y'))}}"
                                               id="txtEndDate" name="endDate" type="text" readonly>
                                    </div>
                                </div>

                                <div class="span4">
                                    <div class="span3" id="startSearch">
                                        <label class="separator hidden-phone" for="add"></label>
                                        <button class="btn btn-info btn-flat-full hidden-phone searchReport">LỌC
                                        </button>
                                        <button class="btn btn-info btn-flat visible-phone searchReport">LỌC</button>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </form>

                </div>
            </div>
            <div class="row-fluid bg_light" id="tb_report">
                <h4 style="text-align: center">BÁO CÁO DOANH THU</h4>
                <div class="infoSearch"></div>
                <table class="tb_report table table-striped">
                    <thead>
                    <tr class="total">
                        <th colspan="9">TỔNG DOANH THU</th>
                        <td class="center soLuongVeTotal"></td>
                        <td class="center thuThemTotal"></td>
                        <td class="thanhTienTotal"></td>
                        <td class="daThuTotal"></td>
                        <td class="conLaiTotal"></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th class="center">Mã</th>
                        <th class="center">KH</th>
                        <th class="center">Mã code</th>
                        <th class="center">SĐT</th>
                        <th class="center">Tuyến</th>
                        <th class="center">Điểm khởi hành</th>
                        <th class="center">TG đi</th>
                        <th class="center">Người bán</th>
                        <th class="center">Nguồn vé</th>
                        <th class="center">SL</th>
                        <th class="center">Thu thêm</th>
                        <th class="center">Thành tiền (VNĐ)</th>
                        <th class="center">Đã thu (VNĐ)</th>
                        <th class="center">Còn lại (VNĐ)</th>
                        <th class="center">Người thu</th>
                        <th class="center">Ghi chú</th>
                    </tr>
                    {{--@php--}}
                    {{--$totalTicket=0;--}}
                    {{--$totalPrice=0;--}}
                    {{--$totalPaidMoney=0;--}}
                    {{--$totalUnPaidMoney=0;--}}

                    {{--$listMoney = [--}}
                    {{--'price',--}}
                    {{--'paidMoney',--}}
                    {{--'unPaidMoney'--}}
                    {{--];--}}

                    {{--$dateFormat = [--}}
                    {{--'time'--}}
                    {{--];--}}

                    {{--foreach($result as $row) :--}}
                    {{--//$row['intoMoney']=$row['number']!=0?$row['number']*$row['price']:$row['price'];--}}
                    {{--$totalTicket += (float)$row['number'];--}}
                    {{--$totalPrice += (float)($row['price']);--}}
                    {{--$totalPaidMoney += (float)($row['paidMoney']);--}}
                    {{--$totalUnPaidMoney += (float)($row['unPaidMoney']);--}}
                    {{--endforeach;--}}
                    {{--@endphp--}}
                    {{--<tr>--}}
                    {{--<th>Mã vé</th>
                    <th>Chuyến</th>
                    <th>Xe</th>
                    <th>Khách hàng</th>
                    <th>Số điện thoại</th>
                    <th>Ngày</th>
                    <th>Nguồn</th>

                    <th class="center">SL Khách hàng</th>
                    <th>Giá</th>
                    <th>Thành tiền</th>
                    <th>Tiền mặt</th>--}}
                    {{--@foreach(REPORT_ROW as $item => $value)--}}
                    {{--<th class="center">{{$value}}</th>--}}
                    {{--@endforeach--}}
                    {{--</tr>--}}
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                    <tr class="total">
                        <th colspan="9">TỔNG DOANH THU</th>
                        <td class="center soLuongVeTotal"></td>
                        <td class="center thuThemTotal"></td>
                        <td class="thanhTienTotal"></td>
                        <td class="daThuTotal"></td>
                        <td class="conLaiTotal"></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div class="row-fluid" id="chart_report">
                <div class="widget-body">
                    <div id="chart_by_date" style="height: 250px;"></div>
                </div>
            </div>
        </div>

        @include('cpanel.Print.print')
    </div>

    <script type="text/javascript">

        var dataReport = [];

        setTimeout(function () {
            $('#chart_report').hide();
        }, 1000);

        // $('#startSearch,.hidden-phone,.visible-phone').click(function () {
        //     var text = $('#cbb_NguonThu option:selected').text();
        //     $('#tenNguonThu').val(text);
        // });

        $(document).ready(function () {
            $("#content").attr("style", "width:100%");
            $("#menu").attr("style", "margin-left:-400px");
            $("#ht_btn_close_open_menu").html('<button onClick="open_menu()" type="button" class="btn-navbar"> <div class="icon_open_menu"></div> </button>');
            $('.searchReport').click(function () {
                getDataReport();
                return false
            });

            $('.userList').select2();
            $('#cashierUserId').select2();
            $('#driverId').select2({
                dropdownCss: {'z-index': '999999'},
//                containerCss: {'width': '273px'},
                minimumInputLength: 0,
                placeholder: "Chọn Tài xế",
                ajax: {
                    url: urlDBD("user/getlist"),
                    contentType : "application/json; charset=utf-8",
                    type: "post",
                    headers: {
                        'DOBODY6969': '{{ session('userLogin')['token']['tokenKey'] }}',
                    },
                    delay: 1000,
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            fullName: params.term,
                            page: 0,
                            count: 20,
                            listUserType: [2],
                        };
                        return JSON.stringify(query);
                    },
                    processResults: function (data) {
                        var r = [];
                        r.push({text: "Tất cả", id: "-1"});
                        if (data.code === 200) {
                            $.map(data.results.result, function (item) {
                                if (data.results.result.length > 0) {
                                    r.push({text: item.fullName, id: item.userId})
                                }
                            });
                        }
                        return {
                            results: r
                        };
                    },
                    cache: true,
                }
            });

            $('#assistantId').select2({
                dropdownCss: {'z-index': '999999'},
//                containerCss: {'width': '273px'},
                minimumInputLength: 0,
                placeholder: "Chọn Phụ xe",
                ajax: {
                    url: urlDBD("user/getlist"),
                    type: "post",
                    headers: {
                        'DOBODY6969': '{{ session('userLogin')['token']['tokenKey'] }}',
                    },
                    delay: 1000,
                    contentType : "application/json; charset=utf-8",
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            fullName: params.term,
                            page: 0,
                            count: 20,
                            userType: [3],
                        };
                        return JSON.stringify(query);
                    },
                    processResults: function (data) {
                        var r = [];
                        r.push({text: "Tất cả", id: "-1"});
                        if (data.code === 200) {
                            $.map(data.results.result, function (item) {
                                if (data.results.result.length > 0) {
                                    r.push({text: item.fullName, id: item.userId})
                                }
                            });
                        }
                        return {
                            results: r
                        };
                    },
                    cache: true,
                }
            });

            $('#creatorId').select2({
                dropdownCss: {'z-index': '999999'},
//                containerCss: {'width': '273px'},
                minimumInputLength: 0,
                placeholder: "Chọn người bán",
                ajax: {
                    url: urlDBD("user/getlist"),
                    contentType : "application/json; charset=utf-8",
                    type: "post",
                    headers: {
                        'DOBODY6969': '{{ session('userLogin')['token']['tokenKey'] }}',
                    },
                    delay: 1000,
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            fullName: params.term,
                            page: 0,
                            count: 20,
                            listUserType: [1,2,3,5],
                        };
                        return JSON.stringify(query);
                    },
                    processResults: function (data) {
                        var r = [];
                        r.push({text: "Tất cả", id: "-1"});
                        r.push({text: "Admin web", id: "ADM1109778852414622"});
                        if (data.code === 200) {
                            $.map(data.results.result, function (item) {
                                if (data.results.result.length > 0) {
                                    r.push({text: item.fullName, id: item.userId})
                                }
                            });
                        }
                        return {
                            results: r
                        };
                    },
                    cache: true,
                }
            });

            $('#cbb_NguonThu').select2({
                dropdownCss: {'z-index': '999999'},
//                containerCss: {'width': '273px'},
                multiple : true,
                minimumInputLength: 0,
                placeholder: "Chọn đại lý",
                ajax: {
                    url: "{{action('AgencyController@getListAgency')}}",
                    delay: 1000,
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            keyword: params.term
                        };
                        return query;
                    },
                    processResults: function (data) {
                        var r = [];
                        r.push({text: "An vui", id: "anvui"});
                        r.push({text: "Nhà xe", id: "{{session('companyId')}}"});
                        $.map(data, function (item) {
                            if (data.length > 0) {
                                r.push({text: item.fullName, id: item.userId})
                            }
                        });
                        return {
                            results: r
                        };
                    },
                    cache: true,
                }
            });
        });


        function getDataReport() {
            $('#tb_report').find('tbody').html('<tr><td colspan="15" style="text-align: center"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td></tr>')
            companyId = '{{session('companyId')}}';

            startDate = $('#txtStartDate').val();
            startDate = startDate.split("-");
            startDate = startDate[1] + "/" + startDate[0] + "/" + startDate[2];
            startDate = new Date(startDate).getTime() + 1;

            endDate = $('#txtEndDate').val();
            endDate = endDate.split("-");
            endDate = endDate[1] + "/" + endDate[0] + "/" + endDate[2];
            endDate = new Date(endDate).getTime() + 86400000;//23h 59p 59s

            numberPlate = $('#cbb_Xe').val() || '';
            driverId = $('#driverId').val() || '';
            assistantId = $('#assistantId').val() || '';
            departmentId = $('#departmentId').val() || '';
            routeId = $('#cbb_Tuyen').val() || '';
            sourceId = ($('#cbb_NguonThu').val() || '').toString();
            creatorId = $('#creatorId').val() || '';
            cashierUserId=$('#cashierUserId').val()||'';
            customerPhone=$('#customerPhone').val()||'';
            dateType=$('#dateType').val()||2;
            setHeaderInfoSearch();

            dataRequest = removeItemNullInArray({
                "timeZone": 7,
                "companyId": companyId,
                "startDate": startDate,
                "endDate": endDate,
                "numberPlate": numberPlate,
                "driverId": driverId,
                "assistantId": assistantId,
                "departmentId": departmentId,
                "routeId": routeId,
                "sourceId": sourceId,
                "creatorId": creatorId,
                "cashierUserId":cashierUserId,
                "phoneNumber":customerPhone,
                "dateType" : dateType,
            });
            console.log(JSON.stringify(dataRequest));

            $.ajax({
                dataType: "json",
                type: 'post',
                url: 'http://103.56.158.205:8668/report/create-report-detail',//urlDBD("report/create-report-detail")
                headers: {
                    'DOBODY6969': '{{ session('userLogin')['token']['tokenKey'] }}',
                },
                contentType: 'application/json',
                data: JSON.stringify(dataRequest),
                success: function (data) {
                    if (data.code === 200) {
                        dataReport = data.results.report;
                        setReport();
                    } else {
                        notyMessage("Lỗi khi lấy dữ liệu", 'error');
                    }
                    console.log(data);
                },
                error: function (data) {
                    notyMessage("Lỗi khi gửi yêu cầu", 'error');
                    console.log(data);
                }
            });
        }

        function setReport() {
            console.log(0, charts_data);
            count = {slve: 0, extra: 0, paid: 0, unpaid: 0, totalPrice: 0};
            data_chart_new = [];
            html = '';
            $.each(dataReport, function (k, v) {
                html += '<tr>';
                html += '<td>'+v.ticketCode+'</td>';
                html += '<td>'+v.nameGuest+'</td>';
                html += '<td>'+v.foreignKey+'</td>';
                html += '<td>'+v.phoneNumber+'</td>';
                html += '<td>'+v.routeName+'</td>';
                html += '<td>'+v.inPoint+'</td>';
                html += '<td>'+new Date(v.time).customFormat('#hhhh#:#mm# #DD#-#MM#-#YYYY#')+'</td>';
                html += '<td>'+v.sellerName+'</td>';
                html += '<td>'+v.sourceName+'</td>';
                html += '<td class="right">'+v.number+'</td>';
                html += '<td class="right">'+v.extraPrice+'</td>';
                html += '<td class="right">'+parseFloat(v.price).format()+'</td>';
                html += '<td class="right">'+parseFloat(v.paidMoney).format()+'</td>';
                html += '<td class="right">'+parseFloat(v.unPaidMoney).format()+'</td>';
                html += '<td>'+v.cashier+'</td>';
                html += '<td>'+v.note+'</td>';
                html += '</tr>';
                count.slve += v.number;
                count.extra += v.extraPrice;
                count.paid += v.paidMoney;
                count.unpaid += v.unPaidMoney;
                count.totalPrice += v.price;
                data_chart_new.push([v.time, v.price]);
            });
            $('.soLuongVeTotal').html(count.slve);
            $('.thuThemTotal').html(moneyFormat(count.extra));
            $('.thanhTienTotal').html(moneyFormat(count.totalPrice));
            $('.conLaiTotal').html(moneyFormat(count.unpaid));
            $('.daThuTotal').html(moneyFormat(count.paid));
            console.log('1', charts_data);
            charts_data.data.d2 = data_chart_new;
            if (typeof charts != 'undefined')
                charts.initCharts();

            console.log(2, charts_data);
            $('#tb_report').find('tbody').html(html);
        }

        function showinfo(oject, type) {
            switch (type) {
                case 'table': {
                    $('#tb_report').show();
                    $('#chart_report').hide();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');
                    $('.filterDetail').show();
                    $('#headerName').text('Báo cáo doanh thu chi tiết');


                    break;
                }
                case 'chart': {
                    $('#tb_report').hide();
                    $('.filterDetail').hide();
                    $('#chart_report').show();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');
                    $('#headerName').text('Biểu đồ doanh thu chi tiết');
                    break;
                }
                default: {
                    break;
                }
            }
        }

        function printDiv(divName) {
            if ($('#tb_report').is(":visible")) {
                $('#' + divName).printThis({
                    pageTitle: '&nbsp;'
                });
            }
        }

        function removeItemNullInArray(arr) {
            newArr = {};
            if (typeof arr === 'object') {
                $.each(arr, function (k, v) {
                    if (v !== '' && v !== null && v !== "-1" && typeof v !== 'undefined') {
                        newArr[k] = v;
                    }
                });
                return newArr;
            }
            return arr
        }
        function setHeaderInfoSearch() {
            var html = '';
            console.log()
            dateTypeName = $('#dateType').val()!==''?$('#dateType option:selected').text():'';
            // nguonThu = $('#cbb_NguonThu').val()!==''?$('#cbb_NguonThu option:selected').text():'';
            xe = $('#cbb_Xe').val()!==''?$('#cbb_Xe option:selected').text():'';
            taiXe = $('#driverId').val()!==''?$('#driverId option:selected').text():'';
            phuXe = $('#assistantId').val()!==''?$('#assistantId option:selected').text():'';
            tuyen = $('#cbb_Tuyen').val()!==''?$('#cbb_Tuyen option:selected').text():'';
            phongBan = $('#departmentId').val()!==''?$('#departmentId option:selected').text():'';
            nguoiThu = $('#cashierUserId').val()!==''?$('#cashierUserId option:selected').text():'';
            nguoiBan = $('#creatorId').val()!==''?$('#creatorId option:selected').text():'';
            sdtKhach = $('#customerPhone').val()!==''?$('#customerPhone').val():'';
            tuNgay = $('#txtStartDate').val()!==''?$('#txtStartDate').val():'';
            denNgay = $('#txtEndDate').val()!==''?$('#txtEndDate').val():'';
            html += dateTypeName!==''?'Kiểu : '+dateTypeName:'';
            // html += nguonThu!==''?', Nguồn thu : '+nguonThu:'';
            html += xe!==''?', Xe : '+xe:'';
            html += taiXe!==''?', Tài Xế : '+taiXe:'';
            html += phuXe!==''?', Phụ xe : '+phuXe:'';
            html += tuyen!==''?', Tuyến : '+tuyen:'';
            html += phongBan!==''?', Phòng Ban : '+phongBan:'';
            html += nguoiThu!==''?', Người Thu : '+nguoiThu:'';
            html += nguoiBan!==''?', Người Bán : '+nguoiBan:'';
            html += sdtKhach!==''?', SDT Khách : '+sdtKhach:'';
            html += tuNgay!==''?', Từ Ngày : '+tuNgay:'';
            html += denNgay!==''?', Đến Ngày : '+denNgay:'';
            $('.infoSearch').html(html);
        }
    </script>
    <!-- End Content -->
    <script>
        // charts data
        charts_data = {
            data: {
                d2: []
            }
        };
    </script>
    <script src="/public/javascript/jquery.printPage.js" type="text/javascript"></script>
    <script src="/public/javascript/printThis.js" type="text/javascript"></script>
    <script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
    <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>
    <!-- Charts Helper Demo Script -->
    <script src="/public/javascript/chart/charts.helper.js"></script>
    <!-- Charts Page Demo Script -->
    {{--<script src="/public/javascript/chart/charts.js"></script>--}}
    <!--  Flot (Charts) JS -->
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.selection.js"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.orderBars.js" type="text/javascript"></script>
    <!-- Add thư viện pdf -->
    <script src="https://github.com/niklasvh/html2canvas/releases/download/v0.5.0-beta4/html2canvas.min.js"></script>
    <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
@endsection