@extends('cpanel.template.layout')
@section('title', 'Báo cáo khách hàng')
@section('content')
    <style>
        #tb_report{
            margin-top: 50px;
            margin-left: 10px;
        }
    </style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Báo cáo khách hàng</h3>
                </div>
                <div class="pull-right span4 t_align_r">
                    {{--<div class="row-fluid">--}}
                        {{--<div class="input-append">--}}
                            {{--<button onclick="showinfo(this,'chart')"--}}
                                    {{--class="btn btn-default btn-flat btn-report"><i--}}
                                        {{--class="iconic-chart"></i> Biểu đồ--}}
                            {{--</button>--}}
                            {{--<button onclick="showinfo(this,'table')"--}}
                                    {{--class="btn-activate btn btn-default btn-flat btn-report"><i--}}
                                        {{--class="icon-list-alt"></i> Chi tiết--}}
                            {{--</button>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i
                                        class="icon-print"></i> In
                            </button>
                            <div class="btn-group export-btn">
                                <button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất
                                    file <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="excel" data-fileName="bao_cao">Excel</a></li>
                                    <li><a id="pdf" data-fileName="bao_cao">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div >
            <div class="row-fluid">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Điều kiện lọc</h4>
                    </div>
                    <form action="">
                        @if(empty(session('userLogin')['userInfo']['listAgency']))
                            <div class="row-fluid">

                                <div class="span3">
                                    <label for="routeId">Tuyến</label>
                                    <select name="routeId" id="routeId">
                                        <option value="">Tất cả</option>
                                        @foreach($route as $row)
                                        <option {{request('routeId')==$row['routeId']?'selected':''}}  value="{{$row['routeId']}}">{{$row['routeName']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="span3">
                                    <label for="paymentType">Hình thức thanh toán</label>
                                    <select name="paymentType" id="paymentType" class="paymentType">
                                        <option value="[1,2,3,4,5,6,7]" {{request('paymentType')=='[1,2,3,4,5,6,7]'?'selected':''}}>Tất cả</option>
                                        <option value="1" {{request('paymentType')=='1'?'selected':''}}>Thanh toán Trực tuyến</option>
                                        <option value="2" {{request('paymentType')=='2'?'selected':''}}>Thanh toán tiền mặt cho phụ xe</option>
                                        <option value="3" {{request('paymentType')=='3'?'selected':''}}>Thanh toán tiền mặt tại quầy</option>
                                        <option value="4" {{request('paymentType')=='4'?'selected':''}}>Thanh toán bằng tiền mặt khi người nhận nhận đồ</option>
                                        <option value="5" {{request('paymentType')=='5'?'selected':''}}>Thanh toán bằng tiền mặt tại Payoo</option>
                                        <option value="6" {{request('paymentType')=='6'?'selected':''}}>Chuyển khoản</option>
                                        <option value="7" {{request('paymentType')=='7'?'selected':''}}>Epay</option>
                                    </select>
                                </div>

                                <div class="span3">
                                    <label for="startDate">Từ ngày</label>
                                    <div class="input-append">
                                        <input autocomplete="off" value="{{request('startDate',date('d-m-Y',time()))}}"
                                               id="startDate" name="startDate" type="date">
                                    </div>
                                </div>

                            </div>
                            <div class="row-fluid">

                                <div class="span3">
                                    <label for="status">Trạng thái vé</label>
                                    <select name="status" id="status">
                                        <option value="[1,2,3,4,5,6,7]" {{request('status')=='[1,2,3,4,5,6,7]'?'selected':''}}>Tất cả</option>
                                        <option value="1" {{request('status')=='1'?'selected':''}}>Hết hạn giữ chỗ</option>
                                        <option value="2" {{request('status')=='2'?'selected':''}}>Đang giữ chỗ</option>
                                        <option value="3" {{request('status')=='3'?'selected':''}}>Đã thanh toán</option>
                                        <option value="4" {{request('status')=='4'?'selected':''}}>Đã lên xe</option>
                                        <option value="5" {{request('status')=='5'?'selected':''}}>Đã hoàn thành</option>
                                        <option value="6" {{request('status')=='6'?'selected':''}}>Quá giờ giữ chỗ</option>
                                        <option value="7" {{request('status')=='7'?'selected':''}}>Ưu tiên giữ chỗ</option>
                                    </select>
                                </div>

                                <div class="span3">
                                    <label for="phoneNumber">Số điện thoại</label>
                                    <input type="text" value="{{request('phoneNumber')}}" name="phoneNumber" id="phoneNumber">
                                </div>

                                <div class="span3">
                                    <label for="endDate">Đến ngày</label>
                                    <div class="input-append">
                                        <input autocomplete="off" value="{{request('endDate',date('d-m-Y',time()))}}"
                                               id="endDate" name="endDate" type="date" >
                                    </div>
                                </div>


                                <div class="span3">
                                    <label class="separator hidden-phone" for="add"></label>
                                    <button class="btn btn-info btn-flat-full hidden-phone">TÌM</button>
                                </div>
                            </div>
                        @endif
                    </form>

                </div>
            </div>
            @php $tong=0; $tongghehuy=0;$tongghedat=0; @endphp
            @foreach($results as $row)
                @php
                    $tong+=@$row['totalMoney'];
                    $tongghedat+=@$row['totalSeat'];
                    $tongghehuy+=@$row['totalCancelSeat'];
                @endphp
            @endforeach
            <div class="row-fluid bg_light" id="tb_report">
                <table class=" tb_report table table-striped   ">
                    <thead>
                    @if(count($results)>0)
                        <tr class="total">
                            <th colspan="3">TỔNG</th>
                            <td class="center">{{number_format($tongghedat)}}</td>
                            <td class="center">{{number_format($tongghehuy)}}</td>
                            <td class="center">@moneyFormat($tong)</td>
                        </tr>
                        @endif
                    <tr >
                        <th class="center">STT</th>
                        <th class="center">Khách hàng</th>
                        <th class="center">Số điện thoại</th>
                        <th class="center">Số ghế đặt</th>
                        <th class="center">Số ghế hủy</th>
                        <th class="center">Tổng tiền</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $dem=0 @endphp
                    @foreach($results as $row)
                        @php $dem++; @endphp
                        <tr>
                           <td class="center">{{$dem}}</td>
                           <td>{{@$row['fullName']}}</td>
                           <td>{{@$row['phoneNumber']}}</td>
                           <td class="center">{{@$row['totalSeat']}}</td>
                           <td class="center">{{@$row['totalCancelSeat']}}</td>
                           <td class="right">{{number_format($row['totalMoney'])}}</td>
                        </tr>
                    @endforeach

                    <tr class="total">
                        <th colspan="3">TỔNG</th>
                        <td class="center">{{number_format($tongghedat)}}</td>
                        <td class="center">{{number_format($tongghehuy)}}</td>
                        <td class="center">@moneyFormat($tong)</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="row-fluid" id="chart_report">
                <div class="widget-body">
                    <div id="chart_by_date" style="height: 250px;"></div>
                </div>
            </div>
        </div>

        @include('cpanel.Print.print')
    </div>

    <script type="text/javascript">

        setTimeout(function () {
            $('#chart_report').hide();
        }, 1000);

        $('#startSearch,.hidden-phone,.visible-phone').click(function () {
            var text = $('#cbb_NguonThu option:selected').text();
            $('#tenNguonThu').val(text);
        });

        $(document).ready(function () {


        });


        function showinfo(oject, type) {
            switch (type) {
                case 'table': {
                    $('#tb_report').show();
                    $('#chart_report').hide();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');


                    break;
                }
                case 'chart': {
                    $('#tb_report').hide();
                    $('#chart_report').show();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');

                    break;
                }
                default: {
                    break;
                }
            }
        }

        function printDiv(divName) {
            if($('#tb_report').is(":visible")) {
                $('#'+divName).printThis({
                    pageTitle:  '&nbsp;'
                });
            }
        }
    </script>
    <!-- End Content -->
    <script>
        charts_data = {
            data: {
                d2: []
            }
        };
    </script>
    <script src="/public/javascript/jquery.printPage.js" type="text/javascript"></script>
    <script src="/public/javascript/printThis.js" type="text/javascript"></script>
    <script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
    <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>
    <!-- Charts Helper Demo Script -->
    <script src="/public/javascript/chart/charts.helper.js"></script>
    <!-- Charts Page Demo Script -->
    <script src="/public/javascript/chart/charts.js"></script>
    <!--  Flot (Charts) JS -->
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.selection.js"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.orderBars.js" type="text/javascript"></script>
    <!-- Add thư viện pdf -->
    <script src="https://github.com/niklasvh/html2canvas/releases/download/v0.5.0-beta4/html2canvas.min.js"></script>
    <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
@endsection