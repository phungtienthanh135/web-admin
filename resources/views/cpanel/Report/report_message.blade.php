@extends('cpanel.template.layout')
@section('title', 'Báo cáo tin nhắn')
@section('content')
    <style>
        #tb_report{
            margin-top: 50px;
        }
    </style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Báo cáo tin nhắn</h3>
                </div>
                <div class="pull-right span4 t_align_r">
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i
                                        class="icon-print"></i> In
                            </button>

                            <div class="btn-group export-btn">
                                <button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất
                                    file <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="excel" data-fileName="bao_cao_tin_nhan">Excel</a></li>
                                    <li><a id="pdf">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span3" style="padding: 5px;">
                <fieldset style="border : 2px groove threedface;padding: 10px;background: #fff">
                    <legend style="text-align: center;width: auto">Số dư ví tin nhắn</legend>
                    <h2 class="text-center">@moneyFormat($money!=null ? $money['currentAmount']:0)</h2>
                    @if($money!=null&&$money['currentAmount']<100000)
                    <div class="text-center" style="color: red">Luôn đảm bảo số dư > 1000 VND<br> để hệ thống tin nhắn không bị gián đoạn </div>
                    @endif
                </fieldset>
                <br>
                    <form action="{{ action("ReportController@topUp") }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="objectId" value="{!! $money!=null ? $money['id']:"" !!}">
                        <div class="input-append" style="width: 95%">
                            <input type="number" min="1000" value="500000" required style="margin:0;width: 65%" name="amount" placeholder="Nhập số tiền muốn nạp"><button style="border-radius: 0;width: 35%" class="btn btn-warning">Nạp tiền</button>
                        </div>
                        <span style="color: red">{{ $msg }}</span>
                    </form>
                <div style="font-size: 15px;text-align: justify;padding-top: 10px;color: #0000CC;">Dịch vụ tin nhắn là dịch vụ của nhà mạng. AN VUI kết nối hệ thống gửi tin nhắn nhằm tạo tiện ích cho nhà xe có thể gửi tin nhắn tới toàn bộ khách hàng và nội bộ của mình. Phí tin nhắn được tính là <span style="color: red">{{@$result['smsSummary']['pricePerSms']}} VNĐ/1 tin</span> đối với tin nhắn thông thường và <span style="color: red">800 VNĐ/1 tin</span>  đối với tin nhắn mang thương hiệu riêng của nhà xe.
                    <br>Số liệu được cập nhật và thống kê theo báo cáo để nhà xe có thể theo dõi và đối chiếu. Nếu có bất cứ vấn đề gì cần hỗ trợ xin vui lòng liên hệ : 1900 7034 hoặc website
                    <a href="http://anvui.vn">http://anvui.vn</a></div>
            </div>
            <div class="span9">
                <div>
                    <div class="row-fluid">
                            <form action="" method="get">
                                <div class="row-fuild">
                                    <div class="span3">
                                        <label for="txtStartDate">Từ ngày</label>
                                        <div class="input-append">
                                            <input value="{{request('startDate',date('d-m-Y'))}}"
                                                   id="txtStartDate" name="startDate" type="text" readonly>
                                        </div>
                                    </div>

                                    <div class="span3">
                                        <label for="txtEndDate">Đến ngày</label>
                                        <div class="input-append">
                                            <input value="{{request('endDate',date('d-m-Y'))}}" id="txtEndDate" name="endDate"
                                                   type="text" readonly>
                                        </div>
                                    </div>

                                    <div class="span3">
                                        <label class="separator hidden-phone" for="add"></label>
                                        <button class="btn btn-info btn-flat-full hidden-phone search">LỌC</button>
                                        <button class="btn btn-info btn-flat visible-phone search search">LỌC</button>
                                    </div>
                                </div>
                            </form>
                    </div>
                    <div class="row-fluid" id="tb_report" style="margin-top: 10px">
                        <table class="table table-striped">
                            <thead>
                            @if(count($result['smsDetail'])>0)
                                <tr class="total">
                                    <th colspan="2">Giá: {{@$result['smsSummary']['pricePerSms']}} (VNĐ/tin)</th>
                                    <td class="center">{{($result['smsSummary']['totalSent'])}}</td>
                                    <td class="center">{{($result['smsSummary']['totalFailed'])}}</td>
                                    <td class="center">{{($result['smsSummary']['totalMessage'])}}</td>
                                    <td class="center">@moneyFormat(@$result['smsSummary']['totalMoney'])</td>
                                </tr>
                            @endif
                            <tr>
                                <th class="center">STT</th>
                                <th class="center">Ngày</th>
                                <th class="center">Gửi đi</th>
                                <th class="center">Chưa thành công</th>
                                <th class="center">Thành công</th>
                                <th class="center">Đã thanh toán</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($count=1)
                            @foreach($result['smsDetail'] as $row)
                                <tr>
                                    <td class="center">{{$count}}</td>
                                    <td class="center">{{date('d-m-Y',$row['createdDate'] /1000)}}</td>
                                    <td class="center">{{$row['totalSent']}}</td>
                                    <td class="center">{{$row['totalSent']-$row['totalSuccess']}}</td>
                                    <td class="center">{{$row['totalSuccess']}}</td>
                                    <td class="right">{{number_format($row['totalPaidMoney'])}}</td>

                                </tr>
                                @php($count++)
                            @endforeach
                            </tbody>
                            <tr class="total">
                                <th colspan="2">Giá: {{@$result['smsSummary']['pricePerSms']}} (VNĐ/tin)</th>
                                <td class="center">{{($result['smsSummary']['totalSent'])}}</td>
                                <td class="center">{{($result['smsSummary']['totalFailed'])}}</td>
                                <td class="center">{{($result['smsSummary']['totalMessage'])}}</td>
                                <td class="center">@moneyFormat(@$result['smsSummary']['totalMoney'])</td>
                            </tr>
                        </table>
                    </div>
                    <div class="row-fluid" id="chart_report">
                        <div class="widget-body">
                            <div id="message" style="height: 250px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('cpanel.Print.print')
    <script type="text/javascript">

    </script>
   <script>
        function printDiv(divName) {
            if ($('#tb_report').is(":visible")) {
                $('#' + divName).printThis({
                    pageTitle: '&nbsp;'
                });
            }

        }
    </script>
    <script src="/public/javascript/jquery.printPage.js" type="text/javascript"></script>
    <script src="/public/javascript/printThis.js" type="text/javascript"></script>
    <script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
    <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>
    <!-- Add thư viện pdf -->
    <script src="https://github.com/niklasvh/html2canvas/releases/download/v0.5.0-beta4/html2canvas.min.js"></script>
    <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
    <!--  Flot (Charts) JS -->
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.selection.js"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.orderBars.js" type="text/javascript"></script>
    <!-- End Content -->

@endsection