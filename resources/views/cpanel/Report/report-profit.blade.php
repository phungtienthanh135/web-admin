@extends('cpanel.template.layout')
@section('title', 'Báo cáo kết quả theo chuyến')
@section('content')
    <style>
        #tb_report {
            margin-top: 50px;
            margin-left: 10px;
        }

        .select2-selection {
            height: 20px;
        }

        table td {
            max-width: 200px;
        }

        .lds-roller {
            display: inline-block;
            position: relative;
            width: 64px;
            height: 64px;
        }

        .lds-roller div {
            animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            transform-origin: 32px 32px;
        }

        .lds-roller div:after {
            content: " ";
            display: block;
            position: absolute;
            width: 6px;
            height: 6px;
            border-radius: 50%;
            background: #000;
            margin: -3px 0 0 -3px;
        }

        .lds-roller div:nth-child(1) {
            animation-delay: -0.036s;
        }

        .lds-roller div:nth-child(1):after {
            top: 50px;
            left: 50px;
        }

        .lds-roller div:nth-child(2) {
            animation-delay: -0.072s;
        }

        .lds-roller div:nth-child(2):after {
            top: 54px;
            left: 45px;
        }

        .lds-roller div:nth-child(3) {
            animation-delay: -0.108s;
        }

        .lds-roller div:nth-child(3):after {
            top: 57px;
            left: 39px;
        }

        .lds-roller div:nth-child(4) {
            animation-delay: -0.144s;
        }

        .lds-roller div:nth-child(4):after {
            top: 58px;
            left: 32px;
        }

        .lds-roller div:nth-child(5) {
            animation-delay: -0.18s;
        }

        .lds-roller div:nth-child(5):after {
            top: 57px;
            left: 25px;
        }

        .lds-roller div:nth-child(6) {
            animation-delay: -0.216s;
        }

        .lds-roller div:nth-child(6):after {
            top: 54px;
            left: 19px;
        }

        .lds-roller div:nth-child(7) {
            animation-delay: -0.252s;
        }

        .lds-roller div:nth-child(7):after {
            top: 50px;
            left: 14px;
        }

        .lds-roller div:nth-child(8) {
            animation-delay: -0.288s;
        }

        .lds-roller div:nth-child(8):after {
            top: 45px;
            left: 10px;
        }

        @keyframes lds-roller {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

    </style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Báo cáo kết quả theo chuyến</h3>
                </div>
                <div class="pull-right span4 t_align_r">
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="showinfo(this,'chart')" class=" btn btn-default btn-flat btn-report"><i
                                        class="iconic-chart"></i> Biểu đồ
                            </button>
                            <button onclick="showinfo(this,'table')"
                                    class="btn-activate btn btn-default btn-flat btn-report"><i
                                        class="icon-list-alt"></i> Chi tiết
                            </button>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i
                                        class="icon-print"></i> In
                            </button>

                            <div class="btn-group export-btn">
                                <button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất
                                    file <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="excel" data-fileName="bao_cao_theo_chuyen">Excel</a></li>
                                    <li><a id="pdf">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <div>
            <div class="row-fluid filterProfit" style="background: #dedfe1;">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Điều kiện lọc</h4>
                    </div>
                    <form action="" method="get">
                        <div class="row-fluid">
                            <div class="span3">
                                <label for="cbb_Tuyen">Tuyến ( không chọn sẽ lấy tất cả) </label>
                                <select name="routeId" id="cbb_Tuyen" multiple>
                                    @foreach($listRoute as $route)
                                        <option {{request('routeId')==$route['routeId']?'selected':''}} value="{{$route['routeId']}}">{{$route['routeName']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="span3">
                                <label for="">Chọn Lịch</label>
                                <select name="" id="cbbSchedule">
                                    <option value="">Tất cả</option>
                                </select>
                            </div>
                            <div class="span3">
                                <label for="txtStartDate">Từ ngày</label>
                                <div class="input-append">
                                    <input value="{{request('startDate',date('d-m-Y'))}}" class="span11"
                                           id="txtStartDate" name="startDate" type="text" readonly>
                                </div>
                            </div>
                            <div class="span3">
                                <label for="txtticketStatus">Trạng thái vé</label>
                                <select name="ticketStatus" id="txtticketStatus">
                                    <option value="">Tất cả</option>
                                    <option value="2" {!! request('ticketStatus')==2?"selected":"" !!}>Đang giữ chỗ
                                    </option>
                                    <option value="7" {!! request('ticketStatus')==7?"selected":"" !!}>Ưu tiên giữ chỗ
                                    </option>
                                    <option value="3" {!! request('ticketStatus')==3?"selected":"" !!}>Đã thanh toán
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="row-fuild">
                            <div class="span3">
                                <label for="numberPlate">Biển số</label>
                                <select name="numberPlate" id="numberPlate">
                                    <option value="">Tất cả</option>
                                    @foreach($listVehicle as $row)
                                        <option value="{{@$row['numberPlate']}}" {!! $row['numberPlate']==request('numberPlate')?'selected':'' !!}>{{$row['numberPlate']}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="span3">
                                <label for="txtEndDate">Lái xe</label>
                                <select name="driver" id="driver">
                                    <option value="">Tất cả</option>
                                    @foreach($listDriver as $row)
                                        <option value="{{@$row['userId']}}" {!! $row['userId']==request('driver')?'selected':'' !!}>{{$row['fullName']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="span3">
                                <label for="txtEndDate">Đến ngày</label>
                                <div class="input-append">
                                    <input value="{{request('endDate',date('d-m-Y'))}}" id="txtEndDate" name="endDate"
                                           class="span11" type="text" readonly>
                                </div>
                            </div>

                            <div class="span3">
                                <label class="separator hidden-phone" for="add"></label>
                                <button class="btn btn-info btn-flat-full hidden-phone"
                                        onclick="return getDataReport();">LỌC
                                </button>
                                <button class="btn btn-info btn-flat visible-phone" onclick="return getDataReport();">
                                    LỌC
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row-fluid" id="tb_report">
                <table class="table table-tripped">
                    <thead>
                    <tr class="total">
                        <th colspan="4">TỔNG</th>
                        <td class="totalStaffOrder right"></td>
                        <td class="totalAgencyOrder right"></td>
                        <td class="totalOnlineOrder right"></td>
                        <td class="totalTicketCancel right"></td>
                        <td class="totalNumberTicket right"></td>
                        <td class="totalProfit right"></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>STT</th>
                        <th>Tuyến</th>
                        <th>Chuyến</th>
                        <th>Xe</th>
                        <th>Phòng vé nhập</th>
                        <th>Vé Đại Lí</th>
                        <th>Vé Online</th>
                        <th>Vé Hủy</th>
                        <th>Tổng vé</th>
                        <th>Tổng tiền</th>
                        <th>Lái xe</th>
                        <th>Ghi chú</th>
                    </tr>
                    </thead>
                    <tbody id="contentReport">
                    </tbody>
                    <tfoot>
                    <tr class="total">
                        <th colspan="4">TỔNG</th>
                        <td class="totalStaffOrder right"></td>
                        <td class="totalAgencyOrder right"></td>
                        <td class="totalOnlineOrder right"></td>
                        <td class="totalTicketCancel right"></td>
                        <td class="totalNumberTicket right"></td>
                        <td class="totalProfit right"></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div class="row-fluid" id="chart_report">
                <div class="widget-body" id="listChar">
                </div>
            </div>
        </div>

    </div>
    @include('cpanel.Print.print')
    <script>
        var dataReport = [], dataChar = [];
        var dataReportSorted = [],listIndexChar=[];
        var mock=(new Date(2018,8,30)).getTime();
        $(document).ready(function () {
            changeSchedule();
            setTimeout(function () {
                $('#chart_report').hide();
            }, 1000);
            $('input[type=radio][name=chartType]').change(function () {
                $.each(listIndexChar,function(k,v){
                    setChart(dataReport.ticket, dataReport.money,v);
                });
            });
            $('body').on('change','#cbb_Tuyen',function () {
                changeSchedule();
            })
        });
        $(function () {
            $('#cbb_Tuyen').select2();
            var lt = JSON.parse(localStorage.getItem('listColumnTripShowVer1'));
            $('#cbb_Tuyen').val(lt).change();
            $('#cbb_listDriverId,#cbb_listAssistantId').select2();
            $('.btnUpdate').click(function () {
                var scheduleId = $(this).parents('tr').data('scheduleid'),
                    dateRun = $(this).parents('tr').data('starttime');
                $('#txtScheduleId').val(scheduleId);
                $('#txtDateRun').val(dateRun);
            });

            $('.btnUpdateTrip').click(function () {
                var scheduleId = $(this).parents('tr').data('scheduleid'),
                    day = $(this).parents('tr').data('day'),
                    dateRun = $(this).parents('tr').data('starttime'),
                    time = $(this).parents('tr').data('starttimeformat');
                //Cau hinh wickedpicker
                var options = {
                    now: time,
                    twentyFour: true,
                    upArrow: 'wickedpicker__controls__control-up',
                    downArrow: 'wickedpicker__controls__control-down',
                    close: 'wickedpicker__close',
                    hoverState: 'hover-state',
                    title: 'Chọn thời gian xuất bến',
                    clearable: false
                };

                $('#txt_newDate').wickedpicker(options);
                $('#scheduleId').val(scheduleId);
                $('#dateRun').val(dateRun);
                $('#day').val(day);
            });
        });

        function getDataReport() {
            $('#tb_report').find('tbody').html('<tr><td colspan="15" style="text-align: center"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td></tr>');
            companyId = '{{session('companyId')}}';

            startDate = $('#txtStartDate').val();
            startDate = startDate.split("-");
            startDate = startDate[1] + "/" + startDate[0] + "/" + startDate[2];
            startDate = new Date(startDate).getTime() + 1;

            endDate = $('#txtEndDate').val();
            endDate = endDate.split("-");
            endDate = endDate[1] + "/" + endDate[0] + "/" + endDate[2];
            endDate = new Date(endDate).getTime() + 86400000;//23h 59p 59s

            numberPlate = $('#numberPlate').val() || '';
            driverId = $('#driver').val() || '';
            routeId = $('#cbb_Tuyen').val() || [];
            ticketStatus = $('#txtticketStatus').val();
            ticketStatus = ticketStatus != '' ? '["' + ticketStatus + '"]' : '';
            var scheduleId = $('#cbbSchedule').val();

            dataRequest = removeItemNullInArray({
                "timeZone": 7,
                "startDate": startDate,
                "endDate": endDate,
                "option": 1,
                "numberPlate": numberPlate,
                "driverId": driverId,
                "listRouteId": routeId,
                "ticketStatus": ticketStatus,
                scheduleId:scheduleId,
            });

            console.log(JSON.stringify(dataRequest));

            $.ajax({
                dataType: "json",
                type: 'post',
                url: urlDBD("report/reconciliation-staff"),
                headers: {
                    'DOBODY6969': '{{ session('userLogin')['token']['tokenKey'] }}',
                },
                contentType: 'application/json',
                data: JSON.stringify(dataRequest),
                success: function (data) {
                    console.log(data);
                    if (data.code === 200) {
                        dataReport = data.results.listReport;
                        setReport();
                    } else {
                        notyMessage("Lỗi khi lấy dữ liệu", 'error');
                    }
                },
                error: function (data) {
                    notyMessage("Lỗi khi gửi yêu cầu", 'error');
                    console.log(data);
                }
            });
            return false;
        }

        function makeData(data,index) {
            totalTicket = 0;
            totalMoney = 0;
            dataRp = {ticket: [], money: []};
            var listStartTime = [],listTripSorted=[],listTimeSorted=[],minTime=1390845600000,item;
            var dataSorted=[];
            for(var i=0;i<data.length;i++) {
                $.each(data, function (k, v) {
                  if(listTimeSorted.indexOf(v.tripId)==-1 && minTime>v.startTime) {
                      item = v;
                      minTime = v.startTime;
                  }
                });
                listTimeSorted.push(item.tripId);
                dataSorted.push(item);
                minTime=1390845600000;
            }
            console.log('dataSorted',dataSorted);
            $.each(dataSorted, function (k, v) {
                if(listStartTime.indexOf(v.startTime)==-1) {
                    var numberTicket=0,profit=0;
                    $.each(dataSorted, function (k1, v1) {
                        if (v1.startTime==v.startTime) {
                            minStartTime = v.startTime;
                            totalTicket += v1.numberOfTicket;
                            totalMoney += v1.profit;
                            numberTicket+=v1.numberOfTicket;
                            profit+=v1.profit;
                        }
                    });
                    listTripSorted.push([v.startTime,numberTicket,profit]);
                    listStartTime.push(v.startTime);
                }
            });
            $.each(listTripSorted, function (k, v) {
                dataRp.ticket.push([1390780800000+v[0], v[1]]);
                dataRp.money.push([1390780800000+v[0], v[2]]);
            });
            if($('#chart'+index).length==0) $('#listChar').append(' <div id="chart'+index+'" style="width:100%;height: 350px;"></div>' +
                '<div class="text-center TotalTicket'+index+'" style="margin: 20px"></div>');

            $('.TotalTicket'+index).html('<span style="padding:10px;border:2px solid #084388;font-size: 20px;color:#fff;background: #4f6c8d">Tuyến :'+data[0].routeId+' - Tổng số vé : <strong>' + $.number(totalTicket) + " (Vé)</strong> . Tổng số tiền : <strong>" + $.number(totalMoney) + ' (VNĐ)' + '</strong></span>');
            return dataRp;
        }
        function setChart(dataNumberTicket, dataTotalMoney,index) {
            $.each(dataNumberTicket,function (k,v) {
                if(v[2]) delete v[2];
                if(v[3]) delete v[3];
            });
            $.each(dataTotalMoney,function (k,v) {
                if(v[2]) delete v[2];
                if(v[3]) delete v[3];
            });
            console.log(dataNumberTicket,dataTotalMoney);
            var dataset = [
                {label: "Số Vé", data: dataNumberTicket},
                {label: "Số Tiền", data: dataTotalMoney, yaxis: 2},
            ];
            var options= {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        radius: 3,
                        fill: true,
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: "%H:%M",
                    tickSize: [1, "hour"], // tick every hour
                    twelveHourClock: true,
                    // min: mock, // start of today
                    // max: mock+3600000*24,
                    min: 1390780800000,
                    max: 1390863600000
                },
                yaxes: [{
                    axisLabel: "Số vé",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 3,
//                    tickFormatter: function (v, axis) {
//                        return v+ 'Vé';
//                    }
                },
                    {
                        position: "right",
                        axisLabel: "Change(%)",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabelPadding: 3
                    }
                ],
                legend: {
                    noColumns: 0,
                    labelBoxBorderColor: "#000000",
                    position: "nw"
                },
                grid: {
                    hoverable: true,
                    borderWidth: 2,
                    borderColor: "#633200",
                    backgroundColor: {colors: ["#ffffff", "#EDF5FF"]}
                },
                colors: ["#084388", "red"],
            };
            if($('#chart'+index).length>0)
            $.plot($("#chart"+index), dataset, options);
            $("#chart"+index).UseTooltip();

        }

        var previousPoint = null, previousLabel = null;
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        $.fn.UseTooltip = function () {
            $(this).bind("plothover", function (event, pos, item) {
                if (item) {
                    if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                        previousPoint = item.dataIndex;
                        previousLabel = item.series.label;
                        $("#tooltip").remove();

                        var x = item.datapoint[0];
                        var y = item.datapoint[1];

                        var color = item.series.color;
                        var month = new Date(x).getMonth();
                        if (item.seriesIndex == 0) {
                            showTooltip(item.pageX,
                                item.pageY,
                                color,
                                "<strong>" + item.series.label + "</strong> : <strong>" + y + "</strong>(Vé)");
                        } else {
                            showTooltip(item.pageX,
                                item.pageY,
                                color,
                                "<strong>" + item.series.label + "</strong> : <strong>" + $.number(y) + "</strong></strong>(VNĐ)");
                        }
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            });
        };

        function showTooltip(x, y, color, contents) {
            $('<div id="tooltip">' + contents + '</div>').css({
                position: 'absolute',
                display: 'none',
                top: y - 40,
                left: x - 120,
                border: '2px solid ' + color,
                padding: '3px',
                'font-size': '9px',
                'border-radius': '5px',
                'background-color': '#fff',
                'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                opacity: 0.9
            }).appendTo("body").fadeIn(200);
        }
        function showinfo(oject, type) {
            switch (type) {
                case 'table': {
                    $('#tb_report').show();
                    $('.filterProfit').show();
                    $('#chart_report').hide();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');
                    $('#headerName').text('Báo cáo kết quả theo chuyến');
                    break;
                }
                case 'chart': {
                    if(dataReport.length) {
                        var routes = [];
                        $.each(dataReport, function (k, v) {
                            if (routes.indexOf(v.routeId) == -1) {
                                $.each(dataReport, function (k1, v1) {
                                    if (v.routeId == v1.routeId) dataReportSorted.push(v1);
                                });
                                routes.push(v.routeId);
                            }
                        });
                        route = [];
                        $.each(dataReportSorted, function (k, v) {
                            if (route.indexOf(v.routeId) == -1) {
                                var dataChar = [];
                                $.each(dataReportSorted, function (k1, v1) {
                                    if (v.routeId == v1.routeId) dataChar.push(v1);
                                });
                                var dataGenerateChar = makeData(dataChar, k);
                                setChart(dataGenerateChar.ticket, dataGenerateChar.money, k);
                                listIndexChar.push(k);
                                route.push(v.routeId);
                            }
                        });
                        $('#tb_report').hide();
                        $('.filterProfit').hide();
                        $('#chart_report').show();
                        $('.btn-activate').removeClass('btn-activate');
                        $(oject).addClass('btn-activate');
                        $('#headerName').text('Biểu đồ báo cáo theo chuyến');
                    }else Message('Không có dữ liệu');
                    break;
                }
                default: {
                    break;
                }
            }
        }

        function setReport() {
            count = {
                totalStaffOrder: 0,
                totalAgencyOrder: 0,
                totalOnlineOrder: 0,
                totalTicketCancel: 0,
                totalNumberTicket: 0,
                totalProfit: 0
            };
//            data_chart_new = [];
            stt = 1;
            html = '';
            $.each(dataReport, function (k, v) {
                html += '<tr>';
                html += '<td  style="text-align: center">' + stt + '</td>';
                html += '<td>' + v.routeId + '</td>';
                html += '<td>' + new Date(v.date + v.startTime).customFormat('#hhhh#:#mm# #DD#-#MM#-#YYYY#') + '</td>';
                html += '<td>' + v.numberPlate + '</td>';
                html += '<td class="right">' + v.staffOrder + '</td>';
                html += '<td class="right">' + v.agencyOrder + '</td>';
                html += '<td class="right">' + v.onlineOrder + '</td>';
                html += '<td class="right">' + v.numberOfCancelTicket + '</td>';
                html += '<td class="right">' + v.numberOfTicket + '</td>';
                html += '<td class="right">' + $.number(v.profit) + '</td>';
                html += '<td>' + (v.driverName || '') + '</td>';
                html += '<td>' + (v.note || '') + '</td>';
                html += '</tr>';
                stt++;
                count.totalStaffOrder += v.staffOrder;
                count.totalAgencyOrder += v.agencyOrder;
                count.totalOnlineOrder += v.onlineOrder;
                count.totalTicketCancel += v.numberOfCancelTicket;
                count.totalNumberTicket += v.numberOfTicket;
                count.totalProfit += v.profit;
            });
            $('.totalStaffOrder').html(count.totalStaffOrder);
            $('.totalAgencyOrder').html(count.totalAgencyOrder);
            $('.totalOnlineOrder').html(count.totalOnlineOrder);
            $('.totalTicketCancel').html(count.totalTicketCancel);
            $('.totalNumberTicket').html(count.totalNumberTicket);
            $('.totalProfit').html(moneyFormat(count.totalProfit));
            $('#tb_report').find('tbody').html(html);
        }

        function removeItemNullInArray(arr) {
            newArr = {};
            if (typeof arr === 'object') {
                $.each(arr, function (k, v) {
                    if (v !== '' && v !== null && typeof v !== 'undefined') {
                        newArr[k] = v;
                    }
                });
                return newArr;
            }
            return arr
        }

        function printDiv(divName) {
            if ($('#tb_report').is(":visible")) {
                $('#' + divName).printThis({
                    pageTitle: '&nbsp;'
                });
            }
        }
        function changeSchedule() {
            var htmlOption = '';
            var routes = $('#cbb_Tuyen').val()||[];
            if(routes.length===1){

                var numberPlate = $('#numberPlate').val() || '';
                var dataSend = {
                    count : 50,
                    page : 0,
                    timeZone : 7,
                    routeId :routes[0],
                    numberPlate:numberPlate,
                };
                $('#cbbSchedule').html('<option value="" disabled>Đang tải dữ liệu</option>');
                SendAjaxWithParam({
                    url : urlDBD('web_schedule/getlist'),
                    type : 'post',
                    dataType : 'json',
                    data : dataSend,
                    success : function (data) {
                        htmlOption ='<option value="">Tất cả</option>';
                        $.each(data.results.result,function (k,v) {
                            htmlOption +='<option value="'+v.scheduleId+'">'+new Date(v.startTime).customFormat('#hhhh# : #mm#')+'</option>';
                        });
                        $('#cbbSchedule').html(htmlOption);
                    },
                    functionIfError : function (data) {
                        $('#cbbSchedule').html('<option value="">Tất cả</option>');
                    }
                });
            }else{
                htmlOption ='<option value="">Tất cả</option>';
                $('#cbbSchedule').html(htmlOption);
            }
        }
    </script>
    <script src="/public/javascript/jquery.printPage.js" type="text/javascript"></script>
    <script src="/public/javascript/printThis.js" type="text/javascript"></script>
    <script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
    <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>
    <!-- Add thư viện pdf -->
    <script src="https://github.com/niklasvh/html2canvas/releases/download/v0.5.0-beta4/html2canvas.min.js"></script>
    <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
    <!--  Flot (Charts) JS -->
    <!-- Table Export To File -->
    <link rel="stylesheet" href="/public/javascript/wickedpicker/wickedpicker.css">
    <script type="text/javascript" src="/public/javascript/wickedpicker/wickedpicker.js"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.selection.js"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.orderBars.js" type="text/javascript"></script>
    <!-- End Content -->

@endsection