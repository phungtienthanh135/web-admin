@extends('cpanel.template.layout')
@section('title', 'Báo cáo doanh thu tổng hợp theo ngày')
@section('content')
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Danh Sách Đại Lý</h3>
                </div>
                <div class="pull-right span4 t_align_r">
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i
                                        class="icon-print"></i> In
                            </button>
                            <div class="btn-group export-btn">
                                <button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất file <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="excel" data-fileName="bao_cao">Excel</a></li>
                                    <li><a id="pdf" data-fileName="bao_cao">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="innerLR">

            <div class="row-fluid m_bottom_20" id="tb_report" >
                <table class="table table-striped">
                    {{--<thead>--}}
                    {{--<tr>--}}
                        {{--<th>STT</th>--}}
                        {{--<th>Mã Đại Lí</th>--}}
                        {{--<th>Tên Đại Lí</th>--}}
                        {{--<th>Số Điện Thoại</th>--}}
                        {{--<th>Địa Chỉ</th>--}}
                    {{--</tr>--}}
                    {{--</thead>--}}
                    {{--<tbody>--}}
                    {{--@php--}}
                        {{--$i=1;--}}
                    {{--@endphp--}}
                    {{--@foreach($report as $row)--}}
                        {{--<tr>--}}
                            {{--<td>{{$i++}}</td>--}}
                            {{--<td>{{$row['userId']}}</td>--}}
                            {{--<td>{{$row['fullName']}}</td>--}}
                            {{--<td>{!! $row['phoneNumber']!=""?"'0".$row['phoneNumber']:"" !!}</td>--}}
                            {{--<td>{{$row['address']}}</td>--}}
                        {{--</tr>--}}
                    {{--@endforeach--}}
                    {{--</tbody>--}}
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên Chuyến</th>
                            <th>Viết Tắt</th>
                            <th>Biển Số Xe</th>
                            <th>Mã</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php
                    $i=1;
                    @endphp
                    @foreach($routes as $route)
                        @foreach($vehicles as $vehicle)
                            <tr>
                            <td>{{$i++}}</td>
                            <td>{{$route['routeName']}}</td>
                            <td>{{$route['routeNameShort']}}</td>
                            <td>{{$vehicle['numberPlate']}}</td>
                            <td>{{$route['routeNameShort']}}-{{$vehicle['numberPlate']}}</td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row-fluid" id="chart_report">
                <div class="widget-body" >
                    <div id="chart_by_date" style="height: 250px;width: 99%"></div>
                </div>
            </div>
        </div>
    </div>
    @include('cpanel.Print.print')
    <script type="text/javascript">
        $('#template_report_title').text('An Vui - Báo cáo tổng hợp theo ngày');
        setTimeout(function () {
            $('#chart_report').hide();
        },1000);


        function showinfo(oject,type) {
            switch(type)
            {
                case 'table':
                {
                    $('#tb_report').show();
                    $('#chart_report').hide();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');


                    break;
                }
                case 'chart':
                {
                    $('#tb_report').hide();
                    $('#chart_report').show();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');

                    break;
                }
                default:
                {
                    break;
                }
            }
        }

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            $('#template_report_content').html(printContents);

            document.body.innerHTML = $('#template_report').html();

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
    <!-- End Content -->

    <script>

        {{--// charts data--}}
        {{--charts_data = {--}}
            {{--@php--}}
                {{--$j=1;--}}
                {{--$i=1;--}}
            {{--@endphp--}}
            {{--data: {--}}
                {{--d1: [[0, 0],@foreach($report as $row)[{{$i++}}, {{$row['totalTicket']}}], @endforeach ],--}}
                {{--d2: [[0, 0],@foreach($report as $row)[{{$j++}}, {{$row['totalRevenue']}}], @endforeach ]--}}
            {{--}--}}

        {{--};--}}
    </script>
    <!-- Table Export To File -->
    <script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
    <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>
    <!-- Charts Helper Demo Script -->
    <script src="/public/javascript/chart/charts.helper.js"></script>
    <!-- Charts Page Demo Script -->
    <script src="/public/javascript/chart/charts.js"></script>
    <!--  Flot (Charts) JS -->
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.selection.js"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.orderBars.js" type="text/javascript"></script>

@endsection