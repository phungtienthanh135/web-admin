@extends('cpanel.template.layout')
@section('title', 'Báo cáo danh sách vé thanh toán online')

@section('content')
    <style>
        table td {
            max-width: 200px;
        }

        #barChart {
            margin-left: 36%;
        }
        .khung_lich_ve,#tb_report{
            margin-left: 10px;
        }
    </style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8"><h3 id="headerName">Danh sách thanh toán vé online</h3></div>
                <div class="pull-right span4 t_align_r">
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="showinfo(this,'chart')"
                                    class="btn btn-default btn-flat btn-report"><i
                                        class="iconic-chart"></i> Biểu đồ
                            </button>
                            <button onclick="showinfo(this,'table')"
                                    class="btn-activate btn btn-default btn-flat btn-report"><i
                                        class="icon-list-alt"></i> Chi tiết
                            </button>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i
                                        class="icon-print"></i> In
                            </button>
                            <div class="btn-group export-btn">
                                <button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất
                                    file <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="excel" data-fileName="bao_cao_thanh_toan_ve_online">Excel</a></li>
                                    <li><a id="pdf" data-fileName="bao_cao">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="khung_lich_ve">
            <div class="date_picker">
                <div style="padding: 7px;" class="widget widget-4">
                    <div class="widget-head">
                        <h4>TÌM VÉ</h4>
                    </div>
                </div>
                <div>
                    <form action="">
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="row-fluid">
                                    <div class="span6">
                                        <label for="txtPhoneNumber">SĐT</label>
                                        <input class="span12" type="text" name="phoneNumber" id="txtPhoneNumber"
                                               value="{{request('phoneNumber')}}">
                                    </div>
                                    <div class="span6">
                                        <label for="txtTicketCode">Mã Vé</label>
                                        <input class="span12" type="text" name="ticketCode" id="txtTicketCode"
                                               value="{{request('ticketCode')}}">
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span6">
                                        <label for="txtStartDate">Từ ngày</label>
                                        <div class="input-append">
                                            <input value="{{request('startDate',date('d-m-Y'))}}" readonly
                                                   id="txtStartDate" name="startDate" type="text">
                                        </div>
                                    </div>

                                    <div class="span6">
                                        <label for="txtEndDate">Đến ngày</label>
                                        <div class="input-append">
                                            <input value="{{request('endDate',date('d-m-Y'))}}" id="txtEndDate"
                                                   name="endDate"
                                                   readonly type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="span3">

                                <div class="span12">
                                    <label for="txtTransCode">Mã giao dịch</label>
                                    <input class="span12" type="text" name="transCode" id="txtTransCode"
                                           value="{{request('transCode')}}">
                                </div>
                                <div class="span12">
                                    <label>&nbsp;</label>
                                    <button style=" width: 100%; margin-left: -6px;border-radius:0px"
                                            class="btn btn-info hidden-phone"
                                            id="search">TÌM VÉ
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @php $sumCompany = 0;$sumAnvui = 0; $sum = 0;$stt = 0;$sumTicket=0; @endphp
        @foreach($result as $row)
            @php $sumCompany += $row['moneyToTransportCompany'];$sum += $row['totalMoney'];$sumAnvui += $row['moneyToAnVui'];$sumTicket+=$row['totalSeat'];
            @endphp
        @endforeach
        <div class="row-fluid bg_light m_top_10">
            <div class="widget widget-4 bg_light">
                <div class="widget-body" id="tb_report">
                    <table class="table table-hover table-vertical-center tb_report">
                        <thead>
                        @if(count($result)>0)
                            <tr class="total">
                                <th colspan="5">TỔNG</th>
                                <td class="right">{{$sumTicket}}</td>
                                <td class="right"><strong>@moneyFormat($sum)</strong></td>
                                <td class="right"><strong>@moneyFormat($sumCompany)</strong></td>
                                <td class="right"><strong>@moneyFormat($sumAnvui)</strong></td>
                                <td></td>
                            </tr>
                        @endif
                        <tr>
                            <th>STT</th>
                            <th>Ngày đi</th>
                            <th>Mã giao dịch</th>
                            <th>Họ tên</th>
                            <th>Số điện thoại</th>
                            <th>SL</th>
                            <th class="center">Tổng tiền</th>
                            <th class="center">Tiền về nhà xe</th>
                            <th class="center">Tiền về An Vui</th>
                            <th>Trạng thái</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($result as $row)
                            <tr>
                                <td class="center">{{ $stt }}</td>
                                <td>@dateFormat($row['getInTimePlan'])</td>
                                <td>{{$row['ticketCode']}}</td>
                                <td>{{$row['fullName']}}</td>
                                <td>{{$row['phoneNumber']}}</td>
                                <td class="right">{{ $row['totalSeat'] }}</td>
                                <td class="right">{{number_format($row['totalMoney'])}}</td>
                                <td class="right">{{number_format($row['moneyToTransportCompany'])}}</td>
                                <td class="right">{{number_format($row['moneyToAnVui'])}}</td>
                                <td>{{$row['ticketStatus']==3?'Đã thanh toán':
                                ($row['ticketStatus']==1?'Hết hạn giữ chỗ':
                                ($row['ticketStatus']==2?'Đang giữ chỗ':
                                ($row['ticketStatus']==4?'Đã lên xe':
                                ($row['ticketStatus']==5?'Đã hoàn thành':
                                ($row['ticketStatus']==6?'Quá giờ giữ chỗ':
                                ($row['ticketStatus']==7?'Ưu tiên giữ chỗ':''))))))
                                }}</td>
                            </tr>
                            <!--hiển thị chi tiết-->
                            <tr class="info" style="display: none">
                                <td colspan="10">
                                    <table class="tttk" cellspacing="20" cellpadding="4">
                                        <tbody>
                                        <tr>
                                            <td><b>MÃ GIAO DỊCH</b></td>
                                            <td style="width: 25%">{{$row['ticketCode']}}</td>
                                            <td class="right" style="width: 25%"><b>THỜI GIAN GIAO DỊCH</b></td>
                                            <td class="left">@dateFormat($row['createdDate'])</td>
                                        </tr>
                                        <tr>
                                            <td><b>HỌ TÊN</b></td>
                                            <td style="width: 25%">{{$row['fullName']}}</td>
                                            <td class="right" style="width: 25%"><b>SỐ ĐIỆN THOẠI</b></td>
                                            <td class="left">{{$row['phoneNumber']}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>SỐ TIỀN</b></td>
                                            <td style="width: 25%">@moneyFormatNoCurrency($row['totalMoney'])</td>
                                            <td class="right" style="width: 25%"><b>Trạng thái</b></td>
                                            <td class="left">@checkTicketStatus($row['ticketStatus'])</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            @php $stt++;@endphp
                        @endforeach
                        <tr class="total">
                            <th colspan="5">TỔNG</th>
                            <td class="right">{{$sumTicket}}</td>
                            <td class="right"><strong>@moneyFormat($sum)</strong></td>
                            <td class="right"><strong>@moneyFormat($sumCompany)</strong></td>
                            <td class="right"><strong>@moneyFormat($sumAnvui)</strong></td>
                            <td></td>
                        </tr>

                        </tbody>
                    </table>
                    {{--@include('cpanel.template.pagination-without-number',['page'=>$page])--}}
                </div>
                <div class="row-fluid" id="chart_report">
                    <div class="widget-body">
                        <input type="radio" id="barChart" checked name="chartType" class="radio"><label for="barChart" style="display: inline">Biểu đồ cột</label> &nbsp; &nbsp;
                        <input type="radio" id="lineChart" name="chartType" class="radio"><label for="lineChart" style="display: inline">Biểu đồ đường</label> &nbsp; &nbsp; &nbsp; &nbsp;
                        <div id="chart" style="height: 350px;"></div>
                    </div>
                    <br>
                    <div class="text-center TotalTicket"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <script>
        var result ={!! json_encode($result) !!};
        var dataReport = [];
        var checkChartMonth = false;
        $(document).ready(function () {
            setTimeout(function () {
                $('#chart_report').hide();
            }, 1000);
            $('input[type=radio][name=chartType]').change(function () {
                setChart(dataReport.ticket, dataReport.money);
            });
        });

        function printDiv(divName) {
            if ($('#tb_report').is(":visible")) {
                $('#' + divName).printThis({
                    pageTitle: '&nbsp;'
                });
            }
        }

        function makeData(data) {
            var count = 1;
            var startDateFilter = data[0].createdDate;
            var endDateFilter = data[data.length - 1].createdDate;
            checkChartMonth = (endDateFilter - startDateFilter) > 10368000000;
            console.log(endDateFilter, startDateFilter,endDateFilter - startDateFilter);
            totalTicket = 0;
            totalMoney = 0;
            dataRp = {ticket: [], money: []};
            for (var i = 0; count < data.length; i++) {
                var year = (new Date(startDateFilter)).getFullYear(), month = (new Date(startDateFilter)).getMonth(),
                    date = (new Date(startDateFilter)).getDate();
                var miliseconds, overMiliseconds;
                miliseconds = (new Date(year, month, date)).getTime();
                overMiliseconds = (new Date(year, month, date + 1)).getTime();
                if (checkChartMonth) {
                    miliseconds = (new Date(year, month, 1)).getTime();
                    overMiliseconds = (new Date(year, month + 1, 1)).getTime();
                }
                var seat = 0, money = 0;
                $.each(data, function (k, v) {
                    if (v.createdDate < overMiliseconds) {
                        if (v.createdDate >= miliseconds) {
                            seat += v.totalSeat;
                            money += v.totalMoney;
                            totalTicket += v.totalSeat;
                            totalMoney += v.totalMoney;
                            count++;
                            console.log(count, data.length);
                        }
                    } else {
                        startDateFilter = v.createdDate;
                        return false;
                    }
                });
                dataRp.ticket.push([miliseconds, seat]);
                dataRp.money.push([miliseconds, money]);
            }
            $('.TotalTicket').html('<span style="padding:10px;border:2px solid #084388;font-size: 20px;color:#fff;background: #4f6c8d">Tổng số vé : <strong>' + $.number(totalTicket) + " (Vé)</strong> . Tổng số tiền : <strong>" + $.number(totalMoney) + ' (VNĐ)' + '</strong></span>');
            return dataRp;
        }

        function setChart(dataNumberTicket, dataTotalMoney) {
            var dataset = [
                {label: "Số Vé", data: dataNumberTicket},
                {label: "Số Tiền", data: dataTotalMoney, yaxis: 2},
            ];
            var options= {
                series: {
//                    lines: {
//                        show: true
//                    },
//                    bars: {
//                        show: true
//                    },
//                    points: {
//                        radius: 3,
//                        fill: true,
//                        show: true
//                    }
                },
//                bars: {
//                    align: "center",
//                    barWidth : 36000000
//                },
                xaxis: {
                    mode: "time",
                    tickSize: [1, "day"],
                    tickLength: 0,
                    align: 'center',
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 10
                },
                yaxes: [{
                    axisLabel: "Số vé",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 3,
//                    tickFormatter: function (v, axis) {
//                        return v+ 'Vé';
//                    }
                },
                    {
                        position: "right",
                        axisLabel: "Change(%)",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabelPadding: 3
                    }
                ],
                legend: {
                    noColumns: 0,
                    labelBoxBorderColor: "#000000",
                    position: "nw"
                },
                grid: {
                    hoverable: true,
                    borderWidth: 2,
                    borderColor: "#633200",
                    backgroundColor: {colors: ["#ffffff", "#EDF5FF"]}
                },
                colors: ["#084388", "red"],
            };
            if (checkChartMonth)
                options = {
                    series: {
                    },
                    xaxis: {
                        mode: "time",
                        tickSize: [1, "month"],
                        tickLength: 0,
                        align: 'center',
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabelPadding: 10
                    },
                    yaxes: [{
                        axisLabel: "Số vé",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabelPadding: 3,
//                    tickFormatter: function (v, axis) {
//                        return v+ 'Vé';
//                    }
                    },
                        {
                            position: "right",
                            axisLabel: "Change(%)",
                            axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 12,
                            axisLabelFontFamily: 'Verdana, Arial',
                            axisLabelPadding: 3
                        }
                    ],
                    legend: {
                        noColumns: 0,
                        labelBoxBorderColor: "#000000",
                        position: "nw"
                    },
                    grid: {
                        hoverable: true,
                        borderWidth: 2,
                        borderColor: "#633200",
                        backgroundColor: {colors: ["#ffffff", "#EDF5FF"]}
                    },
                    colors: ["#084388", "red"],
                };
            if ($('#barChart').is(':checked')) {
                options.series.bars = {
                    show: true,
                    barWidth: 3600000,
                    align: "center",
                    order: 1
                };
            } else {
                options.series = {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        radius: 3,
                        fill: true,
                        show: true
                    }
                }
            }
            $.plot($("#chart"), dataset, options);
            $("#chart").UseTooltip();

        }

        var previousPoint = null, previousLabel = null;
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        $.fn.UseTooltip = function () {
            $(this).bind("plothover", function (event, pos, item) {
                if (item) {
                    if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                        previousPoint = item.dataIndex;
                        previousLabel = item.series.label;
                        $("#tooltip").remove();

                        var x = item.datapoint[0];
                        var y = item.datapoint[1];

                        var color = item.series.color;
                        var month = new Date(x).getMonth();

                        //console.log(item);

                        if (item.seriesIndex == 0) {
                            showTooltip(item.pageX,
                                item.pageY,
                                color,
                                "<strong>" + item.series.label + "</strong> : <strong>" + y + "</strong>(Vé)");
                        } else {
                            showTooltip(item.pageX,
                                item.pageY,
                                color,
                                "<strong>" + item.series.label + "</strong> : <strong>" + $.number(y) + "</strong></strong>(VND)");
                        }
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            });
        };

        function showTooltip(x, y, color, contents) {
            $('<div id="tooltip">' + contents + '</div>').css({
                position: 'absolute',
                display: 'none',
                top: y - 40,
                left: x - 120,
                border: '2px solid ' + color,
                padding: '3px',
                'font-size': '9px',
                'border-radius': '5px',
                'background-color': '#fff',
                'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                opacity: 0.9
            }).appendTo("body").fadeIn(200);
        }

        function showinfo(oject, type) {
            switch (type) {
                case 'table': {
                    $('#tb_report').show();
                    $('#chart_report').hide();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');
                    $('.khung_lich_ve').show();
                    $('#headerName').text('Danh sách thanh toán vé online');
                    break;
                }
                case 'chart': {
                    dataReport = makeData(result);
                    setChart(dataReport.ticket, dataReport.money);
                    $('#lineChart').click();
                    $('#tb_report').hide();
                    $('.khung_lich_ve').hide();
                    $('#chart_report').show();
                    $('.btn-activate').removeClass('btn-activate');
                    $(oject).addClass('btn-activate');
                    $('#headerName').text('Biểu đồ thanh toán vé online'+(checkChartMonth?'(Tháng)':'(Ngày)'));
                    break;
                }
                default: {
                    break;
                }
            }
        }
    </script>
    <script src="/public/javascript/jquery.printPage.js" type="text/javascript"></script>
    <script src="/public/javascript/printThis.js" type="text/javascript"></script>
    <script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
    <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>
    <!-- Charts Helper Demo Script -->
    {{--<script src="/public/javascript/chart/charts.helper.js"></script>--}}
    <!-- Charts Page Demo Script -->
    <script src="/public/javascript/chart/charts.js"></script>
    <!--  Flot (Charts) JS -->
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.selection.js"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.orderBars.js" type="text/javascript"></script>
    <!-- Add thư viện pdf -->
    <script src="https://github.com/niklasvh/html2canvas/releases/download/v0.5.0-beta4/html2canvas.min.js"></script>
    <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
@endsection
