{{--@extends('cpanel.template.help')--}}
{{--@section('title', 'help')--}}
{{--@section('content')--}}
<div style="margin:auto; width:600px">
    <h3>QUẢN TRỊ XE KHÁCH</h3>
    <h4> Menu trên</h4>
    <h4>Ở thanh menu trên, người dùng có thể sử dụng các chức năng sau : </h4>
    <img src="{{ asset('public/imghelp/images/Untitled.png', true) }}"/>
    <p>
        1- Thông báo từ hê thống về<br/>
        2- Thông tin tài khoản<br/>
        3- Đăng xuất<br/>
        4- Tìm kiếm
    </p>
    <h4>Thông báo</h4>
    Cho phép người quản trị xem những thông báo được gửi đến trang. Khi click "Xem hết thư" thì người dùng sẽ xem được
    toàn bộ danh sách các thông báo mà hệ thống gửi về.
    <h4>Thông tin tài khoản </h4>
    Bao gồm thông tin tài khoản và đổi mật khẩu cho tài khoản.
    <img src="{{ asset('public/imghelp/images/thong tin tai khoan.png', true) }}"/>

    Khi click xem thông tin tài khoản thì sẽ có thông tin của tài khoản:
    <img src="{{ asset('public/imghelp/images/thong tin tai khoan chi tiet.png', true) }}"/>
    <p>
        Người dùng muốn đổi mật khẩu thì thực hiện chọn "Đổi mật khẩu"</p>
    <img src="{{ asset('public/imghelp/images/doi mk.png', true) }}"/>
    <h4>Đăng xuất </h4>
    Cho phép người dùng đăng xuất khỏi hệ thống
    <h4>Tìm kiếm</h4>
    <p>Người dùng nhập từ muốn tìm kiếm vào thanh tìm kiếm trên thanh công cụ( góc trên cùng bên phải)</p>
    <img src="{{ asset('public/imghelp/images/timkiem.png', true) }}"/>
    <p>
        Kết quả hiển thị tìm kiếm được sẽ hiển thị như sau:</p>
    <img src="{{ asset('public/imghelp/images/timkiemct.png', true) }}"/>
    <h3>HOME ( TRANG CHỦ )</h3>
    Trang chủ là nơi cho người dùng biết tổng quan như báo cáo tổng quan, danh sách các chuyến đang lưu hành…
    <img src="{{ asset('public/imghelp/images/main.PNG', true) }}"/>
    <h4>BÁN VÉ</h4>
    Là chức năng bán vé của quản trị.
    <h4>Bán vé</h4>
    Để bán vé, thực hiện theo các bước sau:
    Bước 1: Chọn tuyến đi
    Bước 2: Chọn ngày đi. Sau khi chọn ngày và chọn tuyến
    hệ thống sẽ hiển thị ra tất cả các xe phù hợp.
    Bước 3: Chọn xe. Sau khi chọn xe sẽ chuyển sang màn hình nhập thông tin chi tiết cho chuyến đó.
    Sau bước này, người dùng sẽ chọn bán vé hành khách hay bán vé đồ:
    <img src="{{ asset('public/imghelp/images/banve.png', true) }}"/>
    <h4>Bán vé hành khách</h4>
    <h4>Thêm mới vé</h4>
    Để đặt vé cho hành khách, người dùng sẽ thực hiện các bước sau:
    Bước 1: Chọn "BÁN VÉ CHO HÀNH KHÁCH"
    Bước 2: Chọn chỗ ( Chọn những chỗ chưa có người đặt)
    Bước 3: Nhập thông tin hành khách, bao gồm : bến lên, bến xuống, có vé ăn hay không, tên, số điện thoại, chọn phương
    thức thanh toán…
    Bước 4: Click "giữ chỗ" nếu khách muốn giữ chỗ hoặc "thanh toán" nếu khách thanh toán ngay.
    <img src="{{ asset('public/imghelp/images/banve2.png', true) }}"/>
    <h4>Danh sách vé hành khách của chuyến:</h4>
    Người dùng có thể click vào ghế đã đặt sẽ hiển thị ra thông tin của ghế đó trên danh sách vé hành khách của chuyến
    đó. Trong đó trạng thái vé sẽ tương ứng với các tùy chọn:
    • Ghế giữ chỗ: sẽ được xem chi tiết vé, hủy vé, và thanh toán
    • Ghế đã bán vé: xem được chi tiết vé và hủy vé

    <p>Chi tiết 1 vé</p>
    <img src="{{ asset('public/imghelp/images/chitietve.png', true) }}"/>
    <h4> Bán vé gửi đồ</h4>
    <p>
        Người dùng nhập đầy đủ thông tin các trường thông tin về đồ cần gửi và người gửi. Nếu người gửi muốn thanh toán
        sẽ chọn phương thức thanh toán rồi click " thanh toán". Hoặc người nhận thanh toán thì click chọn "Người nhận
        thanh toán" và " Hoàn thành".
        <img src="{{ asset('public/imghelp/images/veguido.png', true) }}"/>
    <p>Sau khi bán vé đồ xong, người dùng có thể xem lại chi tiết của vé đồ trong trang danh sách vé của chuyến đó,
        click chọn chi tiết vé sẽ hiển thị toàn bộ thông tin vé:</p>
    <img src="{{ asset('public/imghelp/images/chitietveguido.png', true) }}"/>
    <h4>Danh sách vé:</h4>
    Người dùng có thể tìm kiếm vé hành khách và vé đồ bằng cách nhập : mã vé hoặc số điện thoại hoặc ngày tháng, biển số
    xe
    <h4>Danh sách vé hành khách</h4>
    <img src="{{ asset('public/imghelp/images/dsvehanhkhach.png', true) }}"/>
    <h4>Danh sách vé gửi đồ</h4>
    <img src="{{ asset('public/imghelp/images/dsveguidoo.png', true) }}"/>
    <h4>Danh sách vé thanh toán online</h4>
    <img src="{{ asset('public/imghelp/images/dsvettol.png', true) }}"/>
    <h4>Chi tiết 1 vé thanh toán online</h4>
    <img src="{{ asset('public/imghelp/images/chitietvettol.png', true) }}"/>
    <h4>Mã khuyến mại </h4>
    Người dùng có thể tìm kiếm mã khuyến mại bằng cách nhập dữ liệu vào trường "mã khuyến mại". Click "tìm mã". Tại đây
    người dùng cũng có thể tạo mã khuyến mại mới.
    <p>Giao diện tạo mã khuyến mại:</p>
    <img src="{{ asset('public/imghelp/images/km.png', true) }}"/>
    <p>Trên danh sách các mã khuyến mại, người dùng có thể chỉnh sửa mã khuyến mại hoặc xóa mã khuyến mại
    </p>
</div>
{{--@endsection--}}