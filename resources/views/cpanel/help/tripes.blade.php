{{--@extends('cpanel.template.help')--}}
{{--@section('title', 'help')--}}
{{--@section('content')--}}
<div style="width:600px; margin:auto">
    <h3> LẬP LỊCH XE CHẠY</h3>
    <b>Danh sách chuyến xe</b>
    <ul>
        <li>Người dùng có thể tìm kiếm chuyến xe của tất cả chuyến đã đi của nhà xe bằng cách <br> nhập thông tin các
            trường: lái xe hoặc phụ xe, biển số xe, thời gian. Click “tìm kiếm”
        </li>
        <li>Tại đây cũng có thể xuất file danh sách các chuyến xe dưới dạng PDF hoặc excel và có<br> chức năng in danh
            sách chuyến đi.
        </li>
    </ul>
    <img src="{{ asset('public/imghelp/images/ll1.png', true) }}"/>

    <p><b> Danh sách lịch chạy</b></p>
    <ul>
        <li>Người dùng có thể tìm kiếm lịch chạy theo: tuyến, biển số xe, loại lặp, ngày tháng.</li>
        <li>Thêm mới lịch thành công sẽ hiển thị toàn bộ thông tin của lịch chạy trong danh sách lịch<br> bao gồm: tên
            tuyến, biển số xe, thời gian xuất bến, trạng thái lặp, phụ xe,<br> tài xế. Tại đây người dùng có thể thay
            đổi lịch hoặc xóa lịch
        </li>
    </ul>
    <img src="{{ asset('public/imghelp/images/ll2.png', true) }}"/>
    <p>Trang chi tiết lịch chạy</p>
    <img src="{{ asset('public/imghelp/images/ll3.png', true) }}"/>
    <p>Sửa/Xóa lịch chạy</p>
    <img src="{{ asset('public/imghelp/images/ll4.png', true) }}"/>
    <p><b>Tạo lịch xe chạy</b></p>
    <p>&nbsp;&nbsp;Để tạo lịch chạy cần nhập thông tin các trường: tuyến đường, thời gian xuất bến, lái xe, phụ xe,<br>
        lặp lại lịch làm việc:</p>
    <ul>
        <li>Tuyến đường: danh sách các tuyến được tạo ở phần quản lý tuyến đường</li>
        <li>Thời gian xuất bến: giờ chạy sáng hay chiều</li>
        <li>Tài xế: chọn từ danh sách nhân viên của nhà xe</li>
        <li>Phụ xe: chọn từ danh sách nhân viên của nhà xe</li>
        <li>Lặp lại lịch làm việc: chọn thời gian lặp lại và chu kỳ lặp</li>
    </ul>
    <img src="{{ asset('public/imghelp/images/ll5.png', true) }}"/>
    <p>&nbsp;&nbsp;- Chỉnh sửa lặp lại lịch làm việc</p>
    <img src="{{ asset('public/imghelp/images/ll6.png', true) }}"/>
</div>
{{--@endsection--}}