{{--@extends('cpanel.template.help')--}}
{{--@section('title', 'help')--}}
{{--@section('content')--}}
<div style="width:600px; margin:auto">

    <h3><b> QUẢN LÝ NHÂN VIÊN</b></h3>
    <p> Danh sách nhân viên</p>
    <p>Người dùng có thể tìm thông tin của nhân viên bằng cách nhập các trường: họ tên, quê quán, số điện thoại, số
        chứng minh thư nhân dân. Click “tìm kiếm”</p>

    <img src="{{ asset('public/imghelp/images/nv1.png', true) }}"/>

    <p> &nbsp;&nbsp; Xem chi tiết một hồ sơ nhân viên</p>
    <img src="{{ asset('public/imghelp/images/nv2.png', true) }}">
    <p> &nbsp;&nbsp; Tại đây người dùng có thể chỉnh sửa hoặc xóa thông tin của nhân viên: </p>
    <img src="{{ asset('public/imghelp/images/nv3.png', true) }}">
    <p>&nbsp;&nbsp;Thêm mới nhân viên</p>
    <p>&nbsp;&nbsp;Người dùng chọn vai trò của nhân viên cần tạo: là người dùng thường, là lái xe, là kế toán, là nhân
        viên hành chính, là thanh tra, hay là quản trị. Từ đó hệ thống sẽ bật ra các form tương ứng với từng vài trò</p>
    <img src="{{ asset('public/imghelp/images/nv4.png', true) }}">

    <p><b>&nbsp;&nbsp;&nbsp;&nbsp;9.2 Thông báo</b></p>
    <p>&nbsp;&nbsp;Giao diện thông báo với danh sách các thông báo cho nhân viên</p>
    <img src="{{ asset('public/imghelp/images/nv5.png', true) }}">
    <p>&nbsp;&nbsp;Tạo thông báo cho nhân viên</p>
    <img src="{{ asset('public/imghelp/images/nv6.png', true) }}">

    <p><b>&nbsp;&nbsp;&nbsp;&nbsp;9.2 Quản lý nhóm quyền</b></p>
    <p>&nbsp;&nbsp;Giao diện quản lý nhóm quyền</p>
    <img src="{{ asset('public/imghelp/images/nv7.png', true) }}">
    <p>&nbsp;&nbsp;Xem chi tiết một nhóm quyền</p>
    <img src="{{ asset('public/imghelp/images/nv8.png', true) }}">
    <p>&nbsp;&nbsp;Thêm mới một nhóm quyền</p>
    <img src="{{ asset('public/imghelp/images/nv9.png', true) }}">
    <p>&nbsp;&nbsp;Chỉnh sửa một nhóm quyền</p>
    <img src="{{ asset('public/imghelp/images/nv10.png', true) }}">
</div>
{{--@endsection--}}