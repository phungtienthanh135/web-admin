{{--@extends('cpanel.template.help')--}}
{{--@section('title', 'help')--}}
{{--@section('content')--}}
<div style="width:600px; margin:auto">
    <h3><b> QUẢN LÝ KHÁCH HÀNG</b></h3>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;1. Danh sách khách hàng</p>
    <p>&nbsp;&nbsp;Giao diện danh sách khách hàng</p>
    <img src="{{ asset('public/imghelp/images/kh1.png', true) }}">
    <p>&nbsp;&nbsp;&nbsp;&nbsp;2. Phản hồi khách hàng</p>
    <p>&nbsp;&nbsp;Giao diện phản hồi khách hàng</p>
    <img src="{{ asset('public/imghelp/images/kh2.png', true) }}">
    <p>&nbsp;&nbsp;&nbsp;Thêm thông báo phản hồi cho khách hàng</p>
    <img src="{{ asset('public/imghelp/images/kh3.png', true) }}">
    <p>&nbsp;&nbsp;&nbsp;&nbsp;3. SMS chăm sóc khách hàng</p>
    <p>&nbsp;&nbsp;Giao diện SMS chăm sóc khách hàng</p>
    <img src="{{ asset('public/imghelp/images/kh4.png', true) }}">
    <p>&nbsp;&nbsp;Thêm thông báo SMS chăm sóc khách hàng</p>
    <img src="{{ asset('public/imghelp/images/kh5.png', true) }}">
</div>
{{--@endsection--}}