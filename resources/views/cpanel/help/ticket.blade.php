{{--@extends('cpanel.template.help')--}}
{{--@section('title', 'help')--}}
{{--@section('content')--}}
<div style="width:600px; margin:auto">
    <h3>&nbsp; BÁN VÉ</h3>
    <p>Là chức năng bán vé của quản trị</p>
    <p><b>&nbsp; Bán vé</b></p>
    <p>Để bán vé thực hiện các bước sau:</p>
    <p>Bước 1: Chọn tuyến đi</p>
    <p>Bước 2: Chọn ngày đi. Sau khi chọn ngày và chọn tuyến <br> Hệ thống sẽ hiển thị ra các xe phù hợp</p>
    <p>Bước 3: Chọn xe. Sau khi chọn xe sẽ chuyển sang màn hình nhập thông tin chi tiết cho chuyến đó. </p>
    <p>Sau bước này, người dùng sẽ chọn bán vé hành khách hay bán vé đồ:</p>
    <img src="{{ asset('public/imghelp/images/bv1.png', true) }}"/>

    <p><i>&nbsp;&nbsp; Bán vé khách hàng</i></p>
    <p>&nbsp;&nbsp;a. Thêm vé mới</p>
    <p>&nbsp;&nbsp;Để đặt vé cho hành khách, người dùng sẽ thực hiện các bước sau:</p>

    <p>Bước 1: Chọn “BÁN VÉ CHO HÀNH KHÁCH”</p>
    <p>Bước 2: Chọn chỗ ( Chọn những chỗ chưa có người đặt)</p>
    <p>Bước 3: Nhập thông tin hành khách, bao gồm : bến lên, bến xuống, có vé ăn hay không, tên, số điện thoại,<br> chọn
        phương thức thanh toán…</p>
    <p>Bước 4: Click “giữ chỗ” nếu khách muốn giữ chỗ hoặc “thanh toán” nếu khách thanh toán ngay.</p>
    <img src="{{ asset('public/imghelp/images/bv2.png', true) }}"/>

    <p>&nbsp;&nbsp;b. Danh sách vé hành khách của chuyến</p>
    <p>Người dùng có thể click vào ghế đã đặt sẽ hiển thị ra thông tin của ghế đó trên danh sách vé hành khách của
        chuyến đó.<br> Trong đó trạng thái vé sẽ tương ứng với các tùy chọn:</p>
    <ul>
        <li>Ghế giữ chỗ: sẽ được xem chi tiết vé, hủy vé, và thanh toán</li>
        <li>Ghế đã bán vé: xem được chi tiết vé và hủy vé</li>
    </ul>
    <p>Chi tiết một vé</p>
    <img src="{{ asset('public/imghelp/images/bv3.png', true) }}"/>
    <p><i>&nbsp;4.3.1.2 Bán vé gửi đồ</i></p>
    <p>&nbsp;&nbsp;Người dùng nhập đầy đủ thông tin các trường thông tin về đồ cần gửi và người gửi. Nếu người gửi muốn
        thanh toán <br>sẽ chọn phương thức thanh toán rồi click “ thanh toán”. Hoặc người nhận thanh toánthì click chọn
        “Người nhận thanh toán” <br> và “ Hoàn thành”.</p>
    <img src="{{ asset('public/imghelp/images/bv4.png', true) }}"/>
    <p>&nbsp;&nbsp;Sau khi bán vé đồ xong, người dùng có thể xem lại chi tiết của vé đồ trong trang gửi đồ</p>
    <p><b> Danh sách vé: </b></p>
    <p>Người dùng có thể tìm kiếm vé hành khách và vé đồ bằng cách nhập : mã vé hoặc số điện thoại <br> hoặc ngày tháng,
        biển số xe.</p>
    <p>a. Danh sách vé hành khách</p>
    <img src="{{ asset('public/imghelp/images/bv5.png', true) }}"/>
    <p>b.Danh sách vé gửi đồ</p>
    <img src="{{ asset('public/imghelp/images/bv6.png', true) }}"/>
    <p><b> Danh sách vé thanh toán online </b></p>
    <img src="{{ asset('public/imghelp/images/bv7.png', true) }}"/>
    <p>Chi tiết 1 vé thanh toán online</p>
    <img src="{{ asset('public/imghelp/images/bv8.png', true) }}"/>
    <p><b>&nbsp; Mã khuyến mại</b></p>
    <p>Người dùng có thể tìm kiếm mã khuyến mại bằng cách nhập dữ liệu vào trường “mã khuyến mại”. Click “tìm mã”.<br>.
        Tại đây người dùng cũng có thể tạo mã khuyến mại mới. Giao diện tạo mã khuyến mại:</p>
    <img src="{{ asset('public/imghelp/images/bv9.png', true) }}"/>
    <p>Trên danh sách các mã khuyến mại, người dùng có thể chỉnh sửa mã khuyến mại hoặc xóa mã khuyến mại</p>
</div>
{{--@endsection--}}