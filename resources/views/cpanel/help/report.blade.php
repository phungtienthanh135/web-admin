{{--@extends('cpanel.template.help')--}}
{{--@section('title', 'help')--}}
{{--@section('content')--}}
<div style="width:600px; margin:auto">
<PRE>
1.	Báo cáo chi tiết từ ngày tới ngày
    <img src="{{ asset('public/imghelp/images/reportDetailDayToDay.png', true) }}" />
2.	Báo cáo chi tiết doanh thu theo xe
    <img src="{{ asset('public/imghelp/images/reportDetailForNumberPlate.png', true) }}" />
3.	Báo cáo chi tiết doanh thu theo tuyến
    <img src="{{ asset('public/imghelp/images/reportDetailForRoute.png', true) }}" />
4.	Báo cáo chi tiết doanh thu theo nguồn
    <img src="{{ asset('public/imghelp/images/reportDetailForSource.png', true) }}" />
5.	Báo cáo tổng hợp thu chi
    <img src="{{ asset('public/imghelp/images/reportTotal.png', true) }}" />
</PRE>
</div>
{{--@endsection--}}