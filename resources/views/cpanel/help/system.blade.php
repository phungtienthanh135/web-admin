{{--@extends('cpanel.template.help')--}}
{{--@section('title', 'help')--}}
{{--@section('content')--}}
<div style="width:600px; margin:auto">
    <H4>Thông tin hệ thống</H4>
    <p>Giao diện thông tin hệ thống</p>
    <img src="{{ asset('public/imghelp/images/hethong.png', true) }}"/>
    <p>Màn hình hiển thị thông tin khi dùng thử</p>
    <img src="{{ asset('public/imghelp/images/manhinhdungthu.png', true) }}"/>
    <p>Chỉnh sửa thông tin hệ thống</p>
    <img src="{{ asset('public/imghelp/images/chinhsuahethong.png', true) }}"/>
    <h4>Cấu hình hệ thống</h4>
    <img src="{{ asset('public/imghelp/images/cauhinhhethong.png', true) }}"/>
    <h4>Hoạt động của nhân viên</h4>
    <p>Danh sách các hoạt động của nhân viên</p>
    <img src="{{ asset('public/imghelp/images/hoatdongnv.png', true) }}"/>
</div>
{{--@endsection--}}
