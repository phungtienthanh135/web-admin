{{--@extends('cpanel.template.help')--}}
{{--@section('title', 'help')--}}
{{--@section('content')--}}
<div style="width:600px; margin:auto;">
    <h3> GỬI ĐỒ</h3>
    <p><b>&nbsp;&nbsp; Danh sách gửi đồ</b></p>
    <p>Danh sách đồ được gửi được hiển thị như sau</p>
    <img src="{{ asset('public/imghelp/images/danhsachguido.PNG', true) }}"/>
    <p>Xem chi tiết đồ được gửi</p>
    <img src="{{ asset('public/imghelp/images/chitietguido.PNG', true) }}"/>
    <p>Tìm kiếm đồ theo ngày, theo tuyến xe</p>
    <img src="{{ asset('public/imghelp/images/timkiemdo.PNG', true) }}"/>
    <p><b>&nbsp;&nbsp; Gửi đồ mới</b></p>
    <p>Khách hàng có thêm gửi đồ mới tại giao diện gửi đồ mới như sau:</p>
    <p>Tại đây khách hàng điền đầy đủ các thông tin về đồ được gửi và sẽ biết được giá tiền tại tổng tiền</p>
    <img src="{{ asset('public/imghelp/images/guidomoi.PNG', true) }}"/>
    <p>Tiếp theo quay lại mục danh sách gửi để chọn đồ cần gửi thì tích chọn đồ cần gửi và chọn tiếp theo để chọn tuyến
        và xe</p>
    <img src="{{ asset('public/imghelp/images/chonxeguido.PNG', true) }}"/>
    <p>Tiếp theo chọn tiếp tục và ấn xác nhận để gửi đồ</p>
    <img src="{{ asset('public/imghelp/images/danhsachhangduochon.PNG', true) }}"/>
</div>
{{--@endsection--}}