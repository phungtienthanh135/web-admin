{{--@extends('cpanel.template.help')--}}
{{--@section('title', 'help')--}}
{{--@section('content')--}}
<div style="width:600px; margin:auto;">
    <h3>&nbsp;&nbsp; QUẢN LÝ ĐẠI LÝ</h3>
    <p><b>&nbsp;Danh sách đại lý</b></p>
    <p>Danh sách đại lý được liệt kê đầy đủ khi kích chọn vào quản lý đại lý, trong đó có các chức năng</p>
    <p>&nbsp;- Thêm đại lý</p>
    <p>&nbsp;- Chỉnh sửa đại lý</p>
    <p>&nbsp;- Tìm đại lý</p>
    <img src="{{ asset('public/imghelp/images/danhsachdaily.png', true) }}"/>
    <p><b>&nbsp;Thêm đại lý mới</b></p>
    <p>Thêm đại lý mới tại mục thêm đại lý, trong đó điền đầy đủ các thông tin như tên đại lý, số điện thoại,<br> tài
        khoản, mật khẩu, địa chỉ, email,... và chọn thêm đại lý để thêm một đại lý mới</p>
    <img src="{{ asset('public/imghelp/images/themdaily.PNG', true) }}"/>
    <p><b>&nbsp;Tìm kiếm đại lý</b></p>
    <p>Tại đây có thể tìm đại lý theo tên hoặc số điện thoại</p>
    <img src="{{ asset('public/imghelp/images/timdaily.PNG', true) }}"/>
    <p><b>&nbsp;Chỉnh sửa đại lý</b></p>
    <p>Chọn chỉnh sửa đại lý hoặc xóa</p>
    <img src="{{ asset('public/imghelp/images/chinhsuadaily.PNG', true) }}"/>
    <p>Sau khi chỉnh sửa đại lý thì ấn đồng ý để kết thúc chỉnh sửa</p>
    <img src="{{ asset('public/imghelp/images/suadaily.PNG', true) }}"/>
</div>
{{--@endsection--}}