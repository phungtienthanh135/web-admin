{{--@extends('cpanel.template.help')--}}
{{--@section('title', 'help')--}}
{{--@section('content')--}}
<div style="width:600px; margin:auto">
    <h4>Danh sách phương tiện</h4>
    <p>Người dùng có thể tìm kiếm phương tiện bằng cách nhập thông tin các trường :</br> biển số, loại xe, trạng thái
        của xe, số chỗ rồi click " tìm kiếm". Sẽ hiển thị toàn bộ danh sách xe của nhà xe</p>
    <img src="{{ asset('public/imghelp/images/dsphuongtien.png', true) }}"/>

    <p>Click vào 1 xe bất kỳ sẽ hiển thị chi tiết xe đó bao gồm: biển số, loại xe, số chỗ, nguyên giá, thời gian khấu
        hao, hạn mức khấu hao, và sơ đồ ghế</p>
    <img src="{{ asset('public/imghelp/images/chitietxe.png', true) }}"/>
    <p>Click vào tùy chọn người dùng có thể chỉnh sửa hoặc xóa thông tin của xe khỏi danh sách phương tiện</p>
    <img src="{{ asset('public/imghelp/images/dsphuongtien2.png', true) }}"/>
    <h4>Thêm mới phương tiện</h4>
    <p>B1. Người dùng nhập thông tin các trường: biển số, nguyên giá, ngày đăng ký, hạn mức khấu hao, thời gian khấu
        hao, giá trị còn lại, còn trường chọn loại xe sẽ được lấy từ danh sách các loại xe được tạo trên hệ thống</p>
    <img src="{{ asset('public/imghelp/images/themmoipt.png', true) }}"/>
    <p>B2. Thiết lập sơ đồ ghế
        Người dùng sẽ chọn 1 sơ đồ từ hệ thống đã tạo hoặc tạo mới sơ đồ bằng cách thêm sơ đồ. Tại đây, người dùng có
        thể thay đổi màu sắc các ghế tương ứng các vị trí ghế:
        • Màu vàng: tương ứng với cửa
        • Màu đỏ là ghế tài xế
        • Màu cam là ghế phụ xe
        • Màu xanh lá là ghế khách
        • Màu xanh lam là ghế giường nằm
        • Màu xanh da trời là nhà vệ sinh
        • Màu xám là lối đi
        Click "Cập nhật sơ đồ", sơ đồ ghế vừa tạo sẽ được lưu vào danh sách sơ đồ ghế. Người dùng vào "tùy chọn" để thay
        đổi thông tin hoặc xóa sơ đồ ghế
    </p>
    <img src="{{ asset('public/imghelp/images/dssodoge.png', true) }}"/>
    <p>B3. Sau khi nhập đầy đủ thông tin và cập nhật sơ đồ ghế xong, người dùng click "Lưu" để hoàn thành quá trình thêm
        mới phương tiện
        Chỉnh sửa thêm mới phương tiện
        Người dùng vào danh sách phương tiện chọn "tùy chọn", chọn sửa sẽ hiển thị thông tin người dùng thay đổi theo ý
        muốn
    </p>
    <img src="{{ asset('public/imghelp/images/chínhuathemmoipt.png', true) }}"/>
    <h4>Loại xe</h4>
    <h4>Danh sách loại xe</h4>
    <p>Người dùng có thể tìm kiếm xe theo các trường: tên xe, số chỗ trên danh sách các loại xe.</p>
    <img src="{{ asset('public/imghelp/images/loaixe.png', true) }}"/>
    <p>Tại đây, người dùng có thể vào "tùy chọn" để thay đổi nội dung của xe hoặc xóa loại xe đó khỏi danh sách xe
        Thêm mới loại xe
        Người dùng nhập các thông tin: tên xe, số chỗ, mô tả. Click "Thêm mới" để lưu thông tin của xe lên hệ thống
    </p>
    <img src="{{ asset('public/imghelp/images/themmoixe.png', true) }}"/>

</div>
{{--@endsection--}}