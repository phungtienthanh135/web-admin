{{--@extends('cpanel.template.help')--}}
{{--@section('title', 'help')--}}
{{--@section('content')--}}
<div style="width:600px; margin:auto">
    <h3><b> ĐIỀU HÀNH XE</b></h3>
    <h4><b> Điều hành xe</b></h4>
    <p>Người dùng có thể tìm kiếm xe theo các trường: tuyến, biển sổ, mã chuyến. Click "Tìm xe"</p>
    <p><i> Danh sách các xe xuất bến và đang lưu hành</i></p>
    <p>&nbsp;&nbsp;Đây là danh sách bao gồm toàn bộ thông tin của những xe đã xuất bến và đang đi trên hành trình của
        mình. Các xe này có thể thực hiện các chức năng: bán vé, điều chuyển xe, hủy chuyến, in phơi vé.</p>

    <img src="{{ asset('public/imghelp/images/nx1.png', true) }}">

    <p><i>Điều chuyển xe</i></p>
    <p>B1. Chọn xe cần điều chuyển. Click “điều chuyển xe” </br>
        B2. Nhập thông tin đê chọn xe cho phù hợp : thời gian, tuyến. Hệ thống tìm ra tất cả các xe có giờ chạy phù hợp
        với điều kiện lọc. </p>
    <img src="{{ asset('public/imghelp/images/nx2.png', true) }}"/>

    <p>B3: Người dùng chọn 1 xe trong danh sách xe đó. Có thể chọn 1 ghế hoặc chọn tất cả các ghế.</br> Click “ điều
        chuyển”. màu ghế ở xe cần chuyển sẽ chuyển sang màu xanh là ở trạng thái sẵn sang đặt. màu ghế ở xe được chuyển
        sẽ chuyển sang màu đỏ là ghế đã đặt.</p>

    <img src="{{ asset('public/imghelp/images/nx3.png', true) }}">
    <p>Trên danh sách những xe đã xuất bến và đang lưu hành, người dùng có thể thực hiện các chức năng:</p>
    <h4><b>5.2 Nhật trình xe</b></h4>
    <p> - Giao diện chức năng năng xem Nhật trình xe</p>
    <img src="{{ asset('public/imghelp/images/nx4.png', true) }}">
    <p>Chi tiết nhật trình xe</p>
    <img src="{{ asset('public/imghelp/images/nx5.png', true) }}">
</div>
{{--@endsection--}}