{{--@extends('cpanel.template.help')--}}
{{--@section('title', 'help')--}}
{{--@section('content')--}}
<div style="width:600px; margin:auto">
    <h3><b> QUẢN LÝ TUYẾN ĐƯỜNG</b></h3>

    <p><b> Danh sách tuyến</b></p>

    <p>Giao diện chính danh sách tuyến đường</p>

    <img src="{{ asset('public/imghelp/images/td1.png', true) }}">
    <p>1. Chỉnh sửa thông tin tuyến :</p>
    <img src="{{ asset('public/imghelp/images/td2.png', true) }}">
    <p>2. Thêm tuyến mới</p>
    <img src="{{ asset('public/imghelp/images/td3.png', true) }}">
    <p>3. Chỉnh sửa thông tin tuyến</p>
    <img src="{{ asset('public/imghelp/images/td5.png', true) }}">
    <p>4. Thêm bến đỗ</p>
    <img src="{{ asset('public/imghelp/images/td4.png', true) }}">

    <p><b>7.2 Danh sách điểm dừng</b></p>

    <p>Giao diện chính danh sách điểm dừng</p>

    <img src="{{ asset('public/imghelp/images/td5.png', true) }}">
    <p>1. Chi tiết điểm dừng</p>
    <img src="{{ asset('public/imghelp/images/td7.png', true) }}">
    <p>2. Thêm mới điểm dừng</p>
    <img src="{{ asset('public/imghelp/images/td8.png', true) }}">
</div>
{{--@endsection--}}