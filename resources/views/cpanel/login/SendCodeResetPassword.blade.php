@extends('cpanel.template.layout_login')

@section('title', 'Gửi OTP đặt lại mật khẩu')

@section('content')
    <form name="phpForm" action="{{action('LoginController@ForgotPassword', ['buoc' => 2])}}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="userName" value="{{$username}}">
        <table class="tb_thongbao">
            <tr>
                <td align="center">
                    <img src="{{ asset('public/images/logoANVUI.jpg', true) }}" class="__img_logo" alt="">
                    <hr class="__hr_login">
                    <div class="__login_title">
                        Quên mật khẩu
                    </div>
                    <div class="hterror">{{$msg}}</div>
                </td>
            </tr>
            <tr>
                <td>
                    <span>Chúng tôi vừa gửi mã xác nhận đến số điện thoại của tài khoản:{{$username}}</span>
                </td>
            </tr>
            <tr>
                <td>
                    <input placeholder="Mã xác thực" type="text" name="otpCode" id="otpCode" class="form-control" value="" required="required" >
                </td>
            </tr>
            <tr>
                <td>
                    <input placeholder="Mật khẩu mới" type="password" name="newPassword" id="newPassword" class="form-control" value="" required="required" >
                </td>
            </tr>
            <tr>
                <td  style="text-align:center"><INPUT class="btnDangNhap" type=submit value="XÁC NHẬN" name="submit"></td>
            </tr>
            <tr>
                <td  style="text-align:center"><INPUT class="btnDangNhap" type=button value="QUAY LẠI" onclick="back()"></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
    <script>
        function back() {
            window.location = "{{action('LoginController@ForgotPasswordDefaul')}}";
        }
    </script>

@endsection