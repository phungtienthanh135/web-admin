@extends('cpanel.template.layout_login')

@section('title', 'Gửi link đặt lại mật khẩu')

@section('content')

<form name="phpForm" method="post">
    <table class="tb_thongbao">
        <tr>
            <td align="center">
                <img src="{{ asset('public/images/logoANVUI.jpg', true) }}" class="__img_logo" alt="">
                <hr class="__hr_login">
                <div class="__login_title">
                    Quên mật khẩu
                </div>

            </td>
        </tr>
        <tr>
            <td>
                <span>Chúng tôi đã gửi liên kết để bạn đặt lại mật khẩu đến</span>
            </td>
        </tr>
        <tr>
            <td><a style="display:inline" class="forgetpass" href="#">dongdv1056@gmail.com</a></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td  style="text-align:center"><INPUT class="btnDangNhap" type=button value="QUAY LẠI" onclick="back()"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    <input type="hidden" name="hidden" value="login" />
</form>

<script>
    function back() {
        window.location = "{{action('LoginController@index')}}";
    }
</script>

@endsection