<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->
<head>

    <title>An Vui - Đăng Nhập</title>
    <!-- Meta -->
    <meta charset="UTF-8"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=vietnamese" rel="stylesheet">

    <!-- Icon -->
    <link rel="shortcut icon" href="/public/favicon.png">
    <!-- Bootstrap -->
    <link href="/public/bootstrap-3-full/css/bootstrap.min.css" rel="stylesheet"/>

    <!-- Gritter Notifications Plugin -->
    <!-- JQuery v1.8.2 -->
    <script src="/public/theme/scripts/plugins/system/jquery-1.8.2.min.js"></script>
    <!-- JQueryUI v1.9.2 -->
    <script src="/public/theme/scripts/plugins/system/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="/public/bootstrap-3-full/js/bootstrap.min.js"></script>

    <!-- Jquery Noty -->
    <link rel="stylesheet" href="/public/javascript/noty/lib/noty.css">
    <script src="/public/javascript/noty/lib/noty.js"></script>
    <script src='https://www.google.com/recaptcha/api.js?hl=vi'></script>
    <!-- Font Awesome Free 5.0.1 -->
    <link rel="stylesheet" href="/public/theme/css/fontawesome-all.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900|Open+Sans:300,600&display=swap" rel="stylesheet">
    {{--<script src='https://www.google.com/recaptcha/api.js'></script>--}}
    <style>
        .header{
            height:50px;
            padding: 5px 0;
            width: 100%;
            box-sizing: border-box;
            background: #19448a;
            position: fixed;
            text-align: center;
            top: 0px;
            z-index: 10;
        }
        .content{
            background: #eee;width: 100%;height: 100vh;overflow: scroll;
        }
        .box{
            background: #fff;
            padding: 20px;
            box-shadow: 5px 5px 5px #ddd;
        }
        .padding-bottom-10{
            padding-bottom:10px;
        }
        input{border-radius: 0!important;}
        .box-psd{
            margin: auto;
            width: 640px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px #444;
            background: #fff;
        }
        .input-text{
            width: 100%;
            height: 48px;
            line-height: 17px;
            font-family: 'Roboto';
            border-radius: 6px !important;
            margin: auto;
            border:1px solid #ccc;
            padding-left: 10px;
            font-size: 16px;
        }
        .button-action {
            background: #084388;
            border-radius: 4px;
            height: 60px;
            width: 100%;
            font-family: Montserrat;
            font-style: normal;
            font-weight: 500;
            font-size: 18px;
            line-height: 22px;
            text-align: center;
            color: #FFFFFF;
        }
        .btnlogin{
            background: #084388;
            color:#fff;
            cursor: pointer;
        }
        .btnlogin:hover{
            background: #0C54AB;
        }
        .box-login{
            margin-top: 150px;padding: 40px;text-align: center;margin-bottom: 20px
        }
        .underline{
            text-decoration: underline;
        }
        .g-recaptcha > div{
            margin: auto;
        }
        label{
            font-family: Montserrat;
            font-style: normal;
            font-weight: 500;
            font-size: 16px;
            line-height: 20px;
            color: #000000;
        }
        @media only screen and (max-width:600px){
            .box-psd{
                width: 96%;
            }
            .box-login {
                margin-top: 80px;
                padding: 30px 20px;
            }
            .input-text{
                width: 100%;
            }
            /* styles for browsers larger than 960px; */
        }
    </style>
</head>
<body>
<div class="header">
</div>
<div class="content" style="background: #eee">
    <div class="stepLogin stepLoginAll">
        <form action="{{ url('dang-nhap') }}" method="post" name="phpForm">
            <div class="box-psd box-login" style="">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="deviceToken" id="deviceToken"/>
                <input type="hidden" name="hidden" value="login" />
                <label for="txtUid" style="width: 100%;text-align: left">Tài khoản</label>
                <input type="text" class="input-text" name="uid" id="txtUid" required style="margin-bottom: 24px">
                <label for="txtPass" style="width: 100%;text-align: left">Mật khẩu</label>
                <input type="password" class="input-text" name="pass" id="txtPass" autocomplete="off" required style="margin-bottom: 16px">
                {{--<div>--}}
                    {{--<div class="g-recaptcha" data-sitekey="6Lf2FF8UAAAAAOeliwEgE5C3gDyuXHF-C9P1HkYc"></div>--}}
                    {{--<p class="captchaMessage text-center" style="color:red">&nbsp; {{session('msg')}}</p>--}}
                {{--</div>--}}
                <div>
                    <p class="captchaMessage text-center" style="color:red">{{session('msg')}}</p>
                </div>
                <input type="submit" class="input-text button-action checkCapcha" value="ĐĂNG NHẬP">
                <div style="margin-top: 8px">
                    <a href="#" data-step=".stepGetCode" style="width: 100%;text-align: right">Quên mật khẩu ?</a>
                </div>
            </div>
        </form>
        <div class="box-psd" style="padding: 20px;font-family: 'Roboto';font-size: 16px;;margin-bottom: 20px">
            <strong>THÔNG BÁO</strong><br>
            <p class="text-justify">Bạn đang sử dụng phần mềm quản lý điều hành bán vé thông minh do Công ty CP Công Nghệ AN VUI cung cấp. Sản phẩm được phát triển và liên tục cải tiến nhằm phục vụ các đơn vị vận tải quản lý khoa học và nâng cao năng lực cạnh tranh.
                <br>Chúng tôi luôn lắng nghe ý kiến góp ý của các bạn qua email: <strong>info@anvui.vn</strong> - hotline: <strong>1900 7034</strong>. Trân trọng cảm ơn!</p>
        </div>
        <div class="box-psd">
            <img src="/public/images/LoginAdmin.jpg" style="width: 100%;">
        </div>
    </div>
    <div class="stepGetCode stepLoginAll" style="display: none">
        <div class="box-psd box-login" style="">
            <div class="text-center input-text" style="margin-bottom: 10px;border: 0;line-height: 20px">Nhập tên đăng nhập</div>
            <input type="text" class="input-text" id="userNameGetCode" placeholder="Nhập tên đăng nhập." required autocomplete="off" style="margin-bottom: 10px">
            {{--<div>--}}
                {{--<div class="g-recaptcha" data-sitekey="6Lf2FF8UAAAAAOeliwEgE5C3gDyuXHF-C9P1HkYc"></div>--}}
                {{--<p class="captchaMessage text-center" style="color:red">&nbsp; {{session('msg')}}</p>--}}
            {{--</div>--}}
            <input type="submit" class="input-text btnlogin btnSendCode" value="GỬI YÊU CẦU QUÊN MẬT KHẨU">
            <div style="margin-top: 22px">
                <a href="#" data-step=".stepLogin" class="underline">Trở lại đăng nhập</a>
            </div>
        </div>
    </div>
    <div class="stepRePass stepLoginAll" style="display: none">
        <div class="box-psd box-login" style="">
            <div class="text-center input-text" style="margin-bottom: 10px;border: 0;line-height: 20px">Tin nhắn chứa mã xác nhận đang được gửi đến số điện thoại của bạn, vui lòng cập nhật mật khẩu mới</div>
            <input type="text" class="input-text" placeholder="Mã xác nhận" id="otpCode" autocomplete="off" style="margin-bottom: 10px">
            <input type="password" class="input-text" placeholder="Mật khẩu mới" id="newPass" autocomplete="off" style="margin-bottom: 10px">
            <input type="password" class="input-text" placeholder="Nhập lại mật khẩu" id="reNewPass" autocomplete="off" style="margin-bottom: 10px">
            <input type="hidden" id="phoneNumberUser" >
            {{--<div>--}}
                {{--<div class="g-recaptcha" data-sitekey="6Lf2FF8UAAAAAOeliwEgE5C3gDyuXHF-C9P1HkYc"></div>--}}
                {{--<p class="captchaMessage text-center" style="color:red">&nbsp; {{session('msg')}}</p>--}}
            {{--</div>--}}
            <input type="submit" class="input-text btnlogin btnRequestReset" value="GỬI YÊU CẦU QUÊN MẬT KHẨU">
            <div style="margin-top: 22px">
                <a href="#" data-step=".stepLogin" class="underline">Trở lại đăng nhập</a>
            </div>
        </div>
    </div>
</div>
<div class="footer">

</div>
<script>
    var usernameGetCode='';
    $("[data-step]").click(function(){
        step = $(this).data('step');
        showStep('.stepLoginAll',step);
    });
//    $('.checkCapcha').click(function(){
//        return checkCaptcha();
//    });
    $('.btnSendCode').click(function(){
        if($('#userNameGetCode').val()===''){
            notyMessage('Tài khoản không được bỏ trống','error');
            $('#userNameGetCode').focus();
            return false;
        }
//        if(checkCaptcha()){
//            return false;
//        }
        usernameGetCode = $('#userNameGetCode').val();
        $.ajax({
            url :'{{action('LoginController@ForgotPassword', ['buoc' => 1])}}',
            type : 'post',
            dataType :'json',
            data:{
                uid : $('#userNameGetCode').val(),
                _token : '{{ csrf_token() }}'
            },
            success : function(data){
                dataGetCode=data;
                if(data.code===200){
                    $('#phoneNumberUser').val(data.results.phoneNumber);
                    showStep('.stepLoginAll','.stepRePass');
                }else if(data['results']['error']['propertyName'] === 'USER_NAME_INVALID'){
                    notyMessage('Tài khoản không tồn tại!','error');
                    $('#phoneNumberUser').val('');
                }else{
                    $('#phoneNumberUser').val('');
                    notyMessage('Lỗi,vui lòng tải lại trang!','error');
                }
            },
            error : function () {
                notyMessage('Lỗi khi gửi yêu cầu,vui lòng load lại trang','error');
            }
        });
    });
    $('.btnRequestReset').click(function(){
       if($('#otpCode').val()===''){
           notyMessage('Vui lòng điền đầy đủ thông tin!','warning');
           $('#otpCode').focus();
           return false;
       }
        if($('#newPass').val()===''){
            notyMessage('Vui lòng điền đầy đủ thông tin!','warning');
            $('#newPass').focus();
            return false;
        }
        if($('#reNewPass').val()===''){
            notyMessage('Vui lòng điền đầy đủ thông tin!','warning');
            $('#reNewPass').focus();
            return false;
        }
        if($('#newPass').val()!==$('#reNewPass').val()){
            notyMessage('2 mật khẩu không trùng nhau!','warning');
            $('#reNewPass').focus();
            return false;
        }
        $.ajax({
            url :'{{action('LoginController@ForgotPassword', ['buoc' => 2])}}',
            type : 'post',
            dataType :'json',
            data:{
                phoneNumber : $('#phoneNumberUser').val(),
                userName : usernameGetCode,
                otpCode : $('#otpCode').val(),
                newPassword : $('#newPass').val(),
                _token : '{{ csrf_token() }}'
            },
            success : function(data){
                console.log(data);
                if(data.code===200){
                    showStep('.stepLoginAll','.stepLogin');
                    notyMessage('Đổi mật khẩu thành công','success');
                }else{
                    notyMessage('Lỗi,vui lòng tải lại trang!','error');
                }
            },
            error : function () {
                notyMessage('Lỗi khi gửi yêu cầu,vui lòng load lại trang','error');
            }
        });
    });
    function notyMessage(content,type,time){
        type = type!==undefined?type:'info';
        time = time!==undefined?time:3000;
        new Noty({
            type: type,
            theme: 'sunset',
            timeout: time,
            text: content
        }).show();
    }
    function checkCaptcha() {
        var v = grecaptcha.getResponse();
        if(v.length === 0)
        {
            $('.captchaMessage').html("You can't leave Captcha Code empty");
            return false;
        }
        else
        {
            $('.captchaMessage').html("&nbsp;");
            return true;
        }
    }
    function showStep(allHide,onlyShow){
        $('.captchaMessage').html("&nbsp;");
        $(allHide).hide();
        $(onlyShow).show();
    }
</script>
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase-messaging.js"></script>

<script>
    // Initialize Firebase
    // TODO: Replace with your project's customized code snippet
    var config = {
        apiKey: "AIzaSyDanMfIbFRjuYY0QcIc8A-yMH_0PtI7dlk",
        authDomain: "dobody-anvui.firebaseapp.com",
        databaseURL: "https://dobody-anvui.firebaseio.com",
        storageBucket: "dobody-anvui.appspot.com",
        messagingSenderId: "999268943503",
    };
    firebase.initializeApp(config);
    var database = firebase.database();
    console.log("database", database);
    var starCountRef = database.ref('trip/02GcWDPO3t9D');
    console.log("starCountRef", starCountRef);
    starCountRef.on('value', function(snapshot) {
        console.log("snapshot", snapshot.val());
    });
    const messaging = firebase.messaging();
    messaging.onMessage(function(payload) {
        console.log("Message received. ", payload);
        window.alert("ban nhan duoc tin nhan " + payload.title);
    });
    function requestPermission() {
        console.log('Requesting permission...');

        messaging.requestPermission().then(function() {
            console.log('Notification permission granted.');

        }).catch(function(err) {
            console.log('Unable to get permission to notify.', err);
        });
        // [END request_permission]
    }
    /*
    * Tạo mới token mỗi lần đăng nhập
    * */
    function getToken()
    {
        //Xóa token cũ rồi tạo mới
        messaging.getToken()
            .then(function(currentToken) {
                console.log('currentToken', currentToken);
                document.getElementById("deviceToken").value = currentToken;
            })
            .catch(function(err) {
                console.log('An error occurred while retrieving token. ', err);
            });
    }

    function deleteToken() {
        // [START delete_token]
        console.log('del');
        messaging.getToken()
            .then(function(currentToken) {
                messaging.deleteToken(currentToken)
                    .then(function() {
                        console.log('Deleted');
                        getToken();
                        // [END_EXCLUDE]
                    })
                    .catch(function(err) {
                        console.log('Unable to delete token. ', err);
                    });
                // [END delete_token]
            })
            .catch(function(err) {

            });

    }

    requestPermission();
    deleteToken();

</script>
</body>
</html>
