@extends('cpanel.template.layout_login')

@section('title', 'Quên mật khẩu')

@section('content')


    <form action="{{action('LoginController@ForgotPassword', ['buoc' => 1])}}" name="phpForm" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <table style="">
            <tr>
                <td align="center">
                    <img src="{{ asset('public/images/logoANVUI.jpg', true) }}" class="__img_logo" alt="">
                    <hr class="__hr_login">
                    <div class="__login_title">
                        Quên mật khẩu
                    </div>
                    <div class="hterror">{{session('msg')}}</div>
                </td>
            </tr>
            <tr>
                <td>
                    <input placeholder="Tài khoản đăng nhập" type="text" name="uid" id="uid" class="form-control" value="" required="required" >
                </td>
            </tr>
            <tr>
                <td  style="text-align:center"><INPUT class="btnDangNhap" type=submit value="TIẾP THEO" name="submit"></td>
            </tr>
            <tr>
                <td  style="text-align:center"><INPUT class="btnDangNhap" type=button value="QUAY LẠI" onclick="back()"></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                {{--<td  style="text-align:center">--}}
                    {{--<span>Bạn gặp sự cố?</span>--}}
                    {{--<a style="display:inline" class="forgetpass" href="#QuenMatKhau">Nhận trợ giúp</a>--}}
                {{--</td>--}}
            </tr>
        </table>
        {{--<input type="hidden" name="hidden" value="login" />--}}
    </form>

    <script>
        function back() {
            window.location = "{{action('LoginController@index')}}";
        }
    </script>

@endsection