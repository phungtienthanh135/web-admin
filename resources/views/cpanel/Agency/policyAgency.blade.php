@extends('cpanel.template.layout')
@section('title', 'Chính sách đại lý')
@section('content')
    <style>
        .text-center{text-align: center !important;}
        .li-root{padding-left: 5px;margin-bottom: 3px;}
        .li-root ul{display: none}
        .text-right{padding-right: 10px;text-align: right;padding-top: 5px;}
        .showHideItem{font-size: 15px;}
        .full-width{width:100%}
        .no-margin-bottom{margin-bottom: 0px}
        .no-margin{margin: 0px !important;}
        li.row-fuild{border-left:3px solid #002ca6;padding: 10px;background: #fff;}
        .label-span{padding-top: 5px;}
    </style>
    <div id="content">
        <div class="heading_top">
                <div class="pull-left span8">
                    <h3>Tạo chính sách</h3>
                </div>
        </div>
        <div class="separator bottom"></div>
        <div class="innerLR">
            <img style="padding-left: 50%" src="/public/images/loading/loading32px.gif" alt="" class="img-loading">
            <ul class="content-loading">

            </ul>
        </div>
    </div>

    {{--modal--}}

    <div class="modal modal-custom hide fade" id="addLevelForm">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Tạo Mới Cấp Đại Lý</h3>
        </div>
        <form action="{{action('AgencyController@postAddLevelAgency')}}" method="post">
            <div class="modal-body">
                <div class="form-horizontal row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtAgencyCode">Kiểu</label>
                        <div class="controls">
                            <select name="" id="">
                                @foreach($levels as $key =>$level)
                                    <option value="{{ $key }}">{{$level}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-horizontal row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtAgencyCode">Định mức</label>
                        <div class="controls">
                            <input autocomplete="off" class="span10" type="number" name="dinhmuc">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </form>
    </div>

    <script>

        var listPolicyAgency = {};
        var listLevelAgency ={}

        $(document).ready(function(){
            init();
//            $('body').on('click','.txtDatePicker',function(){
//                $(this).datepicker({
//                    dateFomat:"dd-mm-yy",
//                });
//            });

            $('.showHideItem').on('click',function () {
                var routeId = $(this).data('route-id');
                if($(this).parents('li').children('ul').is(':hidden')) {
                    $(this).parents('li').children('ul').html(generateHtmlFormSavePolicy(listLevelAgency, listPolicyAgency[routeId]));
                }
                $(this).parents('li').children('ul').slideToggle();
                if($(this).find('.icon').hasClass('fa-plus-square')){
                    $(this).find('.icon').addClass('fa-minus-square');
                    $(this).find('.icon').removeClass('fa-plus-square');
                }else{
                    $(this).find('.icon').removeClass('fa-minus-square');
                    $(this).find('.icon').addClass('fa-plus-square');
                }
                $('.txtDatePicker').datepicker({
                   dateFormat : 'dd-mm-yy'
               });
                return false;
            });

            $('body').on('click','.btn-send-form',function () {
               var data={};
               var flag=true;
               var form = $(this).parents('.form-send');
               data._token = '{{ csrf_token() }}';
               data.routeId = $(this).parents('.form-send').data('route-id');
               data.policyId = $(this).parents('.form-send').data('policy-id');
               data.formAction = $(form).data('action');
               data.applyDate = $(form).find('.applydateValue').val();
               if(data.applyDate==""){
                   Message("Thông báo","Vui Lòng chọn ngày áp dụng",'');
                   $(form).find('.applydateValue').focus();
                   return false;
               }
               data.commission=[];
               $(form).find('.commission').each(function(i,e){
                   var cms={};
                   cms.level = $(e).data('level');
                   cms.commissionType = $(e).find('select').val();
                   cms.commissionValue = $(e).find('input').val();
                   if(cms.commissionType==2){
                       cms.commissionValue=cms.commissionValue/100;
                   }
                   if(cms.commissionValue==""){
                       Message("Thông báo","Vui lòng nhập giá trị",'');
                       $(e).find('input').focus();
                       flag=false;
                       return false;
                   }
                   data.commission.push(cms);
               });
               if(flag==false){
                   return false;
               };
               {{--if(data.formAction == '{{ action('AgencyController@postUpdatePolicyAgency') }}'){--}}
                   {{--Message('Thông báo','chức năng cập nhật chưa được mở','');--}}
                   {{--return false;--}}
               {{--}--}}
                saveLoading(true,$(form));
               $.ajax({
                   url : data.formAction,
                   dataType : "json",
                   data:data,
                   type : "post",
                   success:function(result){
                       console.log(result);
                       if(result.code==200){
                           Message("Thông báo !"," Gửi yêu cầu thành công",'');
                           listPolicyAgency[result.results.agencyPricePolicy.routeId] = result.results.agencyPricePolicy;
                           saveLoading(false,$(form));
                       }else{
                           Message("Lỗi !","Thực thi hành động thất bại,vui lòng thử lại sau",'');
                           saveLoading(false,$(form));
                       }
                   },
                   error : function(error){
                       Message("Lỗi !","có lỗi xảy ra khi gửi yêu cầu,vui lòng thử lại",'');
                       saveLoading(false,$(this));
                   }
               });
            });


        });

        function init(){
            Loading(true,'img.img-loading','.content-loading');
            getPolicyAgency();
            $('.content-loading').html(generateHtmlListRoute(listPolicyAgency));
            console.log(listPolicyAgency,listLevelAgency);
            Loading(false,'img.img-loading','.content-loading');
        }

        function getPolicyAgency(){
            $.ajax({
                url : '{{ action('AgencyController@getListPolicyAgency') }}',
                type : 'get',
                dataType : 'json',
                data : {
                    _token :'{{ csrf_token() }}',
                },
                async :false,
                success : function(data){
                    if(data.code==200){
                        listPolicyAgency = data.results.listAgencyPolicy;
                        listLevelAgency = data.results.listLevelOfAgency;
                        Loading(false,'.img-loading-create-update','');
                    }else{
                        Message('Thông Báo','Gửi yêu cầu thất bại !','');
                    }
                },
                error : function(){
                    Message('Thông Báo','Lấy danh sách cấp thất bại! vui lòng tải lại trang ','');
                }
            });
        }
        function Loading(stt,classLoad,ClassContent){
            if(stt==true){
                $(classLoad).show();
                $(ClassContent).hide();
            }else{
                $(classLoad).hide();
                $(ClassContent).show();
            }
        }

        function saveLoading(stt,element) {
            if(stt){
                $(element).parents('li.root-item').find('.img-loading-create-update').show();
            }else{
                $(element).parents('li.root-item').find('.img-loading-create-update').hide();
            }
        }

        function generateHtmlListRoute(listPolicy){
            var html='';
            $.each(listPolicy,function(i,e){
                var actionform ='{{ action('AgencyController@postAddPolicyAgency') }}';
                if(e.commission != undefined){
                    actionform='{{ action('AgencyController@postUpdatePolicyAgency') }}';
                }
                html+='<li class="li-root root-item"><a class="showHideItem" href="#" data-route-id="'+i+'"><i class="icon far fa-plus-square"></i> '+e.routeName+'</a> <img style="width: 15px;display: none" src="/public/images/loading/loading32px.gif" alt="" class="img-loading-create-update">' +
                    '       <ul class="form-send" data-action="'+actionform+'" data-route-id="'+i+'" data-policy-id="'+e.policyId+'">' +
                    '        </ul>' +
                    '    <div class="clearfix"></div>' +
                    ' </li>'
            });
            return html;
        }

        function generateHtmlFormSavePolicy(listLevel,routeInfo){
            var commission = {};
            if(routeInfo.commission!=undefined){
                commission = routeInfo.commission;
            }
            var applyDate = routeInfo.applyDate == 0 ? new Date() : routeInfo.applyDate;
            var html='';
            html += '<li class="row-fuild li-root">' +
'                            <div class="span2 label-span">Ngày bắt đầu áp dụng (*)</div>' +
'                            <div class="span10">' +
'                                <div class="span1 no-margin"> </div>' +
'                                <div class="span10 no-margin">' +
'                                    <input type="text" required class="txtDatePicker no-margin applydateValue" value="'+formatDate(applyDate)+'">' +
'                                </div>' +
'                            </div>' +
'                            <div class="clearfix"></div>' +
'                        </li>' +
'                        <li class="row-fuild  li-root">' +
'                            <div class="span2">Chiết khấu (*)</div>' +
'                            <div class="span10">' +
                                generateHtmlListCommission(listLevel,commission)+
'                                <div class="span1 no-margin"> </div>' +
'                                <div class="span10 no-margin">' +
'                                    <button class="btn btn-warning full-width span2 btn-send-form">Lưu</button>' +
'                                </div>' +
'                                <div class="clearfix"></div>' +
'                            </div>' +
'                            <div class="clearfix"></div>' +
'                        </li>';
            return html;
        }

        function generateHtmlListCommission(listLevel,listCommission){
            var html='';
            $.each(listLevel,function(i,e){
                var commissionInfo = $.grep(listCommission,function (ic) {
                   return e.level==ic.level;
                });
                if(commissionInfo.length>0){
                    commissionInfo = commissionInfo[0];
                }else{
                    commissionInfo = [];
                }
                html+='<div class="span1 text-right no-margin">Cấp '+e.level+'</div>' +
                    '       <div class="input-append commission" data-level="'+e.level+'">' +
                    '           <input required value="'+(commissionInfo.commissionType!=2?commissionInfo.commissionValue:commissionInfo.commissionValue*100)+'" style="width:145px" name="" type="number" min="1" placeholder="Bắt buộc nhập">' +
                    '           <select class="btn">' +
                    '               <option value="1" '+(commissionInfo.commissionType==1?"selected":"")+'>VNĐ</option>' +
                    '               <option value="2" '+(commissionInfo.commissionType==2?"selected":"")+'>%</option>' +
                    '           </select>' +
                    '       </div>' +
                    ' <div class="clearfix"></div>';
            });
            return html;
        }

        function formatDate(int) {
            var date = new Date(int);

            var dd = date.getDate();
            var mm = date.getMonth()+1; //January is 0!

            var yyyy = date.getFullYear();
            if(dd<10){
                dd='0'+dd;
            }
            if(mm<10){
                mm='0'+mm;
            }
            return dd+'-'+mm+'-'+yyyy;
        }
    </script>

@endsection
