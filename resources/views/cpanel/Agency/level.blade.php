@extends('cpanel.template.layout')
@section('title', 'Định nghĩa cấp đại lí')
@section('content')
    <style>
        .text-center{text-align: center !important;}
    </style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3 class="">Danh Sách Cấp Đại Lý</h3>
                </div>
            </div>
        </div>
        <div class="separator bottom"></div>
        <div class="innerLR">
            <div class="row-fuild">
                <img style="padding-left: 50%" src="/public/images/loading/loading32px.gif" alt="" class="img-loading">
                <table class="table table-striped content-loading" style="display: none">
                    <thead>
                        <tr>
                            <th style="width:50px">STT</th>
                            <th class="text-center">Cấp Đại Lý</th>
                            <th class="text-center">Định Mức</th>
                            <th class="text-center">Loại</th>
                            <th class="text-center">Ngày bắt đầu áp dụng</th>
                            <th class="text-center">Tùy chọn</th>
                        </tr>
                    </thead>
                    <tbody id="tableListLevelAgency">
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="center">
                                <a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                    <ul class="onclick-menu-content">

                                        <li>
                                            <button class="btnUpdateAgency" value="">Sửa</button>
                                        </li>
                                        <li>
                                            <button value="">Xóa</button>
                                        </li>
                                    </ul>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6" style="opacity: 0.5"><div id="addLevel" class="btn"><i class="fa fa-plus"></i> Thêm cấp mới</div></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    {{--modal--}}

    <div class="modal modal-custom hide fade" id="LevelForm">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="title-model">Tạo Mới Cấp Đại Lý</h3>
        </div>
        <form action="" method="post" id="form-add-edit-level">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="levelAgencyId">
            <div class="modal-body">
                <div class="form-horizontal row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtAgencyCode">Số cấp</label>
                        <div class="controls">
                            <input autocomplete="off" class="span10" type="text" name="level" readonly data-validation="required number" data-validation-error-msg-required="Cấp không được bỏ trống" data-validation-error-msg-number="Cấp phải là số" data-validation-allowing="float" placeholder="Nhập số VD : 1">
                        </div>
                    </div>
                </div>
                <div class="form-horizontal row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtAgencyCode">Kiểu</label>
                        <div class="controls">
                            <select name="conditionAchieveType" id="">
                                @foreach($levels as $key =>$level)
                                <option value="{{ $key }}">{{$level}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-horizontal row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtAgencyCode">Định mức</label>
                        <div class="controls">
                            <input autocomplete="off" class="span10"  data-validation="required number" data-validation-error-msg-required="Định mức không được bỏ trống" data-validation-error-msg-number="Định mức phải là số" data-validation-allowing="float" type="text" name="achieveValue">
                        </div>
                    </div>
                </div>
                <div class="form-horizontal row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtAgencyCode">Ngày bắt đầu áp dụng</label>
                        <div class="controls">
                            <input autocomplete="off" class="span10 dateTimePicker" type="text" name="applyDate" placeholder="dd-mm-yy">
                        </div>
                    </div>
                </div>
                <div class="form-horizontal row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtAgencyCode"> </label>
                        <div class="controls">
                            <input type="submit" class="btn btn-warning actionLevelAgency" style="width:200px" value="Lưu">
                            <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Hủy</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </form>
    </div>
    <script>

        var listLevel = [];
        var urlAddLevel = '{{action('AgencyController@postAddLevelAgency')}}';
        var urlEditLevel = '{{action('AgencyController@postEditLevelAgency')}}';
        $(document).ready(function(){
            init();

//            event
            $('#addLevel').click(function(){
                //cập nhật title
                $('#title-modal').html('TẠO MỚI CẤP ĐẠI LÝ');
                //cập nhật action form
                $('#form-add-edit-level').attr('action',urlAddLevel);
//                reset input
                $("[name='conditionAchieveType']").val('');
                $("[name='achieveValue']").val('');
                $("[name='applyDate']").val('');
//                cập nhật level
                var lengthLLV = listLevel.length;
                if(lengthLLV>0){
                    $("[name='level']").val(listLevel[lengthLLV-1].level+1)
                }else{
                    $("[name='level']").val(1);
                }
                $('#LevelForm').modal('show');
            });

            $('body').on('click','.btnUpdateLevelAgency',function(){
                var level = $(this).val();
                var levelInfo =  $.grep(listLevel,function(i,e){
                    return i.level == level;
                });
                if(levelInfo.length>0){
                    levelInfo=levelInfo[0];
                }
                if(levelInfo.level!=undefined){
                    console.log(levelInfo);
                    $('#form-add-edit-level').attr('action',urlEditLevel);
                    $('#title-modal').html('CẬP NHẬT CẤP ĐẠI LÝ');
                    $("[name='level']").val(levelInfo.level);
                    $("[name='levelAgencyId']").val(levelInfo.levelAgencyId);
                    $("[name='conditionAchieveType']").val(levelInfo.conditionAchieveType).change();
                    $("[name='achieveValue']").val(levelInfo.achieveValue);
                    $("[name='applyDate']").datepicker('setDate',new Date(levelInfo.applyDate));
                    $('#LevelForm').modal('show');
                }
            });

            $('body').on('click','.deleteLevel',function(){
                Message('Thông báo!','Chức năng chưa được mở','');
            });

            $('.actionLevelAgency').on('click',function(){
                $.ajax({
                   url : $('#form-add-edit-level').attr('action'),
                   type : 'post',
                   dataType : 'json',
                   data :{
                       _token : $("[name='_token']").val(),
                       level : $("[name='level']").val(),
                       conditionAchieveType : $("[name='conditionAchieveType']").val(),
                       achieveValue : $("[name='achieveValue']").val(),
                       applyDate : $("[name='applyDate']").val(),
                       levelAgencyId : $("[name='levelAgencyId']").val(),
                   },
                    beforeSend:function(){
                       $('.actionLevelAgency').css('cursor','progress');
                    },
                    success : function(data){
                       console.log(data);
                       if(data.code==200){
                           Message('Thông báo','Gửi yêu cầu thành công','');
                           init();
                           $('#LevelForm').modal('hide');
                       }else{
                           if(data.code == 'CLIENT'){
                               Message('Lỗi',data.results.message,'');
                               $("[name='"+data.results.field+"']").focus();
                           }else{
                               Message('Lỗi',data.results.error.message,'');
                           }
                       }
                    },
                    error : function(err){
                        Message('Lỗi','Gửi yêu cầu thất bại!','');
                    }
                });
                return false;
            });
        });

        function init(){
            Loading(true);
            getListLevel();
            $('#tableListLevelAgency').html(generateListLevel());
            $('.dateTimePicker').datepicker({dateFormat:"dd-mm-yy"});
            Loading(false);
        }

        function getListLevel(){
            $.ajax({
                url : '{{ action('AgencyController@getListLevelAgency') }}',
                type : 'get',
                dataType : 'json',
                data : {
                    _token :'{{ csrf_token() }}',
                },
                async :false,
                success : function(data){
                    if(data.code==200){
                        //Message('Thông Báo',data.results.message,'');
                        listLevel = data.results.listLevelOfAgency;
                    }else{
                        Message('Thông Báo','Gửi yêu cầu thất bại !','');
                    }
                },
                error : function(){
                    Message('Thông Báo','Lấy danh sách cấp thất bại! vui lòng tải lại trang ','');
                }
            });
        }

        function generateListLevel(){
            var html='';
            $.each(listLevel,function(e,i){
                html += '<tr>';
                html += '<td>' + (e+1) +'</td>';
                html += '<td class="center"> Cấp ' + i.level +'</td>';
                html += '<td class="center">' + $.number(i.achieveValue) +'</td>';
                html += '<td class="center">' + getTypeLevel(i.conditionAchieveType) +'</td>';
                html += '<td class="center">'+ new Date(i.applyDate).toLocaleDateString()+'</td>';
                html = html+ '<td class="center">' +
                    '<a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>' +
                    '<ul class="onclick-menu-content">' +
                    '   <li>' +
                    '       <button class="btnUpdateLevelAgency" value="'+i.level+'">Sửa</button>' +
                    '   </li>' +
                    '   <li>' +
                    '       <button class="deleteLevel" value="'+i.level+'">Xóa</button>' +
                    '   </li>' +
                    '</ul>' +
                    '</a>' +
                    '</td>';
                html += '</tr>';
            });
            return html;
        }

        function getTypeLevel(i){
            switch(i){
                case 1 : return "Số ghế bán";
                case 2 : return "Tiền bán vé";
                default : return "_";
            }
        }

        function Loading(stt){
            if(stt==true){
                $('img.img-loading').show();
                $('.content-loading').hide();
            }else{
                $('img.img-loading').hide();
                $('.content-loading').show();
            }
        }
    </script>

@endsection
