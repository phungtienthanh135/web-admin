@extends('cpanel.template.layout')
@section('title', 'Danh sách đại lý')
@section('content')
    <?php

    function getGroupPermissionName($id, $list)
    {
        for ($i = 0; $i < count($list); $i++) {
            if ($id == $list[$i]['groupPermissionId']) {
                return $list[$i]['groupPermissionName'];
            }
        }
        return "";
    }

    ?>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Danh Sách Đại Lý</h3></div>
            </div>
        </div>
        <div class="separator bottom"></div>
        <div class="innerLR">
            <div class="row-fluid">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="row-fluid">
                <div class="control-box">
                    <h4>Tìm đại lý</h4>
                    <div class="row-fluid">
                        <form action="">
                            <div class="span3">
                                <label for="name_customer">Tên đại lý</label>
                                <input autocomplete="off" type="text" name="fullName" id="fullName"
                                       value="{{request('fullName')}}">
                            </div>
                            <div class="span3">
                                <label for="phone_customer">Số điện thoại</label>
                                <input autocomplete="off" type="text" name="phoneNumber" id="phoneNumber"
                                       value="{{request('phoneNumber')}}">
                            </div>

                            <div class="span2">
                                <label class="separator hidden-phone" for="add"></label>
                                <button class="btn btn-info btn-flat-full hidden-phone" id="search">TÌM ĐẠI LÝ</button>
                                <button class="btn btn-info btn-flat visible-phone" id="search">TÌM ĐẠI LÝ</button>
                            </div>
                            <div class="span2">
                                <label class="separator hidden-phone" for="add"></label>
                                <button data-toggle="modal" data-target="#add_agency" type="button"
                                        class="btn btn-warning btn-flat-full hidden-phone" id="add">THÊM
                                    ĐẠI LÝ
                                </button>
                                <button data-toggle="modal" data-target="#add_agency" type="button"
                                        class="btn btn-warning btn-flat visible-phone" id="add">THÊM TÌM
                                    ĐẠI LÝ
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row-fluid">

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Mã đại lý</th>
                        <th>Tên đại lý</th>
                        <th>Cấp độ</th>
                        <th>Tài khoản</th>
                        <th>Điện thoại</th>
                        <th>Địa chỉ</th>
                        <th>Công nợ</th>
                        <th>Hạn mức</th>
                        <th class="center">Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>

                    @php($stt= RECORD_PER_PAGE*($page-1))
                    @forelse($result as $row)
                        @php($stt++)
                        <tr>
                            <td>{{$stt}}</td>
                            <td>{{@$row['agencyCode']}}</td>
                            <td>{{$row['fullName']}}</td>
                            <th>{{ $row['listAgency'][0]['level'] }}</th>
                            <td>{{@$row['userName']}}</td>
                            <td>{{@$row['phoneNumber'] != '' ? '+'.$row['stateCode'].$row['phoneNumber'] : '-'}}</td>
                            <td>{{$row['address']}}</td>
                            @php
                                $companyId= session('companyId');
                                  $listAgency =  array_where($row['listAgency'],function ($item) use ($companyId){
                                        return $item['companyId'] == $companyId;
                                    });
                                $commission = head($listAgency)['commission']*100;
                                $debtLimit = head($listAgency)['debtLimit'];
                                $currentDebt = head($listAgency)['currentDebt'];
                            @endphp
                            <td>{{ number_format($currentDebt) }}</td>
                            <td>{{ $debtLimit<10000000000000?number_format($debtLimit):$debtLimit}}</td>
                            <td class="center">
                                <a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                    <ul class="onclick-menu-content">

                                        <li>
                                            <button class="btnUpdateAgency" data-fullname="{{$row['fullName']}}"
                                                    data-commission="{{$commission}}"
                                                    data-phone="0{{@$row['phoneNumber']}}" data-email="{{$row['email']}}" data-current-debt="{{ number_format($currentDebt) }}"
                                                    data-address="{{$row['address']}}"
                                                    data-debtLimit="{{$debtLimit}}"
                                                    data-groupPermissionId="{{@$row['groupPermissionId']}}" data-agencycode="{{@$row['agencyCode']}}" data-level="{{ $row['listAgency'][0]['level'] }}"
                                                    value="{{$row['userId']}}">Sửa
                                            </button>

                                        <!-- onclick="$('#txtUpdateAgencyId').val($(this).val());
                                                    $('#txtUpdateCommission').val({{$commission}});" -->
                                        </li>
                                        <li>
                                            <button data-toggle="modal" data-target="#modal_delete"
                                                    value="{{$row['userId']}}"
                                                    onclick="$('#txtDeleteAgencyId').val($(this).val());">Xóa
                                            </button>
                                        </li>
                                    </ul>
                                </a>
                            </td>
                        </tr>
                        <tr class="info" id="chitiet_{{$row['userId']}}" style="display: none">
                            <td colspan="6">
                                <table class="tttk" cellspacing="20" cellpadding="4">
                                    <tbody>
                                    <tr>
                                        <th>HỌ TÊN</th>
                                        <td>
                                            {{$row['fullName']}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>SỐ ĐIỆN THOẠI</th>
                                        <td>{{@$row['phoneNumber'] != '' ? '+'.$row['stateCode'].$row['phoneNumber'] : '-'}}</td>
                                    </tr>
                                    <tr>
                                        <th>ĐỊA CHỈ</th>
                                        <td>{{@$row['address']}}</td>
                                    </tr>
                                    <tr>
                                        <th>NHÓM QUYỀN</th>
                                        <td>{{ getGroupPermissionName(@$row['groupPermissionId'],$GroupPermissions)}}</td>
                                    </tr>
                                    <tr>
                                        <th>EMAIL</th>
                                        <td>{{@$row['email']}}</td>
                                    </tr>
                                    <tr>
                                        <th>TÀI KHOẢN</th>
                                        <td>{{@$row['userName']}}</td>
                                    </tr>
                                    <tr>
                                        <th>MẬT KHẨU</th>
                                        <td>**********</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td colspan="1">
                                <div class="avatar">
                                    <img src="{{$row['avatar']}}">
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6" class="center">Hiện không có dữ liệu</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                @include('cpanel.template.pagination-without-number',['page'=>$page])
            </div>
        </div>
    </div>

    <div class="modal modal-custom hide fade" id="add_agency">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Tạo Mới Đại Lý</h3>
        </div>
        <form action="{{action('AgencyController@postAdd')}}" method="post">
            <div class="modal-body">
                <div class="form-horizontal row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtAgencyCode">Mã Đại lý</label>
                        <div class="controls">
                            <input autocomplete="off" class="span12" type="text" name="agencyCode">
                        </div>
                    </div>
                </div>
                <div class="form-horizontal row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtFullName">Tên đại lý</label>
                        <div class="controls">
                            <input autocomplete="off" data-validation="required"
                                   data-validation-error-msg="Vui lòng nhập tên đại lý"
                                   class="span12" type="text" name="fullName">
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span8">
                        <div class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label" for="txtPhoneNumber">Số điện thoại</label>
                                <div class="controls">
                                    <input class="span12 fomatPhoneNumber" type="tel" name="phoneNumber" id="txtPhoneNumberAdd"  {{--data-validation="required" data-validation-error-msg="Vui lòng nhập số điện thoại"--}}>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="form-inline">
                            <label for="txtCommission" style="padding-top:5px;text-align: right;padding-right: 5px;" class="span5">Cấp</label>
                            <select name="level" id="levelagc" class="span7">
                                @foreach($listLevel as $level)
                                    <option value="{{$level['level']}}">{{$level['level']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-horizontal row-fluid">

                    <div class="control-group">
                        <label class="control-label" for="txtDebtLimit">Hạn mức</label>
                        <div class="controls">
                            <input class="span12 MoneyInputFormat" type="text" name="debtLimit">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="txtCurrenDebt">Công nợ</label>
                        <div class="controls">
                            <input class="span12 " type="text" name="txtCurrenDebt" id="txtCurrenDebt" disabled value="0">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="txtPassword">Tài khoản</label>
                        <div class="controls">
                            <input data-validation-error-msg="Vui lòng nhập tài khoản" data-validation="required"
                                   class="span12" type="text" name="userName">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="txtPassword">Mật khẩu</label>
                        <div class="controls">
                            <input data-validation-error-msg="Vui lòng nhập mật khẩu" data-validation="required"
                                   class="span12" type="password" name="password" id="txtPassword">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="txtConfirmationPassword">Xác nhận mật khẩu</label>
                        <div class="controls">
                            <input data-validation-error-msg-required="Vui lòng nhập lại mật khẩu chính xác"
                                   data-validation-error-msg-confirmation="Mật khẩu xác nhận không chính xác"
                                   data-validation="confirmation required"
                                   data-validation-confirm="password" class="span12" type="password"
                                   id="txtConfirmationPassword">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="txtEmail">Địa chỉ email</label>
                        <div class="controls">
                            <input data-validation-error-msg="Địa chỉ email không hợp lệ" type="email" name="email"
                                   class="span12"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="txtAddress">Địa chỉ</label>
                        <div class="controls">
                            <textarea name="address" class="span12" rows="4"></textarea>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="groupPermissionId">Nhóm quyền</label>
                        <div class="controls">
                            <select autocomplete="off" class="span12" name="groupPermissionId">
                                @foreach($GroupPermissions as $groupPermission)
                                    <option value="{{$groupPermission['groupPermissionId']}}">{{$groupPermission['groupPermissionName']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row-fluid">
                    <div class="span9">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button class="btn btn-warning btn-flat-full">THÊM MỚI</button>
                    </div>
                    <div class="span3">
                        <a data-dismiss="modal"
                           class="btn btn-default btn-flat no_border bg_light">HUỶ</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- End Content -->
    <div class="modal hide fade" id="modal_delete" style="width: 250px; margin-left: -10%;margin-top: 10%;">
        <div class="modal-body center">
            <p>Bạn có chắc muốn xoá?</p>
        </div>
        <form action="{{action('AgencyController@updateOrDelete')}}" method="post">
            <div class="modal-footer">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="userId" value="" id="txtDeleteAgencyId">
                <input type="hidden" name="option" value="2">
                <a style="cursor: pointer" data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
            </div>
        </form>
    </div>
    <div class="modal hide fade modal-custom" id="modal_update">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Sửa Thông Tin Đại Lý</h3>
        </div>
        <form action="{{action('AgencyController@updateOrDelete')}}" method="post">
            <div class="modal-body">
                <div class="form-horizontal row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtAgencyCodeUpdate">Mã Đại lý</label>
                        <div class="controls">
                            <input autocomplete="off" class="span12" id="txtAgencyCodeUpdate" type="text" name="agencyCode">
                        </div>
                    </div>
                </div>
                <div class="form-horizontal row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtFullName">Tên đại lý</label>
                        <div class="controls">
                            <input autocomplete="off" data-validation="required"
                                   data-validation-error-msg="Vui lòng nhập tên đại lý"
                                   class="span12 txtFullName" type="text" name="fullName">
                        </div>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span8">
                        <div class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label" for="txtPhoneNumber">Số điện thoại</label>
                                <div class="controls">
                                    <input class="span12 fomatPhoneNumber" type="tel"
                                           name="phoneNumber">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="form-inline">
                            <label for="txtCommission" style="padding-top:5px;text-align: right;padding-right: 5px;" class="span5">Cấp</label>
                            <select name="level" id="level" class="span7">
                                @foreach($listLevel as $level)
                                    <option value="{{$level['level']}}">{{$level['level']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-horizontal row-fluid">

                    <div class="control-group">
                        <label class="control-label" for="txtDebtLimit">Hạn mức</label>
                        <div class="controls">
                            <input type="text" name="debtLimit"
                                   class="span12 txtDebtLimit MoneyInputFormat"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="txtCurrenDebtUpdate">Công nợ</label>
                        <div class="controls">
                            <input class="span12 MoneyInputFormat" type="text" name="txtCurrenDebtUpdate" id="txtCurrenDebtUpdate" disabled>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="txtEmail">Địa chỉ email</label>
                        <div class="controls">
                            <input data-validation-error-msg="Địa chỉ email không hợp lệ" type="email" name="email"
                                   class="span12 txtEmail"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="txtAddress">Địa chỉ</label>
                        <div class="controls">
                            <textarea name="address" class="span12 txtAddress" rows="4"></textarea>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="groupPermissionId">Nhóm quyền</label>
                        <div class="controls">
                            <select autocomplete="off" class="span12 groupPermissionId" name="groupPermissionId">
                                @foreach($GroupPermissions as $groupPermission)
                                    <option value="{{$groupPermission['groupPermissionId']}}">{{$groupPermission['groupPermissionName']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="control-group">

                        <div class="controls">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="userId" value="" class="txtUserId">
                            <input type="hidden" name="option" value="1">
                            <div class="row-fluid">
                                <div class="span9">
                                    <button class="btn btn-warning btn-flat-full">CẬP NHẬT</button>
                                </div>
                                <div class="span3">
                                    <a data-dismiss="modal"
                                       class="btn btn-default btn-flat no_border bg_light">HUỶ</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
            </div>
        </form>
    </div>

    <script>
        $(document).ready(function () {
            $('body').on('click', '.btnUpdateAgency', function () {
                var agencyCode = $(this).data('agencycode');
                var fullname = $(this).data('fullname');
                var phone = $(this).data('phone');
                var email = $(this).data('email');
                var userId = $(this).val();
                var commission = $(this).data('commission');
                var address = $(this).data('address');
                var groupPermissionId = $(this).data('grouppermissionid');
                var debtLimit = $(this).data('debtlimit');
                var currentDebt = $(this).data('current-debt');

                $('#txtAgencyCodeUpdate').val(agencyCode);
                $('.txtFullName').val(fullname);
                $('.txtUserId').val(userId);
                $('.groupPermissionId').val(groupPermissionId);
                $('.txtPhoneNumber').val(phone);
                $('.txtEmail').val(email);
                $('.txtDebtLimit').val(formatNumber(debtLimit));
                $('#txtCurrenDebtUpdate').val(formatNumber(currentDebt));
                $('.txtCommission').val(commission);
                $('.txtAddress').val(address);
                $('#level').val($(this).data('level')).change();
                $('#modal_update').modal({show: 'true'});
            });
            $('.fomatPhoneNumber').on('keyup',function(){
               formatInputInt(this);
            });
            $('.MoneyInputFormat').on('keyup focus',function(){
               $(this).val(formatNumber(stringToInt($(this).val())));
            });
            $('.MoneyInputFormat').on('focusout',function(){
                $(this).val(stringToInt($(this).val()));
            });
            $('.btn-flat-full').click(function(){
                $.each($('.MoneyInputFormat'),function (i,e) {
                    $(e).val(stringToInt($(e).val()));
                })
            });
        });

        function formatInputInt(element){
            $(element).val(stringToInt($(element).val()));
        }
        function stringToInt(str) {
            var val = str.replace(/[^0-9]/gi, '');
            return val;
        }
        function formatNumber (num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
        }

    </script>
@endsection