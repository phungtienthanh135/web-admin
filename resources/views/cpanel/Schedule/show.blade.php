@extends('cpanel.template.layout')
@section('title', 'Danh sách lịch chạy')

@section('content')
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Danh sách lịch chạy</h3></div>
                <div class="pull-right span4 t_align_r">
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i
                                        class="icon-print"></i> In
                            </button>
                            <div class="btn-group export-btn">
                                <button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất file <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="excel" data-fileName="Lich_chay">Excel</a></li>
                                    <li><a id="pdf" data-fileName="Lich_chay">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="khung_lich_ve">
            <div class="date_picker">
                <form>
                <div class="innerLR">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <div class="span2">
                                <label>Tuyến</label>
                                <select class="w_full select2" name="routeId" id="slTXPX">
                                    <option value="">Chọn tuyến</option>
                                    @foreach($listRoute as $route)
                                        <option {{request('routeId')==$route['routeId']?'selected':''}}  value="{{$route['routeId']}}">{{$route['routeName']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="span2">
                                <label>Biển số xe</label>
                                <select class="w_full select2" name="numberPlate" id="slBSX">
                                    <option value="">Chọn biển số xe</option>
                                    @foreach($listVehicle as $Vehicle)
                                        <option {{request('numberPlate')==$Vehicle['numberPlate']?'selected':''}}  value="{{$Vehicle['numberPlate']}}">{{$Vehicle['numberPlate']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="span2">
                                <label>Loại lập</label>
                                <select class="w_full" name="type" id="slNL">
                                    <option value="">Chọn loại</option>
                                    <option {{request('type')==1?'selected':''}} value="1">Không lặp</option>
                                    <option {{request('type')==2?'selected':''}} value="2">Lặp hữu hạn</option>
                                    <option {{request('type')==3?'selected':''}} value="3">Lặp vô hạn</option>
                                </select>
                            </div>
                            <div class="span2">
                                <label>&nbsp;</label>
                                <button style="border-radius:0px" class="w_full btn btn-info hidden-phone" id="search">
                                    TÌM LỊCH CHẠY
                                </button>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span3">
                                <label for="txtStartDate">Từ ngày</label>
                                <div class="input-append">
                                    <input autocomplete="off" value="{{request('startDate')}}"
                                           id="txtStartDate" name="startDate" type="text">
                                </div>
                            </div>
                            <div class="span3">
                                <label for="txtEndDate">Đến ngày</label>
                                <div class="input-append">
                                    <input autocomplete="off" value="{{request('endDate')}}" id="txtEndDate" name="endDate" type="text">
                                </div>
                            </div>
                            @if(hasAnyRole(CREATE_SCHEDULE))
                            <div class="span2">
                                <label>&nbsp;</label>
                                <a href="{{action('ScheduleController@getAdd')}}" style="border-radius:0px" class="w_full btn btn-warning hidden-phone" id="add">
                                    THÊM MỚI LỊCH CHẠY
                                </a>
                            </div>
                            @endif
                        </div>
                    </div>

                </div>
                </form>
            </div>
        </div>
        <div class="row-fluid bg_light m_top_10">
            <div class="widget widget-4 bg_light">
                <div class="widget-body">
                    <div id="tb_report">
                        <table class="table table-striped">
                            <thead>
                            <tr style="background: white;">
                                <th>Tuyến</th>
                                <th>Biển số</th>
                                <th>Thời gian xuất bến</th>
                                <th>Thời gian kết thúc</th>
                                <th>Loại lặp</th>
                                <th>Tên tài xế</th>
                                <th>Tên phụ xe</th>
                                <th>Tùy chọn</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($result as $row)
                                <tr>
                                    <td>{{$row['routeName']}}</td>
                                    <td>{{$row['numberPlate']}}</td>
                                    <td>
                                        {{--@if($row['startTime'] <= 0)--}}
                                            {{date('H\hi',($row['startTime'] / 1000))}}
                                        {{--@else--}}
                                            {{--@timeFormat($row['startTime']/1000)--}}
                                        {{--@endif--}}

                                    </td>
                                    <td>
                                        @if($row['scheduleType'] !== 3)
                                            @if($row['endDate']<92233720368548)
                                                @dateFormat($row['endDate'])
                                            @endif
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        @php
                                            switch ($row['scheduleType'])
                                            {
                                                case '1':
                                                {
                                                    echo 'Không lặp';
                                                    break;
                                                }
                                                case '2':
                                                {
                                                    echo 'Lặp';
                                                    break;
                                                }
                                                case '3':
                                                {
                                                    echo 'Vô hạn';
                                                    break;
                                                }
                                                default:
                                                {
                                                    echo 'Chưa xác định';
                                                    break;
                                                }

                                            }
                                        @endphp
                                    </td>
                                    <td>{{@implode(',',@$row['listDriverName'])}}
                                    </td>
                                    <td>
                                        {{@implode(',',@$row['listAssistantName'])}}
                                    </td>
                                    <td>
                                        <a style="cursor: pointer" tabindex="0"
                                           class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                            <ul class="onclick-menu-content">
                                                @if(hasAnyRole(UPDATE_SCHEDULE))
                                                <li>
                                                    <button onclick="window.location.href='{{action('ScheduleController@getEdit',['id'=>$row['scheduleId']])}}'">Sửa</button>
                                                </li>
                                                @endif
                                                @if(hasAnyRole(DELETE_SCHEDULE))
                                                <li>
                                                    <button data-toggle="modal" data-target="#modal_delete" onclick="$('#delete_schedule').val('{{$row['scheduleId']}}');">Xóa</button>
                                                </li>
                                                    @endif
                                            </ul>
                                        </a>
                                    </td>
                                </tr>
                                <!--hiển thị chi tiết-->
                                <tr class="info" style="display:none;">
                                    <td colspan="8">
                                        <table class="tttk no_last" cellspacing="20" cellpadding="4">
                                            <tbody>
                                            <tr>
                                                <td><b>TUYẾN</b></td>
                                                <td>{{$row['routeName']}}</td>
                                                <td class="right"><b>THỜI GIAN XUẤT BẾN</b></td>
                                                <td class="left">
                                                    @timeFormat($row['startTime']/1000)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><b>BIỂN SỐ</b></td>
                                                <td>{{$row['numberPlate']}}</td>
                                                <td class="right"><b>THỜI GIAN KẾT THÚC</b></td>
                                                <td class="left">

                                                    @if($row['scheduleType'] !== 3)
                                                        @if($row['endDate']<92233720368548)
                                                            @dateFormat($row['endDate'])
                                                        @endif
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><b>TÀI XẾ</b></td>
                                                <td>{{@implode(',',@$row['listDriverName'])}}</td>
                                                <td class="right"><b>SỐ ĐIỆN THOẠI</b></td>
                                                <td>-</td>
                                            </tr>
                                            <tr>
                                                <td><b>PHỤ XE</b></td>
                                                <td> {{@implode(',',@$row['listAssistantName'])}}</td>
                                                <td class="right"><b>SỐ ĐIỆN THOẠI</b></td>
                                                <td>-</td>
                                            </tr>
                                            <tr>
                                                <td><b>LOẠI LẶP</b></td>
                                                <td>@php
                                                        switch ($row['scheduleType'])
                                                        {
                                                            case '1':
                                                            {
                                                                echo 'Không lặp';
                                                                break;
                                                            }
                                                            case '2':
                                                            {
                                                                echo 'Lặp';
                                                                break;
                                                            }
                                                            case '3':
                                                            {
                                                                echo 'Vô hạn';
                                                                break;
                                                            }
                                                            default:
                                                            {
                                                                echo 'Chưa xác định';
                                                                break;
                                                            }

                                                        }
                                                    @endphp</td>
                                                <td class="right"><b>LOẠI XE</b></td>
                                                <td>{{@$row['vehicleTypeId']}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="8" class="center">Hiện không có dữ liệu</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    @include('cpanel.template.pagination-without-number',['page'=>$page])
                </div>
            </div>
        </div>
    </div>
    @if(hasAnyRole(DELETE_SCHEDULE))
    <div class="modal hide fade" id="modal_delete" style="width: 250px; margin-left: -10%;margin-top: 10%;">
        <div class="modal-body center">
            <p>Bạn có chắc muốn xoá?</p>
        </div>
        <form action="{{action('ScheduleController@delete')}}" method="post">
            <div class="modal-footer">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="scheduleId" value="" id="delete_schedule">
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
            </div>
        </form>
    </div>
    @endif
    <!-- End Content -->
    <!-- Table Export To File -->
    <script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>

    <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>
     <!-- Add thư viện pdf -->
    <script src="https://github.com/niklasvh/html2canvas/releases/download/v0.5.0-beta4/html2canvas.min.js"></script>
     <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
    <script>
        function printDiv(divName) {
             var printContents = document.getElementById(divName).innerHTML;
             var originalContents = document.body.innerHTML;
             document.body.innerHTML = printContents;
             window.print();
             document.body.innerHTML = originalContents;
        }

        $('.select2').select2();
    </script>
@endsection