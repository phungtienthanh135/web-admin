@extends('cpanel.template.layout')
@section('title', 'Sửa lịch chạy')

@push('activeMenu')
    activeMenu('{{action('ScheduleController@show',[],true)}}');
@endpush

@section('content')
    <form action="{{action('ScheduleController@postEdit',['id'=>$schedule['scheduleId']])}}" method="post">
        <div id="content">
            <div class="heading_top">
                <div class="row-fluid">
                    <div class="pull-left span8"><h3>Sửa lịch chạy xe</h3></div>
                </div>
            </div>
            <div class="row-fluid bg_light">
                <div class="widget widget-4 bg_light">
                    <div class="widget-body">
                        <div class="innerLR">
                            <div class="rowfluid">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                            <div class="row-fluid">
                                <div class="span4">

                                    <div class="control-group">
                                        <label for="cbb_route">Tuyến</label>
                                        <select disabled class="span12" name="routeId" id="cbb_route">
                                            @foreach($listRoute as $route)
                                                <option {{$schedule['routeId']==$route['routeId']?'selected':''}} value="{{$route['routeId']}}">{{$route['routeName']}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="control-group">
                                        <label for="cbb_vehicleId">Biển số xe</label>
                                        <select class="span12" name="vehicleId" id="cbb_vehicleId">
                                            @foreach($listVehicle as $vehicle)
                                                @php
                                                    $vehicleTypeId = $vehicle['vehicleTypeId'];
                                                    $_vehicleType = array_where($listVehicleType,function ($item) use ($vehicleTypeId){
                                                                     return $item['vehicleTypeId'] == $vehicleTypeId;
                                                         })
                                                @endphp
                                                <option {{$schedule['numberPlate']==$vehicle['numberPlate']?'selected':''}} value="{{$vehicle['vehicleId']}}">{{$vehicle['numberPlate']}}
                                                    ({{$vehicle['numberOfSeats']}} chỗ)
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                                <div class="span4">
                                    <div class="control-group">

                                        <label for="txt_startTime">Thời gian xuất bến</label>
                                        <input class="span12" readonly type="text" name="startTime"
                                               id="txt_startTime" value="{{date('H:i',$schedule['startTime']/1000)}}">
                                    </div>

                                    <div class="control-group">
                                        <label for="txtStartDate" class="control-label">Bắt đầu vào</label>
                                        <div class="controls">
                                            <div class="input-append span12">
                                                <input autocomplete="off" type="text" class="span10" name="startDate"
                                                       id="txtStartDate"
                                                       value="{{date('d-m-Y',$schedule['startDate']/1000)}}"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row-fluid">
                                <div class="span4">
                                    <div class="control-group">
                                        <label for="cbb_listDriverId">Tài xế</label>
                                        <select class="span12" name="listDriverId[]" id="cbb_listDriverId" multiple>
                                            @foreach($listDriver as $driver)
                                                @if(in_array($driver['userId'],array_get($schedule,'listDriverId',[])))
                                                    <option selected
                                                            value="{{$driver['userId']}}">{{$driver['fullName']}}</option>
                                                @else
                                                    <option value="{{$driver['userId']}}">{{$driver['fullName']}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="span4">
                                    <div class="control-group">
                                        <label for="cbb_listAssistantId">Phụ xe</label>
                                        <select class="span12" name="listAssistantId[]" id="cbb_listAssistantId"
                                                multiple="multiple">
                                            @foreach($listAssistant as $assistant)
                                                @if(in_array($assistant['userId'],array_get($schedule,'listAssistantId',[])))
                                                    <option selected
                                                            value="{{$assistant['userId']}}">{{$assistant['fullName']}}</option>
                                                @else
                                                    <option value="{{$assistant['userId']}}">{{$assistant['fullName']}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row-fluid">
                                <div class="span3">
                                    <div class="control-group">
                                        <label class="f_left fs_large">Lặp lại lịch làm việc</label>
                                        <label class="switch">
                                            <input {{$schedule['scheduleType']>1?'checked':''}} type="checkbox"
                                                   id="sl_LapLaiLichLamViec"
                                                   class="sl_LapLaiLichLamViec">
                                            <div class="slider round"></div>
                                        </label>
                                    </div>
                                </div>
                                <div id="ht_txt_lich" class="span6 d_none">
                                    <input class="span12" type="text" name="txtScheduleName" id="txtScheduleName">
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span2">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="scheduleId" id="{{$schedule['scheduleId']}}">
                                    <input type="hidden" id="txtScheduleType" name="scheduleType"
                                           value="{{$schedule['scheduleType']}}">
                                    <button class="btn btn-warning btn-flat-full">LƯU</button>
                                </div>

                                <div class="span2">
                                    <a style="margin-top:5px" class="d_block" href="{{ action('ScheduleController@show') }}">HỦY</a>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- Modal inline -->
        <div class="modal hide fade no_border" id="modal_laplailichlamviec">
            <div class="modal-header header_modal center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>LẶP LẠI LỊCH LÀM VIỆC</h3>
            </div>
            <div class="form-horizontal modal_tao_lich_chay_xe">
                <div class="modal-body">

                    <div class="control-group">
                        <label for="cbb_cycle" class="control-label">Lặp lại mỗi</label>
                        <div class="controls">
                            <select class="span4" name="cycle" id="cbb_cycle">
                                @for($i=1;$i<12;$i++)
                                    <option {{$schedule['cycle']==$i*604800000?'selected':''}} value="{{$i*604800000}}">{{$i}} tuần</option>
                                @endfor
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Lặp lại vào</label>
                        <div style="padding-top:5px" class="controls listdate">
                            <?php
                            $listwday = array_map(function ($item){return getdate($item/1000 +7*3600)['wday'];},$schedule['listDate']);
                            if(!is_array($listwday)){$listwday=[-1];}
                            ?>
                            <input {{in_array(0,$listwday)?'checked':''}}
                                   data-name="CN" type="checkbox" id="cl_cn"
                                   {{--value="{{$listDateThisWeek['Sunday']*1000}}"--}} name="listDate[]"/>
                            <label class="f_left" for="cl_cn"></label>
                            <span class="f_left m_right_10">CN</span>

                            <input {{in_array(1,$listwday)?'checked':''}} data-name="T2"
                                   type="checkbox" id="cl_thu2"
                                   {{--value="{{$listDateThisWeek['Monday']*1000}}"--}} name="listDate[]"/>
                            <label class="f_left" for="cl_thu2"></label>
                            <span class="f_left m_right_10">T2</span>

                            <input {{in_array(2,$listwday)?'checked':''}} data-name="T3"
                                   type="checkbox" id="cl_thu3"
                                   {{--value="{{$listDateThisWeek['Tuesday']*1000}}"--}} name="listDate[]"/>
                            <label class="f_left" for="cl_thu3"></label>
                            <span class="f_left m_right_10">T3</span>

                            <input {{in_array(3,$listwday)?'checked':''}} data-name="T4"
                                   type="checkbox" id="cl_thu4"
                                   {{--value="{{$listDateThisWeek['Wednesday']*1000}}"--}} name="listDate[]"/>
                            <label class="f_left" for="cl_thu4"></label>
                            <span class="f_left m_right_10">T4</span>

                            <input {{in_array(4,$listwday)?'checked':''}} data-name="T5"
                                   type="checkbox" id="cl_thu5"
                                   {{--value="{{$listDateThisWeek['Thursday']*1000}}"--}} name="listDate[]"/>
                            <label class="f_left" for="cl_thu5"></label>
                            <span class="f_left m_right_10">T5</span>

                            <input {{in_array(5,$listwday)?'checked':''}} data-name="T6"
                                   type="checkbox" id="cl_thu6"
                                   {{--value="{{$listDateThisWeek['Friday']*1000}}"--}} name="listDate[]"/>
                            <label class="f_left" for="cl_thu6"></label>
                            <span class="f_left m_right_10">T6</span>

                            <input {{in_array(6,$listwday)?'checked':''}} data-name="T7"
                                   type="checkbox" id="cl_thu7"
                                   {{--value="{{$listDateThisWeek['Saturday']*1000}}"--}} name="listDate[]"/>
                            <label class="f_left" for="cl_thu7"></label>
                            <span class="f_left m_right_10">T7</span>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="content_notification">Kết thúc</label>
                        <div style="padding-top:5px" class="controls">
                            <input type="radio" name="rd_kt" value="0" checked="checked" class="f_left"/>
                            <span class="f_left m_right_10">Không bao giờ</span>

                            <input type="radio" name="rd_kt" value="1" class="f_left"/>
                            <span class="f_left">Sau</span>
                            <input disabled="disabled" type="text" value="0" name="numberOfCycle"
                                   style="width:50px;margin-top: -5px;float:left;margin-right:10px"
                                   id="txtNumberOfCycle"/>

                            <input type="radio" name="rd_kt" value="2" class="f_left"/>
                            <span class="f_left">Vào</span>

                            <div class="input-append" style="margin-top: -5px;">
                                <input autocomplete="off" disabled="disabled" type="text" value="" style="width:98px;"
                                       class="datepicker span6" id="txtEndDate" name="endDate"/>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <div class="row-fluid">
                                <div class="span9">
                                    <button type="button" id="btn_luu" class="btn btn-warning btn-flat-full">LƯU
                                    </button>
                                </div>
                                <div class="span3">
                                    <a data-dismiss="modal" id="btn_huy"
                                       class="btn btn-default btn-flat no_border bg_light">HUỶ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- // Modal footer END -->
    <script>
        $(document).ready(function (e) {

            var input = $('#txt_startTime');
            input.clockpicker({
                autoclose: true
            });

            $('#cbb_listAssistantId,#cbb_listDriverId').select2();
            // $('#txtStartDate').datepicker("option", "minDate", 0);
            $("#sl_LapLaiLichLamViec").change(function () {
                if ($('#sl_LapLaiLichLamViec').is(':checked')) {
                    $("#ht_txt_lich").removeClass("d_none");
                    $('#modal_laplailichlamviec').modal('show');
                    $("#txtScheduleName").val('');
                    $("#txtScheduleType").val('3');
                } else {
                    $("#ht_txt_lich").addClass("d_none");
                    $("#txtScheduleType").val('1');
                }
            });
            $("#txtScheduleName").focus(function () {
                $('#modal_laplailichlamviec').modal('show');
            });
            $("#btn_huy,.modal-backdrop").click(function () {
                $("#ht_txt_lich").addClass("d_none");
                $('#sl_LapLaiLichLamViec').removeAttr('checked');
                $("#txtScheduleType").val('1');
            });
            $("#btn_luu").click(function () {

                string = '';

                $.each($('.listdate input:checked'), function (key, value) {
                    string += $(value).attr('data-name') + ' - ';
                });

                $("#txtScheduleName").val(string + $('#cbb_cycle option:selected').text().replace('1', 'Hàng'));
                $('#modal_laplailichlamviec').modal('hide');
            });

            @if($schedule['scheduleType']>1)
                __string = '';

            $.each($('.listdate input:checked'), function (key, value) {
                __string += $(value).attr('data-name') + ' - ';
            });

            $("#txtScheduleName").val(__string + $('#cbb_cycle option:selected').text().replace('1', 'Hàng'));
            $("#ht_txt_lich").removeClass("d_none");
            @endif


            $('input[name=rd_kt]').change(function (e) {
                var value = $(this).val();
                if (value == 1) {
                    $("#txtNumberOfCycle").removeAttr("disabled").focus();
                    $("#txtEndDate").attr("disabled", "disabled");
                    $("#txtScheduleType").val('2');
                } else if (value == 2) {
                    $("#txtNumberOfCycle").attr("disabled", "disabled");
                    $("#txtEndDate").removeAttr("disabled").focus();
                    $("#txtScheduleType").val('2');
                } else {
                    $("#txtNumberOfCycle").attr("disabled", "disabled");
                    $("#txtEndDate").attr("disabled", "disabled");
                    $("#txtScheduleType").val('3');
                }
            });

            // Ajax get list date of week
            $.post('{{action('ScheduleController@listDateThisWeek')}}', {
                "_token": '{{csrf_token()}}',
                "date": $('#txtStartDate').val()
            }, function (result) {
                console.log(result);
                var res = JSON.parse(result);
                $('#cl_cn').val(res['Sunday']);
                $('#cl_thu2').val(res['Monday']);
                $('#cl_thu3').val(res['Tuesday']);
                $('#cl_thu4').val(res['Wednesday']);
                $('#cl_thu5').val(res['Thursday']);
                $('#cl_thu6').val(res['Friday']);
                $('#cl_thu7').val(res['Saturday']);
            });
            $('#txtStartDate').change(function () {
                $.post('{{action('ScheduleController@listDateThisWeek')}}', {
                    "_token": '{{csrf_token()}}',
                    "date": $(this).val()
                }, function (result) {
                    var res = JSON.parse(result);
                    $('#cl_cn').val(res['Sunday']);
                    $('#cl_thu2').val(res['Monday']);
                    $('#cl_thu3').val(res['Tuesday']);
                    $('#cl_thu4').val(res['Wednesday']);
                    $('#cl_thu5').val(res['Thursday']);
                    $('#cl_thu6').val(res['Friday']);
                    $('#cl_thu7').val(res['Saturday']);
                });
            });

        });
    </script>

@endsection