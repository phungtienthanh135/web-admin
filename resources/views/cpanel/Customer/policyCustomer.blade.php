@extends('cpanel.template.layout')
@section('title', 'Chính sách')
@section('content')
    <link href="/public/bootstrap/css/bootstrap-grid.min.css" rel="stylesheet"/>
    <style>
        .padding-all-10px{
            padding:10px;
        }
        .lds-roller div {
            animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            transform-origin: 32px 32px;
        }
        .lds-roller div:after {
            content: " ";
            display: block;
            position: absolute;
            width: 6px;
            height: 6px;
            border-radius: 50%;
            background: #000;
            margin: -3px 0 0 -3px;
        }
        .lds-roller div:nth-child(1) {
            animation-delay: -0.036s;
        }
        .lds-roller div:nth-child(1):after {
            top: 50px;
            left: 50px;
        }
        .lds-roller div:nth-child(2) {
            animation-delay: -0.072s;
        }
        .lds-roller div:nth-child(2):after {
            top: 54px;
            left: 45px;
        }
        .lds-roller div:nth-child(3) {
            animation-delay: -0.108s;
        }
        .lds-roller div:nth-child(3):after {
            top: 57px;
            left: 39px;
        }
        .lds-roller div:nth-child(4) {
            animation-delay: -0.144s;
        }
        .lds-roller div:nth-child(4):after {
            top: 58px;
            left: 32px;
        }
        .lds-roller div:nth-child(5) {
            animation-delay: -0.18s;
        }
        .lds-roller div:nth-child(5):after {
            top: 57px;
            left: 25px;
        }
        .lds-roller div:nth-child(6) {
            animation-delay: -0.216s;
        }
        .lds-roller div:nth-child(6):after {
            top: 54px;
            left: 19px;
        }
        .lds-roller div:nth-child(7) {
            animation-delay: -0.252s;
        }
        .lds-roller div:nth-child(7):after {
            top: 50px;
            left: 14px;
        }
        .lds-roller div:nth-child(8) {
            animation-delay: -0.288s;
        }
        .lds-roller div:nth-child(8):after {
            top: 45px;
            left: 10px;
        }
        @keyframes lds-roller {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        #txtStartDate + button{
            width: 20%;
        }
        #txtEndDate + button{
            width: 20%;
        }

    </style>
    <div id="content" style="padding-bottom: 20px">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3 id="headerName">Phân loại khách hàng</h3>
                </div>
            </div>
        </div>

        <div data-tabs="list">
            <div class="col-xs-12" style="padding-top: 20px">
                <table class="table table-bordered listTypeCustomer">
                    <thead>
                    <tr>
                        <td>STT</td>
                        <td>Hạng Khách Hàng</td>
                        <td>Giá trị phân loại</td>
                        <td>Hành động</td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        listTypeCustomer = [];
        $(document).ready(function () {
            init();
        });

        function init() {
            getListTypeCustomer();
            $('body').on('click','.btnAddType',function(){
                var parentE = $(this).parents('tr');
                var customerTypeName = $(parentE).find('.customerTypeName').val();
                var conditionAchieveType = $(parentE).find('.conditionAchieveType').val();
                var conditionAchieveValue = $(parentE).find('.conditionAchieveValue').val();
                SendAjaxWithJson({
                    type : "post",
                    url :urlDBD('company_customer_type/create'),
                    data : {
                        customerTypeName : customerTypeName,
                        conditionAchieveType : conditionAchieveType,
                        conditionAchieveValue : conditionAchieveValue
                    },
                    success :function (result) {
                        getListTypeCustomer();
                    }
                });
            });
            $('body').on('click','.btnUpdateType',function(){
                var parentE = $(this).parents('tr');
                var customerTypeId = $(this).attr('data-id');
                var customerTypeName = $(parentE).find('.customerTypeName').val();
                var conditionAchieveType = $(parentE).find('.conditionAchieveType').val();
                var conditionAchieveValue = $(parentE).find('.conditionAchieveValue').val();
                SendAjaxWithJson({
                    type : "post",
                    url :urlDBD('company_customer_type/create'),
                    data : {
                        customerTypeName : customerTypeName,
                        conditionAchieveType : conditionAchieveType,
                        conditionAchieveValue : conditionAchieveValue,
                        customerTypeId : customerTypeId
                    },
                    success :function (result) {
                        notyMessage("Cập nhật thành công","success");
                        getListTypeCustomer();
                    },
                    functionIfError : function (data) {
                        getListTypeCustomer();
                    }
                });
            });
            $('body').on('click','.btnDeleteType',function(){
                notyMessage("Chức năng đang được nâng cấp","error");
//                var parentE = $(this).parents('tr');
//                var customerTypeId = $(this).attr('data-id');
//                var customerTypeName = $(parentE).find('.customerTypeName').val();
//                var conditionAchieveType = $(parentE).find('.conditionAchieveType').val();
//                var conditionAchieveValue = $(parentE).find('.conditionAchieveValue').val();
//                SendAjaxWithJson({
//                    type : "post",
//                    url :urlDBD('company_customer_type/update'),
//                    data : {
//                        customerTypeName : customerTypeName,
//                        conditionAchieveType : conditionAchieveType,
//                        conditionAchieveValue : conditionAchieveValue,
//                        customerTypeId : customerTypeId
//                    },
//                    success :function (result) {
//                        notyMessage("Cập nhật thành công","success");
//                        getListTypeCustomer();
//                    },
//                    error : function (data) {
//                        getListTypeCustomer();
//                    }
//                });
            });
        }

        function getListTypeCustomer() {
            $('.listTypeCustomer tbody').html('<tr><td colspan="4" style="text-align: center">Đang tải dữ liệu...</td></tr>');
            SendAjaxWithJson({
                url : urlDBD('company_customer_type/getList'),
                type : 'post',
                success :function (data){
                    console.log(data);
                    listTypeCustomer = data.results.listCustomerType;
                    $('.listTypeCustomer tbody').html(buildHtmlCustomerType());
                },
                error :function (data) {
                    $('.listTypeCustomer tbody').html('<tr><td style="text-align: center" colspan="4">Lấy dữ liệu thất bại</td></tr>');
                }
            });
        }

        function buildHtmlCustomerType() {
//            currentTime = new Date().getTime();
            html = '';
            $.each(listTypeCustomer,function (k,v) {
                html+='<tr>';
                html+='<td>'+parseInt(k+1)+'</td>';
                html+='<td><input type="text" class="customerTypeName" style="width: 100%;margin:0" placeholder="Tên loại khách hàng" value="'+v.customerTypeName+'"></td>';
                html+='<td>';
                html+='<div class="input-append has-success" style="width:100%;margin: 0">';
                html+='<input value="'+v.conditionAchieveValue+'" style="width:55%" name="price" type="number" min="1" placeholder="Giá trị" data-validation="number" data-validation-allowing="range[1;9007199254740992]" data-validation-optional="true" class="valid conditionAchieveValue">';
                html+='<select class="btn conditionAchieveType" style="width:45%">';
                html+='<option value="1" '+(v.conditionAchieveType=="1"?"selected":"")+'>Tiền Vé</option>';
                html+='<option value="2" '+(v.conditionAchieveType=="2"?"selected":"")+'>Số lượng vé</option>';
                html+='</select>';
                html+='</div>';
                html+='<td>';
                html+='<button class="btn btn-warning btnUpdateType" data-id="'+v.customerTypeId+'">CẬP NHẬT</button> ';
                html+='<button class="btn btn-danger btnDeleteType" data-id="'+v.customerTypeId+'">XÓA</button>';
                html+='</td>';
                html+='</tr>';
            });
            html+='<tr>';
            html+='<td></td>';
            html+='<td><input type="text" class="customerTypeName" style="width: 100%;margin:0" placeholder="Tên loại khách hàng"></td>';
            html+='<td>';
            html+='<div class="input-append has-success" style="width:100%;margin: 0">';
            html+='<input value="" style="width:55%" name="price"  type="number" min="1" placeholder="Giá trị" data-validation="number" data-validation-allowing="range[1;9007199254740992]" data-validation-optional="true" class="valid conditionAchieveValue">';
            html+='<select class="btn conditionAchieveType" style="width:45%">';
            html+='<option value="1">Tiền Vé</option>';
            html+='<option value="2">Số lượng vé</option>';
            html+='</select>';
            html+='</div>';
            html+='<td>';
            html+='<button class="btn btn-success btnAddType">THÊM</button> ';
            html+='</td>';
            html+='</tr>';
            return html;
        }

        function dateToMiliS(date) {
            date = date.split("-");
            var newDate = date[1] + "/" + date[0] + "/" + date[2];
            datem = new Date(newDate).getTime();
            return datem;
        }

        function getValueWithType(type,value) {
            if(type==2){
                return value*100 +" %";
            }
            else{
                return $.number(value) + ' VND';
            }
        }

    </script>
@endsection