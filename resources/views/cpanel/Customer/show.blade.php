
@extends('cpanel.template.layout')
@section('title', 'Danh sách khách hàng')
@section('content')

    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Danh Sách Khách Hàng</h3></div>
            </div>
        </div>
        <div class="separator bottom"></div>
        <div class="innerLR">
            <div class="row-fluid">
                <div class="control-box">
                    <h4>Tìm khách hàng</h4>
                <form action="">
                    <div class="row-fluid">
                        <div class="span3">
                            <label for="phone_customer">Số điện thoại</label>
                            <input type="text" name="phoneNumber" id="phone_customer" value="{{request('phoneNumber')}}">
                        </div>
                        <div class="span2">
                            <label class="separator hidden-phone" for="add"></label>
                            <button class="btn btn-info btn-flat" id="search">TÌM KHÁCH HÀNG</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
            <div class="row-fluid">
                Danh sách khách hàng
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Họ tên</th>
                        <th>Số điện thoại</th>
                        <th>Số chuyến đi</th>
                        <th>Số tiền đã thanh toán</th>
                        <th>Địa chỉ</th>
                        {{--<th class="center">Loại khách hàng</th>--}}
                        {{--<th class="center">Tùy chọn</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @php($stt=1)
                    @foreach($listCustomer as $row)
                    <tr>
                        <td>{{$stt++}}</td>
                        <td>{{$row['fullName']}}</td>
                        <td>{{$row['phoneNumber']}} @php echo !empty($row['phoneNumber']) ? '<button class="btn btn-primary btn-circle makeCall" value="'.$row['phoneNumber'].'"><i class="fa fa-phone" aria-hidden="true"></i></button>' : '' @endphp</td>
                        <td>{{$row['numberOfTickets']}}</td>
                        <td>@moneyFormat($row['totalMoney'])</td>
                        <td>{{@$row['address']}}</td>
                        {{--<td class="{{($row['status']) == 2 ? 'than-thiet' : ''}} center">@checkRank($row['status'])</td>--}}
                        {{--<td class="center">
                            <a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                <ul class="onclick-menu-content">
                                    <li>
                                        <button onclick="showinfo();">Sửa</button>
                                    </li>
                                    <li>
                                        <button>Xóa</button>
                                    </li>
                                </ul>
                            </a>
                        </td>--}}
                    </tr>
                   @endforeach
                    </tbody>
                </table>

                @include('cpanel.template.pagination-without-number',['page'=>$page])
            </div>
            <div class="row-fluid bg_light"  style="margin-bottom:10px;display: none;" id="info">
                <div class="widget widget-4 bg_light">
                    <div class="widget-body">
                        <div class="span8">
                            <table class="tttk" cellspacing="20" cellpadding="4">
                                <tbody>
                                <tr>
                                    <th>HỌ TÊN</th>
                                    <td>NGUYỄN HOÀNG PHÚC</td>
                                    <td><a href="javascipt:void(0)" data-toggle="modal" data-target="#edit"><i
                                                    class="icon-pencil"></i></a></td>
                                </tr>
                                <tr>
                                    <th>NGÀY SINH</th>
                                    <td>11-07-1995</td>
                                </tr>
                                <tr>
                                    <th>GIỚI TÍNH</th>
                                    <td>NAM</td>
                                </tr>
                                <tr>
                                    <th>BẰNG LÁY XE</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>NGÀY VÀO CTY</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>MÃ NHÂN SỰ</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>SỐ ĐIỆN THOẠI</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>KHI CẦN BÁO TIN CHO AI</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>QUÊ QUÁN</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>CMND</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>NGÀY HẾT HẠN</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>CHỨC VỤ</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>EMAIL</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>TÀI KHOẢN</th>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th>MẬT KHẨU</th>
                                    <td>***********</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="span4">
                            <div class="avatar">
                                <img src="images/avatar.PNG">
                            </div>

                            <input type="file" id="selectedFile" style="display: none;">
                            <input type="button" style="width:172px" value="CHỌN ẢNH" class="btncustom"
                                   onclick="document.getElementById('selectedFile').click();">

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="clearfix"></div>
        <div class="modal hide fade" style="width: 250px;margin-left: -10%" id="edit">
            <div class="modal-body">
                <p>Họ tên</p>
                <input type="text">
            </div>
            <div class="modal-footer">
                <a data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <a href="#" class="btn btn-warning btn-flat">ĐỒNG Ý</a>
            </div>
        </div>
    </div>
    <!-- End Content -->

@endsection