
@extends('cpanel.template.layout')
@section('title', 'Nhật trình xe')
@section('content')

<style type="text/css">
    .table thead tr th{text-align: center;}
    .table tbody tr td{text-align: center;}
</style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Nhật trình xe</h3></div>
            </div>
        </div>
        <div class="khung_lich_ve">
            <div class="date_picker">
                <div style="padding: 7px;" class="widget widget-4">
                    <div class="widget-head">
                        <h4>Tìm tuyến</h4>
                    </div>
                </div>
                <div class="innerLR">
                <form action="">
                    <div class="row-fluid">
                        <div class="span3">
                            <label>Biển số</label>
                            <input type="text" name="numberPlate" id="txtMaGiaoDich" value="{{request('numberPlate')}}">
                        </div>
                        <div class="span3">
                            <label for="txtStartDate">Từ ngày</label>
                            <div class="input-append">
                                <input value="{{request('startDate')}}"
                                       id="txtStartDate" name="startDate" type="text">
                            </div>
                        </div>

                        <div class="span3">
                            <label for="txtEndDate">Đến ngày</label>
                            <div class="input-append">
                                <input value="{{request('endDate')}}" id="txtEndDate" name="endDate" type="text">
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid">
                        {{--<!-- <div class="span3">--}}
                            {{--<label>Mã chuyến</label>--}}
                            {{--<div class="input-append">--}}
                                {{--<input name="tripId" type="text" value="{{request('tripId')}}">--}}
                            {{--</div>--}}
                        {{--</div> -->--}}
                        <div class="span6">
                            <label>Nội dung</label>
                            <input style="width:481px" name="_content" id="txtNoidung" type="text"  value="{{request('_content')}}">
                        </div>
                        <div class="span3">
                            <label>&nbsp;</label>
                            <button style="width:220px;border-radius:0px" class="btn btn-info hidden-phone" id="search">TÌM NHẬT TRÌNH</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <div class="row-fluid bg_light m_top_10">
            <div class="widget widget-4 bg_light">
                <div class="widget-body">
            
                @php(date_default_timezone_set('UTC'))
                    <table class="table table-hover table-vertical-center">
                        <thead>
                        <tr>
                            <!-- <th>Mã chuyến</th> -->
                            <th class="span3">Biển số</th>
                            <th class="span3">Tuyến</th>
                            <th class="span3">Thời gian xuất bến</th>
                            <th class="span3">Thời gian kết thúc</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($result as $row)
                        <tr>
                            {{--<!-- <td>--}}
                                {{--@if(hasAnyRole(VIEW_TRIP_ACTIVITY))--}}
                                {{--<a href="{{action('TripController@logdetail',['TripId'=>$row['tripId']])}}">{{$row['tripId']}}</a>--}}
                                {{--@else--}}
                                    {{--{{$row['tripId']}}--}}
                                {{--@endif--}}
                            {{--</td> -->--}}
                            <td>{{$row['numberPlate']}}</td>
                            <td>{{$row['routeName']}}</td>
                            <td>@timeFormat($row['startTimeReality']/1000) - @dateFormat(strtotime($row['startDateReality'])*1000)</td>
                            <td>
                            @timeFormat($row['endTimeReality']/1000) - @dateFormat(strtotime($row['endDateReality'])*1000)
                           </td>
                        </tr>
                        @empty
                        <tr>
                            <td class="center" colspan="5">Hiện không có dữ liệu</td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>

                    @include('cpanel.template.pagination-without-number',['page'=>$page])
                </div>
            </div>
        </div>
    </div>

    

@endsection