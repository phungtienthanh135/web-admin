<table class="table table-hover table-vertical-center">
    <thead>
    <tr>
        <th>Biển số</th>
        <th>Tuyến</th>
        <th>Thời gian xuất bến</th>
        <th>Tỷ lệ lấp đầy</th>
        <th>Tùy chọn</th>
    </tr>
    </thead>
    <tbody>
    @if($listTripOfTable)
    @forelse($listTripOfTable as $row)
        @php($time = strtotime($row['startDateReality']) + $row['startTimeReality']/1000 + 7*3600)
        <tr>
            <td>{{$row["numberPlate"]}}</td>
            <td>{{$row['routeName']}}</td>
            <td>@dateTime($time*1000)</td>
            <td>{{$row['sumOfNumberSeatSold']}}/{{$row['sumOfNumberSeat']}}</td>
            <td>
                <a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                    <ul class="onclick-menu-content">
                        @if(hasAnyRole(SELL_TICKET) && ($type == 0 || $type == 1))
                            <li>
                                <button onclick="window.location.href='{{action('TicketController@sell',[
                                                'routeId' => $row['routeId'],
                                                'date'=>date('d-m-Y',$time),
                                                'tripId'=>$row['tripId'],
                                                'startPointId'=>$row['tripId'],
                                                'endPointId'=>$row['tripId']

                                                ])}}'">Bán vé
                                </button>
                            </li>
                        @endif
                        @if(hasAnyRole(OUT_OF_STATION_COMMAND) && $type == 0)
                            <li>
                                <button onclick="window.location.href='{{action('TripController@startTrip',[
                                                'id' => $row['tripId'],])}}'">Xuất bến
                                </button>
                            </li>
                        @endif
                        @if($type == 1)
                            <li>
                                <button onclick="window.location.href='{{action('TripController@endTrip',[
                                                'id' => $row['tripId'],])}}'">Kết thúc chuyến
                                </button>
                            </li>
                        @endif
                        @if(hasAnyRole(CANCEL_OUT_OF_STATION_COMMAND) && $type != 2)
                            <li>
                                <button onclick="window.location.href='{{action('TripController@cancelTrip',[
                                                'id' => $row['tripId'],])}}'">Hủy chuyến
                                </button>
                            </li>
                        @endif
                        @if(hasAnyRole(CREATE_CHANGE_VEHICLE) && $type == 0)
                            <li>
                                <button type="button" onclick="window.location.href='{{action('TripController@transfer',[
                                                'tripSourceId' => $row['tripId'],])}}'">Điều chuyển xe
                                </button>
                            </li>
                        @endif

                        @if($type == 2)
                            <li>
                                <button type="button" onclick="window.location.href='{{action('BalanceSheetController@income',[
                                            'tripId' => $row['tripId'],])}}'">Nhập thu
                                </button>
                            </li>

                                <li>
                                    <button type="button" onclick="window.location.href='{{action('BalanceSheetController@expenses',[
                                            'tripId' => $row['tripId'],])}}'">Nhập chi
                                    </button>
                                </li>
                        @endif
                        {{--TODO chưa check quyền in phơi vé--}}
                        @if(true)
                            <li>
                                <button class="printVoid" onclick="window.open('{{action('TripController@printVoidTicket',[
                                                'tripId' => $row['tripId'],])}}')">In phơi vé
                                </button>
                            </li>
                        @endif
                    </ul>
                </a>
            </td>
        </tr>
        <tr class="info" style="display: none">
            <td colspan="6">
                <table class="tttk no_last" cellspacing="20" cellpadding="4">
                    <tbody>
                    <tr>
                        <td><b>BIỂN SỐ</b></td>
                        <td>{{$row["numberPlate"]}}</td>
                        <td class="right"><b>TÀI XẾ</b></td>
                        <td class="left">-</td>
                    </tr>
                    <tr>
                        <td><b>TUYẾN</b></td>
                        <td>{{$row["routeName"]}}</td>
                        <td class="right"><b>SĐT TÀI XÊ</b></td>
                        <td class="left">-</td>
                    </tr>
                    <tr>
                        <td><b>THỜI GIAN XUẤT BẾN</b></td>
                        <td>@dateTime($time)</td>
                        <td class="right"><b>PHỤ XE</b></td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td><b>THỜI GIAN ĐẾN ĐÍCH</b></td>
                        <td>@dateTime($time+($row["runTimeReality"]/1000))</td>
                        <td class="right"><b>SĐT PHỤ XE</b></td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td><b>QUÃNG ĐƯỜNG</b></td>
                        <td>-</td>
                        <td class="right"><b>DOANH THU HIỆN TẠI</b></td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td><b>THỜI GIAN ĐI</b></td>
                        <td>@timeFormat($row["runTimeReality"]/1000)</td>
                        <td rowspan="3" class="right"><b>VỊ TRÍ CỦA XE</b></td>
                        <td rowspan="3">
                            <a href="#modal_vitricuaxe" data-toggle="modal"><img src="/public/images/bando.png"></a>
                        </td>
                    </tr>
                    <tr>
                        <td><b>LOẠI XE</b></td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td><b>TỶ LỆ LẤP ĐẦY</b></td>
                        <td>{{$row['sumOfNumberSeatSold']}}/{{$row['sumOfNumberSeat']}}</td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    @empty
        <tr>
            <td class="center" colspan="6">Hiện tại không có dữ liệu</td>
        </tr>
    @endforelse
@endif
    </tbody>
</table>