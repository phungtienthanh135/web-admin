@extends('cpanel.template.layout')
@section('title', 'Điều hành xe')
@section('content')
<div id="content">
    <div class="heading_top">
        <div class="row-fluid">
            <div class="pull-left span8">
                <ul class="hr_list stripe_list">
                    <li><h3>
                        <a {!! request('option')==1 || empty(request('option'))?'style="color: #19448a;font-weight: 200;"':'' !!} href="{{action('TripController@control',['option'=>'1'])}}">DS
                        các xe đã xuất bến</a></h3></li>
                    <li><h3>
                        <a {!! request('option')==2?'style="color: #19448a;font-weight: 200;"':'' !!} href="{{action('TripController@control',['option'=>'2'])}}">DS
                            các xe chuẩn bị xuất bến</a></h3></li>
                    <li><h3>
                        <a {!! request('option')==3?'style="color: #19448a;font-weight: 200;"':'' !!} href="{{action('TripController@control',['option'=>'3'])}}">DS
                                các xe đã kết thúc</a></h3></li>
                    <li><h3>
                        <a {!! request('option')==4?'style="color: #19448a;font-weight: 200;"':'' !!} href="{{action('TripController@control',['option'=>'4'])}}">DS chuyến đã xếp xe</a></h3></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="khung_lich_ve">
                        <div class="date_picker">
                            <div style="padding: 7px;" class="widget widget-4">
                                <div class="widget-head">
                                    <h4>Tìm xe</h4>
                                </div>
                            </div>
                            <div class="innerLR">
                                <form action="">
                                    <div class="row-fluid">
                                        <div class="span3">
                                            <label>Tuyến</label>
                                            <select name="routeId">
                                                <option value="">Chọn tuyến</option>
                                                @foreach($listRoute as $route)
                                                <option value="{{$route['routeId']}}" {{ request('routeId') == $route['routeId'] ? 'selected' : '' }}>{{$route['routeName']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="span2">
                                            <label>Biển số</label>
                                            <input type="text" class="w_full" name="numberPlate" value="{{ request('numberPlate') }}"/>
                                        </div>
                                        <div class="span2">
                                            <label>&nbsp;</label>
                                            <button style="width:220px;border-radius:0px" class="btn btn-info hidden-phone"
                                            id="search">
                                            TÌM XE
                                            </button>
                                        </div>
                                        <div class="span1">
                                            <input type="hidden" name="option" value="{{ request('option') }}">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Danh sách các xe đã xuất bến đang lưu hành -->
                    @includeWhen(request('option')==1 || empty(request('option')), 'cpanel.Trip.table-trip',[
                    'listTripOfTable'=>$listTrip,
                    'listItem'=>$listItem,
                    'nhanvien'=>$nhanvien,
                    'type'=>1
                    ])
                    <!-- Danh sách các xe chuẩn bị xuất bến -->
                    @includeWhen(request('option')==2, 'cpanel.Trip.table-trip',[
                    'listTripOfTable'=>$listTrip,
                    'listItem'=>$listItem,
                    'nhanvien'=>$nhanvien,
                    'type'=>0
                    ])
                    <!-- Danh sách các xe chuẩn bị xuất bến -->
                    @includeWhen(request('option')==3, 'cpanel.Trip.table-trip',[
                    'listTripOfTable'=>$listTrip,
                    'listItem'=>$listItem,
                    'nhanvien'=>$nhanvien,
                    'type'=>2
                    ])
                    <!-- Danh sách các chuyến đã xếp xe -->
                    @includeWhen(request('option')==4, 'cpanel.Trip.table-trip',[
                    'listTripOfTable'=>$listTrip,
                    'listItem'=>$listItem,
                    'nhanvien'=>$nhanvien,
                    'type'=>2
                    ])
                    @include('cpanel.template.pagination-without-number',['page'=>$page])
                </div>
                <!-- End Content -->
                <!-- Modal inline -->
                <div style="width:90%;height:90%;left:5%;margin-left:0;top:5%" class="modal hide fade no_border"
                    id="modal_vitricuaxe">
                    <div class="modal-header header_modal center">
                        <button type="button" style="" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3>VỊ TRÍ CỦA XE</h3>
                    </div>
                    <div class="modal-body" style="width:100%;height:100%;max-height:88%;max-width:98.5%;overflow:hidden">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d11101.66506373379!2d105.46592814082197!3d10.339378268789087!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1503393760662"
                        width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
                <script src="/public/javascript/jquery.printPage.js" type="text/javascript"></script>
                @endsection