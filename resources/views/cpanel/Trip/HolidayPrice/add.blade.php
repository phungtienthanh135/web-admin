@extends('cpanel.template.layout')
@section('title', 'Thêm mới phương tiện')
@section('content')
    <link rel="stylesheet" href="/public/theme/css/SimpleImage.css">
    <style>
        .loidi, .ghegiucho {
            color: rgba(0, 0, 0, 0);;
        }
    </style>
    <form action="" method="post">
        <div id="content">
            <div class="heading_top">
                <div class="row-fluid">
                    <div class="pull-left span8"><h3>Thêm mới bảng giá lễ tết</h3></div>
                </div>
            </div>
            <div class="innerLR">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row-fluid m_top_10">
                    <div class="span3">
                        <label>Tuyến</label>
                        <select name="routeId" id="routeId">
                            @foreach($listRoute as $route)
                                <option value="{{$route['routeId']}}" {{ request('routeId') == $route['routeId'] ? 'selected' : '' }}>{{$route['routeName']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="span3">
                        <label for="txtStartDate">Ngày bắt đầu</label>
                        <div class="input-append">
                            <input  type="text" class="datepicker span11" name="startDate"
                                    id="txtStartDate" required
                                    value="{{old('startDate')}}">
                        </div>
                    </div>
                    <div class="span3">
                        <label for="txtEndDate">Ngày kết thúc</label>
                        <div class="input-append">
                            <input  type="text" required class="datepicker span11" name="endDate"
                                    id="txtEndDate"
                                    value="{{old('endDate')}}">
                        </div>
                    </div>
                    <input type="hidden" name="seatList" id="seatList">
                </div>

                <div class="row-fluid bg_light m_top_10">
                    <div class="widget widget-4 bg_light">
                        <div class="widget-body">
                            @include('cpanel.Trip.HolidayPrice.seatmap')
                            <div class="row-fluid m_top_15 m_bottom_20">
                                <div class="span3">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="seatMapId" id="seatMapId_add"
                                           value="{{old('seatMapId')}}">
                                    <button class="btn btn-warning btn-flat-full" id="Saveform">LƯU</button>
                                </div>
                                <div class="span2">
                                    <a href="{{action('TripController@getAddPriceHoliday')}}" class="btn btn-default btn-flat-full">HỦY</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>
        $('body').on('click','#Saveform',function(){
            $('#seatList').val(JSON.stringify(getseatList()));
            if($('#seatMapId_add').val()=== ''){
                alert("vui lòng chọn sơ đồ xe");
                return false;
            }
        });
        $('#routeId').select2();
    </script>
@endsection