<style>
    .edited{color : #000;}
</style>
<div class="row-fluid">
    <div class="span4 table-responsive">
        <div class="row-fluid">
            <table class="table table-hover table-vertical-center">
                <thead>
                <tr>
                    <th>Mã sơ đồ</th>
                    <th>Loại sơ đồ</th>
                    {{--<th>Tùy chọn</th>--}}
                </tr>
                </thead>
                <tbody id="table-seatMap">
                @foreach($listSeatMap as $seatMap)
                    <tr id="{{$seatMap['seatMapId']}}"  onclick="return editSeatMap('{{$seatMap['seatMapId']}}')">
                        <td>{{$seatMap['seatMapId']}}</td>
                        <td>{{$seatMap['vehicleTypeName']}}
                            <br>({{$seatMap['numberOfFloors'].' Tầng'}})
                        </td>

                        {{--<td>--}}
                            {{--<button onclick="return editSeatMap('{{$seatMap['seatMapId']}}')">--}}
                                {{--Sửa--}}
                            {{--</button>--}}
                            {{--<a href="javascript:void(0)" tabindex="0"--}}
                               {{--class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>--}}
                                {{--<ul class="onclick-menu-content">--}}
                                    {{--<li>--}}
                                        {{--<button onclick="return showSeatMap('{{$seatMap['seatMapId']}}')">--}}
                                            {{--Chọn sơ đồ--}}
                                        {{--</button>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<button onclick="return editSeatMap('{{$seatMap['seatMapId']}}')">--}}
                                            {{--Sửa--}}
                                        {{--</button>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<button data-toggle="modal" data-target="#modal_delete"--}}
                                                {{--onclick="return deleteSeatMap('{{$seatMap['seatMapId']}}')">--}}
                                            {{--Xóa--}}
                                        {{--</button>--}}
                                    {{--</li>--}}

                                {{--</ul>--}}
                            {{--</a>--}}
                        {{--</td>--}}
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="row-fluid m_bottom_20">
            {{--<button id="btn_addSeatMap" class="btncustom btn-flat-full m_top_10 " data-toggle="modal"--}}
                    {{--data-target="#them_so_do"><i class="icon-plus icon-white"></i> THÊM SƠ ĐỒ--}}
            {{--</button>--}}

            {{--<button style="display: none" id="btn_saveSeatMap" onclick="return saveSeatMap()"--}}
                    {{--class="btn btn-warning btn-flat-full m_top_10">LƯU SƠ ĐỒ--}}
            {{--</button>--}}

            {{--<button style="display: none" id="btn_editSeatMap" onclick="return updateSeatMap()"--}}
                    {{--class="btn btn-warning btn-flat-full m_top_10">CẬP NHẬT SƠ ĐỒ--}}
            {{--</button>--}}
        </div>
    </div>
    <div class="span8">
        <div class="row-fulid m_top_10">
            <div id="note" class="ghichu_datghe" style="float: none">
                <p>Chú thích: Click chọn vào ô bên dưới sau đó chọn vào ô trong sơ đồ để thay đổi sơ
                    đồ</p>
                <div class="span2"><a data-TypeId="1" data-seatType="ghe ghegiucho"
                                      class="gc_ghe gc_cua"></a>Cửa
                </div>

                <div class="span2"><a data-TypeId="2" data-seatType="ghe ghedadat"
                                      class="gc_ghe gc_ghe_tai"></a>Ghế tài
                </div>

                <div class="span2"><a data-TypeId="6" data-seatType="ghe ghephuxe"
                                      class="gc_ghe gc_ghe_phu_xe"></a>Ghế phụ xe
                </div>

                <div class="span2"><a data-TypeId="3" data-seatType="ghe ghetrong"
                                      class="gc_ghe gc_ghe_khach"></a>Ghế khách
                </div>

                <div class="span2"><a data-TypeId="4" data-seatType="ghe giuongnam"
                                      class="gc_ghe gc_giuong"></a>Giường nằm
                </div>

                <div class="span2"><a data-TypeId="5" data-seatType="ghe nhavesinh"
                                      class="gc_ghe gc_nha_ve_sinh"></a>Nhà vệ sinh
                </div>

                <div class="span2"><a data-TypeId="0" data-seatType="ghe loidi"
                                      class="gc_ghe gc_loi_di"></a>Lối đi
                </div>

                {{--<div class="span5"> <input type="checkbox" id="cbUpdateInfo"/><label title="Chọn vào đây sau đó click vào ghế để sửa thông tin" for="cbUpdateInfo"></label> <span style="margin-left: 25px">Sửa Thông Tin Ghế</span></div>--}}
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row-fluid m_top_10">
            <div class="span6" id="Floor1">
                <div class="khungxe" style="width: 320px">

                </div>
                <div class="center">Tầng 1</div>
            </div>
            <div class="span6" id="Floor2">
                <div class="khungxe" style="width: 320px">

                </div>
                <div class="center">Tầng 2</div>
            </div>
        </div>

    </div>

</div>



<div class="modal modal-custom hide fade in" id="them_so_do" aria-hidden="false"
     style="margin-top: 10%;margin-left: -10%;max-width: 500px; display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="center">thêm sơ đồ </h3>
    </div>
    <form action="" method="post">
        <div class="modal-body">
            <div class="alert alert-warning">
                Số cột bao gồm cả lối đi.
            </div>

            <div class="row-fluid">
                <div class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label" for="txtSoHang">Số hàng</label>
                        <div class="controls">
                            <div class="form-inline">
                                <input class="span3" type="number" id="txtSoHang" min="4">

                                <label for="txtSoCot">Số cột</label>
                                <input class="span3" type="number" id="txtSoCot" min="2" max="7">

                            </div>
                        </div>
                    </div>

                    <div class="control-group">

                        <label class="control-label" for="cbb_SoTang">Số tầng</label>
                        <div class="controls">
                            <select class="span3" id="cbb_SoTang" name="numberOfFloors">
                                <option value="1">1</option>
                                <option value="2">2</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="cbb_LoaiXe">Loại xe</label>
                        <div class="controls">
                            <select class="span12" id="cbb_LoaiXe" name="vehicleTypeId">
                                @foreach($listVehicleType as $VehicleType)
                                    <option value="{{$VehicleType['vehicleTypeId']}}">{{$VehicleType['vehicleTypeName']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="txtGiaGhe">Giá cộng thêm:</label>
                        <div class="controls">
                            <input class="span12" type="number" id="txtGiaGhe" min="4">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a onclick="createSeatMapHTML()" class="btn btn-warning btn-flat">TIẾP TỤC</a>
            <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
        </div>
    </form>
</div>
<!-- End Content -->

{{--form delete--}}
<div class="modal hide fade" id="modal_delete" style="width: 250px; margin-left: -10%;margin-top: 10%;">
    <div class="modal-body center">

        <p>Bạn có chắc muốn xoá?</p>
    </div>
    <form action="{{action('SeatMapController@delete')}}" method="post">
        <div class="modal-footer">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="seatMapId" value="" id="delete_seatMap">
            <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
            <button class="btn btn-primary btn_dongy">ĐỒNG Ý</button>
        </div>
    </form>
</div>
{{--end form delete--}}


<div class="modal hide fade  modal-custom" id="modal_editSeat"
     style="width: 677px;margin-left: -22%;margin-top: 3%;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="center">Sửa thông tin ghế </h3>
    </div>
    <div class="modal-body center">
        <div class="row-fluid">
            <div class="form-group text-center">
                <div class="box-upload-images">
                    <form action="" id="frmUploadImage">
                            <span class="btn btn-warning fileinput-btn">
                                <span>Tải Ảnh Lên</span>
                                    <input name="img" type="file" id="imgUpload" multiple/>
                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                             </span>
                    </form>
                    <p style="margin:10px auto"><strong>Tối đa 3 ảnh.</strong></p>
                    <div class="row1">
                        <section id="preview"></section>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row-fluid m_top_15">
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label" for="txtSeatId">Số Ghế</label>
                    <div class="controls">
                        <input readonly class="span12" type="text" id="txtSeatId">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="txtExtraprice" id="focusExtraprice">Phụ thu lễ tết : </label>
                    <div class="controls">
                        <input class="span12" type="number" id="txtExtraprice">
                    </div>
                </div>
                {{--<div class="control-group">--}}
                    {{--<label class="control-label" for="txtTenLoaiXe">Phụ thu lễ tết : </label>--}}
                    {{--<div class="input-append">--}}
                        {{--<input class="" style="width:400px;" type="text" id="txtExtraprice">--}}
                        {{--<select id="sl_donvi" class="btn">--}}
                            {{--<option  value="1">VNĐ</option>--}}
                            {{--<option  value="2">%</option>--}}
                        {{--</select>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="control-group">
                    <label class="control-label" for="setAll">Áp dụng cho tất cả </label>
                    <div class="controls">
                        <input type="checkbox" id="setAll">
                        <label for="setAll"></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
        <button id="btnSubmitInfoSeat" type="button" class="btn btn-warning btn-flat">ĐỒNG Ý</button>
    </div>
</div>
<script src="/public/javascript/jquery.number.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="/public/theme/css/SimpleImage.css">
<script>

    var loai_ghe = 'ghe ghetrong', loai_ghe_id = 3, gia_cong_them = 0;
    var seatMapId_edit, numberOfRows_edit, numberOfColumns_edit, numberOfFloors_edit, vehicleTypeId_edit;
    $(document).ready(function () {
        $('#seatMapId_add').val('{{$vehicleInfo['seatMapId'] or old('seatMapId')}}');

        if ($('#seatMapId_add').val() != '') {
            showSeatMap($('#seatMapId_add').val());
        }
        $('#note').removeClass('ghichu_datghe');
        $('body').on('click', '.ghichu_datghe a', function () {
            $('.ghichu_datghe a').removeClass('active');
            $(this).addClass('active');
            loai_ghe = $(this).attr('data-seatType');
            loai_ghe_id = $(this).attr('data-TypeId');
        });

        $('body').on('click', '.khungxe input', function () {

            /*if (loai_ghe == 'ghe loidi' || loai_ghe == 'ghe ghegiucho') {
                $(this).val('');
            }*/
            // if(!($('#cbUpdateInfo')['0'].checked))
            // {
            //     $(this).attr('class', loai_ghe);
            //     if (loai_ghe == 'ghe nhavesinh') {
            //         var soghe = $(this).val();
            //         $(this).val('WC').attr('data-seatIdOld', soghe);
            //     }
            //     if ($(this).attr('data-type') == 5) {
            //         $(this).val($(this).attr('data-seatIdOld')).removeAttr('data-seatIdOld');
            //     }
            //     $(this).attr('data-type', loai_ghe_id);
            // }else{

                if($(this).is('.ghetrong') || $(this).is('.giuongnam')){
                    $('#modal_editSeat').modal('show');

                    $('#txtExtraprice').val($(this).attr('data-extraprice'));
                    $('#txtSeatId').val($(this).val());
                    var listImages =  $(this).attr('data-images').split(",");

                    $(listImages).each(function(key,item) { loadPreviewThumb(item); });
                }

            // }
            // }
        });


        $('#modal_editSeat').on('hidden', function () {
            clearPreviewThumb();
        });
        $('body').on('click','#btnSubmitInfoSeat',function(){
            var seatId = $('#txtSeatId').val(),
                extraprice = $('#txtExtraprice').val();
            if($('#setAll').is(':checked')){
                $('.khungxe input.ghetrong,input.giuongnam').each(function(i,e){
                    $(e).attr({
                        'data-extraprice': extraprice,
                        'title': 'Giá cộng thêm là: ' + moneyFormat(extraprice),
                        'data-images':getListImages().join(',')
                    });
                    $(e).addClass('edited');
                });
                // $('.khungxe').find('input[id=' + seatId.split(' ').join('-') + ']')
                //     .attr({
                //         'data-extraprice': extraprice,
                //         'title': 'Giá cộng thêm là: ' + moneyFormat(extraprice),
                //         'data-images':getListImages().join(',')
                //     });
            }else{

                $('.khungxe').find('input[value=' + seatId.split(' ').join('-') + ']')
                    .attr({
                        'data-extraprice': extraprice,
                        'title': 'Giá cộng thêm là: ' + moneyFormat(extraprice),
                        'data-images':getListImages().join(',')
                    });
                $('.khungxe').find('input[value=' + seatId.split(' ').join('-') + ']').addClass('edited');
            }
            $('#modal_editSeat').modal('hide');

            clearPreviewThumb();
        });


        $('#vehicleType').change(function () {
            $("#txt_numberOfSeats").val($('#vehicleType option:selected').attr('data-numberofseats'));
            var numberOfSeats = $('#vehicleType option:selected').attr('data-numberOfSeats');
            $.ajax({
                url: '/cpanel/seat-map/show?numberOfSeats=' + numberOfSeats,
                dataType: 'json',
                success: function (result) {
                    var html = '';
                    $(result).each(function (key, item) {
                        console.log();
                        html += '<tr id="' + item.seatMapId + '">';
                        html += '<td>' + item.seatMapId + '</td>';
                        html += '<td>' + item.vehicleTypeName + '<br>(' + item.numberOfFloors + ' Tầng)</td>';
                        html += '<td><a href="javascript:void(0)" tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i><ul class="onclick-menu-content">';
                        html += '<li><button onclick="return showSeatMap(\'' + item.seatMapId + '\')">Chọn sơ đồ</button></li>';
                        html += '<li><button onclick="return editSeatMap(\'' + item.seatMapId + '\')">Sửa</button></li>';
                        // html += '<li><button data-toggle="modal" data-target="#modal_delete" onclick="return deleteSeatMap(\'' + item.seatMapId + '\')">Xóa</button></li>';
                        html += '</ul></a></td>';
                        html += '</tr>';
                    });
                    $('#table-seatMap').html(html);
                }
            });
        })
    });

    function createSeatMapHTML() {
        numberOfRows = $('#txtSoHang').val();
        numberOfColumns = $('#txtSoCot').val();
        numberOfFloors = $('#cbb_SoTang').val();
        gia_cong_them = $('#txtGiaGhe').val();


        if (numberOfColumns <= 7 && numberOfColumns >= 2 && numberOfRows >= 4) {
            $('#them_so_do').modal('toggle');
            addSeatMap(numberOfRows, numberOfColumns, numberOfFloors);
            $('#btn_addSeatMap').hide();
        }

    }

    function showSeatMap(seatMapId) {
        $('.table tr').removeClass('active');
        $('#' + seatMapId).addClass('active');
        $('#btn_saveSeatMap').hide();
        $('#btn_editSeatMap').hide();
        $('#note').removeClass('ghichu_datghe');
        $('#btn_addSeatMap').show();
        $('#Floor1 .khungxe').html('');
        $('#Floor2 .khungxe').html('');
        $('#seatMapId_add').val(seatMapId);
        $.ajax({
            url: '/cpanel/seat-map/show/' + seatMapId,
            dataType: 'json',
            success: function (result) {
                //alert(result);
                html_floor1 = '';
                html_floor2 = '';

                Row = result['numberOfRows'];
                Column = result['numberOfColumns'];


                for (hang = 1; hang <= Row; hang++) {
                    for (cot = 1; cot <= Column; cot++) {
                        data_floor1 = '<div class="ghe loidi"></div>';
                        data_floor2 = '<div class="ghe loidi"></div>';
                        $.each(result['seatList'], function (key, item) {
                            if (item['column'] == cot && item['row'] == hang) {
                                var seatType = generateSeatType(item);
                                if (item['floor'] == 1) {
                                    /*Tầng 1*/
                                    data_floor1 = '<div data-price-seat="" title="Giá cộng thêm là: ' + moneyFormat(item['extraPrice']) + '" class="ghe ' + seatType + '">' + item['seatId'] + '</div>';
                                } else {
                                    /*Tầng 2*/
                                    data_floor2 = '<div data-price-seat="" title="Giá cộng thêm là: ' + moneyFormat(item['extraPrice']) + '" class="ghe ' + seatType + '">' + item['seatId'] + '</div>';
                                }
                            }
                        });
                        html_floor1 += data_floor1;
                        html_floor2 += data_floor2;
                    }
                    //xử lý tràn ghế trong div html
                    if (Column < 7) {
                        for (i = 0; i < 7 - Column; i++) {
                            html_floor1 += '<div class="ghe loidi-noboder"></div>';
                            html_floor2 += '<div class="ghe loidi-noboder"></div>';
                        }
                    }
                }

                $('#Floor1 .khungxe').html(html_floor1);

                if (result['numberOfFloors'] > 1) {
                    $('#Floor2 .khungxe').html(html_floor2);
                }
            }
        });
        return false;
    }

    function addSeatMap(numberOfRows, numberOfColumns, numberOfFloors) {
        $('.table tr').removeClass('active');
        $('#btn_saveSeatMap').show();
        $('#btn_editSeatMap').hide();
        $('#note').addClass('ghichu_datghe');
        $('#Floor1 .khungxe').html('');
        $('#Floor2 .khungxe').html('');

        html_floor1 = '';
        html_floor2 = '';

        for (hang = 1; hang <= numberOfRows; hang++) {
            for (cot = 1; cot <= numberOfColumns; cot++) {
                html_floor1 += '<input title="giá cộng thêm là: ' + moneyFormat(gia_cong_them) + '" data-images="" data-extraPrice="' + gia_cong_them + '" data-row="' + hang + '" data-column="' + cot + '" data-type="3" data-floor="1" class="ghe ghetrong" value="A' + hang.toString() + cot.toString() + '">';
                html_floor2 += '<input title="giá cộng thêm là: ' + moneyFormat(gia_cong_them) + '" data-images="" data-extraPrice="' + gia_cong_them + '" data-row="' + hang + '" data-column="' + cot + '" data-type="3" data-floor="2" class="ghe ghetrong" value="B' + hang.toString() + cot.toString() + '">';
            }

            //xử lý tràn ghế trong div html
            if (numberOfColumns < 7) {
                for (i = 0; i < 7 - numberOfColumns; i++) {
                    html_floor1 += '<div class="ghe loidi-noboder"></div>';
                    html_floor2 += '<div class="ghe loidi-noboder"></div>';
                }
            }
        }

        $('#Floor1 .khungxe').html(html_floor1);

        if (numberOfFloors > 1) {
            $('#Floor2 .khungxe').html(html_floor2);
        }

        return false;
    }

    function getseatList() {
        var seatList = [];

        $.each($('.khungxe input'), function (key, item) {
            if ($(item).attr('data-type') > 0) {
                seat = {
                    "createTime": Date.now(),
                    "seatId": $(item).val().toUpperCase() ,
                    'seatType': $(item).attr('data-type'),
                    'row': $(item).attr('data-row'),
                    "seatStatus": "1",
                    'column': $(item).attr('data-column'),
                    'floor': $(item).attr('data-floor'),
                    'extraPrice': $(item).attr('data-extraPrice'),
                    'images':$(item).attr('data-images').split(",")
                };
                //console.log(seat);
                seatList.push(seat);
            }

        });

        return seatList;
    }

    function saveSeatMap() {

        $.post('{{action('SeatMapController@postAdd')}}', {
            "vehicleTypeId": $('#cbb_LoaiXe').val(),
            "numberOfRows": $('#txtSoHang').val(),
            "numberOfColumns": $('#txtSoCot').val(),
            "numberOfFloors":  $('#cbb_SoTang').val(),
            "seatList": getseatList(),
            "_token": '{{csrf_token()}}'

        }, function (result) {
            if (result['status'] == 'success') {
                Message('Thành công', 'Thêm sơ đồ thành công', '');
                setTimeout(function () {location.reload();}, 3000)
            } else {
                Message('Thất bại', 'Thêm sơ đồ thất bại', '');
            }
        }, 'json');

        return false;
    }

    function updateSeatMap() {

        $.post('{{action('SeatMapController@edit')}}', {
            "vehicleTypeId": vehicleTypeId_edit,
            "numberOfRows": numberOfRows_edit,
            "numberOfColumns": numberOfColumns_edit,
            "numberOfFloors": numberOfFloors_edit,
            "seatList": getseatList(),
            "seatMapId": seatMapId_edit,
            "_token": '{{csrf_token()}}'

        }, function (result) {
            if (result['status'] == 'success') {
                Message('Thành công', 'Cập nhật sơ đồ thành công', '');
            } else {
                Message('Thất bại', 'Cập nhật sơ đồ thất bại', '');
            }
        }, 'json');

        return false;
    }

    function editSeatMap(seatMapId) {
        $('.table tr').removeClass('active');
        $('#' + seatMapId).addClass('active');
        $('#btn_saveSeatMap').hide();
        $('#btn_editSeatMap').show();
        $('#btn_addSeatMap').show();
        $('#note').addClass('ghichu_datghe');
        seatMapId_edit = seatMapId;
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        $('#Floor1 .khungxe').html('');
        $('#Floor2 .khungxe').html('');
        $('#seatMapId_add').val(seatMapId);
        $.ajax({
            url: '/cpanel/seat-map/show/' + seatMapId,
            dataType: 'json',
            success: function (result) {
                console.log(result);
                html_floor1 = '';
                html_floor2 = '';


                Row = result['numberOfRows'];
                Column = result['numberOfColumns'];
                numberOfRows_edit = Row;
                numberOfColumns_edit = Column;
                numberOfFloors_edit = result['numberOfFloors'];
                vehicleTypeId_edit = result['vehicleTypeId'];

                for (hang = 1; hang <= Row; hang++) {
                    for (cot = 1; cot <= Column; cot++) {
                        data_floor1 = '<input class="ghe loidi disabled" data-images="" data-floor="1" data-row="' + hang + '" data-column="' + cot + '" value="A' + hang.toString() + cot.toString() + '">';
                        data_floor2 = '<input class="ghe loidi" data-images="" data-floor="2" data-row="' + hang + '" data-column="' + cot + '" value="B' + hang.toString() + cot.toString() + '">';
                        $.each(result['seatList'], function (key, item) {
                            if (item['column'] == cot && item['row'] == hang) {
                                var seatType = generateSeatType(item);
                                if (item['floor'] == 1) {
                                    /*Tầng 1*/
                                    if (seatType != 'nhavesinh') {
                                        data_floor1 = '<input title="Giá cộng thêm là: ' + moneyFormat(item['extraPrice']) + '" data-images="' + item['images'].join(',') + '" data-extraPrice="' + item['extraPrice'] + '" class="ghe ' + seatType + '" id="' + item['seatId'].split(' ').join('-') + '" data-seatidold="A' + hang.toString() + cot.toString() + '" value="' + item['seatId'] + '" data-type="' + item['seatType'] + '" data-row="' + item['row'] + '" data-column="' + item['column'] + '" data-floor="' + item['floor'] + '">';
                                    } else {
                                        data_floor1 = '<input title="Giá cộng thêm là: ' + moneyFormat(item['extraPrice']) + '" data-images="' + item['images'].join(',') + '" data-extraPrice="' + item['extraPrice'] + '" class="ghe ' + seatType + '" value="' + item['seatId'] + '" data-type="' + item['seatType'] + '" data-row="' + item['row'] + '" data-column="' + item['column'] + '" data-floor="' + item['floor'] + '">';
                                    }
                                } else {
                                    /*Tầng 2*/
                                    if (seatType != 'nhavesinh') {
                                        data_floor2 = '<input title="Giá cộng thêm là: ' + moneyFormat(item['extraPrice']) + '" data-images="' + item['images'].join(',') + '" data-extraPrice="' + item['extraPrice'] + '" class="ghe ' + seatType + '" id="' + item['seatId'].split(' ').join('-') + '" data-seatidold="B' + hang.toString() + cot.toString() + '" value="' + item['seatId'] + '" data-type="' + item['seatType'] + '" data-row="' + item['row'] + '" data-column="' + item['column'] + '" data-floor="' + item['floor'] + '">';
                                    } else {
                                        data_floor2 = '<input title="Giá cộng thêm là: ' + moneyFormat(item['extraPrice']) + '" data-images="' + item['images'].join(',') + '" data-extraPrice="' + item['extraPrice'] + '" class="ghe ' + seatType + '" value="' + item['seatId'] + '" data-type="' + item['seatType'] + '" data-row="' + item['row'] + '" data-column="' + item['column'] + '" data-floor="' + item['floor'] + '">';
                                    }
                                }
                            }
                        });
                        html_floor1 += data_floor1;
                        html_floor2 += data_floor2;
                    }
                    //xử lý tràn ghế trong div html
                    if (Column < 7) {
                        for (i = 0; i < 7 - Column; i++) {
                            html_floor1 += '<div class="ghe loidi-noboder"></div>';
                            html_floor2 += '<div class="ghe loidi-noboder"></div>';
                        }
                    }
                }

                $('#Floor1 .khungxe').html(html_floor1);

                if (result['numberOfFloors'] > 1) {
                    $('#Floor2 .khungxe').html(html_floor2);
                }
            }
        });

        return false;
    }

    function generateSeatType(seat) {

        switch (seat['seatType']) {
            case 1:
                seatType = 'ghegiucho';//CỬA
                break;
            case 2:
                seatType = 'ghedadat';//GHẾ TÀI XẾ
                break;
            case 3:
                seatType = 'ghetrong';//GHẾ THƯỜNG
                break;
            case 4:
                seatType = 'giuongnam';
                break;
            case 5:
                seatType = 'nhavesinh';
                break;
            case 6:
                seatType = 'ghephuxe';
                break;
            default:
                seatType = 'loidi';
                break;
        }
        return seatType;
    }

    function deleteSeatMap(seatMapId) {
        $('#delete_seatMap').val(seatMapId);
        return false;
    }

    function moneyFormat(money) {
        return $.number(money) + ' VNĐ';
    }

</script>

<script>
    // Config
    // Hình ảnh tối đa được upload
    var maxImageUpload = 3;

    $(document).ready(function () {

        $("#imgUpload").on("click", function () {
            if ($("section#preview").children('.content-thumb').length >= maxImageUpload) {
                Message('Cảnh báo',"Bạn không được tải lên quá " + maxImageUpload + " hình",'');
                return false;
            }
        });

        // Nhấn nút upload chọn file
        $("#imgUpload").change(function () {
            if ($("section#preview").children('.content-thumb').length >= maxImageUpload) {
                Message('Cảnh báo',"Bạn không được tải lên quá " + maxImageUpload + " hình",'');
            } else {
                UploadImage(CreatePreviewThumb());
            }
        });
        $('body').on('click','.iconRemove',function () {
            $(this).parent('.content-thumb').remove();
        });
    });



    function UploadImage(elem) {
        var form = new FormData($("#frmUploadImage")[0]);
        $.ajax({
            type: 'POST',
            url: '/cpanel/system/upload-image',
            data: form,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false
        }).done(function (data) {
            elem.children(".preview-thumb").attr("data-path", data.url);
            elem.children(".preview-thumb").css("background-image", "url('" + data.url + "')");
        });
    }

    function CreatePreviewThumb() {
        var html = '<div class="content-thumb">' +
            '<span class="iconRemove" title="Xóa hình"></span>' +
            '<div class="preview-thumb" data-path style="background-image: url(/public/images/loading/giphy-downsized.gif);"></div>' +
            '</div>';
        return $(html).appendTo($("section#preview"));
    }
    function clearPreviewThumb() {
        $(".preview-thumb").each(function (key,item) {
            $("section#preview").html('');
        });
    }
    function getListImages() {
        var listImage=[];

        $("section#preview .preview-thumb").each(function (key,item) {
            listImage.push($(item).attr("data-path"))
        });
        return listImage;
    }
    function loadPreviewThumb(url) {

        var html = '';
        if(url!=''){
            html = '<div class="content-thumb">' +
                '<span class="iconRemove" title="Xóa hình"></span>' +
                '<div class="preview-thumb" data-path="'+url+'" style="background-image: url('+url+');"></div>' +
                '</div>';
        }
        return $(html).appendTo($("section#preview"));

    }
</script>