@extends('cpanel.template.layout')
@section('title', 'Danh sách chuyến đi')
@section('content')
    <link href="/public/bootstrap/css/bootstrap-grid.min.css" type="text/css" rel="stylesheet">
    <style>
        .header-trip{padding:5px 10px;font-weight:bold}
        .header-trip .time{font-size:13px;line-height: 15px;color:#0b5fc6}
        .header-trip .number-plate{border:1px solid #333;float:right;padding:0px 5px;font-size:12px;line-height: 17px;border-radius:5px;cursor: pointer}
        .infomation-trip{background: #eee;padding:1px 10px;font-size: 10px}
        .progress{width:100%;background: #aaa;text-align:center;height:20px;position: relative;margin:0px}
        .probar{text-align:center;line-height:20px;color:#fff;font-weight: bold;}
        .textbar{line-height:22px;}
        .select2-container {
            z-index: 10002;
            width: 100% !important;
        }
        .wickedpicker {
            z-index: 99999;
        }
        .bg-red{background: #aaa}
        .popover {
            z-index: 215000000 !important;
        }
        .form-horizontal .control-group{margin-bottom: 0}
        th,td{cursor:pointer;}
        .actionTrip{position: absolute;right: 15px;bottom:10px;opacity: 0;}
        td:hover{}
        .trip-information:hover .actionTrip{opacity: 1;transition: opacity 0.5s;z-index: 2}
        .actionTrip .btn{margin:5px;}
        .trip-information{position: relative;}
        .border-top{border-top:1px solid #000;}
        input[type='checkbox']{outline:none}
        .dropdown-list-route{position: relative}
        .dropdown-list-route-content{z-index: -1;position: absolute;background: #fff;width:400px;opacity: 0;border:1px solid #aaa;margin-top:2px;}
        .open .dropdown-list-route-content{z-index: 2;opacity: 1}
        .no-margin{margin:0}
        .loading{cursor:progress}
        .q-panel{width: 100%;background: #fff;border:1px solid #0000CC;margin-bottom: 10px;}
        .q-panel-title{
            font-weight: bold;
            text-transform: uppercase;
            line-height: 7px;
            background: #084388;
            color: #fff;
            padding: 10px;
        }
        .q-panel-content{
            padding: 10px;
            background: #525050;
        }
        #lockAllInModal{
             background: red;color: #fff;padding:2px 5px;border:1px solid #fff;
         }
        #lockAllInModal:hover{
            color:red;
            background: #fff;
            border:1px solid red;
        }
        #openLockAllInModal{
            background: #00aa00;color: #fff;padding:2px 5px;border:1px solid #fff;
        }
        #openLockAllInModal:hover{
            color:#00aa00;
            background: #fff;
            border:1px solid #00aa00;
        }
        .lock{text-decoration: line-through}
        .q-box-5{width: 19%;height: 100px;background: #ddd;margin:0.5%}
        .modal-mobile{width: 400px;margin-left: -10%;}
        @media(max-width : 500px) {
            .modal-mobile{width: 350px;margin-left: -50%;}
        }
        @media (max-width: 1000px) {
            .modal-mobile{width: 350px;margin-left: -20%;}
            .actionTrip{position: static;text-align: center;opacity: 1}
        }
    </style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Danh sách chuyến xe</h3></div>
                <div class="pull-right span4 t_align_r">
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i
                                        class="icon-print"></i> In
                            </button>
                            <div class="btn-group export-btn">
                                <button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất
                                    file <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="excel" data-fileName="danh_sach_chuyen">Excel</a></li>
                                    <li><a id="pdf" data-fileName="danh_sach_chuyen">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="khung_lich_ve">
            <div class="date_picker">
                <div class="innerLR">
                    <form action="">
                        <div class="row-fluid">
                            <div class="row-fluid">
                                {{--<div class="span3">--}}
                                    {{--<label>Tài xế - phụ xe</label>--}}
                                    {{--<input type="text" id="txtTXPX">--}}
                                {{--</div>--}}
                                {{--<div class="span3">--}}
                                    {{--<label>Biển số xe</label>--}}
                                    {{--<input type="text" id="txtBSX">--}}
                                {{--</div>--}}
                                <div class="pull-right">
                                    <label for="txtStartDate">&nbsp; </label>
                                    <div class="dropdown-list-route">
                                        <button class="dropdown-list-route-btn" href="#" style="display: inline-block;padding: 4px 12px; margin-bottom: 0;font-size: 12px;line-height: 17px;"><i class="icon-list"></i></button>
                                        <div class="dropdown-list-route-content" style="border-radius: 0;padding: 5px;right:0;left: auto !important;">
                                            <div class="input-group span12">
                                                <input type="checkbox" id="checkAll" value="checkAll">
                                                <label for="checkAll">Tất Cả</label>
                                            </div>
                                            @foreach($listRoute as $route)
                                                <div class="input-group span6 no-margin" style="margin:0">
                                                    <input type="checkbox" value="{{ $route['routeId'] }}" id="checkRoute-{{ $route['routeId']  }}" class="routeShow">
                                                    <label for="checkRoute-{{ $route['routeId']  }}">{{ $route['routeName'] }}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="span3">
                                    <label for="txtStartDate">Ngày</label>
                                    <div class="input-append">
                                        <input value="{{request('startDate',date('d-m-Y'))}}"
                                               id="txtStartDate" name="startDate" type="text">
                                    </div>
                                </div>
                                <div class="span3">
                                    <span class="hidden-phone">
                                        <label>&nbsp;</label>
                                    </span>
                                    <button style="width:220px;border-radius:0px" class="btn btn-info"
                                            id="search">TÌM CHUYẾN XE
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="table-trip" style="margin: 10px 10px 20px 10px" class="table-responsive">
            <div class="text-center">Đang tải dữ liệu</div>
        </div>
        <div class="box-list-route" style="width: 99%;padding: 10px;">
        </div>
    </div>

    <div class="modal hide fade no_border modal-mobile" id="modal_delete" style="display: none;">
        <div class="modal-body center">
            <p>Bạn có chắc muốn xoá?</p>
        </div>
        <form action="" id="frmCancelTrip">
            <div class="modal-footer">
                <a style="cursor: pointer" data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn btn-warning btn-flat btnDoDelete">ĐỒNG Ý</button>
            </div>
        </form>
    </div>

    <div class="modal hide fade no_border modal-mobile" id="modal_update">
        <div class="modal-header header_modal center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3>XẾP XE</h3>
        </div>
        <form action="{{action('TripController@postEdit')}}" method="post">
            <div class="modal-body">
                <div class="form-horizontal row-fluid">
                    <div>(<span class="required">*</span>) Bắt buộc</div>
                    <div class="span12 tripInfo" style="text-align:center"></div>
                    <div class="control-group">
                        <label for="sl_type">Khóa bán vé ( Bạn có thể <span id="lockAllInModal">Khóa tất cả</span> hoặc <span id="openLockAllInModal">Bỏ khóa</span></label>
                        <div class="cancelsell">
                            <select id="sl_type" name="" multiple>
                                <option value="1">Website quản lí</option>
                                <option value="2">Website bán vé online</option>
                                <option value="3">Trên di động</option>
                                <option value="4">Đại lý</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="cbb_vehicleId">Thời gian xuất bến</label>
                        <div class="input-group clockpicker">
                            <input type="text" class="span12" value="09:30" id="startTime">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="cbb_vehicleId">Biển số (<span class="required">*</span>)</label>
                        <select data-validation-error-msg="Vui lòng chọn xe" data-validation="required" class="span12 listVehicleOption" name="vehicleId" id="cbb_vehicleId">
                        </select>
                    </div>
                    <div class="control-group">
                        <label for="cbb_listDriverId">Tài xế</label>
                        <select multiple class="span12 listDriverOption" name="listDriverId[]" id="cbb_listDriverId">
                            <option  value="">Chọn tài xế</option>
                        </select>
                    </div>
                    <div class="control-group">
                        <label for="cbb_listAssistantId">Phụ xe</label>
                        <select multiple class="span12 listAssistantOption" name="listAssistantId[]" id="cbb_listAssistantId">
                            <option value="">Chọn phụ xe</option>
                        </select>
                    </div>
                    <div class="control-group">
                        <label for="txtNote">Ghi chú</label>
                        <textarea name="txtNote" id="txtNote" style="width: 100%;" rows="3"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row-fluid">
                    <div class="span6 offset3">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="scheduleId" id="txtScheduleId" value="">
                        <input type="hidden" name="dateRun" id="txtDateRun" value="">
                        <button class="btn btn-warning btn-flat-full btnXepXe">LƯU</button>
                    </div>
                    <div class="span3">
                        <button type="reset" class="btn btn-default btn-flat-full" data-dismiss="modal">HỦY</button>
                    </div>


                </div>
            </div>
        </form>
    </div>

    <div class="modal hide fade no_border" id="modal_update_trip">
        <div class="modal-header header_modal center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3>SỬA THỜI GIAN XUẤT BẾN</h3>
        </div>
        <form action="{{action('TripController@postEditTrip')}}" method="post">
            <div class="modal-body">
                <div class="form-horizontal row-fluid">
                    <label for="txt_newDate">Thời gian xuất bến</label>
                    <input readonly class="span12" type="text" name="newDate" id="txt_newDate">
                </div>
            </div>
            <div class="modal-footer">
                <div class="row-fluid">
                    <div class="span6 offset3">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="scheduleId" id="scheduleId" value="">
                        <input type="hidden" name="tripId" id="tripId" value="">
                        <input type="hidden" name="dateRun" id="dateRun" value="">
                        <input type="hidden" name="day" id="day" value="">
                        <button class="btn btn-warning btn-flat-full">LƯU</button>
                    </div>
                    <div class="span3">
                        <button type="reset" class="btn btn-default btn-flat-full" data-dismiss="modal">HỦY</button>
                    </div>


                </div>
            </div>
        </form>
    </div>
    <!-- Table Export To File -->
    <script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
    <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>
    <link rel="stylesheet" href="/public/javascript/wickedpicker/wickedpicker.css">
    <script type="text/javascript" src="/public/javascript/wickedpicker/wickedpicker.js"></script>
    <script>
        var database = firebase.database();
        var listVehicle = {!! json_encode($listVehicle,JSON_PRETTY_PRINT) !!},
            listDriveAndAssitant = {!! json_encode($listDriverAndAssistant,JSON_PRETTY_PRINT) !!},
            listRoute = {!! json_encode($listRoute,JSON_PRETTY_PRINT) !!},
            listDriver=[],
            listAssistant=[],
            listTrip = [],
            tripSelected={},
            indexSelected=-1,
            urlUpdateTrip = '{{action('TripController@postEdit')}}',
            urlCancelTrip = '{{action('TripController@cancelTrip')}}';

        var dataTable = {
            listTime:[],
            listRouteShow : []
        }
        var listConnectionRealTime = [];

        $(function () {
            $('#sl_type').select2();
            $('.clockpicker').clockpicker({autoclose:true});
            $('#cbb_listDriverId,#cbb_listAssistantId,#cbb_vehicleId').select2({});
            $('body').on('click','.btnUpdate',function() {
                var tripId=$(this).data('trip-id');
                var startTime = $(this).data('start-time');
                var scheduleId=$(this).data('schedule-id');
                tripSelected = $.grep(listTrip,function(e,i){
                    return e.tripId==tripId&&e.startTime==startTime&&e.scheduleId==scheduleId;
                })[0]||{};
                indexSelected = $(this).data('index');
                $('#startTime').val(getTime(tripSelected.startTime));
                $('.tripInfo').html('<strong>'+tripSelected.routeName+ '</strong><br>' +formatDate(tripSelected.startTime));
                $('#cbb_vehicleId').val(tripSelected.numberPlate).change();
                $('#cbb_listDriverId').val(getListNameWithArrayUser(tripSelected.listDri).userId).trigger('change');
                $('#cbb_listAssistantId').val(getListNameWithArrayUser(tripSelected.listAss).userId).trigger('change');
                $('#txtDateRun').val(tripSelected.startTime);
                $('#txtNote').val(tripSelected.note||'');
                $('#txtScheduleId').val(tripSelected.scheduleId);
                $('#sl_type').val(tripSelected.listLockTrip||[]).change();
                $('#modal_update').modal('show');
            });
            $('body').on('click','.btnDelete',function(){
                var tripId=$(this).data('trip-id');
                var startTime = $(this).data('start-time');
                var scheduleId=$(this).attr('data-schedule-id');
                tripSelected = $.grep(listTrip,function(e,i){
                    return e.tripId==tripId&&e.startTime==startTime&&e.scheduleId==scheduleId;
                })[0]||{};
                indexSelected = $(this).data('index');
                $('#modal_delete').modal('show');
            });
            $('body').on('click','.btnDoDelete',function(){
                $.ajax({
                    url : urlCancelTrip,
                    type : 'get',
                    dataType : 'json',
                    data : {
                        tripId : tripSelected.tripId,
                        scheduleId :tripSelected.scheduleId,
                        startTime : tripSelected.startTime
                    },
                    async :false,
                    success : function (data) {
                        if(data.code==200){
                            listTrip.splice(indexSelected,1);
                            generateTableListSchedule();
                            Message('Thông báo','Hủy chuyến thành công','');
                        }else{
                            if(data.results.error.message==='TRIP_00000058'){
                                notyMessage("Chuyến đã có vé không thể hủy",'warning');
                            }else{
                                Message('Lỗi','Hủy chuyến thất bại','');
                            }
                        }
                    },
                    error : function(){
                        Message('Lỗi','Gửi yêu cầu thất bại','');
                    }
                });
                $('#modal_delete').modal('hide');
                return false;
            });
            $('body').on('click','.btnXepXe',function(){
                var ele = $(this);
                listLockTrip = $('#sl_type').val()||"[]";
                $(ele).addClass('loading');
               $.ajax({
                   url : urlUpdateTrip,
                   type:'post',
                   dataType : "json",
                   data : {
                        _token : '{{csrf_token()}}',
                       scheduleId : tripSelected.scheduleId,
                       dateRun : tripSelected.startTime,
                       vehicleId : $('#cbb_vehicleId option:selected').data('vehicle-id'),
                       listDriverId : $('#cbb_listDriverId').select2('val'),
                       listAssistantId : $('#cbb_listAssistantId').select2('val'),
                       startTime : $('#startTime').val(),
                       note : $('#txtNote').val(),
                       listLockTrip : listLockTrip
                   },
                   async :false,
                   success:function (data) {
                        if(data.code==200){
                            listTrip[indexSelected] = data.results.schedule;
                            generateTableListSchedule();
                        }else{
                            notyMessage('Lỗi !'+(data.results.message||' Không rõ nguyên nhân'),'');
                        }
                       $(ele).removeClass('loading');
                   },
                   error:function () {
                       notyMessage('Lỗi ! Gửi yêu cầu thất bại','');
                       $(ele).removeClass('loading');
                   }
               });
               $('#modal_update').modal('hide');
               return false;
            });
            classifyDriverAndAssistant();
            initFunc();
            $('#search').click(function(){
               initFunc();
               return false;
            });

            $('body').on('click','#checkAll',function(){
                if($(this).is(':checked')){
                    $('.routeShow').prop('checked',true);
                }else{
                    $('.routeShow').prop('checked',false);
                }
                saveLocalListColumnTripShow();
                generateTableListSchedule();
            });

            $('body').on('change','.routeShow',function(){
                val= $(this).val();
                full = true;
                $.each($('.routeShow'),function(i,e){
                    if(!$(e).is(':checked')){
                       full = false;
                    }
                });
                if(full){
                    $('#checkAll').prop('checked',true).change();
                }else{
                    $('#checkAll').prop('checked',false).change();
                }
                listColumnTripShowVer1 = JSON.parse(localStorage.getItem('listColumnTripShowVer1')||'[]');
                if($(this).is(':checked')){
                    listColumnTripShowVer1.push($(this).val());
                }else{
                    $.each(listColumnTripShowVer1,function(i,e){
                       if(val===e){
                           listColumnTripShowVer1.splice(i,1);
                       }
                    });
                }
                localStorage.setItem('listColumnTripShowVer1',JSON.stringify(listColumnTripShowVer1));
                generateTableListSchedule();
            });

            $('body').on('click','.dropdown-list-route-btn',function(){
                $('.dropdown-list-route').toggleClass('open');
                return false
            });

            $('body').on('click','.number-plate',function(){
                sessionStorage.setItem('tripId',$(this).data('trip-id'));
                sessionStorage.setItem('scheduleId',$(this).data('schedule-id'));
                sessionStorage.setItem('date',$('#txtStartDate').val());
                sessionStorage.setItem('routeId',$(this).data('route-id'));
                openInNewTab('{{ action('TicketController@sellVer1') }}');
            });
            $('body').on('click','.btnLockAll',function(){

                var tripId=$(this).data('trip-id');
                var startTime = $(this).data('start-time');
                var scheduleId=$(this).data('schedule-id');
                tripSelected = $.grep(listTrip,function(e,i){
                    return e.tripId==tripId&&e.startTime==startTime&&e.scheduleId==scheduleId;
                })[0]||{};
                indexSelected = $(this).data('index');
                listOC="[1,2,3,4]";
                if($(this).hasClass('lock')){
                    listOC="[]";
                }
                data = {
                    _token: '{{csrf_token()}}',
                    listOptionCancel: listOC ,
                    tripId:tripId,
                    scheduleId: scheduleId,
                    dateRun: startTime
                };
                lockTrip(data);
                return false;
            });

            //tăng cường
            $('body').on('click','.btnSchedulePlus',function(){
                var routeId = $(this).data('route-id');
                $('#routeSchedulePlus').val(routeId).change();
                $('#dateSchedulePlus').val($('#txtStartDate').val());
                $('#schedulePlus').modal('show');
            });
            $('body').on('click','#btnSchedulePlusSend',function(){
                if($('#dateSchedulePlus').val()===''){
                    Message('Cảnh báo' , 'Vui lòng chọn ngày','');
                    $('#dateSchedulePlus').focus();
                    return false;
                }
                if($('#timeStartSchedulePlus').val()===''){
                    Message('Cảnh báo' , 'Vui lòng chọn giờ','');
                    $('#timeStartSchedulePlus').focus();
                    return false;
                }
                $('#btnSchedulePlusSend').prop('disabled',true);
                var listDate = [];
                listDate.push($('#dateSchedulePlus').val());
                $.ajax({
                    url : '{{ action('ScheduleController@postAdd') }}',
                    dataType : 'json',
                    type : 'post',
                    data : {
                        _token : '{{ csrf_token() }}',
                        scheduleType : 1 ,
                        startDate : $('#dateSchedulePlus').val(),
                        startTime : $('#timeStartSchedulePlus').val(),
                        routeId : $('#routeSchedulePlus').select2('val'),
                        listAssistantId : $('#assSchedulePlus').select2('val'),
                        listDriverId : $('#driveSchedulePlus').select2('val'),
                        vehicleId : $('#vehicleSchedulePlus option:selected').data('vehicle-id'),
                        listDate : listDate,
                    },
                    success : function (data) {
                        if(data.code==200){
                            notyMessage('Tạo chuyến tăng cường thành công','success');
                            initFunc();
                            $('#schedulePlus').modal('hide');
                        }else{
                            Message('Lỗi','Tạo chuyến thất bại','');
                        }
                        $('#btnSchedulePlusSend').prop('disabled',false);
                    } ,
                    error : function (err) {
                        Message('Lỗi','Có lỗi xảy ra khi gửi yêu cầu','');
                        $('#btnSchedulePlusSend').prop('disabled',false);
                    }
                });
            });

            $('#lockAllInModal').click(function () {
                $.each($('#sl_type>option'),function(i,e){
                    $(e).prop('selected',true);
                })
                $('#sl_type').change();
            });
            $('#openLockAllInModal').click(function () {
                $.each($('#sl_type>option'),function(i,e){
                    $(e).prop('selected',false);
                })
                $('#sl_type').change();
            })
        });

        function openInNewTab(url) {
            var win = window.open(url, '_blank');
            win.focus();
        }

        function lockTrip(dataSend) {
            $.ajax({
                url: '{{action('TripController@updateLockTrip')}}',
                data: dataSend,
                async:false,
                success: function (data) {
                    console.log(data);
                    if (data.code === 200){
                        listTrip[indexSelected] = data.results.schedule;
                        generateTableListSchedule();
                        notyMessage('Cập nhật khóa chuyến thành công',"success");
                    }
                    else notyMessage('Khóa chuyến thất bại','error');
                }
            });
        }

        function initTableListTrip() {
            $('#table-trip').html('<div class="text-center">Đang tải dữ liệu</div>');
            $('#search').prop('disabled', true);
            $.ajax({
               url : '{{ action('TripController@getListTrip') }}',
               type : 'get',
               dataType : 'json',
               data : {
                    startDate : $('#txtStartDate').val(),
                   endDate : $('#txtEndDate').val(),
                   routeId : $('#cbb_Tuyen').val(),
               },
                async : true,
                success : function (data) {
                   //console.log(data);
                    if(data.code==200){
                        listTrip= data.results.listSchedule;
                        generateTableListSchedule();
                        makeListConnectionRealTimeAllTrip();
                    }else{
                        Message('Lỗi','Lấy danh sách chuyến đi thất bại','');
                    }
                    $('#search').prop('disabled', false);
                },
                error : function (data) {
                    Message('Lỗi lấy danh sách chuyến đi','Gửi yêu cầu thất bại','');
                    $('#search').prop('disabled', false);
                }
            });
        }

        function generateTableListSchedule() {
            getListRouteShow();
            $('#table-trip').html('');
//            getListTimeStart();
            htmlx = '';
            $.each(dataTable.listRouteShow,function(i,e){
                htmlx += '<div class="q-panel">';
                    htmlx+= '<div class="q-panel-title"><span>';
                    htmlx += e.routeName;
                    htmlx += '</span><span class="btn btn-mini pull-right btnSchedulePlus" style="margin-top: -8px;" title="Thêm chuyến tăng cường" data-route-id="'+e.routeId+'"><i class="fas fa-plus"></i></span>';
                    htmlx+= '</div>';
                    htmlx+= '<div class="q-panel-content"><div class="list-trip-route-'+e.routeId+'"></div>';
                    htmlx+= '<div class="clearfix"></div></div>';
                htmlx+='</div>';
            });
            $('.box-list-route').html(htmlx);
            if(dataTable.listRouteShow.length===0){
                $('.dropdown-list-route').addClass('open');
            }
            bindInfoTrip();
        }
        function bindInfoTrip() {
            $.each(listTrip,function (i,e) {
                lock = '';
                if(typeof e.listLockTrip !=='undefined' && e.listLockTrip.length===4){
                    lock='lock';
                }
                bg = '';
                if(e.startTime < <?= round(microtime(true) * 1000);?>){
                    bg ='bg-red';
                }
                htmlTrip='';
                htmlTrip +='<div class="col-md-2 col-sm-4 col-xs-6 trip-information">';
                htmlTrip += '<div style="background: #fff;border-radius:5px;margin:2px;">';
                htmlTrip += '<div class="header-trip">';
                htmlTrip += '<span class="time '+lock+'">' + getTime(e.startTime, 'time') + '</span><span title="Chuyển đến chuyến này" class="number-plate '+bg+'"  data-trip-id="'+e.tripId+'" data-route-id="'+e.routeId+'" data-schedule-id="'+e.scheduleId+'">' + e.numberPlate + '</span><div class="clearfix"></div>';//+ (e.scheduleType == 1 ? ' (TC) ' : '')
                htmlTrip += '</div>';
                htmlTrip += '<div class="infomation-trip">';
                htmlTrip += '<div><span style="font-weight:bold;font-size: 12px;">' + (getListNameWithArrayUser(e.listDri).fullName.toString()||'<span style="color:red">Chưa chọn</span>') + '</span></div>';
                htmlTrip += '<div> <span style="font-size:10px;">' + (getListNameWithArrayUser(e.listAss).fullName.toString()||'<span style="color:red">Chưa chọn</span>') + '</span></div>';
                htmlTrip += '</div>';
                htmlTrip += '<div class="footer-trip"><div class="progress totalSeatEmpty' + e.tripId + '"><div class="probar text-center" style="width:' + Math.floor((e.totalSeat - e.totalEmptySeat) * 100 / e.totalSeat) + '%;background:' + (e.totalEmptySeat == 0 ? "red" : "#4CAF50") + ';">&nbsp;</div><div class="textbar text-center soghetrong'+e.tripId+'" style="position: absolute;height: 30px;width:100%;top:0px;color:#fff;font-weight: bold">Trống : ' + (e.totalEmptySeat || "0") + '</div></div>';
                htmlTrip += '</div>';
                {{--@if(hasAnyRole(TRANSPORT_MANAGEMENT))--}}
                htmlTrip += '<div class="actionTrip">';
                htmlTrip += '<button class="btnLockAll btn btn-mini btn-primary '+lock+'" title="'+ (lock===''?'':"Mở ") +' Khóa chuyến"  data-index="'+i+'" data-trip-id="'+e.tripId+'" data-start-time="'+e.startTime+'" data-schedule-id="'+e.scheduleId+'"><i style="color:#000" class="fas fa-'+(lock?'lock':'lock-open')+'" style="color:#fff"></i></button>';
                htmlTrip += '<button class="btnUpdate btn btn-mini btn-primary"  data-index="'+i+'" data-trip-id="'+e.tripId+'" data-start-time="'+e.startTime+'" data-schedule-id="'+e.scheduleId+'" style="color:#fff"><i class="fas fa-edit" style="color:#fff"></i></button>';
                htmlTrip += '<button class="btnDelete btn btn-mini btn-danger" data-index="'+i+'" style="font-weight: bold" data-trip-id="'+e.tripId+'" title="Xóa chuyến" data-start-time="'+e.startTime+'" data-schedule-id="'+e.scheduleId+'"><i class="icon-trash" style="color:#fff"></i></button>'
                htmlTrip+='</div>';
                {{--@endif--}}
                htmlTrip += '</div>';
                htmlTrip += '</div>';
                divApp = $('.list-trip-route-'+e.routeId);
                index = $(divApp).children('div.trip-information').length;
                if(index%6===5){
                    htmlTrip+='<div class="clearfix"></div>';
                }
                $(divApp).append(htmlTrip);


//                divApp = $('.trip-'+e.startTime+'-'+e.routeId);
//                classBorder = $(divApp).text()!=='' ? 'border-top' : '';
//                html='';
//                html+='<div class="trip-information '+classBorder+'">';
//                html+='<div>'+e.numberPlate+'</div>';
//                html+='<div style="font-weight: bold">'+(getListNameWithArrayUser(e.listDri).fullName.toString()||'<span style="color:red">Chưa chọn</span>')+'</div>'+(getListNameWithArrayUser(e.listAss).fullName.toString()||'<span style="color:red">Chưa chọn</span>')+'<br><span class="totalSeatEmpty'+e.tripId+'" style="color:blue;font-size: 15px;font-weight: bold">'+(e.totalEmptySeat||0)+'</span>';
//                html+='<div class="actionTrip">';
//                html+='<button class="btnUpdate btn btn-primary"  data-index="'+i+'" data-trip-id="'+e.tripId+'" data-start-time="'+e.startTime+'" data-schedule-id="'+e.scheduleId+'"><i class="icon-edit" style="color:#fff"></i></button>';
//                html+='<button class="btnDelete btn btn-danger" data-index="'+i+'" style="font-weight: bold" data-trip-id="'+e.tripId+'" title="Xóa chuyến" data-start-time="'+e.startTime+'" data-schedule-id="'+e.scheduleId+'"><i class="icon-trash" style="color:#fff"></i></button>'
//                html+='</div>';
//                html+='</div>';
//                $(divApp).append(html);
            });
        }
        function getListRouteShow() {
            dataTable.listRouteShow=[];
            listColumnTripShowVer1 = JSON.parse(localStorage.getItem('listColumnTripShowVer1')||'[]');
            $.each(listColumnTripShowVer1,function(i,e){
                $.each(listRoute,function (k,v) {
                    if(e===v.routeId){
                        dataTable.listRouteShow.push(v);
                    }
                });
            });
            // $.each($('.routeShow'),function(i,e){
            //     routeId= $(e).val();
            //     if($(e).is(':checked')){
            //         dataTable.listRouteShow.push($.grep(listRoute,function(k,v){
            //             return routeId===k.routeId;
            //         })[0]||[]);
            //     }
            // });
        }
        function generateHtmlDriver() {
            html='';
            $.each(listDriver,function (i,e) {
                html+='<option value="'+e.userId+'">'+e.fullName+'</option>'
            });
            return html;
        }

        function generateHtmlAss() {
            html='';
            $.each(listAssistant,function (i,e) {
                html+='<option value="'+e.userId+'">'+e.fullName+'</option>'
            });
            return html;
        }
        function generateHtmlVehicle() {
            html='';
            $.each(listVehicle,function (i,e) {
                html+='<option data-vehicle-id="'+e.vehicleId+'" value="'+e.numberPlate+'">'+e.numberPlate +' ('+e.numberOfSeats+' chỗ)</option>'
            })
            return html;
        }
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;

        }
        function formatDate(int) {
            var date = new Date(int);
            var day = date.getDay();
            var hh = date.getHours();//giờ
            var ms = date.getMinutes();//phút
            var dd = date.getDate();//ngày
            var mm = date.getMonth()+1; //January is 0!
            if(day==0){
                day= 'Chủ nhật';
            }else{
                day = 'Thứ '+ parseInt(day+1)
            }

            var yyyy = date.getFullYear();
            if(dd<10){
                dd='0'+dd;
            }
            if(mm<10){
                mm='0'+mm;
            }
            if(hh<10){
                hh='0'+hh;
            }
            if(ms<10){
                ms='0'+ms;
            }
            return day + ' Lúc '+ hh+':'+ms+' Ngày : '+dd+'-'+mm+'-'+yyyy;
        }
        function getTime(int){
            var date = new Date(int);
            var hh = date.getHours();//giờ
            var ms = date.getMinutes();//phút
            if(hh<10){
                hh='0'+hh;
            }
            if(ms<10){
                ms='0'+ms;
            }
            return hh+':'+ms;
        }
        function getListNameWithArrayUser(arrUser) {
            var arr={fullName:[],userId:[]};
            $.each(arrUser,function(i,e){
               arr.fullName.push(e.fullName||'');
               arr.userId.push(e.userId);
            });
            return arr;
        }
        function classifyDriverAndAssistant(){
            $.each(listDriveAndAssitant,function(i,e){
               if(e.userType==2){
                   listDriver.push(e);
               }
               if(e.userType==3){
                   listAssistant.push(e);
               }
            });
        }

        function getListTimeStart() {
            var listTime=[];//khởi tạo danh sách rỗng
            $.each(listTrip,function(i,e){
                flag=true;
                $.each(dataTable.listRouteShow,function(x,y){
                    if(y.routeId===e.routeId){
                        flag=false;
                    }
                });

                $.each(listTime,function (k,v) {
                    if(v===e.startTime){flag=true}// nếu trong danh sách đẫ có startTime rồi thì thay đổi trạng thái để không push vào
                });
                if(flag===false){listTime.push(e.startTime);}
            });
            dataTable.listTime=listTime;
        }
        function makeListConnectionRealTimeAllTrip() {
            $.each(listConnectionRealTime, function (i, e) {
                e.off();
            });
            listConnectionRealTime = [];
            $.each(listTrip, function (i, e) {
                if (e.tripId !== '-1') {
                    cnn = database.ref().child('trip').child(e.tripId).child('totalEmptySeat');
                    cnn.on('value', function (snapshot) {
                        var data = snapshot.val();
                        listTrip[i].totalEmptySeat = data;
                        $(".soghetrong" + e.tripId).html("Trống : "+data);
                        $('.totalEmptySeat' + e.tripId).find('.probar').css('width',(Math.floor((listTrip[i].totalSeat - listTrip[i].totalEmptySeat) * 100 / listTrip[i].totalSeat))+'%')
                    });
                    listConnectionRealTime.push(cnn);
                }
            });
        }

        function saveLocalListColumnTripShow() {
            listColumnTripShowVer1 = [];
            $('.routeShow:checked').each(function(i,e){
                listColumnTripShowVer1.push($(e).val());
            });
            if(listColumnTripShowVer1.length>0){
                localStorage.setItem('listColumnTripShowVer1',JSON.stringify(listColumnTripShowVer1));
            }else{
                localStorage.removeItem('listColumnTripShowVer1');
            }
        }
        function setListChecked(){
            listColumnTripShowVer1 = JSON.parse(localStorage.getItem('listColumnTripShowVer1')||'[]');
            openBox=true;//mở form chọn tuyến
            $.each(listColumnTripShowVer1,function(k,v){
                if($('#checkRoute-'+v).length<=0){
                    listColumnTripShowVer1.splice(k,1);
                }else{
                    $('#checkRoute-'+v).prop('checked',true);
                    openBox=false;//không mở
                }
            });
            if(openBox){
                $('.dropdown-list-route').addClass('open');//mở
            }
            localStorage.setItem('listColumnTripShowVer1',JSON.stringify(listColumnTripShowVer1));//lưu lại các tuyến tồn tại
        }
        function generateListRoute(){
            html='';
            $.each(listRoute,function(i,e){
                html+='<option value="'+ e.routeId+'" '+(e.routeId===$('#chontuyen').val()?"selected":"")+'>'+e.routeName+'</option>';
            });
            $('#routeSchedulePlus').html(html);
        }
        function initSchedulePlus(){
            $('#routeSchedulePlus,#vehicleSchedulePlus,#driveSchedulePlus,#assSchedulePlus').select2({
                width: '100%',
                dropdownParent: $('#schedulePlus'),
                dropdownAutoWidth : true
            });
            $('#dateSchedulePlus').datepicker({
                autoclose: true,
                dateFormat: 'dd-mm-yy',
                zIndexOffset: 10000000,
            });
            $('#timeStartSchedulePlus').clockpicker({
                autoclose: true
            });
//        $('#dateSchedulePlus').val($('#txtCalendar').val());

//        $('#dateSchedulePlus').datepicker({
//            dateFormat : "dd-mm-yy"
//        });
        }
        function initFunc() {//khởi tạo danh sách  chuyến đi
            // date = sessionStorage.getItem('date');
            // if(typeof date !=='undefined'&&date !== ''&&date!==null){
            //     $('#txtStartDate').val(date);
            // }

            generateListRoute();
            $('.listAssistantOption').html(generateHtmlAss());
            $('.listDriverOption').html(generateHtmlDriver());
            $('.listVehicleOption').html(generateHtmlVehicle());

            setListChecked();
            initTableListTrip();
            initSchedulePlus();
        }
    </script>

    <style>
        .popover {
            z-index: 215000000 !important;
        }
    </style>
    <div class="modal modal-custom hide fade modal-mobile" id="schedulePlus">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>TẠO LỊCH TĂNG CƯỜNG</h3>
        </div>
        <div class="modal-body" style="overflow-y: visible;">
            <div class="row-fluid">
                <div class="form-horizontal row-fluid">
                    <div class="span6">
                        {{--dữ liệu từ change seatmap đổ vào--}}
                        <div class="form-group">
                            <label for="">Tuyến</label>
                            <select name="" id="routeSchedulePlus"></select>
                        </div>
                        <div class="form-group">
                            <label for="">Ngày</label>
                            <input type="text" id="dateSchedulePlus" required class="span12" placeholder="dd-mm-yy">
                        </div>
                        <div class="form-group">
                            <label for="">Tài Xế</label>
                            <select name="" id="driveSchedulePlus" class="span12 listDriverOption" multiple></select>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="form-group">
                            <label for="">Xe</label>
                            <select name="" id="vehicleSchedulePlus" class="span12 listVehicleOption"></select>
                        </div>
                        <div class="form-group">
                            <label for="">Giờ Chạy</label>
                            <input type="text" id="timeStartSchedulePlus" required class="span12" placeholder="hh:mm">
                        </div>
                        <div class="form-group">
                            <label for="">Phụ Xe</label>
                            <select name="" id="assSchedulePlus" class="span12 listAssistantOption" multiple></select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="modal-footer">
                <a style="cursor: pointer" data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button type="button" id="btnSchedulePlusSend"
                        class="btn btn-primary">ĐỒNG Ý
                </button>
            </div>

        </div>
    </div>

@endsection