@extends('cpanel.template.layout')
@section('title', 'Điều chuyển xe')
@push('activeMenu')
    activeMenu('{{action('TripController@control',[],true)}}');
@endpush
@section('content')


    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Điều chuyển xe</h3></div>
            </div>
        </div>
        <div class="row-fluid bg_light">
            <div class="widget widget-4 bg_light">
                <div class="widget-body">
                    <div class="span5 m_bottom_20" id="tripSource">
                        <table class="tb_dcx" cellpadding="3">
                            <tr>
                                <td colspan="2">Xe chuyển khách</td>
                            </tr>
                            <tr>
                                <td><b>BIỂN SỐ: </b></td>
                                <td>{{$tripSource['trip']['numberPlate']}}</td>
                            </tr>
                            <tr>
                                <td><b>TUYẾN: </b></td>
                                <td>{{$tripSource['trip']['routeName']}}</td>
                            </tr>
                            <tr>
                                <td><b>THỜI GIAN XB: </b></td>
                                <td>@dateTime(strtotime($tripSource['trip']['startDateReality'])+($tripSource['trip']['startTimeReality']/1000)+25200)</td>
                            </tr>
                        </table>
                        <div class="widget widget-2 widget-tabs tab_dcx p_bottom_0 p_0">
                            <div class="widget-head">
                                <ul>
                                    <li class="active"><a href="#ChoNgoiTang1" data-toggle="tab"><i></i><span>CHỖ NGỒI TẦNG 1</span></a>
                                    </li>
                                    <li><a href="#ChoNgoiTang2"
                                           data-toggle="tab"><i></i><span>CHỖ NGỒI TẦNG 2</span></a></li>
                                </ul>
                            </div>
                            <div class="widget-body">
                                <div class="row-fluid">
                                    <div class="tab-content">
                                        <div id="ChoNgoiTang1" class="relative tab-pane active widget-body-regular">
                                            <div class="khung_chongoi" style="width: 294px;height: 100%;">
                                            </div>
                                        </div>
                                        <div id="ChoNgoiTang2" class="relative tab-pane widget-body-regular">
                                            <div class="khung_chongoi" style="width: 294px;height: 100%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="ghichu_datghe">
                                        <p><i>Chú thích</i></p>
                                        <div class="gc_ghe gc_giucho"></div>
                                        <div class="title_ghichu_datghe">Ghế giữ chỗ</div>
                                        <div class="gc_ghe gc_dachon"></div>
                                        <div class="title_ghichu_datghe">Ghế đang chọn</div>
                                        <br>
                                        <div class="gc_ghe gc_dadat"></div>
                                        <div class="title_ghichu_datghe">Ghế đã đặt</div>
                                        <div class="gc_ghe gc_trong"></div>
                                        <div class="title_ghichu_datghe">Ghế trống</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-info btn-flat btnSelectAll m_top_10  f_right" id="search">
                            CHUYỂN HẾT
                        </button>
                    </div>
                    <div class="span2 m_bottom_20">
                        <div class="khung_chuyencho row-fluid" style="width: 100%">
                            <div class="span6 pull-left" id="khung_ck" style="width: 40px"></div>
                            <div class="span6 pull-right" id="khung_nk" style="width: 40px"></div>
                        </div>
                    </div>
                    @if(request()->has('tripDestinationId'))
                        <div class="span5" id="tripDestination">
                            <table class="tb_dcx" cellpadding="3">
                                <tr>
                                    <td colspan="2">Xe nhận khách</td>
                                </tr>
                                <tr>
                                    <td><b>BIỂN SỐ: </b></td>
                                    <td>{{@$tripDestination['trip']['numberPlate']}}</td>
                                </tr>
                                <tr>
                                    <td><b>TUYẾN: </b></td>
                                    <td>{{@$tripDestination['trip']['routeName']}}</td>
                                </tr>
                                <tr>
                                    <td><b>THỜI GIAN XB: </b></td>

                                    <td>
                                        @dateTime(strtotime($tripDestination['trip']['startDateReality'])+($tripDestination['trip']['startTimeReality']/1000)+25200)
                                    </td>
                                </tr>
                            </table>
                            <div class="widget widget-2 widget-tabs tab_dcx p_bottom_0 p_0">
                                <div class="widget-head">
                                    <ul>
                                        <li class="active"><a href="#xnk_ChoNgoiTang1" data-toggle="tab"><i></i><span>CHỖ NGỒI TẦNG 1</span></a>
                                        </li>
                                        <li><a href="#xnk_ChoNgoiTang2"
                                               data-toggle="tab"><i></i><span>CHỖ NGỒI TẦNG 2</span></a></li>
                                    </ul>
                                </div>
                                <div class="widget-body">
                                    <div class="row-fluid">
                                        <div class="tab-content">
                                            <div id="xnk_ChoNgoiTang1"
                                                 class="relative tab-pane active widget-body-regular">
                                                <div class="khung_chongoi" style="width: 294px;height: 100%;">
                                                </div>
                                            </div>
                                            <div id="xnk_ChoNgoiTang2" class="relative tab-pane widget-body-regular">
                                                <div class="khung_chongoi" style="width: 294px;height: 100%;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="ghichu_datghe">
                                            <p><i>Chú thích</i></p>
                                            <div class="gc_ghe gc_giucho"></div>
                                            <div class="title_ghichu_datghe">Ghế giữ chỗ</div>
                                            <div class="gc_ghe gc_dachon"></div>
                                            <div class="title_ghichu_datghe">Ghế đang chọn</div>
                                            <br>
                                            <div class="gc_ghe gc_dadat"></div>
                                            <div class="title_ghichu_datghe">Ghế đã đặt</div>
                                            <div class="gc_ghe gc_trong"></div>
                                            <div class="title_ghichu_datghe">Ghế trống</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <form action="{{action('TripController@postTransfer')}}" method="post">
                                <input type="hidden" name="destinationInPointId" id="destinationInPointId">
                                <input type="hidden" name="destinationOffPointId" id="destinationOffPointId">
                                <input type="hidden" name="listSeatSourceId" id="txt_dsGheNguon">
                                <input type="hidden" name="listSeatDestinationId" id="txt_dsGheDich">
                                <input type="hidden" name="tripSourceId" value="{{request('tripSourceId')}}">
                                <input type="hidden" name="tripDestinationId" value="{{request('tripDestinationId')}}">
                                <input type="hidden" name="numberOfGuests" id="numberOfGuests">
                                <input type="hidden" name="timeChange" value="{{request('getInTime')}}">
                                <input type="hidden" name="place" value="{{request('getInPointId')}}">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button onclick="return check()" class="btn btn-warning hidden-phone btn_chuyencho f_left">THỰC HIỆN CHUYỂN XE
                                </button>
                            </form>

                            <a class="f_right m_top_10 btn btn-default btn-flat"
                               href="{{url()->previous()}}">QUAY LẠI</a>
                        </div>
                    @else
                        <div class="span5 m_bottom_20" id="searchTrip">
                            <table class="tb_dcx">
                                <tr>
                                    <td colspan="2">Tìm xe nhận khách</td>
                                </tr>
                                <tr>
                                    <td class="label_tb_dcx"><b>THỜI GIAN:</b></td>
                                    <td>
                                        <div class="input-append">
                                            <input style="width:180px;" class="span10" id="txtDate" type="text"
                                                   value="{{date('d-m-Y')}}">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label_tb_dcx"><b>TUYẾN:</b></td>
                                    <td>
                                        <select name="sl_tuyen" id="chontuyen">
                                            <option value="">Chọn tuyến</option>
                                            @foreach($listRoute as $route)
                                                <option data-startPoint="{{$route['listPoint'][0]['pointId']}}"
                                                        data-endPoint="{{last($route['listPoint'])['pointId']}}"
                                                        value="{{$route['routeId']}}">{{$route['routeName']}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            <table class="table table-hover table-vertical-center m_top_10">
                                <thead>
                                <tr>
                                    <th>Biển số</th>
                                    <th>Thời gian xuất bến</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody id="trip_body_tb">


                                </tbody>
                            </table>
                            <div class="row-fluid">
                                <div class="span4">
                                    <a class="m_top_10 btn btn-default btn-flat-full"
                               href="{{action('TripController@transfer')}}">HỦY</a>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <script src="/public/javascript/jquery-dateFormat-master/dist/jquery-dateFormat.min.js"
            type="text/javascript"></script>
    <script>
        var seatMapSource = {!!json_encode($tripSource)!!};
        var seatMapDestination = {!!json_encode($tripDestination)!!};
        var listTrip = [],
            datepickerBox = $("#txtDate"),
            selectBoxRoute = $('#chontuyen');
        var ghe_nguon = 0,ghe_dich=0;

        loadSeatMap('Source', seatMapSource.trip.seatMap);
        @if(request()->has('tripDestinationId'))
        loadSeatMap('Destination', seatMapDestination.trip.seatMap);
        $('.btn_chuyencho').show();
        @endif
        $(document).ready(function () {
            selectBoxRoute.select2();
            selectBoxRoute.on('select2:select', function (e) {
                updateTripTable();
            });

            datepickerBox.datepicker({
                onSelect: function () {
                    updateTripTable();
                }
            });

            $('body').on('click', '#tripSource div.ghedadat', function () {
                $(this).toggleClass('ghedangchon');

                var khungChuyenCho_ck = $('#khung_ck'),
                    html = khungChuyenCho_ck.html(),
                    seatId = $(this).text(),
                    txtdsGheNguon = $('#txt_dsGheNguon').val();

                if (txtdsGheNguon.indexOf(seatId) !== -1) {
                    listSeatArray = txtdsGheNguon.replace(seatId, '').split(' ');
                    newStr = '';
                    listSeatArray.forEach(function (item) {
                        if (item != '') {
                            newStr += item + ' ';
                        }
                    });
                    $('#seatS_'+seatId).remove();

                    $('#txt_dsGheNguon').val(newStr);
                    ghe_nguon--;
                    if(ghe_nguon<ghe_dich)
                    {
                        txtdsGheDich = $('#txt_dsGheDich').val();
                        listSeatArray = txtdsGheDich.replace(txtdsGheDich.split(' ')[ghe_dich-1], '').split(' ');
                        newStr = '';
                        listSeatArray.forEach(function (item) {
                            if (item != '') {
                                newStr += item + ' ';
                            }
                        });
                        $('#seatD_'+txtdsGheDich.split(' ')[ghe_dich-1]).remove();
                        $("#tripDestination div:contains('"+txtdsGheDich.split(' ')[ghe_dich-1]+"')").removeClass('ghedangchon');
                        $('#txt_dsGheDich').val(newStr);
                        ghe_dich--;
                    }
                } else {
                    $('#txt_dsGheNguon').val(txtdsGheNguon+ ' ' + seatId);
                    khungChuyenCho_ck.html(html+'<div id="seatS_'+seatId+'" class="ghe ghe_den">'+seatId+'</div>');
                    ghe_nguon++;
                }

                $('#numberOfGuests').val(ghe_nguon);
            });

            $('body').on('click', '#tripDestination div.ghetrong,#tripDestination div.giuongnam', function () {

                var khungChuyenCho_nk = $('#khung_nk'),
                    html = khungChuyenCho_nk.html(),
                    seatId = $(this).text(),
                    txtdsGheDich = $('#txt_dsGheDich').val();



                    if (txtdsGheDich.indexOf(seatId) !== -1) {
                        listSeatArray = txtdsGheDich.replace(seatId, '').split(' ');
                        newStr = '';
                        listSeatArray.forEach(function (item) {
                            if (item != '') {
                                newStr += item + ' ';
                            }
                        });
                        $('#seatD_'+seatId).remove();
                        $('#txt_dsGheDich').val(newStr);
                        $(this).removeClass('ghedangchon');
                        ghe_dich--;
                    } else {
                        if($(khungChuyenCho_nk).children().length<ghe_nguon)
                        {
                            $(this).addClass('ghedangchon');
                        $('#txt_dsGheDich').val(txtdsGheDich+ ' ' + seatId);
                        khungChuyenCho_nk.html(html+'<div id="seatD_'+seatId+'" class="ghe ghe_den">'+seatId+'</div>');
                            ghe_dich++;
                        }
                    }

            });
            $('.btnSelectAll').click(function () {
                $('#tripSource div.ghedadat').each(function (key, elem) {
                    $(elem).click();
                })
            })

        });
        function check() {
            if(ghe_nguon===ghe_dich){
                return true;
            }else {
                Message('Thông Báo','Vui lòng chọn cân bằng số ghế xe chuyển khách và xe nhận khách','');
                return false;
            }
        }
        function updateTripTable() {

            var routeId = $(selectBoxRoute).val();
            var date = $(datepickerBox.datepicker()).val();
            $('#trip_body_tb').html('<tr align="center"><td class="center" colspan="3" ><img src="/public/images/loading/loader_blue.gif" alt=""></td></tr>');
            $.get('{{action('TripController@search')}}',
                {
                    'routeId': routeId,
                    'date': date,
                    'startPointId': $('#chontuyen option:selected').attr('data-startPoint'),
                    'endPointId': $('#chontuyen option:selected').attr('data-endPoint')
                },
                function (data) {

                    html = '';
                    listTrip = $.grep(data, function (item) {
                        return item.routeId == routeId;
                    });


                    listTrip.forEach(function (trip) {

                        html += '<tr data-startTime="' + trip.startTime + '" data-offTime="' + trip.endTime + '">';
                        html += '<td><span class="bienso_xe">' + trip.numberPlate + '</span></td>';
                        html += '<td>' + getFormattedDate(trip.startTime, 'date') + '</td>';
                        html += '<td><a href="' + window.location.href + '&tripDestinationId=' + trip.tripId +'&getInPointId='+trip.getInPointId+'&getInTime='+trip.getInTime+ '">Điều chuyển<i class="icon-chevron-right"></i></a></td>';
                        html += '</tr>';
                    });
                    if (html !== '') {
                        $('#trip_body_tb').html(html);
                    } else {
                        $('#trip_body_tb').html('<tr align="center"><td class="center" colspan="3" >Không có dữ liệu</td></tr>');
                    }
                }, "json")
        }

        function getFormattedDate(unix_timestamp, methor) {
            var date = new Date(unix_timestamp);
            str = '';
            if (methor === 'time') {
                str = $.format.date(date.getTime(), 'HH:mm')
            } else {
                str = $.format.date(date.getTime(), 'HH:mm, dd/MM/yyyy')
            }

            return str;
        }

        function loadSeatMap(type, result) {

            html_floor1 = '';
            html_floor2 = '';

            Row = result['numberOfRows'];
            Column = result['numberOfColumns'];
            numberOfRows_edit = Row;
            numberOfColumns_edit = Column;
            numberOfFloors_edit = result['numberOfFloors'];
            vehicleTypeId_edit = result['vehicleTypeId'];

            for (hang = 1; hang <= Row; hang++) {
                for (cot = 1; cot <= Column; cot++) {
                    data_floor1 = '<div class="ghe loidi"></div>';
                    data_floor2 = '<div class="ghe loidi"></div>';
                    $.each(result['seatList'], function (key, item) {
                        if (item['column'] == cot && item['row'] == hang) {
                            if (item['floor'] == 1) {
                                /*Tầng 1*/
                                data_floor1 = generateSeatHTML(item);
                            } else {
                                /*Tầng 2*/
                                data_floor2 = generateSeatHTML(item);
                            }

                        }
                    });
                    html_floor1 += data_floor1;
                    html_floor2 += data_floor2;
                }
                //xử lý tràn ghế trong div html
                if (Column < 7) {
                    for (i = 0; i < 7 - Column; i++) {
                        html_floor1 += '<div class="ghe loidi-noboder"></div>';
                        html_floor2 += '<div class="ghe loidi-noboder"></div>';
                    }
                }
            }

            if (type == 'Source') {
                $('#ChoNgoiTang1 .khung_chongoi').html(html_floor1);

                if (result['numberOfFloors'] > 1) {
                    $('#ChoNgoiTang2 .khung_chongoi').html(html_floor2);
                }
            } else {
                $('#xnk_ChoNgoiTang1 .khung_chongoi').html(html_floor1);

                if (result['numberOfFloors'] > 1) {
                    $('#xnk_ChoNgoiTang2 .khung_chongoi').html(html_floor2);
                }
            }


        }

        function generateSeatHTML(seat) {
            /*
                DOOR(1),//cửa
                DRIVER_SEAT(2), //ghế cho tài xế
                NORMAL_SEAT(3),//ghế thường
                BED_SEAT(4),//ghế giường nằm
                WC(5),//nhà vê sinh
                AST_SEAT(6), //ghế cho phụ xe
            */
            switch (seat['seatType']) {
                case 1:
                    seatType = 'cuaxe';
                    break;
                case 2:
                    seatType = 'taixe';
                    break;
                case 3:
                case 4:
                    seatType = checkStatusSeatAndTicket(seat.seatStatus, seat.overTime).seatStatus;
                    break;
                case 5:
                    seatType = 'nhavesinh';
                    break;
                case 6:
                    seatType = 'ghephuxe';
                    break;
                default:
                    seatType = 'loidi';
                    break;
            }
            var seatHTML = '<div ';
            seatHTML += ' data-ticketId="' + seat['listTicketId'][0] + '"';
            seatHTML += ' data-extraPrice="' + seat['extraPrice'] + '"';
            seatHTML += ' class="ghe ' + seatType + '">';
            seatHTML += seat['seatId'];
            seatHTML += '</div>';
            return seatHTML;
        }

        function checkStatusSeatAndTicket(Status, overTime) {

            /*
            *   INVALID(-2),
                CANCELED(0),
                EMPTY(1), // rỗng. hết hạn giữ chỗ sẽ chuyển về trạng thái này
                BOOKED(2), // đã giữ - (SMS khoảng cách <=10 km)
                BOUGHT(3), // đã thanh toán - (SMS khoảng cách <=10 km)
                ON_THE_TRIP(4), // đã lên xe
                COMPLETED(5), // đã hoàn thành
                OVERTIME(6),// quá giờ giữ chỗ
                BOOKED_ADMIN(7) // siêu phụ xe đặt chỗ
            * */
            var result = {seatStatus: '', TicketStatusHTML: '', TicketStatusMessage: ''};

            switch (Status) {
                case 0:
                case 1:
                case 5:
                case 6:
                    result.seatStatus = 'ghetrong';
                    result.TicketStatusHTML = 'Hết hạn giữ chỗ';
                    result.TicketStatusMessage = 'Hết hạn giữ chỗ';
                    break;
                case 3:
                case 4:
                    result.seatStatus = 'ghedadat';
                    result.TicketStatusHTML = '<div class="gc_ghe gc_dadat"></div>';
                    result.TicketStatusMessage = 'Đã thanh toán';

                    break;
                case 2:
                    result.seatStatus = 'ghegiucho';
                    result.TicketStatusHTML = '<div class="gc_ghe gc_giucho"></div>';
                    result.TicketStatusMessage = 'Giữ chỗ';
                    break;
                case 7:
                    result.seatStatus = 'ghegiucho';
                    result.TicketStatusHTML = '<div class="gc_ghe gc_giucho"></div>';
                    result.TicketStatusMessage = 'Giữ chỗ';
                    break;
                default:
                    result.seatStatus = 'ghetrong';
                    break;
            }
            return result;
        }
    </script>


@endsection