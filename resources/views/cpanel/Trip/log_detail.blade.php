

@extends('cpanel.template.layout')
@section('title', 'Chi tiết nhật trình xe')

@push('activeMenu')
    activeMenu('{{action('TripController@log',[],true)}}');
@endpush

@section('content')

    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Chi tiết nhật trình chuyến xe {{$tripId}}</h3></div>
            </div>
        </div>

        <div class="row-fluid bg_light">
            <div class="widget widget-4 bg_light">
                <div class="widget-body">
                    <table class="table table-hover table-vertical-center">
                        <thead>
                        <tr>
                            <th>Thời gian</th>
                            <th>Tên sự việc</th>
                            <th>Nội dung</th>
                            <th>Tiền</th>
                            <th>Trạng thái</th>
                            <th>Người tạo</th>
                            <th>Ghi chú</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($result as $row)
                        <tr>
                            <td>@dateFormat(strtotime($row['actionDate']))</td>
                            <td>{{$row['action']}}</td>
                            <td>{{$row['content']}}</td>
                            <td>---</td>
                            <td>{{$row['tripActivityType']}}</td>
                            <td>{{$row['userId']}}</td>
                            <td>Không có ghi chú nào</td>
                        </tr>
                       @empty
                        <tr>
                            <td class="center" colspan="7">Hiện không có dữ liệu</td>
                        </tr>
                        @endforelse
                       
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection