
@extends('cpanel.template.layout')
@section('title', 'Tạo thông báo cho khách hàng')
@section('content')

    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Tạo Thông Báo Cho Khách Hàng</h3></div>
            </div>
        </div>


        <div class="innerLR">
            <div class="row-fluid">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Tìm thông báo</h4>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">
                            <label for="type_notification">Loại thông báo</label>
                            <select name="type_notification" id="type_notification">
                                <option value="">Cảnh Báo</option>
                                <option value="1">Tiêu cực</option>
                            </select>
                        </div>

                        <div class="span3">
                            <label for="method_send">Hình thức gửi</label>
                            <select name="method_send" id="method_send">
                                <option value="">Email</option>
                                <option value="1">SMS</option>
                                <option value="2">Hệ Thống</option>
                                
                            </select>
                        </div>

                        <div class="span3">
                            <label for="conten_notification">Nội dung</label>
                            <input autocomplete="off" type="text" name="conten_notification" id="conten_notification">
                        </div>

                        <div class="span2">
                            <label class="separator hidden-phone" for="add"></label>
                            <button class="btn btn-info btn-flat-full hidden-phone" id="search">TÌM THÔNG BÁO</button>
                            <button class="btn btn-info btn-flat visible-phone" id="search">TÌM THÔNG BÁO</button>
                        </div>

                        <div class="span1">
                            <label class="separator hidden-phone" for="add"></label>
                            <button data-toggle="modal" data-target="#add_notification" class="btn btn-warning btn-flat-full hidden-phone" id="add">THÊM</button>
                            <button data-toggle="modal" data-target="#add_notification" class="btn btn-warning btn-flat visible-phone" id="add">THÊM THÔNG BÁO</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                Danh sách thông báo đã gửi cho khách hàng
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Thời gian gửi</th>
                        <th>Loại thông báo</th>
                        <th>Hình thức gửi</th>
                        <th>Nội dung</th>
                        <th>Trạng thái</th>
                        <th class="center">Tuỳ chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="center" colspan="7">Hiện tại không có dữ liệu</td>
                    </tr>
                    {{--<tr>
                        <td>1</td>
                        <td>12h00 Ngày 17/04/2017</td>
                        <td>Chúc mừng</td>
                        <td>ahihi</td>
                        <td>SMS</td>
                        <td>Không thành công</td>
                        <td class="center">
                            <a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                <ul class="onclick-menu-content">
                                    <li>
                                        <button onclick="showinfo();">Chi tiết</button>
                                    </li>
                                    <li>
                                        <button>Xóa</button>
                                    </li>
                                </ul>
                            </a>
                        </td>
                    </tr>--}}

                    </tbody>
                </table>
            </div>
            <div class="row-fluid bg_light"  style="margin-bottom:10px;display: none;" id="info">
                <div class="widget widget-4 bg_light">
                    <div class="widget-body">
                        <table class="tttk" cellspacing="20" cellpadding="4">
                            <tbody>
                            <tr>
                                <th>HÌNH THỨC GỬI</th>
                                <td>Email</td>
                                <th>NGƯỜI GỬI</th>
                                <td>Phan Diêu Tinh</td>
                            </tr>
                            <tr>
                                <th>THỜI GIAN GỬI</th>
                                <td>12h00 Ngày 17/04/2017</td>
                                <th>TRẠNG THÁI</th>
                                <td>Không thành công</td>
                            </tr>
                            <tr>
                                <th>lOẠI THÔNG BÁO</th>
                                <td>Cảnh Báo</td>
                                <th>ĐỐI TƯỢNG NHẬN</th>
                                <td>Khách hàng thân thiết</td>
                            </tr>
                            <tr>
                                <th>NỘI DUNG</th>
                                <td>Bạn đã quá 80 tuổi</td>
                                <th></th>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="modal modal-custom hide fade" id="add_notification">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 >Tạo Thông Báo Cho Khách Hàng</h3>
        </div>
        <form class="form-horizontal">
            <div class="modal-body">
                <div class="control-group">
                    <label class="control-label" for="type_notification">Loại thông báo</label>
                    <div class="controls">
                        <select  style="width: 390px" name="type_notification" id="type_notification">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="type_notification">Loại thông báo</label>
                    <div class="controls">
                        <div class="row-fluid">
                            <div class="span5">
                                <input type="checkbox" id="type_email">
                                <label for="type_email">Email</label>
                            </div>
                            <div class="span4">
                                <input type="checkbox" id="type_sms">
                                <label for="type_sms">SMS</label>
                            </div>
                            <div class="span3">
                                <input type="checkbox" id="type_system">
                                <label for="type_system">Hệ Thống</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="receiver_notification">Đối tượng nhận</label>
                    <div class="controls">
                        <div class="row-fluid">
                            <div class="span5">
                                <input name="receiver_notification[]" type="checkbox" id="receiver1">
                                <label for="receiver1">Khách hàng thường</label>
                            </div>
                            <div class="span5">
                                <input name="receiver_notification[]" type="checkbox" id="receiver2">
                                <label for="receiver2">Tất cả khách hàng</label>
                            </div>
                        </div>

                        <div class="row-fluid">
                            <div class="span5">
                                <input name="receiver_notification[]" type="checkbox" id="receiver4">
                                <label for="receiver4">Khách hàng thân thiết</label>
                            </div>
                            <div class="span5">
                                <input name="receiver_notification[]" type="checkbox" id="receiver6" >
                                <label for="receiver6"><input autocomplete="off" class="span12" name="receiver_notification[]" type="text" id="search_user" placeholder="User cụ thể"></label>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="content_notification">Nội dung</label>
                    <div class="controls">
                        <textarea style="width: 380px" id="content_notification" rows="4"></textarea>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-warning btn-flat">LƯU</a>
                <a data-dismiss="modal" aria-hidden="true" class="btn btn_huy_modal">HỦY</a>
            </div>
        </form>
    </div>
    <script type="text/javascript">
        function showinfo(){
            $('#info').show();
        }
    </script>
    <!-- End Content -->

@endsection