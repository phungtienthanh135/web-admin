
@extends('cpanel.template.layout')
@section('title', 'Tạo Thông Báo Cho Nhân Viên')
@section('content')
    <style>
        .form-horizontal .control-group{margin-bottom: 10px}
    </style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Thông Báo <button class="btn btn-warning btnAddPopup" title="Tạo mới"><i class="fa fa-plus-circle"></i></button></h3></div>
            </div>
        </div>

        <div class="separator bottom"></div>
        <div class="innerLR">
            <div class="row-fluid">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Tìm thông báo</h4>
                    </div>
                    {{--<div class="row-fluid">--}}
                        {{--<div class="span3">--}}
                            {{--<label for="type_notification">Loại thông báo</label>--}}
                            {{--<select name="type_notification" id="type_notification">--}}
                                {{--<option value=""></option>--}}
                            {{--</select>--}}
                        {{--</div>--}}

                        {{--<div class="span3">--}}
                            {{--<label for="method_send">Phương thức gửi</label>--}}
                            {{--<select name="method_send" id="method_send">--}}
                                {{--<option value=""></option>--}}
                            {{--</select>--}}
                        {{--</div>--}}

                        {{--<div class="span3">--}}
                            {{--<label for="conten_notification">Nội dung</label>--}}
                            {{--<input autocomplete="off" type="text" name="conten_notification" id="conten_notification">--}}
                        {{--</div>--}}

                        {{--<div class="span2">--}}
                            {{--<label class="separator hidden-phone" for="add"></label>--}}
                            {{--<button class="btn btn-info btn-flat-full hidden-phone" id="search">TÌM THÔNG BÁO</button>--}}
                            {{--<button class="btn btn-info btn-flat visible-phone" id="search">TÌM THÔNG BÁO</button>--}}
                        {{--</div>--}}

                        {{--<div class="span1">--}}
                            {{--<label class="separator hidden-phone" for="add"></label>--}}
                            {{--<button data-toggle="modal" data-target="#add_notification"--}}
                                    {{--class="btn btn-warning btn-flat-full hidden-phone" id="add">THÊM--}}
                            {{--</button>--}}
                            {{--<button data-toggle="modal" data-target="#add_notification"--}}
                                    {{--class="btn btn-warning btn-flat visible-phone" id="add">THÊM THÔNG BÁO--}}
                            {{--</button>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="row-fluid">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Thời gian bắt đầu</th>
                        <th>Thời gian kết thúc</th>
                        <th>Hình ảnh</th>
                        {{--<th class="center">Tuỳ chọn</th>--}}
                    </tr>
                    </thead>
                    <tbody class="listPopupHere">
                    {{--<tr>--}}
                        {{--<td class="center" colspan="7">Hiện tại không có dữ liệu</td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td>12h00 Ngày 17/04/2017</td>
                        <td>Chúc mừng</td>
                        <td>ahihi</td>
                        {{--<td class="center">--}}
                            {{--<a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>--}}
                                {{--<ul class="onclick-menu-content">--}}
                                    {{--<li>--}}
                                        {{--<button onclick="showinfo();">Chi tiết</button>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<button>Xóa</button>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</a>--}}
                        {{--</td>--}}
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="row-fluid" id="info" style="display: none;">
                <table class="tttk" cellspacing="20" cellpadding="4">
                    <tbody>
                    <tr>
                        <th>SỐ ĐIỆN THOẠI/EMAIL</th>
                        <td>0923911007</td>
                        <th>HÌNH THỨC GỬI</th>
                        <td>Email</td>
                    </tr>
                    <tr>
                        <th>THỜI GIAN GỬI</th>
                        <td>12h00 Ngày 17/04/2017</td>
                        <th>NGƯỜI GỬI</th>
                        <td>Phan Diêu Tinh</td>
                    </tr>
                    <tr>
                        <th>lOẠI THÔNG BÁO</th>
                        <td>Cảnh Báo</td>
                        <th>TRẠNG THÁI</th>
                        <td>Không thành công</td>
                    </tr>
                    <tr>
                        <th>NỘI DUNG</th>
                        <td>Bạn đã quá 80 tuổi</td>
                        <th>ĐỐI TƯỢNG NHẬN</th>
                        <td>Phụ xe,Kế toán, Nguyễn Văn Tùng</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal modal-custom hide fade" id="add_notification">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="center">TẠO THÔNG BÁO CHO NHÂN VIÊN </h3>
        </div>
        <form method="post" class="form-horizontal" enctype="multipart/form-data" id="form-upload-image">
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span7">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="popupId" id="popupId" value="">
                        {{--<div class="control-group">--}}
                            {{--<label class="" for="notifyName">Tên thông báo</label>--}}
                            {{--<input type="text" id="notifyName" class="span12">--}}
                        {{--</div>--}}
                        {{--<div class="control-group">--}}
                            {{--<label class="" for="notifyContent">Nội dung</label>--}}
                            {{--<textarea name="" id="notifyContent" class="span12" rows="3"></textarea>--}}
                        {{--</div>--}}
                        <div class="control-group">
                            <label class="" for="notifyContent">Ngày bắt đầu</label>
                            <input type="text" id="startDate" class="span12" readonly placeholder="mm/dd/yyyy" autocomplete="off">
                        </div>
                        <div class="control-group">
                            <label class="" for="notifyContent">Ngày kết thúc</label>
                            <input type="text" id="endDate" class="span12" readonly autocomplete="off" placeholder="mm/dd/yyyy">
                        </div>

                    </div>
                    <div class="span5" style="text-align: center">
                        <div class="chonanh" style="width: 100% !important;">
                            <img id="imgNotify" src="/public/images/anhsanpham.png">
                            <input type="hidden" name="locationImage" id="locationImage">
                        </div>
                        <label for="notifyImage" style="text-align: center;padding: 5px;background: #0A246A;color: #fff;line-height: 20px">Chọn Ảnh</label>
                        <input type="file" id="notifyImage" name="img" style="display: none">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <a href="#" class="btn btn-warning btn-flat savePopup">ĐỒNG Ý</a>
            </div>
        </form>
    </div>
    <script type="text/javascript">
        var listPopup = {!! json_encode($listPopup,JSON_PRETTY_PRINT) !!}
        $(document).ready(function () {
            generateListPopup();
            $('#startDate,#endDate').datepicker({dateFormat : 'mm/dd/yy'});
            $('#notifyImage').on('change',function () {
                UploadImage('form-upload-image','imgNotify','locationImage');
            });
            $('.btnAddPopup').click(function () {
                $('#popupId').val('');
                $('#add_notification').modal('show');
            });
            $('.savePopup').on('click',function () {
                if($('#startDate').val()===''){
                    $('#startDate').focus();
                    return false;
                }
                if($('#endDate').val()===''){
                    $('#endDate').focus();
                    return false;
                }
                if($('#locationImage').val()===''){
                    notyMessage("vui lòng chọn ảnh !","error");
                    return false;
                }
                var pId =$('#popupId').val(),
                    startDate = new Date($('#startDate').val()).getTime(),
                    endDate = new Date($('#endDate').val()).getTime(),
                    link = $('#locationImage').val();
                var dataSend = {
                    startDate : startDate,
                    endDate : endDate,
                    link : link,
                    companyId : '{{session('companyId')}}',
                }
                var url = '/popup/create';
                if(pId !==''&&pId!==null){
                    url = '/popup/update';
                    dataSend.id=pId;
                }
//                $('#add_notification').modal('hide');
                SendAjaxWithJson({
                    url : urlDBD(url),
                    type:'post',
                    dataType:'json',
                    data :dataSend,
                    success : function (result) {
                        notyMessage("Thực thi thành công","success");
                        location.reload();
                    },
                    functionIfError : function () {
                        
                    }
                });
            });
        });
        function showinfo() {
            $('#info').show();
        }
        function UploadImage(formId,imgId,inputId) {
            console.log(typeof $('#notifyImage').val(),$('#notifyImage').val());
            if($('#notifyImage').val()===''||$('#notifyImage').val()===null){
                $('#'+imgId).attr('src', '/public/images/anhsanpham.png');
                return false;
            }
            var form = new FormData($("#"+formId)[0]);
            $('#'+imgId).attr('src', '/public/images/loading/loader_blue.gif');
            $.ajax({
                type: 'POST',
                url: '/cpanel/system/upload-image',
                data: form,
                dataType: 'json',
                processData: false,
                contentType: false,
                cache: false
            }).done(function (data) {
                $('#'+imgId).attr('src', data.url);
                $('#'+inputId).val(data.url);
            });
        }
        function generateListPopup() {
            var html = '';
            if(listPopup.length<=0){
                $('.listPopupHere').html('<tr><td colspan="3">Không có dữ liệu</td></tr>');
                return false;
            }
            $.each(listPopup,function (i,e) {
                html += '<tr>';
                html += '<td>'+new Date(e.startDate).customFormat('#DD#/#MM#/#YYYY#')+'</td>';
                html += '<td>'+new Date(e.endDate).customFormat('#DD#/#MM#/#YYYY#')+'</td>';
                html += '<td><img src="'+(e.link||'/public/images/anhsanpham.png')+'" style="max-width: 200px;max-height: 200px" alt=""></td>';
                html += '</tr>';
            });
            $('.listPopupHere').html(html);
        }
    </script>
    <!-- End Content -->

@endsection