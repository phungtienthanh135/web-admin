@extends('cpanel.template.layout')
@section('title', 'Danh sách phương tiện')

@section('content')

    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Danh sách phương tiện</h3></div>
            </div>
        </div>
        <div class="khung_lich_ve">
            <div class="date_picker">
                <div class="innerLR">
                    <div class="row-fluid">
                        <h4>Tìm phương tiện</h4>
                        <form action="{{action('VehicleController@show')}}">
                            <div class="span2">
                                <label>Loại xe</label>
                                <select class="span12" name="vehicleTypeId" id="cbb_LoaiXe">
                                    <option value="">Chọn loại xe</option>
                                    @foreach($vehicleType as $vt)
                                        <option {{request('vehicleTypeId')==$vt['vehicleTypeId']?'selected':''}} value="{{ $vt['vehicleTypeId'] }}">{{ $vt['vehicleTypeName'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="span2">
                                <label>Biển số</label>
                                <input class="w_full" value="{{request('numberPlate')}}" type="text" name="numberPlate"
                                       id="txt_DiemDau">
                            </div>
                            <div class="span2">
                                <label>Trạng thái</label>
                                <select class="span12" name="vehicleStatus" id="cbb_TrangThai">
                                    <option {{request('vehicleStatus')==0?'selected':''}} value="">Không xác định
                                    </option>
                                    <option {{request('vehicleStatus')==1?'selected':''}} value="1">Cần xác thực
                                    </option>
                                    <option {{request('vehicleStatus')==2?'selected':''}} value="2">Sẵn sàng nhận
                                        chuyến
                                    </option>
                                    <option {{request('vehicleStatus')==3?'selected':''}} value="3">Đang chạy</option>
                                    <option {{request('vehicleStatus')==4?'selected':''}} value="4">Đang nghỉ</option>
                                    <option {{request('vehicleStatus')==5?'selected':''}} value="5">Đã xóa</option>
                                </select>
                            </div>
                            <div class="span2">
                                <label for="insuranceEndTime">Hạn bảo hiểm</label>
                                <div class="input-append span12">
                                    <input class="span12" autocomplete="off" value="{{request('insuranceEndTime')}}"
                                           id="insuranceEndTime" name="insuranceEndTime" type="text">
                                </div>
                            </div>

                            <div class="span1">
                                <label>Số chỗ</label>
                                <input class="span12" type="text" name="numberOfSeats" id=""
                                       value="{{request('numberOfSeats')}}">
                            </div>
                            <div class="span1">
                                <label>&nbsp;</label>
                                <button class="btn btn-info btn-flat-full" id="search">TÌM</button>
                            </div>
                            @if(hasAnyRole(CREATE_VEHICLE))
                                <div class="span1">
                                    <label>&nbsp;</label>
                                    <a href="{{action('VehicleController@getadd')}}"
                                       class="btn btn-warning btn-flat-full"
                                       id="add">THÊM XE</a>
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid bg_light m_top_10">
            <div class="widget widget-4 bg_light">
                <div class="widget-body">

                    <table class="table table-hover table-vertical-center">
                        <thead>
                        <tr>
                            <th>Biển số</th>
                            <th>Tên loại xe</th>
                            <th>Số chổ</th>
                            <th>Hạn đăng kiểm</th>
                            <th>Trạng thái</th>
                            <th>Tùy chọn</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($result)==0)
                            <tr>
                                <td class="center" colspan="7">Hiện không có dữ liệu</td>
                            </tr>
                        @else
                            @foreach($result as $row)
                                <tr data-seatMapId="{{$row['seatMapId']}}">
                                    <td>{{str_limit($row['numberPlate'],15)}}</td>
                                    <td>{{$row['vehicleTypeName']}}</td>
                                    <td>{{$row['numberOfSeats']}}</td>
                                    <td>@dateFormat($row['registrationDeadline'])</td>
                                    <td>
                                    @php
                                        switch($row['vehicleStatus'])
                                        {
                                        case '0':
                                        {
                                            echo  '<i class="dang_di"></i> Không xác định';
                                           break;
                                        }
                                        case '1':
                                        {
                                            echo  '<i class="dang_di"></i> Cần xác thực';
                                           break;
                                        }
                                        case '2':
                                        {
                                            echo  ' <i class="dang_nghi"></i>Sẵn sàng nhận chuyến';
                                           break;
                                        }
                                        case '3':
                                        {
                                            echo  '<i class="dang_di"></i>Đang chạy';
                                           break;
                                        }
                                        case '4':
                                        {
                                            echo  '<i class="dang_nghi"></i>Đang nghỉ';
                                           break;
                                        }
                                        case '5':
                                        {
                                            echo  '<i class="dang_di"></i>Đã xóa';
                                           break;
                                        }

                                        default:
                                        {
                                        echo  '<i class="dang_di"></i> Không xác định';
                                           break;
                                        }
                                        }
                                    @endphp
                                    <td>
                                        <a href="javascript:void(0)" tabindex="0"
                                           class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                            <ul class="onclick-menu-content">
                                                @if(hasAnyRole(UPDATE_VEHICLE))
                                                    <li>
                                                        <button onclick="javascript:void(location.href='{{action('VehicleController@getEdit',['id'=>$row['vehicleId']],false)}}')">
                                                            Sửa
                                                        </button>
                                                    </li>
                                                @endif
                                                @if(hasAnyRole(DELETE_VEHICLE))
                                                    <li>
                                                        <button data-toggle="modal" data-target="#modal_delete"
                                                                value="{{$row['vehicleId']}}" class="_delete">Xóa
                                                        </button>
                                                    </li>
                                                @endif
                                            </ul>
                                        </a>
                                    </td>
                                </tr>
                                <!--hiển thị chi tiết-->
                                <tr class="info" style="display: none">
                                    <td colspan="3">
                                        <table id="table-info-vehicle" class="tttk no_last" cellspacing="20"
                                               cellpadding="4">
                                            <tbody>
                                            <tr>
                                                <th>BIỂN SỐ</th>
                                                <td>{{str_limit($row['numberPlate'],15)}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>LOẠI XE</th>
                                                <td>
                                                    @php
                                                        $vehicleTypeId = $row['vehicleTypeId'];
                                                        $_vehicleType = array_where($listVehicleType,function ($item) use ($vehicleTypeId){
                                                                     return $item['vehicleTypeId'] == $vehicleTypeId;
                                                         })
                                                    @endphp
                                                    {{last($_vehicleType)['vehicleTypeName']}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>SỐ CHỖ</th>
                                                <td>{{$row['numberOfSeats']}}</td>
                                            </tr>
                                            <tr>
                                                <th>NGUYÊN GIÁ</th>
                                                <td>@moneyFormat($row['originPrice'])</td>
                                            </tr>
                                            <tr>
                                                <th>SỐ ĐIỆN THOẠI</th>
                                                <td>{{@$row['phoneNumber']}}</td>
                                            </tr>

                                            <tr>
                                                <th>NGÀY ĐĂNG KÝ</th>
                                                <td>@dateFormat($row['registrationDeadline'])</td>
                                            </tr>
                                            <tr>
                                                <th>THỜI GIAN KHẤU HAO</th>
                                                <td>{{$row['reduceTime']}} Tháng</td>
                                            </tr>
                                            <tr>
                                                <th>HẠN MỨC TIÊU HAO</th>
                                                <td>{{$row['estimatedFuelConsumption']}}</td>
                                            </tr>
                                            <tr>
                                                <th>SỐ KHUNG</th>
                                                <td>{{@$row['chasisNumber']}}</td>
                                            </tr>
                                            <tr>
                                                <th>SỐ MÁY</th>
                                                <td>{{@$row['engineNumber']}}</td>
                                            </tr>
                                            <tr>
                                                <th>MÃ THIẾT BỊ ĐỊNH VỊ</th>
                                                <td>{{@$row['gpsId']}}</td>
                                            </tr>
                                            <tr>
                                                <th>GIÁ TRỊ CÒN LẠI</th>
                                                <td>@moneyFormat($row['remain'])</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="3">
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <div class="ghichu_datghe_inline">
                                                    <div class="gc_ghe gc_cua"></div>
                                                    <div class="title_ghichu_datghe">Cửa</div>
                                                    <div class="gc_ghe gc_ghe_tai"></div>
                                                    <div class="title_ghichu_datghe">Ghế tài</div>
                                                    <div class="gc_ghe gc_ghe_phu_xe"></div>
                                                    <div class="title_ghichu_datghe">Ghế phụ xe</div>
                                                    <div class="gc_ghe gc_ghe_khach"></div>
                                                    <div class="title_ghichu_datghe">Ghế khách</div>
                                                    <div class="gc_ghe gc_giuong"></div>
                                                    <div class="title_ghichu_datghe">Giường nằm</div>
                                                    <div class="gc_ghe gc_nha_ve_sinh"></div>
                                                    <div class="title_ghichu_datghe">Nhà vệ sinh</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-fluid" style="width: 600px">
                                            <div id="Floor1" class="span6">
                                                <div class="khungxe" style="width: 320px"></div>
                                                <div class="center">Tầng 1</div>

                                            </div>

                                            <div id="Floor2" class="span6">
                                                <div class="khungxe" style="width: 320px"></div>
                                                <div class="center">Tầng 2</div>
                                            </div>

                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    @include('cpanel.template.pagination-without-number',['page'=>$page])
                </div>
            </div>
        </div>
    </div>
    <div class="modal hide fade in" id="edit" aria-hidden="false"
         style="margin-top: 10%;margin-left: -10%;max-width: 250px; display: none;">
        <div class="modal-body">
            <strong>BIỂN SỐ</strong>
            <form action="" method="post" class="frm_changepass">
                <input autocomplete="off" value="29A1-35141" type="text">
            </form>
        </div>
        <div class="modal-footer">
            <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
            <button class="btn btn-warning btn-flat">CẬP NHẬT</button>
        </div>
    </div>
    <!-- End Content -->

    {{--form delete--}}
    <div class="modal hide fade" id="modal_delete" style="width: 250px; margin-left: -10%;margin-top: 10%;">
        <div class="modal-body center">

            <p>Bạn có chắc muốn xoá?</p>
        </div>
        <form action="{{action('VehicleController@delete')}}" method="post">
            <div class="modal-footer">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="vehicleId" value="" id="delete_vehicle">
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
            </div>
        </form>
    </div>
    {{--end form delete--}}
    <script>
        $('._delete').click(function () {
            $('#delete_vehicle').val($(this).val());
        });
        jQuery(function ($) {
            $('table.table > tbody > tr:not(.info)').on('click', function (e) {
                LoadSeatMap($(this).attr('data-seatMapId'))
            });
        });
        $('#insuranceEndTime').datepicker({
            dateFormat: "dd-mm-yy",
            numberOfMonths: 1
        });
        function LoadSeatMap(seatMapId) {

            $('#Floor1 .khungxe').html('');
            $('#Floor2 .khungxe').html('');
            $.ajax({
                url: '/cpanel/seat-map/show/' + seatMapId,
                dataType: 'json',
                success: function (result) {
                    html_floor1 = '';
                    html_floor2 = '';

                    Row = result['numberOfRows'];
                    Column = result['numberOfColumns'];


                    for (hang = 1; hang <= Row; hang++) {
                        for (cot = 1; cot <= Column; cot++) {
                            data_floor1 = '<div class="ghe loidi"></div>';
                            data_floor2 = '<div class="ghe loidi"></div>';
                            $.each(result['seatList'], function (key, item) {
                                if (item['column'] == cot && item['row'] == hang) {
                                    var seatType = generateSeatType(item);
                                    if (item['floor'] == 1) {
                                        /*Tầng 1*/
                                        data_floor1 = '<div class="ghe ' + seatType + '">' + item['seatId'] + '</div>';
                                    } else {
                                        /*Tầng 2*/
                                        data_floor2 = '<div class="ghe ' + seatType + '">' + item['seatId'] + '</div>';
                                    }
                                }
                            });
                            html_floor1 += data_floor1;
                            html_floor2 += data_floor2;
                        }
                        //xử lý tràn ghế trong div html
                        if (Column < 7) {
                            for (i = 0; i < 7 - Column; i++) {
                                html_floor1 += '<div class="ghe loidi-noboder"></div>';
                                html_floor2 += '<div class="ghe loidi-noboder"></div>';
                            }
                        }
                    }

                    $('#Floor1 .khungxe').html(html_floor1);

                    if (result['numberOfFloors'] > 1) {
                        $('#Floor2 .khungxe').html(html_floor2);
                    }


                }
            });
        }

        function generateSeatType(seat) {

            switch (seat['seatType']) {
                case 1:
                    seatType = 'cuaxe';//CỬA
                    break;
                case 2:
                    seatType = 'ghedadat';//GHẾ TÀI XẾ
                    break;
                case 3:
                    seatType = 'ghetrong';//GHẾ THƯỜNG
                    break;
                case 4:
                    seatType = 'giuongnam';
                    break;
                case 5:
                    seatType = 'nhavesinh';
                    break;
                case 6:
                    seatType = 'ghephuxe';
                    break;
                default:
                    seatType = 'loidi';
                    break;
            }
            return seatType;
        }
    </script>
@endsection


