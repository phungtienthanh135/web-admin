@extends('cpanel.template.layout')
@section('title', 'Thêm mới phương tiện')
@section('content')
    <link rel="stylesheet" href="/public/theme/css/SimpleImage.css">
    <style>
        .loidi, .ghegiucho {
            color: rgba(0, 0, 0, 0);;
        }
    </style>
    <form id="add_vehicle" action="" method="post" novalidate>
        <div id="content">
            <div class="heading_top">
                <div class="row-fluid">
                    <div class="pull-left span8"><h3>Thêm mới phương tiện</h3></div>
                </div>
            </div>
            <div class="innerLR">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row-fluid m_top_10">
                    <div class="span3">
                        <label for="txt_BienSo">Biển số</label>
                        <input type="text" value="{{old('numberPlate')}}" name="numberPlate" id="txt_BienSo" required
                               data-validation-error-msg-required="Vui lòng nhập biển số xe">
                    </div>
                    {{--<div class="span3">--}}
                        {{--<label for="txtPhoneNumber">Số điện thoại</label>--}}
                        {{--<input type="text" name="phoneNumber" autocomplete="off"--}}
                               {{--value="{{@$vehicleInfo['phoneNumber']}}">--}}
                    {{--</div>--}}
                    <div class="span3">
                        <label for="txt_XuatXu">Nguyên giá</label>
                        <input type="text" name="originPrice" id="txt_XuatXu" value="{{old('originPrice')}}">
                    </div>
                    <div class="span3">
                        <label for="txtReduceTime">Thời gian khấu hao (tháng)</label>
                        <input min="0" type="number" name="reduceTime" id="txtReduceTime"
                               value="{{old('reduceTime')}}"
                               data-validation-error-msg-number="Thời gian khấu hao là số nguyên và lớn hơn 0. Vd: 1">
                    </div>
                    <div class="span3">
                        <label for="txtNgayDangKy">Hạn đăng kiểm</label>
                        <div class="input-append">
                            <input type="text" class="datepicker span11" name="registrationDeadline"
                                   id="txtNgayDangKy"
                                   value="{{old('registrationDeadline')}}">
                        </div>
                    </div>

                </div>
                <div class="row-fluid">
                    <div class="span3">
                        <label for="txtEstimatedFuelConsumption">Hạn mức hao</label>
                        <input autocomplete="off" placeholder="Vd: 0.5" data-validation=""
                               data-validation-allowing="float" id="txtEstimatedFuelConsumption" step="any"
                               type="number"
                               name="estimatedFuelConsumption" value="{{old('estimatedFuelConsumption')}}"
                               data-validation-error-msg-number="Hạn mức hao phải là số và lớn hơn 0. Vd: 1.5"/>
                    </div>
                    <div class="span3">
                        <label for="txtSoMay">Số máy</label>
                        <input type="text" autocomplete="off" name="engineNumber"
                               value="{{old('engineNumber')}}"
                               data-validation-allowing="float"
                               data-validation-error-msg-required="Vui lòng nhập số máy"
                               data-validation-error-msg-number="Số máy phải là số và lớn hơn 0">
                    </div>
                    <div class="span3">
                        <label for="txtGpsId">Mã thiết bị định vị</label>
                        <input type="text" name="gpsId" autocomplete="off"
                               value="{{old('gpsId')}}"
                               data-validation-error-msg-required="Vui lòng nhập mã thiết bị định vị">
                    </div>
                    <div class="span3">
                        <label for="txtDate">Ngày đăng ký</label>
                        <div class="input-append">
                            <input type="text" class="datepicker span11" name="" id="txtDate" value="">
                        </div>
                    </div>
                    {{--<div class="span3">
                        <label for="txtGiaConLai"  >Giá còn lại</label>
                        <input type="text" name="remain" id="txtGiaConLai">
                    </div>--}}
                </div>
                <div class="row-fluid">
                    <div class="span3">
                        <label for="vehicleType">Loại xe</label>
                        <select name="vehicleType" id="vehicleType" required
                                data-validation-error-msg-required="Vui lòng chọn loại xe">
                            <option value="">Chọn loại xe</option>
                            @foreach($listVehicleType as $VehicleType)
                                <option data-vehicleTypeId="{{$VehicleType['vehicleTypeId']}}"
                                        data-numberOfSeats="{{$VehicleType['numberOfSeats']}}"
                                        {{old('vehicleTypeId')==$VehicleType['vehicleTypeName']?'selected':''}}  value="{{$VehicleType['vehicleTypeName']}}">{{$VehicleType['vehicleTypeName']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="span3">
                        <label for="txtTypeSeatMap">Chọn sơ đồ mẫu</label>
                        <select id="typeSeatMap" name="typeSeatMap"></select>
                    </div>
                    <div class="span3">
                        <label for="txtSoKhung">Số khung</label>
                        <input type="text" autocomplete="off" min="0" name="chasisNumber"
                               value="{{old('chasisNumber')}}"
                               data-validation-allowing="float"
                               data-validation-error-msg-required="Vui lòng nhập số khung"
                               data-validation-error-msg-number="Số khung phải là số và lớn hơn 0"/>
                    </div>
                    <div class="span3">
                        <label for="insuranceEndTime">Hạn bảo hiểm</label>
                        <div class="input-append">
                            <input type="text" class="datepicker span11" name="insuranceEndTime" id="insuranceEndTime" value="">
                        </div>
                    </div>
                    <input type="hidden" name="numberOfSeats" id="txt_numberOfSeats" value="{{old('numberOfSeats')}}">
                </div>
                <div class="row-fluid">
                    <div class="span3" style="margin-left: 0">
                        <label for="txtPhoneNumber">Số điện thoại</label>
                        <input type="number" name="phoneNumber" id="txtPhoneNumber"
                               value="{{old('phoneNumber')}}">
                    </div>
                </div>
                <div class="row-fluid bg_light m_top_10">
                    <div class="widget widget-4 bg_light">
                        <div class="widget-body">
                            @include('cpanel.SeatMap.show')
                            <div class="row-fluid m_top_15 m_bottom_20">
                                <div class="span3">
                                    <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="seatMapId" id="seatMapId_add" value="{{old('numberPlate')}}">
                                    <input type="hidden" name="vehicleTypeId_pattern" id="vehicleTypeId_pattern">
                                    <button id="btnSubmit" class="btn btn-warning btn-flat-full">LƯU</button>
                                </div>
                                <div class="span2">
                                    <a href="{{action('VehicleController@show')}}" id="cancel_create"
                                       class="btn btn-default btn-flat-full">HỦY</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>
        // $('#insuranceEndTime').datepicker({
        //     dateFormat: "dd-mm-yy",
        //     numberOfMonths: 1
        // });
    </script>
@endsection