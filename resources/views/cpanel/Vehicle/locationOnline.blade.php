@extends('cpanel.template.layout')
@section('title', 'Danh sách phương tiện')

@section('content')
    <style>
        #listVehicle + span {margin-bottom:0 !important;}
    </style>
    <div id="content">
        <div class="row-fluid" style="margin: 0">
            <select name="" id="listVehicle">
            </select>
            <span class="loadingVehicle">Đang lấy dữ liệu xe</span>
        </div>
        <div id="map" style="width: 100%;height:calc(100vh - 50px)"></div>
    </div>
    <script>
        $(document).ready(function(){
            $('#listVehicle').select2();
            $('#listVehicle').on('change',function () {
                id = $(this).val();
                new google.maps.event.trigger( markers[id], 'click' );
                var vehicleSelected = $.grep(listVehicle,function (e,i) {
                    return e.vehicleID == id;
                })[0];
                updateChangeCenter(vehicleSelected.latitude,vehicleSelected.longitude);
                sessionStorage.setItem('vehicleIdSkySoft',vehicleSelected.vehicleID);
            });
            getOnlineSkySoft();
        });
         function getOnlineSkySoft(){
             $('.loadingVehicle').show();
             $.ajax({
                 url : '{{action('VehicleController@getLocationOnlineData')}}',
                 type:"get",
                 dataType:'json',
                 success : function(data){
                     listVehicle = data.vehicle;
                     clearMarkers();
                     addListMarker();
                     $('.loadingVehicle').hide();
                 },
                 error:function(err){
                     notyMessage("Có lỗi xảy ra",'error')
                     $('.loadingVehicle').hide();
                 }
             });
         }
         setInterval(function(){
             getOnlineSkySoft();
         },36000);
        var listVehicle = [];
        var LatLngDefault = {lat:21.027764,lng:105.834160};
        var map,infowindow,markers=[];
        function initMap(){
            // var input = document.getElementById('addressSearch');
            //
            // var autocompleteAddress = new google.maps.places.Autocomplete(input);
            map = new google.maps.Map(document.getElementById('map'), {zoom: 14,center: LatLngDefault});
            infowindow = new google.maps.InfoWindow({
                content: "<h1>Hello world</h1>"
            });
            // autocompleteAddress.setComponentRestrictions({'country'😞'vn']});
            // autocompleteAddress.addListener('place_changed',function(){
            //     var latlngPlace = autocompleteAddress.getPlace();
            //     updateChangeCenter(latlngPlace.geometry.location.lat(),latlngPlace.geometry.location.lng());
            //     showhere('#list-building');
            // });
            // map.addListener('idle',function(){
            //     changeLatLngDefault(map.getCenter().lat(),map.getCenter().lng());
            //     getListBuilding(LatLngDefault);
            //     deleteAllMarker();
            //     addListMarker();
            //     generateHtmlListBuilding();
            // });


            updateChangeCenter(LatLngDefault.lat,LatLngDefault.lng);

            addListMarker();
        }

        function addMarker(position,label,title,vehicleInfo){
            var marker = new google.maps.Marker({
                position: position,
//                label:label,
//                title:title,
                map: map
            });
            marker.vehicleInfo=vehicleInfo;
            marker.addListener('click',function(){
//                console.log(this);
                var vhcInfo = this.vehicleInfo;
                infowindow.setContent(generateHtmlInfoWindow(vhcInfo)); infowindow.open(map, marker);
            });
            markers[vehicleInfo.vehicleID]=marker;
        }

        function showMarkers() {
            setMapOnAll(map);
        }

        function clearMarkers() {
            setMapOnAll(null);
        }

        function setMapOnAll(map) {
            for (var i in markers){
                markers[i].setMap(map);
            }
//            $.each(markers,function(i,e){
//                console.log(i,markers[i],e);
//                markers[e.vehicleID].setMap(map);
//            });
        }
        function deleteAllMarker() {
            clearMarkers();
            markers=[];
        }

        function changeCenter(){
            map.setCenter(LatLngDefault);
        }

        function changeLatLngDefault(lat,lng){
//            console.log(lat,lng);
            LatLngDefault = { lat : parseFloat(lat) , lng : parseFloat(lng)};
        }
        function addListMarker() {
            html='';
            vhcSlt = sessionStorage.getItem('vehicleIdSkySoft');
            $.each(listVehicle,function(i,e){
                var select = '';
                if(vhcSlt===e.vehicleID){select="selected"}
                html+='<option value="'+e.vehicleID+'" '+select+'>'+e.registerNo+'--'+e.engineState+'</option>';
                addMarker({lat:parseFloat(e.latitude),lng:parseFloat(e.longitude)},e.registerNo,e.registerNo,e);
            });
            $('#listVehicle').html(html);
            if(listVehicle.length>0){
                $('#listVehicle').trigger('change');
            }
        }

        function updateChangeCenter(lat,lng) {
            changeLatLngDefault(lat,lng);
            changeCenter();
        }

        function generateHtmlInfoWindow(vehicleInfo) {
            html = '';
            html+='<h1>'+vehicleInfo.registerNo+'</h1>';
            html+='<div>Tốc độ : '+vehicleInfo.speed+' KM/h</div>';
            return html;
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key={!! API_GOOGLE_KEY !!}&callback=initMap&libraries=places&language=vn" type="text/javascript"></script>
@endsection