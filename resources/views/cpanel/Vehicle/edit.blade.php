@extends('cpanel.template.layout')
@section('title', 'Sửa phương tiện')

@push('activeMenu')
    activeMenu('{{action('VehicleController@show',[],true)}}');
@endpush
@section('content')
    <style>
        .loidi, .ghegiucho {
            color: rgba(0, 0, 0, 0);;
        }
    </style>
    <form action="{{action('VehicleController@postEdit',[],false)}}" method="post" novalidate>
        <div id="content">
            <div class="heading_top">
                <div class="row-fluid">
                    <div class="pull-left span8"><h3>Sửa phương tiện</h3></div>
                </div>
            </div>
            <div class="innerLR">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row-fluid m_top_10">
                    <div class="span3">
                        <label for="txt_BienSo">Biển số</label>
                        <input type="text" value="{{$vehicleInfo['numberPlate']}}" name="numberPlate" id="txt_BienSo">
                    </div>
                    <div class="span3">
                        <label for="vehicleType">Loại xe</label>

                        <select name="vehicleName" id="vehicleType">
                            <option value="">Chọn loại xe</option>
                            @php
                                $vehicleName = isset($vehicleInfo['vehicleName']) ? $vehicleInfo['vehicleName'] : '';
                            @endphp
                            @foreach($listVehicleType as $VehicleType)
                                <option data-numberOfSeats="{{$VehicleType['numberOfSeats']}}"
                                        {{$vehicleName==$VehicleType['vehicleTypeName']?'selected':''}}
                                        value="{{$VehicleType['vehicleTypeName']}}">{{$VehicleType['vehicleTypeName']}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    {{--<div class="span3">
                        <label for="txt_XuatXu">Xuất xứ</label>
                        <input type="text" name="origin" id="txt_XuatXu" value="{{@$vehicleInfo['origin']}}">
                    </div>--}}
                    <div class="span3">
                        <label for="txt_numberOfSeats">Số chỗ</label>
                        <input readonly type="number" name="numberOfSeats" id="txt_numberOfSeats"
                               value="{{$vehicleInfo['numberOfSeats']}}">
                    </div>

                    <div class="span3">
                        <label for="txtNgayDangKy">Hạn đăng kiểm</label>
                        <div class="input-append">
                            <input style="width: 165px" class="datepicker" type="text" name="registrationDeadline"
                                   id="txtNgayDangKy"
                                   value="@dateFormat($vehicleInfo['registrationDeadline'])">
                        </div>

                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3">
                        <label for="txt_XuatXu">Nguyên giá</label>
                        <input type="text" name="originPrice" id="txt_XuatXu" value="{{$vehicleInfo['originPrice']}}">
                    </div>
                    <div class="span3">
                        <label for="txtReduceTime">Thời gian khấu hao (tháng)</label>
                        <input min="0" type="number" name="reduceTime" id="txtReduceTime"
                               value="{{$vehicleInfo['reduceTime']}}"
                               data-validation-error-msg-number="Thời gian khấu hao là số nguyên và lớn hơn 0. Vd: 1">
                    </div>
                    <div class="span3">
                        <label for="txtEstimatedFuelConsumption">Hạn mức hao</label>
                        <input type="text" autocomplete="off" data-validation="number" data-validation-allowing="float"
                               id="txtEstimatedFuelConsumption"
                               name="estimatedFuelConsumption" value="{{$vehicleInfo['estimatedFuelConsumption']}}"
                               data-validation-error-msg-number="Hạn mức hao phải là số và lớn hơn 0. Vd: 1.5"/>
                    </div>
                    <div class="span3">
                        <label for="txtDate">Ngày đăng ký</label>
                        <div class="input-append">
                            <input type="text" class="datepicker span11"  id="txtDate" value="">
                        </div>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span3">
                        <label for="txtSoKhung">Số khung</label>
                        <input type="number" autocomplete="off" min="0" name="chasisNumber"
                               value="{{@$vehicleInfo['chasisNumber']}}"
                               data-validation-allowing="float"
                               data-validation-error-msg-number="Số khung phải là số và lớn hơn 0"/>
                    </div>
                    <div class="span3">
                        <label for="txtSoMay">Số máy</label>
                        <input type="number" autocomplete="off" name="engineNumber"
                               value="{{@$vehicleInfo['engineNumber']}}"
                               data-validation-allowing="float"
                               data-validation-error-msg-number="Số máy phải là số và lớn hơn 0">
                    </div>
                    <div class="span3">
                        <label for="txtGpsId">Mã thiết bị định vị</label>
                        <input type="text" name="gpsId" autocomplete="off"
                               value="{{@$vehicleInfo['gpsId']}}"
                        >
                    </div>
                    <div class="span3">
                        <label for="insuranceEndTime">Hạn bảo hiểm</label>
                        <div class="input-append">
                            <input type="text" class="datepicker span11" name="insuranceEndTime" id="insuranceEndTime"
                                   value="">
                        </div>

                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3">
                        <label for="txtPhoneNumber">Số điện thoại</label>
                        <input type="text" name="phoneNumber" autocomplete="off"
                               value="{{@$vehicleInfo['phoneNumber']}}"
                        >
                    </div>
                </div>
                <div class="row-fluid bg_light m_top_10">
                    <div class="widget widget-4 bg_light">
                        <div class="widget-body">

                            @include('cpanel.SeatMap.show')

                            <div class="row-fluid m_top_15 m_bottom_20">
                                <div class="span3">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="vehicleId" value="{{$id}}">
                                    <input type="hidden" name="seatMapId" id="seatMapId_add"
                                           value="{{$vehicleInfo['numberPlate']}}">
                                    <button class="btn btn-warning btn-flat-full">LƯU</button>
                                </div>
                                <div class="span2">
                                    <a href="{{action('VehicleController@show')}}"
                                       class="btn btn-default btn-flat-full">HỦY</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- End Content -->



@endsection