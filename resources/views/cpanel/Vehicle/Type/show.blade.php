@extends('cpanel.template.layout')
@section('title', 'Loại xe')

@section('content')

    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Loại xe</h3></div>
            </div>
        </div>

        <div class="separator bottom"></div>
        <div class="innerLR">
            <div class="row-fluid">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Tìm loại xe</h4>
                    </div>
                    <form action="">
                        <div class="row-fluid">
                            {{--<div class="span3">--}}
                                {{--<label for="cbb_LoaiXe">Tên loại xe</label>--}}
                                {{--<select class="span12" name="seatMapType" id="cbb_LoaiXe">--}}
                                    {{--<option value="">Chọn loại xe</option>--}}
                                    {{--<option {{request('seatMapType')==1?'selected':''}}   value="1">Ghế ngồi</option>--}}
                                    {{--<option {{request('seatMapType')==2?'selected':''}}  value="2">Giường nằm</option>--}}
                                    {{--<option {{request('seatMapType')==3?'selected':''}}  value="3">Hỗn hợp</option>--}}
                                    {{--<option {{request('seatMapType')==4?'selected':''}}  value="4">Phòng</option>--}}
                                    {{--<option {{request('seatMapType')==4?'selected':''}}  value="5">xe demo</option>--}}
                                {{--</select>--}}

                            {{--</div>--}}

                            <div class="span3">
                                <label for="txtSoCho">Số chỗ</label>
                                <input type="text" name="numberOfSeats" id="txtSoCho"
                                       value="{{request('numberOfSeats')}}">
                            </div>

                            <div class="span2">
                                <label class="separator hidden-phone" for="search"></label>
                                <button class="btn btn-info btn-flat-full" id="search">TÌM LOẠI XE</button>
                            </div>
                            @if(hasAnyRole(CREATE_VEHICLE_TYPE))
                                <div class="span2">
                                    <label class="separator hidden-phone" for="add"></label>
                                    <button data-toggle="modal" data-target="#modal_add"
                                            class="btn btn-warning btn-flat-full" id="add">THÊM LOẠI XE
                                    </button>
                                </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
            <div class="row-fluid">
                Danh sách loại xe
                <table class="table table-striped">
                    <thead>
                    <tr>
                        {{--<th>Mã loại xe</th>--}}
                        <th width="25%">Tên loại xe</th>
                        <th width="10%">Số chỗ</th>
                        <th>Hạng bằng lái</th>
                        <th width="30%">Mô tả</th>
                        <th class="center">Tuỳ chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($result as $row)
                        <tr>
                            {{--<td>{{$row['vehicleTypeId']}}</td>--}}
                            <td id="vehicleTypeName_{{$row['vehicleTypeId']}}">{{$row['vehicleTypeName']}}</td>
                            <td id="numberOfSeats_{{$row['vehicleTypeId']}}">{{$row['numberOfSeats']}}</td>
                            <td></td>
                            <td id="description_{{$row['vehicleTypeId']}}">{{@$row['description']}}</td>
                            <td class="center">
                                <a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                    <ul class="onclick-menu-content">
                                        @if(hasAnyRole(UPDATE_VEHICLE_TYPE))
                                            <li>
                                                <button data-toggle="modal" data-target="#modal_edit"
                                                        value="{{$row['vehicleTypeId']}}" class="_edit">Sửa
                                                </button>
                                            </li>
                                        @endif
                                        @if(hasAnyRole(DELETE_VEHICLE_TYPE))
                                            <li>
                                                <button data-toggle="modal" data-target="#modal_delete"
                                                        value="{{$row['vehicleTypeId']}}" class="_delete">Xóa
                                                </button>
                                            </li>
                                        @endif
                                    </ul>
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td class="center" colspan="6">Hiện tại không có dữ liệu</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                @include('cpanel.template.pagination-without-number',['page'=>$page])
            </div>
        </div>
    </div>
    @if(hasAnyRole(CREATE_VEHICLE_TYPE))
    {{--form add--}}
    <div class="modal modal-custom hide fade" id="modal_add">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="center">thêm loại xe </h3>
        </div>
        <form action="{{action('VehicleTypeController@add')}}" method="post" class="form-horizontal" id="form_add">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtTenLoaiXe">Tên loại xe</label>
                        <div class="controls">
                            <input required class="span12" type="text" value="{{old('vehicleTypeName')}}"
                                   name="vehicleTypeName" id="txtTenLoaiXe">
                        </div>
                    </div>
                    <div class="control-group">

                        <label class="control-label" for="txtSoCho">Số chỗ</label>
                        <div class="controls form-inline">
                            <input required min="4" class="span2" type="text" value="{{old('numberOfSeats')}}"
                                   name="numberOfSeats" id="txtSoCho">

                            {{--<label for="cbbHangBangLay" class="m_left_20">Hạng bằng lái</label>
                            <select class="span7" name="" id="cbbHangBangLay">
                                <option value=""></option>
                            </select>--}}
                        </div>

                    </div>

                    <div class="control-group">
                        <label class="control-label" for="txtMoTa">Mô tả</label>
                        <div class="controls">
                            <textarea class="span12" name="description" id="txtMoTa"
                                      rows="4">{{old('description')}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="control-group">
                    <div class="controls">
                        <div class="row-fluid">
                            <div class="span8">
                                <button class="btn btn-warning btn-flat-full">THÊM MỚI</button>
                            </div>
                            <div class="span4">
                                <button type="reset" data-dismiss="modal" aria-hidden="true"
                                        class="btn btn-default btn-flat-full no_border">HỦY
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{--end form add--}}
    @endif

    @if(hasAnyRole(UPDATE_VEHICLE_TYPE))
    {{--form edit--}}
    <div class="modal modal-custom hide fade" id="modal_edit">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="center">SỬA LOẠI XE </h3>
        </div>
        <form action="{{action('VehicleTypeController@edit')}}" method="post" class="form-horizontal">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="vehicleTypeId" id="vehicleTypeId_edit">
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="vehicleTypeName_edit">Tên loại xe</label>
                        <div class="controls">
                            <input class="span12" type="text" name="vehicleTypeName" id="vehicleTypeName_edit">
                        </div>
                    </div>
                    <div class="control-group">

                        <label class="control-label" for="numberOfSeats_edit">Số chỗ</label>
                        <div class="controls form-inline">
                            <input class="span2" type="text" name="numberOfSeats" id="numberOfSeats_edit">
                        </div>

                    </div>

                    <div class="control-group">
                        <label class="control-label" for="description_edit">Mô tả</label>
                        <div class="controls">
                            <textarea class="span12" name="description" id="description_edit" rows="4"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="control-group">
                    <div class="controls">
                        <div class="row-fluid">
                            <div class="span8">
                                <button class="btn btn-warning btn-flat-full">SỬA</button>
                            </div>
                            <div class="span4">
                                <button data-dismiss="modal" aria-hidden="true"
                                        class="btn btn-default btn-flat-full no_border">HỦY
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{--end form edit--}}
    @endif

    @if(hasAnyRole(DELETE_VEHICLE_TYPE))
    {{--form delete--}}
    <div class="modal hide fade" id="modal_delete" style="width: 250px; margin-left: -10%;margin-top: 10%;">
        <div class="modal-body center">

            <p>Bạn có chắc muốn xoá?</p>
        </div>
        <form action="{{action('VehicleTypeController@delete')}}" method="post">
            <div class="modal-footer">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="vehicleTypeId" value="" id="delete_vehicleType">
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
            </div>
        </form>
    </div>
    {{--end form delete--}}
    @endif
    <script>
        $('._delete').click(function () {
            $('#delete_vehicleType').val($(this).val());
        });
        $('#modal_add').on('hidden', function () {
            $('#form_add')[0].reset();
        });
        $('._edit').click(function () {
            $('#vehicleTypeId_edit').val($(this).val());
            $('#vehicleTypeName_edit').val($('#vehicleTypeName_' + $(this).val()).text());
            $('#description_edit').val($('#description_' + $(this).val()).text());
            $('#numberOfSeats_edit').val($('#numberOfSeats_' + $(this).val()).text());
        })
    </script>
    <!-- End Content -->

@endsection