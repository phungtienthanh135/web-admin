@extends('cpanel.template.layout')
@section('title', 'Quản Lí Phòng Ban')
@section('content')
	<div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Danh sách Phòng</h3></div>
                <div class="pull-right span4 t_align_r">
                    <div class="row-fluid">
                        <div class="input-append">
                            <a class="btn btn-default" id="addDepartment" style="border-radius:0px;">Thêm Mới
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive" style="background: #fff">
    		<table class="table table-hover table-vertical-center">
    			<thead>
    				<tr>
    					<th style="text-align:center">STT</th>
    					<th>Tên Phòng Ban</th>
    					<th>Tùy Chọn</th>
    				</tr>
    			</thead>
    			<tbody>
    				<?php $stt=1; ?>
    				@foreach($data['results']['listDepartment'] as $item)
    				<tr>
    					<td style="text-align:center">{{ $stt }}</td>
    					<td>{{$item['departmentName']}}</td>
    					<td>
			    			<a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
			                    <ul class="onclick-menu-content">
			                       <li><button edit-department-btn="{{ $item['departmentId'] }}" edit-departmentName="{{$item['departmentName']}}"> Sửa </button></li>
			                       <li><button delete-department-btn="{{ $item['departmentId'] }}"> Xóa </button></li>
			                    </ul>
			                </a>
            			</td>
    				</tr>
    				<?php $stt++; ?>
    				@endforeach
    			</tbody>
    		</table>
    	</div>
    </div>
    <div class="modal modal-custom hide fade" id="modal-department">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Phòng Ban</h3>
        </div>
            <div class="modal-body" id="content-modal">
                
            </div>
            <div class="modal-footer">

            </div>
    </div>
    <script>
    	$('#addDepartment').click(function(){
    		html='<form action="{{ action('DepartmentController@postAdd') }}" method="post">';
    		html += '<div class="form-horizontal row-fluid">';
    		html +=	'<div class="control-group"><label class="control-label" for="departmentName">Tên Phòng Ban</label><div class="controls"><input autocomplete="off" required data-validation="required" data-validation-error-msg="Vui lòng nhập tên Phòng " class="span12" type="text" name="departmentName" id="departmentName"></div></div>';
    		html += '</div>';
    		html+='<div class="form-horizontal row-fluid">';
    		html+='<div class="control-group"><div class="controls"><div class="row-fluid"> <div class="span9"><input type="hidden" name="_token" value="{{csrf_token()}}"><button class="btn btn-warning btn-flat-full">THÊM MỚI</button></div><div class="span3"><a data-dismiss="modal" class="btn btn-default btn-flat no_border bg_light">HUỶ</a></div></div></div></div>';
    		html += '</div>';
    		html+='<form>';
    		$('#content-modal').html(html);
    		$('#modal-department').modal('show');
    	});
    	$('body').delegate('[delete-department-btn]','click',function(){
    		var ok = confirm("Bạn Có Chắc Chắn Muốn Xóa !!! ");
    		if(ok){
    			window.location='{{ url("cpanel/department/delete") }}'+"/"+$(this).attr("delete-department-btn");
    		}else{
    			return false;
    		}
    	});
    	$('body').delegate('[edit-department-btn]','click',function(){
    		var id = $(this).attr('edit-department-btn');
    		var name = $(this).attr('edit-departmentName');
    		html='<form action="{{ action('DepartmentController@postEdit') }}" method="post">';
    		html += '<input type="hidden" name="departmentId" value="'+ id +'">';
    		html += '<div class="form-horizontal row-fluid">';
    		html +=	'<div class="control-group"><label class="control-label" for="departmentName">Tên Phòng Ban</label><div class="controls"><input autocomplete="off" required data-validation="required" data-validation-error-msg="Vui lòng nhập tên Phòng " class="span12" type="text" value="'+name+'" name="departmentName" id="departmentName"></div></div>';
    		html += '</div>';
    		html+='<div class="form-horizontal row-fluid">';
    		html+='<div class="control-group"><div class="controls"><div class="row-fluid"> <div class="span9"><input type="hidden" name="_token" value="{{csrf_token()}}"><button class="btn btn-warning btn-flat-full">CẬP NHẬT</button></div><div class="span3"><a data-dismiss="modal" class="btn btn-default btn-flat no_border bg_light">HUỶ</a></div></div></div></div>';
    		html += '</div>';
    		html+='<form>';
    		$('#content-modal').html(html);
    		$('#modal-department').modal('show');
    	});
    </script>
@endsection