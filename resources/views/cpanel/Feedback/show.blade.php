@extends('cpanel.template.layout')
@section('title', 'Phản hồi khách hàng')
@section('content')

    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Phản Hồi Khách Hàng</h3></div>
            </div>
        </div>


        <div class="innerLR">
            <div class="row-fluid">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Tìm phản hồi</h4>
                    </div>
                    <div class="row-fluid">
                        <div class="span2">
                            <label for="id_tour">Mã chuyến</label>
                            <input  class="input-medium" type="text" name="id_tour" id="id_tour">
                        </div>

                        <div class="span2">
                            <label for="type_feedback">Loại phản hồi</label>
                            <select style="width: 170px" class="input-medium" name="type_feedback" id="type_feedback">
                                <option value="">Tất cả</option>
                                <option value="">Tích cực</option>
                                <option value="">Tiêu cực</option>
                            </select>
                        </div>



                        <div class="span3">
                            <label for="txtTuNgay">Từ ngày</label>
                            <div class="input-append">
                                <input class="datepicker" id="txtStartDate" type="text">

                            </div>
                        </div>

                        <div class="span3">
                            <label for="txtDenNgay">Đến ngày</label>
                            <div class="input-append">
                                <input class="datepicker" id="txtEndDate" type="text">

                            </div>
                        </div>

                        <div class="span2">
                            <label class="separator hidden-phone" for="add"></label>
                            <button class="btn btn-info btn-flat-full" id="search">TÌM PHẢN HỒI</button>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span4">
                            <label for="txtKhachHang">Khách hàng</label>
                            <input class="w_full" type="text" name="id_customer" id="txtKhachHang">
                        </div>

                        <div class="span3">
                            <label for="txtTaiXe">Tài xế</label>
                            <input class="w_full" type="text" name="id_employee" id="txtTaiXe">
                        </div>

                        <div class="span3">
                            <label for="txtPhuXe">Phụ xe</label>
                            <input class="w_full" type="text" name="id_employee" id="txtPhuXe">
                        </div>
                        @if(hasAnyRole(CREATE_FEEDBACK))
                        <div class="span2">
                            <label class="separator hidden-phone" for="add"></label>
                            <button data-toggle="modal" data-target="#add_notification"
                                    class="btn btn-warning btn-flat-full" id="add">THÊM THÔNG BÁO
                            </button>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                Danh sách phản hồi khách hàng
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Mã chuyến</th>
                        <th>Mã vé</th>
                        <th>Thời gian phản hồi</th>
                        <th>Nội dung</th>
                        <th>Người tạo phản hồi</th>
                        <th>Loại phản hồi</th>
                        <th>Tuỳ chọn</th>
                    </tr>
                    </thead>
                    <tbody>

                   @forelse($result as $row)
                    <tr>
                        <td>{{@$row['tripId']}}</td>
                        <td>{{@$row['ticketCode']}}</td>
                        <td>@dateFormat($row['createDate']/1000)</td>
                        <td>{{$row['content']}}</td>
                        <td>{{$row['userId']}}</td>
                        <td>{{$row['feedbackType']==1?'Tích cực':'Tiêu cực'}}</td>
                        <td class="center">
                            <a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                <ul class="onclick-menu-content">
                                    @if(hasAnyRole(UPDATE_FEEDBACK))
                                    <li>
                                        <button onclick="showinfo();">Sửa</button>
                                    </li>
                                    @endif
                                    @if(hasAnyRole(DELETE_FEEDBACK))
                                    <li>
                                        <button>Xóa</button>
                                    </li>
                                    @endif
                                </ul>
                            </a>
                        </td>
                    </tr>
                    <tr class="info" style="display: none">
                        <td colspan="7">
                            <table class="tttk" cellspacing="20" cellpadding="4">
                                <tbody>
                                <tr>
                                    <th>MÃ CHUYẾN</th>
                                    <td>{{@$row['tripId']}}</td>
                                    <th>NGƯỜI TẠO PHẢN HỒI</th>
                                    <td>{{$row['userId']}}</td>
                                </tr>
                                <tr>
                                    <th>MÃ VÉ</th>
                                    <td>{{@$row['ticketCode']}}</td>
                                    <th>LOẠI PHẢN HỒI</th>
                                    <td>{{$row['feedbackType']==1?'Tích cực':'Tiêu cực'}}</td>
                                </tr>
                                <tr>
                                    <th>THỜI GIAN PHẢN HỒI</th>
                                    <td>12h00 Ngày 17/04/2017</td>
                                    <th>TRẠNG THÁI</th>
                                    <td>Thành công</td>
                                </tr>
                                <tr>
                                    <th>NỘI DUNG</th>
                                    <td rowspan="2">{{$row['content']}}</td>
                                    <th></th>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                       @empty
                       <tr>
                           <td class="center" colspan="7">Hiện tại không có dữ liệu</td>
                       </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="row-fluid bg_light"  style="margin-bottom:10px;display: none;" id="info">
                <div class="widget widget-4 bg_light">
                    <div class="widget-body">

                    </div>
                </div>
            </div>

        </div>
    </div>
    @if(hasAnyRole(CREATE_FEEDBACK))
    <div class="modal modal-custom hide fade" id="add_notification">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 >Tạo Thông Báo Cho Khách Hàng</h3>
        </div>
        <form action="{{action('FeedbackController@postAdd')}}" class="form-horizontal row-fluid">
            <div class="modal-body">
                <div class="control-group">
                    <label class="control-label" for="receiver_notification">Mã vé</label>
                    <div class="controls">
                        <div class="row-fluid">
                            <div class="span12 form-inline">
                                <input class="span5" name="ticketId" type="text" id="txtMaVe">
                                <div class="pull-right">
                                    <label for="txtMaChuyen">Mã chuyến</label>
                                    <input  class="span6" name="tripId" type="text" id="txtMaChuyen">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="txtTaiXe">Tài xế</label>
                    <div class="controls">
                        <input class="span12" type="text" name="id_employee" id="txtTaiXe">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="txtPhuXe">Phụ xe</label>
                    <div class="controls">
                        <input class="span12" type="text" name="id_employee" id="txtPhuXe">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="cbb_LoaiPhanHoi">Loại phản hồi</label>
                    <div class="controls">
                        <select  class="span12" name="feedbackType" id="cbb_LoaiPhanHoi">
                            <option value="1">Tích cực</option>
                            <option value="2">Tiêu cực</option>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="content_notification">Nội dung</label>
                    <div class="controls">
                        <textarea class="span12" id="content_notification" rows="4" name="_content"></textarea>
                    </div>
                </div>
                <div class="control-group">

                    <div class="controls">
                        <div class="row-fluid">
                            <div class="span9">
                                <button class="btn btn-warning btn-flat-full">THÊM MỚI</button>
                            </div>
                            <div class="span3">
                                <a data-dismiss="modal" class="btn btn-default btn-flat no_border bg_light">HUỶ</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </form>
    </div>
    @endif
    <script type="text/javascript">
        function showinfo(){
            $('#info').show();
        }
    </script>
    <!-- End Content -->

@endsection