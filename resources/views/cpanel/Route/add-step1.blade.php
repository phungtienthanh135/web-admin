<div id="step1">
    <div class="heading_top">
        <div class="row-fluid">
            <div class="pull-left span8"><h3>Thêm mới tuyến <span>(1/2)</span></h3></div>
        </div>
    </div>
    <div class="row-fluid bg_light">
        <div class="widget widget-4 bg_light">
            <div class="widget-body">
                <div class="innerLR">
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="row-fluid">
                                <table class="table table-hover table-vertical-center tb_themmoi timeline">
                                    <thead>
                                    <tr>
                                        <th>
                                            <div></div>
                                        </th>
                                        <th width="30%">Danh sách bến và trạm</th>
                                        <th></th>
                                        <th width="10%"></th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="first">
                                        <td>
                                            <select id="firstpoint" style="display: none"></select>
                                        </td>
                                        <td id="lb_pointStrarName">Chọn điểm đầu tuyến</td>
                                        <td id="lb_pointStrarAddress"></td>
                                        <td>
                                            <a tabindex="0"
                                               class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                                <ul class="onclick-menu-content">
                                                    <li>
                                                        <button type="button" data-toggle="modal"
                                                                data-target="#modal_Route">Sửa thông tin tuyến
                                                        </button>
                                                    </li>
                                                </ul>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr class="tool">
                                        <td>
                                            <div></div>
                                        </td>
                                        <td colspan="3">
                                            <a class="c_pointer" onclick="addPoint()">
                                                <i class="icon-plus"></i> Thêm điểm dừng giữa tuyến
                                            </a>
                                        </td>
                                    </tr>
                                    <tr class="last">
                                        <td>
                                            <div><i class="icon-map-marker"></i>
                                            </div>
                                        </td>
                                        <td id="lb_pointEndName"><span>Chọn điểm cuối tuyến</span></td>
                                        <td id="lb_pointEndAddress"></td>
                                        <td>
                                            <select id="lastpoint" style="display: none"></select>
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>

                            </div>

                            <div class="row-fluid m_top_15">
                                <button type="button" id="btn_next"
                                        class="span4 btn btn-warning btn-flat">
                                    TIẾP TỤC
                                </button>
                                <a href="{{action('RouteController@show')}}" id="btn_huy"
                                   class="span3 btn btn-default btn-flat no_border bg_light">HUỶ</a>
                            </div>

                        </div>
                        <div class="span2">
                            <label class="separator"></label>
                            <div class="frm_chonanh">
                                <div class="chonanh">
                                    <img id="imgRoute" src="/public/images/anhsanpham.PNG">
                                </div>
                                <input type="file" name="img" id="selectedFile" style="display: none;">
                                <input type="button" style="width:172px" value="CHỌN ẢNH" class="btncustom"
                                       onclick="document.getElementById('selectedFile').click();">
                                <input type="hidden" id="listImage" name="listImage">
                            </div>
                        </div>
                        <div class="span4">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d8819383.855541544!2d105.85383176357185!3d15.934584965867383!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1503569252580"
                                    width="100%" height="450" frameborder="0" style="border:0"
                                    allowfullscreen></iframe>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="listTransshipment" id="listTransshipment" value="">
</div>




