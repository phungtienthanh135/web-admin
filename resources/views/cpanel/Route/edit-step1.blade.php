<div id="step1">
    <div class="heading_top">
        <div class="row-fluid">
            <div class="pull-left span8"><h3>Sửa tuyến <span>(1/2)</span></h3></div>
        </div>
    </div>
    <div class="row-fluid bg_light">
        <div class="widget widget-4 bg_light">
            <div class="widget-body">
                <div class="innerLR">
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="row-fluid">
                                <table class="table table-hover table-vertical-center tb_themmoi timeline">
                                    <thead>
                                    <tr>
                                        <th>
                                            <div></div>
                                        </th>
                                        <th width="30%">Danh sách bến và trạm</th>
                                        <th></th>
                                        <th width="10%"></th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($resultRoute['route']['listTransshipmentPoint']))
                                        @foreach($resultRoute['route']['listTransshipmentPoint'] as $k=>$v)
                                            @php $listTransshipment[]=$k; @endphp
                                        @endforeach
                                    @endif

                                    @php $dem=0;@endphp
                                    @foreach($resultRoute['listPoint'] as $key=>$point)
                                        @if($loop->first)
                                            <tr class="first">
                                                <td>
                                                    <div>
                                                        <select id="firstpoint" style="display: none">
                                                            @if(isset($resultRoute['route']['listTransshipmentPoint']))
                                                                @foreach($resultRoute['route']['listTransshipmentPoint'] as $v1)
                                                                    @foreach($v1 as $v2)
                                                                        @if(isset($v2['index'])&& $v2['index']==0)
                                                                            @if(isset($v2['pointId'])&& $v2['pointId']!='')
                                                                                <option data-id="{{$v2['pointId']}}"
                                                                                        data-price="{{$v2['transshipmentPrice']}}"
                                                                                data-name=" @if(isset($v2['pointName'])&& $v2['pointName']!='')
                                                                                {{$v2['pointName']}} @endif">
                                                                                    @if(isset($v2['pointName'])&& $v2['pointName']!='')
                                                                                        {{$v2['pointName']}}
                                                                                    @else
                                                                                        Chưa đặt tên
                                                                                    @endif
                                                                                </option>
                                                                            @endif
                                                                        @endif
                                                                    @endforeach
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </td>
                                                <td id="lb_pointStrarName">{{$point['pointName']}}</td>
                                                <td id="lb_pointStrarAddress">{{$point['address']}}</td>
                                                <td>
                                                    <a tabindex="0"
                                                       class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                                        <ul class="onclick-menu-content">
                                                            <li>
                                                                <button id="open_Modal_Route"type="button" data-toggle="modal"
                                                                        data-target="#modal_Route">Sửa thông tin tuyến
                                                                </button>
                                                            </li>
                                                        </ul>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endif
                                        @if(!$loop->last && !$loop->first)
                                            <tr class="pointStop" id="{{$point['pointId']}}" draggable="true"
                                                ondragstart="drag(event,this)"
                                                ondrop="drop(event)" ondragover="allowDrop(event)">
                                                <td>
                                                    <select class="pointedit" style="display: none">

                                                        @php $dem++;@endphp
                                                        @if(isset($resultRoute['route']['listTransshipmentPoint']))
                                                            @foreach($resultRoute['route']['listTransshipmentPoint'] as $v1)
                                                                @foreach($v1 as $v2)
                                                                    @if(isset($v2['index'])&& $v2['index']==$dem)
                                                                        @if(isset($v2['pointId'])&& $v2['pointId']!='')
                                                                            <option data-id="{{$v2['pointId']}}"
                                                                                    data-price="{{$v2['transshipmentPrice']}}"
                                                                                    data-name=" @if(isset($v2['pointName'])&& $v2['pointName']!='')
                                                                                    {{$v2['pointName']}} @endif">
                                                                                @if(isset($v2['pointName'])&& $v2['pointName']!='')
                                                                                    {{$v2['pointName']}}
                                                                                    @else
                                                                                    Chưa đặt tên
                                                                                @endif
                                                                            </option>
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endforeach
                                                        @endif

                                                    </select>
                                                </td>
                                                <td class="lbPointName">{{$point['pointName']}}</td>
                                                <td class="lbPointAddress">{{$point['address']}}</td>
                                                <td><a tabindex="0"
                                                       class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                                        <ul class="onclick-menu-content">
                                                            <li>
                                                                <button id="open_Modal_Point" type="button" onclick="editPoint(this)">Sửa bến
                                                                    đỗ
                                                                </button>
                                                            </li>
                                                            <li>
                                                                <button type="button" onclick="deletePoint(this)">Xóa
                                                                </button>
                                                            </li>
                                                        </ul>
                                                    </a></td>

                                            </tr>
                                        @endif
                                        @if($loop->last)
                                            <tr class="tool addPoint">
                                                <td>
                                                    <div></div>
                                                </td>
                                                <td colspan="3">
                                                    <a class="c_pointer" onclick="addPoint()">
                                                        <i class="icon-plus"></i> Thêm điểm dừng giữa tuyến
                                                    </a>
                                                </td>
                                            </tr>

                                            <tr class="last">
                                                <td>
                                                    <div><i class="icon-map-marker"></i>
                                                    </div>
                                                </td>
                                                <td id="lb_pointEndName"><span>{{$point['pointName']}}</span></td>
                                                <td id="lb_pointEndAddress">{{$point['address']}}</td>

                                                <td>
                                                    <select id="lastpoint" style="display: none">
                                                        @if(isset($resultRoute['route']['listTransshipmentPoint']))
                                                            @foreach($resultRoute['route']['listTransshipmentPoint'] as $v1)
                                                            @foreach($v1 as $v2)
                                                                @if(isset($v2['index'])&& $v2['index']==(count($listTransshipment)-1))
                                                                @if(isset($v2['pointId'])&& $v2['pointId']!='')
                                                                <option data-id="{{$v2['pointId']}}"
                                                                        data-price="{{$v2['transshipmentPrice']}}"
                                                                        data-name=" @if(isset($v2['pointName'])&& $v2['pointName']!='')
                                                                        {{$v2['pointName']}} @endif">
                                                                    @if(isset($v2['pointName'])&& $v2['pointName']!='')
                                                                    {{$v2['pointName']}}
                                                                    @else
                                                                        Chưa đặt tên
                                                                        @endif
                                                                </option>
                                                                @endif
                                                                @endif
                                                            @endforeach
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </td>

                                            </tr>
                                        @endif


                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                            <div class="row-fluid m_top_15">
                                <button type="button" id="btn_next"
                                        class="span4 btn btn-warning btn-flat">
                                    TIẾP TỤC
                                </button>
                                <a href="{{action('RouteController@show')}}" id="btn_huy"
                                   class="span3 btn btn-default btn-flat no_border bg_light">HUỶ</a>
                            </div>

                        </div>
                        <div class="span2">
                            <label class="separator"></label>
                            <div class="frm_chonanh">
                                <div class="chonanh">
                                    <img id="imgRoute"
                                         src="{{!empty($listImage) ? $listImage : '/public/images/anhsanpham.PNG'}}">
                                </div>
                                <input type="file" name="img" id="selectedFile" style="display: none;">
                                <input type="button" style="width:172px" value="CHỌN ẢNH" class="btncustom"
                                       onclick="document.getElementById('selectedFile').click();">
                                <input type="hidden" id="listImage" name="listImage"
                                       value="{{!empty($listImage) ? $listImage : ''}}">
                            </div>
                        </div>
                        <div class="span4">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d8819383.855541544!2d105.85383176357185!3d15.934584965867383!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1503569252580"
                                    width="100%" height="450" frameborder="0" style="border:0"
                                    allowfullscreen></iframe>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="listTransshipment" id="listTransshipment" value="">
</div>
<script>
    $(document).ready(function (e) {

        $('body').on('drag', '.pointStop,.lbPointName,.lbPointAddress', function () {
            if ($(this).attr('class') != 'pointStop') {
                trDrag = '<tr class="pointStop" id="' + $(this.parentElement).attr('id') + '" draggable="true" ondragstart="drag(event,this)" ondrop="drop(event)" ondragover="allowDrop(event)">';
                trDrag += (this.parentElement).innerHTML + '</tr>';
            }
            else {
                trDrag = '<tr class="pointStop" id="' + $(this).attr('id') + '" draggable="true" ondragstart="drag(event,this)" ondrop="drop(event)" ondragover="allowDrop(event)">';
                trDrag += (this).innerHTML + '</tr>';
            }

        });
        $('body').on('drop', '.pointStop,.lbPointName,.lbPointAddress', function () {
            if ($(this).attr('class') != 'pointStop') {
                trDrop = '<tr class="pointStop" id="' + $(this.parentElement).attr('id') + '" draggable="true" ondragstart="drag(event,this)" ondrop="drop(event)" ondragover="allowDrop(event)">';
                trDrop += (this.parentElement).innerHTML + '</tr>';
            }
            else {
                trDrop = '<tr class="pointStop" id="' + $(this).attr('id') + '" draggable="true" ondragstart="drag(event,this)" ondrop="drop(event)" ondragover="allowDrop(event)">';
                trDrop += (this).innerHTML + '</tr>';
            }
            // console.log(trDrag.substring(trDrag.indexOf("pointStop"),trDrag.indexOf("draggable")));
            if (trDrag != '' && trDrop != '')
                for (var i = 1; i < content.length - 1; i++) {
                    if ((content[i]).substring(content[i].indexOf("pointStop"), content[i].indexOf("draggable")) == trDrag.substring(trDrag.indexOf("pointStop"), trDrag.indexOf("draggable"))) {
                        content[i] = trDrop;
                        var index1 = i - 1;
                        // console.log(1, content);
                    }
                    else if ((content[i]).substring(content[i].indexOf("pointStop"), content[i].indexOf("draggable")) == trDrop.substring(trDrop.indexOf("pointStop"), trDrop.indexOf("draggable"))) {
                        content[i] = trDrag;
                        var index2 = i - 1;
                        // console.log(2, content);

                    }
                }
            // console.log(listPoint.point);
            if(index1 !=undefined && index2 !=undefined ) {
                var dem = listPoint.point[index1];
                listPoint.point[index1] = listPoint.point[index2];
                listPoint.point[index2] = dem;
                dem = listPrice[index1];
                listPrice[index1] = listPrice[index2];
                listPrice[index2] = dem;
                dem = listPoint.point[index1].TimeIntend;
                listPoint.point[index1].TimeIntend = listPoint.point[index2].TimeIntend;
                listPoint.point[index2].TimeIntend = dem;


                trDrop = '';
                trDrag = '';
                var html = '';
                for (var i = 0; i < content.length; i++) {
                    html += content[i];
                }

                // console.log(3, content);
                $('tbody').html('');
                $('tbody').html(html);
                Message('Thông báo', 'Thời gian dự tính đã tráo đổi. Hãy kiểm tra lại!');
            }
        });
    });

    function allowDrop(ev) {
    }

    function drag(ev) {
    }

    function drop(ev) {
    }
</script>