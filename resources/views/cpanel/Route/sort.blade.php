@extends('cpanel.template.layout')
@section('title', 'Sắp xếp tuyến tuyến')

@section('content')
    <style>
        #sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
        #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 35px; }
        #sortable li span { position: absolute; margin-left: -1.3em; }
    </style>
    <link href="/public/bootstrap/css/bootstrap-grid.min.css" type="text/css" rel="stylesheet">
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Sắp xếp tuyến</h3></div>
            </div>
        </div>
        <div style="padding: 10px;margin-bottom: 50px">
           <ul id="sortable">
               @foreach($routes as $route)
               <li class="ui-state-default route" data-route="{{$route['routeId']}}"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>{{ $route['routeName'] }}</li>
               @endforeach
           </ul>
            <button id="saveSort" class="btn btn-warning" style="position: fixed;top: 80px; right:50px">LƯU SẮP XẾP</button>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $("#sortable" ).sortable({
                placeholder: "ui-state-highlight"
            });
            $("#sortable" ).disableSelection();
            $('body').on('click','#saveSort',function () {
                $('#saveSort').prop('disabled',true);
                $('#saveSort').text('Đang Lưu');
                var listRoute = [];
                $.each($('.route'),function (k,e) {
                    var route = $(e).data('route');
                    listRoute.push({orderDisplay : parseInt(k+1),routeId : route});
                });
                console.log(listRoute);
                SendAjaxWithJson({
                    url : urlDBD('route/order'),
                    type : 'post',
                    data : listRoute,
                    dataType : 'json',
                    success : function (res) {
                        notyMessage("Lưu thành công!",'success');
                        $('#saveSort').prop('disabled',false);
                        $('#saveSort').text('LƯU SẮP XẾP');
                    },
                    functionIfError : function (err) {
                        $('#saveSort').prop('disabled',false);
                        $('#saveSort').text('LƯU SẮP XẾP');
                    }
                })
            })
        });
    </script>
@endsection