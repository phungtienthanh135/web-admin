@extends('cpanel.template.layout')
@section('title', 'Danh sách tuyến')

@section('content')

    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Danh sách tuyến</h3></div>
            </div>
        </div>
        <div class="khung_lich_ve">
            <div class="date_picker">
                <div style="padding: 7px;" class="widget widget-4">

                    <div class="widget-head">
                        <h4>Tìm tuyến</h4>
                    </div>
                </div>
                <div class="innerLR">
                    {{--<form action="{{action('PointController@search')}}">--}}
                    <form action="">
                        <div class="row-fluid">
                            <div class="span3">
                                <label>Tên tuyến</label>
                                <select id="routeName" class="w_full" name="routeName">
                                    <option>Chọn tuyến</option>
                                    @foreach($result as $route)
                                        <option {!! $route['routeName']== request('routeName') ? 'selected ' : ''!!} value="{{$route['routeId']}}">
                                            {{$route['routeName']}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="span2">
                                <label>Bến đi</label>
                                <select class="w_full" name="startPoint" id="txt_DiemDau">
                                    <option>Chọn điểm</option>
                                </select>
                            </div>
                            <div class="span2">
                                <label>Bến đến</label>
                                <select class="w_full" name="endPoint" id="txt_DiemCuoi">
                                    <option>Chọn điểm</option>

                                </select>
                            </div>

                            <div class="span2">
                                <label>&nbsp;</label>
                                <button class="btn btn-info btn-flat-full span12" id="search">TÌM TUYẾN</button>
                            </div>
                            @if(hasAnyRole(CREATE_ROUTE))
                                <div class="span2">
                                    <label>&nbsp;</label>
                                    <a href="{{action('RouteController@getadd')}}" style="border-radius:0px"
                                       class="span12 btn btn-warning" id="add">
                                        THÊM
                                        MỚI TUYẾN
                                    </a>
                                </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row-fluid bg_light m_top_10">
            <div class="widget widget-4 bg_light">
                <div class="widget-body">
                    <table class="table table-hover table-vertical-center">
                        <thead>
                        <tr>
                            <!-- <th>Mã tuyến</th> -->
                            <th>Tuyến</th>
                            <th>Bến đi</th>
                            <th>Bến đến</th>
                            <th class="center">Giá vé</th>
                            <th class="center">Có ăn</th>
                            <th>Tùy chọn</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($result as $row)
                            <tr>
                            <!--  <td>{{ $row['routeId'] }}</td> -->
                                <td>{{ $row['routeName'] }}</td>
                                <td>{{ $row['listPoint'][0]['pointName'] }}</td>
                                <td>{{@last($row['listPoint'])['pointName']}}</td>
                                <td class="center">
                                    @if($row['displayPrice'] != 0)
                                        @moneyFormat($row['displayPrice'])
                                    @else
                                        @if(count($row['listPriceByVehicleType']) >= count($row['listPoint']))
                                            @moneyFormat($row['listPriceByVehicleType'][count($row['listPoint'])-1])
                                        @else
                                            -
                                        @endif
                                    @endif
                                </td>

                                <td class="center">
                                    @if(last($row['listMealPrice'])!='-1' && !empty(last($row['listMealPrice'])))
                                        +@moneyFormat(@last($row['listMealPrice']))
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    <a style="cursor: pointer" tabindex="0"
                                       class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                        <ul class="onclick-menu-content">
                                            @if(hasAnyRole(UPDATE_ROUTE))
                                                <li>
                                                    <button onclick="window.location.href='{{action('RouteController@getEdit',['id'=>$row['routeId']],false)}}'">
                                                        Sửa tuyến
                                                    </button>
                                                </li>
                                            @endif
                                            @if(hasAnyRole(DELETE_ROUTE))
                                                <li>
                                                    <button data-toggle="modal" data-target="#modal_delete"
                                                            value="{{$row['routeId']}}"
                                                            onclick="$('#delete_route').val($(this).val())">Xóa
                                                    </button>
                                                </li>
                                            @endif
                                        </ul>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @include('cpanel.template.pagination-without-number',['page'=>$page])
                </div>
            </div>
        </div>
    </div>

    <!-- End Content -->
    @if(hasAnyRole(DELETE_ROUTE))
        {{--Model xóa--}}
        <div class="modal hide fade" id="modal_delete" style="width: 250px; margin-left: -10%;margin-top: 10%;">
            <div class="modal-body center">

                <p>Bạn có chắc muốn xoá?</p>
            </div>
            <form action="{{action('RouteController@delete',[],false)}}" method="post">
                <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="routeId" value="" id="delete_route">
                    <a style="cursor: pointer" data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                    <button class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
                </div>
            </form>
        </div>
    @endif

    <script>
        $(document).ready(function () {
            $('#txt_DiemCuoi').attr("disabled", true);
            $('#txt_DiemDau').attr("disabled", true);
            var listRoute ={!! json_encode($result)!!};
            $('#routeName').select2();
            $('#txt_DiemDau').select2();
            $('#txt_DiemCuoi').select2();
            $('#routeName').change(function () {
                var routeid = $(this).val();
                var html = '<option>Chọn điểm</option>';
                for (var i = 0; i < listRoute.length; i++) {
                    if (routeid == listRoute[i].routeId) {
                        for (var j = 0; j < (listRoute[i].listPoint).length; j++)
                            html += '<option value="' + listRoute[i].listPoint[j].pointId + '">' + listRoute[i].listPoint[j].pointName + '</option>';
                        $('#txt_DiemCuoi').attr("disabled", false);
                        $('#txt_DiemDau').attr("disabled", false);
                        i = listRoute.length;
                    } else {
                        if (i == listRoute.length - 1)
                            $('#txt_DiemCuoi').attr("disabled", true);
                        $('#txt_DiemDau').attr("disabled", true);
                    }
                }
                $('#txt_DiemDau').html('');
                $('#txt_DiemDau').html(html);
                $('#txt_DiemCuoi').html('');
                $('#txt_DiemCuoi').html(html);
            });
        });
    </script>
@endsection