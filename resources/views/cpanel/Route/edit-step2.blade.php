<div id="step2" style="display: none">
    <div class="row-fluid heading_top">
        <div class="pull-left span8"><h3>Cho phép bán</h3></div>
    </div>
    <div class="span12 bg_light">
        <div class="controls span12">
            <div class="span4">

            </div>
        </div>
        <div class="controls span12">
            <div class="span3">
                <input id="admin" type="checkbox" data-index="1" class="span12 sell">
                <label style="margin-top: 5px" class="f_left" for="admin">Website quản lí</label>
            </div>
            <div class="span3">
                <input id="online" type="checkbox" data-index="2" class="span12 sell">
                <label style="margin-top: 5px" class="f_left" for="online">Website bán online</label>
            </div>
            <div class="span3">
                <input id="app" type="checkbox" data-index="3" class="span12 sell">
                <label style="margin-top: 5px" class="f_left" for="app">Trên di động</label>
            </div>
            <div class="span3">
                <input id="Agency" type="checkbox" data-index="4" class="span12 sell">
                <label style="margin-top: 5px" class="f_left" for="Agency">Đại lí</label>
            </div>
        </div>
        {{--<div class="controls span12">--}}
            {{--<div class="span4">--}}
                {{--<input id="online" type="checkbox" data-index="2" class="span12 sell">--}}
                {{--<label style="margin-top: 5px" class="f_left" for="online">Website bán online</label>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="controls span12">--}}
            {{--<div class="span4">--}}
                {{--<input id="app" type="checkbox" data-index="3" class="span12 sell">--}}
                {{--<label style="margin-top: 5px" class="f_left" for="app">Trên di động</label>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="controls span12">--}}
            {{--<div class="span4">--}}
                {{--<input id="Agency" type="checkbox" data-index="4" class="span12 sell">--}}
                {{--<label style="margin-top: 5px" class="f_left" for="Agency">Đại lí</label>--}}
            {{--</div>--}}
        {{--</div>--}}
        <input name="listNotSell" id="listNotSell" type="hidden" value="">
    </div>
    <div class="heading_top" style="padding-top: 100px">
        <div class="row-fluid">
            <div class="pull-left span8"><h3>Thêm bảng giá <span>(2/2)</span></h3></div>
        </div>
    </div>
    <div class="row-fluid bg_light">
        <div class="widget widget-4 bg_light" style="    width: 15%;margin-left: 10px;float: left;">
            <label class="control-label" for="displayPrice">Giá của tuyến</label>
            <input type="number" style="width: 100px" name="displayPrice" id="displayPrice"
                   oninput="checkDisplayPrice()"/>
        </div>
        <div class="widget widget-4 bg_light" style="width: 15%; margin-left: 140px;">
            <label class="control-label" for="displayPrice">Tỉ lệ giá đồ(%)</label>
            <input type="number" style="width: 100px" name="displayPriceShip" id="displayPriceShip" max="100"
                   placeholder="VD: 50"/>
        </div>
        <div class="widget widget-4 bg_light">
            <div class="widget-body">
                <div class="innerLR">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div>( Giá -1 là không bán )</div>
                    <table class="table giatuyen">
                        <thead>
                        <tr>
                            <th>Bến</th>
                            <th>Hà Nội</th>
                            <th>Ninh Bình</th>
                            <th>Thanh Hoá</th>
                            <th>Nha Trang</th>
                            <th>TP.HCM</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Hà Nội</td>
                            <td class="giarong"></td>
                            <td class="dacogia">
                                <span>250.000 VNĐ</span>
                                <a tabindex="0"
                                   class="glyphicon glyphicon-option-vertical onclick-menu pull-right"><i></i>
                                    <ul class="onclick-menu-content">
                                        <li>
                                            <button data-toggle="modal" data-target="#modal_updatePrice">Sửa Giá
                                            </button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn_noSell">Không bán</button>
                                        </li>
                                    </ul>
                                </a>
                            </td>
                            <td class="dacogia"><span>250.000 VNĐ</span>
                                <a tabindex="0"
                                   class="glyphicon glyphicon-option-vertical onclick-menu pull-right"><i></i>
                                    <ul class="onclick-menu-content">
                                        <li>
                                            <button data-toggle="modal" data-target="#modal_updatePrice">Sửa Giá
                                            </button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn_noSell">Không bán</button>
                                        </li>
                                    </ul>
                                </a></td>
                            <td class="chuacogia"><span>Chưa có giá</span>
                                <a tabindex="0"
                                   class="glyphicon glyphicon-option-vertical onclick-menu pull-right"><i></i>
                                    <ul class="onclick-menu-content">
                                        <li>
                                            <button data-toggle="modal" data-target="#modal_updatePrice">Sửa Giá
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn_noSell">Không bán</button>
                                        </li>
                                    </ul>
                                </a></td>
                            <td class="chuacogia"><span>Chưa có giá</span>
                                <a tabindex="0"
                                   class="glyphicon glyphicon-option-vertical onclick-menu pull-right"><i></i>
                                    <ul class="onclick-menu-content">
                                        <li>
                                            <button data-toggle="modal" data-target="#modal_updatePrice">Sửa Giá
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn_noSell">Không bán</button>
                                        </li>
                                    </ul>
                                </a></td>
                        </tr>

                        <tr>
                            <td>Ninh Bình</td>

                            <td class="dacogia"><span>200.000 VNĐ</span></td>
                            <td class="giarong"></td>
                            <td class="khongban"><span>Không bán</span></td>
                            <td class="chuacogia"><span>Chưa có giá</span></td>
                            <td class="chuacogia"><span>Chưa có giá</span></td>
                        </tr>

                        <tr>
                            <td>Thanh Hoá</td>

                            <td class="dacogia"><span>200.000 VNĐ</span></td>
                            <td class="khongban"><span>Không bán</span></td>
                            <td class="giarong"></td>
                            <td class="chuacogia"><span>Chưa có giá</span></td>
                            <td class="chuacogia"><span>Chưa có giá</span></td>
                        </tr>

                        <tr>
                            <td>Nha Trang</td>

                            <td class="dacogia"><span>200.000 VNĐ</span></td>
                            <td class="dacogia"><span>250.000 VNĐ</span></td>
                            <td class="chuacogia"><span>Chưa có giá</span></td>
                            <td class="giarong"></td>
                            <td class="chuacogia"><span>Chưa có giá</span></td>
                        </tr>

                        <tr>
                            <td>TP.HCM</td>

                            <td class="dacogia"><span>250.000 VNĐ</span></td>
                            <td class="dacogia"><span>250.000 VNĐ</span></td>
                            <td class="chuacogia"><span>Chưa có giá</span></td>
                            <td class="chuacogia"><span>Chưa có giá</span></td>
                            <td class="giarong"></td>
                        </tr>


                        </tbody>
                    </table>
                    <div class="row-fluid m_top_15">
                        <input type="hidden" name="listPoint" id="listPoint">
                        <input type="hidden" name="listPrice" id="listPrice">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="button" value="save" class="btnSubmit span3 btn btn-warning btn-flat">LƯU</button>
                        <button type="button" value="saveAsNew" class="btnSubmit span3 btn btn-warning btn-flat">LƯU THÀNH TUYẾN MỚI</button>
                        <button type="button" id="btn_back"
                                class="span2 btn btn-default btn-flat no_border bg_light">QUAY LẠI
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<script>
    $('.sell').change(function () {
        var list = document.getElementsByClassName('sell');
        var listOption = '[';
        for (var i = 0; i < list.length; i++) {
            if (!list[i].checked)
                if (listOption == '[') listOption += $(list[i]).data('index');
                else
                    listOption += ',' + $(list[i]).data('index');
        }
        listOption += ']';
        $('#listNotSell').val(listOption);
    });
</script>