@extends('cpanel.template.layout')
@section('title', 'Sửa tuyến')
@push('activeMenu')
    activeMenu('{{action('RouteController@show')}}');
@endpush
@section('content')
    <style>
        .select2-container {
            z-index: 10002;
            width: 100% !important;
        }

        .display {
            font-weight: bold;
        }

        select#start_transshipment, #end_transshipment, #select_transshipment {
            /* for Firefox */
            -moz-appearance: none;
            /* for Chrome */
            -webkit-appearance: none;
            padding-top: 0px;
            width: 100%;
            height: 32px;
        }

        /* For IE10 */
        select#start_transshipment, #end_transshipment, #select_transshipment::-ms-expand {
            display: none;
        }
    </style>
    <form id="frmAdd" action="{{action('RouteController@postEdit')}}" method="post">
        <div id="content">
            @include('cpanel.Route.edit-step1')
            @include('cpanel.Route.edit-step2')
        </div>

        <!-- Modal inline -->
        <div class="modal hide fade no_border" data-keyboard="false" data-backdrop="static" id="modal_Route">
            <div class="modal-header header_modal center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>SỬA THÔNG TIN TUYẾN</h3>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label" for="routeName">Tên tuyến</label>
                        <div class="controls form-inline">
                            <input required type="text" id="routeName" name="routeName" class="span12" minlength="1"
                                   value="{{$resultRoute['route']['routeName']}}"/>
                            <input type="hidden" name="routeId" value="{{$resultRoute['route']['routeId']}}">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="routeNameShort">Tên tuyến rút gọn</label>
                        <div class="controls form-inline">
                            <input required type="text" id="routeNameShort" name="routeNameShort" class="span12"
                                   minlength="1"
                                   maxlength="30" value="{{$resultRoute['route']['routeNameShort']}}"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Điểm đầu</label>
                        <div class="controls">

                            <select class="span12" id="pointStrar">
                                <option value="{{head($resultRoute['listPoint'])['pointId']}}">{{head($resultRoute['listPoint'])['pointName']}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" style="margin-top: -10px;">
                        <label class="control-label">Thêm trung chuyển</label>
                        <div class="controls">
                            <div id="addstart" class="span3" style="float: left;width: 7%">
                                <a style="color: firebrick;" class="addpoint btn">+</a>
                            </div>
                            <div style="float: left;width:90%" class="span9">
                                <select id="start_transshipment">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Điểm cuối</label>
                        <div class="controls">
                            <select class="span12" id="pointEnd">
                                <option value="{{last($resultRoute['listPoint'])['pointId']}}">{{last($resultRoute['listPoint'])['pointName']}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" style="margin-top: -10px;">
                        <label class="control-label">Thêm trung chuyển</label>
                        <div class="controls">
                            <div id="addend" class="span3" style="float: left;width: 7%">
                                <a style="color: firebrick;" class="addpoint btn">+</a>
                            </div>
                            <div style="float: left;width:90%" class="span9">
                                <select id="end_transshipment">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Chọn tuyến khứ hồi</label>
                        <div class="controls">
                            <select id="routeRound" class="span12" name="routeRound" value="">
                            </select>
                        </div>
                    </div>
                    <input type="hidden" value="" name="roundId" id="roundId">
                    {{--<div class="control-group">
                        <label class="control-label" for="cbbListVehicleType">Loại xe</label>
                        <div class="controls">
                            <select class="span12" name="listVehicleType[]" id="cbbListVehicleType">
                                <option value="">Chọn loại xe</option>
                                @foreach($listVehicleType as $VehicleType)
                                    <option value="{{$VehicleType['vehicleTypeId']}}">{{$VehicleType['vehicleTypeName']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>--}}
                    @php(date_default_timezone_set('GMT'))
                    <div class="control-group">
                        <label class="control-label" for="hourTimeIntendOfRoute">Thời gian dự kiến</label>
                        <div class="controls form-inline">
                            <div class="input-append span4">
                                <input type="number" id="hourTimeIntendOfRoute" class="span6"
                                       value="{{floor(last($resultRoute['route']['listTimeIntend'])/3600000)}}" min="0">
                                <button type="button" class="btn" disabled="">Giờ</button>
                            </div>
                            <div class="input-append span4">
                                <input type="number" class="span6" id="minuteTimeIntendOfRoute"
                                       value="{{date('i',last($resultRoute['route']['listTimeIntend'])/1000)}}" min="0"
                                       max="60">
                                <button type="button" class="btn" disabled="">Phút</button>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="childrenTicketRatio">Tỷ lệ giá vé trẻ em so với người
                            lớn</label>
                        <div class="controls">
                            <div class="input-append span8">
                                <input type="number" id="childrenTicketRatio" name="childrenTicketRatio" class="span10"
                                       value="{{$resultRoute['route']['childrenTicketRatio']*100}}" min="0" max="100">
                                <button type="button" class="btn" disabled="">%</button>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="txtPriceMealOfRoute">Bao gồm ăn</label>
                        <div style="padding-top:5px" class="controls">
                            <input {{last($resultRoute['route']['listMealPrice'])>0?'checked':''}} type="checkbox"
                                   id="cbPriceMealOfRoute"/>
                            <label class="f_left" for="cbPriceMealOfRoute"></label>
                            <input value="{{last($resultRoute['route']['listMealPrice'])>0?last($resultRoute['route']['listMealPrice']):''}}"
                                   {{last($resultRoute['route']['listMealPrice'])>0?'':'readonly'}} type="number"
                                   style="margin-top:-5px" class="span11"
                                   id="txtPriceMealOfRoute"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="txtPickUpHomeOfRoute">Bao gồm đưa đón</label>
                        <div style="padding-top:5px" class="controls">
                            <input {{last($resultRoute['route']['listPickUpHome'])>0?'checked':''}} type="checkbox"
                                   id="cbPickUpHomeOfRoute"/>
                            <label class="f_left" for="cbPickUpHomeOfRoute"></label>
                            <input type="number" style="margin-top:-5px"
                                   value="{{last($resultRoute['route']['listPickUpHome'])>0?last($resultRoute['route']['listPickUpHome']):''}}"
                                   {{last($resultRoute['route']['listPickUpHome'])>0?'':'readonly'}} class="span11"
                                   id="txtPickUpHomeOfRoute"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="controls">
                            <div class="row-fluid">
                                <div class="span9">
                                    <button type="button" id="btnUpdateRoute" class="btn btn-warning btn-flat-full">LƯU
                                    </button>
                                </div>
                                <div class="span3">
                                    <a data-dismiss="modal" id="btn_huy"
                                       class="btn btn-default btn-flat no_border bg_light">HUỶ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal hide fade no_border" id="modal_Point" data-keyboard="false" data-backdrop="static">
            <div class="modal-header header_modal center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="title_bendo">THÊM BẾN ĐỖ</h3>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning">
                    Thời gian dự kiến tính từ bến đi.
                </div>
                <div class="form-horizontal">


                    <div class="control-group">
                        <label class="control-label" for="point">Chọn điểm dừng</label>
                        <div class="controls">
                            <select class="span12" id="point">
                                <option value="">Chọn điểm dừng</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group" style="margin-top: -10px;">
                        <label class="control-label">Thêm trung chuyển</label>
                        <div class="controls">
                            <div id="editpoint" class="span3" style="float: left;width: 7%">
                                <a style="color: firebrick;" class="addpoint btn">+</a>
                            </div>
                            <div style="float: left;width:90%" class="span9">
                                <select id="select_transshipment">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="hourTimeIntendOfPoint">Thời gian dự kiến</label>
                        <div class="controls form-inline">
                            <div class="input-append span4">
                                <input type="number" id="hourTimeIntendOfPoint" class="span6"
                                       value="0" min="0"/>
                                <button type="button" class="btn" disabled="">Giờ</button>
                            </div>
                            <div class="input-append span4">
                                <input type="number" class="span6" id="minuteTimeIntendOfPoint"
                                       value="0" min="0" max="60"/>
                                <button type="button" class="btn" disabled="">Phút</button>
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="txtPriceMealOfPoint">Bao gồm ăn</label>
                        <div style="padding-top:5px" class="controls">
                            <input type="checkbox" id="cbPriceMealOfPoint"/>
                            <label class="f_left" for="cbPriceMealOfPoint"></label>
                            <input type="text" style="margin-top:-5px" class="span11" id="txtPriceMealOfPoint"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="txtPickUpHomeOfPoint">Bao gồm đưa đón</label>
                        <div style="padding-top:5px" class="controls">
                            <input type="checkbox" id="cbPickUpHomeOfPoint"/>
                            <label class="f_left" for="cbPickUpHomeOfPoint"></label>
                            <input type="text" style="margin-top:-5px" class="span11" id="txtPickUpHomeOfPoint"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="controls">
                            <div class="row-fluid">
                                <div class="span9">
                                    <button type="button" id="btnAddPoint" class="btn btn-warning btn-flat-full">THÊM
                                    </button>
                                    <button type="button" id="btnEditPoint" class="btn btn-warning btn-flat-full">CẬP
                                        NHẬT
                                    </button>
                                    <input type="hidden" name="" id="editPointId">
                                </div>
                                <div class="span3">
                                    <a data-dismiss="modal" id="btn_huy"
                                       class="btn btn-default btn-flat no_border bg_light">HUỶ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal hide fade no_border" id="modal_updatePrice">
            <div class="modal-header header_modal center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>CHỈNH SỬA MỨC GIÁ</h3>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="control-group">
                        <label for="txtBenDi" class="control-label">Bến đi</label>
                        <div class="controls">
                            <input readonly type="text" class="span12" id="txtPointStart">
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="txtBenDen" class="control-label">Bến đến</label>
                        <div class="controls">
                            <input readonly type="text" class="span12" id="txtPointEnd">
                        </div>
                    </div>
                    {{--<div class="control-group">
                        <label class="control-label">Trạng thái</label>
                        <div class="controls">
                            <select class="span12">
                                <option value="">Có vé bán</option>
                            </select>
                        </div>
                    </div>--}}
                    <div class="control-group">
                        <label class="control-label" for="txtPrice">Giá</label>
                        <div class="controls">
                            <input type="text" class="span12" id="txtPrice">
                        </div>
                    </div>

                    <div class="control-group">
                        <div style="margin-left: 145px;" class="f_left m_right_10">
                            Sử dụng làm giá của tuyến
                        </div>
                        <input type="checkbox" id="displayPriceChk"/>
                        <label class="f_left" for="displayPriceChk"></label>
                        <input type="hidden" name="displayPriceIndex" id="index"/>
                    </div>

                    <div class="control-group">
                        <div class="controls">
                            <div class="row-fluid">
                                <div class="span5">
                                    <button type="button" id="btn_updatePrice" class="btn btn-warning btn-flat-full">LƯU
                                    </button>
                                </div>

                                <div class="span3">
                                    <a data-dismiss="modal" id="btn_huy"
                                       class="btn btn-default btn-flat no_border bg_light">HUỶ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
    <div class="modal" id="modal_add" data-keyboard="false" data-backdrop="static">
        <div class="modal-header">
            {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--}}
            <h3 class="center">Thêm điểm trung chuyển</h3>
        </div>
        <div class="span12">
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label"></label>
                    <div class="controls">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Phụ thu</label>
                    <div class="controls">
                        <input placeholder="VNĐ" type="number" class="pricemodal right" style="width: 80%">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Chọn điểm</label>
                    <div class="controls" style="width: 62%;">
                        <select id="selectmodal">
                            <option value="">Chọn điểm</option>
                        </select>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal-footer">
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label"></label>
                    <div class="controls">
                        <div class="row-fluid" style=" margin-left: 4px;width: 88%">
                            <div class="span9">
                                <button id="btnadd" class="btn btn-warning btn-flat-full">THÊM</button>
                            </div>
                            <div class="span3">
                                <button id="btnabort" class="btn btn-default btn-flat-full no_border">HỦY
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="modal_edit" data-keyboard="false" data-backdrop="static">
        <div class="modal-header">
            {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--}}
            <h3 class="center">Sửa điểm trung chuyển</h3>
        </div>
        <div class="span12">
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label"></label>
                    <div class="controls">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Phụ thu</label>
                    <div class="controls">
                        <input placeholder="VNĐ" type="number" class="pricepoint right" style="width: 80%">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Tên điểm</label>
                    <div class="controls">
                        <input type="text" class="namepoint right" style="width: 80%">
                    </div>
                </div>

            </div>
        </div>
        <input type="hidden" id="idpoint" data-id="" value="">
        <div class="modal-footer">
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label"></label>
                    <div class="controls">
                        <div class="row-fluid" style="margin-left: 4px;width: 88%">
                            <div class="span9">
                                <button id="updatepoint" class="btn btn-warning btn-flat-full">Cập nhật</button>
                            </div>
                            <div class="span3">
                                <button type="reset" data-dismiss="modal" aria-hidden="true"
                                        id="deletepoint" class="btn btn-default btn-flat-full no_border">Bỏ điểm
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <script>
        var listPoint = {
                pointStrar: {
                    TimeIntend: '',
                    address: "",
                    id: "",
                    mealprice: '',
                    pickUpHome: '',
                    pointName: '',
                    selected: true,
                    text: ''
                },
                pointEnd: {
                    TimeIntend: '',
                    address: "",
                    id: "",
                    mealprice: '',
                    pickUpHome: '',
                    pointName: '',
                    selected: true,
                    text: ''
                },
                point: []
            },
            classStyle,
            route,
            currentPoint = {},
            currentElement,
            displayPrice,
            goodsPriceRatio,
            indexPrice,
            indexChecked,
            listPrice = [],
            listTransshipment = '',
            start_point = '',
            end_point = '',
            urlSearchPoint = '{{action('PointController@search')}}';
            urlAddRoute= '{{action('RouteController@postadd')}}';
            urlUpdateRoute = '{{action('RouteController@postEdit')}}';

        var first = document.getElementsByClassName('first');
        var add = document.getElementsByClassName('addPoint');
        var last = document.getElementsByClassName('last');
        var lsPoint ={!! json_encode($resultRoute['listPoint'])!!};
        var trDrag;
        var trDrop;
        var checkone = false;
        var content = [];


        (init());

        function init() {
            var listRoute = {!! json_encode($listRound['results']['result'], JSON_PRETTY_PRINT) !!};
            var html = '';
            var name = '';
            for (var i = 0; i < listRoute.length; i++) {
                html += "<option  data-id='" + listRoute[i].routeId + "'>" + listRoute[i].routeName + "</option>";
                if (listRoute[i].routeId == '{{@$resultRoute["route"]["routeBack"]}}') {
                    name = listRoute[i].routeName;
                }
            }
            $('#routeRound').append(html);
            $('#routeRound').attr('value', name);
            $('#start_transshipment').hide();
            $('#end_transshipment').hide();
            $('#modal_add').hide();
            $('#modal_edit').hide();
            $('#modal_Point').modal('hide');
        }

        if (content.length == 0) {
            content[0] = '<tr class="first">' + first[0].innerHTML + '</tr>';
            for (var i = 1; i < lsPoint.length - 1; i++) {
                content[i] = '<tr class="pointStop" id="' + lsPoint[i].pointId + '" draggable="true" ondragstart="drag(event,this)" ondrop="drop(event)" ondragover="allowDrop(event)">';
                content[i] += '<td>' +
                    '                                                    <div></div>' +
                    '                                                </td>' +
                    '                                                <td class="lbPointName">' + lsPoint[i].pointName + '</td>' +
                    '                                                <td class="lbPointAddress">' + lsPoint[i].address + '</td>' +
                    '                                                <td><a tabindex="0"' +
                    '                                                       class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>' +
                    '                                                        <ul class="onclick-menu-content">' +
                    '                                                            <li>' +
                    '                                                                <button type="button" onclick="editPoint(this)">Sửa bến' +
                    '                                                                    đỗ' +
                    '                                                                </button>' +
                    '                                                            </li>' +
                    '                                                            <li>' +
                    '                                                                <button type="button" onclick="deletePoint(this)">Xóa' +
                    '                                                                </button>' +
                    '                                                            </li>' +
                    '                                                        </ul>' +
                    '                                                    </a></td></tr>';
            }
            content[lsPoint.length - 1] = '<tr class="addPoint">' + add[0].innerHTML + '</tr>' + '<tr class="last">' + last[0].innerHTML + '</tr>';
        }

        function load() {
            route =  {!! json_encode($resultRoute,JSON_PRETTY_PRINT) !!};
            console.log(route);
            $(route.listPoint).each(function (index, item) {
                if (index == 0 || index == route.listPoint.length - 1) {
                    if (index == 0) {
                        listPoint.pointStrar.pointName = item.pointName;
                        listPoint.pointStrar.address = item.address;
                        listPoint.pointStrar.id = item.pointId;
                        listPoint.pointStrar.mealprice = route.route.listMealPrice[index];
                        listPoint.pointStrar.pickUpHome = route.route.listPickUpHome[index];
                        listPoint.pointStrar.TimeIntend = route.route.listTimeIntend[index];
                    }
                    if (index == route.listPoint.length - 1) {
                        listPoint.pointEnd.pointName = item.pointName;
                        listPoint.pointEnd.address = item.address;
                        listPoint.pointEnd.id = item.pointId;
                        listPoint.pointEnd.mealprice = route.route.listMealPrice[index];
                        listPoint.pointEnd.pickUpHome = route.route.listPickUpHome[index];
                        listPoint.pointEnd.TimeIntend = route.route.listTimeIntend[index];
                    }
                }
                else {
                    listPoint.point.push({
                        pointName: item.pointName,
                        address: item.address,
                        id: item.pointId,
                        mealprice: route.route.listMealPrice[index],
                        pickUpHome: route.route.listPickUpHome[index],
                        TimeIntend: route.route.listTimeIntend[index],
                        selected: true,
                        text: ''
                    })
                }
            });
            console.log('tra', route.listPoint);
            listPrice = route.route.listPriceByVehicleType;
            displayPrice = route.route.displayPrice;
            goodsPriceRatio = route.route.goodsPriceRatio;
            indexChecked = route.route.displayPriceIndex;
            $("#index").val(indexChecked);

            var listCheck = document.getElementsByClassName('sell');
            $('.sell').prop('checked', true);
            for (var i = 0; i < listCheck.length; i++) {
                $(route.route.listLockRoute).each(function (k, v) {
                    if ($(listCheck[i]).data('index') == v) listCheck[i].checked = false;
                });
            }
        }

        $(document).ready(function (e) {
            load();
            $('body').on('click', '#addstart>a.addpoint', function () {
                $('#modal_Route').hide();
                $('#modal_Route').attr('aria-hidden', true);
                $('#selectmodal').html('<option>Chọn điểm</option>');
                $('.pricemodal').val('');
                $('#modal_add').show();
                showModal(document.getElementById('modal_add'));
                $('#btnadd').attr('data-id', 'addstart');
                sessionStorage.setItem('point', 'start');
            });
            $('body').on('click', '#addend>a.addpoint', function () {
                $('#modal_Route').hide();
                $('#selectmodal').html('<option>Chọn điểm</option>');
                $('.pricemodal').val('');
                $('#modal_add').show();
                showModal(document.getElementById('modal_add'));
                $('#btnadd').attr('data-id', 'addend');
                sessionStorage.setItem('point', 'end');
            });
            $('body').on('click', '#editpoint>a.addpoint', function () {
                $('#modal_Point').hide();
                $('#selectmodal').html('<option>Chọn điểm</option>');
                $('.pricemodal').val('');
                $('#modal_add').show();
                showModal(document.getElementById('modal_add'));
                $('#btnadd').attr('data-id', '');
                sessionStorage.setItem('point', '');
            });
            $('body').on('click', '#updatepoint', function () {
                var price = $('.pricepoint').val();
                var name = $('.namepoint').val();
                var idtransship = $('#idpoint').data('select');
                var selectitem = $('#idpoint').data('id');
                $('#' + idtransship + ' option').each(function () {
                    if ($(this).data('id') == selectitem) {
                        $(this).attr('data-price', price);
                        $(this).attr('data-name', name);
                        $(this).text(name);
                    }
                });
                $('#modal_edit').hide();

                if (sessionStorage.getItem('open') == 1) $('#modal_Route').show();
                else $('#modal_Point').show();
            });
            $('body').on('click', '#deletepoint', function () {
                var idtransship = $('#idpoint').data('select');
                var selectitem = $('#idpoint').data('id');
                var index = -1;
                $('#' + idtransship + ' option').each(function () {
                    index++;
                    if ($(this).data('id') == selectitem) {
                        return false;
                    }
                });
                $('#' + idtransship).find('option').get(index).remove();
                $('#modal_edit').hide();
                if (sessionStorage.getItem('open') == 1) $('#modal_Route').show();
                else $('#modal_Point').show();
            });
            $('body').on('change', '#start_transshipment,#end_transshipment ,#select_transshipment', function () {
                $('#idpoint').val($(this).val());
                if ($(this).val() != 'Xem danh sách') {
                    if ($(this).is('#start_transshipment')) {
                        $('#idpoint').attr('data-select', 'start_transshipment');
                        $('#idpoint').attr('data-id', $(this.options[this.selectedIndex]).data('id'));
                        $('#modal_Route').hide();
                    }
                    if ($(this).is('#end_transshipment')) {
                        $('#idpoint').attr('data-select', 'end_transshipment');
                        $('#idpoint').attr('data-id', $(this.options[this.selectedIndex]).data('id'));
                        $('#modal_Route').hide();
                    }
                    if ($(this).is('#select_transshipment')) {
                        $('#idpoint').attr('data-select', 'select_transshipment');
                        $('#idpoint').attr('data-id', $(this.options[this.selectedIndex]).data('id'));
                        $('#modal_Point').hide();
                    }
                    $('.pricepoint').val($(this.options[this.selectedIndex]).data('price'));
                    $('.namepoint').val($(this).val());
                    $('#modal_edit').show();
                }
            });
            $('body').on('click', '#btnadd', function () {
                $('#modal_add').hide();
                if (sessionStorage.getItem('point') == 'start') {
                    if ($('#selectmodal').text() != '') $('#start_transshipment').show();
                    var check = true;
                    $('#start_transshipment option').each(function () {
                        if ($(this).data('id') == $('#selectmodal option:selected').val()) check = false;
                    });
                    if ($('#selectmodal option:selected').val() != $('#selectmodal option:selected').text())
                        check = true;
                    else check = false;
                    if (check) {
                        var element = document.createElement('option');
                        $(element).attr('data-id', $('#selectmodal option:selected').val());
                        $(element).prop('selected', true);
                        var text = $('#selectmodal option:selected').text();
                        // console.log(text,text.indexOf('>')+1, text.indexOf('<'))
                        $(element).html(text.substring(text.indexOf('>') + 1, text.indexOf('.')));
                        $(element).attr('data-price', $('.pricemodal').val());
                        $(element).attr('data-name', text.substring(text.indexOf('>') + 1, text.indexOf('.')));
                        document.getElementById('start_transshipment').add(element, 1);
                    }
                }
                else {
                    if (sessionStorage.getItem('point') == 'end') {
                        if ($('#selectmodal').text() != '') $('#end_transshipment').show();
                        var check = true;
                        $('#end_transshipment option').each(function () {
                            if ($(this).data('id') == $('#selectmodal option:selected').val()) check = false;
                        });
                        if ($('#selectmodal option:selected').val() != $('#selectmodal option:selected').text())
                            check = true;
                        else check = false;
                        if (check) {
                            var element = document.createElement('option');
                            $(element).attr('data-id', $('#selectmodal option:selected').val());
                            $(element).prop('selected', true);
                            var text = $('#selectmodal option:selected').text();
                            // console.log(text,text.indexOf('>')+1, text.indexOf('<'))
                            $(element).html(text.substring(text.indexOf('>') + 1, text.indexOf('.')));
                            $(element).attr('data-price', $('.pricemodal').val());
                            $(element).attr('data-name', text.substring(text.indexOf('>') + 1, text.indexOf('.')));
                            document.getElementById('end_transshipment').add(element, 1);
                        }


                    }
                    else {
                        var check = true;
                        if ($('#selectmodal').text() != '') $('#select_transshipment').show();
                        $('#select_transshipment option').each(function () {
                            if ($(this).data('id') == $('#selectmodal option:selected').val()) check = false;
                        });
                        if ($('#selectmodal option:selected').val() != $('#selectmodal option:selected').text())
                            check = true;
                        else check = false;
                        if (check) {
                            var element = document.createElement('option');
                            $(element).attr('data-id', $('#selectmodal option:selected').val());
                            $(element).prop('selected', true);
                            var text = $('#selectmodal option:selected').text();
                            $(element).html(text.substring(text.indexOf('>') + 1, text.indexOf('.')));
                            $(element).attr('data-price', $('.pricemodal').val());
                            $(element).attr('data-name', text.substring(text.indexOf('>') + 1, text.indexOf('.')));
                            document.getElementById('select_transshipment').add(element, 1);
                        }
                    }
                }
                if (sessionStorage.getItem('point') != '') showModal(document.getElementById('modal_Route'));
                else showModal(document.getElementById('modal_Point'));
                if ($('#selectmodal option:selected').val() == $('#selectmodal option:selected').text())
                    alert('Điểm trung chuyển chưa được tạo ra');
            });
            $('body').on('click', '#open_Modal_Route', function () {
                sessionStorage.setItem('open', 1);
            });
            $('body').on('click', '#open_Modal_Point', function () {
                sessionStorage.setItem('open', 2);
            });
            $('#modal_Route').on('shown.bs.modal', function () {
                $('#start_transshipment').html('<option>Xem danh sách</option>' + $('select#firstpoint').html());
                $('#end_transshipment').html('<option>Xem danh sách</option>' + $('select#lastpoint').html());
                if ($('select#firstpoint option').length > 0) $('#start_transshipment').show();
                else $('#start_transshipment').hide();
                if ($('select#lastpoint option').length > 0) $('#end_transshipment').show();
                else $('#end_transshipment').hide();
            });

            $('body').on('click', '#btnabort', function () {
                // hiddenModal(document.getElementById('modal_add'));
                $('#modal_add').attr('aria-hidden', 'true');
                document.getElementById('modal_add').style.display = 'none';
                $('#modal_add').removeClass('in');
                $('.pricemodal').val('');
                $('#selectmodal').html('<option>Chọn điểm</option>');
                if ($('#btnadd').data('id') != '') showModal(document.getElementById('modal_Route'));
                else showModal(document.getElementById('modal_Point'));
            });

            $('body').on('focus', '.chuacogia,.dacogia', function () {
                classStyle = $(this).attr('class');
                $(this).removeClass(classStyle.toString());
                var content = sessionStorage.getItem($(this).attr('id')) > 0 ? sessionStorage.getItem($(this).attr('id')) : ($(this).data('price') >= 0 ? $(this).data('price') : 0);
                var html = '<input class="setPrice price" type="number" min="0" style="width: 90%;min-width:100px;background-color:white;border: none;" value="' + content + '">';
                $(this).html('');
                $(this).html(html);
            });
            $('body').on('focusout', '.setPrice', function () {
                var content = $(this).val();
                sessionStorage.setItem($(this.parentElement).attr('id'), content);
                $(this.parentElement).attr('data-price', sessionStorage.getItem($(this.parentElement).attr('id')));
                if (parseInt(content) < 0) {
                    var html = '<input type="button" style="width: 100%;background-color:#ffc985;border: none;" value="Chưa bán">';
                    if (classStyle == 'price dacogia') classStyle = 'price chuacogia';
                    $(this.parentElement).attr('class', classStyle);
                } else {
                    var html = '<input type="button" style="width: 90%;background-color:white;border: none;" value="' + moneyFormat(sessionStorage.getItem($(this.parentElement).attr('id'))) + '">';
                    $(this.parentElement).attr('class', 'price dacogia');
                }
                $(this.parentElement).html(html);
            });
            $('#displayPriceShip').val(goodsPriceRatio * 100);
            if (displayPrice != 0) {
                $("#displayPriceChk").prop('checked', true);
                $("#displayPrice").val(displayPrice);
            }

            $('#routeRound').bind('change', function () {
                $('#routeRound').attr('value', $('#routeRound option:selected').val());
                $('#roundId').attr('value', $('#routeRound option:selected').data('id'));
            });
            $("#displayPriceChk").change(function () {
                if (this.checked) {
                    var price = $('#txtPrice').val();
                    indexChecked = indexPrice;
                    $("#index").val(indexPrice);
                    $("#displayPrice").val(price);
                    return;
                }
                $("#displayPrice").val(0);
            });
            $('#btn_next').click(function () {
                if (listPoint.pointStrar.id != undefined && listPoint.pointEnd.id != undefined) {
                    $('#step1').hide();
                    $('#step2').show();
                    generateTablePrice();
                } else {
                    Message('Cảnh báo', 'Vui lòng chọn bến đi và bến đến', '');
                }

            });
            $('#btn_back').click(function () {
                $('#step1').show();
                $('#step2').hide();

            });

            $('#pointStrar,#pointEnd,#point').select2({
                "language": "vi",
                minimumInputLength: 2,
                escapeMarkup: function (markup) {
                    return markup;
                },
                //templateResult: formatRepo,
                //templateSelection: formatRepoSelection,
                tags: [],
                ajax: {
                    url: urlSearchPoint,
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function (param) {
                        return {
                            searchWord: param.term
                        };
                    },
                    processResults: function (data) {
                        console.log(data);
                        return {
                            results:
                                $.map(data.result, function (item) {
                                    return {
                                        text: "<h6 style='margin: 0'>" + item.pointName + "</h6>" + "<small>Địa chỉ: " + item.address + "</small>",
                                        id: item.pointId,
                                        address: item.address,
                                        pointName: item.pointName
                                    }
                                })
                        };
                    }
                }
            });

            $('.selectTransshipment,#selectmodal').select2({
                "language": "vi",
                minimumInputLength: 2,
                escapeMarkup: function (markup) {
                    return markup;
                },
                tags: [],
                ajax: {
                    url: urlSearchPoint,
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function (param) {
                        return {
                            searchWord: param.term,
                            listPointType: '[7]'
                        };
                    },
                    processResults: function (data) {
                        return {
                            results:
                                $.map(data.result, function (item) {
                                    return {
                                        text: "<h6 style='margin: 0'>" + item.pointName + ".</h6>" + "<small>Địa chỉ: " + item.address + "</small>",
                                        id: item.pointId,
                                        address: item.address,
                                        pointName: item.pointName
                                    }
                                })
                        };
                    }
                }
            });

            $('#pointStrar,#pointEnd,#point,.selectTransshipment,#start_transshipment,#end_transshipment').on("select2:select", function (e) {
                var data = e.params.data;
                if ($(this).is('#pointStrar')) {
                    listPoint.pointStrar = data;
                }
                if ($(this).is('#pointEnd')) {
                    listPoint.pointEnd = data;
                }
                if ($(this).is('#point')) {
                    currentPoint = data;
                }
                if ($(this).is('.selectTransshipment') || $(this).is('#start_transshipment') || $(this).is('#end_transshipment')) {
                    listTransshipment = data.id;
                }
                if ($(this).is('#start_transshipment')) {
                    start_point = data.id;
                }
                if ($(this).is('#end_transshipment')) {
                    end_point = data.id;
                }
            });

            $('#btnUpdateRoute').click(function () {
                if (listPoint.pointStrar.pointName != undefined && listPoint.pointEnd.pointName != undefined) {
                    listPoint.pointStrar.TimeIntend = 0;
                    listPoint.pointEnd.TimeIntend = converMilisec($('#hourTimeIntendOfRoute').val(), $('#minuteTimeIntendOfRoute').val());
                    listPoint.pointStrar.mealprice = 0;
                    listPoint.pointEnd.mealprice = $('#txtPriceMealOfRoute').val() != '' ? $('#txtPriceMealOfRoute').val() : -1;
                    listPoint.pointStrar.pickUpHome = 0;
                    listPoint.pointEnd.pickUpHome = $('#txtPickUpHomeOfRoute').val() != '' ? $('#txtPickUpHomeOfRoute').val() : -1;
                    if (listPoint.pointEnd.TimeIntend > 0 /*&& $('#cbbListVehicleType').val() != ''*/ && $('#routeName').val() != '' && parseInt($('#childrenTicketRatio').val()) > 0) {
                        // $("#modal_Route").modal("hide");
                        hiddenModal(document.getElementById('modal_Route'));
                        $('#lb_pointStrarName').text(listPoint.pointStrar.pointName);
                        $('#lb_pointStrarAddress').text(listPoint.pointStrar.address);
                        $('#lb_pointEndName').text(listPoint.pointEnd.pointName);
                        $('#lb_pointEndAddress').text(listPoint.pointEnd.address);
                        $('#start_point').val(start_point);
                        $('#end_point').val(end_point);
                        if ($('#start_transshipment option').length > 0)
                            $('#start_transshipment').find('option').get(0).remove();
                        if ($('#end_transshipment option').length > 0)
                            $('#end_transshipment').find('option').get(0).remove();
                        var first = $('#start_transshipment').html();
                        var last = $('#end_transshipment').html();
                        $('tr.first td select').html('');
                        $('tr.last td select').html('');
                        $('tr.first td select').html(first);
                        $('tr.last td select').html(last);
                    } else {
                        listError = '';
                        if ($('#routeName').val() == '') {
                            listError += '<li>Tên tuyến</li>';
                        }
                        if (listPoint.pointEnd.TimeIntend <= 0) {
                            listError += '<li>Thời gian dự kiến</li>';
                        }
                        /*if ($('#cbbListVehicleType').val() == '') {
                            listError += '<li>Loại xe</li>';
                        }*/
                        if (parseInt($('#childrenTicketRatio').val()) <= 0) {
                            listError += '<li>Tỷ lệ vé trẻ em</li>';
                        }
                        Message('Cảnh báo', 'Vui lòng nhập đầy đủ thông tin tuyến <br> <ul>' + listError + '</ul>', '');
                    }
                }

            });

            $('#btnEditPoint').click(function () {

                var id = $('#editPointId').val();
                var check = true;
                var listpoint = document.getElementsByClassName('pointStop');
                $.each(listpoint, function (k, v) {
                    if (currentPoint.id == $(v).attr('id') && $(v).attr('id') != id) check = false;
                });
                if ($('select#point').val() == listPoint.pointEnd.pointId || $('select#point').val() == listPoint.pointStrar.pointId) check = false;
                if (check) {
                    $('#select_transshipment').find('option').get(0).remove();
                    var html_tran = $('#select_transshipment').html();
                    currentPoint.pickUpHome = $('#txtPickUpHomeOfPoint').val() != '' ? $('#txtPickUpHomeOfPoint').val() : -1;
                    currentPoint.mealprice = $('#txtPriceMealOfPoint').val() != '' ? $('#txtPriceMealOfPoint').val() : -1;
                    currentPoint.TimeIntend = converMilisec($('#hourTimeIntendOfPoint').val(), $('#minuteTimeIntendOfPoint').val());
                    if (currentPoint.TimeIntend < listPoint.pointEnd.TimeIntend) {
                        if (currentPoint.pointName != undefined) {
                            $('tr#' + id + ' .lbPointName').text(currentPoint.pointName);
                            $('tr#' + id + ' .lbPointAddress').text(currentPoint.address);
                            $('tr#' + id + ' td select').html('');
                            $('tr#' + id + ' td select').html(html_tran);

                            $("#modal_Point").modal("hide");
                            var index = listPoint.point.findIndex(function (item) {
                                return item.id == id
                            });
                            listPoint.point[index] = currentPoint;
                            $('tr#' + id).prop('id', currentPoint.id);

                        }

                    } else {
                        Message('Cảnh báo', 'Thời gian dự kiến của bến đỗ phải nhỏ hơn thời gian dự kiến của tuyến', '');
                    }
                } else {
                    Message('Cảnh báo', 'Điểm dừng không được trùng nhau', '');
                }
            });
            $('#modal_Point').on('hidden.bs.modal', function () {
                listTransshipment = '';
                $('#modal_Route').hide();
            });

            $('#btnAddPoint').click(function () {
                var check = true;
                var listpoint = document.getElementsByClassName('pointStop');
                $.each(listpoint, function (k, v) {
                    if ($(v).attr('id') == $('select#point').val()) check = false;
                });
                if ($('select#point').val() == listPoint.pointEnd.pointId || $('select#point').val() == listPoint.pointStrar.pointId) check = false;
                if (check) {
                    var html_tran = $('#select_transshipment').html();
                    currentPoint.pickUpHome = $('#txtPickUpHomeOfPoint').val() != '' ? $('#txtPickUpHomeOfPoint').val() : -1;
                    currentPoint.mealprice = $('#txtPriceMealOfPoint').val() != '' ? $('#txtPriceMealOfPoint').val() : -1;
                    currentPoint.TimeIntend = converMilisec($('#hourTimeIntendOfPoint').val(), $('#minuteTimeIntendOfPoint').val());

                    var TimeIntendOfLastPoint = getListPointOfRoute()[getListPointOfRoute().length - 2].TimeIntend;
                    if ($('select#point').val() != '') {
                        if (currentPoint.TimeIntend < listPoint.pointEnd.TimeIntend &&
                            currentPoint.TimeIntend > TimeIntendOfLastPoint) {
                            if (currentPoint.pointName != undefined) {
                                listPoint.point.push(currentPoint);
                                html = '<tr class="pointStop" id="' + currentPoint.id + '" draggable="true" ondragstart="drag(event,this)" ondrop="drop(event)" ondragover="allowDrop(event)"><td><div></div></td>' +

                                    '<td class="lbPointName">' + currentPoint.pointName + '<select class="pointedit" style="display: none">' + html_tran + '</select></td>' +
                                    '<td class="lbPointAddress">' + currentPoint.address + '</td>' +
                                    '<td><a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>' + '<ul class="onclick-menu-content">' +
                                    '<li><button type="button"  onclick="editPoint(this)">Sửa bến đỗ</button>' +
                                    '</li><li><button type="button" onclick="deletePoint(this)">Xóa</button></li>' +
                                    '</ul></a></td>' +
                                    '</tr>';
                                content[content.length - 1] = html;
                                content[content.length] = '<tr class="addPoint">' + add[0].innerHTML + '</tr>' + '<tr class="last">' + last[0].innerHTML + '</tr>';

                                $('.tool').before(html);
                            }

                            hiddenModal(document.getElementById('modal_Point'));
                        }
                        else {
                            Message('Cảnh báo', 'Thời gian dự kiến phải nhỏ hơn thời gian dự kiến của tuyến và lớn hơn thời gian dự kiến của bến trước đó', '');
                        }
                    } else {
                        Message('Cảnh báo', 'Chưa chọn điểm dừng', '');

                    }
                } else {
                    Message('Cảnh báo', 'Điểm dừng không được trùng nhau', '');
                }
            });

            $('#btn_updatePrice').click(function () {
                var price = $('#txtPrice').val();
                if ($.isNumeric(price) && parseInt(price) > 0) {
                    $(currentElement).removeAttr('class').addClass('price dacogia')
                        .data('price', price)
                        .children('span').text(moneyFormat(price));

                    $('#modal_updatePrice').modal('hide');
                    $(".dacogia").removeClass("display");
                    $("#index" + indexChecked + "").addClass("display");

                } else {
                    Message('Cảnh báo', 'Vui lòng nhập giá!', '');
                }

            });

            $('body').on('click', 'button.btn_noSell', function () {
                $(this).parents('td').removeAttr('class').addClass('price khongban')
                    .data('price', -1)
                    .children('span').text('Không bán');
            });

            $('body').on('click', 'button.btn_updatePrice', function () {

                currentElement = $(this).parents('td');
                indexPrice = currentElement.data('index');
                $("#displayPriceChk").prop('checked', false);
                if (indexChecked == indexPrice) {
                    $("#displayPriceChk").prop('checked', true);
                }

                var price = $(currentElement).data('price') > 0 ? $(currentElement).data('price') : 0;
                $('#txtPointStart').val($(currentElement).data('pointstart'));
                $('#txtPointEnd').val($(currentElement).data('pointend'));
                $('#txtPrice').val(price);
            });

            $('#cbPriceMealOfPoint,#cbPickUpHomeOfPoint,#cbPickUpHomeOfRoute,#cbPriceMealOfRoute').change(function () {
                $(this).nextAll('input').prop('readonly', !this.checked).val('');
            });
            $('.btnSubmit').click(function (e) {
                e.preventDefault();
                console.log((getListTransshipment()));
                if ($('.dacogia').length != 0) {
                    $('#listPoint').val(JSON.stringify(getListPointOfRoute()));
                    $('#listPrice').val(JSON.stringify(getListPrice()));
                    $('#listTransshipment').val(JSON.stringify(getListTransshipment()));
                    var value = $(this).val();
                    if(value==='save'){
                        $('#frmAdd').attr('action',urlUpdateRoute);
                    }else{
                        $('#frmAdd').attr('action',urlAddRoute);
                    }
                    $('#frmAdd').submit();
                } else {
                    Message('Cảnh báo', 'Vui lòng nhập bảng giá', '');
                }

            });
        });

        function hiddenModal(that) {
            $(that).removeClass('in');
            $(that).attr('aria-hidden', 'true');
            that.style.display = 'none';
            $(that).modal({backdrop: "false"});
            $(that).modal('hide');
        }

        function showModal(that) {
            // alert('hiện'+that);
            $(that).addClass('in');
            $(that).attr('aria-hidden', 'false');
            that.style.display = 'block';
        }

        function editPoint(that) {
            var id = $(that).parents('tr').prop('id');
            var select = $.trim($(that).parents('tr').find('select').html());
            $('#select_transshipment').html('');
            $('#select_transshipment').html('<option>Xem danh sách<option>' + select);
            $('#select_transshipment').find('option').get(1).remove();
            if ($('#select_transshipment option').length > 1)
                $('#select_transshipment').show();
            else $('#select_transshipment').hide();
            $('#modal_Point').modal('show');
            $('#title_bendo').text('CẬP NHẬT BẾN ĐỖ');
            $('#btnAddPoint').hide();
            $('#btnEditPoint').show();
            $('#editPointId').val(id);
            var pointEditing = $(listPoint.point).filter(function (index, item) {
                return item.id == id;
            })[0];
            currentPoint = pointEditing;
            $('#point').html('').html('<option selected value="' + id + '">' + pointEditing.pointName + '</option>');
            var date = new Date(pointEditing.TimeIntend);
            var minutes = date.getUTCMinutes();
            var hours = Math.floor(pointEditing.TimeIntend/3600000);

            $('#hourTimeIntendOfPoint').val(hours);
            $('#minuteTimeIntendOfPoint').val(minutes);

            $('#cbPriceMealOfPoint,#cbPickUpHomeOfPoint').prop('checked', false).nextAll('input').prop('readonly', true).val('');
            if (pointEditing.mealprice > 0)
                $('#cbPriceMealOfPoint').prop('checked', true).nextAll('input').prop('readonly', false).val(pointEditing.mealprice);
            if (pointEditing.pickUpHome > 0)
                $('#cbPickUpHomeOfPoint').prop('checked', true).nextAll('input').prop('readonly', false).val(pointEditing.pickUpHome);

        }

        function addPoint() {
            // showModal(document.getElementById('modal_Point'));
            $('#modal_Point').modal('show');
            $('#title_bendo').text('THÊM BẾN ĐỖ');
            $('#point').html('').html('<option value="">Chọn bến đến</option>');
            $('#select_transshipment').html('');
            $('#select_transshipment').hide();
            $('#btnAddPoint').show();
            $('#btnEditPoint').hide();
            $('#hourTimeIntendOfPoint,#minuteTimeIntendOfPoint').val(0);
            $('#cbPriceMealOfPoint,#cbPickUpHomeOfPoint').prop('checked', false).nextAll('input').prop('readonly', true).val('');
        }

        function deletePoint(that) {
            var id = $(that).parents('tr').prop('id');
            $('tr#' + id).remove();
            listPoint.point = listPoint.point.filter(function (item) {
                return item.id != id
            })
        }

        function generateTablePrice() {
            var listPointOfRoute = getListPointOfRoute();
            // if(route.listPoint.length!=(listPointOfRoute.length-2) &&route.listPoint[route.listPoint.length-2].pointId !=listPointOfRoute[route.listPoint.length-2].id)
            if (route.listPoint.length != listPointOfRoute.length)
                for (var t = 0; t < listPrice.length; t++)
                    listPrice[t] = -1;
            for (var i = 0; i < (listPointOfRoute.length * listPointOfRoute.length); i++) {
                if (parseInt(sessionStorage.getItem('index' + i)) > 0)
                    sessionStorage.setItem('index' + i, '0');
            }
            var widthOfTd = 100 / (listPointOfRoute.length + 1) + '%';
            var html = '';
            var intdexOfListPrice = 0;
            for (row = 0; row <= listPointOfRoute.length; row++) {
                var cell ='';
                for (column = 0; column <= listPointOfRoute.length; column++) {
                    if (row == 0 && column == 0) {
                        cell += '<th style="width:' + widthOfTd + '">Bến</th>';
                    } else {
                        if (row == 0 || column == 0) {
                            if (row == 0) {
                                cell += '<th style="width:' + widthOfTd + '">' + listPointOfRoute[column - 1].pointName + '</th>';
                            }
                            if (column == 0) {
                                cell += '<td style="width:' + widthOfTd + '">' + listPointOfRoute[row - 1].pointName + '</td>';
                            }
                        } else {
                            if ((column - row) <= 0) {
                                cell += '<td data-price="-1" class="price giarong" style="width:' + widthOfTd + '"></td>';
                            } else {
                                var price = listPrice[intdexOfListPrice] || 0;
                                sessionStorage.setItem('index' + intdexOfListPrice, price);
                                var type = ' class="price chuacogia"><input style="width: 100%;background-color:#ffc985;border: none;"type="button" value="Chưa bán"/>';
                                // if (price < 0) type = ' class="price khongban"><span>Không bán</span>';
                                if (price >= 0) type = ' class="price dacogia"><input style="width: 100%;background-color:white;border: none;" type="button" value="' + moneyFormat(price) + '"/>';

                                cell += '<td style="width:' + widthOfTd + '" data-index="' + intdexOfListPrice + '" id="index' + intdexOfListPrice + '" data-price="' + price + '"' +
                                    ' data-pointStart="' + listPointOfRoute[row - 1].pointName + '"' +
                                    ' data-pointEnd="' + listPointOfRoute[column - 1].pointName + '" ' + type+'</td>';
                            }
                            intdexOfListPrice++;
                        }

                    }
                }
                html += '<tr>' + cell + '</tr>';
            }
            $('.giatuyen').html(html);
            $("#index" + indexChecked + "").addClass("display");
        }

        function getListTransshipment() {
            var dem = 0, check = false;
            var transshipment = {
                    pointId: '',
                    pointName: '',
                    index: 0,
                    transshipmentPrice: ''
                }, listTransshipment = [],
                listTransshipmentPoint = [];
            $('select#firstpoint option').each(function () {
                check = true;
                transshipment.pointId = $(this).data('id');
                transshipment.pointName = $(this).data('name') ? $.trim($(this).data('name')) : '';
                transshipment.transshipmentPrice = $(this).data('price') != '' ? $(this).data('price') : '0';
                listTransshipment.push(JSON.parse(JSON.stringify(transshipment)));
            });
            if (listTransshipment.length == 0) {
                transshipment.transshipmentPrice = 0;
                listTransshipment.push(JSON.parse(JSON.stringify(transshipment)));
            }
            transshipment.pointId = '';
            transshipment.pointName = '';
            transshipment.transshipmentPrice = 0;
            listTransshipmentPoint.push(JSON.parse(JSON.stringify(listTransshipment)));
            console.log(listTransshipmentPoint);
            listTransshipment = [];
            dem++;
            $('select.pointedit').each(function () {
                var listop = this.querySelectorAll('option');
                $.each(listop, function () {
                    check = true;
                    transshipment.pointId = $(this).data('id');
                    transshipment.pointName = $(this).data('name') ? $.trim($(this).data('name')) : '';
                    transshipment.index = dem;
                    transshipment.transshipmentPrice = $(this).data('price') != '' ? $(this).data('price') : '0';
                    listTransshipment.push(JSON.parse(JSON.stringify(transshipment)));
                });
                if (listTransshipment.length == 0) {
                    transshipment.transshipmentPrice = 0;
                    transshipment.index = dem;
                    dem++;
                    listTransshipment.push(JSON.parse(JSON.stringify(transshipment)));
                } else dem++;
                transshipment.pointId = '';
                transshipment.pointName = '';
                transshipment.transshipmentPrice = 0;
                listTransshipmentPoint.push(JSON.parse(JSON.stringify(listTransshipment)));
                listTransshipment = [];
            });
            console.log(listTransshipmentPoint);
            $('select#lastpoint option').each(function () {
                check = true;
                transshipment.pointId = $(this).data('id');
                transshipment.pointName = $(this).data('name') ? $.trim($(this).data('name')) : '';
                transshipment.index = dem;
                transshipment.transshipmentPrice = $(this).data('price') != '' ? $(this).data('price') : '0';
                listTransshipment.push(JSON.parse(JSON.stringify(transshipment)));
            });
            if (listTransshipment.length == 0) {
                transshipment.transshipmentPrice = 0;
                transshipment.index = ++dem;
                listTransshipment.push(JSON.parse(JSON.stringify(transshipment)));
            }
            transshipment.pointId = '';
            transshipment.pointName = '';
            transshipment.transshipmentPrice = 0;
            listTransshipmentPoint.push(JSON.parse(JSON.stringify(listTransshipment)));
            console.log(listTransshipmentPoint);
            if (check)
                return listTransshipmentPoint;
            else return 0;

        }

        function getListPointOfRoute() {
            var listPointOfRoute = listPoint.point.slice();
            listPointOfRoute.unshift(listPoint.pointStrar);
            listPointOfRoute.push(listPoint.pointEnd);
            $(listPointOfRoute).each(function (index, item) {
                item.text = '';
            });
            return listPointOfRoute;
        }

        function getListPrice() {
            var listPrice = [];
            var list = (document.getElementsByClassName('price'));
            for (var i = 0; i < list.length; i++) {
                if (parseInt(sessionStorage.getItem('index' + i)) >= -1) {
                    var p = parseInt(sessionStorage.getItem('index' + i)) < 0 ? -1 : parseInt(sessionStorage.getItem('index' + i));
                    console.log(0,p,i);
                    listPrice.push(p);
                    sessionStorage.setItem('index' + i, '-1');
                }
                else{
                    console.log(1,p,i);
                    var p = parseInt($(list[i]).data('price')) < 0 ? -1 : parseInt($(list[i]).data('price'));
                    listPrice.push(p);
                }
            }

            console.log(listPrice);
            return listPrice;
        }

        function buttom() {
            return '<a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu pull-right"><i></i>\n' +
                '                                        <ul class="onclick-menu-content">\n' +
                '                                            <li>\n' +
                '                                                <button type="button" class="btn_updatePrice" data-toggle="modal" data-target="#modal_updatePrice">Sửa Giá</button>\n' +
                '                                            </li>\n' +
                '                                            <li>\n' +
                '                                                <button type="button" class="btn_noSell">Không bán</button>\n' +
                '                                            </li>\n' +
                '                                        </ul>\n' +
                '                                    </a>';
        }

        function converMilisec(hour, minute) {
            hour = $.isNumeric(hour) ? hour : 0;
            minute = $.isNumeric(minute) ? minute : 0;
            return (parseInt(hour * 60) + parseInt(minute)) * 60 * 1000;
        }

        function checkDisplayPrice() {
            var currentInput = $("#displayPrice").val();
            if (currentInput !== displayPrice) {
                displayPrice = 0;
                indexChecked = null;
                $(".dacogia").removeClass("display");
            }
        }

        /*Upload ảnh cho tuyến*/
        $("#selectedFile").change(function () {
            UploadImage('frmAdd', 'imgRoute', 'listImage');
        });

        function UploadImage(formId, imgId, inputId) {
            var form = new FormData($("#" + formId)[0]);
            $('#' + imgId).attr('src', '/public/images/loading/loader_blue.gif');
            $.ajax({
                type: 'POST',
                url: '/cpanel/system/upload-image',
                data: form,
                dataType: 'json',
                processData: false,
                contentType: false,
                cache: false
            }).done(function (data) {
                $('#' + imgId).attr('src', data.url);
                $('#' + inputId).val(data.url);
            });
        }
    </script>
@endsection

