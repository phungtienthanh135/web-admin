@extends('cpanel.template.layout')
@section('title', 'Danh mục khoản mục')

@section('content')

    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Danh mục khoản mục</h3></div>
            </div>
        </div>

        <div class="innerLR">
            <form action="">
                <div class="row-fluid">
                <div class="control-box">
                    <h4>Tìm khoản mục</h4>
                    <div class="row-fluid">
                        <form action="">
                            <div class="span3">
                                <label for="cbb_Loai">Loại khoản mục</label>
                                <select class="w_full" name="itemType" id="cbb_Loai">
                                    <option value="1">Tài sản</option>
                                    <option value="2">Nguồn vốn</option>
                                    <option value="3">Lưỡng tính</option>
                                </select>
                            </div>
                            <div class="span2">
                                <label for="txtMaKhoanMuc">Mã khoản mục</label>
                                <input class="w_full" type="text" name="itemId" id="txtMaKhoanMuc" value="{{ request('itemId') }}">
                            </div>
                            <div class="span3">
                                <label for="txtTenKhoanMuc">Tên khoản mục</label>
                                <input class="w_full" type="text" name="itemName" id="txtTenKhoanMuc" value="{{ request('itemName') }}">  
                            </div>
                            <div class="span2">
                                <label class="separator hidden-phone" for="add"></label>
                                <button class="btn btn-info btn-flat-full hidden-phone" id="search">TÌM KHOẢN MỤC
                                </button>
                                <button class="btn btn-info btn-flat visible-phone" id="search">TÌM KHOẢN MỤC</button>
                            </div>
                            <div class="span2">
                                <label class="separator hidden-phone" for="add"></label>
                                <button data-toggle="modal" data-target="#add_categogy"
                                        class="btn btn-warning btn-flat-full hidden-phone" id="add">THÊM KHOẢN MỤC
                                </button>
                                <button data-toggle="modal" data-target="#add_categogy"
                                        class="btn btn-warning btn-flat visible-phone" id="add">THÊM KHOẢN MỤC
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            </form>
            
            <div class="row-fluid">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        {{--<th>Mã khoản mục</th>--}}
                        <th>Tên khoản mục</th>
                        <th>Loại khoản mục</th>
                        <th>Loại</th>
                        <th class="center">Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($result)==0)
                        <tr>
                            <td class="center" colspan="4">Hiện không có dữ liệu</td>
                        </tr>
                    @else
                        @foreach($result as $row)
                            <tr>
                                {{--<td>{{$row['itemId']}}</td>--}}
                                <td>{{$row['itemName']}}</td>
                                <td>@checkItem($row['itemType'])</td>
                                <td>{{$row['itemReceiptPayment']==1?'Thu' :$row['itemReceiptPayment']==2?"Chi": "Thu và chi" }}</td>
                                <td class="center">
                                    <a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                        <ul class="onclick-menu-content">
                                            <li>
                                                <button data-toggle="modal" data-target="#moadel_edit"
                                                        onclick="editItem('{{$row['itemName']}}','{{$row['itemType']}}','{{$row['itemId']}}')">
                                                    Sửa
                                                </button>
                                            </li>
                                            <li>
                                                <button data-toggle="modal" data-target="#modal_delete"
                                                        onclick="deleteItem('{{$row['itemId']}}')">Xóa
                                                </button>
                                            </li>
                                        </ul>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                @include('cpanel.template.pagination-without-number',['page'=>$page])
            </div>
            <div class="separator bottom"></div>
            <div class="row-fluid" style="display: none;" id="info">

            </div>

            <div class="clearfix"></div>
            <div class="modal modal-custom hide fade" id="add_categogy">
                <div class="modal-header center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>THÊM KHOẢN MỤC</h3>
                </div>
                <form action="{{action('ItemController@postAdd',[],true)}}" method="post" class="form-horizontal">
                    <div style="max-height: 500px;" class="modal-body">
                        {{--<div class="control-group">
                            <label class="control-label" for="txtMa">Mã khoản mục</label>
                            <div class="controls">
                                <input value="23456" class="span4" type="text" name="" id="txtMa">
                            </div>
                        </div>--}}
                        <div class="row-fluid">
                            <div class="control-group">
                                <label class="control-label" for="txtLoai">Loại khoản mục</label>
                                <div class="controls">
                                    <select class="span12" name="itemType" id="txtLoai">
                                        <option value="1">Tài sản</option>
                                        <option value="2">Nguồn vốn</option>
                                        <option value="3">Lưỡng tính</option>
                                    </select>
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label" for="txtTen">Tên khoản mục</label>
                                <div class="controls">
                                    <input class="span12" type="text" name="itemName" id="txtTen">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="itemReceiptPayment">Thu-Chi</label>
                                <div class="controls">
                                    <select class="span12" name="itemReceiptPayment" id="itemReceiptPayment">
                                        {{--<option value="3">Thu và Chi</option>--}}
                                        <option value="1">Thu</option>
                                        <option value="2">Chi</option>
                                    </select>
                                </div>
                            </div>
                        </div>



                        {{--<div class="control-group">
                            <label class="control-label" for="note">Ghi Chú</label>
                            <div class="controls">
                                <textarea class="span4" id="note" rows="2">phát lương cho nhân viên</textarea>
                            </div>
                        </div>--}}

                        <div class="control-group">

                            <div class="controls">
                                <div class="row-fluid">
                                    <div class="span9">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <button class="btn btn-warning btn-flat-full">THÊM MỚI</button>
                                    </div>
                                    <div class="span3">
                                        <a data-dismiss="modal"
                                           class="btn btn-default btn-flat no_border bg_light">HUỶ</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal hide fade" id="modal_delete" style="width: 250px; margin-left: -10%;margin-top: 10%;">
        <div class="modal-body center">

            <p>Bạn có chắc muốn xoá?</p>
        </div>
        <form action="{{action('ItemController@delete')}}" method="post">
            <div class="modal-footer">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="itemId" value="" id="id_delete">
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
            </div>
        </form>
    </div>


    <div class="modal modal-custom hide fade" id="moadel_edit">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>SỬA KHOẢN MỤC</h3>
        </div>
        <form action="{{action('ItemController@postEdit',[],true)}}" method="post" class="form-horizontal">
            <div style="max-height: 500px;" class="modal-body">
                <div class="row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtLoai_edit">Loại khoản mục</label>
                        <div class="controls">
                            <select class="span12" name="itemType" id="txtLoai_edit">
                                <option value="1">Tài sản</option>
                                <option value="2">Nguồn vốn</option>
                                <option value="3">Lưỡng tính</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="txtTen_edit">Tên khoản mục</label>
                        <div class="controls">
                            <input class="span12" type="text" name="itemName" id="txtTen_edit">
                        </div>
                    </div>
                </div>


                <div class="control-group">
                    <div class="controls">
                        <div class="row-fluid">
                            <div class="span9">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="itemId" id="id_edit">
                                <button class="btn btn-warning btn-flat-full">CẬP NHẬT</button>
                            </div>
                            <div class="span3">
                                <a data-dismiss="modal"
                                   class="btn btn-default btn-flat no_border bg_light">HUỶ</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </form>
    </div>
    <script type="text/javascript">
        function editItem(name, type, Id) {
            $('#id_edit').val(Id);
            $('#txtTen_edit').val(name);
            $("#txtLoai_edit option[value=" + type + "]").attr('selected', 'selected');
        }

        function deleteItem(Id) {
            $('#id_delete').val(Id);
        }

    </script>
    <!-- End Content -->

@endsection