<div class="modal modal-custom hide fade" id="add_notification">
    <div class="modal-header center">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>THÊM GIAO DỊCH</h3>
    </div>
    <form class="form-horizontal" action="{{action('BalanceSheetController@add')}}" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <div style="max-height: 500px;" class="modal-body">
            <div class="row-fluid">
                <div class="control-group"><label class="control-label" for="txtType">(<span class="required">*</span>) Bắt buộc</label></div>
                <div class="control-group">
                    <label class="control-label" for="txtType">Loại phiếu</label>
                    <div class="controls">
                        <select class="span12" name="type" id="txtType">
                            <option value="2">Phiếu chi</option>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="txtThoiGian">Thời gian</label>
                    <div class="controls">
                        <input autocomplete="off" class="span10" id="txtThoiGian" type="text"
                               name="date" readonly placeholder="Bấm chọn ngày">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="txtReceiver">Người chi</label>
                    <div class="controls">
                        <input autocomplete="off" class="span12" type="text" name="receiver" id="txtReceiver">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="moneyReceiverId">Nhân viên</label>
                    <div class="controls">
                        <select name="moneyReceiverId" class="span12" id="moneyReceiverId">
                            @foreach($nhanvien as $nv)
                                <option value="{{$nv['userId']}}">{{$nv['fullName']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="cbbApproverId">Người phê duyệt</label>
                    <div class="controls">
                        <select name="approverId" class="span12" id="cbbApproverId">
                            @foreach($nhanvien as $nv)
                                <option value="{{$nv['userId']}}">{{$nv['fullName']}}</option>
                            @endforeach
                        </select>
                        <!-- <input value="Dương Văn Đông" class="span4" type="text" name="" id="txtNguoiNop"> -->
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="cbbItemId">Khoản mục</label>
                    <div class="controls">
                        <select name="itemId" class="span12" id="cbbItemId">
                            @foreach($listItem as $item)
                                <option value="{{$item['itemId']}}">{{$item['itemName']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="txtSoTien">Số tiền(<span class="required">*</span>)</label>
                    <div class="controls">
                        <div class="row-fluid">
                            <div class="form-inline">
                                <input autocomplete="off" required value="" placeholder="0" class="span9"
                                       type="text" name="amountSpent" id="txtSoTien">

                                <select class="span3 pull-right" name="" id="">
                                    <option value="">VNĐ</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="txtReason">Nội dung</label>
                    <div class="controls">
                        <textarea class="span12" id="txtReason" rows="4" name="reason" required></textarea>
                    </div>
                </div>
            </div>


            <div class="control-group">

                <div class="controls">
                    <div class="row-fluid">
                        <div class="span9">
                            <button class="btn btn-warning btn-flat-full">THÊM MỚI</button>
                        </div>
                        <div class="span3">
                            <a data-dismiss="modal" class="btn btn-default btn-flat no_border bg_light">HUỶ</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal-footer">

        </div>
    </form>
</div>