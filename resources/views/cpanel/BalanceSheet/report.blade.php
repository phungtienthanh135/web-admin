@extends('cpanel.template.layout')
@section('title', 'Báo cáo tổng hợp thu chi')
@section('content')

    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Báo cáo tổng hợp thu chi</h3>
                </div>
                <div class="pull-right span4 t_align_r">
                    <div class="row-fluid">
                        <div class="input-append">
                            <button class="btn btn-default btn-flat btn-report"><i class="iconic-chart"></i> Biểu đồ
                            </button>
                            <button class="btn btn-default btn-flat btn-report"><i class="icon-list"></i> Chi tiết
                            </button>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="input-append">
                            <button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i
                                        class="icon-print"></i> In
                            </button>
                            <div class="btn-group export-btn">
                                <button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất file <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="excel" data-fileName="bao_cao_thu_chi">Excel</a></li>
                                    <li><a id="pdf" data-fileName="bao_cao_thu_chi">PDF</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="innerLR">
            <div class="row-fluid">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Tìm giao dịch</h4>
                    </div>
                    <form action="">
                    <div class="row-fluid">

                        <div class="span3">
                            <label for="txtStartDate">Từ ngày</label>
                            <div class="input-append">
                                <input autocomplete="off" value="{{request('startDate',date('d/m/Y'))}}"
                                       id="txtStartDate" name="startDate" type="text" readonly>
                            </div>
                        </div>

                        <div class="span3">
                            <label for="txtEndDate">Đến ngày</label>
                            <div class="input-append">
                                <input autocomplete="off" value="{{request('endDate',date('d/m/Y'))}}" id="txtEndDate" name="endDate" type="text" readonly>
                            </div>
                        </div>
                        <div class="span3">
                            <label class="separator hidden-phone" for="add"></label>
                            <button class="btn btn-info btn-flat-full hidden-phone" id="search">TÌM GIAO DỊCH</button>
                            <button class="btn btn-info btn-flat visible-phone" id="search">TÌM GIAO DỊCH</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <div id="tb_report" class="row-fluid">
                <table class="table table-thuchi table-striped">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Nội dung thu chi</th>
                        <th>Trong Ngày</th>
                        <th>Luỹ kế</th>
                        <th>Tỷ lệ/Tổng</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>A</th>
                        <th colspan="4">PHẦN THU</th>
                    </tr>
                    @php($i =1)
                    @foreach($in as $row)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$row['reason']}}</td>
                        <td>@dateFormat($row['date']/1000)</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                    @endforeach
                    <tr>
                        <th>B</th>
                        <th colspan="4">PHẦN CHI</th>
                    </tr>
                    @php($j =1)
                    @foreach($exp as $row)
                        <tr>
                            <td>{{$j++}}</td>
                            <td>{{$row['reason']}}</td>
                            <td>@dateFormat($row['date']/1000)</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                    @endforeach
                    <tr>
                        <th>C</th>
                        <th colspan="4">QUỸ TIỀN HIỆN CÓ</th>
                    </tr>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <!-- Table Export To File -->
    <script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>

    <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>
    <script type="text/javascript">

        function printDiv(divName) {
            // var printContents = document.getElementById(divName).innerHTML;
            // var originalContents = document.body.innerHTML;
            // document.body.innerHTML = printContents;
            // window.print();
            // document.body.innerHTML = originalContents;
            w=window.open();
            w.document.write('<html><head><meta charset="utf-8">');

            w.document.write('<style>.tttk th { width: 30% }</style>');
            w.document.write('</head><body>' + document.getElementById(divName).innerHTML + '</body></html>');
            w.print();
            // w.close();
        }
    </script>
    <!-- End Content -->

@endsection