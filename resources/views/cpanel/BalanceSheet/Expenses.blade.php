@extends('cpanel.template.layout')
@section('title', 'Phiếu chi')

@section('content')

    <style>
        table.table > tbody > tr:not(.balance-info) {
            cursor: pointer;
        }

        #ui-datepicker-div {
            z-index: 10002;
        }
    </style>

    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Phiếu chi</h3></div>
            </div>
        </div>


        <div class="innerLR">
            <div class="row-fluid">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Tìm giao dịch</h4>
                    </div>
                    <form action="">
                        <div class="row-fluid">
                            <div class="span6">
                                <label for="cbb_KhoanMuc">Khoản mục</label>
                                <select class="w_full" name="itemId" id="cbb_KhoanMuc">
                                    <option value="">Chọn khoản mục</option>
                                    @foreach($listItem as $item)
                                        <option {{request('itemId')==$item['itemId']?'selected':''}}  value="{{$item['itemId']}}">{{$item['itemName']}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="span3">
                                <label class="separator hidden-phone" for="add"></label>
                                <button class="btn btn-info btn-flat-full" id="search">TÌM GIAO DỊCH
                                </button>
                            </div>
                        </div>
                        <div class="row-fluid">

                            <div class="span3">
                                <label for="txtStartDate">Từ ngày</label>
                                <div class="input-append">
                                    <input autocomplete="off" value="{{request('startDate',date('d-m-Y'))}}"
                                           id="txtStartDate" name="startDate" type="text" readonly>
                                </div>
                            </div>

                            <div class="span3">
                                <label for="txtEndDate">Đến ngày</label>
                                <div class="input-append">
                                    <input autocomplete="off" value="{{request('endDate',date('d-m-Y'))}}"
                                           id="txtEndDate" name="endDate" type="text" readonly>
                                </div>
                            </div>
                            @if(hasAnyRole(CREATE_RECEIPT_AND_PAYMENT))
                                <div class="span3">
                                    <label class="separator hidden-phone" for="add"></label>
                                    <button data-toggle="modal" data-target="#add_notification"
                                            class="btn btn-warning btn-flat-full" id="add">THÊM GIAO DỊCH
                                    </button>
                                </div>
                            @endif
                        </div>
                    </form>

                </div>
            </div>
            <div class="row-fluid">
                Danh sách phiếu chi
                <table class="table table-striped table-vertical-center">
                    <thead>
                    <tr>
                        <th>Thời gian</th>
                        <th>Số phiếu</th>
                        <th>Người nhận</th>
                        <th>Số tiền</th>
                        <th>Lý do</th>
                        <th>Khoản mục</th>
                        <th>Người chi</th>
                        <th>Tuỳ chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($result as $row)
                        <tr id="{{$row['receiptAndPaymentId']}}">
                            <td>@dateFormat($row['date'])</td>
                            <td>{{$row['numberOfDocuments']}}</td>
                            <td>{{$row['moneyReceiverId']}}</td>
                            <td>@moneyFormat($row['amountSpent'])</td>
                            <td>{{$row['reason']}}</td>
                            <td>{{@$row['item']['itemName']}}</td>
                            <td>{{$row['receiver']}}</td>
                            <td class="center">
                                <a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                    <ul class="onclick-menu-content">
                                        @if(hasAnyRole(UPDATE_RECEIPT_AND_PAYMENT))
                                            <li>
                                                <button>Sửa</button>
                                            </li>
                                        @endif
                                        @if(hasAnyRole(DELETE_RECEIPT_AND_PAYMENT))
                                            <li>
                                                <button data-toggle="modal" data-target="#modal_delete"
                                                        value="{{$row['receiptAndPaymentId']}}" class="btn_delete">Xóa
                                                </button>
                                            </li>
                                        @endif
                                    </ul>
                                </a>
                            </td>
                        </tr>
                        <tr style="display: none" class="info">
                            <td colspan="8">
                                <div id="tb_PhieuThu">
                                    <div class="span6">
                                        <table class="tttk table table-vertical-center" cellspacing="20"
                                               cellpadding="4">
                                            <tbody>
                                            <tr>
                                                <th>LOẠI PHIẾU</th>
                                                <td>@balance($row['type'])</td>
                                            </tr>
                                            <tr>
                                                <th>SỐ PHIẾU</th>
                                                <td>{{$row['numberOfDocuments']}}</td>
                                            </tr>
                                            <tr>
                                                <th>THỜI GIAN</th>
                                                <td>@dateFormat($row['date'])</td>
                                            </tr>
                                            <tr>
                                                <th>NGƯỜI NỘP</th>
                                                <td>{{$row['receiver']}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="span6">
                                        <table class="tttk table table-vertical-center" cellspacing="20"
                                               cellpadding="4">
                                            <tbody>
                                            <tr>
                                                <th>SỐ TIỀN</th>
                                                <td>@moneyFormat($row['amountSpent'])</td>
                                            </tr>
                                            <tr>
                                                <th>KHOẢN MỤC</th>
                                                <td>{{@$row['item']['itemName']}}</td>
                                            </tr>
                                            <tr>
                                                <th rowspan="2">LÝ DO</th>
                                                <td rowspan="2">{{$row['reason']}}</td>
                                            </tr>
                                            <!-- <tr>
                                                <td></td>
                                                <td></td>
                                            </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <button style="margin-top: 10px" onclick="printDiv('tb_PhieuThu')"
                                        class="btn btn-large btn-default btn-flat pull-right"><i class="icon-print"></i>
                                    In Phiếu
                                </button>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7" class="center">Hiện không có dữ liệu</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                @include('cpanel.template.pagination-without-number',['page'=>$page])
            </div>
        </div>
    </div>
    @if(hasAnyRole(CREATE_RECEIPT_AND_PAYMENT))
        @includeWhen(empty($tripId),'cpanel.BalanceSheet.expensesOne', [
            'listItem' => $listItem,
            'nhanvien' => $nhanvien,
        ]);
        @includeWhen(!empty($tripId),'cpanel.BalanceSheet.expensesMultil', [
            'tripId' => $tripId,
            'listItem' => $listItem,
            'nhanvien' => $nhanvien,
        ]);
    @endif
    @if(hasAnyRole(DELETE_RECEIPT_AND_PAYMENT))
    <div class="modal hide fade" id="modal_delete" style="width: 250px; margin-left: -10%;margin-top: 10%;">
        <div class="modal-body center">
            <p>Bạn có chắc muốn xoá?</p>
        </div>
        <form action="{{action('BalanceSheetController@delete')}}" method="post">
            <div class="modal-footer">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="receiptAndPaymentId" value="" id="delete_sheet">
                <a style="cursor: pointer" data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn-warning btn-flat">ĐỒNG Ý</button>
            </div>
        </form>
    </div>
    @endif
    <script type="text/javascript">
        $('#moneyReceiverId').select2({
            width: '395px',
            dropdownCss: {'z-index': '999999'}
        });
        $('#txtThoiGian').datepicker({
            //comment the beforeShow handler if you want to see the ugly overlay
            beforeShow: function () {
                setTimeout(function () {
                    $('.ui-datepicker').css('z-index', 99999999999999);
                }, 0);
            }
        });
        $('.btn_delete').click(function () {
            $('#delete_sheet').val($(this).val());
        });

        function printDiv(divName) {
            // var printContents = document.getElementById(divName).innerHTML;
            // var originalContents = document.body.innerHTML;
            // document.body.innerHTML = printContents;
            // window.print();
            // document.body.innerHTML = originalContents;
            w = window.open();
            w.document.write('<html><head><meta charset="utf-8">');

            w.document.write('<style>.tttk th { width: 30% }</style>');
            w.document.write('</head><body>' + document.getElementById(divName).innerHTML + '</body></html>');
            w.print();
            // w.close();
        }
    </script>
    <!-- End Content -->

@endsection