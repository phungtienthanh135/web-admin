@extends('cpanel.template.layout')
@section('title', 'Thông tin tài khoản')

@section('content')

    <div id="content">
        <div class="separator bottom"></div>
        <div class="heading-buttons">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Lịch sử người dùng</h3></div>
            </div>
            <hr class="gachnho"/>
        </div>
        <div class="bg_light p_10">
            <div class="widget-body">
                <div class="span12">
                    Tìm kiếm từ ngày <input type="text" id="txtFromDate" readonly> trở về trước.
                </div>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Thời gian</th>
                            <th>Hành động</th>
                            <th>Thông tin vé</th>
                        </tr>
                    </thead>
                    <tbody id="listHistory">
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" style="text-align: center"><button class="btn btn-default" onclick="return getDataNew();">Tải lại <i class="fa fa-history"></i></button> <button class="btn btn-default loadMore" onclick="return getDataWithPage();">Xem thêm <i class="fa fa-plus"></i></button></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <script>
        var page = 0;
        var stt =1;
        $(document).ready(function () {
            $('#txtFromDate').datepicker().datepicker('setDate',new Date());
            $('#txtFromTime').clockpicker({
                autoclose: true,
                afterDone: function() {
                    $('#txtFromTime').trigger("change");
                },
                'default':'23:59'
            });
            $('body').on('change','#txtFromDate,#txtFromTime',function(){
                getDataNew();
            });
            getDataNew();
        });
        function getDataNew() {
            page=0;
            stt=1;
            $('#listHistory').html('<tr><td colspan="4">Đang tải dữ liệu...</td></tr>');
            fromDate = $('#txtFromDate').val();
            FromDateRequest = '';
            if(fromDate!=''){
                fromDate = $.datepicker.formatDate('mm/dd/yy',$('#txtFromDate').datepicker('getDate'));
                FromDateRequest += fromDate;
            }

            dataReq = {
                userId: '{{session('userLogin')['userInfo']['userId']}}',
                page: page,
                count: 20,
            };
            if(FromDateRequest!=''){
                t = new Date(FromDateRequest).getTime();
                dataReq.timeZone=7;
                dataReq.toDate = t;
                console.log(t);
            }
            console.log(dataReq);
            disableInput();
            $.ajax({
                dataType: "json",
                url: urlDBD("web_ticket/logs/user"),
                type:"post",
                headers: {
                    'DOBODY6969': '{{ session('userLogin')['token']['tokenKey'] }}',
                },
                data: dataReq,
                success: function (data) {
                    console.log(data);
                    if(data.code===200){
                        innerHtml = generateData(data.results.listTicketLog);
                        $('#listHistory').html(innerHtml);
                    }else{
                        notyMessage("Lỗi ! Vui lòng tải lại trang", "error");
                    }
                    enableInput();
                },
                error: function () {
                    notyMessage("Lỗi ! Vui lòng tải lại trang", "error");
                    enableInput();
                }
            });
        }
        function getDataWithPage(){
            $('.loadMore').html('Đang tải dữ liệu');
            page++;
            fromDate = $('#txtFromDate').val();
            FromDateRequest = '';
            if(fromDate!=''){
                fromDate = $.datepicker.formatDate('mm/dd/yy',$('#txtFromDate').datepicker('getDate'));
                FromDateRequest += fromDate;
            }

            dataReq = {
                userId: '{{session('userLogin')['userInfo']['userId']}}',
                page: page,
                count: 20,
            };
            if(FromDateRequest!=''){
                t = new Date(FromDateRequest).getTime();
                dataReq.timeZone=7;
                dataReq.toDate = t;
                console.log(t);
            }
            console.log(dataReq);
            disableInput();
            $.ajax({
                dataType: "json",
                url: urlDBD("/web_ticket/logs/user"),
                type:"post",
                headers: {
                    'DOBODY6969': '{{ session('userLogin')['token']['tokenKey'] }}',
                },
                data: dataReq,
                success: function (data) {
                    if(data.code===200){
                        innerHtml = generateData(data.results.listTicketLog);
                        $('#listHistory').append(innerHtml);
                    }else{
                        notyMessage("Lỗi ! Vui lòng tải lại trang", "error");
                    }
                    $('.loadMore').html('Xem thêm <i class="fa fa-plus"></i>');
                    enableInput();
                },
                error: function () {
                    notyMessage("Lỗi ! Vui lòng tải lại trang", "error");
                    $('.loadMore').html('Xem thêm <i class="fa fa-plus"></i>');
                    enableInput();
                }
            });
        }
        function generateData(data) {
            html='';
            $.each(data,function(i,e){
                var addressdrop = e.ticket['dropOffAddress']||e.ticket['dropAlongTheWayAddress']||e.ticket.getOffPoint||'',
                    addresspick = e.ticket['pickUpAddress']||e.ticket['alongTheWayAddress']||e.ticket.getInPoint||'';
                time = new Date(e.ticketLog.createdDate||0);
                time = time.customFormat('#DD#-#MM#-#YYYY# lúc #hhhh#:#mm#:#ss#');
                timeTicket = new Date(e.ticket.getInTimePlan||0);
                timeTicket = timeTicket.customFormat('#DD#-#MM#-#YYYY#');
                html+='<tr>';
                html+='<td>'+stt+'</td>';
                html+='<td>'+time+'</td>';
                html+='<td>';
                html += '<h5 style="font-weight:bold;color:#bb200f;margin-bottom:0px;">'+checkHistoryType(parseInt(e.ticketLog.logType))+'</h5>';
                if (e.ticketLog.fullName != undefined) {
                    html += "- Tên khách hàng: " + e.ticketLog.fullName + "<br>";
                }
                if (e.ticketLog.paidMoney != undefined) {
                    html += "- Giá vé: " + e.ticketLog.agencyPrice + "<br>";
                }
                if (e.ticketLog.unPaidMoney != undefined) {
                    html += "- Chưa thanh toán: " + e.ticketLog.unPaidMoney + "<br>";
                }
                if (e.ticketLog.listSeatId != undefined) {
                    html += "- Danh sách ghế: " + e.ticketLog.listSeatId.toString() + "<br>";
                }
                if (e.ticketLog.phoneNumber != undefined) {
                    html += "- Số điện thoại: " + e.ticketLog.phoneNumber + "<br>";
                }
                if (e.ticketLog.email != undefined) {
                    html += "- Email: " + e.ticketLog.email + "<br>";
                }
                if (e.ticketLog.note != undefined) {
                    html += "- Ghi chú: " + e.ticketLog.note + "<br>";
                }
                if (e.ticketLog.pickUpAddress != undefined) {
                    html += "- Điểm đón: " +e.ticketLog.pickUpAddress + "<br>";
                }
                if (e.ticketLog.dropOffAddress != undefined) {
                    html += "- Điểm trả: " + e.ticketLog.dropOffAddress + "<br>";
                }
                if (e.ticketLog.routeName != undefined) {
                    html += '- Chuyến cũ : ' + e.ticketLog.routeName + '<br>';
                }
                if (e.ticketLog.srcSeatIds != undefined) {
                    html += "- Ghế cũ : " + e.ticketLog.srcSeatIds.toString() + "<br>";
                }
                if (e.ticketLog.desSeatIds != undefined) {
                    html += "- Ghế mới : " + e.ticketLog.desSeatIds.toString() + "<br>";
                }
                if (e.ticketLog.newTicketCode != undefined) {
                    html += '- Mã vé mới : ' + e.ticketLog.newTicketCode + '<br>';
                }
                html+='</td>';
                html+='<td>';
                html += '<div class="line-height18"><span class="customerName">Khách hàng :' + e.ticket['fullName'] + '</span>,<br>';
                html += '<span class="ticketPrice"><span style="color: #FF0000">'+(e.ticket['listSeatId']||'').toString()+' (' + e.ticket['listSeatId'].length + 'G)</span> <span class="moneyFormat">' + moneyFormat(e.ticket['agencyPrice']) + '</span></div>';
                html += '<div class="line-height18"><span class="sourceName">Nguồn vé : ' + (e.ticket['sourceName'] || '') + '</span> | Người bán : <span class="sellerName">' + (e.ticket['sellerName'] || '') + '/ Người thu :' + (e.ticket['cashierName'] || e.ticket['sellerName'] || '') + '</span></div>';
                html += '<div class="ticketInPoint">Lên : <i class="fa fa-arrow-alt-circle-up"></i> ' + addresspick + '</div>';
                html += '<div class="ticketOffPoint">Xuống :  ' + addressdrop + ' <i class="fa fa-arrow-alt-circle-down"></i></div>';

                html += '<div class="ticketCode">Mã vé : ' + e.ticket['ticketCode'] + '</div>';
                html+='<a style="height:50px;color :#0000CC" class="link" qs-trip-id="'+ e.ticket.tripId +'" qs-schedule-id="'+e.ticket.scheduleId+'" qs-date="'+timeTicket+'" href="/cpanel/ticket/sell-ver1?ticketId=' + e.ticket.ticketId + '&tripId=' + e.ticket.tripId + '&scheduleId=' + e.ticket.scheduleId + '&routeId=' + e.ticket.routeId + '&startPointId=' + e.ticket.getInPointId + '&endPointId=' + e.ticket.getOffPointId + '&date=' + timeTicket + '&ticketStatus='+e.ticket.ticketStatus+'" >Xem chi tiết vé >></a>';
                html+='</td>';
                html+='</tr>';
                stt++;
            });
            return html;
        }
        function disableInput() {
            $('#txtFromDate').prop("disabled",true);
            $('#txtFromTime').prop("disabled",true);
        }
        function enableInput() {
            $('#txtFromDate').prop("disabled",false);
            $('#txtFromTime').prop("disabled",false);
        }
        function checkHistoryType(type){
            str ='';
            switch(type){
                case 1 :
                    str='TẠO VÉ';
                    break;
                case 2 :
                    str = 'CẬP NHẬT THÔNG TIN';
                    break;
                case 3 :
                    str = 'THÊM GHẾ';
                    break;
                case 4 :
                    str = 'HỦY GHẾ';
                    break;

                case 5 :
                    str = 'CHUYỂN GHẾ';
                    break;

                default :
                    str = 'TẠO VÉ';
            }
            return str;
        }
    </script>
@endsection