@extends('cpanel.template.layout')
@section('title', 'Trang quản lý')

@section('content')

    <div id="content">

        <div class="separator bottom"></div>
        <div class="heading-buttons">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Trang chủ</h3></div>
                @if(hasAnyRole(SELL_TICKET))
                    <div class="panel_l pull-right span4 p_right_30 t_align_r">
                        <h3><a href="{{action('TicketController@sell')}}">Bán vé</a></h3>
                        <p>Cập nhật @dateTime(time())</p>
                    </div>
                @endif
            </div>
            <hr class="gachnho bg_blue"/>
        </div>

        <div class="row-fluid">
            {{--TODO chưa check quyền--}}
            @if(session('userLogin')['userInfo']['userType'] == 7)
                <div class="pull-left span7 section">
                    <div class="widget widget-4">
                        <div class="widget-head no_border">
                            <h4>BÁO CÁO TỔNG QUAN THÁNG {{getdate()['mon']}}</h4>
                        </div>
                        <div class="widget-body">
                            <div id="chart_lines_fill_nopoints" style="height: 250px;"></div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="pull-left span5 section">
                <div class="widget widget-4">
                    <div class="widget-body">
                        <div id="amlich-calendar"></div>
                        <div class="ht_event_calendar">
                            <div class="row-fluid">
                                <div class="span4" style="border-right: 1px solid;">
                                    <p class="fs_large">@getWeekday(time())</p>
                                    <p class="fs_large">@dateFormat(time()*1000)</p>
                                    <p id="lunarDay"> Âm Lịch</p>
                                </div>
                                <div class="span8" id="DetailOfDay">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(empty(session('userLogin')['userInfo']['listAgency']))
        <div class="row-fluid">
            <div class="span3">
                <div class="thongke">
                    <div class="bg_light relative">
                        <a class="glyphicons file m_bottom_20 m_top_10" href=""><i></i></a>
                        <div class="khungtrol">SỐ VÉ BÁN RA</div>
                        <div class="hr"></div>
                        <div class="solieu">{{@$report['totalTicket']}}</div>
                    </div>
                </div>
            </div>
            <div class="span3">
                <div class="thongke">
                    <div class="bg_light relative">
                        <a class="glyphicons usd m_bottom_20 m_top_10 money" href=""><i></i></a>
                        <div class="khungtrol">DOANH THU</div>
                        <div class="hr"></div>
                        <div class="solieu">@moneyFormat(@$report['totalRevenue'])</div>
                    </div>
                </div>
            </div>
            <div class="span3">
                <div class="thongke">
                    <div class="bg_light relative">
                        <a class="glyphicons gbp m_bottom_20 m_top_10" href=""><i></i></a>
                        <div class="khungtrol">LỢI NHUẬN</div>
                        <div class="hr"></div>
                        <div class="solieu">@moneyFormat(@$report['profit'])</div>
                    </div>
                </div>
            </div>
            <div class="span3">
                <div class="thongke">
                    <div class="bg_light relative">
                        <a class="glyphicons cars m_bottom_20 m_top_10" href=""><i></i></a>
                        <div class="khungtrol">TỔNG SỐ XE</div>
                        <div class="hr"></div>
                        <div class="solieu">{{@count($listVehicle)}}</div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        {{--TODO chưa rõ quyền--}}
        @if(hasAnyRole(GET_LIST_OUT_OF_STATION_COMMAND))
            <div class="row-fluid bg_light m_top_10">
                <div class="widget widget-4 bg_light">
                    <div class="widget-head no_border">
                        <h4>DANH SÁCH CHUYẾN XE ĐANG LƯU HÀNH</h4>
                    </div>

                    <div class="widget-body">
                        @include('cpanel.Trip.table-trip',['listTripOfTable'=>$listOnTheTrip,'type'=>1])
                    </div>
                </div>
            </div>
        @endif
        <div class="clearfix"></div>
    </div>

    <!-- End Content -->

    <script>

        // charts data
        charts_data = {
            data: {
                d1: [@foreach($report_detail as $key => $row)[{{$key}}, {{@$row['visit']}}], @endforeach ],
                d2: [@foreach($report_detail as  $key => $row)[{{$key}}, {{@$row['numberOfTicketsSold']}}], @endforeach ]
            },
            data_week: {
                data: {
                    d1: [0{{--@foreach($report_detail_week as $key => $row)[{{getdate($key)['wday']}}, {{$row['turnover']}}], @endforeach --}}],
                    d2: [0{{--@foreach($report_detail_week as  $key => $row)[{{getdate($key)['wday']}}, {{$row['profit']}}], @endforeach --}}]
                }
            }

        };
    </script>

    <!-- Charts Helper Demo Script -->
    <script src="/public/javascript/chart/charts.helper.js"></script>


    <!-- Charts Page Demo Script -->
    <script src="/public/javascript/chart/charts.js"></script>
    <!--  Flot (Charts) JS -->
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.pie.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.selection.js"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <script src="/public/theme/scripts/plugins/charts/flot/jquery.flot.orderBars.js" type="text/javascript"></script>

    <script type="text/javascript" src="/public/theme/scripts/plugins/jquery.amlich.js"></script>
    <script type="text/javascript">
        $('#lunarDay').amLich({
            type: 'date'
        });
        $('#DetailOfDay').amLich({
            type: 'DetailOfDay'
        });
        $('#amlich-calendar').amLich({
            type: 'calendar',
            tableWidth: '100%'
        });

    </script>





@endsection