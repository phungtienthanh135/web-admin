@extends('cpanel.template.layout')
@section('title', 'Kết quả tìm kiếm cho '.request('keyword'))

@section('content')
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Kết quả tìm kiếm cho "{{request('keyword')}}"</h3></div>
            </div>
        </div>
        <div class="row-fluid bg_light m_top_10">
            <div class="widget widget-4 bg_light">
                <div class="widget-body">
                    <table class="table table-hover table-vertical-center">
                        <thead>
                        <tr>
                            <th>Biển số</th>
                            <th>Số ghế</th>
                            <th>Mã vé</th>
                            <th>Họ tên</th>
                            <th>Số điện thoại</th>
                            <th>Thời gian đi</th>
                            <th>Thời gian đến</th>
                            <th>Tùy chọn</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($result as $row)
                            <tr>
                                <td>{{$row['numberPlate']}}</td>
                                <td>{{count($row['listSeatId'])}}</td>
                                <td>{{$row['ticketCode']}}</td>
                                <td>{{$row['fullName']}}</td>
                                <td>{{$row['phoneNumber']}}</td>
                                <td>@dateTime(($row['getInTimePlan']+7*60*60*1000)/1000)</td>
                                <td>@dateTime(($row['getOffTimePlan']+7*60*60*1000)/1000)</td>
                                <td>
                                    <a tabindex="0"
                                       class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                        <ul class="onclick-menu-content">
                                            <li>
                                                <button type="button" onclick="showInfo(this)">Chi tiết</button>
                                            </li>

                                            <li>
                                                <button value="{{$row['ticketId']}}" id="editTicket">Sửa vé</button>
                                            </li>

                                            <li>
                                                <button value="{{$row['ticketId']}}" id="cancelTicket">Hủy vé</button>
                                            </li>

                                            <li>
                                                <button value="{{$row['ticketId']}}" id="orderTicket">Thanh toán</button>
                                            </li>
                                        </ul>
                                    </a>
                                </td>
                            </tr>
                            <!--hiển thị chi tiết-->
                            <tr class="info" style="display: none">
                                <td colspan="8">
                                    <table class="tttk" cellspacing="20" cellpadding="4">
                                        <tbody>
                                        <tr>
                                            <td><b>TUYẾN</b></td>
                                            <td>{{$row['routeName']}}</td>
                                            <td class="right"><b>VÉ NGƯỜI LỚN</b></td>
                                            <td class="left">{{$row['numberOfAdults']}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>BẾN LÊN</b></td>
                                            <td>{{array_get($row,'pickUpAddress','Chưa xác định')}}</td>
                                            <td class="right"><b>VÉ TRẺ EM</b></td>
                                            <td class="left">{{$row['numberOfChildren']}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>BẾN XUỐNG</b></td>
                                            <td>{{array_get($row,'dropOffAddress','Chưa xác định')}}</td>
                                            <td class="right"><b>GIÁ VÉ</b></td>
                                            <td>@moneyFormat($row['paymentTicketPrice'])</td>
                                        </tr>
                                        <tr>
                                            <td><b>THỜI GIAN ĐI</b></td>
                                            <td>@dateTime(($row['getInTimePlan']+7*60*60*1000)/1000)</td>
                                            <td class="right"><b>MÃ KM</b></td>
                                            <td class="right">{{$row['promotionId']}}</td>
                                        </tr>

                                        <tr>
                                            <td><b>THỜI GIAN ĐẾN</b></td>
                                            <td>@dateTime(($row['getOffTimePlan']+7*60*60*1000)/1000)</td>
                                            <td class="right"><b>TRẠNG THÁI VÉ</b></td>
                                            <td class="right">@checkTicketStatus($row['ticketStatus'])</td>
                                        </tr>
                                        <tr>
                                            <td><b>BIỂN SỐ</b></td>
                                            <td>{{$row['numberPlate']}}</td>
                                            <td class="right"><b>ĐÃ BAO GỒM ĂN</b></td>
                                            <td class="right">{{$row['mealPrice']!=-1?$row['mealPrice']:''}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>HỌ TÊN</b></td>
                                            <td>{{$row['fullName']}}</td>
                                            <td class="right"><b>SỐ GHẾ</b></td>
                                            <td class="right">{{implode(',',$row['listSeatId'])}}</td>
                                        </tr>
                                        <tr>
                                            <td style="width:25%">&nbsp;</td>
                                            <td style="width:25%;padding-left:10px">&nbsp;</td>

                                            <td style="width:25%;text-align:right"><b>&nbsp;</b></td>
                                            <td style="width:25%;text-align:left">&nbsp;</td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                    @include('cpanel.template.pagination-without-number',['page'=>$page])
                </div>
            </div>
        </div>

        <div class="modal modal-custom hide fade" id="modal_updateTicket"
             style="width: 400px; margin-left: -10%;margin-top: 10%;">
            <form action="{{action('TicketController@updateStatusTicket')}}" method="post">
                <div class="modal-header center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 id="title-update-ticket">CẬP NHẬT THÔNG TIN VÉ</h3>
                </div>
                <div class="modal-body center">
                    <p id="lb_message"></p>
                    <div id="frmUpdateInfo" class="row-fluid">
                        <div class="form-horizontal row-fluid">
                            <div class="control-group">
                                <label class="control-label" for="txtPhoneNumber">Số điện thoại</label>
                                <div class="controls">
                                    <input autocomplete="off"
                                           data-validation="custom"
                                           data-validation-regexp="^0(([8-9][0-9]{8})|(1[0-9]{9}))$"
                                           data-validation-error-msg-custom="Vui lòng nhập đúng định dạng SĐT"
                                           class="span12" type="number" name="phoneNumber" id="txtPhoneNumber">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="txtFullName">Họ tên</label>
                                <div class="controls">
                                    <input autocomplete="off"
                                           class="span12" type="text" name="fullName" id="txtFullName">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="ticketId" value="" id="txtTicketId">
                        <input type="hidden" name="option" value="" id="txtOption">
                        <a style="cursor: pointer" data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                        <button id="btnUpdateStatusTicket" class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
                    </div>

                </div>
            </form>
        </div>
        <script>
            $('body').on('click', 'button#cancelTicket,button#orderTicket,button#editTicket', function (e) {
                if ($(e.currentTarget).is('#orderTicket')) {
                    $('#lb_message').text('Bạn có muốn thanh toán vé này?').show();
                    $('#title-update-ticket').text('THANH TOÁN VÉ');
                    $('#frmUpdateInfo').hide();
                    $('#txtOption').val(1)
                }

                if ($(e.currentTarget).is('#cancelTicket')) {
                    $('#lb_message').text('Bạn có chắc muốn huỷ vé này?').show();
                    $('#title-update-ticket').text('HUỶ VÉ');
                    $('#frmUpdateInfo').hide();
                    $('#txtOption').val(2)
                }

                if ($(e.currentTarget).is('#editTicket')) {
                    $('#lb_message').text('').hide();
                    $('#title-update-ticket').text('CẬP NHẬT THÔNG TIN VÉ');
                    $('#frmUpdateInfo').show();
                    $('#txtOption').val(3)
                }

                $('#txtTicketId').val($(this).val());
                $('#modal_updateTicket').modal('show');
            })
        </script>
    </div>


    <!-- End Content -->
@endsection