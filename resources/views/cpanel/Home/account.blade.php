@extends('cpanel.template.layout')
@section('title', 'Thông tin tài khoản')

@section('content')

    <div id="content">
        <div class="separator bottom"></div>
        <div class="heading-buttons">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Thông tin tài khoản</h3></div>
            </div>
            <hr class="gachnho"/>
        </div>
        <div class="bg_light p_10">
            <div class="widget-body">
                <form name="account_frm" id="account_frm" method="post">
                    <div class="separator bottom form-inline small">

                        <div class="margin-bottom-none">
                            <div class="row-fluid">
                                <div class="span8">
                                    <table class="tttk" cellspacing="20" cellpadding="4">
                                        <tr>
                                            <td><b>HỌ TÊN</b></td>
                                            <td>{{session('userLogin')['userInfo']['fullName']}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>NGÀY SINH</b></td>
                                            <td>@dateFormat(session('userLogin')['userInfo']['birthDay'])</td>
                                        </tr>
                                        <tr>
                                            <td><b>GIỚI TÍNH</b></td>
                                            <td>{{session('userLogin')['userInfo']['sex']}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>BẰNG LÁI XE</b></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><b>NGÀY VÀO CÔNG TY</b></td>
                                            <td>{{session('userLogin')['userInfo']['joinDate']}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>MÃ NS</b></td>
                                            <td>{{session('userLogin')['userInfo']['userId']}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>SỐ ĐIỆN THOẠI</b></td>
                                            <td>{{'+'.session('userLogin')['userInfo']['stateCode'].session('userLogin')['userInfo']['phoneNumber']}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>KHI CẦN BÁO TIN CHO AI</b></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><b>SĐT NGƯỜI CẦN BÁO TIN</b></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><b>QUÊ QUÁN</b></td>
                                            <td>{{session('userLogin')['userInfo']['district']}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>CMT</b></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><b>NGÀY HẾT HẠN</b></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><b>CHỨC VỤ</b></td>
                                            <td>@ChucVu(session('userLogin')['userInfo']['userType'])
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><b>EMAIL</b></td>
                                            <td>{{session('userLogin')['userInfo']['email']}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>TÊN ĐĂNG NHẬP</b></td>
                                            <td>{{session('userLogin')['userInfo']['userName']}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>MẬT KHẨU</b></td>
                                            <td><a href="#" class="m_left_30" data-toggle="modal" data-target="#ChangePass">Thay đổi mật khẩu</a></td>
                                        </tr>
                                    </table>

                                </div>
                                <div class="span4">
                                    <div class="avatar">
                                        <img src="{{session('userLogin')['userInfo']['avatar']}}" onerror="this.src='/public/images/logoANVUI.jpg'"/>
                                    </div>
                                    <form>
                                        <input type="file" id="selectedFile" style="display: none;"/>
                                        <input type="button" style="width:172px" value="CHỌN ẢNH" class="btncustom"
                                               onclick="document.getElementById('selectedFile').click();"/>
                                    </form>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>

                </form>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- End Content -->
    <script>
        @if(request('view')=="info")
        console.log({!! json_encode(session('userLogin')) !!});
        a = ({!! json_encode(session('listFunction')) !!});
        @endif
        @if(request('view')=="company")
        console.log({!! json_encode(session('companyInfo')) !!});
        @endif

    </script>
@endsection