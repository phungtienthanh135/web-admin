@extends('cpanel.template.layout')
@section('title', 'Chính sách')
@section('content')
    <link href="/public/bootstrap/css/bootstrap-grid.min.css" rel="stylesheet"/>
    <style>
        .padding-all-10px{
            padding:10px;
        }
        .lds-roller div {
            animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            transform-origin: 32px 32px;
        }
        .lds-roller div:after {
            content: " ";
            display: block;
            position: absolute;
            width: 6px;
            height: 6px;
            border-radius: 50%;
            background: #000;
            margin: -3px 0 0 -3px;
        }
        .lds-roller div:nth-child(1) {
            animation-delay: -0.036s;
        }
        .lds-roller div:nth-child(1):after {
            top: 50px;
            left: 50px;
        }
        .lds-roller div:nth-child(2) {
            animation-delay: -0.072s;
        }
        .lds-roller div:nth-child(2):after {
            top: 54px;
            left: 45px;
        }
        .lds-roller div:nth-child(3) {
            animation-delay: -0.108s;
        }
        .lds-roller div:nth-child(3):after {
            top: 57px;
            left: 39px;
        }
        .lds-roller div:nth-child(4) {
            animation-delay: -0.144s;
        }
        .lds-roller div:nth-child(4):after {
            top: 58px;
            left: 32px;
        }
        .lds-roller div:nth-child(5) {
            animation-delay: -0.18s;
        }
        .lds-roller div:nth-child(5):after {
            top: 57px;
            left: 25px;
        }
        .lds-roller div:nth-child(6) {
            animation-delay: -0.216s;
        }
        .lds-roller div:nth-child(6):after {
            top: 54px;
            left: 19px;
        }
        .lds-roller div:nth-child(7) {
            animation-delay: -0.252s;
        }
        .lds-roller div:nth-child(7):after {
            top: 50px;
            left: 14px;
        }
        .lds-roller div:nth-child(8) {
            animation-delay: -0.288s;
        }
        .lds-roller div:nth-child(8):after {
            top: 45px;
            left: 10px;
        }
        @keyframes lds-roller {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        #txtStartDate + button{
            width: 20%;
        }
        #txtEndDate + button{
            width: 20%;
        }

    </style>
    <div id="content" style="padding-bottom: 20px">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3 id="headerName">Chính sách</h3>
                </div>
                {{--<div class="pull-right span4 t_align_r">--}}
                {{--<div class="row-fluid">--}}
                {{--<div class="input-append">--}}
                {{--<button onclick="showinfo(this,'chart')"--}}
                {{--class="btn btn-default btn-flat btn-report"><i--}}
                {{--class="iconic-chart"></i> Biểu đồ--}}
                {{--</button>--}}
                {{--<button onclick="showinfo(this,'table')"--}}
                {{--class="btn-activate btn btn-default btn-flat btn-report"><i--}}
                {{--class="icon-list-alt"></i> Chi tiết--}}
                {{--</button>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row-fluid">--}}
                {{--<div class="input-append">--}}
                {{--<button onclick="printDiv('tb_report')" class="btn btn-default btn-flat btn-report"><i--}}
                {{--class="icon-print"></i> In--}}
                {{--</button>--}}
                {{--<div class="btn-group export-btn">--}}
                {{--<button id="" class="btn btn-report btn-default btn-flat" data-toggle="dropdown">Xuất--}}
                {{--file <span class="caret"></span></button>--}}
                {{--<ul class="dropdown-menu">--}}
                {{--<li><a id="excel" data-fileName="bao_cao">Excel</a></li>--}}
                {{--<li><a id="pdf" data-fileName="bao_cao">PDF</a></li>--}}
                {{--</ul>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>


        <div data-tabs="list">
            {{--<div class="row filterDetail">--}}
                {{--<div class="col-md-4"></div>--}}
            {{--</div>--}}
            <div class="row-fluid filterDetail">
                <div class="control-box" style="padding: 10px; margin-bottom: 20px;margin-top:0">
                    <div class="row-fluid">
                        <div style="margin-left: 5px;" class="col-md-12">
                            <div class="row-fluid">
                                <div class="col-md-4">
                                    <label for="searchPolicyName">Tên chính sách</label>
                                    <input autocomplete="off" id="searchPolicyName" type="text" class="" style="width: -webkit-fill-available"
                                           name="searchPolicyName" placeholder="Tìm chính sách">
                                </div>
                                <div class="col-md-3">
                                    <label for="">&nbsp;</label>
                                    <button class="btn btn-info btn-flat-full searchPolicy">TÌM</button>
                                </div>
                                <div class="col-md-3 p_right_15">
                                    <label for="">&nbsp;</label>
                                    <button class="btn btn-warning btn-flat-full btnAddPolicy" data-tabs-show="add">TẠO CHÍNH SÁCH</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <table class="table table-bordered listPolicy">
                    <thead>
                        <tr>
                            <td>STT</td>
                            <td>Tên Chương trình</td>
                            <td>Ngày tạo</td>
                            <td>Ngày bắt đầu</td>
                            <td>Ngày kết thúc</td>
                            <td>Giá trị</td>
                            <td>tùy chọn</td>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <div data-tabs="add" style="display: none">
            <div class="row-fluid">
                <div class="padding-all-10px">
                    <div class="col-md-3 col-xs-12">
                        <label for="txtPolicyName">Tên chính sách</label>
                        <input type="text" class="w_full" required placeholder="Tên chính sách" id="txtPolicyName" style="margin: 0">
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-3 col-md-6">
                        <label for="txtPolicyDateType">Kiểu Áp Dụng</label>
                        <select name="" id="txtPolicyDateType" class="w_full">
                            <option value="2">Ngày Đi</option>
                            <option value="1">Ngày Tạo Vé</option>
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <label for="txtStartDate">Từ ngày</label>
                        <div class="input-append" style="width: 100%">
                            <input type="text" id="txtStartTime" value="00:00" style="width: 30%" readonly>
                            <input autocomplete="off" value="{{request('startDate',date('d-m-Y',time()))}}"
                                   id="txtStartDate" name="startDate" type="text" readonly style="width:50%;">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <label for="txtEndDate">Đến ngày</label>
                        <div class="input-append" style="width: 100%">
                            <input type="text" id="txtEndTime" value="23:59" style="width: 30%" readonly>
                            <input autocomplete="off" value="{{request('endDate',date('d-m-Y'))}}"
                                   id="txtEndDate" name="endDate" type="text" readonly style="width: 50%">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-3">
                        <label for="txtPolicyAmount">Áp dụng cho loại <span style="color: red">( * )</span></label>
                        <select name="" id="listUserType" multiple>
                            <option value="2">Khách lẻ</option>
                            <option value="4">Đại lý</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="txtPolicyAmount">Giá trị</label>
                        <div class="input-append has-success" style="width:100%">
                            <select id="txtMode" class="btn" style="width:20%">
                                <option value="1">+</option>
                                <option value="-1">-</option>
                            </select>
                            <input value="" id="txtPolicyAmount" style="width:45%" name="price" type="number" min="1" placeholder="Giá trị" data-validation="number" data-validation-allowing="range[1;9007199254740992]" data-validation-optional="true" class="valid">
                            <select id="txtPolicyType" class="btn" style="width:35%">
                                <option value="1">VNĐ</option>
                                <option value="2">%</option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12">
                        <input type="checkbox" id="isAppliedDisplayPriceOfRoute" value="true">
                        <label for="isAppliedDisplayPriceOfRoute">Đồng giá cho tất cả các chặng</label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12">
                        <label for="">Áp dụng vào</label>
                        <div class="col-sm-1">
                            <input type="checkbox" id="t2" value="2" class="day" title="thứ 2">
                            <label for="t2">T2</label>
                        </div>
                        <div class="col-sm-1">
                            <input type="checkbox" id="t3" value="3" class="day" title="thứ 3">
                            <label for="t3">T3</label>
                        </div>
                        <div class="col-sm-1">
                            <input type="checkbox" id="t4" value="4" class="day" title="thứ 4">
                            <label for="t4">T4</label>
                        </div>
                        <div class="col-sm-1">
                            <input type="checkbox" id="t5" value="5" class="day" title="thứ 5">
                            <label for="t5">T5</label>
                        </div>
                        <div class="col-sm-1">
                            <input type="checkbox" id="t6" value="6" class="day" title="thứ 6">
                            <label for="t6">T6</label>
                        </div>
                        <div class="col-sm-1">
                            <input type="checkbox" id="t7" value="7" class="day" title="thứ 7">
                            <label for="t7">T7</label>
                        </div>
                        <div class="col-sm-1">
                            <input type="checkbox" id="t1" value="1" class="day" title="Chủ nhật">
                            <label for="t1">CN</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12">
                        <table class="table table-bordered listRouteAndSchedule">
                            <thead>
                                <tr>
                                    <td>Tuyến</td>
                                    <td style="width: 80%">Lịch</td>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <div style="display: none">
                        <input type="hidden" id="txtPolicyId">
                        <input type="hidden" id="txtPolicyAction" value="add">
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-2">
                        <button class="btn btn-success w_full doAddPolicy">Lưu</button>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-warning w_full" data-tabs-show="list">Quay Lại</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        listRoute = [];
        listPolicy = [];
        policySelected = {};
        $(document).ready(function () {
            init();
            $('body').on('click','[data-tabs-show]',function () {
                var show = $(this).attr('data-tabs-show');
                $('[data-tabs]').hide();
                $("[data-tabs='"+show+"']").show();
                switch (show){
                    case 'add' :
                        resetFormAdd();
                        break;
                    default : break;
                }
                return false;
            });
            $('body').on('change','.route',function () {
                var val = $(this).prop('checked');
                if(val){
                    $.each($(this).parents('tr').find('.schedule'),function(i,e){
                        $(e).prop('checked',true);
                    });
                }else{
                    $.each($(this).parents('tr').find('.schedule'),function(i,e){
                        $(e).prop('checked',false);
                    });
                }
                return false;
            });
            $('body').on('change','.schedule',function () {
                var flag = false;
                $.each($(this).parents('td').find('.schedule'),function(i,e){
                    if($(e).prop('checked')){
                        flag = true;
                        return false;
                    };
                });
                if(flag){
                    $(this).parents('tr').find('.route').prop('checked',true);
                }else{
                    $(this).parents('tr').find('.route').prop('checked',false);
                }
                return false;
            });

            $('body').on('click','.doAddPolicy',function () {
                var name = $('#txtPolicyName').val(),
                    dateType= $('#txtPolicyDateType').val(),
                    startDate = dateToMiliS($('#txtStartDate').val(),$('#txtStartTime').val()),
                    endDate = dateToMiliS($('#txtEndDate').val(),$('#txtEndTime').val()),
                    type = $('#txtPolicyType').val(),
                    amount = $('#txtPolicyAmount').val(),
                    listUserType = $('#listUserType').val(),
                    action = $('#txtPolicyAction').val(),
                    id = $('#txtPolicyId').val();
                    listRouteRequest = [],
                    listScheduleRequest = [],
                    dateOfWeek =[],
                    isAppliedDisplayPriceOfRoute = $('#isAppliedDisplayPriceOfRoute').prop('checked')===true ?true:false;
                    mode = $('#txtMode').val();
                if(type==2){
                    amount =amount/100;
                }
                $.each($('.route'),function (i,e) {
                    if($(e).prop('checked')){
                        listRouteRequest.push($(e).val());
                    }
                });

                $.each($('.schedule'),function (i,e) {
                    if($(e).prop('checked')){
                        listScheduleRequest.push($(e).val());
                    }
                });

                $.each($('.day'),function (i,e) {
                    if($(e).prop('checked')){
                        dateOfWeek.push($(e).val());
                    }
                });
                if(name==''){
                    notyMessage('Không được bỏ trống tên','error');
                    $('#txtPolicyName').focus();
                    return false;
                }
                if(startDate>endDate){
                    if($('#txtStartDate').val()===$('#txtEndDate').val()){
                        notyMessage('Thời gian bắt đầu và thời gian kết thúc không hợp lệ','error');
                        $('#txtEndTime').focus();
                    }else{
                        notyMessage('Ngày bắt đầu và ngày kết thúc không hợp lệ','error');
                        $('#txtEndDate').focus();
                    }
                    return false;
                }
                if(amount==''){
                    notyMessage('Giá trị bỏ trống tên','error');
                    $('#txtPolicyAmount').focus();
                    return false;
                }
                if(listUserType==null){
                    notyMessage('Loại hình áp dụng bỏ trống tên','error');
                    $('#listUserType').select2("open");
                    return false;
                }
                dataSend = {
                    name :name,
                    dateType : dateType,
                    startDate : startDate,
                    endDate : endDate,
                    type :type,
                    amount :amount,
                    listUserType :JSON.stringify(listUserType),
                    listRouteId : JSON.stringify(listRouteRequest),
                    listScheduleId : JSON.stringify(listScheduleRequest),
                    dateOfWeeks  : JSON.stringify(dateOfWeek),
                    mode :mode,
                    isAppliedDisplayPriceOfRoute : isAppliedDisplayPriceOfRoute,
                };
                url = '';
                if(action === "add"){
                    url = urlDBD('addition_price/create');
                }else{
                    url = urlDBD('addition_price/update');
                    dataSend.id = id;
                }
                SendAjaxWithJson({
                    url : url,
                    type : 'post',
                    data : dataSend,
                    success :function (data) {
                        notyMessage("Thực thi thành công","success");
                        getListPolicy();
                        $('[data-tabs-show="list"]').click();
                    }
                });
                return false;
            });

            $('body').on('click','.btnDeletePolicy',function(){
                if(confirm("Bạn có chắc chắn muốn xóa!")){
                    id = $(this).attr('data-id');
                    SendAjaxWithJson({
                        url : urlDBD('addition_price/delete'),
                        type : 'post',
                        data : {
                            id:id
                        },
                        success :function (data){
                            getListPolicy();
                        }
                    });
                }
            });

            $('body').on('click','.btnUpdatePolicy',function(){
//                lấy id của thằng muốn sửa
                id = $(this).attr('data-id');
//                chuyển sang form add
                $('[data-tabs-show="add"]').click();
//                lấy thông tin chi tiết của thằng đó
                policySelected = $.grep(listPolicy,function(v,k){
                    return v.id === id;
                })[0];
                //check lại lịch
                if(typeof policySelected.listScheduleId!=="undefined"){
                    $.each(policySelected.listScheduleId,function (i,e) {
                        $('#schedule'+e).prop('checked',true);
                    });
                }
//                check lại tuyến
                if(typeof policySelected.listRouteId!=="undefined"){
                    $.each(policySelected.listRouteId,function (i,e) {
                        $('#route'+e).prop('checked',true);
                    });
                }
//                cập nhật lại các thứ trong tuần,bỏ check hết rồi check lại
                $('.day').prop('checked',false);
                if(typeof policySelected.dateOfWeeks!=="undefined"){
                    $.each(policySelected.dateOfWeeks,function (i,e) {
                        $('#t'+e).prop('checked',true);
                    });
                }
//                chuyển hành động thành sửa
                $('#txtPolicyAction').val("update");
                $('#txtPolicyId').val(id);
                $('#txtPolicyName').val(policySelected.name);
                $('#txtPolicyType').val(policySelected.type);
                if(typeof policySelected.isAppliedDisplayPriceOfRoute!=='undefined' && policySelected.isAppliedDisplayPriceOfRoute===true){
                    $('#isAppliedDisplayPriceOfRoute').prop('checked', true);
                }else{
                    $('#isAppliedDisplayPriceOfRoute').prop('checked', false);
                }
//                nếu type bằng 1 là kiểu giá tri,2 là phần trăm
                if(policySelected.type==1){
                    $('#txtPolicyAmount').val(policySelected.amount);
                }else{
                    $('#txtPolicyAmount').val(policySelected.amount*100);
                }
                $('#txtStartDate').val(new Date(policySelected.startDate+policySelected.startTime).customFormat('#DD#-#MM#-#YYYY#'));
                $('#txtEndDate').val(new Date(policySelected.endDate+policySelected.endTime).customFormat('#DD#-#MM#-#YYYY#'));
                $('#txtStartTime').val(new Date(policySelected.startDate+policySelected.startTime).customFormat('#hhhh#:#mm#'));
                $('#txtEndTime').val(new Date(policySelected.endDate+policySelected.endTime).customFormat('#hhhh#:#mm#'));
                $('#txtMode').val(policySelected.mode||1);
                $('#listUserType').val(policySelected.listUserType).change();
            });
        });

        function init() {
            $('#listUserType').select2({
                width : '100%'
            });
            $('#txtStartTime,#txtEndTime').clockpicker({
                autoclose : true
            })
            getListRoute();
            getListPolicy();
        }

        function getListRoute() {
            $('.btnAddPolicy').prop('disabled',true);
            SendAjaxWithJson({
                url : urlDBD('route/get-all'),
                type : 'post',
                data : {
                    page : 0,
                    count:1000,
                    companyId : '{{ session('companyId') }}',
                },
                success :function (data){
                    $('.btnAddPolicy').prop('disabled',false);
                    console.log(data);
                    if(data.code===200){
                        listRoute = data.results.result;
                        $('.listRouteAndSchedule tbody').html(buildHtmlRoute());
                    }
                    else{
                        notyMessage("Lấy danh sách tuyến thất bại,vui lòng tải lại trang ","error")
                    }
                }
            });
        }
        
        function buildHtmlRoute() {
            currentTime = new Date().getTime();
            html = '';
            $.each(listRoute,function (k,v) {
                html+='<tr>';
                html+='<td>';
                html+='<div><input type="checkbox" class="route" id="route'+v.routeId+'" value="'+v.routeId+'"><label for="route'+v.routeId+'">'+v.routeName+'</label></div>';
                html+='</td>';
                html+='<td>';
                html+='<div class="row">';
                $.each(v.listSchedule,function (i,e) {
                    if(e.endDate>currentTime){
                        html+='<div class="col-md-2 col-sm-3 col-xs-6"><input type="checkbox"  class="schedule" id="schedule'+e.scheduleId+'" value="'+e.scheduleId+'"><label for="schedule'+e.scheduleId+'">'+new Date(e.startTime).customFormat("#hhhh#:#mm#")+'</label></div>';
                    }
                });
                html+='</div>';
                html+='</td>';
                html+='</tr>';
            });
            return html;
        }

        function dateToMiliS(date,time) {
            date = date.split("-");
            var newDate = date[1] + "/" + date[0] + "/" + date[2]+" "+time+":00";
            datem = new Date(newDate).getTime();
            return datem;
        }

        function resetFormAdd() {
            $('#txtPolicyName').val(''),
            $('#txtPolicyAmount').val(''),
            $('#listUserType').val([]).change(),
            $('.day').prop('checked',true);
            $('.route').prop('checked',false);
            $('.schedule').prop('checked',false);
            $('#txtPolicyAction').val("add");
            $('#txtPolicyId').val('');
            $('#txtStartDate').val(new Date().customFormat('#DD#-#MM#-#YYYY#'));
            $('#txtEndDate').val(new Date().customFormat('#DD#-#MM#-#YYYY#'));
            $('#txtStartTime').val("00:00");
            $('#txtEndTime').val("23:59");
            $('#txtMode').val(1);
        }

        function buildHtmlListPolicy() {
            html = '';
            $.each(listPolicy,function (k,v) {
                html+='<tr>';
                html+='<td>';
                html+=parseInt(k+1);
                html+='</td>';
                html+='<td>';
                html+=v.name||'';
                html+='</td>';
                html+='<td>';
                html+=new Date(v.createdDate).customFormat('#hhhh#:#mm# #DD#-#MM#-#YYYY#');
                html+='</td>';
                html+='<td>';
                html+=new Date(v.startDate+v.startTime).customFormat('#hhhh#:#mm# #DD#-#MM#-#YYYY#');
                html+='</td>';
                html+='<td>';
                html+=new Date(v.endDate+v.endTime).customFormat('#hhhh#:#mm# #DD#-#MM#-#YYYY#');
                html+='</td>';
                html+='<td>';
                html+=getValueWithType(v.type,v.amount);
                html+='</td>';
                html+='<td>';
                html+='<button class="btn btn-warning btnUpdatePolicy" data-id="'+v.id+'">Sửa</button> ';
                html+='<button class="btn btn-danger btnDeletePolicy" data-id="'+v.id+'">Xóa</button>';
                html+= '</td>';
                html+='</tr>';
            });
            return html;
        }

        function getListPolicy() {
            $('.listPolicy tbody').html('<tr><td colspan="6">Đang tải dữ liệu</td></tr>');
            SendAjaxWithJson({
                url : urlDBD('addition_price/get-list'),
                type : 'post',
                success :function (data){
                    console.log(data);
                    if(data.code===200){
                        listPolicy = data.results.listAdditionPrices;
                        $('.listPolicy tbody').html(buildHtmlListPolicy());
                    }
                    else{
                        notyMessage("Lấy danh sách chương trình thất bại,vui lòng tải lại trang ","error")
                    }
                },
                error :function (data) {
                    console.log(data);
                    notyMessage("Gửi yêu cầu lấy danh sách tuyến thất bại","error")
                }
            });
        }

        function getValueWithType(type,value) {
            if(type==2){
                return value*100 +" %";
            }
            else{
                return $.number(value) + ' VND';
            }
        }

    </script>
@endsection