<h5>Trạng thái vận đơn</h5>
<ul class="timeline">
    <li class="li {{$package['goodsStatus']>=1?'complete':''}}">
        <div class="status">
            <strong> NHẬN HÀNG</strong>
        </div>
        <div class="timestamp">
            <span class="date">@if(!empty($package['timeline'][0]))@dateFormat($package['timeline'][0])@endif&emsp;</span>
            <span class="time">@if(!empty($package['timeline'][0]))@timeFormat($package['timeline'][0])@endif&emsp;</span>
        </div>
    </li>
    <li class="li {{$package['goodsStatus']>=2?'complete':''}}">
        <div class="status">
            <strong> GOM HÀNG </strong>
        </div>
        <div class="timestamp">
            <span class="date">@if(!empty($package['timeline'][1]))@dateFormat($package['timeline'][1])@endif&emsp;</span>
            <span class="time">@if(!empty($package['timeline'][1]))@timeFormat($package['timeline'][1])@endif&emsp;</span>
        </div>
    </li>
    <li class="li {{$package['goodsStatus']>=3?'complete':''}}">
        <div class="status">
            <strong>VẬN CHUYỂN </strong>
        </div>
        <div class="timestamp">
            <span class="date">@if(!empty($package['timeline'][2]))@dateFormat($package['timeline'][2])@endif&emsp;</span>
            <span class="time">@if(!empty($package['timeline'][2]))@timeFormat($package['timeline'][2])@endif&emsp;</span>
        </div>
    </li>
    <li class="li {{$package['goodsStatus']>=4?'complete':''}}">
        <div class="status">
            <strong> XUỐNG XE </strong>
        </div>
        <div class="timestamp">
            <span class="date">@if(!empty($package['timeline'][3]))@dateFormat($package['timeline'][3])@endif&emsp;</span>
            <span class="time">@if(!empty($package['timeline'][3]))@timeFormat($package['timeline'][3])@endif&emsp;</span>
        </div>
    </li>
    <li class="li {{$package['goodsStatus']>=5?'complete':''}}">
        <div class="status">
            <strong> TRẢ HÀNG </strong>
        </div>
        <div class="timestamp">
            <span class="date">@if(!empty($package['timeline'][4]))@dateFormat($package['timeline'][4])@endif&emsp;</span>
            <span class="time">@if(!empty($package['timeline'][4]))@timeFormat($package['timeline'][4])@endif&emsp;</span>
        </div>
    </li>
    <li style="margin: 0 0 0 300px;">
        <span>Người tạo : <strong>{{isset($package['createdUserName']) && trim($package['createdUserName'],'')!=''?$package['createdUserName']:''}}</strong></span>
        <hr>
        <span>Người sửa : <strong>{{isset($package['updateUserName']) && trim($package['updateUserName'],'')!=''?$package['updateUserName']:''}}</strong></span>
        <hr>
        <span>Lên xe    : <strong>{{@$package['tripNumberPlate']}}</strong></span>
    </li>
</ul>
