@extends('cpanel.template.layout')
@section('title','Gửi Đồ')
@section('content')
    <style>
        @media print {
            div.print_tem .barCode, div.print_tem_new .barCode {
                /*zoom: 40%;*/
                transform: scaleX(0.6) scaleY(0.6);
            }

            @page {
                /*size: portrait;*/
                margin: 3mm 0mm 0mm 3mm;
            }
        }

        .select2-container--default {
            margin-left: 3px !important;
        }

        legend {
            width: 100px;
            font-size: 19px;
            font-family: serif;
            font-style: italic;
            text-align: center;
        }

        #print3 td, #print3 th {
            border: 1px solid black !important;
        }

        .modal table td, .modal table th {
            border: 1px solid;
        }

        #editNew input, #createNew input {
            width: 90%;
        }

        #createNew h3, #editNew h3 {
            width: 100px;
            float: left;
            margin-left: 42%;
        }

        #editNew select, #createNew select {
            width: 100% !important;
        }

        #editNew .select2, #createNew .select2 {
            width: 100% !important;
        }

        #editNew label.control-label, #createNew label.control-label {
            font-weight: 600;
        }

        .sign1 {
            border-radius: 5px;
            border: 2px solid;
            width: 60px;
            float: left;
            margin-left: 20%;
        }

        .sign2 {
            border-radius: 5px;
            border: 2px solid;
            width: 60px;
            float: left;
        }

        .sign3 {
            border-radius: 5px;
            border: 2px solid;
            width: 60px;
            float: left;
        }

        .divSender {
            /*background-image: url('/public/images/good/senderGood.jpg');*/
            background-size: 400px 200px;
            min-height: 150px !important;
            border: 2px solid;
            border-radius: 10px;
        }

        .divReciever {
            /*background-image: url('/public/images/good/reciever_good.jpg');*/
            background-size: 400px 200px;
            min-height: 150px !important;
            border: 2px solid;
            border-radius: 10px;
        }

        .divGood {
            margin-top: -12px;
            background-size: 800px 400px;
            border: 2px solid;
            border-radius: 10px;
        }

        #contentCreateNew, #bodyEditNew {
            overflow-x: hidden;
            max-height: 500px;
        }

        #title_print_package {
            width: 100%;
            text-align: center;
            margin: 10px 0px 10px 0px;
            font-size: 20px;
            font-weight: 600
        }

        .frm_chonanh {
            margin-top: 12px;
            margin-left: 0%;
        }

        .print_tem td, .print_tem_new td {
            line-height: 3mm;
        }

        .height_tr_a5 {
            line-height: 10mm;
        }

        .height_tr_a6 {
            line-height: 6mm;
        }

        .height_tr_aDHSL {
            line-height: 4mm;
        }

        .title_company {
            width: 39%;
            float: left;
            margin-top: 5mm;
            font-weight: 600;
            font-size: 22px;
            padding-left: 3%;
        }

        .note_company {
            width: 100%;
            font-size: 10.5px;
            line-height: 4mm !important;
        }

        .divLeft {
            width: 50%;
            float: left;
        }

        .divLeft2 {
            width: 35%;
            float: left;
        }

        .divRight {
            width: 49%;
            float: left;
        }

        #titleDate {
            float: left;
            padding-top: 14px;
            padding-left: 8px;
            color: #1f498d
        }

        #js-step-1 th.headerSortDown, #step-search th.headerSortDown, #step4 th.headerSortDown, #step5 th.headerSortDown {
            background-image: url('/public/images/sort/desc.gif');
            background-repeat: no-repeat;
            background-position: center right;
            background-color: #1a5da0;
        }

        #js-step-1 th.headerSortUp, #step-search th.headerSortUp, #step4 th.headerSortUp, #step5 th.headerSortUp {
            background-image: url('/public/images/sort/asc.gif');
            background-repeat: no-repeat;
            background-position: center right;
            background-color: #1a5da0;
        }

        #imgGood {
            transition: transform .2s; /* Animation */
        }

        .chonanh {
            height: 25px;
            width: 50px;
            border: 1px dashed #999;
            overflow: hidden;
            margin-top: -10px;
        }

        #listCodeUp, #listCodePackage, #listUpToTrip, #listCodeDown, #listCodeShip {
            height: 150px;
            overflow-y: inherit;
            overflow-x: hidden;
        }

        .formhtml .print-box table tr td {
            line-height: 3mm !important;
            padding-left: 1mm !important;
        }

        .print_biennhan_new table tr td {
            line-height: 5mm !important;
            padding-left: 1mm !important;
        }

        .print_biennhan td{line-height: 22px}

        .width_listCodeUp {
            width: 100px;
        }

        .print-box {
            border: 1px solid #000000;
        }

        .select2-dropdown {
            z-index: 99999;
        }

        ul.row-merge li.active a, .tabsbar ul li:hover a {
            background: #0077E2 !important;
            color: rgba(25, 7, 7, 0) !important;
        }

        .select2-dropdown.increasezindex {
            z-index: 99999;
        }

        .smallLine {
            margin-bottom: 8px !important;
        }

        .tabsbar.tabsbar-radius {
            border: 1px solid #0077E2;
            border-radius: 32px;
            width: max-content;
            background: #fff;
            height: auto;
        }

        .border {
            border-radius: 8px !important;
        }

        .border2 {
            border: 1px solid !important;
        }

        .fontlabel {
            font-size: 18px !important;
        }

        .tabsbar.tabsbar-radius ul {
            display: block;
            height: auto;
            border-right: none;
            font-weight: bold;
        }

        .tabsbar.tabsbar-radius ul li {
            float: left;
            border-right: 1px solid #0077E2;
            height: auto;
        }

        .tabsbar.tabsbar-radius ul li:last-child {
            border-right: none;
        }

        .tabsbar.tabsbar-radius ul li a {
            color: #0077E2;
            height: 35px;
            line-height: 35px;
            text-decoration: none;
            border-left: none;
            text-shadow: 0 1px 0 #fff;
            -webkit-transition: background .5s ease;
            -moz-transition: background .5s ease;
            -o-transition: background .5s ease;
            transition: background .5s ease;
        }

        .tabsbar.tabsbar-radius ul li.active a, .tabsbar ul li:hover a {
            background: #0077E2;
            color: #ffffff;
        }

        table thead {
            background-color: #78A6E2;
            color: #fff;
        }

        /*timeline*/
        .timeline {
            list-style-type: none;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .li {
            transition: all 200ms ease-in;
        }

        .timestamp {
            padding: 0 10px;
            display: flex;
            flex-direction: column;
            align-items: center;
            font-weight: 100;
        }

        .status {
            margin-bottom: 20px;
            padding: 0 10px;
            display: flex;
            justify-content: center;
            border-bottom: 2px solid #D6DCE0;
            position: relative;
            transition: all 200ms ease-in;
        }

        .status strong {
            font-weight: 600;
            margin-bottom: 20px;
        }

        .status:after {
            content: "✔";
            color: #fff;
            width: 22px;
            height: 22px;
            background-color: white;
            border-radius: 25px;
            border: 1px solid #ddd;
            font-weight: bold;
            padding: 5px;
            text-align: center;
            position: absolute;
            top: 25px;
            left: 35%;
            transition: all 200ms ease-in;
        }

        .li.complete .status {
            border-bottom: 2px solid #0077E2;
        }

        .li.complete .status:after {
            background-color: #0077E2;
            border: none;
            transition: all 200ms ease-in;
        }

        .li.complete .status strong {
            color: #0077E2;
        }

        @media (min-device-width: 320px) and (max-device-width: 700px) {
            .timeline {
                list-style-type: none;
                display: block;
            }

            .li {
                transition: all 200ms ease-in;
                display: flex;
                width: inherit;
            }

            .timestamp {
                width: 100px;
            }

            .status:after {
                left: -8%;
                top: 30%;
                transition: all 200ms ease-in;
            }
        }

        td input {
            width: auto;
        }

        /*custom checkbox*/
        .checker label > [type="checkbox"]:not(:checked),
        .checker label > [type="checkbox"]:checked {
            position: relative;
            padding-left: 1.95em;
            cursor: pointer;
            left: 0;
            top: -10px;
        }

        .checker label > [type="checkbox"] {
            opacity: 10;
        }

        .checker label > [type="checkbox"]:not(:checked):before,
        .checker label > [type="checkbox"]:checked:before {
            content: '';
            position: absolute;
            left: 0;
            top: 0;
            width: 1.25em;
            height: 1.25em;
            border: 2px solid #ccc;
            background: #fff;
            border-radius: 4px;
            box-shadow: inset 0 1px 3px rgba(0, 0, 0, .1);
        }

        .checker label > [type="checkbox"]:not(:checked):after,
        .checker label > [type="checkbox"]:checked:after {
            content: '✔';
            position: absolute;
            top: .1em;
            left: .2em;
            font-size: 1.5em;
            line-height: 0.8;
            color: #2E6BD0;
            transition: all .2s;
        }

        .checker label > [type="checkbox"]:not(:checked):after {
            opacity: 0;
            transform: scale(0);
        }

        .checker label > [type="checkbox"]:checked:after {
            opacity: 1;
            transform: scale(1);
        }

        .adding .select2-container {
            margin-top: -13px !important;
        }

        select {
            max-width: 170px;
        }

        #convertForm {
            width: 200px;
            float: right;
            margin-top: 7px;
        }

        #convertLabel, #convertLabel2 {
            float: right;
            background-color: #10bb8b;
            border-radius: 6px;
            width: 86px;
            text-align: center;
            font-weight: bold;
            padding-bottom: 4px;
        }

        #convertLabel:hover, #convertLabel2:hover {
            color: black;
        }

        .gritter-item-wrapper {
            margin-top: 50px !important;
            margin-right: -20px !important;
            font-size: 12px;
        }
    </style>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <div id="load" class="hide">
        <img id="img_logo" style="width: 150px;height: 150px" src="/public/images/loading/loading-1.gif" alt="">
    </div>
    <div id="content">
        <div class="heading_top" style="    padding: 5px;">
            <div class="row-fluid" style="margin-bottom: -10px">
                <div class="pull-left span8"><h3 style="float: left;">Gửi đồ</h3><h5
                            id="titleDate">{{request('sendDate',date('d-m-Y'))}} {{request('toDate')?'  /  '.request('toDate'):''}}</h5>
                </div>
                <div id="convertForm">
                    <div id="convertLabel">Ngang</div>
                    <label style="float: right"> &nbsp;<-> &nbsp;</label>
                    <div id="convertLabel2">Khung nhập</div>
                </div>
            </div>
        </div>
        <div class="row-fluid bg_light m_top_20">
            <div class="widget widget-4 bg_light">
                <div class="widget-body">
                    <div>
                        <div class="tabsbar tabsbar-radius" style="float: left">
                            <ul class="row-fluid row-merge">
                                <li>
                                    <a href="#tab1" data-id="1" data-toggle="tab">1. Tìm kiếm</a>
                                </li>
                                <li>
                                    <a href="#tab2" data-id="2" data-toggle="tab">2. Nhận hàng mới</a>
                                </li>
                                <li>
                                    <a href="#tab3" data-id="3" data-toggle="tab">3. Hàng đã gom</a>
                                </li>
                                <li>
                                    <a href="#tab4" data-id="4" data-toggle="tab">4. Hàng đã lên xe</a>
                                </li>
                                <li>
                                    <a href="#tab5" data-id="5" data-toggle="tab">5. Hàng đã xuống xe</a>
                                </li>
                                <li>
                                    <a href="#tab6" data-id="6" data-toggle="tab">6. Hàng đã trả</a>
                                </li>
                            </ul>
                        </div>
                        <div class="input-append span2 right" style="margin-left: 30px;" id="div_filter">
                            <input type="text" style="width: 150px;height: 25px;" id="myInput"
                                   onkeyup="filterFunction(this)"
                                   placeholder="Lọc bảng theo cột">
                            <select id="filter" style="width: 150px;height: 35px;" onchange="filterFunction(this)">
                            </select>
                        </div>
                    </div>
                    <div class="tab-content" style="margin-top: 4em">
                        <div class="tab-pane " id="tab1">
                            @include('cpanel.Ship_new.search',['listGoods'=>$listGoodsClassified])
                        </div>
                        <div class="tab-pane" id="tab2">
                            @include('cpanel.Ship_new.step1',['listGoods'=>isset($listGoodsClassified)?array_filter($listGoodsClassified, function ($list) {
                 return $list['goodsStatus'] == 1;
             }) :[]])
                        </div>
                        <div class="tab-pane" id="tab3">
                            <div class="row-fluid">
                                @include('cpanel.Ship_new.step2',['listGoods'=>isset($listGoodsClassified)?array_filter($listGoodsClassified, function ($list) {
                return $list['goodsStatus'] == 2;
            }) :[]])
                            </div>
                        </div>
                        <div class="tab-pane" id="tab4">
                            @include('cpanel.Ship_new.step3',['listGoods'=>isset($listGoodsClassified)?array_filter($listGoodsClassified, function ($list) {
                return $list['goodsStatus'] == 3;
            }) :[]])
                        </div>
                        <div class="tab-pane" id="tab5">
                            @include('cpanel.Ship_new.step4',['listGoods'=>isset($listGoodsClassified)?array_filter($listGoodsClassified, function ($list) {
                return $list['goodsStatus'] == 4;
            }) :[]])
                        </div>
                        <div class="tab-pane" id="tab6">
                            @include('cpanel.Ship_new.step5',['listGoods'=>isset($listGoodsClassified)?array_filter($listGoodsClassified, function ($list) {
                return $list['goodsStatus'] == 5;
            }) :[]])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php
        if(session('companyId') == 'TC04r1lru1vOk3c')  $typePaper='DHSL';
        else if(in_array(session('companyId'),['TC03k1IzZlYcm6I','TC05wM7dDRQSo6'])) $typePaper=5;
        else $typePaper=6;
      $class_print='height_tr_a'.$typePaper;
    @endphp
    <form id="editGoods" action="{{action('ShipNewController@updateGoods')}}">
        <div class="modal hide fade" id="edit" style="width: 800px; margin-left: -30%;margin-top: -4%">
            <div class="modal-header center">
                <div class="row-fluid">
                    <h3>SỬA VẬN ĐƠN </h3>
                </div>
            </div>
            <div class="modal-body center">
                <div class="row-fluid">
                    <div class="span6">
                        <div class="form-horizontal">
                            @php
                                $array_for_left=array();
                                $array_for_right=array();
                                $array_id_left=array(18,17,19,20,5,6,3,4,11,21,22);
                                $array_id_right=array(13,12,14,15,8,9,10,23,24,25);
                               for($i=0;$i<count($array_id_left);$i++){
                               for($j=0;$j<count($listSetting);$j++){
                               if($listSetting[$j]['id']==$array_id_left[$i])
                               $node_left=$listSetting[$j];
                               else if($i<10 && $listSetting[$j]['id']==$array_id_right[$i])
                               $node_right=$listSetting[$j];
                               }
                                if(isset($node_left) && !in_array($node_left,$array_for_left)) array_push($array_for_left,$node_left);
                                if(isset($node_right) && !in_array($node_right,$array_for_right)) array_push($array_for_right,$node_right);
                            }
                            @endphp
                            @foreach($array_for_left as $k=>$list)
                                <div class="control-group smallLine">
                                    @php
                                        if(trim($list['alias'],'') !='') $nameAttr=$list['alias']; else $nameAttr=$list['name'];
                                        if(in_array($list['id'],[6,9,10,23,24,25]) && strpos($nameAttr, 'VNĐ') == false && strpos($nameAttr, 'VND') == false) $nameAttr .='(VNĐ)';
                                    @endphp
                                    <label class="control-label">{{$nameAttr}}</label>
                                    <div class="controls">
                                        @php
                                            switch ($list['id']){
                                            case 18: echo '<input type="text" id="editPlaceSend" name="editPlaceSend">'; break;
                                            case 17:  echo '<select id="editSavePick" name="editSavePick"><option>Chọn bến</option>';
                                                    for($i=0;$i<count($listPoint);$i++)
                                                        echo '<option value="'.$listPoint[$i]['pointId'].'">'.$listPoint[$i]['pointName'].'</option>';
                                                    echo '</select>'; break;
                                            case 19: echo '<input type="number" id="editPhoneSend" name="editPhoneSend">'; break;
                                            case 20: echo '<input type="text" id="editSender" name="editSender">'; break;
                                            case 5: echo '<input type="text" id="editName" name="editName" required>'; break;
                                            case 6: echo '<input type="number" id="editValue" name="editValue" value="0">'; break;
                                            case 3: echo '<input autocomplete="off" id="editDate" type="text" name="editDate" class="datepicker span9" title="Từ ngày" value="">'; break;
                                            case 4: echo '<select id="editGoodType" name="goodType"><option value=""></option>';
                                                        for($i=0;$i<count($listTypeGoods);$i++)
                                                        echo '<option value="'.$listTypeGoods[$i]['goodsTypeId'].'">'.$listTypeGoods[$i]['goodsTypeName'].'</option>';
                                                echo '</select>'; break;
                                            case 11: echo '<input type="text" id="editNoteExtra" name="editNoteExtra">'; break;
                                            case 21: echo '<input type="text" id="editNote" name="editNote">'; break;
                                            case 22: echo '<input type="checkbox" id="editDropMidway" name="editDropMidway"><label style="margin-left: 5px;" for="editDropMidway"></label>'; break;
                                            }
                                        @endphp

                                    </div>
                                    @if($list['id']==7)
                                        <div class="frm_chonanh" style=" margin-left: 320px;    margin-top: 0px;">
                                            <div class="chonanh right" style="margin-top: -5px;margin-right: 20px"><img
                                                        class="imgGoodEdit" src="/public/images/Anhguido1.png"></div>
                                            <input type="file" name="imgEdit" class="selectedFile"
                                                   style="display: none;">
                                            <input type="hidden" class="listImage" name="listImage" value="">
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="span6">
                        <div class="form-horizontal">
                            @foreach($array_for_right as $k=>$list)
                                <div class="control-group smallLine">
                                    @php
                                        if(trim($list['alias'],'') !='') $nameAttr=$list['alias']; else $nameAttr=$list['name'];
                                        if(in_array($list['id'],[6,9,10,23,24,25]) && strpos($nameAttr, 'VNĐ') == false && strpos($nameAttr, 'VND') == false) $nameAttr .='(VNĐ)';
                                    @endphp
                                    <label class="control-label">{{$nameAttr}}</label>
                                    <div class="controls">
                                        @php
                                            switch ($list['id']){
                                            case 13: echo '<input type="text" id="editPlaceReceive" name="editPlaceReceive">'; break;
                                            case 12:  echo '<select  id="editSaveDrop" name="editSaveDrop"><option>Chọn bến</option>';
                                                    for($i=0;$i<count($listPoint);$i++)
                                                        echo '<option value="'.$listPoint[$i]['pointId'].'">'.$listPoint[$i]['pointName'].'</option>';
                                                    echo '</select>'; break;
                                            case 14: echo '<input type="number" id="editPhoneReceive" name="editPhoneReceive" required>'; break;
                                            case 15: echo '<input type="text" id="editReceive" name="editReceive">'; break;
                                            case 8: echo '<input type="number" id="editNumber" name="number" class="editPrice">'; break;
                                            case 9: echo '<input type="number" class="editPrice" id="editPrice" name="editPrice" required>'; break;
                                            case 10: echo '<input type="number" class="editPrice" id="editExtraPeice" name="editExtraPeice">'; break;
                                            case 23: echo '<input type="number" class="editPrice" id="editTotal" name="editTotal" required >'; break;
                                            case 24: echo '<input type="number" id="editPaid" class="editPrice" name="editPaid">'; break;
                                            case 25: echo '<input type="number" id="editUnPaid" class="editPrice" name="editUnPaid">'; break;
                                            }
                                        @endphp
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row-fluid">
                    <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                    <button class="btn btn btn-warning btn-flat" style="    margin-right: 10px;" type="submit">CẬP NHẬT
                    </button>
                </div>
            </div>
            <input type="hidden" id="codeEdit" name="codeEdit">
        </div>
    </form>
    <form id="editGoodsNew" action="{{action('ShipNewController@updateGoods')}}">
        <div class="modal hide fade" id="editNew" style="width: 800px; margin-left: -30%;margin-top: -4%">
            <div class="modal-header center">
                <div class="row-fluid">
                    <h3>SỬA VẬN ĐƠN</h3>
                    {{--<label class="sign1">Hàng</label>--}}
                    {{--<label class="sign2">Khách gửi</label>--}}
                    {{--<label class="sign3">Trả khách</label>--}}
                </div>
            </div>
            <div class="modal-body center" id="bodyEditNew">
            </div>
            <div class="modal-footer">
                <div class="row-fluid">
                    <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                    <button class="btn btn btn-warning btn-flat" style="    margin-right: 10px;" type="submit">CẬP NHẬT
                    </button>
                </div>
            </div>
            <input type="hidden" id="codeEditNew" name="codeEdit">
        </div>
    </form>
    <form id="submitCreateNew" action="{{action('ShipNewController@createGoods')}}">
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
        <div class="modal hide fade" id="createNew" style="width: 800px; margin-left: -30%;margin-top: -2%">
            <div class="modal-header center">
                <div class="row-fluid">
                    <h3>TẠO VẬN ĐƠN</h3>
                    {{--<label class="sign1">Hàng</label>--}}
                    {{--<label class="sign2">Khách gửi</label>--}}
                    {{--<label class="sign3">Trả khách</label>--}}
                </div>
            </div>
            <div class="modal-body center" id="contentCreateNew">
            </div>
            <div class="modal-footer">
                <div class="row-fluid">
                    <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                    <button class="btn btn btn-warning btn-flat" style="    margin-right: 10px;" type="submit">TẠO
                    </button>
                </div>
            </div>
        </div>
    </form>
    <div class="modal modal-custom hide fade" data-backdrop="static" id="print-order"
         style="width: 80%;margin-left: -40%;top: 2%;">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>IN VẬN ĐƠN</h3>
        </div>
        <div id="divVanDon" class="modal-body" style="border: 1px solid;font-family: serif;">
            <div id="print">
            </div>
        </div>

        <div id="divVanDon" class="modal-body" style="border: 1px solid;font-family: serif;">
            <div id="print">
            </div>
        </div>
        <div class="modal-footer">
            <div class="row-fluid">
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn-warning btn-flat" id="inVanDon">IN</button>
            </div>
        </div>
    </div>
    <div id="printnew" hidden style="width:100%;">
        <div class="print-box print_biennhan_new"
             style="width: 140mm;height:96mm;font-size:15px;border: 1px solid #000000;margin-left: 20px;margin-top: 20px">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <div style="width: 30%;float: left">
                            <img src="{{session('userLogin')['logo']}}" width="50%" height="30mm"
                                 style="    padding-left: 10mm;"></div>
                        <div class="title_company"></div>
                        <div class="barCode divRight" style="width: 30%;float: left"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="titlePrint" style="font-size: 16px;text-align: center"><strong>BIÊN NHẬN GỬI HÀNG
                                HÓA</strong></div>
                    </td>
                </tr>
                <tr class="{{$class_print}}">
                    <td style="padding-top: 5mm;">
                        <div class="divLeft">
                            <div class="divLeft2">Nơi gửi:</div>
                            <strong class="returnpick"
                                    style="font-size: 18px;margin-left: 3mm"></strong></div>
                        <div class="divRight">
                            <div class="divLeft2">Nơi nhận:</div>
                            <strong class="returndrop"
                                    style="font-size: 18px;margin-left: 3mm"></strong>
                        </div>
                    </td>
                </tr>
                <tr class="{{$class_print}}">
                    <td>
                        <div class="divLeft">
                            <div class="divLeft2">Người gửi:</div>
                            <strong class="returnsender" style="margin-left: 3mm"></strong>
                        </div>
                        <div class="divRight">
                            <div class="divLeft2">Người nhận:</div>
                            <strong class="returnreceiver"></strong></div>
                    </td>
                </tr>
                <tr class="{{$class_print}}">
                    <td>
                        <div class="divLeft">
                            <div class="divLeft2">SĐT gửi:</div>
                            <strong class="returnsendphone" style="margin-left: 3mm"></strong>
                        </div>
                        <div class="divRight">
                            <div class="divLeft2">SĐT nhận:</div>
                            <strong class="returnreceiverphone"
                                    style="margin-left: 3mm"></strong></div>
                    </td>
                </tr>
                <tr class="{{$class_print}}">
                    <td>
                        <div class="divLeft">
                            <div class="divLeft2">Tên hàng gửi:</div>
                            <strong class="returngood" style="margin-left: 3mm"></strong>
                        </div>
                        <div class="divRight"><strong style="font-size: 16px;margin-left: 3mm"
                                                      class="returnprice"></strong></div>
                    </td>
                </tr>
                <tr class="{{$class_print}}">
                    <td>
                        <div class="divLeft">
                            <div class="divLeft2">Số lượng:</div>
                            <strong style="font-size: 16px;margin-left: 3mm"
                                    class="returnquantity"></strong></div>
                        <div class="divRight">
                            <div class="divLeft2">Tiền cước:</div>
                            <strong style="font-size: 16px;margin-left: 3mm"
                                    class="returntotalprice"></strong></div>
                    </td>
                </tr>
                <tr class="{{$class_print}}">
                    <td>
                        <div class="divLeft">
                            <div class="divLeft2">Đã thanh toán :</div>
                            <strong style="font-size: 16px;margin-left: 3mm"
                                    class="returnpaid"></strong></div>
                        <div class="divRight">
                            <div class="divLeft2">Còn lại:</div>
                            <strong style="font-size: 16px;margin-left: 3mm"
                                    class="returnunpaid"></strong></div>
                    </td>

                </tr>
                <tr class="{{$class_print}}">
                    <td>
                        <div style="float: left;">Lưu ý: <strong class="returnnote" style="margin-left: 3mm"></strong>
                        </div>
                    </td>
                </tr>
                @if(session('companyId') == 'TC04r1lru1vOk3c')
                    <tr>
                        <td>
                            <div class="note_company"></div>
                        </td>
                    </tr>
                @endif
                <tr class="{{$class_print}}" style="line-height: 15mm;">
                    <td>
                        <div class="divLeft" style="padding-top: 4mm;text-align: center">Nhân viên ký tên</div>
                        <div class="divRight" style="padding-top: 4mm;text-align: center">Khách hàng ký tên</div>
                    </td>
                </tr>
                <tr class="{{$class_print}}">
                    <td>
                        <div class="divLeft" style="padding-top: 0mm;text-align: center">.................</div>
                        <div class="divRight" style="padding-top: 0mm;text-align: center">.................</div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="print_tem_new" style="font-size: 10px;">
            <div class="print-box" style="width: 72mm; height: 45mm; float: left;border: 0px solid #000000;">
                <div style="float: right;">{{date('d \- m \- Y')}}</div>
                <div style="margin-left: 15mm">
                    <div class="barCode" style="padding-left: 15mm"></div>
                </div>
                <table style="width: 100%;margin-top: -3mm">
                    <tr>
                        <td style="text-align: center;font-size: 14px;padding-top: 15px;"><strong>NHẬN TẠI XE</strong>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 2mm;">Nơi nhận: <strong style="font-size: 13px;"
                                                                        class="returndrop"></strong></td>
                    </tr>
                    <tr>
                        <td>Người nhận: <strong style="font-size: 13px;" class="returnreceiver"></strong></td>
                    </tr>
                    <tr>
                        <td>SĐT: <strong style="font-size: 13px;" class="returnsendphone"></strong></td>
                    </tr>
                    <tr>
                        <td><strong style="font-size: 13px;" class="returnprice"></strong></td>
                    </tr>
                    <tr>
                        <td>Số lượng: <strong style="font-size: 13px;" class="returnquantity"></strong></td>
                    <tr>
                        <td>Tiền cước: <strong style="font-size: 13px;" class="returntotalprice"></strong></td>
                    </tr>
                    <tr>
                        <td>Đã thanh toán: <strong style="font-size: 13px;" class="returnpaid"></strong></td>
                    </tr>
                    <tr>
                        <td>Còn lại: <strong style="font-size: 13px;" class="returnunpaid"></strong></td>
                    </tr>
                </table>
            </div>
            <div style="width: 28mm;height:22mm;float: left;border: 0px solid #000000;" class="print-box">
                <table style="width: 100%">
                    <tr>
                        <td colspan="2">
                            <div style="margin-left: -5mm" class="findfirst" id="textCode">
                                <div class="barCode"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Đến: <strong style="font-size: 11px;" class="returndrop"></strong></td>
                    </tr>
                    <tr>
                        <td>Hàng: <strong style="font-size: 11px;" class="returngood"></strong></td>
                    </tr>
                </table>
            </div>
            <div style="width: 28mm;height:22mm;float: left;border: 0px solid #000000;"
                 class="print-box">
                <table style="width: 100%">
                    <tr>
                        <td colspan="2">
                            <div class="findfirst" id="textCode">
                                <div style="margin-left: -5mm" class="barCode"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Đến: <strong style="font-size: 11px;" class="returndrop"></strong></td>
                    </tr>
                    <tr>
                        <td>Hàng: <strong style="font-size: 11px;" class="returngood"></strong></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="modal hide fade" id="question" style="width: 300px; margin-left: -10%;margin-top: 10%;">
        <div class="modal-header center">
            <div class="row-fluid">
                <h3>Bạn có muốn in đồ vừa tạo ?</h3>
            </div>
        </div>
        <div class="modal-body center">
            <div class="row-fluid">
                <div style="float: left;margin-left: 20%;">
                    <div style="float: left;    margin-left: 15px; margin-top: 5px;">
                        <input type="checkbox" id="biennhan"> <label for="biennhan">Biên nhận</label>
                    </div>
                    <div style="float: left;    margin-left: 15px;margin-top: 5px;">
                        <input type="checkbox" id="tem"> <label for="tem">Tem dán</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="row-fluid">
                <a data-dismiss="modal" style="    float: left; padding-left: 25%;cursor:pointer" aria-hidden="true"
                   class="btn_huy_modal">KHÔNG IN</a>
                <button class="btn btn btn-warning btn-flat hide" style=" float: left;" id="actionPrint">IN</button>
                <button class="btn btn btn-warning btn-flat" style="float: left;" id="actionprintNew">IN</button>

            </div>
        </div>
    </div>
    <form action="{{action('ShipNewController@deleteGoods')}}">
        <div class="modal hide fade" id="delete" style="width: 300px; margin-left: -16%;margin-top: 10%;">
            <div class="modal-header center">
                <div class="row-fluid">
                    <h3>BẠN CHẮC CHẮN MUỐN XÓA ?</h3>
                </div>
            </div>
            <div class="modal-body center">

            </div>
            <div class="modal-footer">
                <div class="row-fluid">
                    <a data-dismiss="modal" style=" cursor:pointer" aria-hidden="true"
                       class="btn_huy_modal">KHÔNG</a>
                    <button class="btn btn btn-warning btn-flat" type="submit">CÓ</button>
                    <input type="hidden" name="listCode" id="listCode">
                </div>
            </div>
        </div>
    </form>
    <div class="hide">
        <div id="printPackage3">
            <div id="title_print_package">DANH SÁCH HÀNG ĐÃ LÊN XE</div>
            <table id="print3" style="border: 1px solid;width: 100%"></table>
            <div class="row-fluid" style=" margin-top: 15px">
                <div style="float: left;text-align: center; width: 50%">Nhân viên ký</div>
                <div style="float: right;text-align: center; width: 48%">Tài xế/phụ xe ký</div>
            </div>
        </div>
    </div>
    <div class="modal hide fade" id="showImg">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: black;">×</button>
        </div>
        <div class="modal-body center">
            <img id="imgZoom" src="">
        </div>
        <div class="modal-footer">
        </div>
    </div>
    <script src="/public/javascript/jquery-dateFormat-master/dist/jquery-dateFormat.min.js"
            type="text/javascript"></script>
    <script src="/public/javascript/barCode/jquery-barcode.min.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script>
        let datepickerBox = $("#txtCalendar"),//lịch
            selectBoxRoute = $('#listRoute'),//selectbox chọn tuyến
            selectBoxListTrip = $('#listTrip'),//selectbox chọn tuyến

            urlSeachTrip = '{{action('TripController@search')}}';
        var listGoods ={!! json_encode($listGoodsClassified) !!},
            dem = 0, text = '', index = 0, priceType = 0,
            dateBox = $("#calendar"),//lịch
            boxRoute = $('#routeId'),//selectbox chọn tuyến
            boxListTrip = $('#scheduleId'),//selectbox chọn tuyến
            array_idFillter ={!! json_encode($array_idFillter) !!},
            listSetting ={!! json_encode($listSetting) !!},
            listType ={!! json_encode($listTypeGoods) !!},
            listPoint ={!! json_encode($listPoint) !!};
        var note_DHSL = '<hr style="margin: 2px 0;"><strong>Trách nhiệm của khách hàng và nghĩa vụ bồi thường của Công ty.</strong><br>' +
            '- Người gửi hàng tự chịu trách nhiệm trước pháp luật về nội dung thông tin đã cung cấp dưới dạng văn bản, kiện hàng gửi được ghi trên phiếu gửi hàng này.<br>' +
            '- Nếu hàng gửi bị mất hoặc hư hỏng hoàn toàn, Công ty sẽ bồi thường tối đa bằng 100% giá trị hàng gửi khách hàng đã cung cấp và được ghi trên phiếu gửi hàng này.<br>' +
            '- Nếu hàng gửi bị hư hỏng hoặc thiếu một phần, Công ty sẽ bồi thường theo tỉ lệ % hư hỏng hoặc thiếu một phần đó theo giá trị hàng gửi khách hàng cung cấp và<br> được ghi trên phiếu gửi hàng này.<br>' +
            '- Nếu hàng gửi là giấy tờ hoặc hàng không kê khai giá trị,khi bị mất hoặc hỏng sẽ được bối thường tối đa bằng 50 lần tiền cước gói hàng.<br>' +
            '- Thời gian khiếu nại về hàng gửi chậm nhất 30 ngày,kể từ ngày gửi hàng . Có phiếu gửi hàng kèm theo.<br>' +
            '- Mọi bồi thường cho khách hàng bằng tiền VNĐ, không bồi thường bằng ngoại tệ hoặc hiện vật.';
        var companyA5 = ['TC04r1lru1vOk3c', 'TC03k1IzZlYcm6I', 'TC05wM7dDRQSo6'];
        var companyNewForm = ['TC04r1lru1vOk3c', 'TC01gWSmr8A9Qx'];
        var newForm = false;
        htmlAdd = '', htmlEdit = '';
        var checkfocus = 0, dem = 1;
        //Danh sách thuộc tính gửi đồ

        // var LIST_ID=[
        //     1  -  index_row         - STT,
        //     2  -  goodsCode         - Mã vận đơn,
        //     3  -  sendDate          - Ngày tạo,
        //     4  -  goodsTypeName     - Loại hàng,
        //     5  -  goodsName         - Tên hàng,
        //     6  -  goodsValue        - Giá trị hàng,
        //     7  -  imageURL          - Ảnh hàng,
        //     8  -  quantity          - Số lượng,
        //     9  -  price             - Giá cước,
        //     10 -  extraPrice        - Giá cộng thêm,
        //     11 -  extraPriceNote    - Lí do thêm,
        //     12 -  dropPointName     - Bến xuống,
        //     13 -  dropOffPoint      - Nơi giao hàng,
        //     14 -  receiverPhone     - SĐT khách nhận,
        //     15 -  receiver          - Khách nhận,
        //     16 -  receiverImageURL  - Ảnh khách nhận,
        //     17 -  pickPointName     - Bến lên,
        //     18 -  pickUpPoint       - Nơi nhận hàng,
        //     19 -  senderPhone       - SĐT  khách gửi,
        //     20 -  sender            - Khách gửi,
        //     21 -  notice            - Ghi chú,
        //     22 -  dropMidway        - Trả dọc đường,
        //     23 -  totalPrice        - Tổng tiền,
        //     24 -  paid              - Đã trả,
        //     25 -  unpaid            - Chưa trả
        // ];
        init();

        $(document).ready(function () {

            close_menu();
            $('body').on('click', '#convertLabel,#convertLabel2', function () {
                if ($(this).css('opacity') != 1) {
                    $(this).css('opacity', 1);
                    if ($(this).attr('id') === 'convertLabel') {
                        newForm = false;
                        $('#convertLabel2').css('opacity', 0.7);
                    }
                    else {
                        newForm = true;
                        $('#convertLabel').css('opacity', 0.7);
                        var e = (document.querySelector('.adding'));
                        if (e) e.remove();
                        $('#js-step-1 tr').each(function () {
                            if ($(this).attr('id') != 'setWidth' && !$(this).hasClass('adding') && !$(this).hasClass('info'))
                                $(this).removeClass('hide');
                        });
                        if (e != null) add();
                    }
                }
                Message('Đã chuyển giao diện nhập hàng');
            });
            $('body').on('click', '.imgGood,.imgGoodEdit', function () {
                $(this).parent().parent().find('.selectedFile').click();
            });
            $('body').on('click', '.imgGood,.zoom', function () {
                if ($(this).attr('src') != '/public/images/Anhguido1.png' && $(this).attr('src') != '/public/images/loading/loader_blue.gif') {
                    $('#imgZoom').attr('src', $(this).attr('src'));
                    $('#showImg').modal('show');
                }
            });
            $('body').on('change', '.selectedFile', function () {
                var that = this;
                var form = new FormData();
                var srcAdd = $($(that).parent().find('.imgGood')).attr('src');
                var srcEdit = $($(that).parent().find('.imgGoodEdit')).attr('src');
                form.append('img', $(that).prop('files')[0]);
                form.append('_token', '{{ csrf_token() }}');
                $($(that).parent().find('.imgGood')).attr('src', '/public/images/loading/loader_blue.gif');
                $($(that).parent().find('.imgGoodEdit')).attr('src', '/public/images/loading/loader_blue.gif');
                $.ajaxSetup({
                    processData: false,
                    contentType: false
                });
                $.post('/cpanel/system/upload-image',
                    form,
                    function (data) {
                        $($(that).parent().find('.imgGood')).attr('src', data.url);
                        $($(that).parent().find('.imgGoodEdit')).attr('src', data.url);
                        $($(that).parent().find('.listImage')).val(data.url);
                        if ($('#tab5').hasClass('active')) {
                            $.ajaxSetup({
                                processData: true,
                                contentType: true
                            });
                            $.ajax({
                                url: '{{action('ShipNewController@updateGoods')}}',
                                data: {
                                    codeEdit: $(that).data('id'),
                                    receiverImageURL: data.url
                                }
                            });
                        }
                    }).fail(function (data) {
                    $($(that).parent().find('.imgGood')).attr('src', srcAdd.toString());
                    $($(that).parent().find('.imgGoodEdit')).attr('src', srcEdit.toString());
                });
            });

            $('body').on('click', '.add', function () {
                $('ul.row-merge li:first').removeClass('active');
                $('#tab1').removeClass('active');
                $($('ul.row-merge li')[1]).addClass('active');
                $('#tab2').addClass('active');
                sessionStorage.setItem('tab', 2);
                index++;
                add();
            });
            $('#deleteGood').click(function () {
                var list = [];
                $('.vandon:checked').each(function () {
                    list.push($(this).attr('value'));
                });
                $('#listCode').val(JSON.stringify(list));
                $('#delete').modal('show');
            });
            $('body').on('click', '.tabsbar a', function () {
                sessionStorage.setItem('tab', $(this).data('id'));
                if ($(this).data('id') == 3 || $(this).data('id') == 4) $('#div_filter').hide();
                else $('#div_filter').show();
            });

            $('.tabsbar a').each(function () {
                if (sessionStorage.getItem('tab') == dem)
                    $($(this).parent()).addClass('active');
                dem++;
            });

            $('#tab' + sessionStorage.getItem('tab')).addClass('active');
            $('body').on('change', '#searchGoodCode,#searchReceiverPhone,#searchSenderPhone,#RouteId,#dateFrom,#dateTo', function () {
                var check;
                if ($(this).attr('id') == 'searchGoodCode') {
                    if ($(this).val() != '') check = true; else check = false;
                    $('#searchReceiverPhone').prop('disabled', check);
                    $('#searchSenderPhone').prop('disabled', check);
                    $('#RouteId').prop('disabled', check);
                    $('#dateFrom').prop('disabled', check);
                    $('#dateTo').prop('disabled', check);
                    $(this).prop('disabled', false);
                }
                if ($(this).attr('id') == 'RouteId' || $(this).attr('id') == 'dateFrom' || $(this).attr('id') == 'dateTo') {
                    if ($('#RouteId').val() == 'Tất cả tuyến') check = false; else check = true;
                    $('#RouteId').prop('disabled', false);
                    $('#dateFrom').prop('disabled', false);
                    $('#dateTo').prop('disabled', false);
                    $('#searchReceiverPhone').prop('disabled', check);
                    $('#searchSenderPhone').prop('disabled', check);
                    $('#searchGoodCode').prop('disabled', check);
                }
                if ($(this).attr('id') == 'searchReceiverPhone' || $(this).attr('id') == 'searchSenderPhone') {
                    if ($('#searchSenderPhone').val() == '' && $('#searchReceiverPhone').val() == '') check = false; else check = true;
                    $('#RouteId').prop('disabled', check);
                    $('#dateFrom').prop('disabled', false);
                    $('#dateTo').prop('disabled', false);
                    $('#searchReceiverPhone').prop('disabled', false);
                    $('#searchSenderPhone').prop('disabled', false);
                    $('#searchGoodCode').prop('disabled', check);
                }

            });
            $('tr.showtr2').click(function () {
                if ($(this).hasClass('hidetr2')) {
                    $(this).removeClass('hidetr2');
                    $('.' + $(this).data('code')).addClass('hide');
                    $('.' + $(this).data('code')).each(function () {
                        if ($(this).attr('style') == 'display: table-row;')
                            $(this).attr('style', 'display: none;');
                    });
                }
                else {
                    $(this).addClass('hidetr2');
                    $('.' + $(this).data('code')).each(function () {
                        if (!$(this).hasClass('info')) $(this).removeClass('hide');
                    });
                }
            });
            $('tr.showtr3').click(function () {
                if ($(this).hasClass('hidetr3')) {
                    $(this).removeClass('hidetr3');
                    $('.' + $(this).data('code')).addClass('hide');
                    $('.' + $(this).data('code')).each(function () {
                        if ($(this).attr('style') == 'display: table-row;')
                            $(this).attr('style', 'display: none;');
                    });
                }
                else {
                    $(this).addClass('hidetr3');
                    $('.' + $(this).data('code')).each(function () {
                        if (!$(this).hasClass('info')) $(this).removeClass('hide');
                    });
                }
            });
            $('body').on('click', 'tr,#js-btn-add-package', function () {
                if ($('.requireName')[0]) $(document.getElementsByClassName('timeline')).attr('style', 'margin-left: -58%');
                else {
                    $(document.getElementsByClassName('timeline')).attr('style', 'margin-left: -58%');
                    // $($('.timeline li.complete')[0]).attr('style','margin-left: -200px');
                }
            });


            $('body').on('change', '.extraPrice', function () {
                var id;
                if (newForm) id = 'New';
                else id = ($(this).attr('id')).substring(($(this).attr('id')).indexOf('_'), ($(this).attr('id')).length);

                if ($('#extraPrice' + id).val() != '' && $('#extraPrice' + id).val() != 0)
                    $('#extraPriceNote' + id).prop('disabled', false);
                else $('#extraPriceNote' + id).prop('disabled', true);

            });

            $('body').on('change', '#listTrip option:selected,#scheduleId option:selected', function () {
                $('.tripId').val($(this).data('tripId'));
            });
            $('body').on('change', '.giaohang,#check_all_4', function () {
                if ($(this).attr('id') == 'check_all_4')
                    if ($(this).prop('checked') == true) $('.giaohang').prop('checked', true);
                    else $('.giaohang').prop('checked', false);
                var list = [];
                $('.giaohang:checked').each(function () {
                    list.push($(this).attr('value'));
                });
                if (list.length > 0) $('#listGiaohang').val(JSON.stringify(list));
                else $('#listGiaohang').val('');
                if ($('.giaohang:checked').length > 0) {
                    $('#giaohang').prop('disabled', false);
                    $('#invandon4').prop('disabled', false);
                }
                else {
                    $('#giaohang').prop('disabled', true);
                    $('#invandon4').prop('disabled', true);
                }
            });
            $('body').on('click', '#duaxuong,#phuchoi', function () {
                var list = [];
                $('.inputxuongxe:checked').each(function () {
                    list.push($(this).attr('value'));
                });
                if (list.length > 0) $('#goodsPackageCodeXuongxe').val(JSON.stringify(list));
                if (list.length > 0) $('#goodsPackageCodeReturn').val(JSON.stringify(list));
                list = [];
                $('.khonggom:checked').each(function () {
                    list.push($(this).attr('value'));
                });
                if (list.length > 0) $('#goodsCodeXuongxe').val(JSON.stringify(list));
                if (list.length > 0) $('#goodsCodeReturn').val(JSON.stringify(list));
            });
            $('body').on('keyup', '.ajaxPhone', function () {
                if ($(this).val() < 0) $(this).val(0);
            });
            $('body').on('keyup', '.changePrice', function (event) {
                var form = '';
                $('.changePrice').each(function () {
                    if ($(this).val() == '') $(this).val(0);
                });
                console.log('{{session('companyId')}}');
                if (newForm) form = 'New';
                updatePrice(this, form);
            });
            $('body').on('keyup', '.editPrice', function (event) {

                $('.editPrice').each(function () {
                    if ($(this).val() == '') $(this).val(0);
                });
                if (($(this).attr('id')).includes('New')) var form = 'New'; else var form = '';
                updateEditPrice(this, form);

            });
            $('body').on('change', '.vandon,#check_all_1', function () {
                if (!$(this).is(':checked')) $('#check_all_1').prop('checked', false);
                if ($(this).attr('id') == 'check_all_1') {
                    if ($(this).is(':checked'))
                        $('.vandon').each(function () {
                            if ($($(this).parents('tr')).attr('style') != 'display: none;') {
                                $(this).prop('checked', true);
                            }
                        });
                    else $('.vandon').prop('checked', false);
                }
                if ($('.vandon:checked').length > 0) {
                    $('#editGood').prop('disabled', false);
                    $('#deleteGood').prop('disabled', false);
                    $('.js-btn-print').prop('disabled', false);
                    $('#js-btn-create-package').prop('disabled', false);
                    $('#vandon').prop('disabled', false);
                } else {
                    $('#editGood').prop('disabled', true);
                    $('#deleteGood').prop('disabled', true);
                }
                var list = [];
                $('.vandon:checked').each(function () {
                    if ($(this).data('drop') != '') list.push($(this).data('drop'));
                    if ($(this).data('pick') != '') list.push($(this).data('pick'));
                });
                if (list.length > 0) $('#listPointId').val(JSON.stringify(list));
                else $('#listPointId').val('');
            });
            $('body').on('click', '#editGood', function () {
                var list ={!! json_encode($listGoodsClassified) !!} ;
                var good = $.grep(list, function (goods) {
                    return goods.goodsCode == $('.vandon:checked').attr('value');
                })[0];
                if (newForm) {
                    if (htmlEdit == '') {
                        htmlEdit = gennerateFormNewEdit();
                        $('#bodyEditNew').html(htmlEdit);
                        $('#bodyEditNew').append('<input type="hidden" name="editPrice" value="" id="edit_price_buffer">');
                        $('#bodyEditNew').append('<input type="hidden" name="editExtraPeice" value="" id="edit_extraPrice_buffer">');
                        $('#bodyEditNew').append('<input type="hidden" name="editTotal" value="" id="edit_total_buffer">');
                        $('#bodyEditNew').append('<input type="hidden" name="editPaid" value="" id="edit_paid_buffer">');
                        $('#bodyEditNew').append('<input type="hidden" name="editUnPaid" value="" id="edit_unPaid_buffer">');
                        $('#editSavePickNew').select2();
                        $('#editSaveDropNew').select2();
                    }
                    fillDataEdit('New', good);
                } else fillDataEdit('', good);
            });

            $('.resetGood ').change(function () {
                var list = [];
                $('.resetGood:checked').each(function () {
                    list.push($(this).val());
                });
                if ($('.resetGood:checked').length) {
                    $('.resetGood').prop('disabled', true);
                    $($($(this).parent().parent().parent().parent()).find('input')).prop('disabled', false);
                    $('#resetGood').prop('disabled', false);
                    $('.check_all2').prop("disabled", true);
                }
                else {
                    $('#resetGood').prop('disabled', true);
                    $('.check_all2').prop("disabled", false);
                }
                if (list.length) $('#goodsCodeMove').val(JSON.stringify(list));
                $('#packageCodeReset').val($(this).data('package'));
            });
            $('.check_all2').change(function () {
                var listcode = [], listtrip = [];
                var check = 0;
                var that = this;
                var list = [];

                $('.check_all2').each(function () {
                    if ($(this).is(':checked')) {
                        listcode.push($(this).val());
                        listtrip.push($(this).data('tripid'));
                        if ($(this).data('drop') != '') list.push($(this).data('drop'));
                        if ($(this).data('pick') != '') list.push($(this).data('pick'));
                    }

                    if ($(that).data('tripid') != '') {
                        $('.check_all2').each(function () {
                            if ($(this).data('tripid') == '') $(this).prop("disabled", true);
                            else $(this).prop("disabled", false);
                        });
                    } else {
                        $('.check_all2').each(function () {
                            if ($(this).data('tripid') != '') $(this).prop("disabled", true);
                            else $(this).prop("disabled", false);
                        });
                    }

                });
                if ($('.check_all2:checked').length) {
                    $('input.resetGood').prop('disabled', true);
                    $('button#aceptGoods').prop("disabled", false);
                    $('button#inmagom').prop("disabled", false);
                    $('button#pokeGood').prop("disabled", false);
                }
                else {
                    $('.check_all2').prop('disabled', false);
                    $('input.resetGood').prop('disabled', false);
                    $('button#aceptGoods').prop("disabled", true);
                    $('button#inmagom').prop("disabled", true);
                    $('button#pokeGood').prop("disabled", true);
                }
                $('.listPackageGood2').val(JSON.stringify(listcode));
                $($('.tripId2')[0]).val(JSON.stringify(listtrip));
                $('#listPointId').val(JSON.stringify(list));

            });
            $('.check_all3').change(function () {
                var check = 0;
                var that = this;
                // if ($(this).is(':checked'))
                //     $('.khonggom').each(function () {
                //         $(this).prop("disabled", true);
                //     });
                $('.check_all3').each(function () {

                    if ($(that).data('tripid') != '') {
                        $('.check_all3').each(function () {
                            if ($(this).data('tripid') == '') $(this).prop("disabled", true);
                            else $(this).prop("disabled", false);
                        });
                    } else {
                        $('.check_all3').each(function () {
                            if ($(this).data('tripid') != '') $(this).prop("disabled", true);
                            else $(this).prop("disabled", false);
                        });
                    }

                });

                if ($('.check_all3:checked').length > 0 || $('.khonggom:checked').length > 0) {
                    // $('button#inhanglenxe').prop("disabled", false);
                    $('button#duaxuong').prop("disabled", false);
                    $('button#phuchoi').prop("disabled", false);
                }
                else {
                    // $('button#inhanglenxe').prop("disabled", true);
                    $('button#duaxuong').prop("disabled", true);
                    $('button#phuchoi').prop("disabled", true);
                }
            });
            $('.khonggom').change(function () {
                if ($('.check_all3:checked').length > 0 || $('.khonggom:checked').length > 0) {
                    //$('button#inhanglenxe').prop("disabled", false);
                    $('button#duaxuong').prop("disabled", false);
                    $('button#phuchoi').prop("disabled", false);
                }
                else {
                    //$('button#inhanglenxe').prop("disabled", true);
                    $('button#duaxuong').prop("disabled", true);
                    $('button#phuchoi').prop("disabled", true);
                }

            });
            $(datepickerBox).change(function () {
                updateSelectBoxTrip();
            });
            $(selectBoxRoute).change(function () {
                updateSelectBoxTrip();
            });
            $('body').on('change', '#routeId,#routeId2,#calendar2,#calendar', function () {
                updateSelectBoxTripVd(this);
            });
            $('input.check_all').change(function () {
                var list = [];
                $('table.accept .check_all:checked').each(function () {
                    list.push($(this).attr('value'));
                });
                $('#listPackageGood').val(JSON.stringify(list));
            });
            $('.vandon').on('change', function () {
                var list = [];
                $('.vandon:checked').each(function () {
                    list.push($(this).attr('value'));
                });
                $('#listGood').val(JSON.stringify(list));
            });
            $('.check_all_vandon').on('change', function () {
                var list = [];
                $('.vandon').each(function () {
                    list.push($(this).attr('value'));
                });
                $('#listGood').val(JSON.stringify(list));
            });
            $('body').on('change', '#addCode,#addToPackage,#addCodeShip,#addCodeDown,#addCodeReturn', function () {
                var codeSelect = $(this).val();
                var list = [];
                var html = '';
                if ($(this).attr('id') == 'addCodeDown') {
                    var good = $.grep(listGoods, function (v) {
                        return (v.goodsCode == codeSelect || v.goodsPackageCode == codeSelect);
                    });
                    $('#listCodeDown tr').each(function () {
                        var that = this;
                        $.each(good, function (k, v) {
                            if ($(that).data('id') == v.goodsCode) check = false;
                        });

                    });
                } else {
                    var good = $.grep(listGoods, function (v) {
                        return (v.goodsCode == codeSelect);
                    })[0];
                }
                if ($(this).attr('id') == 'addCodeReturn') {
                    var good = $.grep(listGoods, function (v) {
                        return (v.goodsCode == codeSelect || v.goodsPackageCode == codeSelect);
                    });
                    $('#listCodeReturn tr').each(function () {
                        var that = this;
                        $.each(good, function (k, v) {
                            if ($(that).data('id') == v.goodsCode) check = false;
                        });
                    });
                } else {
                    var good = $.grep(listGoods, function (v) {
                        return (v.goodsCode == codeSelect);
                    })[0];
                }
                if (good.length == 0) Message('Mã không hợp lệ');
                var check = true;
                if ($(this).attr('id') == 'addCode')
                    $('#listCodeUp tr').each(function () {
                        if ($(this).data('id') == good.goodsCode) check = false;
                    });
                if ($(this).attr('id') == 'addToPackage')
                    $('#listCodePackage tr').each(function () {
                        if ($(this).data('id') == good.goodsCode) check = false;
                        else list.push($(this).data('id'));
                    });
                if ($(this).attr('id') == 'addCodeShip')
                    $('#listCodeShip tr').each(function () {
                        if ($(this).data('id') == good.goodsCode) check = false;
                    });

                if (good) {
                    if (check) {
                        var number = 8;
                        if (($(this).attr('id') == 'addCodeDown' || $(this).attr('id') == 'addCodeReturn') && good.length >= 1) {
                            $.each(good, function (k, v) {
                                html += genarateHtmlTable(v);
                            });
                            $('.quantity').val(parseInt($('.quantity').val()) + good.length);
                        } else {
                            html += genarateHtmlTable(good);

                            list = [];
                            $('.vandon:checked').each(function () {
                                list.push($(this).attr('value'));
                            });
                            list.push(codeSelect);
                            $('.quantity').val(parseInt($('.quantity').val()) + 1);
                        }
                        if ($(this).attr('id') == 'addCode') {
                            $('#listCodeUp').append(html);
                            $('#listGood').val(JSON.stringify(list));
                            $('#addCode').val('');
                            $('#addCode').focus();
                        }
                        if ($(this).attr('id') == 'addToPackage') {
                            $('#listCodePackage').append(html);
                            $('#js-input-goods-list').val(JSON.stringify(list));
                            $('#addToPackage').val('');
                            $('#addToPackage').focus();
                        }
                        if ($(this).attr('id') == 'addCodeShip') {
                            list = [];
                            $('.giaohang:checked').each(function () {
                                list.push($(this).attr('value'));
                            });
                            if (list.length > 0) $('#listGiaohang').val(JSON.stringify(list));
                            $('#listCodeShip').append(html);
                            $('#addCodeShip').val('');
                            $('#addCodeShip').focus();
                        }
                        if ($(this).attr('id') == 'addCodeDown') {
                            list = [];
                            $('.inputxuongxe:checked').each(function () {
                                list.push($(this).attr('value'));
                            });
                            if (good.length > 1) list.push(good[0].goodsPackageCode);
                            if (list.length > 0) $('#goodsPackageCodeXuongxe').val(JSON.stringify(list));
                            list = [];
                            $('.khonggom:checked').each(function () {
                                list.push($(this).attr('value'));
                            });
                            if (good.length == 1) list.push(good[0].goodsCode);
                            if (list.length > 0) $('#goodsCodeXuongxe').val(JSON.stringify(list));
                            $('#listCodeDown').append(html);
                            $('#addCodeDown').val('');
                            $('#addCodeDown').focus();
                        }
                        if ($(this).attr('id') == 'addCodeReturn') {
                            list = [];
                            $('.inputxuongxe:checked').each(function () {
                                list.push($(this).attr('value'));
                            });
                            if (good.length > 1) list.push(good[0].goodsPackageCode);
                            if (list.length > 0) $('#goodsPackageCodeReturn').val(JSON.stringify(list));
                            list = [];
                            $('.khonggom:checked').each(function () {
                                list.push($(this).attr('value'));
                            });
                            if (good.length == 1) list.push(good[0].goodsCode);
                            if (list.length > 0) $('#goodsCodeReturn').val(JSON.stringify(list));
                            $('#listCodeReturn').append(html);
                            $('#addCodeReturn').val('');
                            $('#addCodeReturn').focus();
                        }
                        Message('Đã thêm mã ');
                    }
                    else {
                        if ($(this).attr('id') != 'addToPackage') Message('Mã  đã được chọn');
                        else {
                            var goods = $.grep(listGoods, function (v) {
                                return (list.indexOf(v.goodsCode) != -1);
                            });
                            html += titleTable();
                            $.each(goods, function (k, v) {
                                html += genarateHtmlTable(v);
                            });
                            $('.quantity').val(parseInt($('.quantity').val()) - 1);
                            $('#listCodePackage').html(html);
                            $('#js-input-goods-list').val(JSON.stringify(list));
                            $('#addToPackage').val('');
                            $('#addToPackage').focus();

                            Message('Đã bỏ vận đơn ra ');
                        }
                    }
                } else {
                    Message('Mã không tồn tại');
                }

            });
            $('body').on('click', '#vandon,#giaohang,#duaxuong,#phuchoi', function () {
                $('#uptoTrip').submit(false);
                $('#deliverForm').submit(false);
                $('#upToTrip3').submit(false);
                $('#returnPackage').submit(false);
                var html = titleTable(), leng = 0, ob;
                $('#addCode').val('');
                $('#addCodeShip').val('');
                $('#addCodeReturn').val('');
                if ($(this).attr('id') == 'vandon') ob = $('.vandon:checked');
                if ($(this).attr('id') == 'giaohang') ob = $('.giaohang:checked');
                if ($(this).attr('id') == 'duaxuong') ob = $('.khonggom:checked');
                if ($(this).attr('id') == 'phuchoi') ob = $('.khonggom:checked');
                $(ob).each(function () {
                    var codeSelect = $(this).val();
                    var good = $.grep(listGoods, function (v) {
                        return (v.goodsCode == codeSelect);
                    })[0];
                    if (good) {
                        html += genarateHtmlTable(good);
                    }
                });
                if ($(this).attr('id') != 'vandon' && $(this).attr('id') != 'giaohang')
                    $('.check_all3:checked').each(function () {

                        var packageSelect = $(this).val();
                        var listgood = $.grep(listGoods, function (v) {
                            return (v.goodsPackageCode == packageSelect);
                        });
                        leng += listgood.length;
                        if (listgood) {
                            $.each(listgood, function (k, good) {
                                html += genarateHtmlTable(good);
                            });

                        }
                    });
                if ($(this).attr('id') == 'giaohang') {
                    $('#listCodeShip').html(html);
                    $('#shipGood').modal('show');
                    $('.quantity').val($('.giaohang:checked').length);
                }
                if ($(this).attr('id') == 'duaxuong') {
                    $('#listCodeDown').html(html);
                    $('#goodsDown').modal('show');
                    $('.quantity').val($('.khonggom:checked').length + leng);
                }
                if ($(this).attr('id') == 'phuchoi') {
                    $('#listCodeReturn').html(html);
                    $('#modal_phuchoi').modal('show');
                    $('.quantity').val($('.khonggom:checked').length + leng);
                }
                if ($(this).attr('id') == 'vandon') {
                    $('.quantity').val($('.vandon:checked').length);
                    $('#listCodeUp').html(html);
                    $.ajax({
                        url: '{{action('ShipNewController@getRoute')}}',
                        data: {
                            listPointId: $('#listPointId').val()
                        },
                        success: function (result) {
                            if (result.length > 0) {
                                var html = ' <option value="">-- Chọn --</option>';
                                $.each(result, function (k, v) {
                                    html += '<option value="' + v.routeId + '">' + v.routeName + '</option>';
                                });
                                $('#routeId').html(html);
                            } else Message('Không có tuyến phù hợp cho tất cả các bến');
                        }
                    });

                    $('#uptoTrip').modal('show');
                }
            });
            $('body').on('keyup', '#uptoTrip', function (e) {
                $('#addCode').focus();
            });
            $('body').on('keyup', '#submitCreate,#submitCreateNew', function (e) {
                var length = $('.requireName').length - 1;
                if (e.keyCode == 13)
                    if ($($('.requireName')[length]).val() != '' && $($('.changePrice')[length]).val() != '' && $($('.requirereceiverPhone')[length]).val() != '') {
                        $('#submitCreate').submit(true);
                        $('#submitCreateNew').submit(true);
                        $('#submitCreate').submit();
                        $('#submitCreateNew').submit();
                        // $(document.getElementById('submitCreate')).submit();
                        $('#load').attr('style', '    margin-left: 40%; position: fixed; margin-top: 200px;z-index: 10;');
                        $('#load').show();
                        $('#submitCreate').prop('disabled', true);
                        $('#submitCreateNew').prop('disabled', true);
                        $('button').prop('disabled', true);
                        $('#content,#submitCreateNew').css('opacity', '0.5');
                        Message('Vui lòng đợi trong giây lát!');
                    }
                //else $('#submitCreate').submit(false);
            });
            $('body').on('click', '#submitCreateNew button,#submit', function () {
                if ($($('.requireName')[length]).val() != '' && $($('.changePrice')[length]).val() != '' && $($('.requirereceiverPhone')[length]).val() != '') {
                    $('#submitCreate').submit();
                    $('#submitCreateNew').submit();
                    $('#load').attr('style', ' margin-left: 40%; position: fixed; margin-top: 200px;z-index: 10;');
                    $('#load').show();
                    $('#submitCreate').prop('disabled', true);
                    $('#submitCreateNew').prop('disabled', true);
                    $('button').prop('disabled', true);
                    $('#content,#submitCreateNew').css('opacity', '0.5');
                    Message('Vui lòng đợi trong giây lát!');
                }
            });
            $('body').on('change', '.goodType', function () {
                var code = $(this).val();
                var that = this;
                if (code != '')
                    $.ajax({
                        url: '{{action('ShipController@typeShipNew')}}',
                        data: {
                            //goodsTypeId: code
                        },
                        success: function (result) {
                            var data = $.grep(result, function (item) {
                                return item.goodsTypeId == code;
                            })[0];
                            priceType = data.goodsTypePrice;
                            var form = '';
                            if (newForm) form = 'New';
                            updatePrice(that, form);
                            // Message('Giá loại đồ mặc định đã cập nhật');
                        }
                    });
                else priceType = 0;
            });
            $('body').on('change', '#editGoodType,#editGoodTypeNew', function () {
                var code = $(this).val();
                var that = this;
                if (code != '')
                    $.ajax({
                        url: '{{action('ShipController@typeShipNew')}}',
                        data: {
                            //goodsTypeId: code
                        },
                        success: function (result) {
                            var data = $.grep(result, function (item) {
                                return item.goodsTypeId == code;
                            })[0];
                            priceType = data.goodsTypePrice;
                            var form;
                            if (($(that).attr('id')).includes('New')) form = 'New'; else form = '';
                            updateEditPrice(that, form);
                            // Message('Giá loại đồ mặc định đã cập nhật');
                        }
                    });
                else priceType = 0;
            });
            $('body').on('change', '.ajaxPhone', function () {
                var id = ($(this).attr('id')).substring(($(this).attr('id')).indexOf('_'), ($(this).attr('id')).length);
                var that = this;
                var receiverPhone = '', senderPhone = '';
                if ($(this).attr('id') == 'receiverPhone' + id) receiverPhone = $(this).val();
                if ($(this).attr('id') == 'senderPhone' + id) senderPhone = $(this).val();
                $.ajax({
                    url: '{{action('ShipNewController@getNameFromPhone')}}',
                    data: {
                        receiverPhone: receiverPhone,
                        senderPhone: senderPhone,
                    },
                    success: function (result) {
                        if (result.length > 0) {
                            if (($(that).attr('id') == 'receiverPhone' + id) && $('#receiver' + id).val() == '') $('#receiver' + id).val(result[0].receiver);
                            if (($(that).attr('id') == 'senderPhone' + id) && $('#sender' + id).val() == '') $('#sender' + id).val(result[0].sender);
                        }
                    }
                });
            });
            $('#js-btn-add-package').click(function () {
                index++;
                add();
            });

            $('body').on('change', '.checkMidway', function () {
                if ($(this).is(':checked')) $(this).val(1);
                else $(this).val(0);
            });
            $('body').on('click', '#js-step-1 tr', function () {
                if ($(this).attr('class') == '') $($('#js-step-1 tr')[$('tr').index(this) + 1]).removeClass('hide');
            });
            $('#js-btn-create-package').click(function () {
                $('#create-package').submit(false);
                let listGood = $('#js-step-1 input:checked');
                if (listGood.length > 0) {
                    // Message('Vui lòng đợi trong giây lát!');
                    {{--$.ajax({--}}
                    {{--url: '{{action('ShipNewController@getRoute')}}',--}}
                    {{--data: {--}}
                    {{--listPointId: $('#listPointId').val()--}}
                    {{--},--}}
                    {{--success: function (result) {--}}
                    {{--console.log(result);--}}
                    {{--if (result.length > 0) {--}}
                    {{--var html = ' <option value="">-- Chọn --</option>';--}}
                    {{--$.each(result, function (k, v) {--}}
                    {{--html += '<option value="' + v.routeId + '">' + v.routeName + '</option>';--}}
                    {{--});--}}
                    {{--$('#listRoute').html(html);--}}
                    {{--} else Message('Không có tuyến phù hợp cho tất cả các bến');--}}
                    {{--}--}}
                    {{--});--}}
                    $('#create-package').modal('show');
                    let listGoodsId = [];
                    listGood.each(function (index, item) {
                        listGoodsId.push($(item).val());
                    });
                    if (JSON !== undefined) {
                        $('#js-input-goods-list').val(JSON.stringify(listGoodsId));
                    } else {
                        alert("Trình duyệt không hỗ trợ JSON");
                    }

                    var html = titleTable();
                    $('#addToPackage').val('');
                    $('.vandon:checked').each(function () {
                        var codeSelect = $(this).val();
                        var good = $.grep(listGoods, function (v) {
                            return (v.goodsCode == codeSelect);
                        })[0];
                        if (good) {
                            html += genarateHtmlTable(good);
                        }
                    });
                    $('.quantity').val($('.vandon:checked').length);
                    $('#listCodePackage').html(html);
                } else {
                    alert('vui lòng chọn hàng cần gom');
                }
            });
            $('#pokePackage').change(function () {
                var html = '';
                var goodSelect = $(this).val();
                var good = $.grep(listGoods, function (v) {
                    return (v.goodsCode == goodSelect);
                })[0];
                var packageCodeSelected = $('.check_all2:checked:first').attr('value');
                var check = true;
                $('#listPokeToTrip tr').each(function () {
                    if (good != undefined && good.goodsCode != undefined)
                        if ($(this).data('id') == good.goodsCode) check = false;
                });
                if (good != undefined && (good.goodsPackageCode == undefined || good.goodsPackageCode != packageCodeSelected)) {
                    if (check) {
                        if ($('#pokeGoodToPackage tr').length == 0) html += (titleTable());
                        $('#listPokeToTrip').append(html + genarateHtmlTable(good));
                        var list = [];
                        $('#listPokeToTrip tr').each(function () {
                            if ($(this).data('id') != '' && $(this).data('id') != null) list.push($(this).data('id'));
                        });
                        $('#listPoke').val(JSON.stringify(list));
                        $('#packagePoke').attr('value', packageCodeSelected);
                    } else Message('Vận đơn đã được chọn');
                } else Message('Vận đơn đã có trong mã gom');
            });
            $('#addPackage').change(function () {
                var packageSelect = $(this).val();
                var listgood = $.grep(listGoods, function (v) {
                    return (v.goodsPackageCode == packageSelect);
                });
                var check = true;
                $('#listUpToTrip tr').each(function () {
                    if ($(this).data('id') == listgood[0].goodsPackageCode) check = false;
                });
                // alert(JSON.stringify(listgood));
                if (listgood) {
                    if (check) {
                        var html = titleTable();
                        var number = 8;
                        $.each(listgood, function (k, good) {
                            html += genarateHtmlTable(good);
                        });
                        $('#listUpToTrip').append(html);
                        var list = [];
                        $('.check_all2:checked').each(function () {
                            list.push($(this).attr('value'));
                        });
                        list.push(packageSelect);
                        $('.quantity').val(parseInt($('.quantity').val()) + 1);
                        $('.listPackageGood2').val(JSON.stringify(list));
                        Message('Đã thêm mã gom');
                    } else Message('Mã gom đã được chọn');
                } else {
                    Message('Mã gom không tồn tại');
                }
                $('#addPackage').val('');
                $('#addPackage').focus();
            });
            $('#aceptGoods').click(function () {
                //if (/[^A-Za-z0-9]/.test($($('.tripId2')[0]).val())) {
                $('#shipPackage').submit(false);
                $('#upToTrip2').submit(false);
                var html = titleTable();
                $('.check_all2:checked').each(function () {

                    var packageSelect = $(this).val();
                    var listgood = $.grep(listGoods, function (v) {
                        return (v.goodsPackageCode == packageSelect);
                    });
                    if (listgood) {
                        $.each(listgood, function (k, good) {
                            html += genarateHtmlTable(good);
                        });

                    }
                });
                $('#listUpToTrip').html(html);
                $('.quantity').val($('.check_all2:checked').length);
                if ($('.check_all2:checked:first').data('date') != '--') $('#calendar2').attr('value', $('.check_all2:checked:first').data('date'));
                else $('#calendar2').datepicker('setDate', 'today');
                if ($('#listPointId').val() != '') {
                    Message('Vui lòng đợi trong giây lát');
                    $.ajax({
                        url: '{{action('ShipNewController@getRoute')}}',
                        data: {
                            listPointId: $('#listPointId').val()
                        },
                        success: function (result) {
                            if (result.length > 0) {
                                var html = ' <option value="">-- Chọn --</option>';
                                $.each(result, function (k, v) {
                                    html += '<option value="' + v.routeId + '">' + v.routeName + '</option>';
                                });
                                $('#routeId2').html(html);
                                $('#routeId2 option').each(function () {
                                    if ($(this).attr('value') == $('.check_all2:checked:first').data('route')) {
                                        $(this).prop('selected', 'selected');
                                        updateSelectBoxTripVd($('#routeId2'));
                                    }

                                });
                            } else Message('Không có tuyến phù hợp cho tất cả các bến');
                        }
                    });
                }
                // $('#shipPackage').submit(true);

                //}
                $('#upToTrip2').modal('show');
            });
            $('#pokeGood').click(function () {
                $('#listPokeToTrip').html('');
                $('#packageCodeReset').val('');
                $('#goodsCodeMove').val('');
                $('#pokeGoodToPackage').modal('show');
            });
            $('body').on('focus', '#lenxe,#nhanlenxe,#gomhang,#gomlenxe,#btnGiaohang,#btnDown,#btnReturn,#pokeSubmit', function () {
                if ($(this).attr('id') == 'lenxe') document.getElementById('uptoTrip').submit(true);
                if ($(this).attr('id') == 'nhanlenxe') if ($('#routeId2').val() != '' && $('#scheduleId2').val() != '') document.getElementById('upToTrip2').submit(true);
                if ($(this).attr('id') == 'gomhang') document.getElementById('create-package').submit(true);
                if ($(this).attr('id') == 'gomlenxe') document.getElementById('up-package').submit(true);
                if ($(this).attr('id') == 'btnGiaohang') document.getElementById('deliverForm').submit(true);
                if ($(this).attr('id') == 'btnDown') document.getElementById('upToTrip3').submit(true);
                if ($(this).attr('id') == 'btnReturn') document.getElementById('returnPackage').submit(true);
                if ($(this).attr('id') == 'pokeSubmit') document.getElementById('pokeGoodToPackage').submit(true);
            });

            $('body').on('click', '.js-btn-print,#invandonlenxe,#invandon4', function () {
                $('#check_all_1').prop('checked', false);
                var check = 0;
                $('input.vandon').each(function () {
                    if (check > 0 && $(this).is(':checked')) $(this).prop("checked", false);
                    if ($(this).is(':checked'))
                        check++;
                });
                check = 0;
                $('input.giaohang').each(function () {
                    if (check > 0 && $(this).is(':checked')) $(this).prop("checked", false);
                    if ($(this).is(':checked'))
                        check++;
                });
                let goodsSelected = $('table input.vandon:checked:first');
                if (goodsSelected.length == 0) goodsSelected = $('table input.giaohang:checked:first');
                if (goodsSelected.length > 0) {
                    $('#biennhan').prop('checked', true);
                    $('#tem').prop('checked', true);
                    $('#actionprintNew').addClass('hide');
                    $('#actionPrint').removeClass('hide');
                    $('#question').modal('show');
                } else {
                    alert('vui lòng chọn vận đơn cần in');
                }
            });
            $('body').on('click', '#actionPrint', function () {
                let goodsSelected = $('table input.vandon:checked:first');
                if (goodsSelected.length == 0) goodsSelected = $('table input.giaohang:checked:first');
                $(".barCode").barcode($(goodsSelected).attr('value'), // Value barcode (dependent on the type of barcode)
                    "code128" // type (string)
                );
                let id = $(goodsSelected).val();
                let modalPrint = $('#divVanDon #print');
                $(modalPrint).html($('#form-print-' + id).html());
                $('#question').modal('hide');
                if ($('#biennhan').is(':checked')) {
                    if (companyA5.indexOf('{{session('companyId')}}') >= 0) {
                        $('#divVanDon .print_biennhan').attr('style', 'font-size: 15px;width: 202mm;height:140mm;border: 1px solid #000000;padding: 5px;margin: 5px;');
                        $('#divVanDon .print_biennhan .barCode').attr('style', 'transform: scale(1.2);margin-left: 96mm;margin-top: -3mm;width: 160px; height: 50px; float: left;');
                        $('#divVanDon .print_biennhan .datePrint').attr('style', 'text-align: right;font-size: 20px;');
                        $('#divVanDon .print_biennhan .titlePrint').attr('style', 'font-size: 20px;text-align: center; padding-top: 0mm;');

                    } else {
                        $('#divVanDon .print_biennhan .barCode').attr('style', 'margin-left: 50mm;     margin-top: -2mm; width: 130px;float: left; height: 50px;transform: scale(1.3)');
                        $('#img_logo').css('height', '19mm');
                    }
                    if ('{{session('companyId')}}' == 'TC04r1lru1vOk3c') {
                        $('.title_company').text('ĐỒNG HƯƠNG SÔNG LAM');
                        $('.note_company').html(note_DHSL);
                    }
                    $('#divVanDon .print_biennhan').printThis({
                        pageTitle: '&nbsp;'
                    });
                }
                if ($('#tem').is(':checked'))
                    $('#divVanDon .print_tem').printThis({
                        pageTitle: '&nbsp;'
                    });
                document.getElementById('js-btn-add-package').focus();
                $('#question').modal('hide');
            });
            $('body').on('click', '#actionprintNew', function () {
                print();
                $('#question').modal('hide');

            });
            $('body').on('click', '.js-btn-remove-goods', function () {
                let elem_tr = $(this).parents('tr:first').remove();
                if ($('.js-btn-remove-goods').length == 0) {
                    $('#submit').prop('disabled', true);
                    $('#js-step-1 tr').each(function () {
                        if ($(this).attr('id') != 'setWidth' && !$(this).hasClass('adding') && !$(this).hasClass('info'))
                            $(this).removeClass('hide');
                    });
                }
            });
            $('body').on('click', '#gomhang,#gomlenxe', function () {
                $('.tripId').val($('#listTrip option:selected').data('tripid'));
                $('.scheduleId').val($('#listTrip option:selected').val());
            });
            $('body').on('click', '#lenxe', function () {
                $('.tripId').val($('#scheduleId option:selected').data('tripid'));
            });
            $('#nhanlenxe').click(function () {
                $('.tripId2').val($('#scheduleId2 option:selected').data('tripid'));
                $('.scheduleId2').val($('#scheduleId2 option:selected').val());
            });
            $('#inhanglenxe').click(function () {
                $('#print3').html('');
                var html = '';
                if ($('.check_all3:checked').length > 0 || $('.khonggom:checked').length > 0) {
                    html += htmlUpTrip(':checked');
                } else {
                    html += htmlUpTrip('');
                }
                $('#print3').append(html);
                $('#printPackage3').printThis({
                    pageTitle: '&nbsp;'
                });

            });
            $('#invandon4').click(function () {

            });
            $('#inmagom').click(function () {
                if ($('.check_all2').length > 0) {
                    $(".barCode").barcode($('.check_all2:checked:first').val(), // Value barcode (dependent on the type of barcode)
                        "code128" // type (string)
                    );
                    $('#package').text('Mã gom :' + $('.check_all2:checked:first').val());
                    // var dem = 0;
                    // $('.check_all3:checked').each(function () {
                    //     if (dem != 0) $(this).prop('checked', false);
                    //     dem++;
                    // });
                    $('#print3').html('');
                    if ($('#tab3').hasClass('active')) {
                        $('.check_all2:checked').each(function () {
                            var id = $(this).attr('value');
                            $('#barCode_' + id).barcode($(this).val(), // Value barcode (dependent on the type of barcode)
                                "code128" // type (string)
                            );
                            $('#print3').append($('#' + id).html());
                        });
                    }
                    $('#print3').printThis({
                        pageTitle: '&nbsp;'
                    });
                } else {
                    Message('Không in mã vận đơn');
                }
            });

            $('body').on('change', '.pointUp,.pointDown', function () {
                if ($(this).hasClass('pointUp')) sessionStorage.setItem('up', $(this).val());
                if ($(this).hasClass('pointDown')) sessionStorage.setItem('drop', $(this).val());
            });
            $('body').on('keyup', '.select2-search__field', function () {
                var textid = ($($('.select2-results__options')[0]).attr('id'));
                var text2 = textid.substring(textid.indexOf('-') + 1, textid.lenght);
                var id = text2.substring(0, text2.indexOf('-'));
                var text = this.value.toUpperCase();
                // setInterval(function () {
                $('#' + textid + ' li').each(function () {
                    if ((($(this).text()).toUpperCase()).substring(0, text.length) != text)
                        $(this).hide();
                    else $(this).show();
                })
                // ,500});

            });
            $('body').on('click', 'span.select2-selection', function () {
                $('.select2-container--open').css('z-index', '999999');
            });
            $('body').on('focusin', '.validate_number', function () {
                var value = $(this).val();
                $(this).val(value.replace(/[^0-9]/gi, ''));
                checkfocus = 0;
            });
            $('body').on('focusout', '.validate_number', function () {
                var text = $(this).val();
                if (isNaN(text)) {
                    $(this).val(0);
                    Message('Giá tiền chỉ chấp nhận số');
                } else {
                    $(this).val($.number(text));
                }
                // else {
                //     var value;
                //     if (text < 1000 && text > 0) {
                //         value = 1000;
                //         Message('Tiền phải trên 1,000 VNĐ');
                //     }
                //     else value = (parseInt(text / 1000) * 1000);
                //     $(this).val('value', $.number(value));
                // }
            });

        });

        function init() {
            $('#filterRoute').select2();
            $('#js-step-1').tablesorter();
            $('#step-search').tablesorter();
            $('#step4').tablesorter();
            $('#step5').tablesorter();
            $('#convertLabel2').css('opacity', 0.7);
            $($('#filter option')[11]).prop('selected', true);
            if('{{session('companyId')}}'=='TC03k1IzZlYcm6I') $('#convertForm').hide();
            if ([1, 2, 5, 6].indexOf(sessionStorage.getItem('tab')) != -1) {
                var html = '', check_tabs_have_input_check = 0;
                if ([1, 6].indexOf(sessionStorage.getItem('tab')) != -1) check_tabs_have_input_check = 0;
                else check_tabs_have_input_check = 1;
                $.each(listSetting, function (key, setting) {
                    if (sessionStorage.getItem('tab') == 1 && [7, 16].indexOf(setting.id) != -1) check_tabs_have_input_check -= 1;
                    if (array_idFillter.indexOf(setting.id) != -1)
                        html += '<option value="' + (key + check_tabs_have_input_check) + '" >' + (setting.alias != '' ? setting.alias : setting.name) + '</option>';
                });
                $('#filter').html(html);
            }
            if (sessionStorage.getItem('tab') == 3 || sessionStorage.getItem('tab') == 4)
                $('#div_filter').hide();
            if (sessionStorage.getItem('tab') > 0) {
            } else sessionStorage.setItem('tab', 1);

            var t ={!! json_encode(session('print')) !!};
            // in sau khi thêm mã vận đơn
            if (t != null && t.error == undefined) {
                $('#biennhan').prop('checked', true);
                $('#tem').prop('checked', true);
                $('#question').modal('show');
            }
            if (!$('form template#new-package').length) $('button#submit').prop("disabled", true);

            $('button#aceptGoods').prop("disabled", true);
            $('button#inmagom').prop("disabled", true);
            $('button#pokeGood').prop("disabled", true);
            // $('button#inhanglenxe').prop("disabled", true);
            $('button#invandonlenxe').prop("disabled", true);
            $('button#duaxuong').prop("disabled", true);
            $('button#editGood').prop("disabled", true);
            $('button#deleteGood').prop("disabled", true);
            $('button#giaohang').prop("disabled", true);
            $('button#invandon4').prop("disabled", true);
            $('button#phuchoi').prop("disabled", true);
            $('#resetGood').prop('disabled', true);
            $('#editSaveDrop,#editSavePick').select2({
                dropdownCssClass: 'increasezindex'
            });
        }

        function htmlUpTrip(checked) {
            var html = '';
            if ($('#tab4').hasClass('active')) {
                $('.check_all3' + checked).each(function () {
                    var id = $(this).attr('value');
                    $('#barCode_' + id).barcode($(this).val(), // Value barcode (dependent on the type of barcode)
                        "code128" // type (string)
                    );
                    $('#print3').append($('#' + id).html());
                });
            }
            var list_same_trip = [], list_good_same_trip = [];
            $('.khonggom' + checked).each(function () {
                var id = $(this).attr('value');
                var list = $.grep(listGoods, function (goods) {
                    return goods.goodsCode == id;
                })[0];
                if (list_same_trip.indexOf(list.tripId) == -1) {
                    list_good_same_trip = $.grep(listGoods, function (goods) {
                        return goods.goodsPackageCode == undefined && goods.tripId == list.tripId && goods.goodsStatus == 3;
                    });
                    list_same_trip.push(list.tripId);
                }
                if (list_good_same_trip.length > 0) {
                    html += '<thead style="color: black;"><tr>' +
                        '                    <th colspan="7" style="text-align:left; width: 150px">Không gom - Tuyến:' + list.routeName +
                        ' - Lên xe: ' + getFormattedDate(list.timeline[2], '') + '- Chuyến: ' + (list.tripNumberPlate != undefined ? list.tripNumberPlate : '') + (list.tripStartDate != undefined ? ('-' + list.tripStartDate.substring(6, 8) + '/' + list.tripStartDate.substring(4, 6) + '/' + list.tripStartDate.substring(0, 4)) : '') + '</th>' +
                        ' </tr><tr>' +
                        '<th rowspan="' + (list_good_same_trip.length + 1) + '" style=" padding-left:10px"></th>' +
                        '                    <th class="center">Mã vận đơn</th>' +
                        '                    <th class="center">Hàng</th>' +
                        '                    <th class="center">Khách nhận</th>';
                    if ('{{session('companyId')}}' == 'TC04r1lru1vOk3c') {
                        html += '                    <th class="center">Nơi trả</th>' +
                            '                    <th class="center">Giá cước(VNĐ)</th>' +
                            '                    <th class="center">Giá trị hàng(VNĐ)</th>' +
                            '                </tr></thead><tbody>';
                    } else
                        html += '                    <th class="center">Nơi gửi</th>' +
                            '                    <th class="center">Nơi trả</th>' +
                            '                    <th class="center">Phải thu(VNĐ)</th>' +
                            '                </tr></thead><tbody>';
                }
                $.each(list_good_same_trip, function (k, v) {
                    html += '<tr><td></td>' +
                        '                            <td class="center">' + v.goodsCode + '</td>' +
                        '                            <td class="center">' + v.goodsName + '</td>' +
                        '                            <td class="center">' + (v.receiverPhone != undefined && (v.receiverPhone).trim() != '' ? v.receiverPhone : '') + (v.receiver != undefined && (v.receiver).trim() != '' ? '-' + v.receiver : '') + '</td>';
                    if ('{{session('companyId')}}' == 'TC04r1lru1vOk3c') {
                        html += '                            <td  class="center">' + ((v.dropOffPoint != undefined && (v.dropOffPoint).trim() != '') ? v.dropOffPoint : ((v.dropPointName != undefined && (v.dropPointName).trim() != '') ? v.dropPointName : '')) + '</td>' +
                            '                            <td class="center">' + (v.totalPrice != undefined ? formatNumber(v.totalPrice) : '') + '</td>' +
                            '                            <td class="center">' + (v.goodsValue != undefined && v.goodsValue > 0 ? formatNumber(v.goodsValue) : '') + '</td></tr>';
                    } else html +=
                        '                            <td class="center">' + ((v.pickUpPoint != undefined && (v.pickUpPoint).trim() != '') ? v.pickUpPoint : ((v.pickPointName != undefined && (v.pickPointName).trim() != '') ? v.pickPointName : '')) + '</td>' +
                        '                            <td class="center">' + ((v.dropOffPoint != undefined && (v.dropOffPoint).trim() != '') ? v.dropOffPoint : ((v.dropPointName != undefined && (v.dropPointName).trim() != '') ? v.dropPointName : '')) + '</td>' +
                        '                            <td class="center">' + v.unpaid + '</td>' +
                        '                        </tr>';

                });
                list_good_same_trip = [];
            });
            html += '</tbody>';
            return html;
        }

        function fillDataEdit(form, good) {
            var dem = 0, date;
            $('.vandon:checked').each(function () {
                if (dem != 0) $(this).prop('checked', false);
                else date = $(this).data('date');
                dem++;
            });
            $('#codeEdit' + form).val(good.goodsId);
            $('#editName' + form).val(good.goodsName);
            $('#editValue' + form).val(good.goodsValue != undefined ? $.number(good.goodsValue) : '');
            $('#editDate' + form).attr('value', date);
            var html = '<option>Chọn bến</option>';
            if (good.dropPointId != undefined) {
                $.each(listPoint, function (k, v) {
                    html += '<option value="' + v.pointId + '"' + (good.dropPointId == v.pointId ? 'selected' : '') + '>' + v.pointName + '</option>';
                });
                $('#editSaveDrop' + form).html(html);
                html = '<option>Chọn bến</option>';
            }
            if (good.pickPointId != undefined) {
                $.each(listPoint, function (k, v) {
                    html += '<option value="' + v.pointId + '"' + (good.pickPointId == v.pointId ? 'selected' : '') + '>' + v.pointName + '</option>';
                });
                $('#editSavePick' + form).html(html);
            }
            $('#editGoodType' + form + ' option').each(function () {
                if ($(this).attr('value') == good.goodsTypeId) $(this).prop('selected', true);
            });
            $('#editNumber' + form).val(good.quantity);
            $('#editPrice' + form).val($.number(good.price));
            $('#edit_price_buffer').val(good.price);
            $('#editExtraPeice' + form).val($.number(good.extraPrice));
            $('#edit_extraPrice_buffer').val(good.extraPrice);
            $('#editNoteExtra' + form).val(good.extraPriceNote);
            $('#editNote' + form).val(good.notice);
            $('#editDropMidway' + form).prop('checked', good.dropMidway);
            $('#editPlaceReceive' + form).val(good.dropOffPoint);
            $('#editPhoneReceive' + form).val(good.receiverPhone);
            $('#editReceive' + form).val(good.receiver);
            $('#editPlaceSend' + form).val(good.pickUpPoint);
            $('#editPhoneSend' + form).val(good.senderPhone);
            $('#editSender' + form).val(good.sender);
            $('#editTotal' + form).val($.number(good.totalPrice));
            $('#edit_total_buffer').val(good.totalPrice);
            $('#editPaid' + form).val($.number(good.paid));
            $('#edit_paid_buffer').val(good.paid);
            $('#editUnPaid' + form).val($.number(good.unpaid));
            $('#edit_unPaid_buffer').val(good.unpaid);
            $('.imgGoodEdit').attr('src', (good.imageURL != undefined && good.imageURL != '') ? good.imageURL : '/public/images/Anhguido1.png');
            $('#edit' + form).modal('show');
            console.log(good);
        }

        function titleTable() {
            var html = ' <thead>' +
                '                        <th>Mã</th>' +
                '                        <th>Ngày tạo</th>' +
                '                        <th>Tên</th>' +
                '                        <th>SĐT gửi</th>' +
                '                        <th>SĐT nhận</th>' +
                '                        <th>Giá trị</th>' +
                '                        <th>Bến lên</th>' +
                '                        <th>Bến xuống</th>';
            if ($('#tab5').hasClass('active')) html += '<th>Ảnh nhận</th>';
            html += '</thead>';
            return html;
        }

        function genarateHtmlTable(good) {
            var number = 8, html = '';
            var date = new Date(good.sendDate);
            var style = 'width:' + 100 / number + '%';
            html += '<tr data-id="' + good.goodsCode + '" style="width: 100%;' + ($('#tab5').hasClass('active') ? ' line-height: 35px;' : '') + '">';
            html += '<td style="">' + good.goodsCode + '</td>';
            html += '<td style="' + style + '">' + date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + '</td>';
            html += '<td style="' + style + '">' + good.goodsName + '</td>';
            html += '<td style="' + style + '">' + good.senderPhone + '</td>';
            html += '<td style="' + style + '">' + good.receiverPhone + '</td>';
            html += '<td style="' + style + '" class="right">' + good.totalPrice + '</td>';
            html += '<td style="' + style + '">' + (good.pickPointName ? good.pickPointName : '') + '</td>';
            html += '<td style="' + style + '">' + (good.dropPointName ? good.dropPointName : '') + '</td>';
            if ($('#tab5').hasClass('active')) {
                html += '<td style="width: ' + 100 / number + '%;">' +
                    '<div class="frm_chonanh"><div class="chonanh"><img class="imgGood" src="' + (good.receiverImageURL != undefined && good.receiverImageURL != '' ? good.receiverImageURL : '/public/images/Anhguido1.png') +
                    '" class="' + (good.receiverImageURL != undefined && good.receiverImageURL != '' ? 'zoom' : '') + '" style="margin-top: -12px;">' +
                    '<input type="file" name="img" class="selectedFile" style="display: none;" data-id="' + good.goodsId + '">' +
                    '                                <input type="hidden" class="listImage" name="listImage[]" value="">' +
                    '</div></div></td></tr>';
            }
            return html;
        }

        function gennerateFormNewAdd() {
            var html = '';
            var array_for_good = [], array_for_sender = [], array_for_reciever = [];
            var array_id_good = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 21, 22, 23, 24, 25];
            var array_id_sender = [17, 18, 19, 20];
            var array_id_reciever = [12, 13, 14, 15, 16];
            var listBreak = [];
            $.each(listSetting, function (k, v) {
                if (array_id_good.indexOf(v.id) >= 0) {
                    if (v.id != 7 && v.id != 22) array_for_good.push(v);
                    else listBreak.push(v);
                } else if (array_id_sender.indexOf(v.id) >= 0) array_for_sender.push(v);
                else if (array_id_reciever.indexOf(v.id) >= 0) array_for_reciever.push(v);
            });
            $.each(listBreak, function (k, v) {
                array_for_good.push(v);
            });
            html += '<fieldset class="divGood"><legend>Hàng hóa</legend>';
            $.each(array_for_good, function (k, v) {
                var nameAttr;
                if ([0, 4, 8, 12, 16].indexOf(k) >= 0) html += '<div class="row-fluid">';
                html += '<div class="control-group smallLine span3">';
                if ((v.alias).trim('') != '') nameAttr = v.alias; else nameAttr = v.name;
                // if ([6, 9, 10, 23, 24, 25].indexOf(v.id) >= 0) {
                //     nameAttr.replace('VNĐ').replace('VND').replace('(').replace(')');
                //     nameAttr += '(nghìn VNĐ)';
                // }
                html += '<label class="control-label">' + nameAttr + '</label><div class="controls">';
                switch (v.id) {
                    case 2:
                        html += '<input type="text" placeholder="Không bắt buộc" value="" name="goodsCode[]" pattern="[A-Za-z0-9-]{1,20}" title="Chỉ nhập số và chữ không dấu"/>';
                        break;
                    case 3:
                        html += '<input  id="dateNew" data-value="{{date('Y-m-d')}}" type="date" name="sendDate[]" autocomplete="off" class="sendDate datepicker" value="{{date('Y-m-d')}}">';
                        break;
                    case 4:
                        html += '<select name="goodsTypeId[]" class="goodType" id="typeNew">' +
                            '<option value="">-- Chọn --</option>';
                        $.each(listType, function (k1, v1) {
                            html += '<option value="' + v1.goodsTypeId + '">' + v1.goodsTypeName + '</option>';
                        });
                        html += '</select>';
                        break;
                    case 5:
                        html += '<input type="text" class="requireName" name="goodsName[]" required >';
                        break;
                    case 6:
                        html += '<input type="text" min="0" class="right" name="goodsValue[]" value="0" >';
                        break;
                    case 7:
                        html += '<div class="frm_chonanh" ><div class="chonanh"><img class="imgGood" src="/public/images/Anhguido1.png"></div>' +
                            '<input type="file" name="img" class="selectedFile" style="display: none;">' +
                            '<input type="hidden" class="listImage" name="listImage[]" value=""></div>';
                        break;
                    case 8:
                        html += '<input id="numberNew" class="span11 changePrice right updateDate" style="width: 50px;" type="number" min="1" value="1" name="quantity[]" required >';
                        break;
                    case 9:
                        html += '<input id="priceNew" class="changePrice right updateDate validate_number" type="text" min="1"  value="0" required >';
                        break;
                    case 10:
                        html += '<input id="extraPriceNew" class="changePrice right extraPrice validate_number" type="text" min="0" value="0">';
                        break;
                    case 11:
                        html += '<input type="text" value="" class="extraPriceNote" id="extraPriceNoteNew" name="extraPriceNote[]">';
                        break;
                    case 21:
                        html += '<input type="text" value="" name="notice[]">';
                        break;
                    case 22:
                        html += '<div class="checker" style="width: 70px;"><label style="padding-right: 47px"><input type="checkbox"  class="checkMidway" value="0" name="dropMidway[]"></label></div>';
                        break;
                    case 23:
                        html += '<input id="totalPriceNew"  class="changePrice right updateDate validate_number" type="text" min="0" value="0"  required>';
                        break;
                    case 24:
                        html += '<input id="paidNew" class="changePrice right updateDate validate_number" type="text" min="0" value="0">';
                        break;
                    case 25:
                        html += '<input id="unpaidNew" class="changePrice right updateDate validate_number" type="text" min="0" value="0">';
                        break;
                }
                html += '</div></div>';
                if ([3, 7, 11, 15].indexOf(k) >= 0 || k == (array_for_good.length - 1)) html += '</div>';
            });
            html += '</fieldset><div style="margin-top: 2px" class="row-fluid">' +
                '<fieldset class="span6 divSender"><legend>Khách gửi</legend><div>';
            $.each(array_for_sender, function (k, v) {
                var nameAttr;
                if ([0, 2, 4].indexOf(k) >= 0) html += '<div class="row-fluid">';
                html += ' <div class="control-group smallLine span6">';
                if ((v.alias).trim('') != '') nameAttr = v.alias; else nameAttr = v.name;
                // if ([6, 9, 10, 23, 24, 25].indexOf(v.id) >= 0) {
                //     nameAttr.replace('VNĐ').replace('VND').replace('(').replace(')');
                //     nameAttr += '(nghìn VNĐ)';
                // }
                html += '<label class="control-label">' + nameAttr + '</label><div class="controls">';
                switch (v.id) {
                    case 17:
                        html += '<select class="pointUp" id="pickUpSavePointNew" name="pickUpSavePoint[]" >' +
                            '<option value=""></option>';
                        $.each(listPoint, function (k3, v3) {
                            html += '<option value="' + v3.pointId + '"' + (sessionStorage.getItem('up') && sessionStorage.getItem('up') == v3.pointId ? 'selected' : '') + ' >' + v3.pointName + '</option>';
                        });
                        html += '</select>';
                        break;
                    case 18:
                        html += '<input type="text"  name="pickUpPoint[]">';
                        break;
                    case 19:
                        html += '<input type="text" value="" id="senderPhoneNew" class="ajaxPhone" name="senderPhone[]">';
                        break;
                    case 20:
                        html += '<input type="text" value="" id="senderNew" name="sender[]">';
                        break;
                }
                html += '</div></div>';
                if ([1, 3, 5].indexOf(k) >= 0 || k == array_for_sender.length - 1) html += '</div>';
            });
            html += '</div> </fieldset><fieldset class="span6 divReciever"><legend>Trả khách</legend>';

            $.each(array_for_reciever, function (k, v) {
                var nameAttr;
                if ([0, 2, 4].indexOf(k) >= 0) html += '<div class="row-fluid">';
                html += '<div class="control-group smallLine span6">';
                if ((v.alias).trim('') != '') nameAttr = v.alias; else nameAttr = v.name;
                // if ([6, 9, 10, 23, 24, 25].indexOf(v.id) >= 0) {
                //     nameAttr.replace('VNĐ').replace('VND').replace('(').replace(')');
                //     nameAttr += '(nghìn VNĐ)';
                // }
                html += '<label class="control-label">' + nameAttr + '</label><div class="controls">';
                switch (v.id) {
                    case 12:
                        html += '<select  class="pointDown" id="dropOffSavePointNew" name="dropOffSavePoint[]">' +
                            ' <option value=""></option>';
                        $.each(listPoint, function (k2, v2) {
                            html += '<option value="' + v2.pointId + '"' + (sessionStorage.getItem('drop') && sessionStorage.getItem('drop') == v2.pointId ? 'selected' : '') + '>' + v2.pointName + '</option>';
                        });
                        html += '</select>';
                        break;
                    case 13:
                        html += '<input type="text"  name="dropOffPoint[]"/>';
                        break;
                    case 14:
                        html += '<input type="number" id="receiverPhoneNew" class="ajaxPhone requirereceiverPhone updateDate"  name="receiverPhone[]" required>';
                        break;
                    case 15:
                        html += '<input type="text" value="" id="receiverNew" name="receiver[]" >';
                        break;
                    case 16:
                        html += '<div class="frm_chonanh"><div class="chonanh">' +
                            '<img class="imgGood" src="/public/images/Anhguido1.png"></div>' +
                            '<input type="file" name="img" class="selectedFile" style="display: none;">' +
                            '<input type="hidden" class="listImage" name="listUserImage[]" value=""></div>';
                        break;
                }
                html += '</div></div>';
                if ([1, 3, 5].indexOf(k) >= 0 || k == array_for_reciever.length - 1) html += '</div>';
            });
            html += '</fieldset></div>';
            return html;
        }

        function gennerateFormNewEdit() {
            var html = '';
            var array_for_good = [], array_for_sender = [], array_for_reciever = [];
            var array_id_good = [3, 4, 5, 6, 7, 8, 9, 10, 11, 21, 22, 23, 24, 25];
            var array_id_sender = [17, 18, 19, 20];
            var array_id_reciever = [12, 13, 14, 15];
            var listBreak = [];
            $.each(listSetting, function (k, v) {
                if (array_id_good.indexOf(v.id) >= 0) {
                    if (v.id != 7 && v.id != 22) array_for_good.push(v);
                    else listBreak.push(v);
                } else if (array_id_sender.indexOf(v.id) >= 0) array_for_sender.push(v);
                else if (array_id_reciever.indexOf(v.id) >= 0) array_for_reciever.push(v);
            });
            $.each(listBreak, function (k, v) {
                array_for_good.push(v);
            });
            html += '<fieldset class="divGood"><legend>Hàng hóa</legend>';
            $.each(array_for_good, function (k, v) {
                var nameAttr;
                if ([0, 4, 8, 12, 16].indexOf(k) >= 0) html += '<div class="row-fluid">';
                html += '<div class="control-group smallLine span3">';
                if ((v.alias).trim('') != '') nameAttr = v.alias; else nameAttr = v.name;
                if ([6, 9, 10, 23, 24, 25].indexOf(v.id) >= 0 && !nameAttr.includes('VNĐ') && !nameAttr.includes('VND')) nameAttr += '(VNĐ)';
                html += '<label class="control-label">' + nameAttr + '</label>';
                html += '<div class="controls">';
                switch (v.id) {
                    case 8:
                        html += '<input type="number" id="editNumberNew" name="number" class="editPrice">';
                        break;
                    case 9:
                        html += '<input type="text" class="editPrice right validate_number" id="editPriceNew" required>';
                        break;
                    case 10:
                        html += '<input type="text" class="editPrice right validate_number" id="editExtraPeiceNew">';
                        break;
                    case 5:
                        html += '<input type="text" id="editNameNew" name="editName" required>';
                        break;
                    case 6:
                        html += '<input type="text" id="editValueNew" class="right validate_number" name="editValue" value="0">';
                        break;
                    case 7:
                        html += ' <div class="frm_chonanh" style="margin-top: 10px;"><div class="chonanh right" style="margin-top: -5px;margin-right: 20px"><img' +
                            '        class="imgGoodEdit" src="/public/images/Anhguido1.png"></div><input type="file" name="imgEdit" class="selectedFile"' +
                            '            style="display: none;"><input type="hidden" class="listImage" name="listImage" value=""></div>';
                        break;
                    case 3:
                        html += '<input autocomplete="off" id="editDateNew" type="text" name="editDate" class="datepicker span9" title="Từ ngày" value="">';
                        break;
                    case 4:
                        html += '<select id="editGoodTypeNew" name="goodType"><option value=""></option>';
                        $.each(listType, function (k1, v1) {
                            html += '<option value="' + v1.goodsTypeId + '">' + v1.goodsTypeName + '</option>';
                        });
                        html += '</select>';
                        break;
                    case 11:
                        html += '<input type="text" id="editNoteExtraNew" name="editNoteExtra">';
                        break;
                    case 21:
                        html += '<input type="text" id="editNoteNew" name="editNote">';
                        break;
                    case 22:
                        html += '<input type="checkbox" id="editDropMidwayNew" name="editDropMidway"><label style="margin-left: 5px;" for="editDropMidway"></label>';
                        break;
                    case 23:
                        html += '<input type="text" id="editTotalNew" class="editPrice right validate_number" required>';
                        break;
                    case 24:
                        html += '<input type="text" id="editPaidNew" class="editPrice right validate_number">';
                        break;
                    case 25:
                        html += '<input type="text" id="editUnPaidNew" class="editPrice right validate_number">';
                        break;
                }
                html += '</div></div>';
                if ([3, 7, 11, 15].indexOf(k) >= 0 || k == array_for_good.length - 1) html += '</div>';
            });
            html += '</fieldset><div style="margin-top: 2px" class="row-fluid">' +
                '<fieldset class="span6 divSender"><legend>Khách gửi</legend><div>';
            $.each(array_for_sender, function (k, v) {
                var nameAttr;
                if ([0, 2, 4].indexOf(k) >= 0) html += '<div class="row-fluid">';
                html += '<div class="control-group smallLine span6">';
                if ((v.alias).trim('') != '') nameAttr = v.alias; else nameAttr = v.name;
                if ([6, 9, 10, 23, 24, 25].indexOf(v.id) && !nameAttr.includes('VNĐ') && !nameAttr.includes('VND')) nameAttr += '(VNĐ)';
                html += '<label class="control-label">' + nameAttr + '</label><div class="controls">';
                switch (v.id) {
                    case 18:
                        html += '<input type="text" id="editPlaceSendNew" name="editPlaceSend">';
                        break;
                    case 17:
                        html += '<select id="editSavePickNew" name="editSavePick"><option>Chọn bến</option>';
                        $.each(listPoint, function (k2, v2) {
                            html += '<option value="' + v2.pointId + '">' + v2.pointName + '</option>';
                        });
                        html += '</select>';
                        break;
                    case 19:
                        html += '<input type="number" id="editPhoneSendNew" name="editPhoneSend">';
                        break;
                    case 20:
                        html += '<input type="text" id="editSenderNew" name="editSender">';
                        break;
                }
                html += '</div></div>';
                if ([1, 3, 5].indexOf(k) >= 0 || k == array_for_sender.length - 1) html += '</div>';
            });
            html += '</div> </fieldset><fieldset class="span6 divReciever"><legend>Trả khách</legend>';
            $.each(array_for_reciever, function (k, v) {
                var nameAttr;
                if ([0, 2, 4].indexOf(k) >= 0) html += '<div class="row-fluid">';
                html += '<div class="control-group smallLine span6">';
                if ((v.alias).trim('') != '') nameAttr = v.alias; else nameAttr = v.name;
                if ([6, 9, 10, 23, 24, 25].indexOf(v.id) >= 0 && !nameAttr.includes('VNĐ') && !nameAttr.includes('VND')) nameAttr += '(VNĐ)';
                html += '<label class="control-label">' + nameAttr + '</label>';
                html += '<div class="controls">';
                switch (v.id) {
                    case 13:
                        html += '<input type="text" id="editPlaceReceiveNew" name="editPlaceReceive">';
                        break;
                    case 12:
                        html += '<select  id="editSaveDropNew" name="editSaveDrop"><option>Chọn bến</option>';
                        $.each(listPoint, function (k3, v3) {
                            html += '<option value="' + v3.pointId + '">' + v3.pointName + '</option>';
                        });
                        html += '</select>';
                        break;
                    case 14:
                        html += '<input type="number" id="editPhoneReceiveNew" name="editPhoneReceive" required>';
                        break;
                    case 15:
                        html += '<input type="text" id="editReceiveNew" name="editReceive">';
                        break;

                }
                html += '</div></div>';
                if ([1, 3, 5].indexOf(k) >= 0 || k == array_for_reciever.length - 1) html += '</div>';
            });
            html += '</fieldset></div>';
            return html;
        }

        function add() {
            var html = '';
            $('button#submit').prop("disabled", false);
            if ('{{session('companyId')}}' == 'TC03k1IzZlYcm6I') {   // cho nhập đồng thời nhiều đồ
                Message('Nhập giá tiền với đơn vị nghìn đồng');
                $('th').each(function () {
                    var text = $(this).text();
                    if (!text.includes('Nghìn') && text.includes('VNĐ')) $(this).text(text.replace('VNĐ', 'Nghìn VNĐ'));
                });
                $('#js-step-1 tr').each(function () {
                    if ($(this).attr('id') != 'setWidth' && !$(this).hasClass('adding')) $(this).addClass('hide');
                    if ($(this).hasClass('info')) $(this).attr('style', 'display: none');
                });
                html += '<tr class="adding">';
                var colspan = 1;
                var stt = $.grep(listSetting, function (v) {
                    return v.id == 1;
                });
                if (stt.length > 0) colspan = 2;
                html += ' <td  class="center uniformjs" colspan="' + colspan + '"><button style="    margin-top: -12px;" class="btn js-btn-remove-goods" type="button"><i class="icon-remove"></i></button></td>';
                $.each(listSetting, function (k, v) {
                    if (v.id != 1) {
                        html += '<td>';
                        switch (v.id) {
                            case 2:
                                html += '<input type="text" placeholder="Không bắt buộc" value="" name="goodsCode[]" pattern="[A-Za-z0-9-]{1,20}" title="Chỉ nhập số và chữ không dấu"/>';
                                break;
                            case 3:
                                html += '<input  id="date_' + index + '" data-value="{{date('Y-m-d')}}" type="date" name="sendDate[]" autocomplete="off" class="sendDate datepicker" value="{{date('Y-m-d')}}">';
                                break;
                            case 4:
                                html += '<select name="goodsTypeId[]" class="goodType" id="type_' + index + '">' +
                                    '<option value="">-- Chọn --</option>';
                                $.each(listType, function (k1, v1) {
                                    html += '<option value="' + v1.goodsTypeId + '">' + v1.goodsTypeName + '</option>';
                                });
                                html += '</select>';
                                break;
                            case 5:
                                html += '<input type="text" class="requireName" name="goodsName[]" required ' + ($('#focus').length == 0 ? 'id="focus"' : '') + '/>';
                                break;
                            case 6:
                                html += '<input type="number" min="0" name="goodsValue[]" value="0" >';
                                break;
                            case 7:
                                html += '<div class="frm_chonanh" style="margin-top: 0px"><div class="chonanh"><img class="imgGood" src="/public/images/Anhguido1.png"></div>' +
                                    '<input type="file" name="img" class="selectedFile" style="display: none;">' +
                                    '<input type="hidden" class="listImage" name="listImage[]" value=""></div>';
                                break;
                            case 8:
                                html += '<input id="number_' + index + '" class="span11 changePrice right updateDate" style="width: 50px;" type="number" min="1" value="1" name="quantity[]" required/>';
                                break;
                            case 9:
                                html += '<input id="price_' + index + '" class="changePrice right updateDate" type="number" min="1" name="price[]" value="0" required/>';
                                break;
                            case 10:
                                html += '<input id="extraPrice_' + index + '" class="changePrice right extraPrice" type="number" min="0" value="0" name="extraPrice[]"/>';
                                break;
                            case 11:
                                html += '<input type="text" value="" class="extraPriceNote" id="extraPriceNote_' + index + '" name="extraPriceNote[]">';
                                break;
                            case 12:
                                html += '<select  class="pointDown" id="dropOffSavePoint' + index + '" name="dropOffSavePoint[]">' +
                                    ' <option value=""></option>';
                                $.each(listPoint, function (k2, v2) {
                                    html += '<option value="' + v2.pointId + '"' + (sessionStorage.getItem('drop') && sessionStorage.getItem('drop') == v2.pointId ? 'selected' : '') + '>' + v2.pointName + '</option>';
                                });
                                html += '</select>';
                                break;
                            case 13:
                                html += '<input type="text"  name="dropOffPoint[]"/>';
                                break;
                            case 14:
                                html += '<input type="number" id="receiverPhone_' + index + '" class="ajaxPhone requirereceiverPhone updateDate"  name="receiverPhone[]" required/>';
                                break;
                            case 15:
                                html += '<input type="text" value="" id="receiver_' + index + '" name="receiver[]" />';
                                break;
                            case 16:
                                html += '<div class="frm_chonanh" style="margin-top: 0px"><div class="chonanh">' +
                                    '<img class="imgGood" src="/public/images/Anhguido1.png"></div>' +
                                    '<input type="file" name="img" class="selectedFile" style="display: none;">' +
                                    '<input type="hidden" class="listImage" name="listUserImage[]" value=""></div>';
                                break;
                            case 17:
                                html += '<select class="pointUp" id="pickUpSavePoint' + index + '" name="pickUpSavePoint[]" >' +
                                    '<option value=""></option>';
                                $.each(listPoint, function (k3, v3) {
                                    html += '<option value="' + v3.pointId + '"' + (sessionStorage.getItem('up') && sessionStorage.getItem('up') == v3.pointId ? 'selected' : '') + ' >' + v3.pointName + '</option>';
                                });
                                html += '</select>';
                                break;
                            case 18:
                                html += '<input type="text"  name="pickUpPoint[]"/>';
                                break;
                            case 19:
                                html += '<input type="text" value="" id="senderPhone_' + index + '" class="ajaxPhone" name="senderPhone[]"/>';
                                break;
                            case 20:
                                html += '<input type="text" value="" id="sender_' + index + '" name="sender[]"/>';
                                break;
                            case 21:
                                html += '<input type="text" value="" name="notice[]"/>';
                                break;
                            case 22:
                                html += '<div class="checker" style="width: 70px;"><label for=""><input type="checkbox"  class="checkMidway" value="0" name="dropMidway[]"></label></div>';
                                break;
                            case 23:
                                html += '<input id="totalPrice_' + index + '"  class=" changePrice right updateDate" type="number" min="0" value="0" required/>';
                                break;
                            case 24:
                                html += '<input id="paid_' + index + '" class="changePrice right updateDate" type="number" min="0" value="0"/>';
                                break;
                            case 25:
                                html += '<input id="unpaid_' + index + '" class="changePrice right updateDate" type="number" min="0" value="0"/>';
                                break;
                        }
                        html += '</td>';
                    }
                });
                html+='<input type="hidden" name="thousand[]" value="1000">';
                if ($('.adding').length == 0) {
                    var save = $("table#js-step-1>tbody").html();
                    $("table#js-step-1>tbody").html(html + save);
                } else $("table#js-step-1>tbody").append(html);
                $('#extraPriceNote_' + (index)).prop("disabled", true);
                $('#dropOffSavePoint' + index).attr('oninvalid', 'this.setCustomValidity("Nhập bến xuống")');
                $('#dropOffSavePoint' + index).attr('oninput', 'this.setCustomValidity("")');
                $('#pickUpSavePoint' + index).attr('oninvalid', 'this.setCustomValidity("Nhập bến lên")');
                $('#pickUpSavePoint' + index).attr('oninput', 'this.setCustomValidity("")');
                $('#dropOffSavePoint' + index + ',#pickUpSavePoint' + index).select2();
                if (document.getElementById('focus'))
                    document.getElementById('focus').focus();
            } else {
                if (!newForm) {
                    if (!document.getElementsByClassName('adding')[0]) {
                        // $('th').each(function () {
                        //     var text = $(this).text();
                        //     if (!text.includes('Nghìn') && text.includes('VNĐ')) $(this).text(text.replace('VNĐ', 'Nghìn VNĐ'));
                        // });
                        if ($('#add_price_buffer').length > 0) {
                            document.getElementById('add_price_buffer').remove();
                            document.getElementById('add_extraPrice_buffer').remove();
                            document.getElementById('add_total_buffer').remove();
                            document.getElementById('add_paid_buffer').remove();
                            document.getElementById('add_unPaid_buffer').remove();
                        }
                        $('#js-step-1 tr').each(function () {
                            if ($(this).attr('id') != 'setWidth' && !$(this).hasClass('adding')) $(this).addClass('hide');
                            if ($(this).hasClass('info')) $(this).attr('style', 'display: none');
                        });
                        html += '<tr class="adding">';
                        html += '<input type="hidden" name="price[]" value="" id="add_price_buffer">';
                        html += '<input type="hidden" name="extraPrice[]" value="" id="add_extraPrice_buffer">';
                        html += '<input type="hidden" name="totalPrice[]" value="" id="add_total_buffer">';
                        html += '<input type="hidden" name="paid[]" value="" id="add_paid_buffer">';
                        html += '<input type="hidden" name="unpaid[]" value="" id="add_unPaid_buffer">';
                        var colspan = 1;
                        var stt = $.grep(listSetting, function (v) {
                            return v.id == 1;
                        });
                        if (stt.length > 0) colspan = 2;
                        html += ' <td  class="center uniformjs" colspan="' + colspan + '"><button style="    margin-top: -12px;" class="btn js-btn-remove-goods" type="button"><i class="icon-remove"></i></button></td>';
                        $.each(listSetting, function (k, v) {
                            if (v.id != 1) {
                                html += '<td>';
                                switch (v.id) {
                                    case 2:
                                        html += '<input type="text" placeholder="Không bắt buộc" value="" name="goodsCode[]" pattern="[A-Za-z0-9-]{1,20}" title="Chỉ nhập số và chữ không dấu"/>';
                                        break;
                                    case 3:
                                        html += '<input  id="date_' + index + '" data-value="{{date('Y-m-d')}}" type="date" name="sendDate[]" autocomplete="off" class="sendDate datepicker" value="{{date('Y-m-d')}}">';
                                        break;
                                    case 4:
                                        html += '<select name="goodsTypeId[]" class="goodType" id="type_' + index + '">' +
                                            '<option value="">-- Chọn --</option>';
                                        $.each(listType, function (k1, v1) {
                                            html += '<option value="' + v1.goodsTypeId + '">' + v1.goodsTypeName + '</option>';
                                        });
                                        html += '</select>';
                                        break;
                                    case 5:
                                        html += '<input type="text" class="requireName" name="goodsName[]" required ' + ($('#focus').length == 0 ? 'id="focus"' : '') + '/>';
                                        break;
                                    case 6:
                                        html += '<input type="text" min="0"  class="right validate_number" name="goodsValue[]" value="0" >';
                                        break;
                                    case 7:
                                        html += '<div class="frm_chonanh" style="margin-top: 0px"><div class="chonanh"><img class="imgGood" src="/public/images/Anhguido1.png"></div>' +
                                            '<input type="file" name="img" class="selectedFile" style="display: none;">' +
                                            '<input type="hidden" class="listImage" name="listImage[]" value=""></div>';
                                        break;
                                    case 8:
                                        html += '<input id="number_' + index + '" class="span11 changePrice right updateDate" style="width: 50px;" type="number" min="1" value="1" name="quantity[]" required/>';
                                        break;
                                    case 9:
                                        html += '<input id="price_' + index + '" class="changePrice right updateDate validate_number" type="text" min="1"  value="0" required/>';
                                        break;
                                    case 10:
                                        html += '<input id="extraPrice_' + index + '" class="changePrice right extraPrice validate_number" type="text" min="0" value="0" />';
                                        break;
                                    case 11:
                                        html += '<input type="text" value="" class="extraPriceNote" id="extraPriceNote_' + index + '" name="extraPriceNote[]">';
                                        break;
                                    case 12:
                                        html += '<select  class="pointDown" id="dropOffSavePoint' + index + '" name="dropOffSavePoint[]">' +
                                            ' <option value=""></option>';
                                        $.each(listPoint, function (k2, v2) {
                                            html += '<option value="' + v2.pointId + '"' + (sessionStorage.getItem('drop') && sessionStorage.getItem('drop') == v2.pointId ? 'selected' : '') + '>' + v2.pointName + '</option>';
                                        });
                                        html += '</select>';
                                        break;
                                    case 13:
                                        html += '<input type="text"  name="dropOffPoint[]"/>';
                                        break;
                                    case 14:
                                        html += '<input type="number" id="receiverPhone_' + index + '" class="ajaxPhone requirereceiverPhone updateDate"  name="receiverPhone[]" required/>';
                                        break;
                                    case 15:
                                        html += '<input type="text" value="" id="receiver_' + index + '" name="receiver[]" />';
                                        break;
                                    case 16:
                                        html += '<div class="frm_chonanh" style="margin-top: 0px"><div class="chonanh">' +
                                            '<img class="imgGood" src="/public/images/Anhguido1.png"></div>' +
                                            '<input type="file" name="img" class="selectedFile" style="display: none;">' +
                                            '<input type="hidden" class="listImage" name="listUserImage[]" value=""></div>';
                                        break;
                                    case 17:
                                        html += '<select class="pointUp" id="pickUpSavePoint' + index + '" name="pickUpSavePoint[]" >' +
                                            '<option value=""></option>';
                                        $.each(listPoint, function (k3, v3) {
                                            html += '<option value="' + v3.pointId + '"' + (sessionStorage.getItem('up') && sessionStorage.getItem('up') == v3.pointId ? 'selected' : '') + ' >' + v3.pointName + '</option>';
                                        });
                                        html += '</select>';
                                        break;
                                    case 18:
                                        html += '<input type="text"  name="pickUpPoint[]"/>';
                                        break;
                                    case 19:
                                        html += '<input type="text" value="" id="senderPhone_' + index + '" class="ajaxPhone" name="senderPhone[]"/>';
                                        break;
                                    case 20:
                                        html += '<input type="text" value="" id="sender_' + index + '" name="sender[]"/>';
                                        break;
                                    case 21:
                                        html += '<input type="text" value="" name="notice[]"/>';
                                        break;
                                    case 22:
                                        html += '<div class="checker" style="width: 70px;"><label for=""><input type="checkbox"  class="checkMidway" value="0" name="dropMidway[]"></label></div>';
                                        break;
                                    case 23:
                                        html += '<input id="totalPrice_' + index + '"  class=" changePrice right updateDate validate_number"  type="text" min="0" value="0" required/>';
                                        break;
                                    case 24:
                                        html += '<input id="paid_' + index + '" class="changePrice right updateDate validate_number" type="text" min="0" value="0"/>';
                                        break;
                                    case 25:
                                        html += '<input id="unpaid_' + index + '" class="changePrice right updateDate validate_number" type="text" min="0" value="0" />';
                                        break;
                                }
                                html += '</td>';
                            }
                        });
                        if ($('.adding').length == 0) {
                            var save = $("#js-step-1>tbody").html();
                            $("#js-step-1>tbody").html(html + save);
                        } else $("#js-step-1>tbody").append(html);
                        $('#extraPriceNote_' + (index)).prop("disabled", true);
                        $('#dropOffSavePoint' + index).attr('oninvalid', 'this.setCustomValidity("Nhập bến xuống")');
                        $('#dropOffSavePoint' + index).attr('oninput', 'this.setCustomValidity("")');
                        $('#pickUpSavePoint' + index).attr('oninvalid', 'this.setCustomValidity("Nhập bến lên")');
                        $('#pickUpSavePoint' + index).attr('oninput', 'this.setCustomValidity("")');
                        $('#dropOffSavePoint' + index + ',#pickUpSavePoint' + index).select2();
                        if (document.getElementById('focus'))
                            document.getElementById('focus').focus();
                    }
                }
                else {
                    if (htmlAdd == '') {
                        htmlAdd = gennerateFormNewAdd();
                        $('#contentCreateNew').html(htmlAdd);
                    }
                    if ($('#add_price_buffer').length == 0) {
                        $('#contentCreateNew').append('<input type="hidden" name="price[]" value="" id="add_price_buffer">');
                        $('#contentCreateNew').append('<input type="hidden" name="extraPrice[]" value="" id="add_extraPrice_buffer">');
                        $('#contentCreateNew').append('<input type="hidden" name="totalPrice[]" value="" id="add_total_buffer">');
                        $('#contentCreateNew').append('<input type="hidden" name="paid[]" value="" id="add_paid_buffer">');
                        $('#contentCreateNew').append('<input type="hidden" name="unpaid[]" value="" id="add_unPaid_buffer">');
                    }
                    $('#extraPriceNoteNew').prop("disabled", true);
                    $('#dropOffSavePointNew').attr('oninvalid', 'this.setCustomValidity("Nhập bến xuống")');
                    $('#dropOffSavePointNew').attr('oninput', 'this.setCustomValidity("")');
                    $('#pickUpSavePointNew').attr('oninvalid', 'this.setCustomValidity("Nhập bến lên")');
                    $('#pickUpSavePointNew').attr('oninput', 'this.setCustomValidity("")');
                    $('#pickUpSavePointNew').select2();
                    $('#dropOffSavePointNew').select2();
                    $('#createNew').modal('show');
                }
            }
            $('.requireName').attr('oninvalid', 'this.setCustomValidity("Tên hàng bắt buộc")');
            $('.requireName').attr('oninput', 'this.setCustomValidity("")');
            $('.changePrice').attr('oninvalid', 'this.setCustomValidity("Giá gửi hàng lớn hơn 0")');
            $('.changePrice').attr('oninput', 'this.setCustomValidity("")');
            $('.requirereceiverPhone').attr('oninvalid', 'this.setCustomValidity("SĐT người nhận bắt buộc")');
            $('.requirereceiverPhone').attr('oninput', 'this.setCustomValidity("")');
            $('.pickUpSavePoint').attr('oninvalid', 'this.setCustomValidity("Điểm lưu hàng nhận bắt buộc")');
            $('.pickUpSavePoint').attr('oninput', 'this.setCustomValidity("")');
            $('.js-btn-print').prop('disabled', true);
            $('#js-btn-create-package').prop('disabled', true);
            $('#vandon').prop('disabled', true);
            $('#deleteGood').prop('disabled', true);
            $('#editGood').prop('disabled', true);
            $($('.pointUp ').parent()).attr('style', 'padding-top: -12px;');
            $($('.dropOff ').parent()).attr('style', 'padding-top: -12px;');
        }

        function updateSelectBoxTrip() {
            let routeId = $(selectBoxRoute).val(),
                date = $(datepickerBox.datepicker()).val();
            $(selectBoxListTrip).html('<option value="">Đang tải dữ liệu...</option>');
            $.ajax({
                dataType: "json",
                url: urlSeachTrip,
                data: {
                    'routeId': '[' + routeId + ']',
                    'startDate': date,
                    'endDate': date
                },
                success: function (data) {
                    let html = '';
                    data.listSchedule.forEach(function (trip) {
                        html += '<option data-tripId="' + trip.tripId + '" value="' + trip.scheduleId + '">' +
                            trip.numberPlate + ' - ' +
                            getFormattedDate(trip.startTime, 'time') +
                            '</option>';
                    });
                    if (html !== '') {
                        html = '<option value="">Toàn bộ các chuyến</option>' + html;
                        $(selectBoxListTrip).html(html);
                        $('#cbbTripDestinationId').html(html);
                    } else {
                        $(selectBoxListTrip).html(' <option value="">Hiện tại không có chuyến</option>');
                    }

                },
                error: function () {
                    Message("Lỗi!", "Vui lòng tải lại trang", "");
                }
            })
        }

        function updateSelectBoxTripVd(that) {
            let routeId, date;
            if ($(that).attr('id') == 'calendar' || $(that).attr('id') == 'routeId') {
                routeId = $(boxRoute).val();
                date = $(dateBox.datepicker()).val();
                $(boxListTrip).html('<option value="">Đang tải dữ liệu...</option>');
            } else {
                routeId = $('#routeId2').val();
                date = $(($('#calendar2')).datepicker()).val();
                $('#scheduleId2').html('<option value="">Đang tải dữ liệu...</option>');
            }
            $.ajax({
                dataType: "json",
                url: urlSeachTrip,
                data: {
                    'routeId': '[' + routeId + ']',
                    'startDate': date,
                    'endDate': date
                },
                success: function (data) {
                    let html = '';
                    data.listSchedule.forEach(function (trip) {
                        html += '<option data-tripId="' + trip.tripId + '"' +
                            ($('.check_all2:checked:first').data('tripid') != '' && $('.check_all2:checked:first').data('tripid') == trip.tripId ? ' selected' : '') +
                            ' value="' + trip.scheduleId + '">' +
                            trip.numberPlate + ' - ' +
                            getFormattedDate(trip.startTime, 'time') +
                            '</option>';
                    });
                    if (html !== '') {
                        html = '<option value="">Toàn bộ các chuyến</option>' + html;
                        if ($(that).attr('id') == 'calendar' || $(that).attr('id') == 'routeId')
                            $(boxListTrip).html(html);
                        else $('#scheduleId2').html(html);
                        $('#cbbTripDestinationId').html(html);
                    } else {
                        if ($(that).attr('id') == 'calendar' || $(that).attr('id') == 'routeId')
                            $(boxListTrip).html(' <option value="">Hiện tại không có chuyến</option>');
                        else $('#scheduleId2').html(' <option value="">Hiện tại không có chuyến</option>');
                    }

                },
                error: function () {
                    Message("Lỗi!", "Vui lòng tải lại trang", "");
                }
            })
        }

        function updatePrice(that, form){
            // $('.validate_number').keyup();
            if('{{session('companyId')}}'!='TC03k1IzZlYcm6I') {
                var id;
                if (form == '') id = ($(that).attr('id')).substring(($(that).attr('id')).indexOf('_'), ($(that).attr('id')).length);
                else id = form;
                if ($(that).attr('id') == ('price' + id)) {
                    $('#paid' + id).val(0);
                    $('#unpaid' + id).val(0);
                    $('#totalPrice' + id).val(0);
                    $('#add_paid_buffer').val(0);
                    $('#add_unPaid_buffer').val(0);
                    $('#add_total_buffer').val(0);
                    $('#add_price_buffer').val($(that).val());
                } else {
                    if (($(that).attr('id')).includes('total')) $('#add_total_buffer').val($(that).val());
                    else if (($(that).attr('id')).includes('un')) $('#add_unPaid_buffer').val($(that).val());
                    else $('#add_paid_buffer').val($(that).val());
                }
                var paid = ($('#add_paid_buffer').val()) ? ($('#add_paid_buffer').val()) : 0;
                var totalPrice = ($('#add_total_buffer').val()) ? ($('#add_total_buffer').val()) : 0;
                var number = parseInt($('#number' + id).val()) ? parseInt($('#number' + id).val()) : 1;
                var price = ($('#add_price_buffer').val()) ? ($('#add_price_buffer').val()) : 0;
                var extraPrice = ($('#add_extraPrice_buffer').val()) ? ($('#add_extraPrice_buffer').val()) : 0;
                var unpaid = ($('#add_unPaid_buffer').val()) ? ($('#add_unPaid_buffer').val()) : 0;
                var totalChange;
                if (!($(that).attr('id')).includes('price') || price == 0)
                    totalChange = totalPrice;
                else {
                    if (parseInt(priceType) == 0) totalChange = number * (price) + extraPrice;
                    else totalChange = number * (parseInt(priceType)) + extraPrice;
                }
                if (priceType > 0) $('#price' + id).val($.number(priceType));
                if (totalChange == 0) totalChange = totalPrice;
                $('#add_total_buffer').val(totalChange);
                console.log(totalChange);
                if ($(that).attr('id') == ('totalPrice' + id)) {
                    $('#unpaid' + id).val(0);
                    $('#add_unPaid_buffer').val(0);
                    $('#paid' + id).val($.number(totalChange));
                    $('#add_paid_buffer').val(totalChange);
                } else if ($(that).attr('id') == ('price' + id)) {
                    $('#totalPrice' + id).val($.number(totalChange));
                    $('#unpaid' + id).val(0);
                    $('#add_unPaid_buffer').val(0);
                    $('#paid' + id).val($.number(totalChange));
                    $('#add_paid_buffer').val(totalChange);

                } else if ($(that).attr('id') == ('paid' + id)) {
                    $('#add_paid_buffer').val($(that).val());
                    $('#unpaid' + id).val(totalChange - $(that).val());
                    $('#add_unPaid_buffer').val(totalChange - $(that).val());
                } else if ($(that).attr('id') == ('unpaid' + id)) {
                    $('#add_unPaid_buffer').val($(that).val());
                    $('#paid' + id).val(totalChange - $(that).val());
                    $('#add_paid_buffer').val(totalChange - $(that).val());
                }
                priceType = 0;
            }else updatePriceForMultiGood(that,form);
        }

        function updatePriceForMultiGood(that,form){
            var id;
            if (form == '') id = ($(that).attr('id')).substring(($(that).attr('id')).indexOf('_'), ($(that).attr('id')).length);
            else id = form;
            var paid = parseInt($('#paid' + id).val()) ? parseInt($('#paid' + id).val()) : 0;
            var totalPrice = parseInt($('#totalPrice' + id).val()) ? parseInt($('#totalPrice' + id).val()) : 0;
            var number = parseInt($('#number' + id).val()) ? parseInt($('#number' + id).val()) : 1;
            var price = parseInt($('#price' + id).val()) ? parseInt($('#price' + id).val()) : 0;
            var extraPrice = parseInt($('#extraPrice' + id).val()) ? parseInt($('#extraPrice' + id).val()) : 0;
            var unpaid = parseInt($('#unpaid' + id).val()) ? parseInt($('#unpaid' + id).val()) : 0;
            var totalChange;
            if (($(that).attr('id')).includes('totalPrice') || price == 0)
                totalChange = parseInt($('#totalPrice' + id).val());
            else {
                if (parseInt(priceType) == 0) totalChange = number * (price) + extraPrice;
                else totalChange = number * (parseInt(priceType)) + extraPrice;
            }
            $('#totalPrice' + id).val(totalChange);
            if (priceType > 0) $('#price' + id).val(priceType);
            if (totalChange > 0) {
                if ($(event.target).attr('id') == ('paid' + id)) $('#unpaid' + id).val(totalChange - paid);
                else if ($(event.target).attr('id') == ('unpaid' + id)) $('#paid' + id).val(totalChange - unpaid);
                else {
                    $('#paid' + id).val(totalChange);
                    $('#unpaid' + id).val(0);
                }
                $('#unpaid' + id).attr('max', totalChange);
            } else {
                $('#unpaid' + id).val(0);
            }
            priceType = 0;
        }
        function updateEditPrice(that, form) {

            if ($(that).attr('id') == ('editPrice' + form)) {
                $('#editPaid' + form).val(0);
                $('#editUnPaid' + form).val(0);
                $('#editTotal' + form).val(0);
                $('#edit_paid_buffer').val(0);
                $('#edit_unPaid_buffer').val(0);
                $('#edit_total_buffer').val(0);
                $('#edit_price_buffer').val($(that).val());
            } else {
                if (($(that).attr('id')).includes('Total')) $('#edit_total_buffer').val($(that).val());
                else if (($(that).attr('id')).includes('Un')) $('#edit_unPaid_buffer').val($(that).val());
                else $('#edit_paid_buffer').val($(that).val());
            }
            var paid = ($('#edit_paid_buffer').val()) ? ($('#edit_paid_buffer').val()) : 0;
            var totalPrice = ($('#edit_total_buffer').val()) ? ($('#edit_total_buffer').val()) : 0;
            var number = parseInt($('#editNumber' + form).val()) ? parseInt($('#editNumber' + form).val()) : 1;
            var price = ($('#edit_price_buffer').val()) ? ($('#edit_price_buffer').val()) : 0;
            var extraPrice = ($('#edit_extraPrice_buffer').val()) ? ($('#edit_extraPrice_buffer').val()) : 0;
            var unpaid = ($('#edit_unPaid_buffer').val()) ? ($('#edit_unPaid_buffer').val()) : 0;
            var totalChange;
            if (!($(that).attr('id')).includes('editPrice') || price == 0)
                totalChange = totalPrice;
            else {
                if (parseInt(priceType) == 0) totalChange = number * (price) + extraPrice;
                else totalChange = number * (parseInt(priceType)) + extraPrice;
            }
            if (priceType > 0) $('#editPrice' + form).val($.number(priceType));
            $('#edit_total_buffer').val(totalChange);

            if ($(that).attr('id') == ('editTotal' + form)) {
                $('#editUnPaid' + form).val(0);
                $('#edit_unPaid_buffer').val(0);
                $('#editPaid' + form).val($.number(totalChange));
                $('#edit_paid_buffer').val(totalChange);

            } else if ($(that).attr('id') == ('editPrice' + form)) {
                $('editTotal' + form).val($.number(totalChange));
                $('#editUnPaid' + form).val(0);
                $('#edit_unPaid_buffer').val(0);
                $('#editPaid' + form).val($.number(totalChange));
                $('#edit_paid_buffer').val(totalChange);

            } else if ($(that).attr('id') == ('editPaid' + form)) {
                $('#edit_paid_buffer').val($(that).val());
                $('#editUnPaid' + form).val(totalChange - $(that).val());
                $('#edit_unPaid_buffer').val(totalChange - $(that).val());
            } else if ($(that).attr('id') == ('editUnPaid' + form)) {
                $('#edit_unPaid_buffer').val($(that).val());
                $('#editPaid' + form).val(totalChange - $(that).val());
                $('#edit_paid_buffer').val(totalChange - $(that).val());
            }
            priceType = 0;
        }

        function print() {
            var donhang =
                    {!! json_encode(session('print')) !!}
                    {{ session()->forget('print')}}
                    $(".barCode").barcode(donhang.goodsCode, // Value barcode (dependent on the type of barcode)
                        "code128" // type (string)
                    );
            $('#question').modal('hide');
            $('.returnpick').html((donhang.pickUpPoint && donhang.pickUpPoint.trim('') != '') ? donhang.pickUpPoint : (donhang.pickPointName != undefined ? donhang.pickPointName : ''));
            $('.returnsender').html(donhang.sender);
            $('.returnreceiver').html(donhang.receiver);
            $('.returngood').html(donhang.goodsName);
            $('.returngood').parent().append((donhang.goodsTypeName != undefined && donhang.goodsTypeName.trim('') != '' ? '(' + donhang.goodsTypeName + ')' : ''));
            $('.returnquantity').html(donhang.quantity);
            $('.returndrop').html((donhang.dropOffPoint && donhang.dropOffPoint.trim('') != '') ? donhang.dropOffPoint : (donhang.dropPointName != undefined ? donhang.dropPointName : ''));
            $('.returnsendphone').html(donhang.senderPhone);
            $('.returnreceiverphone').html(donhang.receiverPhone);
            // $('.returnprice').html(moneyFormat(donhang.goods.price));
            $('.returnunpaid').html(moneyFormat(donhang.unpaid));
            $('.returnpaid').html(moneyFormat(donhang.paid));
            $('.returntotalprice').html(moneyFormat(donhang.totalPrice));
            $('.returnnote').html((donhang.notice));
            if ($('#biennhan').is(':checked')) {
                if (companyA5.indexOf('{{session('companyId')}}') >= 0) {
                    $('.print_biennhan_new').attr('style', 'font-size: 15px;width: 202mm;height:140mm;border: 1px solid #000000;padding: 5px;margin: 5px;');
                    $('.print_biennhan_new .barCode').attr('style', 'transform: scale(1.2);margin-left: 96mm;margin-top: -3mm;;width: 160px; height: 50px; float: left;');
                    $('.print_biennhan_new .datePrint').attr('style', 'text-align: right;margin-right:5mm;font-size: 20px;');
                    $('.print_biennhan_new .titlePrint').attr('style', 'font-size: 20px;text-align: center; padding-top: 0mm;');

                } else {
                    $('.print_biennhan_new .barCode').attr('style', 'margin-left: 50mm;     margin-top: -2mm; width: 143px;float: left; height: 50px;transform: scale(1.3)');
                    $('#img_logo').css('height', '19mm');
                }
                if ('{{session('companyId')}}' == 'TC04r1lru1vOk3c') {
                    $('.title_company').text('ĐỒNG HƯƠNG SÔNG LAM');
                    $('.note_company').html(note_DHSL);
                }
                $('.print_biennhan_new').printThis({
                    pageTitle: '&nbsp;'
                });
            }
            if ($('#tem').is(':checked'))
                $('.print_tem_new').printThis({
                    pageTitle: '&nbsp;'
                });
            document.getElementById('js-btn-add-package').focus();
        }

        function filterFunction() {
            var input, filter, table, tr, td, i, html, jump_column = 0;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            $('.tab-pane').each(function () {
                if ($(this).hasClass('active')) {
                    table = this.querySelector('table');
                    var selected = $("#filter").prop('selectedIndex');
                    if ($(table).attr('id') != 'step-search') jump_column = 1;
                    $.each(listSetting, function (k, v) {
                        if ([7, 16].indexOf(v.id) != -1 && $('ul.row-merge>li.active>a').data('id') == 1) jump_column -= 1; //  bỏ qua ảnh trong tab search
                        if (array_idFillter.indexOf(v.id) > -1)
                            html += '<option value="' + (k + jump_column) + '">' + (v.alias != '' ? v.alias : v.name) + '</option>';
                    });
                    $('#filter').html(html);
                    $($('#filter option')[selected]).prop('selected', true);
                }
            });
            tr = table.getElementsByTagName("tr");
            if ($(table).attr('id') != 'step3' || $(table).attr('id') != 'step2')
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[$('#filter option:selected').attr('value')];
                    if (td) {
                        if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                            tr[i].style.display = "";
                        } else {
                            tr[i].style.display = "none";
                        }
                    }
                }
        }

        function formatNumber(num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
        }
    </script>
    <script src="/public/javascript/jquery.printPage.js" type="text/javascript"></script>
    <script src="/public/javascript/printThis.js" type="text/javascript"></script>
@endsection