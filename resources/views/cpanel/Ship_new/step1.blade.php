@php $max=0;@endphp
@foreach($listSetting as $getTilte)
    @php $max+=strlen($getTilte['alias']==''?$getTilte['name']:$getTilte['alias']);@endphp
@endforeach
@foreach($listGoods as $list)
    @php  $number=0;
        for($i=0;$i<count($listSetting);$i++)
            $number+=strlen(@$list[$listSetting[$i]['attribute']]);
           if($max<$number) $max=$number;
    @endphp
@endforeach
<form id="submitCreate" action="{{action('ShipNewController@createGoods')}}" enctype="multipart/form-data">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <div style="    padding-bottom: 200px;">
        <div class="row-fluid" style="overflow-x: scroll;">
            <table class="table table-hover table-vertical-center checkboxs tablesorter" id="js-step-1" cellspacing="1" style=" table-layout: auto;width: {{$max*10>1300?$max*10 .'px':'100%'}}">
                <thead style="">
                <tr id="setWidth">
                    <td style="width: 1%;" class="uniformjs center">
                        <div class="checker ">
                            <input type="checkbox" class="check_all check_all_vandon " id="check_all_1">
                            <label for="check_all_1"></label>
                        </div>
                    </td>
                    @foreach($listSetting as $setting)
                        @php
                            if(trim($setting['alias'],'') !='') $nameAttr=$setting['alias']; else $nameAttr=$setting['name'];
                            if(in_array($setting['id'],[6,9,10,23,24,25]) && strpos($nameAttr, 'VNĐ') == false && strpos($nameAttr, 'VND') == false) $nameAttr .='(VNĐ)';
                         $style='font-size:'.($setting['size']>0?$setting['size']:12).'px;font-weight:'.($setting['bold']==1?'bold':'');
                        @endphp
                        <th class="left" style="{{$style}}">{{$nameAttr}}</th>
                    @endforeach

                </tr>
                </thead>
                <tbody>
                @php $dem=0;@endphp
                @forelse($listGoods as $package)
                    <tr>
                        <td class="center uniformjs">
                            <div class="checker  ">
                                <input type="checkbox" class="vandon"
                                       data-date="{{date("d-m-Y",@$package['sendDate']/1000)}}"
                                       data-pick="{{@$package['pickPointId']}}" data-drop="{{@$package['dropPointId']}}"
                                       name="goodsIdList[]" id="check_1_{{$package['goodsCode']}}"
                                       value="{{$package['goodsCode']}}">
                                <label for="check_1_{{$package['goodsCode']}}"></label>
                            </div>
                        </td>
                        @foreach($listSetting as $setting)
                            @php
                                $style='font-size:'.$setting['size'].'px;font-weight:'.($setting['bold']==1?'bold':'');
                                  if($setting['id']==1) echo '<td class="center" style="'.$style.'">'.$dem.'</td>';
                                  else if($setting['id']==3) echo '<td class="left" style="'.$style.'">'.date("d-m-Y",@$package[$setting['attribute']]/1000).'</td>';
                                  else if(in_array($setting['id'],[6,8,9,10,23,24,25])) echo '<td class="right" style="'.$style.'">'.number_format(@$package[$setting['attribute']]).'</td>';
                                  else if(in_array($setting['id'],[7,16])) echo '<td><div class="frm_chonanh"><div class="chonanh"><img src="'.
                                              (isset($package[$setting['attribute']]) && $package[$setting['attribute']]!=''?$package[$setting['attribute']]:'/public/images/Anhguido1.png').
                                              '" class="'.(isset($package[$setting['attribute']]) && $package[$setting['attribute']]!=''?'zoom':'').'"></div></div></td>';
                                  else if($setting['id']!=22) echo '<td class="left" style="'.$style.'">'.@$package[$setting['attribute']].'</td>';
                                  else echo '<td class="center uniformjs"><div class="checker"><input class="ck-dropMidway"'.
                                             ((isset($package[$setting['attribute']])?$package[$setting['attribute']]:false)?'checked':'').
                                             ' type="checkbox" id="dropMidway_1_'.$package['goodsId'].'"/><label for="dropMidway_1_'.$package['goodsId'].'">
                                             </label></div></td>';
                            @endphp
                        @endforeach
                        @php $dem++;@endphp
                    </tr>
                    <tr class="info hide">
                        <td colspan="20">
                            @include('cpanel.Ship_new.timeline',['package'=>$package])
                            @include('cpanel.Ship_new.print',['package'=>$package])
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td class="center" colspan="20">Hiện tại không có dữ liệu</td>
                    </tr>
                @endforelse

                </tbody>
            </table>
        </div>
    </div>
    <div class=" m_top_15" style="bottom: 70px; right: 10px;position: fixed;">
        <div style="float: right;   margin-right: 20px;margin-left: 20px;width: 120px">
            <button {{isset($listGoods) && count($listGoods)>0?'':'disabled'}} id="js-btn-create-package"
                    class="btn btn-info btn-flat-full border"
                    type="button">
                GOM HÀNG
            </button>
        </div>
        <div style="float: right;width: 120px">
            <button {{isset($listGoods) && count($listGoods)>0?'':'disabled'}} class="js-btn-print btn btn-info btn-flat-full border"
                    type="button" title="in mã chọn đầu tiên">
                IN VẬN ĐƠN
            </button>
        </div>
        <div style="float: right;    margin-right: 20px;margin-left: 20px;width: 120px">
            <button {{isset($listGoods) && count($listGoods)>0?'':'disabled'}} id="submit"
                    class="btn btn-info btn-flat-full border" type="submit">
                LƯU
            </button>
        </div>
        <div style="float: right;width: 120px">
            <button id="js-btn-add-package" class="btn btn-info btn-flat-full border" type="button">
                THÊM
            </button>
        </div>

    </div>

    <input type="hidden" name="_token" value="{{csrf_token()}}">
</form>
<form action="{{action('ShipNewController@shipPackage')}}" method="post">
    <div class=" m_top_15" style="bottom: 30px; right: 10px;position: fixed;">
        <div style="float: right;width: 260px; padding-right: 20px;">
            <button {{isset($listGoods) && count($listGoods)>0?'':'disabled'}} id="vandon"
                    class="btn btn-info btn-flat-full border"
                    type="button">
                CHUYỂN LÊN XE
            </button>
        </div>
        <div style="float: right;   margin-right: 20px;margin-left: 20px;width: 120px">
            <button {{isset($listGoods) && count($listGoods)>0?'':'disabled'}} id="deleteGood"
                    class="btn btn-info btn-flat-full border"
                    type="button">
                XÓA
            </button>
        </div>
        <div style="float: right;margin-left: 20px;width: 120px">
            <button {{isset($listGoods) && count($listGoods)>0?'':'disabled'}} id="editGood"
                    class="btn btn-info btn-flat-full border"
                    type="button">
                SỬA
            </button>
        </div>
    </div>
    <div class="modal modal-custom hide fade" data-backdrop="static" id="uptoTrip"
         style="width: 60%; margin-left: -28%;">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>CHUYỂN LÊN XE</h3>
        </div>
        <div class="modal-body" style="max-height: 480px;overflow-y:auto;">
            <div class="row-fluid">
                <div class="span3">
                    <div class="control-group">
                        <label for="" class="control-label">Ngày xuất phát</label>
                        <div class="controls">
                            <div class="input-append">
                                <input autocomplete="off" type="text" name="timeCreate" class="datepicker span11"
                                       id="calendar"
                                       value="{{request('sendDateInDay',date('d-m-Y'))}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span1"></div>
                <div class="span3">
                    <div class="control-group">
                        <label for="" class="control-label">Tuyến</label>
                        <div class="controls">
                            <select name="routeId" id="routeId" required>
                                <option value="">-- Chọn --</option>
                                {{--@foreach($listRoute as $route)--}}
                                {{--<option value="{{@$route['routeId']}}">{{$route['routeName']}}</option>--}}
                                {{--@endforeach--}}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="span1"></div>
                <div class="span3">
                    <div class="control-group">
                        <label for="" class="control-label">Chuyến</label>
                        <div class="controls">
                            <select name="scheduleId" id="scheduleId" required>
                                <option value="">-- Chọn --</option>
                            </select>
                            <input type="hidden" name="tripId" class="tripId">
                            <input type="hidden" name="notSendTripExist" id="notSendTripExist" value="1">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span3">
                    <div class="control-group">
                        <label for="" class="control-label">Thêm vận đơn lên xe</label>
                        <div class="controls">
                            <div class="input-append">
                                <input autocomplete="off" type="text" class="span11" placeholder="Mã vận đơn"
                                       id="addCode" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span3">
                    <label>Số lượng</label>
                    <input type="number" disabled class="quantity" style="width: 50%;">
                </div>
            </div>
            <div class="row-fluid">
                <table style="width: 100%;display: inline-grid;">
                    <tbody id="listCodeUp">
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="row-fluid">
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn-warning btn-flat" id="lenxe">CHUYỂN LÊN XE</button>
            </div>
        </div>
    </div>
    <input type="hidden" id="listGood" name="listGood">
</form>
<form action="{{action('ShipNewController@createPackage')}}">
    <div class="modal modal-custom hide fade" data-backdrop="static" id="create-package"
         style="width: 60%; margin-left: -28%;">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>TẠO MÃ GOM</h3>
        </div>
        <div class="modal-body" style="max-height: 480px;overflow-y:auto;">
            <div class="row-fluid">
                <div class="span2">
                    <div class="control-group">
                        <label for="" class="control-label">Tên mã gom</label>
                        <div class="controls">
                            <input type="text" value="" style="width: 100%" name="goodsPackageName"
                                   id="createGoodsPackageName"/>
                        </div>
                    </div>
                </div>
                <div class="span2" style="margin-left: 50px">
                    <div class="control-group">
                        <label for="" class="control-label">Ngày xuất phát</label>
                        <div class="controls">
                            <div class="input-append">
                                <input autocomplete="off" type="text" name="timeCreate" class="span11 datepicker"
                                       id="txtCalendar" style="width: 100%"
                                       value="{{request('sendDateInDay',date('d-m-Y'))}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span3" style="    margin-left: 70px;">
                    <div class="control-group">
                        <label for="" class="control-label">Tuyến</label>
                        <div class="controls">
                            <select name="routeId" id="listRoute" style="width: 100%">
                                <option value="">-- Chọn --</option>
                                @foreach($listRoute as $route)
                                    <option value="{{@$route['routeId']}}">{{$route['routeName']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="span3">
                    <div class="control-group">
                        <label for="" class="control-label">Chuyến</label>
                        <div class="controls" style="width: 100%">
                            <select name="tripId" id="listTrip">
                                <option value="">-- Chọn --</option>
                            </select>
                            <input type="hidden" name="tripId" class="tripId">
                            <input type="hidden" name="scheduleId" class="scheduleId">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span2">
                    <div class="control-group">
                        <label for="" class="control-label">Thêm/bớt vận đơn</label>
                        <div class="controls">
                            <div class="input-append">
                                <input autocomplete="off" type="text" placeholder="Mã vận đơn" style="width: 112%"
                                       class="span11" id="addToPackage" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span3" style="margin-left: 6%">
                    <label>Số lượng </label>
                    <input type="number" disabled class="quantity" style="width: 50%;">
                </div>

            </div>
            <div class="row-fluid">
                <table style="width: 100%;display: inline-grid;">
                    <tbody id="listCodePackage">
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="goodsIdList" id="js-input-goods-list" value="">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="row-fluid">

                <a data-dismiss="modal" style="cursor:pointer;  " aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn-warning btn-flat" style="" id="gomhang">GOM HÀNG</button>
            </div>
        </div>
    </div>
</form>
<input type="hidden" id="listPointId" name="listPointId">
<script>
    jQuery(function ($) {
        $('#txtCalendar').datepicker({
            format: "dd-mm-yyyy",
            minDate: new Date('02-07-2018'),
            toggleActive: true
        });
    });
</script>