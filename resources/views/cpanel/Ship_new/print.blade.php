<div class="formhtml" id="form-print-{{$package['goodsCode']}}" style="display: none">
    <style>
        /*.print-box{*/
        /*border: 1px solid #000000;padding: 5px;margin: 5px;*/
        /*}*/
    </style>
    @php
        if(session('companyId') == 'TC04r1lru1vOk3c')  $typePaper='DHSL';
        else if(session('companyId')=='TC03k1IzZlYcm6I' || session('companyId')=='TC05wM7dDRQSo6') $typePaper=5;
        else $typePaper=6;
        $class_print='height_tr_a'.$typePaper;
    @endphp
    <div class="print_biennhan"
         style="width: 140mm;height:96mm;font-size:15px;border: 1px solid #000000;margin-left: 20px;margin-top: 20px">
        <table style="width: 100%;">
            <tr>
                <td>
                    <div>
                        <div style="width: 30%;float: left">
                            <img src="{{session('userLogin')['logo']}}" width="50%" height="30mm"style="    padding-left: 10mm;"></div>
                        <div class="title_company"></div>
                        <div class="barCode divRight" style="width: 30%;float: left;"></div>
                        {{--<div class="datePrint" style="text-align: right;margin-top:5mm; margin-right: 6mm">{{date('d \- m \- Y')}}. .</div>--}}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="titlePrint" style="font-size: 16px;text-align: center">
                        <strong>BIÊN NHẬN GỬI HÀNG HÓA</strong>
                    </div>
                </td>

            </tr>
            <tr class="{{$class_print}}">
                <td style="padding-top: 5mm;">
                    <div class="divLeft"> <div class="divLeft2">Nơi gửi:</div><strong class="printpick"
                                                         style="font-size: 18px;margin-left: 3mm">{{(isset($package['pickUpPoint'])&& trim($package['pickUpPoint']," ")!="")?$package['pickUpPoint']:((isset($package['pickPointName'])&&$package['pickPointName']!='')?$package['pickPointName']:'')}}</strong>
                    </div>
                    <div class="divRight"><div class="divLeft2">Nơi nhận:</div><strong class="printdrop"
                                                           style="font-size: 18px;margin-left: 3mm">{{(isset($package['dropOffPoint'])&& trim($package['dropOffPoint']," ")!="")?$package['dropOffPoint']:((isset($package['dropPointName'])&&$package['dropPointName']!='')?$package['dropPointName']:'')}}</strong>
                    </div>
                </td>
            </tr>
            <tr class="{{$class_print}}">
                <td>
                    <div class="divLeft"><div class="divLeft2">Người gửi:</div> <strong class="printsender"
                                                            style="margin-left: 3mm">{{@$package['sender']}}</strong>
                    </div>
                    <div class="divRight"><div class="divLeft2">Người nhận:</div><strong class="printreceiver"
                                                             style="margin-left: 3mm">{{@$package['receiver']}}</strong>
                    </div>
                </td>
            </tr>
            <tr class="{{$class_print}}">
                <td>
                    <div class="divLeft"><div class="divLeft2">SĐT gửi: </div><strong class="printsendphone"
                                                          style="margin-left: 3mm">{{@$package['senderPhone']}}</strong>
                    </div>
                    <div class="divRight"><div class="divLeft2">SĐT nhận:</div><strong class="printreceiverphone"
                                                           style="margin-left: 3mm">{{@$package['receiverPhone']}}</strong>
                    </div>
                </td>
            </tr>
            <tr class="{{$class_print}}">
                <td>
                    <div class="divLeft"><div class="divLeft2">Tên hàng gửi: </div><strong class="printgood"
                                                               style="margin-left: 3mm">{{@$package['goodsName']}}</strong>{{isset($package['goodsTypeName']) && $package['goodsTypeName']!=''?'('.$package['goodsTypeName'].')':''}}
                   </div>
                    <div class="divRight"><strong style="font-size: 16px;margin-left: 3mm" class="printprice"></strong>
                    </div>
                </td>
            </tr>
            <tr class="{{$class_print}}">
                <td>
                    <div class="divLeft"><div class="divLeft2">Số lượng:</div><strong style="font-size: 16px;margin-left: 3mm"
                                                          class="printquantity">{{@$package['quantity']}}</strong></div>
                    <div class="divRight"><div class="divLeft2">Tiền cước: </div><strong style="font-size: 16px;margin-left: 3mm"
                                                             class="printtotalprice">@moneyFormat(@$package['totalPrice'])</strong>
                    </div>
                </td>
            </tr>
            <tr class="{{$class_print}}">
                <td>
                    <div class="divLeft"><div class="divLeft2">Đã thanh toán :</div><strong style="font-size: 16px;margin-left: 3mm"
                                                                class="printpaid">@moneyFormat(@$package['paid'])</strong>
                    </div>
                        <div class="divRight"><div class="divLeft2">Còn lại:</div><strong style="font-size: 16px;margin-left: 3mm" class="printunpaid">@moneyFormat(@$package['unpaid'])</strong>
                    </div>
                </td>
            </tr>
            <tr class="{{$class_print}}">
                <td>
                    <div style="float: left;"><strong style="margin-left: 3mm">Lưu ý: {{@$package['notice']}}</strong>
                    </div>
                </td>
            </tr>
            @if(session('companyId') == 'TC04r1lru1vOk3c')
                <tr><td><div class="note_company"></div></td></tr>
            @endif
            <tr class="{{$class_print}}" style="line-height: 10mm;">
                <td>
                    <div class="divLeft" style="text-align: center">Nhân viên ký tên</div>
                    <div class="divRight" style="text-align: center">Khách hàng ký tên</div>
                </td>
            </tr>
            <tr class="{{$class_print}}">
                <td>
                    <div class="divLeft" style="text-align: center">.................</div>
                    <div class="divRight" style="text-align: center">.................</div>
                </td>
            </tr>
        </table>
    </div>
    <div class="print_tem" style="font-size: 10px;float:right">
        <div class="print-box" style="width: 72mm;height: 45mm; float: left;border: 0px solid #000000;">
            <div style="float: right;">{{date('d \- m \- Y')}}</div>
            <div style="margin-left: 15mm">
                <div class="barCode" style="padding-left: 15mm"></div>
            </div>
            <table style="width: 100%; margin-top: -3mm">
                <tr>
                    <td style="text-align: center;font-size: 14px;padding-top: 15px;"><strong>NHẬN TẠI XE</strong>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 2mm;">Nơi nhận: <strong style="font-size: 13px;"
                                                                    class="printdrop">{{(isset($package['dropOffPoint'])&& trim($package['dropOffPoint']," ")!="")?$package['dropOffPoint']:((isset($package['dropPointName'])&&$package['dropPointName']!='')?$package['dropPointName']:'')}}</strong>
                    </td>

                </tr>
                <tr>
                    <td>Người nhận: <strong style="font-size: 13px;"
                                            class="printreceiver">{{@$package['receiver']}}</strong></td>
                </tr>
                <tr>
                    <td>SĐT: <strong style="font-size: 13px;"
                                     class="printsendphone">{{@$package['receiverPhone']}}</strong></td>
                </tr>
                <tr>
                    <td><strong style="font-size: 13px;" class="printprice"></strong></td>
                </tr>
                <tr>
                    <td>Số lượng: <strong style="font-size: 13px;"
                                          class="printquantity">{{@$package['quantity']}}</strong></td>
                <tr>
                    <td>Tiền cước: <strong style="font-size: 13px;" class="printtotalprice">@moneyFormat(@$package['totalPrice'])</strong>
                    </td>
                </tr>
                </tr>
                <tr>
                    <td>Đã thanh toán: <strong style="font-size: 13px;" class="printpaid">@moneyFormat(@$package['paid'])</strong>
                    </td>
                </tr>
                <tr>
                    <td>Còn lại: <strong style="font-size: 13px;"
                                         class="printunpaid">@moneyFormat(@$package['unpaid'])</strong></td>
                </tr>
            </table>
        </div>
        <div style="width: 28mm;height:22mm;float: right;border: 0px solid #000000;" class="print-box">
            <table style="width: 100%">
                <tr>
                    <td>
                        <div style="margin-left: -7.3mm" id="onlyUnder{{@$package['goodsCode']}}">
                            <div class="barCode"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Đến: <strong style="font-size: 11px;"
                                     class="printdrop">{{(isset($package['dropOffPoint'])&& trim($package['dropOffPoint']," ")!="")?$package['dropOffPoint']:((isset($package['dropPointName'])&&$package['dropPointName']!='')?$package['dropPointName']:'')}}</strong>
                    </td>
                </tr>
                <tr>
                    <td>Hàng: <strong style="font-size: 11px;" class="printgood">{{@$package['goodsName']}}</strong>
                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 28mm;height:22mm;float: right;border: 0px solid #000000;" class="print-box">
            <table style="width: 100%">
                <tr>
                    <td>
                        <div style="margin-left: -7.3mm" class="barCode"></div>
                    </td>
                </tr>
                <tr>
                    <td>Đến: <strong style="font-size: 11px;"
                                     class="printdrop">{{(isset($package['dropOffPoint'])&& trim($package['dropOffPoint']," ")!="")?$package['dropOffPoint']:((isset($package['dropPointName'])&&$package['dropPointName']!='')?$package['dropPointName']:'')}}</strong>
                    </td>
                </tr>
                <tr>
                    <td>Hàng: <strong style="font-size: 11px;" class="printgood">{{@$package['goodsName']}}</strong>
                    </td>
                </tr>
            </table>
        </div>
    </div>

</div>
