@php $max=0;
 $listNotPackage=array_filter($listGoods, function ($list) {  return isset($list['goodsPackageCode']) == false;});
 $max=0;
@endphp
@foreach($listSetting as $getTilte)
    @php $max+=strlen($getTilte['alias']==''?$getTilte['name']:$getTilte['alias']);@endphp
@endforeach
@foreach($listGoods as $list)
    @php  $number=0;
        for($i=0;$i<count($listSetting);$i++)
            $number+=strlen(@$list[$listSetting[$i]['attribute']]);
           if($max<$number) $max=$number;
    @endphp
@endforeach
@php($listPackageGoods = [])
<form action="{{action('ShipNewController@arrivePackage')}}" method="post" id="upToTrip3">
    <div class="row-fluid" style="overflow-x: scroll;">
        <table class="table table-vertical-center checkboxs" id="step3"
               style="table-layout: auto;width: {{$max*10>1300?$max*10 .'px':'100%'}};min-width: 100%;">
            @foreach($listGoods as $packageGoods)
                @if(isset($packageGoods['goodsPackageCode']) && !in_array(@$packageGoods['goodsPackageCode'],$listPackageGoods) )
                    @php($listPackageGoods[]=(isset($packageGoods['goodsPackageCode'])?$packageGoods['goodsPackageCode']:''))
                    <thead style="background: #aaa">
                    <tr class="showtr3" data-code="{{$packageGoods['goodsPackageCode']}}">
                        <th class="uniformjs">
                            <div class="checker">
                                <input name="goodsPackage" value="{{$packageGoods['goodsPackageCode']}}"
                                       type="checkbox" class="check_all3 inputxuongxe" id="check_3_{{$loop->index}}">
                                <label for="check_3_{{$loop->index}}"></label>
                                @include('cpanel.Ship_new.printPackage',['listGoods'=>$listGoods,'packageCode23'=>$packageGoods['goodsPackageCode']])
                            </div>
                        </th>
                        <th colspan="24">Mã gom
                            : {{@$packageGoods['goodsPackageCode']}} {{isset($packageGoods['goodsPackageName'])?'-'.$packageGoods['goodsPackageName']:''}}
                            {{isset($packageGoods['routeName'])?' ___ Tuyến đường :'.$packageGoods['routeName']:''}}
                            {{isset($packageGoods['timeline'])?' ___ Lên xe :'.date("d-m-Y H:i:s",$packageGoods['timeline'][2]/1000):''}}
                            {{isset($packageGoods['tripNumberPlate'])?'___ Chuyến xe :'.$packageGoods['tripNumberPlate']:''}} {{isset($packageGoods['tripStartDate'])?('-'.substr(@$packageGoods['tripStartDate'],6,2).'/'.substr(@$packageGoods['tripStartDate'],4,2).'/'.substr(@$packageGoods['tripStartDate'],0,4)):''}}</th>
                    </tr>
                    </thead>
                    <thead style="">
                    <tr class="{{$packageGoods['goodsPackageCode']}} hide">
                        <th style="width: 50px"></th>
                        @foreach($listSetting as $setting)
                            @php
                                if(trim($setting['alias'],'') !='') $nameAttr=$setting['alias']; else $nameAttr=$setting['name'];
                                if(in_array($setting['id'],[6,9,10,23,24,25]) && strpos($nameAttr, 'VNĐ') == false && strpos($nameAttr, 'VND') == false) $nameAttr .='(VNĐ)';
                            $style='font-size:'.($setting['size']>0?$setting['size']:12).'px;font-weight:'.($setting['bold']==1?'bold':'');
                            @endphp
                            <th class="left" style="{{$style}}">{{$nameAttr}}</th>
                        @endforeach
                    </tr>
                    </thead>
                    @php $dem=0;@endphp
                    @foreach($listGoods as $package)
                        @if(isset($packageGoods['goodsPackageCode']) && isset($package['goodsPackageCode']) &&  $packageGoods['goodsPackageCode']==$package['goodsPackageCode'] )
                            <tr class="{{$package['goodsPackageCode']}} hide">
                                <td></td>
                                @foreach($listSetting as $setting)
                                    @php
                                        $style='font-size:'.$setting['size'].'px;font-weight:'.($setting['bold']==1?'bold':'');
                                               if($setting['id']==1) echo '<td style="'.$style.'">'.$dem.'</td>';
                                               else if($setting['id']==3) echo '<td class="left" style="'.$style.'">'.date("d-m-Y",@$package[$setting['attribute']]/1000).'</td>';
                                               else if(in_array($setting['id'],[6,8,9,10,23,24,25])) echo '<td class="right" style="'.$style.'">'.number_format(@$package[$setting['attribute']]).'</td>';
                                               else if(in_array($setting['id'],[7,16])) echo '<td><div class="frm_chonanh"><div class="chonanh"><img src="'.
                                                           (isset($package[$setting['attribute']]) && $package[$setting['attribute']]!=''?$package[$setting['attribute']]:'/public/images/Anhguido1.png').
                                                           '" class="'.(isset($package[$setting['attribute']]) && $package[$setting['attribute']]!=''?'zoom':'').'"></div></div></td>';
                                               else if($setting['id']!=22) echo '<td class="left" style="'.$style.'">'.@$package[$setting['attribute']].'</td>';
                                               else echo '<td class="center uniformjs"><div class="checker"><input class="ck-dropMidway"'.
                                                          ((isset($package[$setting['attribute']])?$package[$setting['attribute']]:false)?'checked':'').
                                                          ' type="checkbox" id="dropMidway_1_'.$package['goodsId'].'"/><label for="dropMidway_1_'.$package['goodsId'].'">
                                                          </label></div></td>';
                                    @endphp
                                @endforeach
                                @php $dem++;@endphp
                            </tr>
                            <tr class="info hide {{$package['goodsPackageCode']}}">
                                <td colspan="20">
                                    @include('cpanel.Ship_new.timeline',['package'=>$package])
                                </td>
                            </tr>
                        @endif
                    @endforeach
                @endif
            @endforeach
            @php $listCheck=[]; @endphp
            @forelse($listNotPackage as $packageGoods)
                @if(!in_array(@$packageGoods['tripId'],$listCheck))
                    <thead style="background: #aaa">
                    <tr class="showtr3" data-code="{{$packageGoods['routeId']}}">
                        <th colspan="1" class="uniformjs"></th>
                        <th colspan="24">Không Gom
                            ___ Tuyến Đường: {{isset($packageGoods['routeName'])?$packageGoods['routeName']:''}}
                            ___ Chuyến
                            Xe: {{isset($packageGoods['tripNumberPlate'])?$packageGoods['tripNumberPlate']:''}} {{isset($packageGoods['tripStartDate'])?('-'.substr(@$packageGoods['tripStartDate'],6,2).'/'.substr(@$packageGoods['tripStartDate'],4,2).'/'.substr(@$packageGoods['tripStartDate'],0,4)):''}}</th>
                    </tr>
                    </thead>
                    <thead style="">
                    <tr class="{{$packageGoods['tripId']}} hide">
                        <th style="width: 50px"></th>
                        @foreach($listSetting as $setting)
                            @php
                                if($setting['alias']!='') $nameAttr=$setting['alias']; else $nameAttr=$setting['name'];
                                if(in_array($setting['id'],[6,9,10,23,24,25]) && strpos($nameAttr, 'VNĐ') == false && strpos($nameAttr, 'VND') == false) $nameAttr .='(VNĐ)';
                             $style='font-size:'.$setting['size'].'px;font-weight:'.($setting['bold']==1?'bold':'');
                            @endphp
                            <th class="left" style="{{$style}}">{{$nameAttr}}</th>
                        @endforeach
                    </tr>
                    </thead>
                    @php $dem=0;@endphp
                    @foreach($listNotPackage as $package)
                        @if($packageGoods['tripId']==$package['tripId'])
                            <tr class="{{$packageGoods['routeId']}} hide">
                                <th colspan="1" class="uniformjs">
                                    <div class="checker" style="padding-left: 50px">
                                        <input name="goodsCode" value="{{@$package['goodsCode']}}"
                                               type="checkbox" class="check_all khonggom"
                                               id="check_3_{{$package['goodsCode']}}">
                                        <label for="check_3_{{$package['goodsCode']}}"></label>
                                    </div>
                                </th>
                                @foreach($listSetting as $setting)
                                    @php
                                        $style='font-size:'.$setting['size'].'px;font-weight:'.($setting['bold']==1?'bold':'');
                                               if($setting['id']==1) echo '<td style="'.$style.'">'.$dem.'</td>';
                                               else if($setting['id']==3) echo '<td class="left" style="'.$style.'">'.date("d-m-Y",@$package[$setting['attribute']]/1000).'</td>';
                                               else if(in_array($setting['id'],[6,8,9,10,23,24,25])) echo '<td class="right" style="'.$style.'">'.number_format(@$package[$setting['attribute']]).'</td>';
                                               else if(in_array($setting['id'],[7,16])) echo '<td><div class="frm_chonanh"><div class="chonanh"><img src="'.
                                                           (isset($package[$setting['attribute']]) && $package[$setting['attribute']]!=''?$package[$setting['attribute']]:'/public/images/Anhguido1.png').
                                                           '" class="'.(isset($package[$setting['attribute']]) && $package[$setting['attribute']]!=''?'zoom':'').'"></div></div></td>';
                                               else if($setting['id']!=22) echo '<td class="left" style="'.$style.'">'.@$package[$setting['attribute']].'</td>';
                                               else echo '<td class="center uniformjs"><div class="checker"><input class="ck-dropMidway"'.
                                                          ((isset($package[$setting['attribute']])?$package[$setting['attribute']]:false)?'checked':'').
                                                          ' type="checkbox" id="dropMidway_1_'.$package['goodsId'].'"/><label for="dropMidway_1_'.$package['goodsId'].'">
                                                          </label></div></td>';
                                    @endphp
                                @endforeach
                                @php $dem++;@endphp
                            </tr>
                            <tr class="info hide">
                                <td colspan="20">
                                    @include('cpanel.Ship_new.timeline',['package'=>$package])
                                    {{--@include('cpanel.Ship_new.print',['package'=>$package])--}}
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    @php $listCheck[]=@$packageGoods['tripId']; @endphp
                @endif

            @empty
                {{--<tr >--}}
                {{--<td class="center" colspan="15">Hiện tại không có dữ liệu</td>--}}
                {{--</tr>--}}

            @endforelse
        </table>
    </div>
    <div class="row-fluid m_top_15" style="bottom: 70px; position: fixed;">
        <div class="span2">
            <button {{isset($listGoods) && count($listGoods)>0?'':'style=display:none'}} class="btn btn-info btn-flat-full border"
                    type="button" id="duaxuong">ĐƯA HÀNG XUỐNG XE
            </button>
        </div>
        <div class="span2">
            <button {{isset($listGoods) && count($listGoods)>0?'':'style=display:none'}} class="btn btn-info btn-flat-full border"
                    type="button" id="inhanglenxe">IN DANH SÁCH
            </button>
            <input type="hidden" name="goodsPackageCode" id="goodsPackageCodeXuongxe">
            <input type="hidden" name="goodsCodeXuongxe" id="goodsCodeXuongxe">
        </div>
        {{--<div class="span2">--}}
        {{--<button {{isset($listGoods) && count($listGoods)>0?'':'style=display:none'}} class="btn btn-info btn-flat-full border"--}}
        {{--type="button" title="in mã chọn đầu tiên" id="invandonlenxe">IN VẬN ĐƠN--}}
        {{--</button>--}}
        {{--</div>--}}
        <div class="span2">
            <button {{isset($listGoods) && count($listGoods)>0?'':'style=display:none'}} class="btn btn-info btn-flat-full border"
                    type="button" title="in mã chọn đầu tiên" id="phuchoi">PHỤC HỒI
            </button>
        </div>
    </div>
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="modal modal-custom hide fade" data-backdrop="static" id="goodsDown"
         style="width: 53%; margin-left: -28%;">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>ĐƯA HÀNG XUỐNG XE</h3>
        </div>
        <div class="modal-body" style="max-height: 480px;overflow-y:auto;">
            <div class="row-fluid">
                <div class="span3">
                    <div class="control-group">
                        <label for="" class="control-label">Giao thêm vận đơn</label>
                        <div class="controls">
                            <div class="input-append">
                                <input autocomplete="off" type="text" class="span11" placeholder="Mã vận đơn/gom"
                                       id="addCodeDown" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span3">
                    <label>Số lượng</label>
                    <input type="number" disabled class="quantity" style="width: 50%;">
                </div>
            </div>
            <div class="row-fluid">
                <table style="width: 100%;display: inline-grid;">
                    <tbody id="listCodeDown">
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="row-fluid">
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn-warning btn-flat" type="submit" id="btnDown">XUỐNG XE</button>
            </div>
        </div>
    </div>
</form>
<form action="{{action('ShipNewController@returnPackage')}}" id="returnPackage">
    <div class="modal modal-custom hide fade" data-backdrop="static" id="modal_phuchoi"
         style="width: 53%; margin-left: -28%;">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>PHỤC HỒI HÀNG NHẦM CHUYẾN</h3>
        </div>
        <div class="modal-body" style="max-height: 480px;overflow-y:auto;">
            <div class="row-fluid">
                <div class="span3">
                    <div class="control-group">
                        <label for="" class="control-label">Thêm hàng</label>
                        <div class="controls">
                            <div class="input-append">
                                <input autocomplete="off" type="text" class="span11" placeholder="Mã vận đơn/gom"
                                       id="addCodeReturn" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span3">
                    <label>Số lượng</label>
                    <input type="number" disabled class="quantity" style="width: 50%;">
                </div>
            </div>
            <div class="row-fluid">
                <table style="width: 100%;display: inline-grid;">
                    <tbody id="listCodeReturn">
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="goodsPackageCodeReturn" id="goodsPackageCodeReturn">
            <input type="hidden" name="goodsCodeReturn" id="goodsCodeReturn">
            <div class="row-fluid">
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn-warning btn-flat" type="submit" id="btnReturn">PHỤC HỒI</button>
            </div>
        </div>
    </div>
</form>