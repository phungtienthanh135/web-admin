@php $max=0;@endphp
@foreach($listSetting as $getTilte)
    @php $max+=strlen($getTilte['alias']==''?$getTilte['name']:$getTilte['alias']);@endphp
@endforeach
@foreach($listGoods as $list)
    @php  $number=0;
        for($i=0;$i<count($listSetting);$i++)
            $number+=strlen(@$list[$listSetting[$i]['attribute']]);
           if($max<$number) $max=$number;
    @endphp
@endforeach
<div class="row-fluid" style="overflow-x: scroll;">
    <table class="table table-hover table-vertical-center checkboxs tablesorter" id="step5" style=" table-layout: auto;width: {{$max*10>1300?$max*10 .'px':'100%'}};">
        <thead style="">
        <tr>
            <th style="width: 1%;" class="uniformjs">
                {{--<div class="checker">--}}
                {{--<input type="checkbox" class="check_all" id="check_all_5">--}}
                {{--<label for="check_all_5"></label>--}}
                {{--</div>--}}
            </th>
            {{--<th style="width: 1%;" class="center">#</th>--}}
            @foreach($listSetting as $setting)
                @php
                    if(trim($setting['alias'],'') !='') $nameAttr=$setting['alias']; else $nameAttr=$setting['name'];
                    if(in_array($setting['id'],[6,9,10,23,24,25]) && strpos($nameAttr, 'VNĐ') == false && strpos($nameAttr, 'VND') == false) $nameAttr .='(VNĐ)';
                 $style='font-size:'.($setting['size']>0?$setting['size']:12).'px;font-weight:'.($setting['bold']==1?'bold':'');
                @endphp
                <th class="left" style="{{$style}}">{{$nameAttr}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @php $dem=0;@endphp
        @forelse($listGoods as $package)
            <tr>
                <td class="center uniformjs"></td>
                @foreach($listSetting as $setting)
                    @php
                        $style='font-size:'.$setting['size'].'px;font-weight:'.($setting['bold']==1?'bold':'');
                           if($setting['id']==1) echo'<td class="center" style="'.$style.'">'.$dem.'</td>';
                           else if($setting['id']==3) echo '<td class="left" style="'.$style.'">'.date("d-m-Y",@$package[$setting['attribute']]/1000).'</td>';
                          else if(in_array($setting['id'],[6,8,9,10,23,24,25])) echo '<td class="right" style="'.$style.'">'.number_format(@$package[$setting['attribute']]).'</td>';
                          else if(in_array($setting['id'],[7,16])) echo '<td><div class="frm_chonanh"><div class="chonanh"><img src="'.
                                      (isset($package[$setting['attribute']]) && $package[$setting['attribute']]!=''?$package[$setting['attribute']]:'/public/images/Anhguido1.png').
                                      '" class="'.(isset($package[$setting['attribute']]) && $package[$setting['attribute']]!=''?'zoom':'').'"></div></div></td>';
                          else if($setting['id']!=22) echo '<td class="left" style="'.$style.'">'.@$package[$setting['attribute']].'</td>';
                          else echo '<td class="center uniformjs"><div class="checker"><input class="ck-dropMidway"'.
                                     ((isset($package[$setting['attribute']])?$package[$setting['attribute']]:false)?'checked':'').
                                     ' type="checkbox" id="dropMidway_1_'.$package['goodsId'].'"/><label for="dropMidway_1_'.$package['goodsId'].'">
                                     </label></div></td>';
                    @endphp
                @endforeach
                @php $dem++;@endphp
            </tr>
            <tr class="info hide">
                <td colspan="20">
                    @include('cpanel.Ship_new.timeline',['package'=>$package])
                </td>
            </tr>
        @empty
            <tr>
                <td class="center" colspan="15">Hiện tại không có dữ liệu</td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>