@php $max=0;@endphp
@foreach($listSetting as $getTilte)
@php $max+=strlen($getTilte['alias']==''?$getTilte['name']:$getTilte['alias']);@endphp
@endforeach
@foreach($listGoods as $list)
    @php  $number=0;
        for($i=0;$i<count($listSetting);$i++)
            $number+=strlen(@$list[$listSetting[$i]['attribute']]);
           if($max<$number) $max=$number;
    @endphp
@endforeach
{{--@php dev($max); @endphp--}}
<div class="khung_lich_ve">
    <div class="date_picker">
        <div class="innerLR">
            <form action="" class="">
                <div class="row-fluid">
                    <div class="span2">
                        <label for="searchGoodCode">Mã hàng</label>
                        <input type="text" class="span12" name="goodsCode" id="searchGoodCode"
                               {{--placeholder="Mã hàng hóa"--}}
                               value="{{request('goodsCode')}}">
                    </div>
                    <div class="span2">
                        <label for="searchReceiverPhone">SĐT người nhận</label>
                        <input type="text" class="span12" name="receiverPhone" id="searchReceiverPhone"
                               pattern="[0-9]{1,11}" title="Nhập số không dài quá 11"
                               {{--placeholder="Số điện thoại nhận"--}}
                               value="{{request('receiverPhone')}}">
                    </div>
                    <div class="span2">
                        <label for="searchSenderPhone">SĐT người gửi</label>
                        <input type="text" class="span12" name="senderPhone" id="searchSenderPhone"
                               pattern="[0-9]{1,11}" title="Nhập số không dài quá 11"
                               {{--placeholder="Số điện thoại gửi"--}}
                               value="{{request('senderPhone')}}">
                    </div>
                    <div class="span2" style="    margin-top: 25px;">
                        <button class="btn btn-info btn-flat-full" id="search">TÌM HÀNG</button>
                    </div>
                    <div class="span2" style="    margin-top: 25px;">
                        <button type="button" class="btn btn-info btn-flat-full add ">THÊM MỚI</button>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span2">
                        <label for="RouteId">Tuyến</label>
                        <select type="text" id="RouteId" class="span12" name="routeId">
                            <option value="">Tất cả tuyến</option>
                            @foreach($listRoute as $route)
                                <option value="{{$route['routeId']}}" {{$route['routeId']==request('routeId')?'selected':''}}>{{$route['routeName']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-append span2">
                        <label for="dateFrom">Từ ngày</label>
                        <input autocomplete="off" id="dateFrom" type="text" name="sendDate"
                               class="datepicker span10" title="Từ ngày"
                               value="{{request('sendDate',date('d-m-Y'))}}">
                    </div>
                    <div class="input-append span2">
                        <label for="dateFrom">Đến ngày</label>
                        <input autocomplete="off" type="text" id="dateTo" name="toDate"
                               class="datepicker span10" title="Đến ngày"
                               value="{{request('toDate',date('d-m-Y'))}}">
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<div style="padding-top: 0px;margin-bottom: 150px">
    <div class="row-fluid" style="overflow-x: scroll;">
        <table class="table table-hover table-vertical-center checkboxs tablesorter" id="step-search"
               style=" table-layout: auto; width:{{$max*10>1300?$max*10 .'px':'100%'}};overflow: auto;">
            <thead style="">
            <tr id="setWidth">
                @foreach($listSetting as $setting)
                    @if(!in_array($setting['id'],[7,16]))
                    @php
                        if(trim($setting['alias'],'') !='') $nameAttr=$setting['alias']; else $nameAttr=$setting['name'];
                        if(in_array($setting['id'],[6,9,10,23,24,25]) && strpos($nameAttr, 'VNĐ') == false && strpos($nameAttr, 'VND') == false) $nameAttr .='(VNĐ)';
                        $style='font-size:'.($setting['size']>0?$setting['size']:12).'px;font-weight:'.($setting['bold']==1?'bold':'');
                    @endphp
                    <th class="left" style="{{$style}}">{{$nameAttr}}</th>
                    @endif
                @endforeach
            </tr>
            </thead>
            <tbody>
            @php $dem=0;@endphp
            @forelse($listGoods as $package)
                <tr>
                    @foreach($listSetting as $setting)
                        @php
                            if(!in_array($setting['id'],[7,16])){
                            $style='font-size:'.$setting['size'].'px;font-weight:'.($setting['bold']==1?'bold':'');
                            if($setting['id']==1) echo '<td class="center" style="'.$style.'">'.$dem.'</td>';
                            else if($setting['id']==3) echo '<td class="left" style="'.$style.'">'.date("d-m-Y",@$package[$setting['attribute']]/1000).'</td>';
                            else if(in_array($setting['id'],[6,8,9,10,23,24,25])) echo '<td class="right" style="'.$style.'">'.number_format(@$package[$setting['attribute']]).'</td>';
                            else if($setting['id']!=22) echo '<td class="left" style="'.$style.'">'.@$package[$setting['attribute']].'</td>';
                            else echo '<td class="center uniformjs"><div class="checker"><input class="ck-dropMidway"'.
                                       ((isset($package[$setting['attribute']])?$package[$setting['attribute']]:false)?'checked':'').
                                       ' type="checkbox" id="dropMidway_1_'.$package['goodsId'].'"/><label for="dropMidway_1_'.$package['goodsId'].'">
                                       </label></div></td>';
                                       }
                        @endphp
                    @endforeach
                    @php $dem++;@endphp
                </tr>
                <tr class="info hide">
                    <td colspan="20">
                        @include('cpanel.Ship_new.timeline',['package'=>$package])
                        {{--@include('cpanel.Ship_new.print',['package'=>$package])--}}
                    </td>
                </tr>
            @empty
                <tr>
                    <td class="center" colspan="20">Hiện tại không có dữ liệu</td>
                </tr>
            @endforelse

            </tbody>
        </table>
    </div>
</div>
<div class=" m_top_15" style="bottom: 70px; right: 10px;position: fixed;">
</div>