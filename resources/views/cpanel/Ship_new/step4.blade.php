@php $max=0;@endphp
@foreach($listSetting as $getTilte)
    @php $max+=strlen($getTilte['alias']==''?$getTilte['name']:$getTilte['alias']);@endphp
@endforeach
@foreach($listGoods as $list)
    @php  $number=0;
        for($i=0;$i<count($listSetting);$i++)
            $number+=strlen(@$list[$listSetting[$i]['attribute']]);
           if($max<$number) $max=$number;
    @endphp
@endforeach
<form action="{{action('ShipNewController@deliverPackage')}}" method="post" id="deliverForm">
    <div class="row-fluid" style="overflow-x: scroll;">
        <table class="table table-hover table-vertical-center checkboxs tablesorter" id="step4" style="table-layout: auto;width: {{$max*10>1300?$max*10 .'px':'100%'}}">
            <thead style="">
            <tr>
                <td style="width: 1%;" class="uniformjs">
                    <div class="checker">
                        <input type="checkbox" class="check_all" id="check_all_4">
                        <label for="check_all_4"></label>
                    </div>
                </td>
                {{--<th style="width: 1%;" class="center">#</th>--}}
                @foreach($listSetting as $setting)
                    @php
                        if(trim($setting['alias'],'') !='') $nameAttr=$setting['alias']; else $nameAttr=$setting['name'];
                        if(in_array($setting['id'],[6,9,10,23,24,25]) && strpos($nameAttr, 'VNĐ') == false && strpos($nameAttr, 'VND') == false) $nameAttr .='(VNĐ)';
                     $style='font-size:'.($setting['size']>0?$setting['size']:12).'px;font-weight:'.($setting['bold']==1?'bold':'');
                    @endphp
                    <th class="left" style="{{$style}}">{{$nameAttr}}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @php $dem=0; @endphp
            @forelse($listGoods as $package)
                <tr>
                    <td class="center uniformjs">
                        <div class="checker">
                            <input name="goodsCode" type="checkbox" class="giaohang"
                                   value="{{@$package['goodsCode']}}"
                                   id="check_4_{{$loop->index}}">
                            <label for="check_4_{{$loop->index}}"></label>
                        </div>
                    </td>
                    @foreach($listSetting as $setting)
                        @php
                               $style='font-size:'.$setting['size'].'px;font-weight:'.($setting['bold']==1?'bold':'');
                               if($setting['id']==1) echo'<td class="center" style="'.$style.'">'.$dem.'</td>';
                               else if($setting['id']==3) echo '<td class="left" style="'.$style.'">'.date("d-m-Y",@$package[$setting['attribute']]/1000).'</td>';
                               else if(in_array($setting['id'],[6,8,9,10,23,24,25])) echo '<td class="right" style="'.$style.'">'.number_format(@$package[$setting['attribute']]).'</td>';
                               else if(in_array($setting['id'],[7,16])) echo '<td><div class="frm_chonanh"><div class="chonanh"><img src="'.
                                           (isset($package[$setting['attribute']]) && $package[$setting['attribute']]!=''?$package[$setting['attribute']]:'/public/images/Anhguido1.png').
                                           '" class="'.(isset($package[$setting['attribute']]) && $package[$setting['attribute']]!=''?'zoom':'').'"></div></div></td>';
                               else if($setting['id']!=22) echo '<td class="left" style="'.$style.'">'.@$package[$setting['attribute']].'</td>';
                               else echo '<td class="center uniformjs"><div class="checker"><input class="ck-dropMidway"'.
                                          ((isset($package[$setting['attribute']])?$package[$setting['attribute']]:false)?'checked':'').
                                          ' type="checkbox" id="dropMidway_1_'.$package['goodsId'].'"/><label for="dropMidway_1_'.$package['goodsId'].'">
                                          </label></div></td>';
                        @endphp
                    @endforeach
                    @php $dem++; @endphp
                </tr>
                <tr class="info hide">
                    <td colspan="20">
                        @include('cpanel.Ship_new.timeline',['package'=>$package])
                        @include('cpanel.Ship_new.print',['package'=>$package])
                    </td>
                </tr>
            @empty
                <tr>
                    <td class="center" colspan="15">Hiện tại không có dữ liệu</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
    <div class="row-fluid m_top_15" style="bottom: 70px; position: fixed;">
        <div class="span2">
            <button {{isset($listGoods) && count($listGoods)>0?'':'style=display:none'}} class="border btn btn-info btn-flat-full"
                    type="button" id="giaohang">GIAO HÀNG
            </button>
            <input type="hidden" name="listGiaohang" id="listGiaohang">
        </div>
        <div class="span2">
            <button {{isset($listGoods) && count($listGoods)>0?'':'style=display:none'}} class="border btn btn-info btn-flat-full"
                    type="button" id="invandon4">IN VẬN ĐƠN
            </button>
        </div>
    </div>
    <input type="hidden" name="goodsStatus" value="5">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="modal modal-custom hide fade" data-backdrop="static" id="shipGood"
         style="width: 60%; margin-left: -28%;">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>GIAO HÀNG</h3>
        </div>
        <div class="modal-body" style="max-height: 480px;overflow-y:auto;">
            <div class="row-fluid">
                <div class="span3">
                    <div class="control-group">
                        <label for="" class="control-label">Giao thêm vận đơn</label>
                        <div class="controls">
                            <div class="input-append">
                                <input autocomplete="off" type="text" class="span11" placeholder="Mã vận đơn"
                                       id="addCodeShip" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span3">
                    <label>Số lượng</label>
                    <input type="number" disabled class="quantity" style="width: 50%;">
                </div>
            </div>
            <div class="row-fluid">
                <table style="width: 100%;display: inline-grid;">
                    <tbody id="listCodeShip">
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="row-fluid">
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn-warning btn-flat" type="submit" id="btnGiaohang">GIAO HÀNG</button>
            </div>
        </div>
    </div>
</form>
