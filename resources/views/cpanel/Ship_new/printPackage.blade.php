@php $checkList = [];$listInPackage=[] @endphp
<div class="hide">
    <table id="{{$packageCode23}}">
        @foreach($listGoods as $packageGoods)
            @if(isset($packageGoods['goodsPackageCode']) && $packageGoods['goodsPackageCode']== $packageCode23 && !in_array($packageGoods['goodsPackageCode'],$checkList))
                @php $checkList[]=$packageCode23 @endphp
                <thead style="color: black">
                <tr style="border: 1px solid">
                    <th colspan="7" style="text-align:left;border: 1px solid; width: 150px">Mã
                        Gom: {{@$packageGoods['goodsPackageCode']}} - Tuyến: {{@$packageGoods['routeName']}}
                        - Lên xe: {{date("d-m-Y H:i:s",@$packageGoods['timeline'][2]/1000)}} -
                        Chuyến: {{isset($packageGoods['tripNumberPlate'])?$packageGoods['tripNumberPlate']:''}} {{isset($packageGoods['tripStartDate'])?('-'.substr(@$packageGoods['tripStartDate'],6,2).'/'.substr(@$packageGoods['tripStartDate'],4,2).'/'.substr(@$packageGoods['tripStartDate'],0,4)):''}}</th>

                </tr>
                </thead>
                @php $dem=1;@endphp
                @foreach($listGoods as $package)
                    @if(isset($package['goodsPackageCode']) &&  $packageCode23==$package['goodsPackageCode'] )
                        @php  $dem++;  @endphp
                    @endif
                @endforeach
                <tbody>
                <tr>
                    <th id="barCode_{{$packageCode23}}" rowspan="{{$dem}}"
                        style="border: 1px solid; padding-left:10px"></th>
                    <th style="border: 1px solid">Mã vận đơn</th>
                    <th style="border: 1px solid">Hàng</th>
                    <th style="border: 1px solid">Khách nhận</th>
                    @if(session('companyId') == 'TC04r1lru1vOk3c')
                        <th style="border: 1px solid">Nơi trả</th>
                        <th style="border: 1px solid">Giá cước(VNĐ)</th>
                        <th style="border: 1px solid">Giá trị hàng(VNĐ)</th>
                    @else
                        <th style="border: 1px solid">Nơi gửi</th>
                        <th style="border: 1px solid">Nơi trả</th>
                        <th style="border: 1px solid">Phải thu(VNĐ)</th>
                    @endif
                </tr>
                @foreach($listGoods as $package)
                    @if(isset($package['goodsPackageCode']) &&  $packageCode23==$package['goodsPackageCode'])
                        @php($listInPackage[]=$package)
                    @endif
                @endforeach
                @php
                    for($i=0;$i<count($listInPackage)-1;$i++){
                        for($j=0;$j<count($listInPackage)-$i-1;$j++){
                           if($listInPackage[$j]['dropPointIndex'] > $listInPackage[$j+1]['dropPointIndex']){
                           $tmp=$listInPackage[$j+1]['dropPointIndex']; $listInPackage[$j+1]=$listInPackage[$j];$listInPackage[$j]=$tmp;
                           }
                        }
                     }
                @endphp
                @foreach($listInPackage as $package)
                    <tr>
                        <td style="border: 1px solid" class="center">{{@$package['goodsCode']}}</td>
                        <td style="border: 1px solid" class="center">{{@$package['goodsName']}}</td>
                        <td style="border: 1px solid"
                            class="center">{{(isset($package['receiverPhone'])&& trim($package['receiverPhone'])!='' ?$package['receiverPhone']:'')}} {{(isset($package['receiver']) ? '-'.$package['receiver']:'')}}</td>
                        @if(session('companyId') == 'TC04r1lru1vOk3c')
                            <td style="border: 1px solid"
                                class="center">{{(isset($package['dropOffPoint'])&& trim($package['dropOffPoint']," ")!="")?$package['dropOffPoint']:((isset($package['dropPointName'])&&$package['dropPointName']!='')?$package['dropPointName']:'')}}</td>
                            <td style="border: 1px solid"
                                class="center">{{number_format($package['totalPrice'])}}</td>
                            <td style="border: 1px solid"
                                class="center">{{(isset($package['goodsValue']) && $package['goodsValue'] >0 )?number_format(@$package['goodsValue']):''}}</td>
                        @else
                            <td style="border: 1px solid"
                                class="center">{{(isset($package['pickUpPoint'])&& trim($package['pickUpPoint']," ")!="")?$package['pickUpPoint']:((isset($package['pickPointName'])&&$package['pickPointName']!='')?$package['pickPointName']:'')}}</td>
                            <td style="border: 1px solid"
                                class="center">{{(isset($package['dropOffPoint'])&& trim($package['dropOffPoint']," ")!="")?$package['dropOffPoint']:((isset($package['dropPointName'])&&$package['dropPointName']!='')?$package['dropPointName']:'')}}</td>
                            <td style="border: 1px solid" class="center">{{number_format(@$package['unpaid'])}}</td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            @endif
        @endforeach
    </table>
</div>