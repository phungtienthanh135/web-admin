@php $max=0;@endphp
@foreach($listSetting as $getTilte)
    @php $max+=strlen($getTilte['alias']==''?$getTilte['name']:$getTilte['alias']);@endphp
@endforeach
@foreach($listGoods as $list)
    @php  $number=0;
        for($i=0;$i<count($listSetting);$i++)
            $number+=strlen(@$list[$listSetting[$i]['attribute']]);
           if($max<$number) $max=$number;
    @endphp
@endforeach
@php($listPackageGoods = [])
<form action="{{action('ShipNewController@shipPackage')}}" id="shipPackage" method="post">
    <div class="row-fluid" style="overflow-x: scroll;width:{{$max*10>1300?$max*13:''}}px;">
        <table class="table table-vertical-center checkboxs" id="step2" style="width:{{$max*10>1300?$max*10 .'px':'100%'}};table-layout: auto;min-width: 100%;">
            @foreach($listGoods as $packageGoods)
                @if(isset($packageGoods['goodsPackageCode']) && !in_array($packageGoods['goodsPackageCode'],$listPackageGoods))
                    @php($listPackageGoods[]=$packageGoods['goodsPackageCode'])
                    <thead style="background: #aaa">
                    <tr class="showtr2" data-code="{{$packageGoods['goodsPackageCode']}}">
                        <th colspan="1" class="uniformjs">
                            <div class="checker">
                                <input name="goodsPackageCode" value="{{$packageGoods['goodsPackageCode']}}"
                                       data-tripid="{{@$packageGoods['tripId']}}"
                                       data-pick="{{@$package['pickPointId']}}" data-drop="{{@$package['dropPointId']}}"
                                       data-route="{{@$packageGoods['routeId']}}"
                                       data-date="{{substr(@$packageGoods['tripStartDate'],6,2).'-'.substr(@$packageGoods['tripStartDate'],4,2).'-'.substr(@$packageGoods['tripStartDate'],0,4)}}"
                                       type="checkbox" class="check_all2 form2" id="check_2_{{$loop->index}}">
                                <label for="check_2_{{$loop->index}}"></label>
                                @include('cpanel.Ship_new.printPackage',['listGoods'=>$listGoods,'packageCode23'=>$packageGoods['goodsPackageCode']])
                            </div>
                        </th>
                        <th colspan="24">Mã gom
                            : {{@$packageGoods['goodsPackageCode']}} {{isset($packageGoods['goodsPackageName'])?'-'.$packageGoods['goodsPackageName']:''}}
                            {{isset($packageGoods['routeName'])?' ___ Tuyến đường : '.$packageGoods['routeName']:''}}
                            {{isset($packageGoods['timeline'])?'___ Thời điểm gom : '.date("d-m-Y H:i:s",$packageGoods['timeline'][1]/1000):''}}
                            {{isset($packageGoods['tripNumberPlate'])?' ___ Chuyến Xe : '.$packageGoods['tripNumberPlate']:''}} {{isset($packageGoods['tripStartDate'])?('-'.substr(@$packageGoods['tripStartDate'],6,2).'/'.substr(@$packageGoods['tripStartDate'],4,2).'/'.substr(@$packageGoods['tripStartDate'],0,4)):''}}</th>
                    </tr>
                    </thead>
                    <thead style="">
                    <tr class="{{$packageGoods['goodsPackageCode']}}  hide">
                        <th style="width: 40px"></th>
                        @foreach($listSetting as $setting)
                            @php
                                if(trim($setting['alias'],'') !='') $nameAttr=$setting['alias']; else $nameAttr=$setting['name'];
                                if(in_array($setting['id'],[6,9,10,23,24,25]) && strpos($nameAttr, 'VNĐ') == false && strpos($nameAttr, 'VND') == false) $nameAttr .='(VNĐ)';
                            $style='font-size:'.($setting['size']>0?$setting['size']:12).'px;font-weight:'.($setting['bold']==1?'bold':'');
                            @endphp
                            <th class="left" style="{{$style}}">{{$nameAttr}}</th>
                        @endforeach

                    </tr>
                    </thead>
                    @foreach($listGoods as $package)
                        @if($packageGoods['goodsPackageCode'] ==$package['goodsPackageCode'])
                            <tr class="{{$packageGoods['goodsPackageCode']}} hide">
                                <th colspan="1" class="uniformjs">
                                    <div class="checker" style="padding-left: 50px">
                                        <input value="{{@$package['goodsCode']}}"
                                               data-pick="{{@$package['pickPointId']}}"
                                               data-drop="{{@$package['dropPointId']}}"
                                               type="checkbox" class="resetGood form2"
                                               id="check_2_{{$package['goodsCode']}}"
                                               data-package="{{$package['goodsPackageCode']}}">
                                        <label for="check_2_{{$package['goodsCode']}}"></label>
                                    </div>
                                </th>
                                @foreach($listSetting as $setting)
                                    @php
                                        $style='font-size:'.$setting['size'].'px;font-weight:'.($setting['bold']==1?'bold':'');
                                           if($setting['id']==3) echo '<td class="left" style="'.$style.'">'.date("d-m-Y",@$package[$setting['attribute']]/1000).'</td>';
                                           else if(in_array($setting['id'],[6,8,9,10,23,24,25])) echo '<td class="right" style="'.$style.'">'.number_format(@$package[$setting['attribute']]).'</td>';
                                           else if(in_array($setting['id'],[7,16])) echo '<td><div class="frm_chonanh"><div class="chonanh"><img src="'.
                                                       (isset($package[$setting['attribute']]) && $package[$setting['attribute']]!=''?$package[$setting['attribute']]:'/public/images/Anhguido1.png').
                                                       '" class="'.(isset($package[$setting['attribute']]) && $package[$setting['attribute']]!=''?'zoom':'').'"></div></div></td>';
                                           else if($setting['id']!=22) echo '<td class="left" style="'.$style.'">'.@$package[$setting['attribute']].'</td>';
                                           else echo '<td class="center uniformjs"><div class="checker"><input class="ck-dropMidway"'.
                                                      ((isset($package[$setting['attribute']])?$package[$setting['attribute']]:false)?'checked':'').
                                                      ' type="checkbox" id="dropMidway_1_'.$package['goodsId'].'"/><label for="dropMidway_1_'.$package['goodsId'].'">
                                                      </label></div></td>';
                                    @endphp
                                @endforeach
                            </tr>
                            <tr class="info hide {{$packageGoods['goodsPackageCode']}}">
                                <td colspan="20">
                                    @include('cpanel.Ship_new.timeline',['package'=>$package])
                                </td>
                            </tr>
                        @endif
                    @endforeach
                @endif
            @endforeach
        </table>
    </div>
    <div class="row-fluid m_top_15" style="bottom: 70px; position: fixed;">
        <div class="span2">
            <button {{isset($listGoods) && count($listGoods)>0?'':'style=display:none'}} class="btn btn-info btn-flat-full border"
                    id="aceptGoods" type="submit">NHẬN HÀNG LÊN XE
            </button>
        </div>
        <div class="span2">
            <button {{isset($listGoods) && count($listGoods)>0?'':'style=display:none'}} class="btn btn-info btn-flat-full border"
                    type="button" title="in mã chọn đầu tiên" id="inmagom">IN MÃ GOM
            </button>
        </div>
    </div>
    <input type="hidden" class="listPackageGood2" name="listPackageGood">
    <input type="hidden" name="tripId" class="tripId2">
    <input type="hidden" name="_token" value="{{csrf_token()}}">

</form>
<form action="{{action('ShipNewController@updatePackage')}}">
    <div class="row-fluid m_top_15" style="bottom: 30px; position: fixed;">
        <div class="span2">
            <button {{isset($listGoods) && count($listGoods)>0?'':'style=display:none'}} class="btn btn-info btn-flat-full border"
                    id="resetGood">TÁCH VẬN ĐƠN
            </button>
        </div>
        <div class="span2">
            <button {{isset($listGoods) && count($listGoods)>0?'':'style=display:none'}} type="button"
                    class="btn btn-info btn-flat-full border"
                    id="pokeGood">THÊM VẬN ĐƠN
            </button>
        </div>
    </div>
    <input type="hidden" name="goodsCode" id="goodsCodeMove">
    <input type="hidden" name="goodsPackageCode" id="packageCodeReset">
    <div class="modal modal-custom hide fade" data-backdrop="static" id="pokeGoodToPackage"
         style="width: 60%; margin-left: -28%;">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>CHÈN THÊM VẬN ĐƠN</h3>
        </div>
        <div class="modal-body" style="max-height: 480px;overflow-y:auto;">
            <div class="row-fluid">
                <div class="span2">
                    <div class="control-group">
                        <label for="" class="control-label">Thêm vận đơn</label>
                        <div class="controls">
                            <div class="input-append">
                                <input autocomplete="off" type="text" placeholder="Mã vận đơn" style="width: 112%"
                                       class="span11" id="pokePackage" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span3" style="margin-left: 6%">
                    <label>Số mã gom </label>
                    <input type="number" disabled class="quantity" style="width: 50%;">
                </div>

            </div>
            <div class="row-fluid">
                <table style="width: 100%;display: inline-grid;">
                    <tbody id="listPokeToTrip">
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="packagePoke" value="" id="packagePoke">
            <input type="hidden" name="listPoke" value="" id="listPoke">
            <div class="row-fluid">
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn-warning btn-flat" type="submit" id="pokeSubmit">CHÈN VÀO</button>
            </div>
        </div>
    </div>
</form>
<form action="{{action('ShipNewController@shipPackage')}}" method="post">
    <div class="modal modal-custom hide fade" data-backdrop="static" id="upToTrip2"
         style="width: 60%; margin-left: -28%;">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>CHUYỂN LÊN XE</h3>
        </div>
        <div class="modal-body" style="max-height: 480px;overflow-y:auto;">
            <div class="row-fluid">
                <div class="span2">
                    <div class="control-group">
                        <label for="" class="control-label">Ngày xuất phát</label>
                        <div class="controls">
                            <div class="input-append">
                                <input autocomplete="off" type="text" name="timeCreate" class="datepicker span11"
                                       id="calendar2"
                                       value="{{request('sendDateInDay',date('d-m-Y'))}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span3" style="margin-left: 100px">
                    <div class="control-group">
                        <label for="" class="control-label">Tuyến</label>
                        <div class="controls">
                            <select name="routeId" id="routeId2" required>
                                <option value="">-- Chọn --</option>
                                {{--@foreach($listRoute as $route)--}}
                                {{--<option value="{{@$route['routeId']}}">{{$route['routeName']}}</option>--}}
                                {{--@endforeach--}}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="span3" style="margin-left: 100px">
                    <div class="control-group">
                        <label for="" class="control-label">Chuyến</label>
                        <div class="controls">
                            <select id="scheduleId2" required>
                                <option value="">-- Chọn --</option>
                            </select>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="tripId" class="tripId2">
                <input type="hidden" name="listPackageGood" class="listPackageGood2">
                <input type="hidden" name="scheduleId" class="scheduleId2">
                <input type="hidden" name="sendTripExist" id="sendTripExist" value="1">
            </div>
            <div class="row-fluid">
                <div class="span2">
                    <div class="control-group">
                        <label for="" class="control-label">Thêm gom</label>
                        <div class="controls">
                            <div class="input-append">
                                <input autocomplete="off" type="text" placeholder="Mã gom" style="width: 112%"
                                       class="span11" id="addPackage" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span3" style="margin-left: 6%">
                    <label>Số mã gom </label>
                    <input type="number" disabled class="quantity" style="width: 50%;">
                </div>

            </div>
            <div class="row-fluid">
                <table style="width: 100%;display: inline-grid;">
                    <tbody id="listUpToTrip">
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="row-fluid">
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn-warning btn-flat" type="submit" id="nhanlenxe">CHUYỂN LÊN XE</button>
            </div>
        </div>
    </div>
</form>
{{--<form action="{{action('ShipNewController@createPackage')}}">--}}
    {{--<div class="modal modal-custom hide fade" data-backdrop="static" id="up-package">--}}
        {{--<div class="modal-header center">--}}
            {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--}}
            {{--<h3>GOM HÀNG LÊN XE</h3>--}}
        {{--</div>--}}
        {{--<div class="modal-body" style="max-height: 480px;overflow-y:auto;">--}}
            {{--<div class="row-fluid">--}}
                {{--<div class="span6">--}}
                    {{--<div class="control-group">--}}
                        {{--<label for="" class="control-label">Tên mã gom</label>--}}
                        {{--<div class="controls">--}}
                            {{--<input type="text" value="" name="goodsPackageName" id="goodsPackageName"/>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="control-group">--}}
                        {{--<label for="" class="control-label">Ngày xuất phát</label>--}}
                        {{--<div class="controls">--}}
                            {{--<div class="input-append">--}}
                                {{--<input autocomplete="off" type="text" name="timeCreate" class="datepicker span11"--}}
                                       {{--id="txtCalendar"--}}
                                       {{--value="{{request('sendDateInDay',date('d-m-Y'))}}">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="span6">--}}
                    {{--<div class="control-group">--}}
                        {{--<label for="" class="control-label">Tuyến</label>--}}
                        {{--<div class="controls">--}}
                            {{--<select name="routeId" id="listRoute">--}}
                                {{--<option value="">-- Chọn --</option>--}}
                                {{--@foreach($listRoute as $route)--}}
                                {{--<option value="{{@$route['routeId']}}">{{$route['routeName']}}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="control-group">--}}
                        {{--<label for="" class="control-label">Chuyến</label>--}}
                        {{--<div class="controls">--}}
                            {{--<select name="tripId" id="listTrip">--}}
                                {{--<option value="">-- Chọn --</option>--}}
                            {{--</select>--}}
                            {{--<input type="hidden" name="tripId" class="tripId">--}}
                            {{--<input type="hidden" name="scheduleId" class="scheduleId">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="modal-footer">--}}
            {{--<input type="hidden" name="goodsIdList" id="js-input-goods-list" value="">--}}
            {{--<input type="hidden" name="_token" value="{{csrf_token()}}">--}}
            {{--<div class="row-fluid">--}}

                {{--<a data-dismiss="modal" style="cursor:pointer;  " aria-hidden="true" class="btn_huy_modal">HỦY</a>--}}
                {{--<button class="btn btn-warning btn-flat" style="  " id="gomlenxe">GOM HÀNG</button>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</form>--}}