<!DOCTYPE html>
<html lang="vi VN">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHẦN MỀM NHÀ XE</title>
    <link rel="shortcut icon" href="/public/favicon.png">
    <!-- Main styles for this application -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900|Open+Sans:300,600&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <meta id="csrf-token" name="csrf-token" value="{{ csrf_token() }}">
</head>
<body>
<div id="app">
</div>
<script>
    var token = '{{$token}}';
    var tripId = '{{$tripId}}';
    var CONFIG = {!! json_encode(CONFIG_AV[env('APP_ENV', 'dev')]) !!};
</script>
<script src="/public{{mix('/js/electronic-contract.js')}}"></script>
</body>
</html>