<!DOCTYPE html>
<html lang="vi VN">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHẦN MỀM NHÀ XE - Đi AN về VUI</title>
    <meta name="description" content="AN VUI là NCC Phần mềm quản lý số 1 tại Việt Nam chuyên dụng cho ngành vận tải. Tăng doanh thu, Chuyên nghiệp và khoa học ... Hotline: 02499996666" />
    <meta name="keywords" content="phần mềm nhà xe, anvui, an vui, quản trị nhà xe, phần mềm quản trị nhà xe số 1 Việt Nam" />
    <meta name="title" content="PHẦN MỀM NHÀ XE - AN VUI là NCC Phần mềm quản lý vận tải số 1 tại Việt Nam" />
    {{--    meta facebook--}}
    <meta property="og:site_name" content="{{Request::getHost()}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" itemprop="url" content="{{Request::root()}}"/>
    <meta property="og:image" itemprop="thumbnailUrl" content="{{asset('/public/img/login/seo_banner.jpg')}}"/>
    <meta property="og:image:alt" content="Đi AN về VUI"/>
    <meta property="og:image:width" content=""/>
    <meta property="og:image:height" content=""/>
    <meta property="fb:app_id" content="1052250315192706"/>
    <meta content="PHẦN MỀM NHÀ XE - Giáp pháp hiệu quả cho xe tuyến và xe hợp đồng Limousine" itemprop="headline" property="og:title"/>
    <meta content="AN VUI là NCC Phần mềm quản lý số 1 tại Việt Nam chuyên dụng cho ngành vận tải. Tăng doanh thu, Chuyên nghiệp và khoa học ... Hotline: 02499996666" itemprop="description" property="og:description"/>
    {{--    end meta facebook--}}
    <link rel="shortcut icon" href="/public/favicon.png">
    <!-- Main styles for this application -->
    <link href="/public{{mix('/css/LoginEmployee.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900|Open+Sans:300,600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <meta id="csrf-token" name="csrf-token" value="{{ csrf_token() }}">
    <script type="application/ld+json">
        {"@context":"http://schema.org","@type":"Organization","name":"Phần mềm quản trị nhà xe","url":"https://phanmemnhaxe.com","slogan":"Đi an về vui, AN VUI là NCC Phần mềm quản lý vận tải số 1 tại Việt Nam","logo":"https://phanmemnhaxe.com/public/img/login/logo.png","email":"info@anvui.vn","address":{"@type":"PostalAddress","streetAddress":"Tòa nhà ecolife số 58, Tố Hữu, Thanh Xuân, Hà Nội","addressLocality":"Hà Nội"},"contactPoint":[{"@type":"ContactPoint","telephone":"19007034","contactOption":"TollFree","contactType":"customer service","areaServed":"VN"}]}
    </script>
</head>
<body>
<div id="app">
</div>
{{--//zalo chat--}}
<div v-if="">
    <div class="zalo-chat-widget" style="right: 80px !important; bottom: 10px !important;"  data-oaid="774796890025057834" data-welcome-message="Rất vui khi được hỗ trợ bạn!" data-autopopup="0" data-width="350" data-height="420"></div>
</div>
<script src="https://sp.zalo.me/plugins/sdk.js"></script>
<style>
    .zalo-chat-widget{
        right: 80px !important;
        bottom: 10px !important;
    }
</style>

{{--Facebook chat--}}
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml            : true,
            version          : 'v8.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<!-- Your Chat Plugin code -->
<div class="fb-customerchat"
     attribution=setup_tool
     page_id="420231528374168"
     theme_color="#ff7e29"
     logged_in_greeting="Xin chào AN VUI rất vui được phục vụ bạn!"
     logged_out_greeting="Xin chào AN VUI rất vui được phục vụ bạn!">
</div>
<script>
    var CONFIG = {!! json_encode(CONFIG_AV[env('APP_ENV', 'dev')]) !!}
    var urlRequestResetPassword = '{{action('LoginController@ForgotPassword', ['buoc' => 1])}}';
    var urlResetPassword = '{{action('LoginController@ForgotPassword', ['buoc' => 2])}}';
    var urlLogin = '{{ url('dang-nhap') }}';
</script>
<script src="/public{{mix('/js/loginEmployee.js')}}"></script>
</body>
</html>
