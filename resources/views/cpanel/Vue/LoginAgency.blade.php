<!DOCTYPE html>
<html lang="vi VN">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHẦN MỀM NHÀ XE</title>
    <link rel="shortcut icon" href="/public/favicon.png">
    <!-- Main styles for this application -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <meta id="csrf-token" name="csrf-token" value="{{ csrf_token() }}">
    <script type="application/ld+json">
        {"@context":"http://schema.org","@type":"Organization","name":"Phần mềm quản trị nhà xe","url":"https://phanmemnhaxe.com","slogan":"Đi an về vui, AN VUI là NCC Phần mềm quản lý vận tải số 1 tại Việt Nam","logo":"https://phanmemnhaxe.com/public/img/login/logo.png","email":"info@anvui.vn","address":{"@type":"PostalAddress","streetAddress":"Tòa nhà ecolife số 58, Tố Hữu, Thanh Xuân, Hà Nội","addressLocality":"Hà Nội"},"contactPoint":[{"@type":"ContactPoint","telephone":"19007034","contactOption":"TollFree","contactType":"customer service","areaServed":"VN"}]}
    </script>
</head>
<body>
<div id="app">
</div>
<script src="/public{{mix('/js/loginAgency.js')}}"></script>
</body>
</html>
