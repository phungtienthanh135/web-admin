<!DOCTYPE html>
<html lang="vi VN">
<head>
    <meta charset="utf-8">
    {{--<meta name="viewport" content="width=device-width, initial-scale=1.0">--}}
    <title>PHẦN MỀM NHÀ XE - Đi AN về VUI</title>
    <meta name="description" content="AN VUI là NCC Phần mềm quản lý vận tải số 1 tại Việt Nam" />
    <meta name="keywords" content="phần mềm nhà xe, anvui, an vui, quản trị nhà xe, phần mềm quản trị nhà xe số 1 Việt Nam" />
    <meta name="title" content="PHẦN MỀM NHÀ XE - AN VUI là NCC Phần mềm quản lý vận tải số 1 tại Việt Nam" />
{{--    meta facebook--}}
    <meta property="og:site_name" content="test.blo.com.vn"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" itemprop="url" content="https://test.blo.com.vn"/>
    <meta property="og:image" itemprop="thumbnailUrl" content="{{asset('/img/login/seo_banner.png')}}"/>
    <meta property="og:image:alt" content="Đi AN về VUI"/>
    <meta property="og:image:width" content=""/>
    <meta property="og:image:height" content=""/>
    <meta property="fb:app_id" content="1052250315192706"/>
    <meta content="PHẦN MỀM NHÀ XE - Giáp pháp hiệu quả cho xe tuyến và xe hợp đồng Limousine" itemprop="headline" property="og:title"/>
    <meta content="AN VUI là NCC Phần mềm quản lý số 1 tại Việt Nam chuyên dụng cho ngành vận tải. Tăng doanh thu, Chuyên nghiệp và khoa học ... Hotline: 02499996666" itemprop="description" property="og:description"/>
{{--    end meta facebook--}}
    <link rel="shortcut icon" href="/public/favicon.png">
    <!-- Main styles for this application -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="/public{{mix('/css/app.css')}}" rel="stylesheet">
    <meta id="csrf-token" name="csrf-token" value="{{ csrf_token() }}">
    <link rel="stylesheet" href="/public/javascript/jquery.datepicker.lunar/jquery.datepicker.lunar.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900|Open+Sans:300,600,900&display=swap" rel="stylesheet">
    {{--css jquery ui--}}
    {{--<script async defer--}}
            {{--src="https://maps.googleapis.com/maps/api/js?key={!! CONFIG_AV[env('APP_ENV', 'dev')]['apiMapKey'] !!}&libraries=places&language=vn"--}}
            {{--type="text/javascript"></script>--}}
    <script src="{{url('node_modules/ckeditor-full/ckeditor.js')}}"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-175461445-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-175461445-1');
    </script>
    <!-- Google Tag Manager -->
{{--    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':--}}
{{--                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],--}}
{{--            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=--}}
{{--            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);--}}
{{--        })(window,document,'script','dataLayer','GTM-NWBT3ZW');</script>--}}
    <!-- End Google Tag Manager -->
    <script type="application/ld+json">
        {"@context":"http://schema.org","@type":"Organization","name":"Phần mềm quản trị nhà xe","url":"https://phanmemnhaxe.com","slogan":"Đi an về vui, AN VUI là NCC Phần mềm quản lý vận tải số 1 tại Việt Nam","logo":"https://phanmemnhaxe.com/public/img/login/logo.png","email":"info@anvui.vn","address":{"@type":"PostalAddress","streetAddress":"Tòa nhà ecolife số 58, Tố Hữu, Thanh Xuân, Hà Nội","addressLocality":"Hà Nội"},"contactPoint":[{"@type":"ContactPoint","telephone":"19007034","contactOption":"TollFree","contactType":"customer service","areaServed":"VN"}]}
    </script>
</head>
<body>
{{--//zalo chat--}}
{{--<div>--}}
    {{--<div id="zalo" class="zalo-chat-widget" style="right: 80px !important; bottom: 10px !important;"  data-oaid="774796890025057834" data-welcome-message="Rất vui khi được hỗ trợ bạn!" data-autopopup="0" data-width="350" data-height="420"></div>--}}
{{--</div>--}}
{{--<script src="https://sp.zalo.me/plugins/sdk.js"></script>--}}
{{--<style>--}}
    {{--.zalo-chat-widget{--}}
        {{--right: 80px !important;--}}
        {{--bottom: 10px !important;--}}
    {{--}--}}
{{--</style>--}}

{{--Facebook chat--}}
{{--<!-- Load Facebook SDK for JavaScript -->--}}
{{--<div id="fb-root"></div>--}}
{{--<script>--}}
    {{--window.fbAsyncInit = function() {--}}
        {{--FB.init({--}}
            {{--xfbml            : true,--}}
            {{--version          : 'v8.0'--}}
        {{--});--}}
    {{--};--}}

    {{--(function(d, s, id) {--}}
        {{--var js, fjs = d.getElementsByTagName(s)[0];--}}
        {{--if (d.getElementById(id)) return;--}}
        {{--js = d.createElement(s); js.id = id;--}}
        {{--js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';--}}
        {{--fjs.parentNode.insertBefore(js, fjs);--}}
    {{--}(document, 'script', 'facebook-jssdk'));--}}
{{--</script>--}}

{{--<!-- Your Chat Plugin code -->--}}
{{--<div class="fb-customerchat"--}}
     {{--attribution=setup_tool--}}
     {{--page_id="420231528374168"--}}
     {{--theme_color="#ff7e29"--}}
     {{--logged_in_greeting="Xin chào AN VUI rất vui được phục vụ bạn!"--}}
     {{--logged_out_greeting="Xin chào AN VUI rất vui được phục vụ bạn!">--}}
{{--</div>--}}
{{--<script>--}}
    {{--function checkShowChatBot() {--}}
        {{--const a = window.location.href;--}}
        {{--if ( a != 'http://web-admin.local/cpanel/vue/Home'--}}
            {{--&& a != 'http://web-admin.local/cpanel/vue/room-work'--}}
            {{--&& a!= 'https://test.blo.com.vn/cpanel/vue/room-work'--}}
            {{--&& a!= 'https://test.blo.com.vn/cpanel/vue/Home'--}}
            {{--&& a!= 'https://quantri.phanmemnhaxe.com/cpanel/vue/room-work'--}}
            {{--&& a!= 'https://quantri.phanmemnhaxe.com/cpanel/vue/Home'){--}}
            {{--var x = document.getElementById("zalo");--}}
            {{--x.style.display = "none";--}}
            {{--var y = document.getElementById("fb-root");--}}
            {{--y.style.display = "none";--}}
        {{--}--}}
        {{--else {--}}
            {{--var c = document.getElementById("zalo");--}}
            {{--c.style.display = "block";--}}
            {{--var d = document.getElementById("fb-root");--}}
            {{--d.style.display = "block";--}}

        {{--}--}}
    {{--}--}}
{{--</script>--}}
<script>
    /* These are the modifications: */
    history.pushState = ( f => function pushState(){
        var ret = f.apply(this, arguments);
        window.dispatchEvent(new Event('pushstate'));
        window.dispatchEvent(new Event('locationchange'));
        return ret;
    })(history.pushState);

    history.replaceState = ( f => function replaceState(){
        var ret = f.apply(this, arguments);
        window.dispatchEvent(new Event('replacestate'));
        window.dispatchEvent(new Event('locationchange'));
        return ret;
    })(history.replaceState);

    window.addEventListener('popstate',()=>{
        window.dispatchEvent(new Event('locationchange'))
    });

    window.addEventListener('locationchange', function(){
        const a = window.location.href;
        if ( a != 'http://web-admin.local/cpanel/vue/Home'
            && a != 'http://web-admin.local/cpanel/vue/room-work'
            && a!= 'https://test.blo.com.vn/cpanel/vue/room-work'
            && a!= 'https://test.blo.com.vn/cpanel/vue/Home'
            && a!= 'https://quantri.phanmemnhaxe.com/cpanel/vue/room-work'
            && a!= 'https://quantri.phanmemnhaxe.com/cpanel/vue/Home'){
            var x = document.getElementById("zalo");
                x.style.display = "none";
            var y = document.getElementById("fb-root");
                y.style.display = "none";
        }
        else {
            var c = document.getElementById("zalo");
                c.style.display = "block";
            var d = document.getElementById("fb-root");
                d.style.display = "block";

        }
    })

</script>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NWBT3ZW"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="app">
</div>
<script>
    var CONFIG = {!! json_encode(CONFIG_AV[env('APP_ENV', 'dev')]) !!}
    var userInfo = {!! json_encode(session('userLogin')) !!};
    var callingHistoryURL = '{{ action('TicketController@getHistoryCall') }}';
</script>
<script src="/public{{mix('/js/app.js')}}"></script>
<script src="/public/javascript/jquery.datepicker.lunar/jquery.datepicker.lunar.js" type="text/javascript"></script>
<script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
<script>
    $('[data-toggle="tooltip"]').tooltip();
    $('#navbarCollapse ul.dropdown-menu li a, #navbarCollapse button.dropdown-item').on('click', function(){
        $('#navbarCollapse').collapse('hide');
    })
    $(window).on('load',function () {
        $('.hasDatepicker').datepicker("refresh");
    })

</script>
</body>
</html>
