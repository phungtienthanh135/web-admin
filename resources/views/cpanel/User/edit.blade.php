@extends('cpanel.template.layout')
@section('title', 'Sửa hồ sơ nhân viên')
@push('activeMenu')
activeMenu('{{action('UserController@show',[],true)}}');
@endpush
@section('content')
<form id="edit-form-user" action="{{action('UserController@postEdit')}}" method="post" enctype="multipart/form-data">
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Sửa hồ sơ
                    <div style="text-transform: lowercase; display: inline" id="title"></div>
                    </h3>
                </div>
            </div>
        </div>
        <div class="separator bottom"></div>
        <div class="innerLR">
            <div class="row-fluid">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
            <div class="row-fluid">
                <div class="span3">
                    <div class="control-group">
                        <label><i><strong>THÔNG TIN CÁ NHÂN</strong></i></label>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="control-group">
                                <label for="fullName">Họ & Tên</label>
                                <input class="span12" type="text" name="fullName" id="fullName"
                                value="{{$row['fullName']}}">
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="control-group">
                        <label for="Birthday">Ngày Sinh</label>
                        <div class="input-append">
                            <input id="birthDay" class="datepicker" type="text" name="birthDay"
                            value="@dateFormat($row['birthDay'])">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label for="sex_employee">Giới tính</label>
                        <select name="sex_employee" id="sex_employee" class="w_full">
                            @foreach( $Sex as $s )
                            @if( $row['sex'] == $s['id'] )
                            <option selected value="{{ $s['id'] }}">{{ $s['value'] }}</option>
                            @else
                            <option value="{{ $s['id'] }}">{{ $s['value'] }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="control-group">
                        <label for="homeTown">Quê quán</label>
                        <input class="span12" type="text" name="homeTown" id="txtQueQuan"
                        value="{{@$row['homeTown']}}">
                    </div>
                    <div class="control-group">
                        <label for="phoneNumber">Số điện thoại</label>
                        <input class="span12" type="number" name="phoneNumber" id="phoneNumber"
                        data-validation-regexp="^0(([0-9][0-9]{8})|(1[0-9]{8}))$"
                        data-validation-error-msg-custom="Vui lòng nhập đúng định dạng SĐT"
                        value="0{{$row['phoneNumber']}}">
                    </div>
                    <div class="control-group">
                        <label for="email">Địa chỉ Email</label>
                        <input class="span12" type="email" name="email" id="email"
                        data-validation-error-msg-email="Vui lòng nhập đúng định dạng"
                        value="{{$row['email']}}">
                    </div>
                    <div class="control-group">
                        <label for="identityCardNumber">Số chứng minh nhân dân</label>
                        @if(isset($row['identityCard']['identityCardNumber']))
                        <input class="span12" type="number" name="identityCardNumber" id="identityCardNumber"
                        value="{{$row['identityCard']['identityCardNumber']}}">
                        @else
                        <input class="span12" type="number" name="identityCardNumber" id="identityCardNumber">
                        @endif
                    </div>
                    <div class="control-group">
                        <label for="issuedDate">Ngày cấp CMND</label>
                        <div class="input-append">
                            @if(isset($row['identityCard']['issuedDate']))
                            <input class="datepicker" id="issuedDate" type="text" name="issuedDate"
                            value="@dateFormat($row['identityCard']['issuedDate'])">{{$row['identityCard']['issuedDate']}}
                            @else
                            <input class="span12 datepicker" id="issuedDate" type="text" name="issuedDate">
                            @endif
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="issuedPlace">Nơi cấp CMND</label>
                        @if(isset($row['identityCard']['issuedPlace']))
                        <input class="span12" type="text" name="issuedPlace" id="issuedPlace"
                        value="{{$row['identityCard']['issuedPlace']}}">
                        @else
                        <input class="span12" type="text" name="issuedPlace" id="issuedPlace">
                        @endif
                    </div>
                    <div class="control-group">
                        <label for="issuedPlace">Mã máy sử dụng (cách nhau bởi dấu phảy)</label>
                        <input class="span12" type="text" name="listWhiteMAC" id="listWhiteMAC" value="{{implode(',',$row['listWhiteMAC'])}}">
                    </div>
                </div>
                <div class="span3">
                    <div class="control-group">
                        <label><i><strong>THÔNG TIN LIÊN QUAN ĐẾN CÔNG TY</strong></i></label>
                    </div>
                    <div class="row-fluid">
                        <div class="control-group">
                            <label for="code">Mã nhân sự</label>
                            <input class="span12" type="text" name="code" id="code"
                            value="{{@$row['code']}}">
                        </div>
                        <div class="control-group span12">
                            <label for="joinDate">Ngày vào công ty</label>
                            <div class="input-append">
                                <input class="datepicker" type="text" id="joinDate" name="joinDate"
                                value="@dateFormat($row['joinDate'])">
                            </div>
                        </div>
                        <style type="text/css" media="screen">
                        .ui-datepicker-trigger {
                        display: none;
                        }
                        </style>
                    </div>
                    <div class="control-group">
                        <label for="department">Phòng ban</label>
                        <select class="span12" name="departmentId" id="departmentId">
                            @foreach($departments as $department)
                            <option {{@$row['departmentId']==$department['departmentId']?'selected':''}} value="{{$department['departmentId']}}">{{$department['departmentName']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="control-group">
                        <label for="level_employee">Nhóm quyền</label>
                        <select class="span12" name="groupPermissionId" id="groupPermissionId">
                            @foreach($groupPermissions as $groupPermission)
                            <option {{@$row['groupPermissionId']==$groupPermission['groupPermissionId']?'selected':''}} value="{{$groupPermission['groupPermissionId']}}">{{$groupPermission['groupPermissionName']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="control-group">
                        <div class="control-group">
                            <label for="userType">Chức vụ</label>
                            <select class="span12" name="userType" id="userType">
                                @foreach($response_role as $type)
                                @if($row['userType']==$type['id'] )
                                <option selected value="{{$type['id']}}">{{$type['value']}}</option>
                                @else
                                <option value="{{$type['id']}}">{{$type['value']}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group" id="banglai">
                        <label for="licenceLevel">Bằng lái</label>
                        <select class="span12" name="licenceLevel" id="licenceLevel">
                            @foreach( $licenceLevels as $level )
                            @if(@$row['licenceLevel'] == $level['id'] )
                            <option selected value="{{ $level['id'] }}">{{ $level['value'] }}</option>
                            @else
                            <option value="{{ $level['id'] }}">{{ $level['value'] }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="control-group" id="sobanglai">
                        <label for="">Số bằng lái</label>
                        <input type="number" name="licenceCode" value="{{@$row['licenceCode']}}" >
                    </div>
                    <div class="control-group" id="hethanbang">
                        <label for="txtNgayHetHanBangLai">Ngày hết hạn bằng lái</label>
                        <div class="input-append">
                            <input class="datepicker" id="txtNgayHetHanBangLai" type="text"
                            name="licenceExpired"
                            value="@dateFormat($row['licenceExpired'])">
                        </div>
                    </div>
                    <div class="control-group" id="hocvan">
                        <label for="education_employee">Học vấn cao nhất</label>
                        <select class="span12" name="education" id="education">
                            @foreach( $education_employees as $edu )
                                @if(isset($row['education']))
                                    @if($row['education'] == $edu['id'] )
                                    <option selected value="{{ $edu['id'] }}">{{ $edu['value'] }}</option>
                                    @else
                                    <option value="{{ $edu['id'] }}">{{ $edu['value'] }}</option>
                                    @endif
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="span3">
                    <div class="control-group">
                        <label><i><strong>THÔNG TIN NGƯỜI CẦN BÁO TIN</strong></i></label>
                    </div>
                    <div class="control-group">
                        <label for="contacterFullName">Khi cần báo tin cho ai?</label>
                        @if(isset($row['contactInfo']['fullName']))
                        <input class="span12" type="text" name="contacterFullName" id="contacterFullName"
                        value="{{$row['contactInfo']['fullName']}}">
                        @else
                        <input class="span12" type="text" name="contacterFullName" id="contacterFullName">
                        @endif
                    </div>
                    <div class="control-group">
                        <label for="contacterPhoneNumber">Số điện thoại người cần báo tin</label>
                        @if(isset($row['contactInfo']['phoneNumber']))
                        <input class="span12" type="number" name="contacterPhoneNumber"
                        data-validation-regexp="^0(([8-9][0-9]{8})|(1[0-9]{9}))$"
                        data-validation-error-msg-custom="Vui lòng nhập đúng định dạng SĐT"
                        id="contacterPhoneNumber" value="{{$row['contactInfo']['phoneNumber']}}">
                        @else
                        <input class="span12" type="text" name="contacterPhoneNumber" id="contacterPhoneNumber">
                        @endif
                    </div>
                    <div class="control-group">
                        <label for="contacterEmail">Email người cần báo tin</label>
                        @if(isset($row['contactInfo']['email']))
                        <input class="span12" type="email" name="contacterEmail"
                        id="contacterEmail"
                        data-validation-error-msg-email="Vui lòng nhập đúng định dạng"
                        value="{{$row['contactInfo']['email']}}">
                        @else
                        <input class="span12" type="text" name="contacterEmail"
                        id="contacterEmail">
                        @endif
                    </div>
                    <div class="control-group" >
                        <label for="phoneNumberReferrer">Số điện thoại người giới thiệu</label>
                        <input class="span12" type="number" name="phoneNumberReferrer"
                        id="phoneNumberReferrer"
                        data-validation-regexp="^0(([8-9][0-9]{8})|(1[0-9]{9}))$"
                        data-validation-error-msg-custom="Vui lòng nhập đúng định dạng SĐT"
                        value="{{@$row['phoneNumberReferrer']}}">
                    </div>
                    <div class="control-group" id="isAdminChk" style="display: none">
                        <label >Là siêu phụ xe</label>
                        <input @if($row['isAdmin']==true) checked @endif  class="span12" type="checkbox"
                        name="isAdmin" id="isAdmin">
                        <label style="margin-top: 10px" for="isAdmin"></label>    
                    </div>
                </div>
                <div class="span3">
                    <label class="separator"></label>
                    <div class="frm_chonanh">
                        <div class="chonanh">
                            <img id="imgAvatar" src="{{!empty($row['avatar']) ? $row['avatar'] : '/public/images/anhsanpham.PNG'}}">
                        </div>
                        <form>
                            <input type="file" name="img" id="selectedFile" style="display: none;">
                            <input type="button" style="width:172px" value="CHỌN ẢNH" class="btncustom"
                            onclick="document.getElementById('selectedFile').click();">
                        </form>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="control-box">
                    <div class="row-fluid">
                        <div class="control-group span5">
                            <label for="userName">Tài khoản</label>
                            @if(isset($row['userName']))
                            <input readonly class="span12" type="text" name="userName" id="userName"
                            value="{{$row['userName']}}">
                            @else
                            <input class="span12" type="text" name="userName" id="userName">
                            @endif
                        </div>
                        <div class="control-group span4">
                            <label for="password">Mật khẩu</label>
                            <input disabled class="span12" type="password" name="password" id="password">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    <input type="hidden" id="avatar" name="avatar" value="{{!empty($row['avatar']) ? $row['avatar'] : ''}}">
                    <input type="hidden" name="stateCode" value="{{$row['stateCode']}}">
                    <input type="hidden" name="userId" value="{{$row['userId']}}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <button class="btn btn-warning btn-flat-full">LƯU</button>
                </div>
                <div class="span2">
                    <a href="{{action('UserController@show')}}" class="btn btn-default btn-flat">Hủy</a>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- End Content -->}
<script>
var userType = {!!$row['userType']!!};
$('#hethanbang').hide();
$('#banglai').hide();
$('#sobanglai').hide();
showByUserType(userType);
userTypeLabel(userType);
$("#userType").change(function() {
userType = $("#userType").val();
showByUserType(parseInt(userType));
userTypeLabel(parseInt(userType));
});
function showByUserType(userType) {
if(userType == {!!DRIVER!!}){
$('#hethanbang').show();
$('#banglai').show();
$('#sobanglai').show();
$('#hocvan').hide();
}else {
$('#banglai').hide();
$('#sobanglai').hide();
$('#hocvan').show();
$('#hethanbang').hide();
}
if(userType == {!!ASSISTANT!!}){
$('#isAdminChk').show();
} else {
$('#isAdminChk').hide();
}
}
function userTypeLabel(userType) {
switch (userType) {
case {!!CUSTOMER!!}:
$("#title").html('Người dùng thường');
break;
case {!!DRIVER!!}:
$("#title").html('Lái xe');
break;
case {!!ASSISTANT!!}:
$("#title").html('Phụ xe');
break;
case {!!ACCOUNTANT!!}:
$("#title").html('Kế toán');
break;
case {!!ADMINISTRATIVE_STAFF!!}:
$("#title").html('Nhân viên hành chính');
break;
case {!!INSPECTOR!!}:
$("#title").html('Thanh tra');
break;
case {!!ORGANIZATION_ADMIN!!}:
$("#title").html('Quản trị');
break;
default:
$("#title").html('Không xác định');
break;
}
}
$.validator.addMethod('validatePhone', function (value, element) {
var flag = false;
var phone = element.value.trim();
phone = phone.replace('(+84)', '0');
phone = phone.replace('+84', '0');
phone = phone.replace('0084', '0');
phone = phone.replace(/ /g, '');
if (phone != '') {
var firstNumber = phone.substring(0, 2);
if ((firstNumber == '09' || firstNumber == '08') && phone.length == 10) {
if (phone.match(/^\d{10}/)) {
flag = true;
}
} else if (firstNumber == '01' && phone.length == 11) {
if (phone.match(/^\d{11}/)) {
flag = true;
}
}
} else {
flag = true;
}
return flag;
}, "Vui lòng nhập đúng định dạng số điện thoại");
$("#selectedFile").change(function(){
UploadImage('edit-form-user','imgAvatar','avatar');
});
$('#edit-form-user').validate({
errorClass: 'error_border',
rules: {
fullName: "required",
userName: "required",
phoneNumber: {
required: true,
validatePhone: true
},
email: "email",
contacterPhoneNumber: "validatePhone",
phoneNumberReferrer: "validatePhone",
contacterEmail: "email"
},
messages: {
fullName: "Vui lòng nhập họ tên",
userName: "Vui lòng nhập tài khoản",
phoneNumber: {
required: "Vui lòng nhập số điện thoại"
},
email: "Không đúng định dạng email",
contacterEmail: "Không đúng định dạng email"
}
});
disableChar('identityCardNumber');
disableChar('phoneNumber');
disableChar('contacterPhoneNumber');
disableChar('phoneNumberReferrer');
// Cấm các ký tự +-.,
function disableChar(id) {
$('#' + id).on("keypress", function(evt) {
var keycode = evt.charCode || evt.keyCode;
switch (keycode) {
case 43:
case 44:
case 45:
case 46:
return false;
default:
return true;
}
});
}
/*
* bắt buộc input file với name="img"
* */
function UploadImage(formId,imgId,inputId) {
var form = new FormData($("#"+formId)[0]);
$('#'+imgId).attr('src', '/public/images/loading/loader_blue.gif');
$.ajax({
type: 'POST',
url: '/cpanel/system/upload-image',
data: form,
dataType: 'json',
processData: false,
contentType: false,
cache: false
}).done(function (data) {
$('#'+imgId).attr('src', data.url);
$('#'+inputId).val(data.url);
});
}
</script>
<style>
.error_border {
color: #aa0000;
}
</style>
@endsection