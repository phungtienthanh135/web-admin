@extends('cpanel.template.layout')
@section('title', 'Thêm mới nhân viên')
@section('content')
<form action="" method="post" id="add-user-form" enctype="multipart/form-data">
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Tạo hồ sơ mới
                    <div style="text-transform: lowercase; display: inline" id="title">
                    </div>
                    </h3>
                </div>
            </div>
        </div>
        <div class="separator bottom"></div>
        <div class="innerLR">
            <div class="row-fluid">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <span id="star"><i>(*) : Là những thông tin bắt buộc nhập</i></span>
                </div>
                <div class="span3">
                    <div class="control-group">
                        <label><i><strong>THÔNG TIN CÁ NHÂN</strong></i></label>
                    </div>
                    <div class="control-group">
                        <label for="name_employee">Họ & Tên &nbsp;<span id="star">(*)</span></label>
                        <input autocomplete="off" class="span12" type="text" name="fullName" id="fullName"
                        value="{{old('fullName')}}"> 
                    </div>
                    
                    <div class="control-group">
                        <label for="Birthday">Ngày Sinh&nbsp;</label>
                        <div class="input-append">
                            <input id="birthDay" class="datepicker" type="text" name="birthDay"
                            value="{{old('birthDay')}}">
                        </div>
                    </div>
                    
                    
                    <div class="control-group">
                        <label for="sex_employee">Giới tính&nbsp;</label>
                        <select name="sex_employee" id="sex_employee" class="w_full">
                            <option value="1">Nam</option>
                            <option value="2">Nữ</option>
                        </select>
                    </div>
                    
                    <div class="control-group">
                        <label for="txtQueQuan">Quê quán&nbsp;</label>
                        <input class="span12" type="text" name="homeTown" id="txtQueQuan"
                        value="{{old('homeTown')}}">
                    </div>
                    <div class="control-group">
                        <label for="phone_employee">Số điện thoại&nbsp;</label>
                        <input class="span12" type="number" name="phoneNumber" id="phoneNumber"
                        autocomplete="off"
                        data-validation="custom required"
                        data-validation-regexp="^0(([0-9][0-9]{8})|(1[0-9]{8}))$"
                        data-validation-error-msg-custom="Vui lòng nhập đúng định dạng SĐT"
                        data-validation-error-msg="Vui lòng nhập số điện thoại"
                        value="{{old('phoneNumber')}}">
                    </div>
                    
                    <div class="control-group">
                        <label for="id_card_employee">Số chứng minh nhân dân</label>
                        <input class="span12" type="number" name="identityCardNumber" id="identityCardNumber"
                        value="{{old('identityCardNumber')}}">
                    </div>
                    <div class="control-group">
                        <label for="issuedDate">Ngày cấp CMND</label>
                        <div class="input-append">
                            <input class=" datepicker" id="issuedDate" type="text" name="issuedDate" value="{{old('issuedDate')}}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="issuedPlace">Nơi cấp CMND</label>
                        <input class="span12" type="text" name="issuedPlace" id="issuedPlace" value="{{old('issuedPlace')}}">
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="control-group">
                                <label for="email">Địa chỉ Email</label>
                                <input class="span12" type="email" name="email" id="email"
                                data-validation-error-msg-email="Vui lòng nhập đúng định dạng"
                                value="{{old('email')}}">
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="issuedPlace">Mã máy sử dụng (cách nhau bởi dấu phảy)</label>
                        <input class="span12" type="text" name="listWhiteMAC" id="listWhiteMAC" value="{{old('listWhiteMAC')}}">
                    </div>
                </div>
                <div class="span3">
                    <div class="control-group">
                        <label><i><strong>THÔNG TIN LIÊN QUAN ĐẾN CÔNG TY</strong></i></label>
                    </div>

                    <div class="control-group">
                        <label for="code">Mã nhân sự</label>
                        <input class="span12" type="text" name="code" id="code"
                        value="{{old('code')}}">
                    </div>
                    
                    <div class="control-group">
                        <label for="joinDate">Ngày vào công ty</label>
                        <div class="input-append">
                            <input autocomplete="off" id="joinDate" class="datepicker" type="text" name="joinDate"
                            value="{{old('joinDate')}}">
                        </div>
                        <style type="text/css" media="screen">
                        .ui-datepicker-trigger {
                        display: none;
                        }
                        </style>
                    </div>
                    
                    <div class="control-group">
                        <label for="departmentId">Phòng ban&nbsp;<span id="star"></span></label>
                        <select autocomplete="off" class="span12" name="departmentId" id="departmentId">
                            <option value="0">Không chọn</option>
                            @foreach($departments as $department)
                            <option value="{{$department['departmentId']}}">
                                {{$department['departmentName']}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="control-group">
                        <label for="level_employee">Nhóm quyền &nbsp;<span id="star">(*)</span></label>
                        <select autocomplete="off" class="span12" name="groupPermissionId" id="groupPermissionId">
                            @foreach($groupPermissions as $groupPermission)
                            <option value="{{$groupPermission['groupPermissionId']}}">{{$groupPermission['groupPermissionName']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="control-group">
                        <label for="userType">Chức vụ &nbsp;<span id="star">(*)</span></label>
                        <select autocomplete="off" class="span12" name="userType" id="userType">
                            @foreach($response_role as $role)
                            <option value="{{$role['id']}}">{{$role['value']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="control-group" id="banglai">
                        <label for="licenceLevel">Bằng lái &nbsp;<span id="star">(*)</span></label>
                        <select class="span12" name="licenceLevel" id="licenceLevel">
                            <option value="B1">Hạng B1</option>
                            <option value="B2">Hạng B2</option>
                            <option value="C">Hạng C</option>
                            <option value="D">Hạng D</option>
                            <option value="E">Hạng E</option>
                        </select>
                    </div>
                    <div class="control-group" id="sobanglai">
                        <label for="">Số bằng lái</label>
                        <input autocomplete="off" type="number" name="licenceCode" value="{{old('licenceCode')}}" >
                    </div>
                    <div class="control-group" id="hethanbang">
                        <label for="txtNgayHetHanBangLai">Ngày hết hạn bằng lái &nbsp;<span id="star">(*)</span></label>
                        <div class="input-append">
                            <input autocomplete="off" class="datepicker" id="txtNgayHetHanBangLai" type="text"
                            name="licenceExpired" value="{{old('licenceExpired')}}">
                        </div>
                    </div>
                    <div class="control-group" id="hocvan">
                        <label for="education_employee">Học vấn cao nhất</label>
                        <select autocomplete="off" class="span12" name="education" id="txt_education ">
                            <option value="Trung học cơ sở">Trung học cơ sở</option>
                            <option value="Trung học phổ thông">Trung học phổ thông</option>
                            <option value="Đại học">Đại học</option>
                            <option value="Cao đẳng">Cao đảng</option>
                            <option value="Thạc sĩ">Thạc sĩ</option>
                            <option value="Tiến sĩ">Tiến sĩ</option>
                        </select>
                    </div>
                </div>
                <div class="span3">
                    <div class="control-group">
                        <label><i><strong>THÔNG TIN NGƯỜI CẦN BÁO TIN</strong></i></label>
                    </div>
                    <div class="control-group">
                        <label for="contacterFullName">Khi cần báo tin cho ai?</label>
                        <input type="text" name="contacterFullName" id="contacterFullName"
                        value="{{old('contacterFullName')}}">
                    </div>
                    <div class="control-group">
                        <label for="contacterPhoneNumber">Số điện thoại người cần báo tin</label>
                        <input type="number" name="contacterPhoneNumber"
                        id="contacterPhoneNumber" value="{{old('contacterPhoneNumber')}}">
                    </div>
                    <div class="control-group">
                        <label for="contacterEmail">Email người cần báo tin</label>
                        <input type="text" name="contacterEmail" id="contacterEmail"
                        value="{{old('email')}}">
                    </div>
                    <div class="control-group">
                        <label for="phoneNumberReferrer">Số điện thoại người giới thiệu</label>
                        <input type="text" name="phoneNumberReferrer"
                        id="phoneNumberReferrer" value="{{old('phoneNumberReferrer')}}">
                    </div>
                    
                </div>
                <div class="span3">
                    <label class="separator"></label>
                    <div class="frm_chonanh">
                        <div class="chonanh">
                            <img id="imgAvatar" src="/public/images/anhsanpham.PNG">
                            <input type="hidden" name="avatar" id="avatar">
                        </div>
                        <form>
                            <input type="file" name="img" id="selectedFile" style="display: none;">
                            <input type="button" style="width:172px" value="CHỌN ẢNH" class="btncustom"
                            onclick="document.getElementById('selectedFile').click();">
                        </form>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="control-box">
                    <div class="row-fluid">
                        <div class="control-group span5">
                            <label for="username_employee">Tài khoản &nbsp;<span id="star">(*)</span></label>
                            <input class="span12" type="text" name="userName" id="userName"
                            value="{{old('userName')}}">
                        </div>
                        <div class="control-group span4">
                            <label for="password_employee">Mật khẩu &nbsp;<span id="star">(*)</span></label>
                            <input class="span12" type="password" name="password" id="password">
                        </div>
                        <div class="control-group span3" id="isAdminChk">
                            <label>Là siêu phụ xe</label>
                            <input class="span12" type="checkbox" name="isAdmin" id="isAdmin">
                            <label style="margin-top: 10px" for="isAdmin"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    <input type="hidden" id="avatar" name="avatar">
                    <input type="hidden" name="deviceType" value="-1">
                    <input type="hidden" name="stateCode" value="84">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <button class="btn btn-warning btn-flat-full">LƯU</button>
                </div>
                <div class="span2">
                    <a href="{{action('UserController@show')}}" class="btn btn-default btn-flat">Hủy</a>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- End Content -->
<script>
var userType = {!!CUSTOMER!!};
$('#hethanbang').hide();
$('#banglai').hide();
$('#sobanglai').hide();
userTypeLabel(userType);
showByUserType(userType);
$("#userType").change(function() {
userType = $("#userType").val();
userTypeLabel(parseInt(userType));
showByUserType(parseInt(userType));
});
disableChar('identityCardNumber');
disableChar('phoneNumber');
disableChar('contacterPhoneNumber');
disableChar('phoneNumberReferrer');
// Cấm các ký tự +-.,
function disableChar(id) {
$('#' + id).on("keypress", function(evt) {
var keycode = evt.charCode || evt.keyCode;
switch (keycode) {
case 43:
case 44:
case 45:
case 46:
return false;
default:
return true;
}
});
}
function showByUserType(userType) {
if(userType == {!!DRIVER!!}){
$('#hethanbang').show();
$('#banglai').show();
$('#sobanglai').show();
$('#hocvan').hide();
}else {
$('#banglai').hide();
$('#sobanglai').hide();
$('#hocvan').show();
$('#hethanbang').hide();
}
if(userType == {!!ASSISTANT!!}){
$('#isAdminChk').show();
} else {
$('#isAdminChk').hide();
}
}
function userTypeLabel(userType) {
switch (userType) {
case {!!CUSTOMER!!}:
$("#title").html('Người dùng thường');
break;
case {!!DRIVER!!}:
$("#title").html('Lái xe');
break;
case {!!ASSISTANT!!}:
$("#title").html('Phụ xe');
break;
case {!!ACCOUNTANT!!}:
$("#title").html('Kế toán');
break;
case {!!ADMINISTRATIVE_STAFF!!}:
$("#title").html('Nhân viên hành chính');
break;
case {!!INSPECTOR!!}:
$("#title").html('Thanh tra');
break;
case {!!ORGANIZATION_ADMIN!!}:
$("#title").html('Quản trị');
break;
default:
$("#title").html('Không xác định');
break;
}
}
$('#modal-userType').on('hidden.bs.modal', function () {
history.go(-1);
});
$("#selectedFile").change(function(){
    UploadImage('add-user-form','imgAvatar','avatar');
});
$.validator.addMethod('validatePhone', function (value, element) {
var flag = false;
var phone = element.value.trim();
phone = phone.replace('(+84)', '0');
phone = phone.replace('+84', '0');
phone = phone.replace('0084', '0');
phone = phone.replace(/ /g, '');
if (phone != '') {
var firstNumber = phone.substring(0, 2);
if ((firstNumber == '09' || firstNumber == '08') && phone.length == 10) {
if (phone.match(/^\d{10}/)) {
flag = true;
}
} else if (firstNumber == '01' && phone.length == 11) {
if (phone.match(/^\d{11}/)) {
flag = true;
}
}
} else {
flag = true;
}
return flag;
}, "Vui lòng nhập đúng định dạng số điện thoại");
$('#add-user-form').validate({
errorClass: 'error_border',
rules: {
fullName: "required",
userName: "required",
password: "required",
phoneNumber: {
required: true,
validatePhone: true
},
email: "email",
contacterEmail: "email",
contacterPhoneNumber: "validatePhone",
phoneNumberReferrer: "validatePhone"
},
messages: {
fullName: "Vui lòng nhập họ tên",
userName: "Vui lòng nhập tài khoản",
password: "Vui lòng nhập mật khẩu",
phoneNumber: {
required: "Vui lòng nhập số điện thoại"
},
email: "Không đúng định dạng email",
contacterEmail: "Không đúng định dạng email"
}
});
/*
* bắt buộc input file với name="img"
* */
function UploadImage(formId,imgId,inputId) {
    var form = new FormData($("#"+formId)[0]);
    $('#'+imgId).attr('src', '/public/images/loading/loader_blue.gif');
    $.ajax({
        type: 'POST',
        url: '/cpanel/system/upload-image',
        data: form,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false
    }).done(function (data) {
        $('#'+imgId).attr('src', data.url);
        $('#'+inputId).val(data.url);
    });
}
</script>
<style>
.error_border {
color: #aa0000;
}
</style>
@endsection