@extends('cpanel.template.layout')
@section('title', 'Danh sách nhân viên')

@section('content')
<?php
    function getNameDepartment($id,$list){
        for($i=0;$i<count($list);$i++){
            if($id==$list[$i]['departmentId']){
                return $list[$i]['departmentName'];
            }
        }
        return "";
    }

    function getGroupPermissionName($id,$list){
        for($i=0;$i<count($list);$i++){
            if($id==$list[$i]['groupPermissionId']){
                return $list[$i]['groupPermissionName'];
            }
        }
        return "";
    }

?>
<style>
    .red{
        color: red;
    }
</style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Danh Sách Nhân Viên</h3></div>
            </div>
        </div>
        <div class="separator bottom"></div>
        <div class="innerLR">
            <div class="row-fluid">
                <div class="control-box">
                    <form action="">
                        <div class="row-fluid">
                            <div class="span4">
                                <label for="name_employee">Họ & Tên</label>
                                <input type="text" name="fullName" id="name_employee" value="{{request('fullName')}}">
                            </div>
                            <div class="span4">
                                <label for="home_town_employee">Quê Quán</label>
                                <input type="text" name="homeTown" id="home_town_employee"
                                       value="{{request('homeTown')}}">
                            </div>
                            <div class="span3">
                                <label class="separator hidden-phone" for="add"></label>
                                <button class="btn btn-info btn-flat-full" id="search">TÌM HỒ SƠ</button>
                            </div>
                        </div>


                        <div class="row-fluid">
                            <div class="span4">
                                <label for="phone_employee">Số điện thoại</label>
                                <input type="number" name="phoneNumber" id="phone_employee"
                                       value="{{request('phoneNumber')}}">
                            </div>
                            <div class="span4">
                                <label for="id_card_employee">Số chứng minh nhân dân</label>
                                <input type="number" name="identityCardNumber" id="id_card_employee"
                                       value="{{request('identityCardNumber')}}">
                            </div>
                            @if(hasAnyRole(CREATE_NEW_EMPLOYEE))
                                <div class="span3">
                                    <label class="separator hidden-phone" for="add"></label>
                                    <a href="{{action('UserController@add')}}"
                                       class="btn btn-warning btn-flat-full" id="add">THÊM NHÂN VIÊN</a>
                                </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
            <div class="row-fluid">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Họ & Tên</th>
                        <th>Chức vụ</th>
                        <th>Điện thoại</th>
                        <th>Giới tính</th>
                        <th>Trạng thái</th>
                        <th class="center">Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>

                    @forelse($result as $row)
                        <tr>
                            <td>{{$row['fullName']}}</td>
                            <td>
                                @ChucVu($row['userType'])
                            </td>
                            <td>{{'+'.$row['stateCode'].@$row['phoneNumber']}}</td>
                            <td>@checkSex($row['sex'])</td>
                            <td> <?php if($row['userStatus']==2) echo 'Khóa';else echo "Đang hoạt động";?></td>
                            <td class="center">
                                <a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                    <ul class="onclick-menu-content">
                                        <li>
                                            <button onclick="showInfo(this);">Chi tiết</button>
                                        </li>
                                        @if(hasAnyRole(UPDATE_EMPLOYEE))
                                            <li>
                                                <button onclick="window.location.href='{{action('UserController@getEdit',['id'=>$row['userId']])}}'">
                                                    Sửa
                                                </button>
                                            </li>
                                        @endif
                                        @if(hasAnyRole(DELETE_EMPLOYEE))
                                            <li>
                                                <button class="LockUser" value="{{$row['userId']}}">@if($row['userStatus']==2) Mở khóa @else Khóa tài khoản @endif</button>
                                            </li>
                                            <li>
                                                <button class="DeleteUser" value="{{$row['userId']}}">Xóa</button>
                                            </li>
                                        @endif
                                    </ul>
                                </a>
                            </td>
                        </tr>
                        <tr class="info" id="chitiet_{{$row['userId']}}" style="display: none">
                            <td colspan="5">
                                <table class="tttk" cellspacing="20" cellpadding="4">
                                    <tbody>
                                    <tr>
                                        <th>HỌ TÊN</th>
                                        <td>
                                            {{$row['fullName']}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>NGÀY SINH</th>
                                        <td>@dateFormat($row['birthDay'])</td>
                                    </tr>
                                    <tr>
                                        <th>GIỚI TÍNH</th>
                                        <td>@checkSex($row['sex'])</td>
                                    </tr>
                                    <tr>
                                        <th>SỐ ĐIỆN THOẠI</th>
                                        <td>{{'+'.$row['stateCode'].@$row['phoneNumber']}}</td>
                                    </tr>
                                    <tr>
                                        <th>QUÊ QUÁN</th>
                                        <td>{{@$row['homeTown']}}</td>
                                    </tr>
                                     <tr>
                                        <th>NGÀY VÀO CÔNG TY</th>
                                        <td>@dateFormat($row['joinDate'])</td>
                                    </tr>
                                    <tr>
                                        <th>MÃ NHÂN SỰ</th>
                                        <td>{{@$row['code']}}</td>
                                    </tr>
                                    <tr>
                                        <th>PHÒNG BAN</th>
                                        <td>{{ getNameDepartment(@$row['departmentId'],$listDepartment) }}</td>
                                    </tr>
                                    <tr>
                                        <th>NHÓM QUYỀN</th>
                                        <td>{{ getGroupPermissionName(@$row['groupPermissionId'],$groupPermissions)}}</td>
                                    </tr>
                                    <tr>
                                        <th>CHỨC VỤ</th>
                                        <td>@ChucVu($row['userType'])</td>
                                    </tr>
                                    @if($row['userType'] == 2)
                                    
                                    <tr>
                                        <th>BẰNG LÁI XE</th>
                                        <td>{{@$row['licenceLevel']}}</td>
                                    </tr>
                                    <tr>
                                        <th>SỐ BẰNG LÁI</th>
                                        <td>{{@$row['licenceCode']}}</td>
                                    </tr>
                                    <tr>
                                        <th>NGÀY HẾT HẠN BẰNG LÁI</th>
                                        <td>@dateFormat(@$row['licenceExpired'])</td>
                                    </tr>
                                    @else
                                    <tr>
                                        <th>HỌC VẤN</th>
                                        <td>{{@$row['education']}}</td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <th>CMND</th>
                                        <td>{{@$row['identityCard']['identityCardNumber']}}</td>
                                    </tr>
                                    <tr>
                                        <th>EMAIL</th>
                                        <td>{{@$row['email']}}</td>
                                    </tr>
                                    <tr>
                                        <th>KHI CẦN BÁO TIN CHO AI</th>
                                        <td>{{@$row['contactInfo']['fullName']}}</td>
                                    </tr>
                                    <tr>
                                        <th>SỐ ĐIỆN THOẠI NGƯỜI BÁO TIN</th>
                                        <td>{{@$row['contactInfo']['phoneNumber']}}</td>
                                    </tr>
                                    <tr>
                                        <th>EMAIL NGƯỜI CẦN BÁO TIN</th>
                                        <td>{{@$row['contactInfo']['email']}}</td>
                                    </tr>
                                    <tr>
                                        <th>SỐ ĐIỆN THOẠI NGƯỜI GIỚI THIỆU</th>
                                        <td>{{@$row['phoneNumberReferrer']}}</td>
                                    </tr>
                                    <tr>
                                        <th>TÀI KHOẢN</th>
                                        <td>{{@$row['userName']}}</td>
                                    </tr>
                                    <tr>
                                        <th>MẬT KHẨU</th>
                                        <td>**********</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td colspan="1">
                                <div class="avatar">
                                    <img src="{{$row['avatar']}}">
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td class="center" colspan="6">Hiện không có dữ liệu</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
                {{--@if($page>=1 && $countUser >= RECORD_PER_PAGE)--}}
                    @include('cpanel.template.pagination-without-number',['page'=>$page])
                {{--@endif--}}
            </div>
        </div>
    </div>
    @if(hasAnyRole(DELETE_EMPLOYEE))
        <div class="modal hide fade" id="modal_delete" style="width: 250px; margin-left: -10%;margin-top: 10%;">
            <div class="modal-body center">
                <p>Bạn có chắc muốn xoá?</p>
            </div>
            <form action="{{action('UserController@delete')}}" method="post">
                <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="userId" value="" id="delete_user">
                    <a style="cursor: pointer" data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                    <button class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
                </div>
            </form>
        </div>
        <script>
            $(document).ready(function(){
                $('body').on('click','.DeleteUser',function(){
                    $('#modal_delete').find('form').attr('action','{{ action('UserController@delete') }}');
                    $('#modal_delete').find('.modal-body').find('p').text('Bạn có chắc chắn muốn xóa!');
                    $('#delete_user').val($(this).val());
                    $('#modal_delete').modal('show');
                });
                $('body').on('click','.LockUser',function(){
                    $('#modal_delete').find('form').attr('action','{{ action('UserController@lock') }}');
                    if($(this).text()===' Mở khóa '){
                        $('#modal_delete').find('.modal-body').find('p').text('Bạn có chắc chắn muốn mở Khóa!');
                    }else{
                        $('#modal_delete').find('.modal-body').find('p').text('Bạn có chắc chắn muốn Khóa!');
                    }
                    $('#delete_user').val($(this).val());
                    $('#modal_delete').modal('show');
                });
            });
        </script>
    @endif
    <!-- End Content -->

@endsection