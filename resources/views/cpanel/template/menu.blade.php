<style>
    li {
        cursor: pointer;
    }
</style>
<div id="menu">
    <div id="menuInner">
        <!-- Scrollable menu wrapper with Maximum height -->
        <div class="logo-user">
            <img src="{{session('userLogin')['logo']}}">
        </div>
        <div class="slim-scroll">
            <ul class="check_active_link">
                {{--<li class="heading"><span>Category</span></li>--}}
                <li class="glyphicons train active"><a
                            href="{{action('HomeController@index')}}"><i></i><span>Home</span></a></li>
                @if(hasAnyRole([SELL_TICKET,GET_LIST_TICKET,GETLIST_PROMOTION]))
                    <li class="hasSubmenu">
                        @if(hasAnyRole(SELL_TICKET))
                            <a class="glyphicons notes" href="{{action('TicketController@sellVer1')}}"
                            ><i></i><span>Bán vé</span><span
                                        class="dcjq-icon"></span></a>
                        {{--@elseif(hasAnyRole(GET_LIST_TICKET))--}}
                            {{--<a class="glyphicons notes" href="{{action('TicketController@show',['type'=>1])}}"--}}
                            {{--><i></i><span>Danh sách vé</span><span--}}
                                        {{--class="dcjq-icon"></span></a>--}}
                        @elseif(hasAnyRole(GETLIST_PROMOTION))
                            <a class="glyphicons notes" href="{{action('CouponController@show')}}"
                            ><i></i><span>Mã khuyến mãi</span><span
                                        class="dcjq-icon"></span></a>
                        @endif
                        <ul class="in collapse" id="menu_components">
                            @if(hasAnyRole(SELL_TICKET))
                                <li><a href="{{action('TicketController@sellVer1')}}" class="glyphicons">
                                        <i></i><span>Bán vé</span></a>
                                </li>
                            @endif

                            {{--@if(hasAnyRole(GET_LIST_TICKET))--}}
                                {{--<li><a href="{{action('TicketController@show',['type'=>1])}}" class="glyphicons">--}}
                                        {{--<i></i><span>Danh sách vé</span></a>--}}
                                {{--</li>--}}
                            {{--@endif--}}
                            @if(hasAnyRole(GETLIST_PROMOTION))
                                <li><a href="{{action('CouponController@show')}}" class="glyphicons">
                                        <i></i><span>Mã khuyến mãi</span></a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif

                {{--<li class="hasSubmenu ">--}}
                {{--<a class="glyphicons briefcase"--}}
                {{--><i></i><span>Gửi đồ</span>--}}
                {{--<span class="dcjq-icon"></span></a>--}}
                {{--<ul class="in collapse" id="menu_forms">--}}
                {{--<li>--}}
                {{--<a href="{{action('ShipController@show')}}"--}}
                {{--class="glyphicons"><span>Danh sách gửi đồ</span></a>--}}
                {{--</li>--}}
                {{--<li><a href="{{action('ShipController@sell')}}"--}}
                {{--class="glyphicons"><span>Gửi đồ mới</span></a>--}}
                {{--</li>--}}
                {{--<li><a href="{{action('ShipController@typeShip')}}"--}}
                {{--class="glyphicons"><span>Loại đồ</span></a>--}}
                {{--</li>--}}
                {{--</ul>--}}
                {{--</li>--}}
                <li class="hasSubmenu ">
                    <a class="glyphicons briefcase"><i></i><span>Gửi đồ</span><span class="dcjq-icon"></span></a>
                    <ul class="in collapse" id="menu_forms">
                        {{--<li>--}}
                            {{--<a href="{{action('ShipNewController@show')}}" class="glyphicons"><span>Gửi đồ</span></a>--}}
                        {{--</li>--}}
                        <li>
                            <a href="{{url('cpanel/vue/goods')}}" class="glyphicons"><span>Gửi đồ</span></a>
                        </li>
                        <li>
                            <a href="{{action('ShipController@typeShip')}}" class="glyphicons"><span>Loại đồ</span></a>
                        </li>
                        {{--<li>--}}
                            {{--<a href="{{url('cpanel/vue/home')}}" class="glyphicons"><span>Vue</span></a>--}}
                        {{--</li>--}}
                    </ul>
                </li>
                {{--@if(hasAnyRole([GET_LIST_OUT_OF_STATION_COMMAND,GET_LIST_TRIP_ACTIVITY, OUT_OF_STATION_COMMAND]))--}}
                    {{--<li class="hasSubmenu ">--}}
                        {{--@if(hasAnyRole([GET_LIST_OUT_OF_STATION_COMMAND, OUT_OF_STATION_COMMAND]))--}}
                            {{--<a class="glyphicons bus"--}}
                            {{--><i></i><span>Điều hành xe</span>--}}
                                {{--<span class="dcjq-icon"></span></a>--}}
                        {{--@elseif(hasAnyRole(GET_LIST_TRIP_ACTIVITY) && session('userLogin')['userInfo']['userType'] === ORGANIZATION_ADMIN)--}}
                            {{--<a class="glyphicons bus"--}}
                            {{--><i></i><span>Nhật trình xe</span>--}}
                                {{--<span class="dcjq-icon"></span></a>--}}
                        {{--@endif--}}
                        {{--<ul class="in collapse" id="menu_forms">--}}
                            {{--@if(hasAnyRole([GET_LIST_OUT_OF_STATION_COMMAND, OUT_OF_STATION_COMMAND]))--}}
                                {{--<li><a href="{{action('TripController@control',['option'=>1])}}"--}}
                                       {{--class="glyphicons"><span>Điều hành xe</span></a>--}}
                                {{--</li>--}}
                            {{--@endif--}}

                            {{--@if(hasAnyRole(GET_LIST_TRIP_ACTIVITY) && session('userLogin')['userInfo']['userType'] === ORGANIZATION_ADMIN)--}}
                                {{--<li><a href="{{action('TripController@log')}}"--}}
                                       {{--class="glyphicons"><span>Nhật trình xe</span></a>--}}
                                {{--</li>--}}
                            {{--@endif--}}
                            {{--@if(hasAnyRole(GET_LIST_OUT_OF_STATION_COMMAND))--}}
                                {{--<li><a href="{{action('TripController@getAddPriceHoliday')}}"--}}
                                       {{--class="glyphicons"><span>Bảng giá lễ tết</span></a>--}}
                                {{--</li>--}}
                            {{--@endif--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                {{--@endif--}}
                @if(hasAnyRole([GET_LIST_TRIP_FOR_USER_TRIP,GET_LIST_SCHEDULE,CREATE_SCHEDULE]))
                    <li class="hasSubmenu">
                        @if(hasAnyRole(GET_LIST_TRIP_FOR_USER_TRIP))
                            <a class="glyphicons calendar"><i></i><span>Lập lịch chạy xe</span>
                                <span class="dcjq-icon"></span> </a>
                        @elseif(hasAnyRole(GET_LIST_SCHEDULE))
                            <a class="glyphicons calendar"><i></i><span>Danh sách lịch chạy</span>
                                <span class="dcjq-icon"></span> </a>
                        @elseif(hasAnyRole(CREATE_SCHEDULE))
                            <a class="glyphicons calendar"><i></i><span>Tạo lịch chạy xe</span>
                                <span class="dcjq-icon"></span> </a>
                        @endif
                        <ul class="in collapse" id="log_tables">

                            @if(hasAnyRole(GET_LIST_TRIP_FOR_USER_TRIP))
                                <li><a href="{{action('TripController@show')}}" class="glyphicons"> <i></i><span>Danh sách chuyến đi</span></a>
                                </li>
                            @endif

                            @if(hasAnyRole(GET_LIST_SCHEDULE))
                                <li><a href="{{action('ScheduleController@show')}}" class="glyphicons"> <i></i><span>Danh sách lịch chạy</span></a>
                                </li>
                            @endif

                            @if(hasAnyRole(CREATE_SCHEDULE))
                                <li><a href="{{action('ScheduleController@getAdd')}}" class="glyphicons"> <i></i><span>Tạo lịch chạy xe</span></a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif
                @if(hasAnyRole([GET_LIST_ROUTE,CREATE_ROUTE,GET_LIST_POINT,CREATE_POINT]))
                    <li class="hasSubmenu">
                        @if(hasAnyRole(GET_LIST_ROUTE))
                            <a class="glyphicons road"><i></i><span>Quản lý tuyến đường</span><span
                                        class="dcjq-icon"></span></a>
                        @elseif(hasAnyRole(CREATE_ROUTE))
                            <a class="glyphicons car"><i></i><span>Thêm mới tuyến</span><span
                                        class="dcjq-icon"></span></a>
                        @elseif(hasAnyRole(GET_LIST_POINT))
                            <a class="glyphicons car"><i></i><span>Danh sách điểm </span><span
                                        class="dcjq-icon"></span></a>
                        @elseif(hasAnyRole(CREATE_POINT))
                            <a class="glyphicons car"><i></i><span>Thêm mới điểm</span><span
                                        class="dcjq-icon"></span></a>
                        @endif
                        <ul class="in collapse" id="lock_doc">

                            @if(hasAnyRole(GET_LIST_ROUTE))
                                <li><a href="{{action('RouteController@show')}}"
                                       class="glyphicons"><span>Danh sách tuyến</span></a>
                                </li>
                                <li><a href="{{action('RouteController@sort')}}"
                                       class="glyphicons"><span>Sắp xếp tuyến</span></a>
                                </li>
                            @endif

                            @if(hasAnyRole(CREATE_ROUTE))
                                <li><a href="{{action('RouteController@getadd')}}"
                                       class="glyphicons"><span>Thêm mới tuyến</span></a></li>
                            @endif

                            @if(hasAnyRole(GET_LIST_POINT))
                                <li><a href="{{action('PointController@show')}}"
                                       class="glyphicons"><span>Danh sách điểm</span></a></li>
                            @endif

                            @if(hasAnyRole(CREATE_POINT))
                                <li><a href="{{action('PointController@getadd')}}"
                                       class="glyphicons"><span>Thêm mới điểm</span></a></li>
                            @endif
                        </ul>
                    </li>
                @endif
                @if(hasAnyRole([GET_LIST_VEHICLE,GET_LIST_VEHICLE_TYPE,CREATE_VEHICLE]))
                    <li class="hasSubmenu">
                        @if(hasAnyRole(GET_LIST_VEHICLE))
                            <a class="glyphicons car"><i></i><span>Quản lý phương tiện</span><span
                                        class="dcjq-icon"></span></a>
                        @elseif(hasAnyRole(CREATE_VEHICLE))
                            <a class="glyphicons car"><i></i><span>Thêm mới phương tiện</span><span
                                        class="dcjq-icon"></span></a>
                        @elseif(hasAnyRole(GET_LIST_VEHICLE_TYPE))
                            <a class="glyphicons car"><i></i><span>Loại xe</span><span
                                        class="dcjq-icon"></span></a>
                        @endif
                        <ul class="in collapse" id="menu_forms">
                            @if(hasAnyRole(GET_LIST_VEHICLE))
                                <li><a href="{{action('VehicleController@show')}}" class="glyphicons"> <i></i><span>Danh sách phương tiện</span></a>
                                </li>
                            @endif

                            @if(hasAnyRole(CREATE_VEHICLE))
                                <li><a href="{{action('VehicleController@getadd')}}" class="glyphicons"> <i></i><span>Thêm mới phương tiện</span></a>
                                </li>
                            @endif

                            @if(hasAnyRole(GET_LIST_VEHICLE_TYPE))
                                <li><a href="{{action('VehicleTypeController@show')}}" class="glyphicons">
                                        <i></i><span>Loại xe</span></a></li>
                            @endif
                        </ul>
                    </li>
                @endif
                @if(hasAnyRole([GET_LIST_EMPLOYEE,CREATE_NEW_EMPLOYEE,CREATE_AUTO_NOTIFICATION,GET_LIST_GROUP_PERMISSION]))
                    <li class="hasSubmenu">
                        @if(hasAnyRole(GET_LIST_EMPLOYEE))
                            <a class="glyphicons user"
                            ><i></i><span>Quản lý nhân viên</span><span
                                        class="dcjq-icon"></span></a>
                        @elseif(hasAnyRole(CREATE_NEW_EMPLOYEE))
                            <a class="glyphicons user"
                            ><i></i><span>Thêm nhân viên</span><span
                                        class="dcjq-icon"></span></a>
                        @elseif(hasAnyRole(CREATE_AUTO_NOTIFICATION))
                            <a class="glyphicons user"
                            ><i></i><span>Tạo thông báo</span><span
                                        class="dcjq-icon"></span></a>
                        @elseif(hasAnyRole(GET_LIST_GROUP_PERMISSION))
                            <a class="glyphicons user"
                            ><i></i><span>Quản lý nhóm quyền</span><span
                                        class="dcjq-icon"></span></a>
                        @endif
                        <ul class="in collapse" id="table_employee">

                            @if(hasAnyRole(GET_LIST_EMPLOYEE))
                                <li><a href="{{action('UserController@show')}}"><span>Danh sách nhân viên</span></a>
                                </li>
                            @endif

                            @if(hasAnyRole(CREATE_NEW_EMPLOYEE))
                                <li><a href="{{action('UserController@add')}}"><span>Thêm nhân viên</span></a></li>
                            @endif

                            @if(hasAnyRole(CREATE_AUTO_NOTIFICATION))
                                <li><a href="{{action('NotificationController@user')}}"><span>Tạo thông báo</span></a>
                                </li>
                            @endif

                            @if(hasAnyRole(GET_LIST_GROUP_PERMISSION))
                                <li>
                                    <a href="{{action('GroupPermissionController@show')}}"><span>Quản lý nhóm quyền</span></a>
                                </li>
                            @endif

                            @if(hasAnyRole(CREATE_GROUP_PERMISSION))
                                <li>
                                    <a href="{{action('GroupPermissionController@getadd')}}"><span>Thêm mới nhóm quyền</span></a>
                                </li>
                            @endif

                            <li>
                                <a class=""
                                   href="{{action('DepartmentController@show')}}"><i></i><span>Quản lý phòng Ban</span></a>
                            </li>

                        </ul>
                    </li>
                @endif
                {{--@if(hasAnyRole([GET_LIST_CUSTOMER,GET_LIST_FEEDBACK]))--}}
                    {{--<li class="hasSubmenu">--}}
                        {{--@if(hasAnyRole(GET_LIST_CUSTOMER))--}}
                            {{--<a class="glyphicons group"><i></i><span>Quản lý khách hàng</span><span--}}
                                        {{--class="dcjq-icon"></span></a>--}}
                        {{--@elseif(hasAnyRole(GET_LIST_FEEDBACK))--}}
                            {{--<a class="glyphicons group"><i></i><span>Phản hồi khách hàng</span><span--}}
                                        {{--class="dcjq-icon"></span></a>--}}
                        {{--@endif--}}

                        {{--<ul class="in collapse" id="customer">--}}
                            {{--@if(hasAnyRole(GET_LIST_CUSTOMER))--}}
                                {{--<li>--}}
                                    {{--<a href="{{action('CustomerController@show')}}"><span>Danh sách khách hàng</span></a>--}}
                                {{--</li>--}}
                            {{--@endif--}}

                            {{--@if(hasAnyRole(GET_LIST_FEEDBACK))--}}
                                {{--<li><a href="{{action('FeedbackController@show')}}"><span>Phản hồi khách hàng</span></a>--}}
                                {{--</li>--}}
                            {{--@endif--}}

                            {{--@if(hasAnyRole(GET_LIST_FEEDBACK))--}}
                                {{--<li>--}}
                                    {{--<a href="{{action('NotificationController@customer')}}"><span>SMS chăm sóc khách hàng</span></a>--}}
                                {{--</li>--}}
                            {{--@endif--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                {{--@endif--}}
                @if(hasAnyRole(GET_LIST_AGENCY))
                    <li class="hasSubmenu">
                        <a class="glyphicons global"
                        ><i></i><span>Quản lý đại lý</span><span
                                    class="dcjq-icon"></span></a>
                        <ul class="in collapse" id="agency">
                            <li><a href="{{action('AgencyController@show')}}"><span>Danh sách đại lý</span></a></li>
                            <li><a href="{{action('AgencyController@getViewLevelAgency')}}"><span>Thêm cấp đại lý</span></a>
                            </li>
                            <li>
                                <a href="{{action('AgencyController@getViewPolicyAgency')}}"><span>Chính sách đại lý</span></a>
                            </li>
                        </ul>
                    </li>
                @endif
                @if(hasAnyRole([GET_LIST_RECEIPT_AND_PAYMENT,REPORT_RECEIPT_AND_PAYMENT]))
                    {{--@if(false)--}}
                    <li class="hasSubmenu">
                        {{--@if(hasAnyRole(GET_LIST_RECEIPT_AND_PAYMENT))--}}
                        <a class="glyphicons coins"><i></i><span>Quản lý thu chi</span><span
                                    class="dcjq-icon"></span></a>
                        {{--@elseif(hasAnyRole(REPORT_RECEIPT_AND_PAYMENT))
                            <a class="glyphicons coins" href="{{action('BalanceSheetController@report')}}"><i></i><span>Bảng tổng hợp thu chi</span><span
                                        class="dcjq-icon"></span></a>--}}
                        {{--@endif--}}
                        <ul class="in collapse" id="receipt">

                            @if(hasAnyRole(GET_LIST_RECEIPT_AND_PAYMENT))
                                <li>
                                    <a href="{{url('cpanel/vue/bill')}}"><span>Quản lí thu chi</span></a>
                                </li>
                                <li>
                                    <a href="{{url('cpanel/vue/bill-create?billType=1')}}"><span>Tạo Phiếu thu</span></a>
                                </li>
                            @endif

                            @if(hasAnyRole(GET_LIST_RECEIPT_AND_PAYMENT))
                                <li><a href="{{url('cpanel/vue/bill-create?billType=2')}}"><span>Tạo Phiếu chi</span></a>
                                </li>
                            @endif

                            {{--@if(hasAnyRole(REPORT_RECEIPT_AND_PAYMENT))
                                <li>
                                    <a href="{{action('BalanceSheetController@report')}}"><span>Bảng tổng hợp thu chi</span></a>
                                </li>
                            @endif--}}
                        </ul>
                    </li>
                @endif
                <li class="glyphicons train"><a
                            href="{{action('PolicyController@getView')}}"><i></i><span>Chính sách giá</span></a></li>
                @if(hasAnyRole([GET_LIST_ITEMS]))
                    <li class="hasSubmenu">
                        <a class="glyphicons show_big_thumbnails"><i></i><span>Danh mục</span><span
                                    class="dcjq-icon"></span></a>
                        <ul class="in collapse" id="category">
                            @if(hasAnyRole(true))
                                <li><a href="{{action('CurrencyController@show')}}"><span>Danh mục tiền tệ</span></a>
                                </li>
                            @endif

                            @if(hasAnyRole(GET_LIST_ITEMS))
                                <li><a href="{{action('ItemController@show')}}"><span>Danh mục khoản mục</span></a></li>
                            @endif
                        </ul>
                    </li>
                @endif
                {{--@if(hasAnyRole([REPORT_MANAGEMENT]))--}}
                @if(hasAnyRole([REPORT_RECONCILIATION, REPORT_DETAIL/*, REPORT_RECEIPT_AND_PAYMENT*/]))
                    <li class="hasSubmenu">
                        @if(hasAnyRole(REPORT_DETAIL))
                            <a class="glyphicons charts"
                            ><i></i><span>Báo cáo</span>
                                <span class="dcjq-icon"></span>
                            </a>
                        @elseif(hasAnyRole(REPORT_RECONCILIATION))
                            <a class="glyphicons charts"
                            ><i></i><span>Báo cáo kết quả theo chuyến</span>
                                <span class="dcjq-icon"></span>
                            </a>
                            {{--@elseif(hasAnyRole(REPORT_RECEIPT_AND_PAYMENT))
                                <a class="glyphicons charts"
                                   href="{{action('ReportController@balanceReport')}}"><i></i><span>Tổng hợp thu chi</span>
                                    <span class="dcjq-icon"></span>
                                </a>--}}
                        @endif
                        <ul class="in collapse" id="report">

                            @if(hasAnyRole(REPORT_DETAIL))
                                <li><a href="{{action('ReportController@detail')}}">Báo cáo doanh thu chi tiết</a></li>
                            @endif

                            {{--@if(hasAnyRole(true))--}}
                            {{--<li><a href="{{action('ReportController@summaryByMonth')}}">Báo cáo tổng hợp theo tháng</a>--}}
                            {{--</li>--}}
                            {{--@endif--}}

                            {{--@if(hasAnyRole(true))--}}
                            {{--<li><a href="{{action('ReportController@summaryByDate')}}">Báo cáo tổng hợp theo ngày</a>--}}
                            {{--</li>--}}
                            {{--@endif--}}

                            {{--@if(hasAnyRole(true))--}}
                            {{--<li><a href="{{action('ReportController@reconciliationForUser')}}">Doanh thu theo chuyến</a></li>--}}
                            {{--@endif--}}

                            @if(hasAnyRole(REPORT_RECONCILIATION))
                                <li><a href="{{action('ReportController@profit')}}">Báo cáo kết quả theo chuyến</a></li>
                            @endif
                            @if(hasAnyRole(REPORT_RECONCILIATION))
                                <li><a href="{{url('cpanel/vue/trips-driver-report')}}">Báo cáo theo tài xế</a></li>
                            @endif
                            @if(hasAnyRole(GET_LIST_TICKET))
                                <li><a href="{{action('TicketController@orderOnline')}}" class="glyphicons">
                                        <i></i><span>Báo cáo thanh toán vé online</span></a>
                                </li>
                                <li><a href="{{action('ReportController@chartByMonth')}}">Thống kê theo tháng</a></li>
                                <li><a href="{{url('cpanel/vue/report-by-agency')}}">Công nợ đại lý</a></li>
                            @endif

                            {{--@if(hasAnyRole(REPORT_RECEIPT_AND_PAYMENT))
                                <li><a href="{{action('ReportController@balanceReport')}}">Tổng hợp thu chi</a></li>
                            @endif--}}
                            @if(session('companyId') == 'TC1OHntfnujP' || session('companyId') == 'TC01gWSmr8A9Qx') {{-- Interbuslines va test--}}
                            <li><a href="{{action('ReportController@accountantReport')}}">Báo cáo kế toán</a></li>
                            @endif
                            {{--<li><a href="{{action('ReportController@debtorsReport')}}">Báo cáo công nợ</a></li>--}}
                            @if(hasAnyRole(REPORT_ATTRITION_BY_VEHICLE))
                                <li><a href="{{action('ReportController@depreciationReport')}}">Báo cáo khấu hao tài
                                        sản</a></li>
                            @endif
                            <li><a href="{{action('ReportController@getPoint')}}">Báo cáo theo điểm</a></li>
                            <li><a href="{{action('ReportController@getCustomer')}}">Báo cáo khách hàng</a></li>
                            <li><a href="{{action('ReportController@messageReport')}}">Báo cáo tin nhắn</a></li>
                        </ul>
                    </li>
                @endif
                @if(hasAnyRole([CONFIG_COMPANY_INFO]))
                    <li class="hasSubmenu">
                        <a class="glyphicons settings"
                        ><i></i><span>Hệ thống</span><span
                                    class="dcjq-icon"></span></a>
                        <ul class="in collapse" id="system">
                            @if(hasAnyRole(true))
                                <li><a href="{{action('SystemController@info')}}"><span>Thông tin hệ thống</span></a>
                                </li>
                            @endif

                            @if(hasAnyRole(CONFIG_COMPANY_INFO))
                                <li><a href="{{action('SystemController@config')}}"><span>Cấu hình hệ thống</span></a>
                                </li>
                            @endif
                            @if(hasAnyRole(CONFIG_COMPANY_INFO))
                                <li>
                                    <a href="{{action('SystemController@listPhoneNumber')}}"><span>Danh sách số máy lẻ</span></a>
                                </li>
                            @endif
                            @if(hasAnyRole(false))
                                <li>
                                    <a href="{{action('SystemController@UserActivity')}}"><span>Hoạt động nhân viên</span></a>
                                </li>
                            @endif
                            <li>
                                <a href="{{action('settingListCustomerController@setup')}}" class="glyphicons">
                                    <i></i><span>Tùy chỉnh phơi</span></a>
                            </li>
                            <li>
                                <a href="{{action('settingPrintTicketController@setup')}}" class="glyphicons">
                                    <i></i><span>Tùy chỉnh in vé</span></a>
                            </li>
                            <li>
                                <a href="{{action('settingShipController@setup')}}" class="glyphicons">
                                    <i></i><span>Tùy chỉnh gửi đồ</span></a>
                            </li>
                            <li>
                                <a href="{{action('SystemController@getContentsMessage')}}" class="glyphicons">
                                    <i></i><span>Tùy chỉnh tin nhắn</span></a>
                            </li>
                            {{--<li>--}}
                                {{--<a href="{{action('PackageServiceController@getViewListPackage')}}"><span>Gia hạn gói dịch vụ</span></a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{{action('NotificationController@notify')}}"><span>Thông báo</span></a>--}}
                            {{--</li>--}}
                        </ul>
                    </li>
                @endif
            </ul>

            <p class="bq">© 2017 ANVUI.VN</p>
            <p class="bq">All right reserved</p>
        </div>
    </div>
    <!-- // Nice Scroll Wrapper END -->
</div>