<!doctype html>
<html lang="{{ app()->getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>An Vui - @yield('title')</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="/public/favicon.png">
    <!-- Styles -->


    <link href="/public/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="/public/dangnhap_skin/style.css" type="text/css" rel="stylesheet">
    <link href="/public/dangnhap_skin/custom.css" type="text/css" rel="stylesheet">
    <style>
        #wrap {
            min-height: 100%;
            height: auto !important;

        }
        #push, #footer {
            height: 11px;
        }
        .container{
            margin: 0;
            width: 100%
        }
    </style>
</head>
<body>
<div id="wrap">
    <nav class="header_top">
        <img src="/public/dangnhap_skin/logo.png">
    </nav>

    <div class="container">


        <div class="__login">
            <div class="__box_login">
                <div class="__detail_login">
                    @yield('content')
                </div>
            </div>
        </div>


    </div>

    <div id="push"></div>
</div>
<div id="footer">
    <div class="container">
        <div class="row-fluid site-footer" style="position: fixed;bottom:0px;">
            <div class="span5 offset1"> <a class="color_light" href="">Điều khoản dịch vụ</a> - <a class="color_light" href="">Chính sách riêng tư</a></div>
            <div class="span5" style="margin-left: 0">  © Anvui 2017.</div>
            <div class="span1">v0.16.9</div>
        </div>
    </div>
</div>



<script language="javascript" src="/public/javascript/javascript.js"></script>
</body>
</html>
