<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Phần mềm nhà xe - @yield('title')</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=vietnamese" rel="stylesheet">

    <!-- Icon -->
    <link rel="shortcut icon" href="/public/favicon.png">
    <!-- Bootstrap -->
    <link href="/public/bootstrap-3-full/css/bootstrap.min.css" rel="stylesheet"/>

    <!-- Gritter Notifications Plugin -->
    <!-- JQuery v1.8.2 -->
    <script src="/public/theme/scripts/plugins/system/jquery-1.8.2.min.js"></script>
    <!-- JQueryUI v1.9.2 -->
    <script src="/public/theme/scripts/plugins/system/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="/public/bootstrap-3-full/js/bootstrap.min.js"></script>

    <!-- Jquery Noty -->
    <link rel="stylesheet" href="/public/javascript/noty/lib/noty.css">
    <script src="/public/javascript/noty/lib/noty.js"></script>
    <!-- Font Awesome Free 5.0.1 -->
    <link rel="stylesheet" href="/public/theme/css/fontawesome-all.css">
    <link href="/public/theme/css/quy.css" rel="stylesheet"/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="header">
                AN VUI - Danh sách các gói dịch vụ.
        </div>
        <div class="menu">

        </div>
        <div class="content">
            @yield('content')
        </div>
    </div>
</div>
<script>
    function notyMessage(content,type,time){
        type = type!==undefined?type:'info';
        time = time!==undefined?time:3000;
        new Noty({
            type: type,
            theme: 'sunset',
            timeout: time,
            text: content
        }).show();
    }
</script>
</body>
</html>