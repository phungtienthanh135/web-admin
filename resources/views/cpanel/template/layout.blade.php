<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->
<head>

    <title>Phần mềm nhà xe - @yield('title')</title>
    <!-- Meta -->
    <meta charset="UTF-8"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=vietnamese" rel="stylesheet">

    <!-- Icon -->
    <link rel="shortcut icon" href="/public/favicon.png">
    <!-- Bootstrap -->
    <link href="/public/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/public/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet"/>

    <!-- Bootstrap Extended -->
    <link href="/public/bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="/public/bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap-responsive.min.css" rel="stylesheet">
    <link href="/public/bootstrap/extend/bootstrap-wysihtml5/css/bootstrap-wysihtml5-0.0.2.css" rel="stylesheet">

    <!-- Select2 -->
    <link rel="stylesheet" href="/public/theme/scripts/plugins/forms/select2-4.0.3/dist/css/select2.min.css"/>
    <link rel="stylesheet" href="/public/theme/scripts/plugins/forms/select2-4.0.3/dist/css/select2-bootstrap.min.css"/>
    <!-- Gritter Notifications Plugin -->
    <link href="/public/theme/scripts/plugins/notifications/Gritter/css/jquery.gritter.css?v=1.5" rel="stylesheet"/>
    <!-- JQueryUI v1.9.2 -->
    <link rel="stylesheet"
          href="/public/theme/scripts/plugins/system/jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.min.css"/>
    <!-- Glyphicons -->
    <link rel="stylesheet" href="/public/theme/css/glyphicons.css"/>
    <!-- Bootstrap Extended -->
    <link rel="stylesheet" href="/public/bootstrap/extend/bootstrap-select/bootstrap-select.css"/>
    <link rel="stylesheet"
          href="/public/bootstrap/extend/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css"/>
    <!-- Uniform -->
    <link rel="stylesheet" media="screen"
          href="/public/theme/scripts/plugins/forms/pixelmatrix-uniform/css/uniform.default.css"/>
    <!-- JQuery v1.8.2 -->
    <script src="/public/theme/scripts/plugins/system/jquery-1.8.2.min.js"></script>
    <!-- JQueryUI v1.9.2 -->
    <script src="/public/theme/scripts/plugins/system/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js"></script>

    <!-- Modernizr -->
    <script src="/public/theme/scripts/plugins/system/modernizr.custom.76094.js"></script>

    {{--colResizeable--}}
    <script src="/public/theme/scripts/plugins/tables/colResizable-1.6.min.js"></script>
    <!-- Jquery Confirm -->
    {{--sortTable--}}
    <script type="text/javascript" src="/public/javascript/tableSort/jquery.tablesorter.min.js"></script>
    {{--dragTable--}}
    <script type="text/javascript" src="/public/javascript/dragTable/drag.js"></script>

    {{--QRCode--}}
    <script type="text/javascript" src="/public/javascript/QRCode/qrCode.js"></script>
    <!-- Jquery Noty -->
        <link rel="stylesheet" href="/public/javascript/noty/lib/noty.css">
        <script src="/public/javascript/noty/lib/noty.js"></script>
    <link rel="stylesheet" href="/public/javascript/jquery.datepicker.lunar/jquery.datepicker.lunar.css">

    <script src="/public/javascript/jquery.datepicker.lunar/jquery.datepicker.lunar.js"
            type="text/javascript"></script>
<!-- Theme -->
    <link rel="stylesheet" href="/public/theme/css/jquery.amlich.css"/>
    <link rel="stylesheet" href="/public/theme/css/style.css?v=1.3"/>
    <link rel="stylesheet" href="/public/theme/css/fix.css?v=1.2"/>
    <link rel="stylesheet" href="/public/theme/css/custom.css?v=1.9"/>
    <!-- LESS 2 CSS -->
    <script src="/public/theme/scripts/plugins/system/less-1.3.3.min.js"></script>

    <!-- jQuery Validate -->
    <script src="/public/theme/scripts/plugins/forms/jquery-validation/dist/jquery.validate.min.js"
            type="text/javascript"></script>
    <!--[if IE]>
    <script type="text/javascript" src="/public/common/theme/scripts/plugins/other/excanvas/excanvas.js"></script>
    <![endif]-->
    <!--[if lt IE 8]>
    <script type="text/javascript" src="/public/common/theme/scripts/plugins/other/json2.js"></script><![endif]-->
    <!-- chuyển chữ thành số -->
    <script src="/public/javascript/DocSo.js"></script>
    <!-- jQuery Slim Scroll Plugin -->
    <script src="/public/theme/scripts/plugins/other/jquery-slimScroll/jquery.slimscroll.min.js"></script>
    <!-- Uniform -->
    <script src="/public/theme/scripts/plugins/forms/pixelmatrix-uniform/jquery.uniform.min.js"></script>
    <!-- Bootstrap Script -->
    <script src="/public/bootstrap/js/bootstrap.min.js"></script>
    <!-- Bootstrap Extended -->
    <script src="/public/bootstrap/extend/bootstrap-select/bootstrap-select.js"></script>
    <script src="/public/bootstrap/extend/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
    <!-- Gritter Notifications Plugin -->
    <script src="/public/theme/scripts/plugins/notifications/Gritter/js/jquery.gritter.min.js?v=1.0"></script>
    <!-- Select2 -->
    <script src="/public/theme/scripts/plugins/forms/select2-4.0.3/dist/js/select2.full.js"></script>
    <script src="/public/theme/scripts/plugins/forms/select2-4.0.3/dist/js/i18n/vi.js"
            type="application/javascript"></script>
    <script src="/public/javascript/jquery.number.min.js" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <!-- Clockpicker -->
    <link rel="stylesheet" type="text/css" href="/public/javascript/clockpicker/dist/bootstrap-clockpicker.min.css">
    <script type="text/javascript" src="/public/javascript/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
    {{--socket--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.js"></script>
    <!-- Font Awesome Free 5.0.1 -->
    <link rel="stylesheet" href="/public/theme/css/fontawesome-all.css">
    <script src="https://www.gstatic.com/firebasejs/4.6.2/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.6.2/firebase-messaging.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.6.2/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.6.2/firebase-database.js"></script>
    @if(session("apiId") != '' && session("telecomAPIKey") != '')
        <script src="https://cdn.taduphone.vn/js/taduphone-js-ws-hook-0.0.1.min.js"></script>
        <script src="https://cdn.taduphone.vn/js/taduphone-js-client-0.0.1.min.js"></script>
    @endif
    <script>
        var userLogin = {
            userId : '{{ session('userLogin')['userInfo']['userId'] }}'
        }
        function urlDBD(path){
            @if(ENVIROMENT == 'dev')
            var bashURL  = "https://test-anvui.appspot.com/";
            @else
            var bashURL =  "https://dobody-anvui.appspot.com/";
            @endif
            return bashURL+path;
        }
        function SendAjaxWithJson(data) {
            if(typeof data.headers==='undefined'){
                data.headers = {
                    'DOBODY6969':'{{ session('userLogin')['token']['tokenKey'] }}',
                };
            }
            data.contentType="application/json; charset=utf-8";
            data.data = JSON.stringify(data.data);
            data.error = function(result){
                if(typeof result!=='undefined' && typeof result.responseText!=='undefined'&&typeof errorObject!=='undefined'){
                    err = JSON.parse(result.responseText);
                    if(typeof errorObject[err.results.error.message]!=="undefined"){
                        notyMessage(errorObject[err.results.error.message],"error");
                    }else{
                        notyMessage("Có lỗi xảy ra vui lòng thử lại","error");
                    }
                }
                if(typeof data.functionIfError==="function"){
                    var err = data.functionIfError(result);
                }
            }

            $.ajax(data);
        }
        function SendAjaxWithParam(data) {
            if(typeof data.headers==='undefined'){
                data.headers = {
                    'DOBODY6969':'{{ session('userLogin')['token']['tokenKey'] }}',
                };
            }
//            data.contentType="application/json; charset=utf-8";
//            data.data = JSON.stringify(data.data);
            data.error = function(result){
                if(typeof data.functionIfError==="function"){
                    var err = data.functionIfError(result);
                }
            }

            $.ajax(data);
        }
        @if(ENVIROMENT == 'dev')
        var config = {
            apiKey: "AIzaSyAgIveNpwRVRulCyDywOuZrbLdzn0gBZpM",
            authDomain: "test-anvui.firebaseapp.com",
            databaseURL: "https://test-anvui.firebaseio.com",
            storageBucket: "test-anvui.appspot.com",
            messagingSenderId: "1092280688930"
        };
        @else
        var config = {
                apiKey: "AIzaSyDanMfIbFRjuYY0QcIc8A-yMH_0PtI7dlk",
                authDomain: "dobody-anvui.firebaseapp.com",
                databaseURL: "https://dobody-anvui.firebaseio.com",
                storageBucket: "dobody-anvui.appspot.com",
                messagingSenderId: "999268943503"
            };
        @endif
        firebase.initializeApp(config);
        const messaging = firebase.messaging();

        Date.prototype.customFormat = function(formatString){
            var YYYY,YY,MMMM,MMM,MM,M,DDDD,DDD,DD,D,hhhh,hhh,hh,h,mm,m,ss,s,ampm,AMPM,dMod,th;
            YY = ((YYYY=this.getFullYear())+"").slice(-2);
            MM = (M=this.getMonth()+1)<10?('0'+M):M;
            MMM = (MMMM=["January","February","March","April","May","June","July","August","September","October","November","December"][M-1]).substring(0,3);
            DD = (D=this.getDate())<10?('0'+D):D;
            DDD = (DDDD=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][this.getDay()]).substring(0,3);
            th=(D>=10&&D<=20)?'th':((dMod=D%10)==1)?'st':(dMod==2)?'nd':(dMod==3)?'rd':'th';
            formatString = formatString.replace("#YYYY#",YYYY).replace("#YY#",YY).replace("#MMMM#",MMMM).replace("#MMM#",MMM).replace("#MM#",MM).replace("#M#",M).replace("#DDDD#",DDDD).replace("#DDD#",DDD).replace("#DD#",DD).replace("#D#",D).replace("#th#",th);
            h=(hhh=this.getHours());
            if (h==0) h=24;
            if (h>12) h-=12;
            hh = h<10?('0'+h):h;
            hhhh = hhh<10?('0'+hhh):hhh;
            AMPM=(ampm=hhh<12?'am':'pm').toUpperCase();
            mm=(m=this.getMinutes())<10?('0'+m):m;
            ss=(s=this.getSeconds())<10?('0'+s):s;
            return formatString.replace("#hhhh#",hhhh).replace("#hhh#",hhh).replace("#hh#",hh).replace("#h#",h).replace("#mm#",mm).replace("#m#",m).replace("#ss#",ss).replace("#s#",s).replace("#ampm#",ampm).replace("#AMPM#",AMPM);
        };
        Number.prototype.format = function(n, x) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
            return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
        };
    </script>
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--}}
    {{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}
    <style>
        .modalfix {
            width: 70%;
            height: 550px;
            position: absolute;
            margin-right: 50%;
            align-self: center;
            margin-top: 0px;
        }

        #ui-id-1 {
            width: 80% !important;
            top: 100px !important;
            left: 17% !important;
            z-index: 99999 !important;
        }

        #ui-id-2 {
            top: 75px !important;
            left: 44% !important;
            width: 55% !important;
        }

        .ui-autocomplete-loading {
            background: url('/public/images/loading/loading.gif') no-repeat right center;
            background-size: 20px 20px;
        }

        .divSearch1 {
            width: 25%;
            float: left;
        }

        .divSearch2 {
            width: 42%;
            float: left;
        }

        .divSearch3 {
            width: 20%;
            float: left;
        }

        .divSearch4 {
            width: 13%;
            float: left;
        }

        .fullheight {
            height: 100%;
        }

        .name {
            color: #9f3a38;
        }

        .code {
            color: #00aa00;
        }

        .phone {
            color: #0A246A;
        }

        .margin-right-10{margin-right: 10px}

        .borderlist {
            border-bottom: 1px solid #eee;
        }
        #history-calling{
            margin-top:10px;
            height:400px;
            background: #ddd;
            overflow-y: scroll;
        }
        #history-calling ul{
            padding:0px;
            margin:0px
        }
        .list-group {
            padding-left: 0;
            margin: 0px;
        }
        .list-group-item {
            position: relative;
            display: block;
            padding: 10px 15px;
            margin-bottom: -1px;
            background-color: #fff;
            border: 1px solid #ddd;
        }
        .list-group-item:hover{background:#428bca }
        .list-group-item:first-child {
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
        }
        .list-group-item:last-child {
            margin-bottom: 0;
            border-bottom-right-radius: 4px;
            border-bottom-left-radius: 4px;
        }
        .list-group-item.active,
        .list-group-item.active:hover,
        .list-group-item.active:focus {
            z-index: 2;
            color: #fff;
            background-color: #428bca;
            border-color: #428bca;
        }

        .superloading{
            width: 100%;
            height: 100vh;
            z-index: 999999999999;
            position: fixed;
            top:0px;
            background: #333;
            display: none;
            opacity: 0.5;
        }
        .superloading .loader{
            margin: auto;
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }
        .ghe .loader{
            margin: auto;
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            width: 20px;
            height: 20px;
            border: 5px solid #ddd;
            border-top: 5px solid #0000CC;
            border-bottom: 5px solid #0000CC;
        }
        .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #0000CC;
            border-bottom: 16px solid #0000CC;
            width: 50px;
            height: 50px;
            -webkit-animation: spin 1s linear infinite;
            animation: spin 1s linear infinite;
        }
        .fixed-bot-left{
            height: 100vh;
            position: fixed;
            width: 234px;
            bottom:0;
            left:-234px;
            background: #525050;
            transition: left 0.5s;
            z-index: 100001;
        }
        .fixed-bot-left *::-webkit-scrollbar {
            width: 0;
        }
        .fixed-bot-left hr{
            border-top:1px solid #676464;
            border-bottom:1px solid #676464;
        }
        .open-box-phone{left: 0;transition: left 0.5s}
        .box-phone-content{height: 100%;overflow-y: scroll}
        .open-close-phone{position: absolute;color:#fff;top:50%;right:-30px;height: 30px;width: 30px; background: #e40d47;line-height: 30px;border-top: 1px;border-right:1px;border-bottom:1px;text-align: center;border-radius: 0 15px 15px 0}
        .open-close-phone:hover{background: #d6de65;color:#e40d47}
        .customer-calling-status{color:#ddd}
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
        @media (max-width: 320px) {
            .modal.fade.in {
                width: 96% !important;
                top: 20px;
                left: 0px;
            }
        }
        @media (max-width: 425px) and (min-width: 321px) {
            .modal.fade.in {
                width: 96% !important;
                top: 20px;
                left: 10px;
            }
        }
    </style>
</head>
<body>
<div class="superloading">
    <div class="loader"></div>
</div>
<!-- Start Content - Menu left -->
<div class="container-fluid">
    <div class="row-fluid" id="scroll">
        <div class="navbar main hidden-print">
            @if(count(session('userLogin')["userInfo"]['listAgency'])<=0)
                <div id="ht_btn_close_open_menu">
                    <button onClick="close_menu()" type="button" class="btn-navbar">
                        <div class="icon_close_menu"></div>
                    </button>
                </div>
            @endif

            {{--<img src="/public/dangnhap_skin/logo.png"/>--}}
            <ul class="topnav pull-right">
                <li>
                    <ul class="notif">
                        {{--<li class="dropdown visible-desktop" data-toggle="tooltip" data-placement="bottom"
                            data-original-title="5 new messages"> <a href="" data-toggle="dropdown" class="glyphicons envelope"><i></i> <span
                                        class="ht_noti_count">5</span></a>
                            <ul class="dropdown-menu pull-right notification_app">
                                <div class="log-arrow-up"></div>
                                <div class="header_noti">
                                    <h3 class="uiHeaderTitle" aria-hidden="true">Thông báo</h3>
                                </div>
                                <div class="ht_noti">
                                    <div class="noti_body">
                                        <div class="ht_img"> <img src=""
                                                                  height="52" width="52"> </div>
                                        <div class="ng_noti"> <a class="name_user">Hoàng Long</a> <span class="date_noti">03/04/2017</span>
                                            <p>Chúng tôi đã nhận được yêu cầu đặt lại mật khẩu Uber của bạn. Nhấp vào liên
                                                kết bên dưới để chọn.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="header_noti">
                                    <div class="text-center"> <a href="#xemtatca"><b>Xem hết thư</b></a> </div>
                                </div>
                            </ul>
                        </li>--}}
                        <li class="">
                            <a href="" id="goHelp" data-toggle="modal" data-target="#help"
                               class="glyphicons iconic-question-mark"></a>
                        </li>
                        <li class="account"><a data-toggle="dropdown" href="" class="glyphicons user"><i> </i> </a>
                            <ul class="dropdown-menu pull-right infor_account">
                                <li><a href="{{action('HomeController@account')}}" class="glyphicons user"><i
                                                class="icon_left"> </i> <span
                                                class="m_left_30">Thông tin tài khoản</span></a></li>
                                {{--@if(hasAnyRole(CHANGE_PASSWORD))--}}
                                <li><a style="cursor:pointer" data-toggle="modal" data-target="#ChangePass"><i
                                                class="icon_left glyphicon glyphicon-lock"> </i> <span
                                                class="m_left_10">Đổi mật khẩu</span></a>
                                </li>
                                <li><a href="{{action('HomeController@userLog')}}" class="glyphicons history" id="userActionHistory"><i
                                                class="icon_left"> </i> <span
                                                class="m_left_30">Lịch sử thao tác</span></a></li>
                                {{--@endif--}}
                            </ul>
                        </li>
                        <li>
                            <a data-toggle="modal" data-target="#Logout" style="cursor:pointer"
                               class="glyphicons power"><i></i></a>
                        </li>
                        <li onClick="focustimkiem()" class="dropdown visible-desktop">
                            <a href="" data-toggle="dropdown" class="glyphicons search"><i></i></a>
                            <ul class="dropdown-menu pull-right">
                                <div class="log-arrow-up"></div>
                                <li>
                                    <form class="form-inline">
                                        <div class="input-append">
                                            <input name="keyword" type="text" class="txtsearch"
                                                   id="txtsearch"
                                                   placeholder="Mã vé/Tên khách hàng/Số điện thoại">
                                            <button type="button" id="quick-search" style="height: 30px;"
                                                    class="btn-default btn-flat"><i
                                                        class="icon-search"></i> Tìm
                                            </button>
                                        </div>
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>

        </div>
    </div>
    <div class="row-fluid">
        @if(count(session('userLogin')["userInfo"]['listAgency'])<=0)
            @include('cpanel.template.menu')
        @endif

        @yield('content')
    </div>

</div>
{{--@if(hasAnyRole(CHANGE_PASSWORD))--}}
<!--modal doi mat khau-->
<div class="modal modal-custom hide fade modal_logout" id="ChangePass">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="center">Thay đổi mật khẩu</h3>
    </div>
    <form action="{{action('UserController@changePassword')}}" method="post" class="frm_changepass">
        <div class="modal-body">
            <input autocomplete="off" placeholder="Mật khẩu củ" type="password" name="oldPassword"
                   data-validation="required" data-validation-error-msg-required="Vui lòng nhập mật khẩu củ">
            <input autocomplete="off" placeholder="Mật khẩu mới" type="password" name="newPassword"
                   data-validation="required" data-validation-error-msg-required="Vui lòng nhập mật khẩu mới">
            <input autocomplete="off" placeholder="Nhập lại mật khẩu" type="password"
                   data-validation="confirmation required" data-validation-confirm="newPassword"
                   data-validation-error-msg-required="Vui lòng nhập lại mật khẩu mới chính xác"
                   data-validation-error-msg-confirmation="Mật khẩu xác nhận không chính xác">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
        </div>
        <div class="modal-footer">
            <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true"
               class="btn_huy_modal">HỦY</a>
            <button class="btn btn btn-warning btn-flat">LƯU</button>
        </div>
    </form>
</div>
{{--@endif--}}
<!--modal dang xuat-->
<div class="modal hide fade modal_logout" id="Logout">
    <div class="modal-body center">
        <p>Bạn có chắc muốn đăng xuất?</p>
    </div>
    <div class="modal-footer">
        <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
        <a href="{{route('cpanel.login.logout')}}" class="btn btn btn-warning btn-flat">ĐỒNG Ý</a>
    </div>
</div>
<div class=" modal hide fade " style="width: 630px" id="help">
    <div class="modal-body center">
    </div>
    <div class="modal-footer">
        <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">ĐÓNG</a>
    </div>
</div>
<script src="/public/javascript/anvui.js?v=1.9" type="text/javascript"></script>
<script>
            {!! session('msg') !!}
            @stack('activeMenu')

    var isPhone =
    {!! (session("apiId") != '' && session("telecomAPIKey") != '') ? 'true' : 'false' !!}




    if (isPhone) {
        messaging.onMessage(function (payload) {

            console.log(payload);
            /*nếu ở form bán vé ver1 thì gọi lệnh update sơ đồ ghế*/
            /*if (location.href.indexOf('cpanel/ticket/sell-ver1')) {
                updateSelectBoxTrip(false);
            }*/
            var notifyType = payload.data.notificationCode;

            var timeout = 1000;

            if (notifyType == 8) {
                timeout = false;
            }

            var content = 'Khách hàng: ' + payload.data.actorFullName + '<br />';
            content += 'Số điện thoại: ' + payload.data.notificationContent;

            new Noty({
                type: 'success',
                theme: 'sunset',
                timeout: timeout,
                text: content,
                callbacks: {
                    onClose: function () {
                        if (notifyType == 8) {
                            if (typeof(Storage) !== "undefined") {
                                sessionStorage.setItem("fullName", payload.data.actorFullName);
                                sessionStorage.setItem("phoneNumber", payload.data.notificationContent);
                            } else {
                                alert("Trình duyệt không được hỗ trợ! Hãy cập nhật bản mới nhất.");
                            }
                            window.open('{{action('TicketController@sell')}}', '_blank');
                        }
                    }
                }

            }).show();
        });
    }

    $(document).ready(function () {

        lockOrUnlockSeat("unlock");
        var socket = null;
        // close_menu();
        $('body').on('click','li.hasSubmenu',function(){
            $(this).toggleClass('active');
        });

        $('body').on('click','.open-close-phone',function(){
            $(this).parent('.fixed-bot-left').toggleClass('open-box-phone');
            if($(this).parent('.fixed-bot-left').hasClass('open-box-phone')){
                open_menu();
            }else{
                close_menu();
            }
        });
        $('body').on('click','[qs-trip-id]',function(){
            tripId = $(this).attr('qs-trip-id');
            scheduleId = $(this).attr('qs-schedule-id');
            date = $(this).attr('qs-date');
            if(tripId != ''&&tripId != undefined){
                sessionStorage.setItem('tripId',tripId);
            }
            if(scheduleId != ''&&scheduleId != undefined){
                sessionStorage.setItem('scheduleId',scheduleId);
            }
            if(date != ''&&date != undefined){
                sessionStorage.setItem('date',date);
            }

        });

        /*$('#content').mousemove(function (e) {
            if(e.pageX <= 10)
            {
                open_menu();
            }
            if(e.pageX > 10)
            {
                close_menu();
            }
        });*/
        // $('#scroll').addClass('modal');
        // $('#scroll').animate({'marginTop': "+=0px", 'top': "0%", 'left': "0%", 'margin-left': "0px"});
        // document.getElementsByTagName("body")[0].setAttribute("style", "margin-top: 50px;");
        // $(window).scroll(function () {
        //     if ($('.btn-navbar').attr("onClick") == "close_menu()") {
        //         (document.getElementById("menu")).setAttribute("style", 'padding-top:' + $(window).scrollTop() + 'px');
        //     } else
        //         (document.getElementById("menu")).setAttribute("style", 'padding-top:' + $(window).scrollTop() + 'px;margin-left:-400px');
        // });
        $('#goHelp').click(function () {
            var pathname = window.location.pathname;
            var arr = pathname.split("/");
            var file;
            if (typeof arr[2] == "undefined") {
                file = "home";
            }
            else {
                file = arr[2];
            }

            if (location.protocol == 'http:') {
                url = '{{ asset("cpanel/help") }}'
            } else {
                url = '{{ asset("cpanel/help", true) }}'
            }


            var path = url + "/" + file;

            //tao path rieng cho dieu hanh xe
            var urlTrip = window.location.href;


            if (urlTrip == "{{action('TripController@control', ['option' => 1])}}" ||
                urlTrip == "{{action('TripController@log')}}" ||
                urlTrip == "{{action('TripController@getAddPriceHoliday')}}")
                path = url + "/" + "control";
            $('#goHelp').attr('href', path);
        });

        $('#quick-search').click(function () {
            var seachText = $('#txtsearch').val();
            if (seachText != '') {
                $('#ui-id-2').show();
            }
        });
        $("#txtsearch").autocomplete({
            source: "{{ action('HomeController@quickSearch') }}",
            focus: function (event, ui) {
                //$( "#search" ).val( ui.item.title ); // uncomment this line if you want to select value to search box
                return false;
            },
            delay: 500,
            cache: false
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var inner_html = '';
            if (item.empty) {
                inner_html = 'Không có dữ liệu';

                return $("<li class='borderlist'></li>")
                    .data("item.autocomplete", item)
                    .append(inner_html)
                    .appendTo(ul);
            }
            var status;
            switch (item.ticketStatus) {
                case 0:
                    status = "<span style='color: #F44336'>Đã hủy</span>"; //do
                    break;
                case 1:
                    status = "<span style='color: #B71C1C'> Hết hạn giữ chỗ"; //do
                    break;
                case 2:
                    if (item.overTime > Date.now() || item.overTime == 0)
                        status = "<span style='color: #FF9800'>Đang giữ chỗ</span>"; //cam
                    else
                        status = "<span style='color: #B71C1C'> Hết hạn giữ chỗ"; //do

                    break;
                case 3:
                    status = "<span class='code'>Đã thanh toán</span>"; //xanh
                    break;
                case 4:
                    status = "<span style='color: #0091EA'>Đã lên xe</span>"; //xanh

                    break;
                case 5:
                    status = "Đã hoàn thành";

                    break;
                case 6:
                    status = "<span style='color: #C62828'>Quá giờ giữ chỗ</span>"; //do

                    break;
                case 7:
                    status = "<span style='color: #33691E'>Ưu tiên giữ chỗ</span>";

                    break;
                default:
                    status = '';
            }
//            var length = item.listSeatId.length;
//            if (length <= 1) {
//                length = 50;
//            } else {
//                length = length * 30;
//            }

//            if (item.ticketStatus == 2 || item.ticketStatus == 3 || item.ticketStatus == 4 || item.ticketStatus == 5 || item.ticketStatus == 7) {
                inner_html = '<a style="line-height: 20px;" class="link link-phone-phuc-xuyen" qs-trip-id="'+ item.tripId +'" qs-schedule-id="'+item.scheduleId+'" qs-date="'+covertDate(item.getInTimePlanInt)+'" href="/cpanel/ticket/sell-ver1?ticketId=' + item.ticketId + '&tripId=' + item.tripId + '&scheduleId=' + item.scheduleId + '&routeId=' + item.routeId + '&startPointId=' + item.getInPointId + '&endPointId=' + item.getOffPointId + '&date=' + covertDate(item.getInTimePlanInt) + '&ticketStatus='+item.ticketStatus+'" >' +
                    '<div class="">';
                inner_html += '<span class="dataTicket" style="display: none">' + JSON.stringify(item) + '</span>'
                inner_html+= '<span>'+ item.fullName + '</span><span style="font-weight: bold"> : ' + item.phoneNumber +'</span>,';
                inner_html+= '<span style="font-size: 11px">'+ item.routeName + '</span><span> ( ' + checkDate(item.getInTimePlan) +')</span>, ';
                inner_html+= 'Ghế : <span style="font-size: 13px;color:red">'+ item.listSeatId.toString() + '</span><span> ( ' + status +')</span>';
//                inner_html += '<div  class="divSearch1 fullheight">KH: ' + item.fullName + ' - ' + item.phoneNumber + '</div>';
//                inner_html += '<div class="divSearch2 fullheight">' + item.routeName + ' - ' + checkDate(item.getInTimePlan) + '</div>';
//                inner_html += '<div class="divSearch3 fullheight" >';
//                inner_html += item.listSeatId.toString();
//                item.listSeatId.forEach(function (element) {
//                    inner_html += element + '</br>';
//                });
//                inner_html += '</div>';
//                inner_html += '<div class="fullheight divSearch4">' + status + '</div>';

                inner_html += '</div></a>';
//            } else {
//                inner_html = '<a style="height:50px" class="link" href="/cpanel/ticket/sell-ver1" >' +
//                    '<div class="list_item_container">';
//                inner_html += '<div  class="divSearch1 fullheight">KH: ' + item.fullName + ' - ' + item.phoneNumber + '</div>';
//                inner_html += '<div class="divSearch2 fullheight">' + item.routeName + ' - ' + checkDate(item.getInTimePlan) + '</div>';
//                inner_html += '<div class="divSearch3 fullheight" >';
//                inner_html += item.listSeatId.toString();
////                item.listSeatId.forEach(function (element) {
////                    inner_html += element + '</br>';+
////                });
//                inner_html += '</div>';
//                inner_html += '<div class="fullheight divSearch4">' + status + '</div>';
//
//                inner_html += '</div></a>';
//            }


            return $("<li class='borderlist'></li>")
                .data("item.autocomplete", item)
                .append(inner_html)
                .appendTo(ul);
        };

        function checkDate(date) {
            var datetime = new Date(date);
            var dd = datetime.getDate();
            var mm = datetime.getMonth() + 1;
            var yyyy = datetime.getFullYear();
            var hour = datetime.getHours();
            var minutes = datetime.getMinutes();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }

            if (hour < 10) {
                hour = '0' + hour
            }

            if (minutes < 10) {
                minutes = '0' + minutes
            }

            datetime = dd + '-' + mm + '-' + yyyy + ' ' + hour + ':' + minutes;
            return datetime
        }

        function covertDate(date) {
            year = date.toString().substring(0, 4);
            month = date.toString().substring(4, 6);
            day = date.toString().substring(6, 8);
            return day + '-' + month + '-' + year;
        }
    });

    function superLoading(flag) {
        if(flag){
            $('.superloading').show();
        }else{d
            $('.superloading').hide();
        }
    }
    function doc_keyUp(e) {
        e.preventDefault();
        // this would test for whichever key is 40 and the Alt key at the same time
        if (e.altKey && e.keyCode == 88) {
            // call your function to do the thing
            if (flashMoveSeat) {
                flashMoveSeat = false;
                moveSeatInfo = {};
                generateHtmlTripInfo();
                notyMessage('Chế độ bình thường', 'info')
            }
        }
        if(e.keyCode === 13){
            return false;
        }
        return false;
    }
    function notyMessage(content,type,time){
        type = type!==undefined?type:'info';
        time = time!==undefined?time:3000;
        new Noty({
            type: type,
            theme: 'sunset',
            timeout: time,
            text: content
        }).show();
    }

    function checkDate(date) {
        var datetime = new Date(date);
        var dd = datetime.getDate();
        var mm = datetime.getMonth() + 1;
        var yyyy = datetime.getFullYear();
        var hour = datetime.getHours();
        var minutes = datetime.getMinutes();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        if (hour < 10) {
            hour = '0' + hour
        }

        if (minutes < 10) {
            minutes = '0' + minutes
        }

        datetime = dd + '-' + mm + '-' + yyyy + ' ' + hour + ':' + minutes;
        return datetime
    }

    function getCurentDate(){
        var datetime = new Date();
        var dd = datetime.getDate();
        var mm = datetime.getMonth() + 1;
        var yyyy = datetime.getFullYear();
        var hour = datetime.getHours();
        var minutes = datetime.getMinutes();
        var seconds = datetime.getSeconds();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        if (hour < 10) {
            hour = '0' + hour
        }

        if (minutes < 10) {
            minutes = '0' + minutes
        }

        datetime = yyyy + '-' + mm + '-' + dd;
        return datetime
    }
    function lockOrUnlockSeat(type) {
        var listSeatId = [];
        $.each($('.ghedangchon'), function (index, element) {
            listSeatId.push($(element).find('.seatId').text());
        });
        listSeatReActive = listSeatId;
        dataLock = JSON.parse(sessionStorage.getItem('lockSeat')||'{}');
        console.log(dataLock);
        if(typeof dataLock.tripId!=="undefined" && typeof dataLock.listSeatId !== "undefined"){
            if(type==="lock"){
                SendAjaxWithJson({
                    url : urlDBD('trip/lockSeat'),
                    type : "post",
                    data : {
                        tripId : dataLock.tripId,
                        listSeatId : dataLock.listSeatId,
                        option : 1
                    },
                    success:function(data){
                        console.log(data);
                    }
                });
            }
            if(type==="unlock"){
                sessionStorage.setItem('lockSeat','{}');
                SendAjaxWithJson({
                    url : urlDBD('trip/lockSeat'),
                    type : "post",
                    data : {
                        tripId : dataLock.tripId,
                        listSeatId : dataLock.listSeatId,
                        option : 2
                    },
                    success:function(data){
                        console.log(data);
                    }
                });
            }
        }
    }
    function removeUnicode(str) {
        str= str.toLowerCase();
        str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
        str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
        str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
        str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
        str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
        str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
        str= str.replace(/đ/g,"d");
        str= str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,"-");

        str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
        str= str.replace(/^\-+|\-+$/g,"");

        return str;
    }

    // register the handler
    document.addEventListener('keyup', doc_keyUp, false);
</script>
<script>
    var errorObject = {
        ERR_000004032 : 'Số điện thoại không được bỏ trống',
        USER_NOT_LOGGED_IN_YET : "User chưa đăng nhập vào hệ thống",
        USER_NOT_HAVE_PERMISSION : "Bạn không có quyền thực hiện thao tác này",
        USER_INFO_NULL_EMPTY : "Thông tin người dùng trống",
        USER_OPENNING_EXCHANGE_REACHED_MAXIMUM : "Tổng số bài đăng đang chạy vượt quá giới hạn",
        USER_PHONE_NUMBER_NULL_EMPTY : "Số điện thoại trống",
        USER_STATE_CODE_NULL_EMPTY : "Mã quốc gia trống",
        USER_PHONE_NUMBER_ALREADY_REGISTERED : "Số điện thoại đã được đăng ký",
        USER_CAPTCHA_INVALID : "Captcha không hợp lệ",
        USER_PHONE_NUMBER_INVALID : "Số điện thoại không hợp lệ",
        USER_PHONE_NUMBER_ALREADY_VERIFIED : "Số điện thoại đã được xác thực",
        USER_EMAIL_ALREADY_VERIFIED : "Email đã được xác thực",
        USER_PASSWORD_NULL_EMPTY : "Mật khẩu trống",
        USER_OTP_CODE_NULL_EMPTY : "Mã xác nhận trống",
        USER_PASSWORD_NOT_MATCH : "Mật khẩu không hợp lệ",
        USER_OTP_CODE_NOT_MATCH : "Mã xác nhận không hợp lệ",
        USER_PHONE_NUMBER_NOT_REGISTED_YET : "Số điện thoại chưa được đăng ký trước đó",
        USER_FACEBOOK_ID_NULL_EMPTY : "Facebook Id trống",
        USER_GOOGLE_ID_NULL_EMPTY : "Google Id trống",
        USER_ID_NULL_EMPTY : "Mã người dùng trống",
        USER_OLD_PASSWORD_NULL_EMPTY : "Mật khẩu cũ không hợp lệ",
        USER_NEW_PASSWORD_NULL_EMPTY : "Mật khẩu mới trống",
        USER_ID_NOT_MATCH_WITH_TOKEN_KEY : "UserId không đúng với tokenKey",
        USER_SEND_VERIFY_CODE_TOO_FAST : "Gửi mã xác nhận quá nhanh",
        USER_SEND_VERIFY_FALSE : "Gửi mã thất bại",
        USER_G_CAPTCHA_NULL_EMPTY : "Captcha trống",
        USER_G_CAPTCHA_VALIDATE_FAIL : "Captcha không hợp lệ",
        USER_RATING_ID_NULL_OR_EMPTY : "Mã đánh giá trống",
        USER_RATING_CONTENT_IS_TOO_LENGTH : "Nội dung đánh giá quá dài",
        USER_RATING_CONTENT_CONTAIN_BAD_WORD : "Nội dung bao gồm các từ ngữ không phù hợp",
        USER_RATING_VALUE_INVALID : "Giá trị đánh giá không hợp lệ",
        USER_ID_BE_RATED_NULL_OR_EMPTY : "Mã người dùng được đánh giá trống",
        USER_RATING_CONTENT_NULL_OR_EMPTY : "Nội dung đánh giá trống",
        USER_RATING_VALUE_NULL_OR_EMPTY : "Điểm đánh giá trong khoảng từ 0 đến 5",
        USER_EMAIL_ALREADY_REGISTERED : "Email đã được đăng ký",
        USER_TOKENKEY_INVALID : "TokenKey lỗi",
        USER_TOKENKEY_NULL_OR_EMPTY : "TokenKey không tồn tại",
        USER_EMAIL_NULL_EMPTY : "Email không tồn tại",
        USER_EMAIL_NOT_REGISTED_YET : "Email chưa được đăng ký",
        USER_FULL_NAME_NULL_OR_EMPTY : "Full name không để trống",
        USER_GOOGLE_ID_ALREADY_REGISTERED : "Google ID đã được đăng ký",
        USER_FACEBOOK_ID_ALREADY_REGISTERED : "Facebook ID đã được đăng ký",
        USER_REFRESHKEY_INVALID : "RefreshKey không hợp lệ",
        USER_LONGITUDE_NOT_IN_VALID_RANGE : "Kinh độ không hợp lệ",
        USER_LATITUDE_NOT_IN_VALID_RANGE : "Vĩ độ không hợp lệ",
        USER_NOT_VERIFY_PHONE_INFO_YET : "Số điện thoại chưa được xác nhận",
        USER_CAN_NOT_RATING_TO_OWNSELF : "Bạn không phù hợp với chuyến này",
        USER_JOIN_DATE_INVALID : "ngày tham gia công ty không hợp lệ",
        USER_COMPANY_ID_INVALID : "Mã công ty không đúng",
        USER_COMPANY_NAME_INVALID : "Tên công ty không đúng",
        USER_EXCEED_REVENUE_INVALID : "Hạn mức đại lí đã hết hoặc không đủ, vui lòng thanh toán để tiếp tục sử dụng dịch vụ",
        GET_LIST_PAGE_OPTION_NULL_EMPTY : "Thiếu tùy chọn PAGE",
        GET_LIST_PAGE_OPTION_INVALID : "Tùy chọn PAGE không hợp lệ",
        GET_LIST_COUNT_OPTION_NULL_EMPTY : "Tùy chọn COUNT trống",
        GET_LIST_COUNT_OPTION_INVALID : "Tùy chọn COUNT không hợp lệ",
        EXECUTE_ACTION_TOO_FAST : "Thực hiện thao tác quá nhanh",
        UNLOCK_TRANSACTION_FAIL : "Mở khóa thao tác thất bại",
        LOCK_TRANSACTION_TIMEOUT : "Hết thời hạn khóa thao tác",
        LOCK_TRANSACTION_CHECK_BY_DATASTORE : "Hết thời hạn khóa thao tác",
        PAY_TYPE_NULL_EMPTY : "Phương thức thanh toán trống",
        PAY_CARD_SERIAL_NULL_EMPTY : "Mã thẻ cào trống",
        PAY_PIN_CODE_NULL_EMPTY : "Mã PIN trống",
        PAY_FAIL : "Thanh toán thất bại",
        TRANSACTION_LOCKED : "Giao dịch trên tài khoản tạm thời chưa được xử lý do bị khóa bởi thao tác khác",
        LOCK_ID_FAIL : "Khóa Id để thực hiện giao dịch không thành công",
        UNLOCK_ID_FAIL : "Mở khóa Id để thực hiện giao dịch không thành công",
        TRANSACTION_TIMEOUT : "Thời gian thực hiện yêu cầu đã hết hạn",
        TRANSACTION_ID_NULL_OR_EMPTY : "Mã giao dịch không tồn tại",
        TRANSACTION_NULL : "Giao dịch trống",
        TRANSACTION_OWNER_NULL : "Khách hàng null",
        TRANSACTION_USER_ID_NULL_OR_EMPTY : "Mã Khách hàng null",
        TRANSACTION_TRANSCODE_NULL_OR_EMPTY : "Mã chuyến null",
        TRANSACTION_IP_NULL_OR_EMPTY : "Ip Khách hàng null",
        TRANSACTION_EXISTED : "Số tiền rỗng",
        TRANSACTION_CYBER_CASH_INVALID : "Common error message",
        OBJECT_NULL : "Thông tin của Object lấy được là null",
        HOST_NULL_EMPTY : "Tên server host không được để trống",
        QUEUE_NULL_EMPTY : "TaskQueue không được để trống",
        API_URL_NULL_EMPTY : "URL của API không được để trống",
        PARAMS_NULL_EMPTY : "Các biến đầu vào không được để trống",
        MESSAGES_NULL_EMPTY : "Tin nhắn không được để trống",
        KEY_NULL_EMPTY : "Key không được để trống",
        VALUE_NULL : "Giá trị không được để trống",
        REQUEST_PARAM_NULL_EMPTY : "Các parameter trong request không được để trống",
        RESPONSE_RESULT_NULL : "Kết quả trả về không được để trống",
        USER_NAME_NULL_EMPTY : "Username không được để trống",
        USER_NAME_INVALID : "Username không hợp lệ.",
        USER_NAME_EXISTED : "Username đã tồn tại trong hệ thống",
        USER_NAME_HAS_BEEN_ALREADY_UPDATED : "Username của tài khoản đã được cập nhật rồi",
        USER_ID_EXISTED : "UserId đã tồn tại trong hệ thống",
        USER_ID_INVALID : "UserId không hợp lệ. Không thể lấy thông tin user từ UserId",
        USER_NULL : "Không thể lấy được thông tin User",
        USER_HAS_BEEN_LOCKED : "User đã bị khóa tài khoản",
        INVALID_OPTION : "Option không hợp lệ",
        OPTION_NULL_EMPTY : "Option không được để trống",
        PASSWORD_NULL_EMPTY : "Password không được để trống",
        PASSWORD_INVALID : "Password không hợp lệ",
        PASSWORD_NOT_MATCH : "Password không đúng",
        REPASSWORD_NULL_EMPTY : "Re-Password không được để trống",
        REPASSWORD_NOT_MATCH : "Re-password không đúng với password",
        EMAIL_NULL_EMPTY : "Email không được để trống",
        EMAIL_EXISTED : "Email đã tồn tại trong hệ thống",
        EMAIL_INVALID : "Email không hợp lệ",
        FULL_NAME_NULL_EMPTY : "Tên họ đầy đủ không được để trống",
        FULL_NAME_INVALID : "Tên họ đầy đủ không hợp lệ",
        ACCOUNT_NULL : "Không lấy được thông tin tài khoản",
        ACCOUNT_LOCKED : "Tài khoản này đã bị khóa, không thực hiện được giao dịch",
        SEX_INVALID : "Giới tính không hợp lệ",
        SEX_NULL_EMPTY : "Giới tính không được để trống",
        SAVE_TO_DATASTORE_FAIL : "Lưu trữ data vào datastore thất bại",
        LOCK_FAIL : "Xử lý khóa thất bại",
        PHONE_NULL_EMPTY : "Số điện thoại không được để trống",
        PHONE_INVALID : "Số điện thoại không hợp lệ",
        PHONE_EXISTED : "Số điện thoại đã tồn tại trong hệ thống",
        STATE_CODE_NULL_EMPTY : "Mã vùng không được để trống",
        STATE_CODE_INVALID : "Mã vùng không hợp lệ",
        G_CAPTCHA_NUL_EMPTY : " G-Captcha trả về không được để trống",
        G_CAPTCHA_VALIDATE_FAIL : "Xác nhận G-Captcha với server thất bại",
        G_SECRET_NULL_EMPTY : "G-Secret không được là trống",
        ADMIN_SECRET_KEY_NULL_EMPTY : "Mã bí mật của Admin không được để trống",
        ADMIN_SECRET_KEY_INVALID : "Mã bí mật của Admin không hợp lệ",
        ACCESS_KEY_NULL_EMPTY : "Access key không được để trống",
        ACCESS_KEY_INVALID : "Access key không hợp lệ",
        IMAGE_UPLOAD_INVALID : "Image tải lên không hợp lệ",
        IMAGE_FILE_NOT_MATCH : "Image file không đúng",
        AVATAR_NULL_EMPTY : "Ảnh đại diện không được để trống",
        AVATAR_INVALID : "Ảnh đại diện không hợp lệ",
        NOTIFY_OPTION_INVALID : "User notify option không hợp lệ",
        SEND_SMS_VERIFY_FAIL : "Gửi SMS mã xác nhận thất bại",
        SEND_SMS_VERIFY_LOCKED : "Gửi mã xác nhận quá nhanh",
        SEND_EMAIL_VERIFY_FALSE : "Gửi email xác nhận thất bại",
        LOCATION_NULL_EMPTY : "Tọa độ không được để trống",
        SEND_EMAIL_FALSE : "Gửi email thất bại",
        SEND_SMS_FALSE : "Gửi sms thất bại",
        PRESENTER_ID_ALREADY_UPDATED : "Id người giới thiệu đã được cập nhật",
        PRESENTER_ID_INVALID : "Mã người giới thiệu không hợp lệ",
        PRESENTER_IS_NOT_ME : "Bạn không thể giới thiệu cho chính mình",
        SAME_PASSWORD : "Bạn không thể đổi mật khẩu mới trùng với mật khẩu hiện tại",
        VEHICLE_NULL : "Thông tin xe trống",
        VEHICLE_OWNER_NULL_OR_EMPTY : "chủ xe null",
        VEHICLE_NUMBER_PLATE_NULL_OR_EMPTY : "biển số xe null",
        VEHICLE_ID_NULL_OR_EMPTY : "id xe trống",
        VEHICLE_NAME_TOO_LENGTH : "tên xe quá dài",
        VEHICLE_NAME_NULL_OR_EMPTY : "tên xe trống",
        VEHICLE_MANUFACTURER_NAME_NULL_OR_EMPTY : "tên nhà sản xuất trống",
        VEHICLE_ORIGIN_NULL_OR_EMPTY : "xuất xứ trống",
        CAR_TYPE_NULL_OR_EMPTY : "loại xe trống",
        CAR_PRODUCT_YEAR_NULL_OR_EMPTY : "năm sản xuất xe trống",
        VEHICLE_LIST_IMAGES_INVALID : "ảnh xe không hợp lệ",
        VEHICLE_NUMBER_OF_SEATS_INVALID : "số chỗ ngồi k hợp lệ",
        VEHICLE_PRODUCTED_YEAR_NULL : "năm null",
        VEHICLE_NUMBER_OF_SEATS_NULL_OR_EMPTY : "số chỗ null",
        VEHICLE_COMPANY_ID_NULL_OR_EMPTY : "mã nhà vận tải null",
        VEHICLE_USER_ID_NULL_OR_EMPTY : "mã người tạo null",
        VEHICLE_NUMBER_PLATE_EXIST : "biển số xe đã tồn tại",
        VEHICLE_ESTIMATED_FUEL_CONSUMPTION_INVALID : "tiêu hao xăng dầu dự tính trên km sai kiểu dữ liệu",
        VEHICLE_SEATS_MAP_ID_NOT_OR_EMPTY : "Mã Sơ đồ trống",
        VEHICLE_REGISTRATION_DEADLINE_INVALID : "Hạng đăng kiểm sai kiểu dữ liệu",
        VEHICLE_LONGITUDE_INVALID : "kinh độ sai kiểu dữ liệu",
        VEHICLE_LATITUDE_INVALID : "vĩ độ sai kiểu dữ liệu",
        VEHICLE_LONGITUDE_NULL_OR_EMPTY : "kinh độ rỗng",
        VEHICLE_ESTIMATED_FUEL_CONSUMPTION_NOT_OR_EMPTY : "tiêu hao xăng dầu dự tính trên km trống",
        VEHICLE_REGISTRATION_DEADLINE_NOT_OR_NULL : "Hạng đăng kiểm trống",
        VEHICLE_LATITUDE_NOT_OR_EMPTY : "vĩ độ trống",
        VEHICLE_PRODUCTED_YEAR_INVALID : "năm sản xuất sai kiểu dữ liệu",
        VEHICLE_TYPE_ID_NOT_OR_EMPTY : "mã loại xe",
        VEHICLE_EXIST_IN_THE_PLANE : "đang trong kế hoạch",
        VEHICLE_SEAT_MAP_NOT_EXIST : "seatMap không tồn tại",
        VEHICLE_EXIST_TICKET : "đã có vé",
        VEHICLE_ORIGIN_PRICE_INVALID : "nguyên giá không hợp lệ",
        VEHICLE_REDUCE_TIME_INVALID : "thời gian khấu hao không hợp lệ",
        VEHICLE_STATUS_INVALID : "trạng thái của xe không hợp lệ",
        TRIP_ID_NULL_OR_EMPTY : "mã gói đặt xe trống",
        TRIP_USER_NULL_OR_EMPTY : "Người dùng trong chuyến xe trống",
        TRIP_NULL : "đối tượng rỗng",
        TRIP_LIST_PICK_UP_POINT_ID_NULL_OR_EMPTY : "danh sách điểm trống",
        TRIP_NUMBER_OF_SEATS_INVALID : "số chỗ sai kiểu",
        TRIP_NUMBER_OF_VEHICLES_INVALID : "",
        TRIP_LIST_DIRVER_REALITY_ID_NULL : "danh sách mã lái xe trống",
        TRIP_USER_ID_NULL_OR_EMPTY : "mã người dùng trống",
        TRIP_NUMBER_OF_VEHICLES_EMPTY_NULL : "",
        TRIP_DISTANCE_NULL_OR_EMPTY : "",
        TRIP_CONVERT_TRIP_STATUS_INVALID : "",
        TRIP_STATUS_NULL_OR_EMPTY : "trạng thái trống",
        TRIP_SEAT_MAP_ID_NULL_OR_EMPTY : "mã sơ đồ chỗ ngồi rỗng",
        TRIP_DISTANCE_INVALID : "khoảng cách sai kiểu dữ liệu",
        TRIP_LIST_ASSISTANT_REALITY_ID_NULL : "danh sách mã phụ xe rỗng",
        TRIP_PAGE_INVALID : "page sai kiểu",
        TRIP_COUNT_INVALID : "count sai kiểu",
        TRIP_START_DATE_REALITY_INVALID : "ngày bắt đầu thực tế sai kiểu",
        TRIP_RUN_TIME_REALITY_INVALID : "thời gian chạy thực tế sai kiểu",
        TRIP_START_DATE_REALITY_NOT_OR_EMPTY : "ngày bắt đầu thực tế trống",
        TRIP_COMPELETED_DATE_REALITY_NOT_OR_EMPTY : "ngày kết thúc thực tế rỗng",
        TRIP_ESTIMATED_FUEL_CONSUMPTION_REALITY_NOT_OR_EMPTY : "ước tính tiêu hao nhiêu liệu thực tế",
        TRIP_ESTIMATED_FUEL_CONSUMPTION_REALITY_INVALID : "ước tính tiêu hao nhiên liệu thực tế sai kiểu",
        TRIP_NUMBER_OF_CURRENT_GUESTS_NOT_OR_EMPTY : "số khách hiện tại null",
        TRIP_NUMBER_OF_CURRENT_GUESTS_INVALID : "số khách hiện tại sai kiểu giá trị",
        TRIP_TOTAL_AMOUNT_OF_MONEY_TICKET_FOR_ASSISTANTS_INVALID : "tổng số tiền vé của tài xế bán sai kiểu giá trị",
        TRIP_TOTAL_AMOUNT_OF_MONEY_TICKETS_INVALID : "tổng số tiền vé sai kiểu",
        TRIP_LIST_DIRVER_REALITY_ID_INVALID : "danh sách mã lái xe sai kiểu",
        TRIP_LIST_ASSISTANT_REALITY_ID_INVALID : "danh sách tài",
        TRIP_SEAT_MAP_NULL : "sơ đồ ghế null",
        TRIP_SCHEDULE_ID_NULL_OR_EMPTY : "mã lịch chạy null",
        TRIP_DATE_INVALID : "ngày sai kiểu",
        TRIP_POINT_ID_START_NULL : "mã điểm bắt đầu null",
        TRIP_POINT_ID_END_NULL : "mã điểm kết thúc null",
        TRIP_DATE_NULL : "ngày sai kiểu",
        TRIP_STATUS_INVALID : "trạng thái chuuyển không hợp lệ",
        TRIP_CURRENT_LATITUDE_INVALID : "latitude sai kiểu",
        TRIP_CURRENT_LONGITUDE_INVALID : "longitude sai kiểu",
        TRIP_COMPANY_ID_NULL_OR_EMPTY : "mã công ty rỗng",
        TRIP_START_DATE_NOT_OR_EMPTY : "ngày bắt đầu trống",
        TRIP_END_DATE_NOT_OR_EMPTY : "ngày kết thúc trống",
        TRIP_START_DATE_INVALID : "ngày bắt đầu sai kiểu",
        TRIP_END_DATE_INVALID : "ngày kết thúc sai kiểu",
        TRIP_START_TIME_INVALID : "thời gian bắt đầu sai kiểu",
        TRIP_MONTH_NOT_OR_EMPTY : "tháng rỗng",
        TRIP_YEAR_NOT_OR_EMPTY : "năm rỗng",
        TRIP_MONTH_INVALID : "tháng sai kiểu",
        TRIP_YEAR_INVALID : "năm sai kiểu",
        TRIP_NOT_RUN : "xe chưa chạy",
        TRIP_START_PROVINCE_NULL_OR_EMPTY : "tỉnh bắt đầu",
        TRIP_END_PROVINCE_NULL_OR_EMPTY : "tỉnh kết thúc",
        TRIP_START_NULL : "điểm bắt đầu null",
        TRIP_END_NULL : "điểm kết thúc null",
        TRIP_NOT_FINISHED : "chuyến chưa hoàn thành",
        TRIP_MONTH_NULL_OR_EMPTY : "tháng rỗng",
        TRIP_YEAR_NULL_OR_EMPTY : "năm null",
        TRIP_OVER_TIME : "quá thời gian",
        TRIP_EXCLUDE_IN_PLAN : "không có trong kế hoạch",
        TRIP_LIST_SEAT_ID_INVALID : "không có trong kế hoạch",
        TRIP_FINISHED_ERROR : "kết thúc chuyến lỗi",
        NOTIFICATION_CODE_INVALID : "Mã thông báo không hợp lệ",
        NOTIFICATION_OBJECT_TYPE_INVALID : "Loại đối tượng thông báo không hợp lện",
        NOTIFICATION_LIST_OBJECT_TYPE_INVALID : "Danh sách loại đối tượng thông báo không hợp lệ",
        NOTIFICATION_ID_NULL_EMPTY : "Mã thông báo không hợp lệ",
        NOTIFICATION_NULL_EMPTY : "Thông báo trống",
        PIORITY_CODE_NULL : "Mã ưu tiên trống",
        PIORITY_CODE_HAS_BEEN_USED : "Mã ưu tiên đã được sử dụng",
        PIORITY_CODE_NULL_EMPTY : "Mã ưu tiên trống",
        TRANSPORT_COMPANY_NULL : "Thông tin công ty vận tải trống",
        TRANSPORT_COMPANY_ID_NULL_OR_EMPTY : "Mã công ty vận tải trống",
        TRANSPORT_COMPANY_NAME_NULL_OR_EMPTY : "Tên công ty vận tải trống",
        TRANSPORT_COMPANY_INTRO_NULL_OR_EMPTY : "Mô tả công ty vận tải trống",
        TRANSPORT_COMPANY_OWNER_NULL_OR_EMPTY : "Người tạo công ty vận tải trống",
        TRANSPORT_COMPANY_INFO_NULL_OR_EMPTY : "Thông tin công ty vận tải trống",
        TRANSPORT_COMPANY_LOCATION_INFO_MISSING : "Vị trí công ty vận tải trống",
        TRANSPORT_COMPANY_LOCATION_TOO_LENGTH : "Thông tin vị trí quá dài",
        TRANSPORT_COMPANY_LOGO_NULL_OR_EMPTY : "Banner không được để trống",
        TRANSPORT_COMPANY_START_DATE_NULL_OR_EMPTY : "Thời gian bắt đầu không được để trống",
        TRANSPORT_COMPANY_END_DATE_NULL_OR_EMPTY : "Thời gian kết thúc không được để trống",
        TRANSPORT_COMPANY_SLOGAN_NULL_OR_EMPTY : "Slogan không được để trống",
        TRANSPORT_COMPANY_LIST_IMAGES_INVALID : "Thời gian kết thúc không được để trống",
        TRANSPORT_COMPANY_LATITUDE_NOT_IN_VALID_RANGE : "Vĩ độ không hợp lệ",
        TRANSPORT_COMPANY_LONGITUDE_NOT_IN_VALID_RANGE : "Kinh độ không hợp lệ",
        TRANSPORT_COMPANY_OWNER_MISSING_PHONE_NUMBER_INFO : "Thiếu thông tin liên hệ của nhà tổ chức",
        TRANSPORT_COMPANY_START_END_DATE_INVALID : "Thời gian bắt đầu, thời gian kết thúc không hợp lệ",
        TRANSPORT_COMPANY_ADDRESS_INFO_MISSING : "Địa điểm công ty vận tải trống",
        TRANSPORT_COMPANY_ADDRESS_TOO_LENGTH : "Thông tin địa điểm quá dài",
        TRANSPORT_COMPANY_STATUS_NULL_OR_EMPTY : "Trạng thái công ty vận tải trống",
        TRANSPORT_COMPANY_STATUS_INVALID : "danh sách tỉ lệ đổi ngoại tệ không hợp lệ",
        COMMENT_ID_NULL_OR_EMPTY : "Mã bình luận trống",
        COMMENT_NULL : "Bình luận trống",
        COMMENT_CONTENT_NULL_OR_EMPTY : "Nội dung bình luận trống",
        COMMENT_CONTENT_IS_TOO_SHORT : "Nội dung bình luận quá ngắn",
        COMMENT_CONTENT_IS_TOO_LENGTH : "Nội dung bình luận quá dài",
        COMMENT_CONTENT_CONTAIN_BAD_WORD : "Bình luận bao gồm các từ không phù hợp",
        USER_PACKAGE_NULL_OR_EMPTY : "Gói Vip trống",
        USER_PACKAGE_NULL : " Gói Vip trống",
        PUSH_DATA_TO_DATASTORE_ERROR : "Có lỗi khi lưu dữ liệu",
        CATEGORY_ID_NULL_EMPTY : "Mã danh mục trống",
        CATEGORY_NULL : "Danh mục trống",
        CATEGORY_NAME_NULL_EMPTY : "Tên danh mục trống",
        CATEGORY_PARENT_ID_NULL_EMPTY : "Mã danh mục cha trống",
        CATEGORY_ID_ALREADY_EXIST : "Mã danh mục trống",
        FUNCTION_ID_NULL_EMPTY : "Id chức năng trống",
        FUNCTION_NULL : "Chức năng trống",
        FUNCTION_NAME_NULL_EMPTY : "Tên chức năng trống",
        FUNCTION_CATEGORY_ID_NULL_EMPTY : "Mã danh mục chức năng",
        FUNCTION_ID_ALREADY_EXIST : "Mã chức năng đã tồn tại trong hệ thống",
        FUNCTION_USER_NULL_OR_EMPTY : "Người tạo chức năng trống",
        FUNCTION_CODE_NULL_EMPTY : "Mã chức năng trống",
        FUNCTION_DESCRIPTION_NULL_EMPTY : "Mô tả trống",
        FUNCTION_CODE_ALREADY_EXIST : "Mã chức năng đã tồn tại trong hệ thống",
        FUNCTION_OLD_NULL : "Chức năng trước khi update không tồn tại",
        FUNCTION_CODE_CAN_NOT_BE_CHANGED : "Không thể đổi mã của chức năng",
        SERVICE_PACK_ID_NULL_EMPTY : "Id gói dịch vụ trống",
        SERVICE_PACK_NULL : "Gói dịch vụ trống",
        SERVICE_PACK_USER_NULL_OR_EMPTY : "Người tạo gói dịch vụ trống",
        SERVICE_PACK_NAME_NULL_EMPTY : "Tên gói dịch vụ trống",
        SERVICE_PACK_DESCRIPTION_NULL_EMPTY : "Mô tả gói dịch vụ trống",
        SERVICE_PACK_CATEGORY_ID_NULL_EMPTY : "Mã danh mục gói dịch vụ trống",
        SERVICE_PACK_CODE_NULL_EMPTY : "Mã gói dịch vụ trống",
        SERVICE_PACK_CODE_ALREADY_EXIST : "Mã gói đã tồn tại",
        SERVICE_PACK_OLD_NULL : "Gói gốc không tồn tại",
        SERVICE_PACK_CODE_CAN_NOT_BE_CHANGED : "Không thể thay đổi mã của gói dịch vụ",
        SERVICE_PACK_LIST_FUNCTION_CODE_NULL_EMPTY : "danh sách chức năng rỗng",
        SERVICE_PACK_LIST_FUNCTION_CODE_INVALID : "danh sách chức năng sai kiểu",
        IMAGE_NOT_AN_IMAGE_ERROR : "File up lên không phải là ảnh.",
        IMAGE_NULL : "Thông tin ảnh bị lỗi",
        IMAGE_CONTAIN_BAD_CONTENT : "Ảnh chứa nội dung không lành mạnh.",
        IMAGE_SIZE_IS_OVER_MAX_VALUE : "Kích thước của ảnh vượt quá 5MB",
        POINT_ID_NULL_OR_EMPTY : "mã điểm đón rỗng",
        POINT_NULL : "điểm đón rỗng",
        POINT_PICK_UP_POINT_ID_NULL_OR_EMPTY : "mã Tuyến rỗng",
        POINT_NAME_NULL_OR_EMPTY : "tên rỗng",
        POINT_ADDRESS_NULL_OR_EMPTY : "địa chỉ rỗng",
        POINT_LONGITUDE_NULL_OR_EMPTY : "kinh độ rỗng",
        POINT_LONGITUDE_INVALID : "kinh độ sai định dạng",
        POINT_LATITUDE_NULL_OR_EMPTY : "vĩ độ rỗng",
        POINT_LATITUDE_INVALID : "vĩ độ sai định dạng",
        POINT_USER_ID_NULL_OR_EMPTY : "vĩ độ sai định dạng",
        POINT_ROUTE_ID_NULL_OR_EMPTY : "mã tuyến null",
        POINT_ADDRESS_EXIT : "địa chỉ đã tồn tại",
        POINT_NAME_EXIT : "tên đã tồn tại",
        POINT_LATITUE_LONGITUDE_EXIT : "toạ độ đã tồn tại",
        POINT_PAGE_INVALID : "page sai kiểu",
        POINT_COUNT_INVALID : "count sai kiểu",
        POINT_TYPE_INVALID : "type sai kiểu",
        POINT_TYPE_NOT_OR_EMPTY : "type rỗng",
        POINT_ACTIVE_INVALID : "active sai kiểu",
        POINT_ARRIVED_END : "điểm đón rỗng",
        POINT_EXIST_LAT_LONG : "trùng tọa độ điểm",
        POINT_EXIT_END : "đã đến điểm cuối cùng",
        POINT_ORDER_INVALID : "thứ tự điểm lên và điểm xuống không hợp lệ",
        POINT_PROVINCE_NULL : "thứ tự điểm lên và điểm xuống không hợp lệ",
        POINT_NOT_DELETE : "không xóa được",
        POINT_NAME_EXIST : "trùng tên điểm",
        POINT_RANGE_INVALID : "khoảng cách sai kiểu",
        POINT_RANGE_EMPTY_NULL : "khoảng cách trống",
        POINT_INVALID : "các thành phần sai giá trị",
        ROUTE_NULL : "đối tượng rỗng",
        ROUTE_NAME_NULL_OR_EMPTY : "tên rỗng",
        ROUTE_LIST_DISTANCE_NULL : "danh sách các giá tiền rỗng",
        ROUTE_COMPANY_ID_NULL_OR_EMPTY : "mã nhà vận tải rỗng",
        ROUTE_ROUTE_ID_NULL_OR_EMPTY : "mã Tuyến rỗng",
        ROUTE_LIST_POINT_ID_NULL : "danh sách các mã điểm rỗng",
        ROUTE_ID_NULL_OR_EMPTY : "mã rỗng",
        ROUTE_USER_ID_NULL_OR_EMPTY : "mã user rỗng",
        ROUTE_LIST_PRICE_BY_VEHICLE_TYPE_INVALID : "danh sách các giá tiền cho người dùng sai",
        ROUTE_LIST_POINT_ID_INVALID : "danh sách các mã điểm sai",
        ROUTE_LIST_TOLL_STATION_ID_INVALID : "danh sách các mã trạm thu phí sai",
        ROUTE_PAGE_INVALID : "page sai kiểu dữ liệu",
        ROUTE_COUNT_INVALID : " count sai kiểu dữ liệu",
        ROUTE_ACTIVE_INVALID : "active sai kiểu dữ liệu",
        ROUTE_LIST_TOLL_STATION_ID_NULL : "danh sách các mã trạm thu phí rỗng",
        ROUTE_LIST_VEHICLE_TYPE_NULL : "loại xe rỗng",
        ROUTE_LIST_VEHICLE_TYPE_INVALID : "loại xe sai gía trị",
        ROUTE_LIST_PRICE_TOLLSTATION_INVALID : "danh sách gía phí trạm thu phí sai kiểu",
        ROUTE_LIST_WEIGHT_INVALID : "danh sách các mức trọng lượng sai kiểu",
        ROUTE_LIST_PRICE_WEIGHT_AND_POINT_INVALID : "danh sách ma trận phí giữa mức cân nặng với điểm sai kiểu",
        ROUTE_LIST_DIMENTION_INVALID : "danh sách các mức kích thước sai kiểu",
        ROUTE_LIST_PRICE_DIMENTION_AND_POINT_INVALID : "danh sách ma trận phí của các mức kích thước với điểm sai kiểu",
        ROUTE_LIST_TIME_INTENT_INVALID : "danh sách thời gian dự kiến sai kiểu",
        ROUTE_LIST_PRICE_WEIGHT_AND_POINT_NULL : "danh sách ma trận phí giữa mức cân nặng với điểm rỗng",
        ROUTE_LIST_PRICE_WEIGHT_AND_POINT_SIZE_INVALID : "danh sách ma trận phí giữa mức cân nặng với điểm sai kích thước",
        ROUTE_LIST_PRICE_DIMENTION_AND_POINT_SIZE_INVALID : "danh sách ma trận phí giữa mức kích thước với điểm sai kích thước",
        ROUTE_LIST_PRICE_DIMENTION_AND_POINT_NULL : "danh sách ma trận phí giữa mức kích thước rỗng",
        ROUTE_LIST_MEAL_PRICE_NULL_OR_EMPTY : "giá ăn rỗng",
        ROUTE_LIST_MEAL_PRICE_INVALID : "giá ăn rỗng",
        ROUTE_LIST_PRICE_BY_VEHICLE_TYPE_SIZE_INVALID : "danh sách các giá tiền cho người dùng có giá trị sai",
        ROUTE_LIST_PRICE_BY_VEHICLE_TYPE_NULL : "danh sách các giá tiền cho người dùng rỗng",
        ROUTE_LIST_TIME_INTENT_NULL_EMPTY : "danh sách thời gian dự kiến sai kiểu",
        ROUTE_POINT_ID_NOT_EXIT : "mã không tồn tại",
        ROUTE_VEHICLE_TYPE_ID_NULL_OR_EMPTY : "mã loại xe rỗng",
        ROUTE_LIST_PRICE_WEIGHT_AND_POINT_NOT_INCREASE_INVALID : "nhập sai giá trị không tăng dần",
        ROUTE_LIST_PRICE_DIMENTION_AND_POINT_NOT_INCREASE_INVALID : "nhập sai giá trị không tăng dần",
        ROUTE_LIST_TIME_INTENT_NOT_INCREASE_INVALID : "nhập sai giá trị không tăng dần",
        ROUTE_LIST_PRICE_BY_VEHICLE_TYPE_NOT_INCREASE_INVALID : "nhập sai giá trị không tăng dần",
        ROUTE_LIST_TIME_INTENT_SIZE_INVALID : "danh sách thời gian dự kiến sai kích thước",
        ROUTE_LIST_POINT_ID_DUPLICATE : "danh sách các mã điểm trùng lặp",
        ROUTE_CHILDREN_TICKET_RATIO_INVALID : "tỷ lệ giá trẻ em không hợp lệ",
        ROUTE_LIST_IMAGES_INVALID : "danh sách ảnh không hợp lệ",
        ROUTE_NAME_SHORT_NULL_OR_EMPTY : "Tên ngắn của tuyến không hợp lệ",
        ROUTE_START_PROVINCE_EMPTY_OR_NULL : "tỉnh bắt đầu rỗng",
        ROUTE_END_PROVINCE_EMPTY_OR_NULL : " tỉnh kết thúc rỗng",
        ROUTE_NOT_DELETED : "tuyến không được xóa",
        ROUTE_WEIGHT_INVALID : "trọng lượng sai kiểu",
        ROUTE_DIMENTION_INVALID : "kích thước sai kiểu",
        ROUTE_VEHICLE_ID_NULL_OR_EMPTY : "mã xe rỗng",
        ROUTE_LIST_DISTANCE_INVALID : "danh sách khoảng chách sai kiểu",
        ROUTE_LIST_DISTANCE_SIZE_INVALID : "danh sách khoảng chách sai kích thước",
        ROUTE_DISTANCE_EMPTY_OR_NULL : "khoảng cách rỗng",
        ROUTE_DISTANCE_INVALID : "khoảng cách sai giá trị",
        ROUTE_LIST_PICK_UP_HOME_EMPTY_OR_NULL : "danh sách đón tại nhà rỗng",
        ROUTE_LIST_PICK_UP_HOME_INVALID : "danh sách đón tại nhà sai kiểu",
        ROUTE_LIST_PICK_UP_HOME_SIZE_INVALID : "danh sách đón tại nhà sai kích thước",
        ROUTE_NOT_UPDATE : "tuyến không được sửa",
        ROUTE_NAME_SAME : "cùng tên",
        PAGE_INVALID : "page sai giá trị",
        COUNT_INVALID : "page sai giá trị",
        TIME_ZONE_INVALID : "time zone sai kiểu",
        TIME_ZONE_NOT_OR_EMPTY : "time zone trống",
        STYLE_EMPTY : "kiểu trống",
        STYLE_INVALID : "kiểu sai giá trị",
        POINT_INFO_NULL : "đối tượng rỗng",
        POINT_INFO_POINT_ID_NULL_OR_EMPTY : "mã điểm đón rỗng",
        POINT_INFO_list_ListPickUpPoint_NULL : "lộ trình rỗng",
        POINT_INFO_ID_NULL_OR_EMPTY : "mã rỗng",
        TICKET_ID_NULL_OR_EMPTY : "Mã vé trống",
        TICKET_GET_IN_POINT_NULL_EMPTY : "Mã điểm đón khách trên vé trống",
        TICKET_GET_OFF_POINT_NULL_EMPTY : "Mã điểm trả khách trên vé trống",
        TICKET_SELLER_ID_NULL_EMPTY : "Mã người bán vé trống",
        TICKET_GET_IN_TIME_NULL_EMPTY : "Mã người bán vé trống",
        TICKET_ORIGINAL_PRICE_NULL_EMPTY : "Giá vé gốc trống",
        TICKET_PAYMENT_PRICE_NULL_EMPTY : "Giá vé gốc trống",
        TICKET_PAID_MONEY_NULL_EMPTY : "Số tiền đã thanh toán trống",
        TICKET_LIST_SEAT_INVALID : "Danh sách ghế không hợp lệ",
        TICKET_NUMBER_ADULT_INVALID : "Số người lớn không hợp lệ",
        TICKET_NUMBER_CHILDREN_INVALID : "Số người lớn không hợp lệ",
        TICKET_WEIGHT_NULL_EMPTY : "cân nặng rỗng",
        TICKET_WEIGHT_INVALID : "cân nặng sai kiểu",
        TICKET_DIMENSION_NULL_EMPTY : "kích thước null",
        TICKET_DIMENSION_INVALID : "kích thước sai kiểu",
        TICKET_GET_OFF_TIME_NULL_EMPTY : "thời gian xuống rỗng",
        TICKET_GET_OFF_TIME_INVALID : "thời gian xuống không chính xác",
        TICKET_PAID_MONEY_NEGATIVE : "tiền đã thanh toán âm",
        TICKET_NAME_NULL_OR_EMPTY : "Tên vé trống",
        TICKET_PAID_MONEY_NOT_MATCH : "số tiền đã thanh toán không khớp với quy định",
        TICKET_LIST_IMAGES_INVALID : "thời gian lên khong hợp lệ",
        TICKET_GET_IN_TIME_INVALID : "thời gian lên khong hợp lệ",
        TICKET_STATUS_INVALID : "Trạng thái vé không hợp lệ",
        TICKET_TYPE_INVALID : "loại vé không hợp lệ",
        TICKET_HOLDER_ID_NULL_EMPTY : "mã người giữ đồ trống",
        TICKET_OVER_BOOKED : "quá số lượng vé được giữ chỗ",
        TICKET_PAID_MONEY_INVALID : "Số tiền đã thanh toán sai kiểu",
        TICKET_PAYMENT_PRICE_INVALID : "Giá vé phải trả sai kiểu",
        TICKET_ORIGINAL_PRICE_INVALID : "Giá vé gốc sai giá trị",
        TICKET_GOODS_NOT_ON_THE_TRIP : "đồ chưa lên xe",
        TICKET_GUEST_NOT_ON_THE_TRIP : "khách chưa lên xe",
        TICKET_GOODS_NOT_LIST_GOODS_NEED_TRANSFER : "đồ không có trong danh sách cần đón",
        TICKET_GUEST_NOT_LIST_GOODS_NEED_TRANSFER : "người này không có trong danh sách cần đón",
        TICKET_NOT_CANCEL : "Vé không được huỷ",
        TICKET_PAID : "Vé đã thanh toán",
        TICKET_MEAL_PRICE_INVALID : "Giá vé bữa ăn sai giá trị",
        TICKET_ORDER_INVALID : "điểm bắt đầu và kết thức sai thứ tự",
        TICKET_START_DATE_EMPTY : "ngày bắt đầu trống",
        TICKET_START_DATE_INVALID : "ngày kết thúc sai kiểu",
        TICKET_PAYMENT_TYPE_INVALID : " loại thanh toán",
        TICKET_PRICE_INSURRANCE_INVALID : "sai gía bảo hiểm",
        TICKET_LIST_OPTION_NULL : "danh sách lựa chọn của khách hàng",
        RECONCILIATION_NULL : "đối tượng đối chiếu null",
        RECONCILIATION_ID_NULL_OR_EMPTY : "mã đối chiếu rỗng",
        RECONCILIATION_STATUS_NULL_OR_EMPTY : "trạng thái rỗng",
        RECONCILIATION_TOTAL_AMOUNT_ANVUI_NULL_OR_EMPTY : "tổng số tiền của anvui rỗng",
        RECONCILIATION_COMPANY_ID_NULL_OR_EMPTY : "mã công ty rỗng",
        RECONCILIATION_COMMISSION_NULL_OR_EMPTY : "hoa hồng rỗng",
        RECONCILIATION_DATE_NULL_OR_EMPTY : "thời gian bắt đầu rỗng",
        RECONCILIATION_COMMISSION_INVALID : " hoa hồng sai kiểu",
        RECONCILIATION_DATE_INVALID : "thời gian sai kiểu",
        RECONCILIATION_STATUS_INVALID : "trạng thái sai kiểu",
        RECONCILIATION_LIST_STATUS_INVALID : " danh sách trạng thái sai kiểu",
        RECONCILIATION_NOT_FIX : "không được sửa",
        RECONCILIATION_NOT_DELETE : "không được xóa",
        RECONCILIATION_ANVUI_MONEY_NULL_OR_EMPTY : "số tiền an vui thu rỗng",
        RECONCILIATION_ANVUI_MONEY_INVALID : "số tiền an vui sai kiểu",
        RECONCILIATION_ORIGINAL_TICKET_PRICE_NULL_OR_EMPTY : " giá vé gốc null",
        RECONCILIATION_ORIGINAL_TICKET_PRICE_INVALID : "giá vé gốc sai kiểu",
        RECONCILIATION_PROMOTION_PERCENT_NULL_OR_EMPTY : "phần trăm khuyến mãi null",
        RECONCILIATION_PROMOTION_PERCENT_INVALID : "phần trăm khuyến mãi sai kiểu",
        RECONCILIATION_PROMOTION_SOURCE_ID_NULL_OR_EMPTY : " nguồn khuyến mãi rỗng",
        RECONCILIATION_PAYMENT_TICKET_PRICE_NULL_OR_EMPTY : "giá vé cần thanh toán null",
        RECONCILIATION_PAYMENT_TICKET_PRICE_INVALID : "giá vé cần thanh toán sai kiểu",
        RECONCILIATION_SELLER_USER_ID_NULL_OR_EMPTY : "mã người mua rỗng",
        RECONCILIATION_DEPOSIT_MONEY_NULL_OR_EMPTY : "số tiền thanh toán qua mạng rỗng",
        RECONCILIATION_DEPOSIT_MONEY_INVALID : "số tiền thanh toán qua mạng sai kiểu",
        RECONCILIATION_GET_IN_CAR_NULL_OR_EMPTY : "khách lên xe rỗng",
        RECONCILIATION_GET_IN_CAR_MONEY_INVALID : "khách lên xe sai kiểu",
        RECONCILIATION_ASSITANTS_MONEY_NULL_OR_EMPTY : "số tiền phụ xe thu rỗng",
        RECONCILIATION_ASSITANTS_MONEY_INVALID : "số tiền phụ xe thanh toán sai kiểu",
        RECONCILIATION_COMMISSION_MONEY_NULL_OR_EMPTY : "số tiền hoa hồng rỗng",
        RECONCILIATION_COMMISSION_MONEY_INVALID : "số tiền hoa hồng sai kiểu",
        RECONCILIATION_MONEY_BACK_NULL_OR_EMPTY : "số tiền hoàn lại rỗng",
        RECONCILIATION_MONEY_BACK_INVALID : " số tiền hoàn lại sai kiểu",
        RECONCILIATION_MONEY_CANCEL_NULL_OR_EMPTY : "số tiền huỷ vé rỗng",
        RECONCILIATION_MONEY_CANCEL_INVALID : "số tiền huỷ vé sai kiểu",
        RECONCILIATION_TICKET_ID_NULL_OR_EMPTY : " mã vé rỗng",
        RECONCILIATION_TICKET_TYPE_NULL_OR_EMPTY : "loại vé rỗng",
        RECONCILIATION_TICKET_TYPE_INVALID : "loại vé sai kiểu",
        RECONCILIATION_IS_ANVUI_COTTAR_NULL_OR_EMPTY : " trạng thái an vui chốt rỗng",
        RECONCILIATION_IS_ANVUI_COTTAR_INVALID : "trạng thái an vui sai kiểu",
        RECONCILIATION_IS_COMPANY_COTTAR_NULL_OR_EMPTY : "trạng thái công ty chốt rỗng",
        RECONCILIATION_IS_COMPANY_COTTAR_INVALID : "trạng thái công ty chốt sai kiểu",
        RECONCILIATION_TICKET_ID_EXISTED : " mã vé đã tồn tại",
        RECEIPT_AND_PAYMENT_NULL : " object null",
        RECEIPT_AND_PAYMENT_ID_NULL_OR_EMPTY : "mã id rỗng",
        RECEIPT_AND_PAYMENT_ITEMS_ID_NULL_OR_EMPTY : "mã khoản mục rỗng",
        RECEIPT_AND_PAYMENT_REASON_NULL_OR_EMPTY : "lí do rỗng",
        RECEIPT_AND_PAYMENT_SPENDING_MONEY_ID_NULL_OR_EMPTY : "mã người chi tiền rỗng",
        RECEIPT_AND_PAYMENT_APPROVER_ID_NULL_OR_EMPTY : "mã người phê duyệt rỗng",
        RECEIPT_AND_PAYMENT_MONEY_RECEIVER_ID_NULL_OR_EMPTY : "mã người nhận tiền rỗng",
        RECEIPT_AND_PAYMENT_AMOUNT_SPENT_NULL_OR_EMPTY : "số tiền chi rỗng",
        RECEIPT_AND_PAYMENT_NUMBER_OF_DOCUMENTS_NULL_OR_EMPTY : "số chứng từ rỗng",
        RECEIPT_AND_PAYMENT_DATE_NULL_OR_EMPTY : "ngày rỗng",
        RECEIPT_AND_PAYMENT_AMOUNT_SPENT_INVALID : "số tiền chi rỗng",
        RECEIPT_AND_PAYMENT_NUMBER_OF_DOCUMENTS_INVALID : "số chứng từ sai kiểu",
        RECEIPT_AND_PAYMENT_DATE_INVALID : "ngày rỗng",
        RECEIPT_AND_PAYMENT_HAVING_INCREASED_INVALID : "phát sinh tăng sai kiểu",
        RECEIPT_AND_PAYMENT_HAVING_DECREASE_INVALID : "phát sinh giảm sai kiểu",
        RECEIPT_AND_PAYMENT_COMPANY_NOT_SAME : "không cùng một nhà xe",
        RECEIPT_AND_PAYMENT_NUMBER_OF_DOCUMENTS_EXISTED : "số chứng từ đã tồn tại",
        FEEDBACK_NULL : "phản hồi rỗng",
        FEEDBACK_ID_NULL : "mã phản hồi rỗng",
        FEEDBACK_TITILE_NULL_OR_EMPTY : "tiêu đề rỗng",
        FEEDBACK_CONTENT_NULL_OR_EMPTY : "nội dung rỗng",
        FEEDBACK_TICKET_ID_NULL_OR_EMPTY : "mã vé rỗng",
        FEEDBACK_TYPE_NULL_OR_EMPTY : "loại phản hồi rỗng",
        FEEDBACK_TYPE_INVALID : "loại phản hồi sai kiểu",
        FEEDBACK_VALUE_INVALID : "số sao sai kiểu",
        FEEDBACK_VALUE_NULL : "số sao rỗng",
        FEEDBACKCOMPANY_NULL : "phản hồi rỗng",
        FEEDBACKCOMPANY_ID_NULL : "mã phản hồi rỗng",
        FEEDBACKCOMPANY_TITILE_NULL_OR_EMPTY : "tiêu đề rỗng",
        FEEDBACKCOMPANY_CONTENT_NULL_OR_EMPTY : " nội dung rỗng",
        FEEDBACKCOMPANY_COMPANY_ID_NULL_OR_EMPTY : "mã vé rỗng",
        FEEDBACKCOMPANY_TYPE_NULL_OR_EMPTY : "loại phản hồi rỗng",
        FEEDBACKCOMPANY_TYPE_INVALID : " loại phản hồi sai kiểu",
        FEEDBACKCOMPANY_VALUE_INVALID : "số sao sai kiểu",
        FEEDBACKCOMPANY_VALUE_NULL : "số sao rỗng",
        FEEDBACKCOMPANY_TRIP_ID_NULL_OR_EMPTY : "mã trip Id",
        FEEDBACKCOMPANY_EXISTED : " bình luận đã tồn tại",
        FEEDBACKCOMPANY_TRIP_NOT_FINISHED : "chuyến chưa kết thúc",
        FEEDBACKCOMPANY_TRIP_NULL : "chuyến đi null",
        ITEMS_NULL : "khoản mục rỗng",
        ITEMS_ID_NULL_OR_EMPTY : "mã khoản mục rỗng",
        ITEMS_NAME_NULL_OR_EMPTY : "tên khoản mục rỗng",
        ITEMS_USER_ID_NULL_OR_EMPTY : "mã user rỗng",
        ITEMS_TYPE_INVALID : "mã loại sai kiểu",
        ITEMS_TYPE_NULL_OR_EMPTY : "mã loại rỗng",
        ITEMS_TYPE_SAME : " trùng mã loại",
        COTTAR_RECEIPT_AND_PAYMENT_NULL : "Chốt rỗng",
        COTTAR_RECEIPT_AND_PAYMENT_ID_NULL_OR_EMPTY : "Chốt ID rỗng",
        SCHEDULE_NULL : "đối tượng rỗng",
        SCHEDULE_ID_NULL_OR_EMPTY : " ma rỗng",
        SCHEDULE_NAME_NULL_OR_EMPTY : "tên rỗng",
        SCHEDULE_LIST_VEHICLE_ID_NULL_OR_EMPTY : "danh sách mã xe rỗng",
        SCHEDULE_ROUTE_ID_NULL_OR_EMPTY : "ma lộ trình rỗng",
        SCHEDULE_LIST_ASSISTANT_ID_NULL : "ma phụ xe rỗng",
        SCHEDULE_LIST_DRIVER_ID_NULL : "ma lái xe rỗng",
        SCHEDULE_USER_ID_NULL_OR_EMPTY : " ma rỗng",
        SCHEDULE_START_TIME_NULL_OR_EMPTY : "thời gian bắt đầu rỗng",
        SCHEDULE_RUN_TIME_NULL_OR_EMPTY : "thời gian kết thúc rỗng",
        SCHEDULE_START_TIME_INVALID : "thời gian bắt đầu rỗng",
        SCHEDULE_RUN_TIME_INVALID : "thời gian kết thúc sai kiểu",
        SCHEDULE_PAGE_INVALID : " page sai kiểu",
        SCHEDULE_COUNT_INVALID : "count sai kiểu",
        SCHEDULE_STATUS_INVALID : "trạng thái sai kiểu",
        SCHEDULE_START_DATE_NULL_OR_EMPTY : "ngày bắt đầu",
        SCHEDULE_ROUTE_FOR_USER_NULL : "đối tượng rỗng",
        SCHEDULE_LIST_DATE_NULL : "danh sách ngày rỗng",
        SCHEDULE_LIST_DATE_INVALID : "danh sách ngày sai kiểu",
        SCHEDULE_LIST_ASSISTANT_ID_INVALID : "ma phụ xe rỗng",
        SCHEDULE_LIST_DRIVER_ID_INVALID : "ma lái xe rỗng",
        SCHEDULE_START_DATE_FORMAT_INVALID : "ngày bắt đầu sai định dạng",
        SCHEDULE_END_DATE_FORMAT_INVALID : " ngày kết thúc sai định dạng",
        SCHEDULE_SEAT_MAP_INVALID : "object seat map sai định dạng",
        SCHEDULE_DUPLICATE_CALENDAR_INVALID : "lỗi trùng lịch",
        SCHEDULE_LIST_VEHICLE_ID_NOT_EXIT : "danh sách mã xe không tồn tại",
        SCHEDULE_ROUTE_ID_NOT_EXIT : "ma lộ trình không tồn tại",
        SCHEDULE_USER_ID_NOT_EXIT : "lái xe và phụ xe không tồn tại",
        SCHEDULE_DUPLICATE_CALENDAR_DATASTORE_INVALID : "lỗi trùng lịch trong datastore",
        SCHEDULE_NOT_FIX : "schedule không được sữa do đã có vé",
        SCHEDULE_END_DATE_NULL_OR_EMPTY : "ngày kết thúc rỗng",
        SCHEDULE_LIST_STATUS_INVALID : "danh sách trạng thái sai kiểu",
        SCHEDULE_START_DATE_INVALID : "ngày bắt đầu sai kiểu",
        SCHEDULE_SEAT_MAP_NULL : "object seat map null",
        SCHEDULE_VEHICLE_TYPE_ID_NOT_EXIT_IN_ROUTE : "mã loại xe chưa có trong route",
        SCHEDULE_CYCLE_NULL_OR_EMPTY : "chu kì rỗng",
        SCHEDULE_NUMBER_OF_CYCLE_NULL_OR_EMPTY : "số chu kì rỗng",
        SCHEDULE_CYCLE_INVAID : "chu ki sai kiểu",
        SCHEDULE_NUMBER_OF_CYCLE_INVALID : " số chu kì sai kiểu",
        SCHEDULE_LIST_TYPE_INVALID : " danh sách loại lịch sai kiểu",
        SCHEDULE_TYPE_NULL_OR_EMPTY : "loại lịch sai kiểu",
        SCHEDULE_TYPE_INVALID : "loại lịch sai kiểu",
        SCHEDULE_TRIP_ALREADY_EXISTS : "trip đã có",
        SCHEDULE_END_DATE_INVALID : "ngày kết thúc sai kiểu",
        SCHEDULE_DATE_INVALID : "ngày sai kiểu",
        SCHEDULE_DATE_NOT_EXIT_IN_SCHEDULE : "ngày không có trong lịch này",
        SCHEDULE_DATE_EXIT : "ngày đã tồn tại",
        SCHEDULE_END_DATE_OR_NUMBER_OF_CYCLE_EMPTY : "ngày kết thúc hoặc chu kì rỗng",
        SCHEDULE_NOT_DELETE : "schedule không được xóa do đã có vé",
        SCHEDULE_DUPLICATE_CALENDAR_IN_LIST_DATE_INVALID : " lỗi trùng lịch trong listDate",
        SCHEDULE_DUPLICATE_CALENDAR_IN_CYCLE_DATE_INVALID : "lỗi trùng lịch trong listDate",
        SCHEDULE_DATE_SOLD_NOT_IN_SCHEDULE_UPDATE : "ngày đã bán ở lịch cũ không có trong lịch mới",
        SCHEDULE_DATE_NEW_SOLD_IN_SCHEDULE : "ngày đã bán có trong lịch rồi",
        SCHEDULE_NOT_SAME_COMPANY : "không cùng nhà vận tải",
        SCHEDULE_SEAT_MAP_ID_NULL_OR_EMPTY : " ma xe rỗng",
        SCHEDULE_TYPE_NOT_UPDATE : "loại schedule không được update",
        SCHEDULE_LIST_UPDATE_FOR_DATE_INVALID : "cập nhật lịch theo ngày",
        SEAT_MAP_ID_NULL : "mã sơ dồ ghế trống",
        SEAT_MAP_NULL : "sơ đồ ghế không tồn tại",
        SEAT_MAP_NUMBER_OF_ROWS_INVALID : "số hàng không hợp lế",
        SEAT_MAP_NUMBER_OF_COLUMNS_INVALID : "số cột không hợp lệ",
        SEAT_MAP_NUMBER_OF_FLOORS_INVALID : " số tầng không hợp lệ",
        SEAT_BOOKED : " Ghế đã đặt",
        SEAT_NOT_EXIST_IN_SEAT_MAP : "Ghế không có trong sơ đồ ghế",
        TRIP_ACTIVITY_NULL : "nhật trình rỗng",
        TRIP_ACTIVITY_ID_NULL : "mã nhật trình rỗng",
        TRIP_ACTIVITY_USER_ID_NULL : "mã người tạo trống",
        TRIP_ACTIVITY_TRIP_ID_NULL : " mã hành trình trống",
        TRIP_ACTIVITY_TRIP_TYPE_INVALID : "kiểu hành trình trống",
        TRIP_COMPLETED_OR_CANCELED : "chuyến đi đã kết thúc hoặc đã dừng lại",
        TRIP_CURRENT_IN_LAST_POINT : " chuyến đi đang ở điểm cuối",
        CHANGE_VEHICLE_ID_NULL : "mã chuyển xe rỗng",
        CHANGE_VEHICLE_NULL : "chuyển xe null",
        CHANGE_VEHICLE_SOURCE_TRIP_ID_NULL : "mã hành trình gốc rỗng",
        CHANGE_VEHICLE_DESTINATION_TRIP_ID_NULL : "mã hành trình gốc đích",
        CHANGE_VEHICLE_USER_ID_NULL : "mã người tạo rỗng",
        CHANGE_VEHICLE_DESTINATION_SEAT_LIST_SIZE_LESS_THAN_SOURCE_LIST : "số ghế chuyển nhiều hơn số ghế trống bên xe đích",
        CHANGE_VEHICLE_IS_IMPOSSIBLE : " không thể chuyển được",
        CHANGE_VEHICLE_NUMBER_OF_GUESTS_NULL_INVALID : "không có số khách",
        SMS_INFO_ID_NULL : "mã tin nhắn trống",
        SMS_INFO_NULL : "tin nhắn trống",
        SMS_COUNTER_ID_NULL : "mã tin nhắn trống",
        SMS_COUNTER_NULL : "tin nhắn trống",
        SEAT_TYPE_INVALID : "loại ghế không hợp lệ",
        SEAT_TYPE_NULL : "loại ghế rỗng",
        NUMBER_OF_SEATS_INVALID : "số lượng ghế không hợp lệ",
        ITEMS_TYPE_INVAILD : "khoản mục không hợp lệ",
        RECEIPT_AND_PAYMENT_TYPE_INVAILID : "loại thu chi không hợp lệ",
        SEAT_MAP_TYPE_INVALID : "loại ghế không hợp lệ",
        TICKET_CODE_NULL : "mã vẽ null",
        EDUCATION_NULL_OR_EMPTY : "trình độ học vấn null",
        RECEIPT_AND_PAYMENT_RECEIVER_NULL_OR_EMPTY : "người nhận rỗng",
        RECEIPT_AND_PAYMENT_SENDER_NULL_OR_EMPTY : "người nộp tiền rỗng",
        REPORT_START_DATE_END_DATE_TOO_MUCH : "Dữ liệu không được lấy quá 1 tháng",
        SEAT_BOOKED : "Ghế đã được đặt",
        USR_ERROR_000000001: "Tài khoản không có quyền",
    };
</script>
</body>
</html>


