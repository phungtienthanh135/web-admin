<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->
<head>

    <title>An Vui - @yield('title')</title>
    <!-- Meta -->
    <meta charset="UTF-8"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=vietnamese" rel="stylesheet">


    <!-- Icon -->
    <link rel="shortcut icon" href="/public/favicon.png">
    <!-- Bootstrap -->
    <link href="/public/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/public/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet"/>

    <!-- Bootstrap Extended -->
    <link href="/public/bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="/public/bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap-responsive.min.css" rel="stylesheet">
    <link href="/public/bootstrap/extend/bootstrap-wysihtml5/css/bootstrap-wysihtml5-0.0.2.css" rel="stylesheet">

    <!-- Select2 -->
    <link rel="stylesheet" href="/public/theme/scripts/plugins/forms/select2-4.0.3/dist/css/select2.min.css"/>
    <link rel="stylesheet" href="/public/theme/scripts/plugins/forms/select2-4.0.3/dist/css/select2-bootstrap.min.css"/>
    <!-- Gritter Notifications Plugin -->
    <link href="/public/theme/scripts/plugins/notifications/Gritter/css/jquery.gritter.css" rel="stylesheet"/>
    <!-- JQueryUI v1.9.2 -->
    <link rel="stylesheet"
          href="/public/theme/scripts/plugins/system/jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.min.css"/>
    <!-- Glyphicons -->
    <link rel="stylesheet" href="/public/theme/css/glyphicons.css"/>
    <!-- Bootstrap Extended -->
    <link rel="stylesheet" href="/public/bootstrap/extend/bootstrap-select/bootstrap-select.css"/>
    <link rel="stylesheet"
          href="/public/bootstrap/extend/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css"/>
    <!-- Uniform -->
    <link rel="stylesheet" media="screen"
          href="/public/theme/scripts/plugins/forms/pixelmatrix-uniform/css/uniform.default.css"/>
    <!-- JQuery v1.8.2 -->
    <script src="/public/theme/scripts/plugins/system/jquery-1.8.2.min.js"></script>
    <!-- JQueryUI v1.9.2 -->
    <script src="/public/theme/scripts/plugins/system/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js"></script>

    <!-- Modernizr -->
    <script src="/public/theme/scripts/plugins/system/modernizr.custom.76094.js"></script>

    <!-- Theme -->
    <link rel="stylesheet" href="/public/theme/css/jquery.amlich.css"/>
    <link rel="stylesheet" href="/public/theme/css/style.css"/>
    <link rel="stylesheet" href="/public/theme/css/fix.css"/>
    <link rel="stylesheet" href="/public/theme/css/custom.css"/>
    <!-- LESS 2 CSS -->
    <script src="/public/theme/scripts/plugins/system/less-1.3.3.min.js"></script>

    <!-- jQuery Validate -->
    <script src="/public/theme/scripts/plugins/forms/jquery-validation/dist/jquery.validate.min.js"
            type="text/javascript"></script>
    <!--[if IE]>
    <script type="text/javascript" src="/public/common/theme/scripts/plugins/other/excanvas/excanvas.js"></script>
    <![endif]-->
    <!--[if lt IE 8]>
    <script type="text/javascript" src="/public/common/theme/scripts/plugins/other/json2.js"></script><![endif]-->
    <!-- chuyển chữ thành số -->
    <script src="/public/javascript/DocSo.js"></script>
    <!-- jQuery Slim Scroll Plugin -->
    <script src="/public/theme/scripts/plugins/other/jquery-slimScroll/jquery.slimscroll.min.js"></script>
    <!-- Uniform -->
    <script src="/public/theme/scripts/plugins/forms/pixelmatrix-uniform/jquery.uniform.min.js"></script>
    <!-- Bootstrap Script -->
    <script src="/public/bootstrap/js/bootstrap.min.js"></script>
    <!-- Bootstrap Extended -->
    <script src="/public/bootstrap/extend/bootstrap-select/bootstrap-select.js"></script>
    <script src="/public/bootstrap/extend/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
    <!-- Gritter Notifications Plugin -->
    <script src="/public/theme/scripts/plugins/notifications/Gritter/js/jquery.gritter.min.js"></script>
    <!-- Select2 -->
    <script src="/public/theme/scripts/plugins/forms/select2-4.0.3/dist/js/select2.js"></script>
    <script src="/public/theme/scripts/plugins/forms/select2-4.0.3/dist/js/i18n/vi.js"
            type="application/javascript"></script>
    <script src="/public/javascript/jquery.number.min.js" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <!-- Clockpicker -->
    <link rel="stylesheet" type="text/css" href="/public/javascript/clockpicker/dist/bootstrap-clockpicker.min.css">
    <script type="text/javascript" src="/public/javascript/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
    <!-- Font Awesome Free 5.0.1 -->
    <link rel="stylesheet" href="/public/theme/css/fontawesome-all.css">
</head>
<body>
<!-- Start Content - Menu left -->
<div class="container-fluid">
    <div class="row-fluid">
        <div class="navbar main hidden-print">
            <div id="ht_btn_close_open_menu">
                <button onClick="close_menu()" type="button" class="btn-navbar">
                    <div class="icon_close_menu"></div>
                </button>
            </div>
            <img src="/public/dangnhap_skin/logo.png"/>
            <ul class="topnav pull-right">
                <li class="visible-desktop">
                    <ul class="notif">
                        {{--<li class="dropdown visible-desktop" data-toggle="tooltip" data-placement="bottom"
                            data-original-title="5 new messages"> <a href="" data-toggle="dropdown" class="glyphicons envelope"><i></i> <span
                                        class="ht_noti_count">5</span></a>
                            <ul class="dropdown-menu pull-right notification_app">
                                <div class="log-arrow-up"></div>
                                <div class="header_noti">
                                    <h3 class="uiHeaderTitle" aria-hidden="true">Thông báo</h3>
                                </div>
                                <div class="ht_noti">
                                    <div class="noti_body">
                                        <div class="ht_img"> <img src=""
                                                                  height="52" width="52"> </div>
                                        <div class="ng_noti"> <a class="name_user">Hoàng Long</a> <span class="date_noti">03/04/2017</span>
                                            <p>Chúng tôi đã nhận được yêu cầu đặt lại mật khẩu Uber của bạn. Nhấp vào liên
                                                kết bên dưới để chọn.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="header_noti">
                                    <div class="text-center"> <a href="#xemtatca"><b>Xem hết thư</b></a> </div>
                                </div>
                            </ul>
                        </li>--}}



                        <li onClick="" class="">
                            <a href="" data-toggle="" class="glyphicons iconic-question-mark"><i></i></a>
                            <ul class="dropdown-menu pull-right">
                                <div class="log-arrow-up"></div>
                            </ul>
                        </li>


                        <li class="account"><a data-toggle="dropdown" href="" class="glyphicons user"><i> </i> </a>
                            <ul class="dropdown-menu pull-right infor_account">
                                <li><a href="{{action('HomeController@account')}}" class="glyphicons user"><i
                                                class="icon_left"> </i> <span
                                                class="m_left_30">Thông tin tài khoản</span></a></li>
                                @if(hasAnyRole(CHANGE_PASSWORD))
                                    <li><a style="cursor:pointer" data-toggle="modal" data-target="#ChangePass"><i
                                                    class="icon_left glyphicon glyphicon-lock"> </i> <span
                                                    class="m_left_10">Đổi mật khẩu</span></a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                        <li>
                            <a data-toggle="modal" data-target="#Logout" style="cursor:pointer"
                               class="glyphicons power"><i></i></a>
                        </li>
                        <li onClick="focustimkiem()" class="dropdown visible-desktop">
                            <a href="" data-toggle="dropdown" class="glyphicons search"><i></i></a>
                            <ul class="dropdown-menu pull-right">
                                <div class="log-arrow-up"></div>
                                <li>
                                    <form class="form-inline" action="">
                                        <div class="input-append">
                                            <input autocomplete="off" type="text" class="txtsearch" id="txtsearch"
                                                   placeholder="tìm kiếm thông tin">
                                            <button style="height: 30px;" class="btn-default btn-flat"><i
                                                        class="icon-search"></i> Tìm
                                            </button>
                                        </div>
                                    </form>
                                </li>
                            </ul>
                        </li>   
                    </ul>
                </li>
            </ul>

        </div>
    </div>

    <div class="row-fluid">
        @include('cpanel.template.MenuHelp')

        @yield('content')

    </div>

</div>
@if(hasAnyRole(CHANGE_PASSWORD))
    <!--modal doi mat khau-->
    <div class="modal modal-custom hide fade modal_logout" id="ChangePass">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 class="center">Thay đổi mật khẩu</h3>
        </div>
        <form action="{{action('UserController@changePassword')}}" method="post" class="frm_changepass">
            <div class="modal-body">
                <input autocomplete="off" placeholder="Mật khẩu củ" type="password" name="oldPassword"
                       data-validation="required" data-validation-error-msg-required="Vui lòng nhập mật khẩu củ">
                <input autocomplete="off" placeholder="Mật khẩu mới" type="password" name="newPassword"
                       data-validation="required" data-validation-error-msg-required="Vui lòng nhập mật khẩu mới">
                <input autocomplete="off" placeholder="Nhập lại mật khẩu" type="password"
                       data-validation="confirmation required" data-validation-confirm="newPassword"
                       data-validation-error-msg-required="Vui lòng nhập lại mật khẩu mới chính xác"
                       data-validation-error-msg-confirmation="Mật khẩu xác nhận không chính xác">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
            </div>
            <div class="modal-footer">
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true"
                   class="btn_huy_modal">HỦY</a>
                <button class="btn btn btn-warning btn-flat">LƯU</button>
            </div>
        </form>
    </div>
@endif
<!--modal dang xuat-->
<div class="modal hide fade modal_logout" id="Logout">
    <div class="modal-body center">
        <p>Bạn có chắc muốn đăng xuất?</p>
    </div>
    <div class="modal-footer">
        <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
        <a href="{{route('cpanel.login.logout')}}" class="btn btn btn-warning btn-flat">ĐỒNG Ý</a>
    </div>
</div>


<div class="modal hide fade modal_notify" id="notify">
    <div class="modal-body center">
        <p>Có cuộc gọi đến</p>
    </div>
    <div class="modal-footer">
        <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
        <a href="{{action('TicketController@sell')}}" class="btn btn btn-warning btn-flat">BÁN VÉ</a>
    </div>
</div>




<script src="/public/javascript/anvui.js" type="text/javascript"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase-messaging.js"></script>

<script>
    {!! session('msg') !!}
    @stack('activeMenu')

    firebase.initializeApp({
        'messagingSenderId': '999268943503'
    });

    const messaging = firebase.messaging();

    messaging.onMessage(function(payload) {
        console.log(payload);
    });
</script>
</body>
</html>


