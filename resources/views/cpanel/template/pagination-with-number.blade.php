@php($requestOfPagination=request()->toArray())
@php($actionName=last(explode('\\',Route::currentRouteAction())))
<div class="pagination pagination-centered">
    <ul class="m_top_10">
        @if($page<=1)
            <li class="disabled"><a>«</a></li>
        @else
            @php($requestOfPagination['page']=$page-1)
            <li>
                <a href="{{action($actionName,$requestOfPagination)}}">«</a>
            </li>
            <li>
                <a href="{{action($actionName,$requestOfPagination)}}">{{$page-1}}</a>
            </li>
        @endif

        <li class="active"><a
                    href="{{action($actionName,$requestOfPagination)}}">{{$page}}</a>
        </li>
            @php($requestOfPagination['page']=$page+1)
        <li>
            <a href="{{action($actionName,$requestOfPagination)}}">{{$page+1}}</a>
        </li>
        <li>
            <a href="{{action($actionName,$requestOfPagination)}}">»</a>
        </li>
    </ul>
</div>