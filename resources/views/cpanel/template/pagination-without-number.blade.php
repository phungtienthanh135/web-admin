@php($requestOfPagination=request()->toArray())
@php($actionName=last(explode('\\',Route::currentRouteAction())))
<ul class="pager m_top_15 m_bottom_0">
    @if($page<=1)
        <li class="previous disabled"><a href="#">← QUAY LẠI</a></li>
    @else
        @php($requestOfPagination['page']=$page-1)
        <li class="previous"><a href="{{action($actionName,$requestOfPagination)}}">← QUAY LẠI</a></li>
    @endif

    @php($requestOfPagination['page']=$page+1)

    <li class="next"><a href="{{action($actionName,$requestOfPagination)}}">TIẾP THEO →</a></li>
</ul>