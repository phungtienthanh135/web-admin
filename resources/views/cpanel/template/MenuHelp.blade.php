<
<div id="menu">
    <div id="menuInner">
        <!-- Scrollable menu wrapper with Maximum height -->
        <div class="logo-user">
            <img src="{{session('userLogin')['logo']}}">
        </div>
        <div class="slim-scroll">
            <ul class="check_active_link">
                {{--<li class="heading"><span>Category</span></li>--}}
                <li class="glyphicons train active"><a
                            href="{{action('HelpController@show','Home')}}"><i></i><span>Home</span></a></li>
                @if(hasAnyRole([SELL_TICKET,GET_LIST_TICKET,GETLIST_PROMOTION]))
                    <li class="hasSubmenu"> 
                        <a class="glyphicons notes"
                           href="{{action('HelpController@show','ticket')}}"><i></i><span>Bán vé</span><span
                                    class="dcjq-icon"></span></a>
                    </li>
                @endif

                <li class="hasSubmenu">
                    <a class=" glyphicons gift" href="{{action('HelpController@show','ship')}}"><i></i><span>Gửi đồ</span>
                            <span class="dcjq-icon"></span></a>
                </li>
                @if(hasAnyRole([GET_LIST_OUT_OF_STATION_COMMAND,GET_LIST_TRIP_ACTIVITY]))
                    <li class="hasSubmenu ">
                        <a class="glyphicons bus"
                           href="{{action('HelpController@show','trip')}}"><i></i><span>Điều hành xe</span>
                            <span class="dcjq-icon"></span></a>
                    </li>
                @endif
                @if(hasAnyRole([GET_LIST_TRIP_FOR_USER_TRIP,GET_LIST_SCHEDULE,CREATE_SCHEDULE]))
                    <li class="hasSubmenu">
                        <a class="glyphicons calendar" href="{{action('HelpController@show','tripes')}}"><i></i><span>Lập lịch chạy xe</span>
                            <span class="dcjq-icon"></span> </a>
                    </li>
                @endif
                @if(hasAnyRole([GET_LIST_ROUTE,CREATE_ROUTE,GET_LIST_POINT,CREATE_POINT]))
                    <li class="hasSubmenu">
                        <a class="glyphicons road" href="{{action('HelpController@show','route')}}"><i></i><span>Quản lý tuyến đường</span><span
                                    class="dcjq-icon"></span></a>
                    </li>
                @endif
                @if(hasAnyRole([GET_LIST_VEHICLE,GET_LIST_VEHICLE_TYPE,CREATE_VEHICLE]))
                    <li class="hasSubmenu">
                        <a class="glyphicons car" href="{{action('HelpController@show','vehicle')}}"><i></i><span>Quản lý phương tiện</span><span
                                    class="dcjq-icon"></span></a>
                    </li>
                @endif
                @if(hasAnyRole([GET_LIST_EMPLOYEE,CREATE_NEW_EMPLOYEE,NOTIFY_SCHEDULE_ASSISTANT_DRIVER,GET_LIST_GROUP_PERMISSION]))
                    <li class="hasSubmenu">
                        <a class="glyphicons user"
                           href="{{action('HelpController@show','user')}}"><i></i><span>Quản lý nhân viên</span><span
                                    class="dcjq-icon"></span></a>
                    </li>
                @endif
                @if(hasAnyRole([GET_LIST_CUSTOMER,GET_LIST_FEEDBACK]))
                    <li class="hasSubmenu">
                        <a class="glyphicons group" href="{{action('HelpController@show','customer')}}"><i></i><span>Quản lý khách hàng</span><span
                                    class="dcjq-icon"></span></a>
                    </li>
                @endif
                {{--TODO chưa check quyền--}}
                @if(hasAnyRole(GET_LIST_EMPLOYEE))
                    <li class="hasSubmenu">
                        <a class="glyphicons global"
                           href="{{action('HelpController@show','agency')}}"><i></i><span>Quản lý đại lý</span><span
                                    class="dcjq-icon"></span></a>
                    </li>
                @endif
                @if(hasAnyRole([GET_LIST_RECEIPT_AND_PAYMENT,REPORT_RECEIPT_AND_PAYMENT]))
                    <li class="hasSubmenu">
                        <a class="glyphicons coins" href="{{action('HelpController@show','balance-sheet')}}"><i></i><span>Quản lý thu chi</span><span
                                    class="dcjq-icon"></span></a>
                        <ul class="in collapse" id="receipt">
                        </ul>
                    </li>
                @endif
                @if(hasAnyRole([GET_LIST_ITEMS]))
                    <li class="hasSubmenu">
                        <a class="glyphicons show_big_thumbnails" href="{{action('HelpController@show','item')}}"><i></i><span>Danh mục</span>
                            <span class="dcjq-icon"></span></a>
                    </li>
                @endif
                {{--TODO chưa check quyền--}}
                <li class="hasSubmenu"><a class="glyphicons charts"
                    href="{{action('HelpController@show','report')}}"><i></i><span>Báo cáo</span><span class="dcjq-icon"></span></a>
                </li>
                @if(hasAnyRole([CONFIG_COMPANY_INFO]))
                    <li class="hasSubmenu">
                        <a class="glyphicons settings"
                           href="{{action('HelpController@show','system')}}"><i></i><span>Hệ thống</span><span
                                    class="dcjq-icon"></span></a>
                    </li>
                @endif
            </ul>

            <p class="bq">© 2017 ANVUI.VN</p>
            <p class="bq">All right reserved</p>
        </div>
    </div>
    <!-- // Nice Scroll Wrapper END -->
</div>