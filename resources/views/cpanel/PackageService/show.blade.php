@extends('cpanel.template.layout_bt')
@section('title', 'Gói dịch vụ')

@section('content')
    <div class="container">
        <div class="row">
            @foreach($listPackage as $pack)
            <div class="col-md-3 col-sm-6 col-xs-12">
                <form action="{{ action('PackageServiceController@buyPackage') }}" method="post">
                    <div class="q-box">
                        <div class="q-box-name">
                            {{ $pack['servicePackName'] }}
                        </div>
                        <div class="q-box-price">@moneyFormat($pack['price'])</div>
                        <div class="q-box-description">
                            Mô tả : {{ $pack['servicePackDescription'] }}
                        </div>
                        <input type="hidden" name="ticketId" value="{{ $pack['servicePackId'] }}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" class="q-box-link">Dùng thử</button>
                    </div>
                </form>
            </div>
            @endforeach
        </div>
        <br>
        <div class="row">
            <div class="col-md-4 col-xs-12 col-md-offset-4"><a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-warning form-control">Quay Lại</a></div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 text-center" style="color:red">{!! session('msg') !!}</div>
        </div>
    </div>
@endsection