@extends('cpanel.template.layout')
@section('title', 'In phơi vé')
@push('activeMenu')
    activeMenu('{{action('TripController@control',[],true)}}');
@endpush
@section('content')
    <?php
    $listDriver = $result['listDriver'];
    $listAss = $result['listAss'];
    $listDriverName = [];
    //dev($result);
    $listAssistantName = [];
    foreach ($listDriver as $driver) {
        $listDriverName[] = $driver['fullName'];
    }
    foreach ($listAss as $ass) {
        $listAssistantName[] = $ass['fullName'];
    }
    ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
        .dathanhtoan {
            float: left;
            border: 8px solid;
            padding: 5px;
            margin-right: 10px;
        }

        .chuathanhtoan {
            float: left;
            border: 1px solid;
            padding: 5px;
            margin-right: 10px;
        }

    </style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>In phơi vé</h3></div>
            </div>


            <script type="text/javascript"
                    src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
            <script type="text/javascript" src="/public/javascript/export/helper-export.js"></script>

            <div class="innerLR">
                <section class="container-fluid">
                    <div class="widget widget-2 widget-tabs widget-tabs-2 no_border p_bottom_0">
                        <div class="widget-head">
                            <ul class="chonbanve">
                                <li class="active">
                                    <a class="" href="#DsKhachHang" data-toggle="tab"><i></i>DANH SÁCH HÀNH KHÁCH</a>
                                </li>
                                <li><a class="" href="#Lenhvanchuyen" data-toggle="tab"><i></i>LỆNH VẬN CHUYỂN</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="DsKhachHang">
                                <div class="print_content row-fluid">
                                    <div style="padding-top:20px;">
                                        <div class="span1"><a class="btn" onclick="printDiv('printMap')"
                                                              style="cursor: pointer"><img
                                                        src="/public/images/printer.png"/></a></div>
                                        <div class="span3">Chọn loại sơ đồ để in:</div>
                                        <div class="span2">
                                            <label class="radio">
                                                <input id="cbList" checked="checked" type="radio" name="radio" value="2"
                                                       onclick="showTable(this)">
                                                Danh sách đón khách
                                            </label>
                                        </div>
                                        <div class="span3">
                                            <label class="radio">
                                                <input id="cbMap" type="radio" name="radio" value="1"
                                                       onclick="showTable(this)">
                                                Sơ đồ ghế
                                            </label>
                                        </div>
                                        <div class="btn-group export-btn">
                                            <button id="exel" class="btn btn-report btn-default btn-flat"
                                                    data-toggle="dropdown"><i class="fa-file-excel-o"></i>Xuất
                                                file <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a id="excel" data-fileName="danh_sach_chuyen">Excel</a></li>
                                                <li><a id="pdf" data-fileName="danh_sach_chuyen">PDF</a></li>
                                            </ul>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                    <hr class="separator">
                                    <div id="printMap">
                                        @php
                                            $time = strtotime($result['startDateReality']) + $result['startTimeReality']/1000 + 7*3600;
                                            $width = 100/$result['seatMap']['numberOfColumns'];
                                            $floor2 = array_where($result['seatMap']['seatList'],function ($item){
                                                return $item['floor'] == 2;
                                            });
                                        @endphp
                                        <div id="title-table">
                                            <h3 align="center">SƠ ĐỒ GHẾ</h3>
                                            <h4 align="center">
                                                Chuyến: {{$result["routeName"]}} (@timeFormat($time)) - Ngày:
                                                @dateFormat($time*1000)
                                            </h4>
                                        </div>

                                        <?php if ((session('companyId') == 'TC03013FPyeySDW')) { ?>

                                         <?php
                                            echo '<div id="table-map_SaoNghe" ><table class="table table-condensed"><thead><tr>';
                                            for ($column = 1; $column <= ($result['seatMap']['numberOfColumns'] * 2); $column++) {
                                                if ($column == 2 || $column == 5) {
                                                    if ($column == 5) echo '<td style="width: 4%"></td>';
                                                    echo '<td  style="width:24%;border: 1px solid black;margin-top:-10px; margin-bottom:-10px;text-align: center">
                                                    <strong style="font-size: 16px">Tầng 1</strong></td>';
                                                } else if ($column == 1 || $column == 6) {
                                                    echo '<td  style="width:24%;border: 1px solid black;margin-top:-10px; margin-bottom:-10px;text-align: center">
                                                    <strong style="font-size:16px">Tầng 2</strong></td>';
                                                }
                                            }
                                            echo '</tr></thead><tbody >';

                                            $seatList = $result['seatMap']['seatList'];
                                            $count_col = $result['seatMap']['numberOfColumns'];
                                            $count_row = $result['seatMap']['numberOfRows'];
                                            for ($row = $count_row; $row > 1; $row--) {
                                                echo '<tr style="height: 175px">';
                                                for ($column = (($count_col - 1) * 2); $column > 0; $column--) {
                                                    //echo ' <td style = "border: 1px solid black;width: 25%">';
                                                    if ($column == 4 && $row != 7) {
                                                        if ((($count_row * 3 + 2) - ($count_row - $row - 1) * 2) <= 24) {
                                                            $seat = $seatList[($count_row * 3 + 2) - ($count_row - $row - 1) * 2];
                                                            echo ' <td style = "border: 1px solid black;width: 24%">';
                                                            if ($seat['seatStatus'] == 3)
                                                                echo '<div style=" float: left;border: 8px solid;padding: 5px;margin-right: 10px;"><h4 style="margin: 1px"">' . $seat['seatId'];
                                                            else  echo '<div style=" float: left;border: 1px solid;padding: 5px;margin-right: 10px;"><h4 style="margin: 1px" >' . $seat['seatId'];
                                                        }
                                                    } else
                                                        if ($column == 3 && $row != 7) {
                                                            if ((13 - ($count_row - $row) * 2) <= 11) {
                                                                $seat = $seatList[13 - ($count_row - $row) * 2];
                                                                echo ' <td style = "border: 1px solid black;width: 24%">';
                                                                if ($seat['seatStatus'] == 3)
                                                                    echo '<div style=" float: left;border: 8px solid;padding: 5px;margin-right: 10px;"><h4 style="margin: 1px">' . $seat['seatId'];
                                                                else  echo '<div style=" float: left;border: 1px solid;padding: 5px;margin-right: 10px;"><h4 style="margin: 1px">' . $seat['seatId'];

                                                            }
                                                        } else
                                                            if ($column == 2) {
                                                                echo ' <td style = "width: 3%"></td>';
                                                                $seat = $seatList[12 - ($count_row - $row) * 2];
                                                                echo ' <td style = "border: 1px solid black;width: 24%">';
                                                                if ($seat['seatStatus'] == 3)
                                                                    echo '<div style=" float: left;border: 8px solid;padding: 5px;margin-right: 10px;"><h4 style="margin: 1px">' . $seat['seatId'];
                                                                else  echo '<div style=" float: left;border: 1px solid;padding: 5px;margin-right: 10px;"><h4 style="margin: 1px">' . $seat['seatId'];

                                                            } else if ($column == 1) {
                                                                if (24 - ($count_row - $row) * 2 >= 14) {
                                                                    $seat = $seatList[24 - ($count_row - $row) * 2];
                                                                    echo ' <td style = "border: 1px solid black;width: 24%">';
                                                                    if ($seat['seatStatus'] == 3)
                                                                        echo '<div style=" float: left;border: 8px solid;padding: 5px;margin-right: 10px;"><h4 style="margin: 1px">' . $seat['seatId'];
                                                                    else  echo '<div style=" float: left;border: 1px solid;padding: 5px;margin-right: 10px;"><h4 style="margin: 1px">' . $seat['seatId'];

                                                                }
                                                            } else echo ' <td style = "border: 1px solid black;width: 25%">';

                                                    if (isset($seat['seatStatus'])) {
                                                        switch ($seat['seatStatus']) {
                                                            case 0:
                                                                $status = 'Ghế trống';
                                                            case 1:
                                                                $status = 'Ghế trống';
                                                            case 5:
                                                                $status = 'Ghế trống';
                                                            case 6:
                                                                $status = 'Ghế trống';
                                                                break;
                                                            case 3:
                                                                $status = 'Đã thanh toán';
                                                                break;

                                                            case 4:
                                                                $status = 'Đã lên xe';

                                                                break;
                                                            case 2:
                                                                if ($seat['overTime'] == 0 || $seat['overTime'] > (intval(microtime(true)) * 1000)) {
                                                                    $status = 'Giữ chỗ';
                                                                } else {
                                                                    $status = 'Hết hạn giữ chỗ';
                                                                }
                                                                break;
                                                            case 7:
                                                                $status = 'Ưu tiên giữ chỗ';
                                                                break;
                                                            default:
                                                                $status = 'Ghế trống';
                                                                break;
                                                        }
                                                        if ($seat['listTicketId'] && count($seat['listTicketId']) > 0 && ($seat['overTime'] == 0 || $seat['overTime'] > (intval(microtime(true)) * 1000) || $seat['seatStatus'] == 3)) {
                                                            if (isset($seat['ticketInfo'][$seat['listTicketId'][0]]['alongTheWayAddress']) && $seat['ticketInfo'][$seat['listTicketId'][0]]['alongTheWayAddress'] != null)
                                                                $pickUp = $seat['ticketInfo'][$seat['listTicketId'][0]]['alongTheWayAddress'];
                                                            else
                                                                $pickUp = '';
                                                            if (isset($seat['ticketInfo'][$seat['listTicketId'][0]]['dropAlongTheWayAddress']) && ($seat['ticketInfo'][$seat['listTicketId'][0]]['dropAlongTheWayAddress']) != null)
                                                                $dropOff = ($seat['ticketInfo'][$seat['listTicketId'][0]]['dropAlongTheWayAddress']);
                                                            else
                                                                $dropOff = '';

                                                            // console.log(pickUp.charAt(5));
                                                            // console.log(drofOff.charAt(5));

//                                                            for ($j = 0; $j < 100; $j++) {
//                                                                if (isset($pickUp[$j]) && $pickUp[$j] == ',') $dem++;
//                                                                if ($dem >= 2 || !isset($pickUp[$j])) {
//                                                                    $pickUp = substr($pickUp, 0, $j);
//                                                                    $j = 100;
//                                                                }
//                                                            }
//                                                            $dem = 0;
//                                                            for ($j = 0; $j < 100; $j++) {
//                                                                if (isset($dropOff[$j]) && $dropOff[$j] == ',') $dem++;
//                                                                if ($dem >= 2 || !isset($dropOff[$j])) {
//                                                                    $dropOff = substr($dropOff, 0, $j);
//                                                                    $j = 100;
//                                                                }
//                                                            }
                                                            echo '</h4></div>';
                                                            echo '</br></br></br><span style="float: left;"> ' . ($seat['ticketInfo'][$seat['listTicketId'][0]]['fullName']) . '</span>';
                                                            echo ' - <strong>' . $seat['ticketInfo'][$seat['listTicketId'][0]]['phoneNumber'] . '</strong>';
                                                            echo '</br><div><span style="width: 100%">'+ $pickUp+' </span> ' ;
                                                            if ($seat['ticketInfo'][$seat['listTicketId'][0]]['paymentTicketPrice'])
                                                                echo '</br><h5 style="float: right;width: 100%"><strong>' .
                                                                    number_format(($seat['ticketInfo'][$seat['listTicketId'][0]]['paymentTicketPrice']), 0, ',', '.') . ' VNĐ</strong></h5>';
                                                            else ;
                                                            echo '</br><span style="width: 100%">'+$dropOff+'</span> </div>';


                                                        } else {
                                                            echo '</strong></h3></div>';
                                                        }
                                                    }
                                                    $seat = null;

                                                    echo '</td>';

                                                }
                                                echo '</tr>';
                                            }
                                            echo '</tbody></table></div>';

                                        } else {?>
                                        <div id="table-map" style="display: none">
                                            <table class="table table-condensed">
                                                <caption>Tầng 1</caption>
                                                <thead>
                                                <tr>
                                                    @for($column = 1;$column<=$result['seatMap']['numberOfColumns'];$column++)
                                                        <td width="{{$width}}%"></td>
                                                    @endfor
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @for($row = 1;$row<=$result['seatMap']['numberOfRows'];$row++)
                                                    <tr>
                                                        @for($column = 1;$column<=$result['seatMap']['numberOfColumns'];$column++)
                                                            @php($flat=false)
                                                            @foreach($result['seatMap']['seatList'] as $seat)
                                                                @if($seat['row']==$row && $seat['column']==$column && $seat["floor"]==1)
                                                                    @if( $seat['seatType'] == 1)
                                                                        <td>
                                                                            <strong>Cửa</strong><br>

                                                                        </td>
                                                                    @elseif( $seat['seatType'] == 2)
                                                                        <td>
                                                                            <strong>Ghế tài xế</strong><br>
                                                                            Tên tài xế

                                                                        </td>
                                                                    @elseif( $seat['seatType'] == 3 || $seat['seatType'] == 4)
                                                                        @if(($seat['seatStatus']==2 && $seat['overTime']/1000 > time())|| $seat['seatStatus']==3 || $seat['seatStatus']==4)
                                                                            @php
                                                                                $ticketId = head($seat['listTicketId']);
                                                                                @$user = head(array_where($listGuestPerPoint,function ($item) use ($ticketId){
                                                                                    return $item['ticketId'] == $ticketId;
                                                                                }));
                                                                            @endphp
                                                                            <td>
                                                                                <strong>Ghế {{$seat['seatId']}}:
                                                                                    @checkTicketStatus(array_get(@$user,'ticketStatus',''))</strong><br>
                                                                                {{array_get(@$user,'fullName','')}}<br>
                                                                                {{array_get(@$user,'phoneNumber','')}}
                                                                                <br>
                                                                                Điểm
                                                                                lên: {{array_get(@$user,'pickUpAddress','')}}
                                                                                <br>
                                                                                Điểm
                                                                                xuống: {{array_get(@$user,'dropOffAddress','')}}
                                                                                <br>
                                                                                Mã
                                                                                vé: {{array_get(@$user,'ticketCode','')}}
                                                                                <br>

                                                                            </td>
                                                                        @elseif($seat['seatStatus']==2 && $seat['overTime']/1000 < time())
                                                                            <td>
                                                                                <strong>Ghế {{$seat['seatId']}}: Hết hạn
                                                                                    giữ
                                                                                    chỗ</strong><br>
                                                                                {{--{{array_get(@$user,'fullName','')}}<br>--}}
                                                                                {{--{{array_get(@$user,'phoneNumber','')}}<br>--}}
                                                                                {{--Mã vé: {{array_get(@$user,'ticketCode','')}}<br>--}}
                                                                            </td>
                                                                        @else
                                                                            <td>
                                                                                <strong>Ghế {{$seat['seatId']}}:
                                                                                    @checkTicketStatus($seat['seatStatus'])</strong>
                                                                                <strong></strong><br>
                                                                                <br>
                                                                            </td>
                                                                        @endif
                                                                    @elseif( $seat['seatType'] == 5)
                                                                        <td>
                                                                            <strong>Nhà Vệ Sinh</strong><br>
                                                                            <strong></strong><br>
                                                                            Tên tài xế
                                                                        </td>

                                                                    @elseif( $seat['seatType'] == 6)
                                                                        <td>
                                                                            <strong>Ghế phụ xe</strong><br>
                                                                            <strong></strong><br>
                                                                            Tên phụ xe
                                                                        </td>

                                                                    @endif
                                                                    @php($flat=true)
                                                                @endif
                                                            @endforeach
                                                            @if($flat!=true)
                                                                <td>
                                                                    <strong></strong><br>
                                                                    <strong></strong><br>
                                                                </td>
                                                            @endif
                                                        @endfor
                                                    </tr>
                                                @endfor
                                                </tbody>
                                            </table>
                                            @if(count($floor2)>0)
                                                <table class="table table-condensed">
                                                    <caption>Tầng 2</caption>
                                                    <thead>
                                                    <tr>
                                                        @for($column = 1;$column<=$result['seatMap']['numberOfColumns'];$column++)
                                                            <td width="{{$width}}%"></td>
                                                        @endfor
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @for($row = 1;$row<=$result['seatMap']['numberOfRows'];$row++)
                                                        <tr>
                                                            @for($column = 1;$column<=$result['seatMap']['numberOfColumns'];$column++)
                                                                @php($flat=false)
                                                                @foreach($result['seatMap']['seatList'] as $seat)
                                                                    @if($seat['row']==$row && $seat['column']==$column && $seat["floor"]==2)

                                                                        @if( $seat['seatType'] == 1)
                                                                            <td>
                                                                                <strong>Cửa</strong><br>

                                                                            </td>
                                                                        @elseif( $seat['seatType'] == 2)
                                                                            <td>
                                                                                <strong>Ghế tài xế</strong><br>
                                                                                Tên tài xế

                                                                            </td>
                                                                        @elseif( $seat['seatType'] == 3 || $seat['seatType'] == 4)
                                                                            @if(($seat['seatStatus']==2 && ($seat['overTime']/1000 > time() || $seat['overTime'] == 0))|| $seat['seatStatus']==3 || $seat['seatStatus']==4)
                                                                                @php
                                                                                    $ticketId = head($seat['listTicketId']);
                                                                                    @$user = head(array_where($listGuestPerPoint,function ($item) use ($ticketId){
                                                                                        return $item['ticketId'] == $ticketId;
                                                                                    }));
                                                                                @endphp
                                                                                <td>
                                                                                    <strong>Ghế {{$seat['seatId']}}:
                                                                                        @checkTicketStatus(array_get(@$user,'ticketStatus',''))</strong><br>
                                                                                    {{array_get(@$user,'fullName','')}}
                                                                                    <br>
                                                                                    {{array_get(@$user,'phoneNumber','')}}
                                                                                    <br>
                                                                                    Mã
                                                                                    vé: {{array_get(@$user,'ticketCode','')}}
                                                                                    <br>

                                                                                </td>
                                                                            @elseif($seat['seatStatus']==2 && ($seat['overTime']/1000 < time() || $seat['overTime'] != 0))
                                                                                <td>
                                                                                    <strong>Ghế {{$seat['seatId']}}: Hết
                                                                                        hạn
                                                                                        giữ chỗ</strong><br>
                                                                                    {{--{{array_get(@$user,'fullName','')}}<br>--}}
                                                                                    {{--{{array_get(@$user,'phoneNumber','')}}<br>--}}
                                                                                    {{--Mã vé: {{array_get(@$user,'ticketCode','')}}<br>--}}
                                                                                </td>
                                                                            @else
                                                                                <td>
                                                                                    <strong>Ghế {{$seat['seatId']}}:
                                                                                        @checkTicketStatus($seat['seatStatus'])</strong>
                                                                                    <strong></strong><br>
                                                                                    <br>
                                                                                </td>
                                                                            @endif
                                                                        @elseif( $seat['seatType'] == 5)
                                                                            <td>
                                                                                <strong>Nhà Vệ Sinh</strong><br>
                                                                                <strong></strong><br>
                                                                                Tên tài xế
                                                                            </td>

                                                                        @elseif( $seat['seatType'] == 6)
                                                                            <td>
                                                                                <strong>Ghế phụ xe</strong><br>
                                                                                <strong></strong><br>
                                                                                Tên phụ xe
                                                                            </td>

                                                                        @endif
                                                                        @php($flat=true)
                                                                    @endif
                                                                @endforeach
                                                                @if($flat!=true)
                                                                    <td>
                                                                        <strong></strong><br>
                                                                        <strong></strong><br>
                                                                    </td>
                                                                @endif
                                                            @endfor
                                                        </tr>
                                                    @endfor
                                                    </tbody>
                                                </table>
                                            @endif
                                        </div>
                                        <?php } ?>

                                        <table class="table table-condensed" id="tb_report">
                                            <thead>
                                            <tr>
                                                <td colspan="8" style="border-bottom: 0px;">
                                                    <h3 align="center" id="title-table">DANH SÁCH HÀNH KHÁCH</h3>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8"
                                                    style="font-size: 15px;font-weight: bold; border-bottom:0px;">
                                                    Chuyến: {{@$result['routeName']}} (@timeFormat($time)) - Ngày:
                                                    @dateFormat($time*1000)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8"
                                                    style="font-size: 15px;font-weight: bold; border-bottom:0px;">
                                                    Biển số xe: {{@$result['numberPlate']}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8"
                                                    style="font-size: 13px;font-weight: bold; border-bottom:0px;">Tên
                                                    lái
                                                    xe: {{@implode(',',@$listDriverName)}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8"
                                                    style="font-size: 13px;font-weight: bold; border-bottom:0px;">
                                                    Tên phụ xe: {{@implode(',',@$listAssistantName) }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8"></td>
                                            </tr>
                                            <tr style="font-weight:bold;">
                                                <th>Mã vé</th>
                                                <th>Số ghế</th>
                                                <th>DS ghế</th>
                                                <th>Tên khách hàng</th>
                                                <th>Số điện thoại</th>
                                                <th>Bến lên</th>
                                                <th>Bến xuống</th>
                                                <th>Đại lý</th>
                                                <th>Trạng thái vé</th>
                                                <th>Ghi chú</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php($stt = 1)
                                            @forelse($listGuestPerPoint as $guest)
                                                @if(($guest['ticketStatus']==2 && ($guest['overTime']/1000 > time() || $guest['overTime'] == 0)) || $guest['ticketStatus']==3 || $guest['ticketStatus']==7)
                                                    <tr>
                                                        <td>{{$guest['ticketCode']}}</td>
                                                        <td>{{count($guest['listSeatId'])}}</td>
                                                        <td>{{implode(", ",$guest['listSeatId'])}}</td>
                                                        <td>{{$guest['fullName']}}</td>
                                                        <td>{{$guest['phoneNumber']}}</td>
                                                        <td>{{$guest['pickUpAddress']}}</td>
                                                        <td>{{$guest['dropOffAddress']}}</td>
                                                        <td>{{$guest['sourceName']}}</td>
                                                        <td>@checkTicketStatus($guest['ticketStatus'])</td>
                                                        <td>{{$guest['note']}}</td>
                                                    </tr>
                                                @endif
                                            @empty
                                                <tr>
                                                    <td class="center" colspan="8">Hiện không có dữ liệu</td>
                                                </tr>
                                            @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="Lenhvanchuyen">
                                <div class="print_content row-fluid">
                                    <div style="padding-top:20px;">
                                        <div class="span1"><a class="btn" onclick="printDiv('printArea')"><img
                                                        src="/public/images/printer.png"/></a></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr class="separator">

                                    <div id="printArea">

                                        <table border="0" style="width:100%">
                                            <tbody>
                                            <tr>
                                                <td style="width:123px"><p style="text-align:center"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px"><strong>Tên đơn vị:........................................ </strong></span></span></span>
                                                    </p>
                                                    <p style="text-align:center"><span style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px"><strong>Điện thoại:........................................</strong></span></span></span>
                                                    </p>
                                                    <p style="text-align:center"><span style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px"><strong>Số:...................................................</strong></span></span></span>
                                                    </p></td>
                                                <td style="width:367px"><p style="text-align:center"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px"><strong>CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM<br>
                  Độc lập - Tự do - Hạnh phúc<br>
                  ---------------</strong></span></span></span></p></td>
                                            </tr>
                                            <tr>
                                                <td style="width:123px"><p style="text-align:center">&nbsp;</p></td>
                                                <td style="width:367px"><p style="text-align: right;"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px"><em>…………, ngày...... tháng......năm.........</em></span></span></span>
                                                    </p></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <p style="text-align:center"><span style="color:#000000"><span
                                                        style="font-family:arial,helvetica,sans-serif"><span
                                                            style="font-size:12px"><strong>LỆNH VẬN CHUYỂN</strong></span></span></span>
                                        </p>
                                        <p style="text-align:center"><span style="color:#000000"><span
                                                        style="font-family:arial,helvetica,sans-serif"><span
                                                            style="font-size:12px"><strong>Dùng cho xe ô tô vận chuyển hành khách tuyến cố định</strong></span></span></span>
                                        </p>
                                        <p style="text-align:center"><span style="color:#000000"><span
                                                        style="font-family:arial,helvetica,sans-serif"><span
                                                            style="font-size:12px">Có giá trị từ ngày ………………… đến ngày ……………………..</span></span></span>
                                        </p>

                                        <table class="table table-condensed table_print"
                                               style="border-left: 1px solid #ddd!important;">
                                            <tbody>
                                            <tr>
                                                <td colspan="4" rowspan="2" style="width:729px"><p
                                                            style="text-align:justify; padding-left:15px;"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">Cấp cho Lái xe 1:………………………...............………….............. hạng GPLX:…………….....</span></span></span>
                                                    </p>
                                                    <p style="text-align:justify;padding-left:29px;"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Lái xe 2:…………………………..........................…………hạng GPLX:…………….</span></span></span>
                                                    </p>
                                                    <p style="text-align:justify;padding-left:29px;"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nhân viên phục vụ trên xe:…………………………………........……….</span></span></span>
                                                    </p>
                                                    <p style="text-align:justify; padding-left:15px;"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">Biển số đăng ký:…….…….. ........Số ghế (gường nằm):……...... Loại xe:………......</span></span></span>
                                                    </p>
                                                    <p style="text-align:justify; padding-left:15px;"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">Bến đi, bến đến:…………………………………......……Mã số tuyến:………....….</span></span></span>
                                                    </p>
                                                    <p style="text-align:justify; padding-left:15px;"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">Hành trình tuyến:………………………………………………………….....</span></span></span>
                                                    </p></td>
                                                <td style="width:152px"><p style="text-align:center"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">Thủ trưởng đơn vị<br>
                  (Ký tên và đóng dấu)</span></span></span><br>
                                                        <br>
                                                        <br>
                                                    </p></td>
                                            </tr>
                                            <tr>
                                                <td style="width:132px"><p style="text-align:center"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">Cán bộ kiểm tra<br>
                  kiểm tra xe</span></span></span><br>
                                                        <br>
                                                        <br>
                                                    </p></td>
                                            </tr>
                                            <tr>
                                                <td style="width:132px"><p style="text-align:center"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">Lượt xe thực hiện<br>
                  </span></span></span></p></td>
                                                <td style="width:132px"><p style="text-align:center"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">Bến xe đi, đến </span></span></span>
                                                    </p></td>
                                                <td style="width:132px"><p style="text-align:center"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">Giờ xe chạy<br>
                  </span></span></span></p></td>
                                                <td style="width:132px"><p style="text-align:center"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">Số khách </span></span></span>
                                                    </p></td>
                                                <td style="width:132px"><p style="text-align:center"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">Bến xe<br>
                  (Ký tên và đóng dấu)</span></span></span></p><br><br></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" style="width:76px"><p style="text-align:center"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">Lượt đi</span></span></span>
                                                    </p></td>
                                                <td style="width:179px"><p style="text-align:justify"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">Bến xe đi:………………</span></span></span>
                                                    </p></td>
                                                <td style="width:170px"><p style="text-align:center"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">xuất bến<br>
                  ……. giờ ngày……..</span></span></span></p></td>
                                                <td style="width:104px"><p style="text-align:justify">&nbsp;</p></td>
                                                <td style="width:132px"><p style="text-align:justify">&nbsp;</p></td>
                                            </tr>
                                            <tr>
                                                <td style="width:179px"><p style="text-align:justify"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">Bến xe nơi đến: ………..</span></span></span>
                                                    </p></td>
                                                <td style="width:170px"><p style="text-align:center"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">đến bến<br>
                  ……. giờ ngày………</span></span></span></p></td>
                                                <td style="width:104px"><p style="text-align:justify">&nbsp;</p></td>
                                                <td style="width:132px"><p style="text-align:justify">&nbsp;</p></td>
                                            </tr>
                                            <tr>
                                                <td rowspan="2" style="width:76px"><p style="text-align:center"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">Lượt về</span></span></span>
                                                    </p></td>
                                                <td style="width:179px"><p style="text-align:justify"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">Bến xe đi:……………..</span></span></span>
                                                    </p></td>
                                                <td style="width:170px"><p style="text-align:center"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">xuất bến<br>
                  …. giờ ngày…….</span></span></span></p></td>
                                                <td style="width:104px"><p style="text-align:justify">&nbsp;</p></td>
                                                <td style="width:132px"><p style="text-align:justify">&nbsp;</p></td>
                                            </tr>
                                            <tr>
                                                <td style="width:179px"><p style="text-align:justify"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">Bến xe nơi đến: ………</span></span></span>
                                                    </p></td>
                                                <td style="width:170px"><p style="text-align:center"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">đến bến<br>
                  ……. giờ ngày………</span></span></span></p></td>
                                                <td style="width:104px"><p style="text-align:justify">&nbsp;</p></td>
                                                <td style="width:132px"><p style="text-align:justify">&nbsp;</p></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="width:236px"><p style="text-align:center"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">NHÂN VIÊN PHỤC VỤ TRÊN XE<br>
                  (Ký và ghi rõ họ tên)</span></span></span>
                                                        <br>
                                                        <br>
                                                        <br>
                                                    </p></td>
                                                <td colspan="2" style="width:170px">
                                                    <p style="text-align:center"><span style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">LÁI XE 1<br>
                  (Ký và ghi rõ họ tên)</span></span></span>
                                                        <br>
                                                        <br>
                                                        <br>
                                                    </p></td>
                                                <td colspan="2" style="width:170px"><p style="text-align:center"><span
                                                                style="color:#000000"><span
                                                                    style="font-family:arial,helvetica,sans-serif"><span
                                                                        style="font-size:12px">LÁI XE 2<br>
                  (Ký và ghi rõ họ tên)</span></span></span>
                                                        <br>
                                                        <br>
                                                        <br>
                                                    </p></td>

                                            </tr>
                                            </tbody>
                                        </table>
                                        <br/>
                                        <div style="line-height: 10px;text-align:justify;color:#000000;font-size: 12px">
                                            <strong>* Ghi chú:</strong>
                                            <ul>
                                                <li>Bến xe ghi vào ô ngày giờ đi đến, đóng dấu.</li>
                                                <li>Trên một tờ Lệnh vận chuyển chỉ được phép bố trí tối đa 4 lượt đi và
                                                    4
                                                    lượt về.
                                                </li>
                                                <li>Ngoài các nội dung nêu trên, đơn vị kinh doanh vận tải bổ sung các
                                                    nội
                                                    dung khác để
                                                    phục vụ
                                                    công
                                                    tác quản lý điều hành của đơn vị.
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div>
        </div>
        <script>
            // auto();
            $('#title-table').hide();
            $(document).ready(function () {
                $('#table-map_SaoNghe').hide();
                $('#tb_report').show();
            });
            $('#table-map_SaoNghe').hide();

            function printDiv(id) {
                var printContents = $('#' + id).html();
                var originalContents = $('body').html();
                $('body').html(printContents);
                window.print();
                $('body').html(originalContents);
                $('#table-map_SaoNghe').hide();
                $('#tb_report').show();


            }

            function auto() {
                var printContents = $('#printArea').html();
                var printContentsMap = $('#printMap').html();
                var originalContents = $('section').html();
                $('section').html(printContents).append('<br>').append(printContentsMap);
                window.print();
                //$('section').html(originalContents);

            }

            if ($(that).prop('id') == 'cbList') {
                $('#table-map').hide();
                $('#table-map_SaoNghe').hide();
                $('#tb_report').show();
                $('#title-table').hide();
            }

            function showTable(that) {
                if ($(that).prop('id') == 'cbMap') {
                    if (<?php if (session('companyId') == 'TC03013FPyeySDW') echo 'true'; else echo 'false';?>) {
                        $('#tb_report').hide();
                        $('#table-map_SaoNghe').show();
                        $('#table-map').hide();


                    }
                    else {
                        $('#table-map_SaoNghe').hide();
                        $('#table-map').show();
                        $('#tb_report').hide();
                        $('#title-table').show();
                    }
                }
                if ($(that).prop('id') == 'cbList') {
                    $('#table-map').hide();
                    $('#table-map_SaoNghe').hide();
                    $('#tb_report').show();
                    $('#title-table').hide();
                }
            }
        </script>

@endsection