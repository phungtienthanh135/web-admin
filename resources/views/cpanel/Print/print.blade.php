
<div id="template_report" style="display: none">
    <div style="display: flex;margin-bottom: 10px">
        <div style="width: 15%;height: 15%"><img src="{{array_get(session('companyInfo'),'companyLogo')}}" alt=""></div>
        <div style="margin-left: 20px">
            <h4>{{array_get(session('companyInfo'),'companyName')}}</h4>
            <h6>Địa chỉ: {{array_get(session('companyInfo'),'address')}}</h6>
            <h6>Điện thoại: {{array_get(session('companyInfo'),'userContact.phoneNumber')}} </h6>
            <h6>Website: https://{{array_get(session('companyInfo'),'userContact.userName')}}.nhaxe.vn</h6>
        </div>
    </div>
    <div style="text-align: center">
        <h3 id="template_report_title">BÁO CÁO DOANH THU BÁN VÉ</h3>
        <h6>Từ ngày: {{request('startDate',date('d/m/Y',time()))}} Đến ngày: {{request('endDate',date('d/m/Y',time()))}}</h6>
    </div>
    <div>
        <h6 id="template_report_route" style="float: left">Tuyến:...............</h6>
        <h6 id="template_report_source" style="float: right">Đại lý:...............</h6>
    </div>
    <div>
        <h6 id="template_report_vehicle" style="clear: right;float: left">Xe:...............</h6>
    </div>
    <div class="row-fluid" id="template_report_content">

    </div>
    <div style="margin-top: 20px">
        <h6 style="float: left;margin-left: 20%"></h6>
        <div style="text-align: center;float: right;margin-right: 20%">
            <h6><i>...........,Ngày....Tháng....Năm......</i></h6>
            <h6>Người lập</h6>
        </div>
    </div>
</div>