<!doctype html>

<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>{{@$result['ticketCode']}}</title>
    <style>
        @page {
            margin: 0;
        }
    </style>
</head>

<body>

<a href="" onclick="printDiv()"><img src="{{session('userLogin')['logo']}}"/></a>
<div id="in">
    <div style="width: 173px;height: 390px;font-size: 11px;line-height: 15px;
    font-family: sans-serif;font-weight: 100;margin-left: -8px;">
        <div style="text-align: center;margin: 0 10px 10px 0">
            <div>------------------------</div>
            <img src="{{session('userLogin')['logo']}}" width="118px" height="51px">
            <div>VÉ ĐẶT CHỖ</div>
            <div style="margin-bottom: 10px;text-transform: uppercase">XE {{@$result['companyName']}}</div>
            <div>{!! $barcode !!}</div>
            <div style="letter-spacing:7px;margin-top: -4px">{{@$result['ticketCode']}}</div>
        </div>
        <div id="ticketInfo">

        </div>
        {{--<div>Tuyến: {{@$result['routeName']}}</div>--}}
        {{--<div>Hành khách: {{$result['fullName']}} ({{$result['phoneNumber']}})</div>--}}

        {{--@if(session('companyId') == 'TC03013FPyeySDW')--}}
        {{--<div>Giường số: {{@implode(',',$result['listSeatId'])}}</div>--}}
        {{--@else--}}
        {{--<div>Chổ ngồi: {{@implode(',',$result['listSeatId'])}}</div>--}}
        {{--@endif--}}

        {{--<div>Giờ đi: {{date('g:i A - d/m/Y',@$result['getInTimePlan']/1000)}}</div>--}}
        {{--<div>Giá vé: @moneyFormat(@$result['paymentTicketPrice'])</div>--}}
        {{--<div>Bến lên: {{@$result['getInPoint']['pointName']}}</div>--}}
        {{--<div>Bến xuống: {{@$result['getOffPoint']['pointName']}}</div>--}}
        {{--<div>Biển số xe: {{@$result['numberPlate']}}</div>--}}
        {{--<div style="margin-top: 20px">Quý khách vui lòng có mặt trước 30 phút</div>--}}
        {{--<div>Cảm ơn Quý khách đã sử dụng xe {{@$result['companyName']}}! (Chi tiết liên hệ: {{session('userLogin')['telecomPhoneNumber']}})</div>--}}
    </div>
</div>
</body>
</html>
<script>
        var listSetting ={!! json_encode($listSetting) !!};
        var info =
                {!! json_encode($result) !!}
        var html = '';

        $.each(listSetting, function (k, v) {
            html += '<div>' + v.alias + ' :<label style="' + generrateStyle(v) + '"> ';
            if (v.attribute == 'routeName') html += info.routeName;
            if (v.attribute == 'customer') html += (info.fullName != undefined ? info.fullName : '') + '-' + (info.phoneNumber != undefined ? info.phoneNumber : '');
            if (v.attribute == 'listSeatId') html += (info.listSeatId).join();
            if (v.attribute == 'startTime') html += formatDate(info.getInTimePlan);
            if (v.attribute == 'priceTicket') html += number(info.paymentTicketPrice) + 'VNĐ';
            if (v.attribute == 'pickUp') html += number(info.getInPoint.pointName);
            if (v.attribute == 'dropDown') html += number(info.getOffPoint.pointName);
            if (v.attribute == 'numberPlate') html += number(info.numberPlate);
            if (v.attribute == 'note') html += 'Quý khách vui lòng có mặt trước 30 phút+' +
                'Cảm ơn Quý khách đã sử dụng dịch vụ! (Chi tiết liên hệ: '{{session('userLogin')['telecomPhoneNumber']}};
            html += '<label></div>';
        });

        $('#ticketInfo').html(html);

    function generrateStyle(v) {
        var html = '';
        if (v.bold == 1) html += 'font:bold;';
        html += 'font-size:' + (v.size > 0 ? v.size : 12) + 'px;';
        html += 'font-family:';
        if (v.fontfamily == 1) html += ' Times New Roman, Times, serif';
        if (v.fontfamily == 2) html += ' Arial, Helvetica, sans-serif';
        if (v.fontfamily == 3) html += 'Arial Black, Gadget, sans-serif ';
        if (v.fontfamily == 4) html += 'Comic Sans MS, cursive, sans-serif ';
        if (v.fontfamily == 5) html += 'Impact, Charcoal, sans-serif';
        if (v.fontfamily == 6) html += 'Tahoma, Geneva, sans-serif ';
        if (v.fontfamily == 7) html += 'Verdana, Geneva, sans-serif';
        return html;
    }

    // $(function () {
    //     printDiv();
    //
    //     function printDiv() {
    //         var printContents = document.getElementById('in').innerHTML;
    //         var originalContents = document.body.innerHTML;
    //         //$('#template_report_content').html(printContents);
    //
    //         document.body.innerHTML = printContents;
    //
    //         window.print();
    //         // document.body.innerHTML = sessionStorage.getItem('oldPage');
    //         document.body.innerHTML = originalContents;
    //     }
    // })

</script>