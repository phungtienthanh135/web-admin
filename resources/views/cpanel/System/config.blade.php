@extends('cpanel.template.layout')
@section('title', 'Cấu hình hệ thống')
@section('content')

    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Cấu hình hệ thống</h3></div>
            </div>
        </div>
        <div class="separator bottom"></div>
        <div class="innerLR">
            <form action="" method="post">
                <div class="row-fluid">
                    <div class="pull-left span8">
                        <h4>Cấu hình Bán vé</h4>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span5">
                        <div class="control-group">
                            <label for="txtTenWebsite">Địa chỉ công ty</label>
                            <input autocomplete="off" class="span12" type="text" name="address" id="txtTenWebsite"
                                   value="{{@$result['address']}}">
                        </div>

                        <div class="control-group">
                            <label for="txtTGGiuaCho">Thời gian giữ chỗ (phút)</label>
                            <input class="span12" type="text" name="timeBookHolder" id="txtTGGiuaCho"
                                   value="{{@$result['timeBookHolder']}}">
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="txtThoiDiemHuyVe">Thời điểm kết thúc bán vé (phút)</label>
                            <input class="span12" type="text" name="sellingTicketEndTime" id="txtThoiDiemHuyVe"
                                   value="{{@$result['sellingTicketEndTime']}}">
                        </div>

                        <div class="control-group">
                            <label for="txtThoiGianGuiTinNhan">Cho phép đặt vé quá khứ</label>
                            <input class="span12" type="text" name="pastDaysToSellTicket" id="pastDaysToSellTicket" value="{{@$result['pastDaysToSellTicket']}}">
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span5" style="margin-left:0px;">
                        <div class="control-group">
                            <input type="checkbox" id="isEditTicketPrice" {{(@$result['isEditTicketPrice']) ? 'checked' : ''}} name="isEditTicketPrice">
                            <label for="isEditTicketPrice">Cho phép sửa giá tổng tiền bán vé</label>
                        </div>
                    </div>
                    <div class="span5">
                        <div class="control-group">
                            <input type="checkbox" id="allowAutoStartTrip" {{isset($result['allowAutoStartTrip']) && $result['allowAutoStartTrip']==true? 'checked' : ''}} name="allowAutoStartTrip">
                            <label for="allowAutoStartTrip">Xuất bến tự động</label>
                        </div>
                    </div>
                </div>
                <div class="row-fluid showAllowAutoStartTrip" style="display: none;">
                    <div class="span5"></div>
                    <div class="span5">
                        <div class="control-group">
                            <label for="allowAutoStartTrip">Số phút ( Âm là xuất bến trước giờ chạy)</label>
                            <input type="text" placeholder="Số phút" id="timeRangeAutoStartTrip" name="timeRangeAutoStartTrip" value="{{isset($result['timeRangeAutoStartTrip'])? $result['timeRangeAutoStartTrip'] : ''}}">
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span5">
                        <div class="control-group">
                            <label for="txtFutureDaysToSellTicket">Cho phép bán về tương lai (số ngày)</label>
                            <input class="span12" type="number" name="futureDaysToSellTicket" id="txtFutureDaysToSellTicket"
                                   value="{{@$result['futureDaysToSellTicket']}}">
                        </div>
                    </div>
                    <div class="span4">
                        <label>&nbsp;</label>
                        <div class="control-group">
                            <input type="checkbox" class="span12" id="isSellTicketWithoutSeat" {{(@$result['isSellTicketWithoutSeat']) ? 'checked' : ''}} name="isSellTicketWithoutSeat">
                            <label for="isSellTicketWithoutSeat">Cho phép đặt dư ghế</label>
                        </div>
                    </div>
                </div>

                <hr>
                <div class="row-fluid">
                    <div class="pull-left span8">
                        <h4>Cấu hình Tổng đài</h4>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3">
                        <div class="control-group">
                            <label for="telecomCompanyId">Nhà cung cấp</label>
                            <select name="telecomCompanyId" id="telecomCompanyId">
                                <option value="">Không có</option>
                                @foreach($telecomCompany as $telCom)
                                    <option @if(isset($result['telecomCompanyId'])&&$telCom['telecomCompanyId']== $result['telecomCompanyId']) selected @endif value="{{ $telCom['telecomCompanyId'] }}">{{ $telCom['telecomCompanyName'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="span9">
                        <label for="">Chuỗi cấu hình</label>
                        <textarea name="telecomConfig" class="form-control" style="width: 100%" id="" rows="3">@if(isset($result['telecomConfig'])){{$result['telecomConfig']}}@endif</textarea>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3">
                        <div class="control-group">
                            <label for="txtTelecomPhoneNumber">Số tổng đài</label>
                            <input autocomplete="off" class="span12" type="text" name="telecomPhoneNumber" id="txtTelecomPhoneNumber"
                                   value="{{@$result['telecomPhoneNumber']}}">
                        </div>
                    </div>
                </div>

                <hr>
                <div class="row-fluid">
                    <div class="pull-left span8">
                        <h4>Cấu hình gửi thông báo</h4>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span5">
                        <div class="control-group">
                            <input type="checkbox" id="sendMail" {{(@$result['sendMail']) ? 'checked' : ''}} name="sendMail">
                            <label for="sendMail">Gửi qua mail</label>
                        </div>
                        <div class="control-group">
                            <label class="span10" for="email">Email</label> <a href="#" id="addEmail">Thêm email</a>
                            {{--<input autocomplete="off" class="span12" type="email" name="email" id="email"
                                   value="{{@$result['userContact']['email']}}">--}}

                            <div id="emailArea">
                                @for($i = 0; $i < count($emails); $i++)
                                    @if($i==0)
                                        <input autocomplete="off" class="span10" type="email" value="{{$emails[$i]}}" name="email[]">
                                    @else
                                        <div>
                                            <input autocomplete="off" class="span10" type="email" value="{{$emails[$i]}}" name="email[]">
                                            <a class="span2 removeEmail" href="#" style="float: right">Xóa</a>
                                        </div>
                                    @endif
                                @endfor
                            </div>
                        </div>
                    </div>
                    <div class="span5">
                        <div class="control-group">
                            <input type="checkbox" id="sendSms" {{(@$result['sendSms']) ? 'checked' : ''}} name="sendSms">
                            <label for="sendSms">Gửi qua SMS</label>
                        </div>
                        <div class="control-group">
                            <label for="phoneNumber">Số điện thoại</label>
                            <input autocomplete="off"
                                   data-validation="custom"
                                   data-validation-regexp="^0(([8-9][0-9]{8})|(1[0-9]{9}))$"
                                   data-validation-error-msg-custom="Vui lòng nhập đúng định dạng SĐT"
                                   id="phoneNumber" class="m_bottom_0r" type="text" value="{{@$result['userContact']['phoneNumber']}}" name="phoneNumber" />
                        </div>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span4">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button class="btn btn-warning btn-flat-full">LƯU</button>
                    </div>

                    <div class="span2">
                        <a href="{{action('SystemController@info')}}" class="btn btn-default btn-flat">HỦY</a>
                    </div>
                </div>
                <input type="hidden" name="img" value="">
            </form>
        </div>
    </div>
    <script>
        if($('#allowAutoStartTrip').prop('checked')==true){
            $('.showAllowAutoStartTrip').show();
        }
        $(document).ready(function () {
            $('#allowAutoStartTrip').on('change',function () {
                if($('#allowAutoStartTrip').prop('checked')==true){
                    $('.showAllowAutoStartTrip').show();
                }else{
                    $('.showAllowAutoStartTrip').hide();
                }
            });
            $('#addEmail').click(function () {
                var fName = $("<div><input type=\"email\" name=\"email[]\" autocomplete=\"off\" class=\"span10\"/><a class=\"span2 removeEmail\" href=\"#\" style=\"float: right\">Xóa</a></div>");

                $('#emailArea').append(fName);

                return false;
            });
            $(document).on('click', '.removeEmail', function () {
                $(this).parent().remove();
                return false;
            });
        });
    </script>

    <!-- End Content -->

@endsection