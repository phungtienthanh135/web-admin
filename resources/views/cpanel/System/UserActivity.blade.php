
@extends('cpanel.template.layout')
@section('title', 'Hoạt động của nhân viên')
@section('content')

    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Hoạt động của nhân viên</h3>
                </div>
            </div>
        </div>
        <div class="innerLR">
            <div class="row-fluid">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Tìm Hoạt động</h4>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">
                            <label for="txtTenNV">Tên nhân viên</label>
                            <select class="input-xlarge" name="userName" id="txtTenNV">
                                <option value="">Chọn tên nhân viên</option>
                                <option value=""></option>
                            </select>
                        </div>

                        <div class="span4">
                            <label for="txtLoaiHD">Loại nhân viên</label>
                            <select class="input-xlarge" name="userType" id="txtLoaiHD">
                                <option value="">Chọn loại nhân viên</option>
                                <option value="2">Tài xế</option>
                                <option value="3">Phụ xe</option>
                                <option value="4">Kế toán</option>
                                <option value="5">Hành chánh</option>
                                <option value="6">Thanh tra</option>
                                <option value="7">Quản trị</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">
                            <label for="txtTuNgay">Từ ngày</label>
                            <div class="input-append">
                                <input autocomplete="off" value="14/07/2017" class="input-xlarge" id="txtStartDate" type="text">

                            </div>
                        </div>

                        <div class="span4">
                            <label for="txtDenNgay">Đến ngày</label>
                            <div class="input-append">
                                <input autocomplete="off" value="14/07/2017" class="input-xlarge" id="txtEndDate" type="text">

                            </div>
                        </div>
                        <div class="span3">
                            <label class="separator hidden-phone" for="add"></label>
                            <button class="btn btn-info btn-flat-full hidden-phone" id="search">TÌM HOẠT ĐỘNG</button>
                            <button class="btn btn-info btn-flat visible-phone" id="search">TÌM HOẠT ĐỘNG</button>
                        </div>

                    </div>

                </div>
            </div>
            <div class="row-fluid">
                Danh sách hoạt động nhân viên
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Thời gian</th>
                        <th>Tên nhân viên</th>
                        <th>Đối tượng</th>
                        <th>Hoạt động</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>HD122</td>
                        <td>Hà Nội - Thanh Hoá</td>
                        <td>Chỉnh sửa bến đi</td>
                        <td>ahii</td>
                    </tr>
                    <tr>
                        <td>HD122</td>
                        <td>Hà Nội - Thanh Hoá</td>
                        <td>Bán 20 vé cho tuyến</td>
                        <td>ahii</td>
                    </tr>
                    <tr>
                        <td>HD122</td>
                        <td>Hà Nội - Thanh Hoá</td>
                        <td>Thêm tuyến</td>
                        <td>ahii</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- End Content -->

@endsection