@extends('cpanel.template.layout')
@section('title', 'Cài đặt hiển thị phơi')
@section('content')
    <style>
        .attribute {
            float: left;
            width: 33%;
            padding-top: 50px
        }

        .textcolor {
            padding: 10px 0px 0px 35px;
            color: black
        }

        .margin-non {
            margin: 5px 0px 0px 0px !important;
        }

        .control-left {
            margin-left: 110px !important;
        }

        table,td,th {
            border: 1px solid;
            text-align: center;
        }

        .grip {
            width: 0;
            height: 0;
            margin-left: 9px;
            border-left: 5px solid transparent;
            border-right: 5px solid transparent;

            border-top: 10px solid #3a2d2d;
        }

        .divtable {
            height: 250px;
            margin-top: 20px
        }

        .widthauto {
            margin-top: 10px;
            width: auto;
        }

        #updateSetting {
            margin: 30px 0px 0px 34% !important;
            font-size: 14px !important;
        }

        .label-left {
            width: 28% !important;
        }
    </style>
    <div>
        <div style="height: 50px" class=" heading_top">
            <div class="row-fluid">
                <div class="pull-left span8" style="padding-left: 10px;">
                    <h3>Thông tin áp dụng</h3>
                </div>

            </div>
        </div>
        <div class="row-fluid heading_top" style="height: 150px;margin-top: 20px;">
            <div class="controls">
                @php $dem=1; @endphp
                @foreach ($result as $row)
                    <div class="span3" {{$dem==1?'style=margin-left:35px;':""}}>
                        <input id="{{$row['attribute']}}_check" type="checkbox" name="{{$row['attribute']}}_check"
                               data-idAttribute="{{$row['id']}}" data-id="{{$row['attribute']}}"
                               class="span12 check {{$row['attribute']}}_check"
                                {{$row['status']==1?'checked':''}}>
                        <label style="margin-top: 5px" class="f_left"
                               for="{{$row['attribute']}}_check">{{$row['name']}}</label>
                    </div>
                    @php $dem++; @endphp
                @endforeach
            </div>
        </div>
        <div class="divtable">
            <div class=" row-fluid">
                <table style="width:99%;font-family: 'Verdana'; font-size: 12px;margin: 0px 0px 0px 10px;"
                       id="setting"></table>
            </div>
        </div>


    </div>
    <div style=" margin-top: 50px;">
        <form action="{{action('settingListCustomerController@updateSetting')}}" method="get">
            <input type="hidden" class="sendListAttribute" name="listAttribute" value="">
            <label style="margin: 30px 0px 0px 36%;font-size: 14px" class="span4"></label>
            <button id="updateSetting" class="span4 btn btn-warning btn-flat">Cập nhật cài đặt
            </button>
        </form>
    </div>

    <script>

        $(document).ready(function () {
            $("#content").attr("style", "width:100%");
            $("#menu").attr("style", "margin-left:-400px");
            $("#ht_btn_close_open_menu").html('<button onClick="open_menu()" type="button" class="btn-navbar"> <div class="icon_open_menu"></div> </button>');
            $('form>label').text('( Tự động căn chỉnh theo mặc định khi không điền thông tin )');
            $(function () {
                $('table').colResizable({
                    liveDrag: true,
                    gripInnerHtml: "<div class='grip'></div>",
                    draggingClass: "dragging"
                });
            });

            var listAttribute ={!! json_encode($result) !!};
            var blankRow ={!! json_encode($blankRow) !!};
            var html = '<tr><th style="width: 90px;"></th>';
            var use = 0;
            var listOption = {
                0: "In đậm",
                1: "Cỡ chữ",
                // 2: "Chiều rộng",
                // 3: "Chiều cao",
                2: "Tên hiển thị",
                3: "idAttribute"
            };
            $.each(listAttribute, function (k, v) {
                if (v.status == 1) {
                    html += '<th style="width:' + (v.width > 0 ? (v.width + 'px') : 'auto') + '">' + v.name + '</th>';
                    use++;
                }
            });
            if (use != 0) {
                html += '</tr>';
                for (var i = 0; i < 3; i++) {
                    html += '<tr><td style="width: 90px;">' + listOption[i] + '</td>';
                    $.each(listAttribute, function (k, v) {
                        if (v.status == 1) {
                            if (i == 0) html += '<td style=" height:40px;width:' + (v.width > 0 ? (v.width + 'px') : 'auto') + '">' +
                                '<input ' + (v.bold == 1 ? 'checked' : '') + ' class="widthauto ' + v.attribute + '_check" id="bold_' + v.attribute + '" name="bold_' + v.attribute + '" type="checkbox">' +
                                '<label for="bold_' + v.attribute + '" style="margin: -10px 45%;"></label></td>';
                            else if (i == 1) html += '<td style="width:' + (v.width > 0 ? (v.width + 'px') : 'auto') + '"><input class="widthauto ' + v.attribute + '_check" type="number" value="' + (v.size > 0 ? v.size : '') + '"></td>';
                            else html += '<td style="width:' + (v.width > 0 ? (v.width + 'px') : 'auto') + '"><input class="widthauto ' + v.attribute + '_check" type="text" value="' + v.alias + '"></td>';
                        }
                    });
                    html += '</tr>';
                }
                html += '<tr><td>Số hàng trống</td>' +
                    '<td colspan="' + use + '" ><input id="inputForBlankRow" style="margin: 8px 0px 8px -86%;width: 100px;" type="number" value="' + blankRow.quantity + '"></td>' +
                    '</tr>';
                $('#setting').html(html);
            }
            $('input.check').click(function () {
                $('table').colResizable({disable: true});
                var html = '<tr><th></th>';
                $('.check:checked').each(function () {
                    var attr = ($(this).attr('id')).substring(0, ($(this).attr('id')).length - 6);
                    $.each(listAttribute, function (k, v) {
                        if (v.attribute == attr)
                            html += '<th style="width:' + (v.width > 0 ? (v.width + 'px') : 'auto') + '">' + v.name + '</th>';
                    });
                });
                html += '</tr>';
                for (var i = 0; i < 3; i++) {
                    html += '<tr><td style="width: 90px;">' + listOption[i] + '</td>';
                    for (var j = 0; j < $('.check:checked').length; j++) {
                        var dem = 0;
                        var html2 = '';
                        var attr = ($($('.check:checked')[j]).attr('id')).substring(0, ($($('.check:checked')[j]).attr('id')).length - 6);
                        $.each(listAttribute, function (k, v) {
                            if (v.attribute == attr && v.status == 1){
                                dem++;
                                if (i == 0) html2 += '<td style=" height:40px;width:' + (v.width > 0 ? (v.width + 'px') : 'auto') + '">' +
                                    '<input ' + (v.bold == 1 ? 'checked' : '') + ' class="widthauto ' + v.attribute + '_check" id="bold_' + v.attribute + '" name="bold_' + v.attribute + '" type="checkbox">' +
                                    '<label for="bold_' + v.attribute + '" style="margin: -10px 45%;"></label></td>';
                                else if (i == 1) html2 += '<td style="width:' + (v.width > 0 ? (v.width + 'px') : 'auto') + '"><input class="widthauto ' + v.attribute + '_check" type="number" value="' + (v.size > 0 ? v.size : 12) + '"></td>';
                                else html2 += '<td style="width:' + (v.width > 0 ? (v.width + 'px') : 'auto') + '"><input class="widthauto ' + v.attribute + '_check" type="text" value="' + v.alias + '"></td>';
                            }
                        });
                        if (dem == 0) {
                            var nameforcheckking = $($('.check:checked')[j]).data('id');
                            if (i == 0) html += '<td style=" height:40px;"><input class="widthauto ' + nameforcheckking + '_check" id="bold_' + nameforcheckking + '" name="bold_' + nameforcheckking + '" type="checkbox">' +
                                '<label for="bold_' + nameforcheckking + '" style="margin: -10px 45%;"></label></td>';
                            else if (i == 1) html += '<td><input class="widthauto ' + nameforcheckking + '_check" type="number"></td>';
                            else html += '<td><input class="widthauto ' + nameforcheckking + '_check" type="text"></td>';
                        } else {
                            html += html2;
                        }
                    }
                    html += '</tr>';
                }
                html += '<tr><td>Số hàng trống</td>' +
                    '<td colspan="' + $('.check:checked').length + '" ><input id="inputForBlankRow" style="margin: 8px 0px 8px -86%;width: 100px;" type="number" value="' + $('#inputForBlankRow').val()+ '"></td>' +
                    '</tr>';
                $('#setting').html(html);
                if (!$(this).is(':checked'))
                    if ($('.check:checked').length == 0) $('#setting').html('');
                    else $('#setting').html(html);
                else $('#setting').html(html);
                $('table').colResizable({
                    disable: false,
                    liveDrag: true,
                    gripInnerHtml: "<div class='grip'></div>",
                    draggingClass: "dragging"
                });
            });

            $('#updateSetting').click(function () {

                var listSubmit = [], list = [],
                    listSetting = {bold: 0, size: 0, width: 0, height: 0, alias: '', name: '', idAttribute: '',quantity:0};
                $('.check:checked').each(function () {
                    var dem = 1;
                    var attr = ($(this).attr('id')).substring(0, ($(this).attr('id')).length - 6);
                    listSetting.status = 1;
                    listSetting.idAttribute = $(this).data('idattribute');
                    listSetting.name = attr;
                    $('.' + attr + '_check').each(function () {
                        if (dem == 2) listSetting.bold = $(this).is(':checked') ? 1 : 0;
                        else if (dem == 3) listSetting.size = $(this).val();
                        else if (dem == 4) {
                            listSetting.alias = $(this).val();
                            listSetting.width = $($(this).parent('td')).width();
                        }
                        dem++;
                    });
                    list.push(JSON.parse(JSON.stringify(listSetting)));
                    listSubmit.push(JSON.parse(JSON.stringify(list)));
                    list = [];
                });
                listSetting.idAttribute=99;
                listSetting.quantity=$('#inputForBlankRow').val();
                list.push(JSON.parse(JSON.stringify(listSetting)));
                listSubmit.push(JSON.parse(JSON.stringify(list)));
                $('.sendListAttribute').val(JSON.stringify(listSubmit));
            });
        });
    </script>
@endsection
