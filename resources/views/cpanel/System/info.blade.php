
@extends('cpanel.template.layout')
@section('title', 'Thông tin hệ thống')
@section('content')

    <div id="content">
        <div class="separator bottom"></div>
        <div class="heading-buttons">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Thông tin hệ thống</h3></div>
            </div>
            <hr class="gachnho" /></div>
        <div class="bg_light p_10">
            <div class="widget-body">
                <form name="account_frm" id="account_frm" method="post">
                    <div class="separator bottom form-inline small">
                        <div class="margin-bottom-none">
                            <div class="row-fluid">
                                <div class="span9">
                                    <table class="tttk table_t_align_l" cellspacing="20" cellpadding="4">
                                        <tr>
                                                <td colspan="2"><h3 style="color: #0b5fc6">{{$result['companyName']}}</h3></td>
                                        </tr>
                                        <tr>
                                            <th>Tên giao dịch</th>
                                            <td>: {{$result['companyName']}}
                                                <a class="pull-right" href="{{action('SystemController@config')}}"><i
                                                            class="icon-pencil"></i></a></td>
                                        </tr>
                                        <tr>
                                            <th>Mã số thuế</th>
                                            <td>: {{@$result['taxId']}} </td>
                                        </tr>
                                        <tr>
                                            <th>Địa chỉ</th>
                                            <td>: {{@$result['address']}} </td>
                                        </tr>
                                        <tr>
                                            <th>Website</th>
                                            <td>: <a href="http://{{$result['userContact']['userName']}}.nhaxe.vn">www.{{$result['userContact']['userName']}}.nhaxe.vn</a> </td>
                                        </tr>
                                        <tr>
                                            <th>Đại diện pháp luật</th>
                                            <td>: {{$result['userContact']['fullName']}}</td>
                                        </tr>
                                        <tr>
                                            <th>Ngày cấp giấy phép</th>
                                            <td>: </td>
                                        </tr>
                                        <tr>
                                            <th>Ngày hoạt động</th>
                                            <td>: </td>
                                        </tr>
                                        <tr>
                                            <th>Số điện thoại</th>
                                            <td>: {{$result['userContact']['phoneNumber']}}</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>: {{$result['userContact']['email']}}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="span3">
                                    <div id="logo">
                                        <img src="{{$result['companyLogo']}}" onerror="this.src='/public/images/logoANVUI.jpg'"/>
                                    </div>
                                    <form>
                                        <input type="file" id="selectedFile" name="img" style="display: none;" />
                                        <input type="button" style="width:250px" value="CHỌN ẢNH" class="btncustom" onclick="document.getElementById('selectedFile').click();" />
                                        <input type="hidden" name="listImage" class="listImage"/>

                                    </form>
                                </div>
                            </div>
                            @if($result['companyStatus']==2)
                            <div class="row-fluid">
                                <div class="span9">
                                    <table class="tttk table_t_align_l" cellspacing="20" cellpadding="4">
                                        <tr>
                                            <td colspan="2"><h5 style="color: #0b5fc6">An Vui 2.0</h5></td>
                                        </tr>
                                        <tr>
                                            <th>Số hiệu hợp đồng</th>
                                            <td>: {{$result['companyId']}}</td>
                                        </tr>
                                        <tr>
                                            <th>TG bắt đầu hợp đồng</th>
                                            <td>: @dateFormat($result['contractStartTime']/1000)</td>
                                        </tr>
                                        <tr>
                                            <th>TG hết hợp đồng</th>
                                            <td>: @dateFormat($result['contractEndTime']/1000)</td>
                                        </tr>
                                        {{--<tr>--}}
                                            {{--<th><p class="separator"></p></th>--}}
                                            {{--<td class="pull-right"><img id="dong-moc" src="/public/images/moc.png" alt="" ></td>--}}
                                        {{--</tr>--}}
                                    </table>
                                </div>
                            </div>
                            @endif
                            <div class="row-fluid">
                                <div id="hotline-logo" class="span4">
                                    <h3 style="text-align: center;">HOTLINE: 1900 7034</h3><br>
                                    <h5 style="opacity: 1">CÔNG TY CỔ PHẦN CÔNG NGHỆ AN VUI</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- End Content -->
<script>
    $(document).ready(function () {
        $('body').on('change', '#selectedFile', function () {
            var that = this;
            var form = new FormData();
            form.append('img', $(that).prop('files')[0]);
            form.append('_token', '{{ csrf_token() }}');
            $($(that).parent().find('img')).attr('src', '/public/images/loading/loader_blue.gif');
            $.ajaxSetup({
                processData: false,
                contentType: false
            });
            $.post('/cpanel/system/upload-image',
                form,
                function (data) {
                    $($(that).parent().find('img')).attr('src', data.url);
                    $($(that).parent().find('.listImage')).val(data.url);
                    $.ajaxSetup({
                        processData: true,
                        contentType: true
                    });
                    updateImg(data.url);
                }).fail(function (data) {
            });
        });
        function  updateImg(url){
            $.ajax({
                url: '{{ action('SystemController@postImg') }}',
                type: "GET",
                data: {
                   img:url
                },
                success: function (data) {
                    console.log(data);
                    if(data.status=='success') Message('Ảnh sẽ thay đổi vào lần đăng nhập sau');
                    else Message('Cập nhật ảnh chưa thành công');
                }

            });
        }
    });
</script>
@endsection