@extends('cpanel.template.layout')
@section('title', 'Test up load file')
@section('content')

    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Test up load file</h3></div>
            </div>
        </div>
        <div class="separator bottom"></div>
        <div class="innerLR">
            <form action="{{action('SystemController@postUploadd')}}" method="post" enctype="multipart/form-data" >
                <input type="file" name="anh" id="">
                <button type="submit">Submit</button>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
            </form>
        </div>
    </div>

    <!-- End Content -->

@endsection