@extends('cpanel.template.layout')
@section('title', 'Cài đặt tin nhắn')
@section('content')
    <style>
        .attribute {
            float: left;
            width: 33%;
            padding-top: 50px
        }

        .textcolor {
            padding: 10px 0px 0px 35px;
            color: black
        }

        .margin-non {
            margin: 5px 0px 0px 0px !important;
        }

        .control-left {
            margin-left: 110px !important;
        }

        .grip {
            width: 0;
            height: 0;
            margin-left: 9px;
            border-left: 5px solid transparent;
            border-right: 5px solid transparent;

            border-top: 10px solid #3a2d2d;
        }

        table, td, th {
            border: 1px solid;
            text-align: center;
        }

        .divtable {
            height: 250px;
            margin-top: 20px
        }

        .widthauto {
            margin-top: 10px;
            width: auto;
        }

        #updateSetting {
            margin: 30px 0px 0px 34% !important;
            font-size: 14px !important;
        }

        .label-left {
            width: 28% !important;
        }

        #ul_tab a {
            color: beige;
            border-radius: 16px;
            margin: 0px 5px 0px 5px;
            background-color: #163b9c;
        }

        #ul_tab li {
            width: 33.33%;
            text-align: center;
            font-size: 18px;
            border: none;
        }

        #ul_tab {
            float: left;
            width: 100%;
            background-color: white;
            border: none;
        }

        .superman {
            margin-left: 1px;
            float: left;
            position: relative;
            text-align: center;
            border-radius: 7px;
            font-size: 15px;
            font-weight: bold;
            background-color: #c7a81a;
            height: 30px !important;
            min-width: 9%;
            padding-top: 5px;
        }

        .submit {
            text-align: center;
            font-size: 14px;
            border: none;
            font-weight: bold;
            background-color: #0b93b9;
            width: 20%;
            height: 34px;
            margin: 10px 0px 0px 38%;
            padding-top: 5px;
        }

        textarea {
            margin-left: 15px;
            width: 97%;
            height: auto;
            font-family: sans-serif;
        }

        .tabsbar.tabsbar-radius ul li.active a, .tabsbar ul li:hover a {
            background: #0077E2;
            color: #ffffff;
        }

        ul.row-merge li.active a, .tabsbar ul li:hover a {
            background: #0077E2 !important;
            color: rgba(25, 7, 7, 0) !important;
        }

        .submit:hover {
            background: #b5c716;
        }

        .superman:hover {
            background: #b5c716;
        }

        .label_active {
            background: #b5c716;
        }

        .tab-content > label {
            margin: 50px 0px 5px 20px;
            color: #293c9e;
            font-weight: 600;
            font-size: 16px;
        }

        .tab-pane > label {
            margin: 10px 0px 30px 20px;
            font-weight: bold;
        }
        .tab-pane span{
            color: #b92d2d;
            font-size: 15px;
        }
    </style>
    {{--@php(dev($setting))--}}
    <div id="content">
        <div style="height: 20px" class=" heading_top">
            <div class="row-fluid">
                <div class="pull-left span8" style="padding-left: 10px;">
                    <h3>Cài đặt hiển thị tin nhắn</h3>
                </div>
            </div>
        </div>
        <div class="row-fluid bg_light m_top_20">
            <div class="widget widget-4 bg_light">
                <div class="widget-body">
                    <div>
                        <div class="tabsbar tabsbar-radius" id="ul_tab">
                            <ul class="row-fluid row-merge">
                                <li class="active" data-tab="#tab1">
                                    <a href="#tab1" data-toggle="tab">Dành cho hành khách</a>
                                </li>
                                <li data-tab="#tab2">
                                    <a href="#tab2" data-toggle="tab">Dành cho nhà vận tải</a>
                                </li>
                                <li data-tab="#tab3">
                                    <a href="#tab3" data-toggle="tab">Tin nhắn thông báo</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content" style="margin: 100px 5px 0px 5px;">
                        <div class="row-fluid">
                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <label>Nội dung tin nhắn :</label>
                        <div class="tab-pane active" id="tab1">
                            <textarea id="textArea1" spellcheck="false"
                                      placeholder=""></textarea>
                            <label></label>
                            <button id="submit_MS1" data-type="1" class="submit"></button>
                        </div>
                        <div class="tab-pane " id="tab2">
                            <textarea id="textArea2" spellcheck="false"
                                      placeholder=""></textarea>
                            <label></label>
                            <button id="submit_MS2" data-type="2" class="submit"></button>
                        </div>
                        <div class="tab-pane " id="tab3">
                            <textarea id="textArea3" spellcheck="false"
                                      placeholder=""></textarea>
                            <label></label>
                            <button id="submit_MS3" data-type="3" class="submit"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script>

        var listAttr = {
            routeName: 'Tuyến',
            ticketCode: 'Mã vé',
            listSeatId: 'Ghế',
            pickUpTime: 'Giờ lên xe',
            startTime: 'Giờ chạy',
            getInTime: 'Ngày đi',
            inPoint: 'Điểm lên',
            offPoint: 'Điểm xuống',
            ticketStatus:'Trạng thái vé',
            phoneNumberCompany: 'SĐT nhà vận tải',
            phoneNumberCustomer: 'SĐT khách',
            emailCustomer: 'Email khách',
            fullName: 'Tên khách',
            website: 'Web online',
            ticketPrice: 'Giá vé',
            mealPrice: 'Giá ăn',
            numberPlate: 'Biển xe'
        }, html = '';
        var strace1=strace2=strace3='';
        var listSMS ={!! json_encode($setting) !!};
        $("#content").attr("style", "width:100%");
        $("#menu").attr("style", "margin-left:-400px");
        $("#ht_btn_close_open_menu").html('<button onClick="open_menu()" type="button" class="btn-navbar"> <div class="icon_open_menu"></div> </button>');

        $(document).ready(function () {
            init();
            strace1=(document.getElementById('textArea1')).value;
            strace2=(document.getElementById('textArea2')).value;
            strace3=(document.getElementById('textArea3')).value;
            $('.copy_clipboard').click(function () {
                var e = document.querySelector('.tab-content .active textarea');
                var text = e.value;
                var indexStart = e.selectionStart;
                if (!text.includes($(this).text()))
                    e.value = (text.substr(0, indexStart) + ' {' + $(this).text() + '} ' + text.substr(indexStart, text.length - 1));
                else {
                    $(this).removeClass('label_active');
                    e.value = text.split('{' + $(this).text() + '}').join('');
                    Message('Thông tin đã được loại bỏ !');
                }
                e.focus();
                updateListLabel(e.value);
            });
            $('.submit').click(function () {
                var e = document.querySelector('.tab-content .active textarea');
                var text = e.value;
                var type = $(this).data('type');
                $.each(listAttr, function (k, v) {
                    if (text.includes('{' + v + '}')) {
                        text = text.replace('{' + v + '}', '${' + k + '}');
                    }
                });
                var data = {
                    nativeContent: text,
                    messageType: type,
                    _token: '{{ csrf_token() }}',
                };
                $.each(listSMS,function (k,v) {
                    if(v.messageType == parseInt(type)){
                        data.id = v.id;
                        return false;
                    }
                })
                $.ajax({
                    url: '{{action('SystemController@setContentMessage')}}',
                    type: 'POST',
                    data: data,
                    success: function (data) {
                        if (data.code == 200 && data.status == 'success')
                            Message('Thao tác thành công !');
                        else Message('Thao tác chưa thành công !');
                    },
                    error: function (data) {
                        Message('Yêu cầu chưa được gửi đi , Có lỗi xảy ra !');
                    }

                });
            });
            $('#ul_tab li').click(function () {
                var e = document.querySelector($(this).data('tab') + ' textarea');
                updateListLabel( e.value);
            });
            $('textarea').keyup(function () {
                var text=this.value,strace_index=[],sms,that=this;
                var indexStart = this.selectionStart;
                if($(this).attr('id')==='textArea1'){sms=strace1;}
                else if($(this).attr('id')==='textArea2') {sms=strace2;}
                else {sms=strace3;}

                if(sms.length<text.length && ['{','}'].indexOf(text[indexStart-1])>=0) {text=sms; this.value=sms;}

                if($(this).attr('id')==='textArea1'){strace1=text;}
                else if($(this).attr('id')==='textArea2') {strace2=text;}
                else {strace3=text;}

               $.each(sms,function (k,v) {
                  if(['}','{'].indexOf(v)>=0) strace_index.push(k);
               });

               for(var i=0;i<strace_index.length;i=i+2){
                   if(indexStart>=strace_index[i] && strace_index[i+1]>=indexStart){
                       sms=sms.substr(0,strace_index[i])+sms.substr(strace_index[i+1]+1,sms.length);
                       indexStart=strace_index[i];
                       that.value=sms;
                       if($(that).attr('id')==='textArea1') strace1=sms;
                       else if($(that).attr('id')==='textArea2') strace2=sms;
                       else strace3=sms;
                       Message('Thông tin đã được loại bỏ !');
                       break;}
               }

                updateListLabel(sms);
                this.focus();
                this.selectionEnd=indexStart;
            });
        });

        function init() {
            $.each(listAttr, function (k, v) {
                html += '<label class="copy_clipboard superman" id="' + k + '">' + v + '</label>';
            });
            $('.tab-content .row-fluid').html(html);
            if (listSMS.length > 0) {
                var SMS1 = $.grep(listSMS, function (v) {
                    return v.messageType === 1;
                })[0];
                var SMS2 = $.grep(listSMS, function (v) {
                    return v.messageType === 2;
                })[0];
                var SMS3 = $.grep(listSMS, function (v) {
                    return v.messageType === 3;
                })[0];
                console.log(SMS1);
                if (SMS1) {
                    (document.getElementById('textArea1')).value = generateTextArea(SMS1.nativeContent,strace1);
                    updateListLabel(SMS1.nativeContent);
                }

                if (SMS2) {
                    (document.getElementById('textArea2')).value = generateTextArea(SMS2.nativeContent,strace2);
                }
                if (SMS3) {
                    (document.getElementById('textArea3')).value = generateTextArea(SMS3.nativeContent,strace3);
                }
            }
            $('.tab-pane>label').html('Lưu ý : <span>1</span> tin nhắn chỉ tối đa <span>160</span> ký tự không dấu hoặc <span>70</span> ký tự có dấu . Phí tin nhắn sẽ tăng lên khi vượt giới hạn !  <span class="lengh"></span>');
            $('.tab-pane>button').text('Hoàn thành cài đặt');
            $('.tab-pane>textarea').attr('placeholder','Click chọn thông tin bên trên để hoàn thành cấu hình');
        }

        function updateListLabel(text) {
            var Length=text.length;
            if (text !== '') {
                $.each(listAttr, function (k, v) {
                    if (text.includes('{' + v + '}') || text.includes('{' + k + '}')){
                        $('#' + k).addClass('label_active');
                        Length-=('{' + v + '}').length;
                    }
                    else $('#' + k).removeClass('label_active');
                });
            } else $('.superman').removeClass('label_active');
            $('.lengh').text('( >'+Length+')');
        }

        function generateTextArea(sms) {
            $.each(listAttr, function (k, v) {
                sms = sms.split("${" + k + "}").join("{" + v + "}");
            });
            return sms;
        }
    </script>

@endsection
