@extends('cpanel.template.layout')
@section('title', 'Cài đặt in vé')
@section('content')
    <style>
        .attribute {
            float: left;
            width: 33%;
            padding-top: 50px
        }

        .textcolor {
            padding: 10px 0px 0px 35px;
            color: black
        }

        .margin-non {
            margin: 5px 0px 0px 0px !important;
        }

        .control-left {
            margin-left: 110px !important;
        }

        .grip {
            width: 0;
            height: 0;
            margin-left: 9px;
            border-left: 5px solid transparent;
            border-right: 5px solid transparent;

            border-top: 10px solid #3a2d2d;
        }

        table, td, th {
            border: 1px solid;
            text-align: center;
        }

        .divtable {
            height: 250px;
            margin-top: 20px
        }

        .widthauto {
            margin-top: 10px;
            width: auto;
        }

        #updateSetting {
            margin: 30px 0px 0px 34% !important;
            font-size: 14px !important;
        }

        .label-left {
            width: 28% !important;
        }
    </style>
    <div>
        <div style="height: 20px" class=" heading_top">
            <div class="row-fluid">
                <div class="pull-left span8" style="padding-left: 10px;">
                    <h3>Thông tin áp dụng</h3>
                </div>

            </div>
        </div>
        <div class="row-fluid heading_top" style="height: 110px;margin-top: 20px;">
            <div class="controls">
                @php $dem=1; @endphp
                @foreach ($result as $row)
                    <div class="span3" {{$dem==1?'style=margin-left:35px;':""}}>
                        <input id="{{$row['attribute']}}_check" type="checkbox" name="{{$row['attribute']}}_check"
                               data-idAttribute="{{$row['id']}}" data-id="{{$row['attribute']}}"
                               class="span12 check {{$row['attribute']}}_check"
                                {{$row['status']==1?'checked':''}}>
                        <label style="margin-top: 5px" class="f_left"
                               for="{{$row['attribute']}}_check">{{$row['name']}}</label>
                    </div>
                    @php $dem++; @endphp
                @endforeach
            </div>
        </div>
        <div class="divtable">
            <div class=" row-fluid" style="overflow-x: scroll;">
                <table style="width:99%;font-family: 'Verdana'; font-size: 12px;margin: 0px 0px 0px 10px;"
                       id="setting"></table>
            </div>
        </div>


    </div>
    <div style=" margin-top: 50px;">
        <form action="{{action('settingPrintTicketController@updateSetting')}}" method="get">
            <input type="hidden" class="sendListAttribute" name="listAttribute" value="">
            <label style="margin: 30px 0px 0px 36%;font-size: 14px" class="span4"></label>
            <button id="updateSetting" class="span4 btn btn-warning btn-flat">Cập nhật cài đặt
            </button>
        </form>
    </div>

    <script>

        $(document).ready(function () {
            $("#content").attr("style", "width:100%");
            $("#menu").attr("style", "margin-left:-400px");
            $("#ht_btn_close_open_menu").html('<button onClick="open_menu()" type="button" class="btn-navbar"> <div class="icon_open_menu"></div> </button>');
            $('form>label').text('( Tự động căn chỉnh theo mặc định khi không điền thông tin )');

            var listAttribute =
                    {!! json_encode($result) !!}
            var html = '<tr><th style="width: 90px;"></th>';
            var use = 0;
            var listOption = {
                0: "In đậm",
                1: "Cỡ chữ",
                // 2: "Chiều rộng",
                // 3: "Chiều cao",
                2: "Tên hiển thị",
                3: "Phông chữ",
                4: "idAttribute",
            };

            $.each(listAttribute, function (k, v) {
                if (v.status == 1) {
                    html += '<th style="width:auto">' + v.name + '</th>';
                    use++;
                }
            });
            html += '</tr>';
            for (var i = 0; i < 4; i++) {
                html += '<tr><td style="width: 90px;">' + listOption[i] + '</td>';
                $.each(listAttribute, function (k, v) {
                    if (v.status == 1) {
                        if (i == 0) html += '<td style=" height:40px;width:auto">' +
                            '<input ' + (v.bold == 1 ? 'checked' : '') + ' class="widthauto ' + v.attribute + '_check" id="bold_' + v.attribute + '" name="bold_' + v.attribute + '" type="checkbox">' +
                            '<label for="bold_' + v.attribute + '" style="margin: -10px 45%;"></label></td>';
                        else if (i == 1) html += '<td style="width:auto"><input class="widthauto ' + v.attribute + '_check" type="number" value="' + (v.size > 0 ? v.size : '') + '"></td>';
                        else if (i == 2) html += '<td style="width:auto"><input class="widthauto ' + v.attribute + '_check" type="text" value="' + v.alias + '"></td>';
                        else html += '<td style="width:auto"><select style="max-width: 155px;" class="widthauto ' + v.attribute + '_check" type="text" >' + generateHtmlSelect(v) + '</select></td>';
                    }
                });
                html += '</tr>';
            }
            if (use != 0) $('#setting').html(html);
            $('input.check').click(function () {
                var html = '<tr><th></th>';
                $('.check:checked').each(function () {
                    var attr = ($(this).attr('id')).substring(0, ($(this).attr('id')).length - 6);
                    $.each(listAttribute, function (k, v) {
                        if (v.attribute == attr)
                            html += '<th style="width:auto">' + v.name + '</th>';
                    });
                });
                html += '</tr>';
                for (var i = 0; i < 4; i++) {
                    html += '<tr><td style="width: 90px;">' + listOption[i] + '</td>';
                    for (var j = 0; j < $('.check:checked').length; j++) {
                        var dem = 0;
                        var html2 = '';
                        var attr = ($($('.check:checked')[j]).attr('id')).substring(0, ($($('.check:checked')[j]).attr('id')).length - 6);
                        $.each(listAttribute, function (k, v) {
                            if (v.attribute == attr && v.status == 1) {
                                dem++;
                                if (i == 0) html2 += '<td style=" height:40px;width:auto">' +
                                    '<input ' + (v.bold == 1 ? 'checked' : '') + ' class="widthauto ' + v.attribute + '_check" id="bold_' + v.attribute + '" name="bold_' + v.attribute + '" type="checkbox">' +
                                    '<label for="bold_' + v.attribute + '" style="margin: -10px 45%;"></label></td>';
                                else if (i == 1) html2 += '<td style="width:auto"><input class="widthauto ' + v.attribute + '_check" type="number" value="' + (v.size > 0 ? v.size : 12) + '"></td>';
                                else if (i == 2) html2 += '<td style="width:auto"><input class="widthauto ' + v.attribute + '_check" type="text" value="' + v.alias + '"></td>';
                                else html2 += '<td style="width:auto"><select style="max-width: 155px;" class="widthauto ' + v.attribute + '_check" type="text">' + generateHtmlSelect(v) + '</select></td>';
                            }
                        });
                        if (dem == 0) {
                            var nameforcheckking = $($('.check:checked')[j]).data('id');
                            if (i == 0) html += '<td style=" height:40px;"><input class="widthauto ' + nameforcheckking + '_check" id="bold_' + nameforcheckking + '" name="bold_' + nameforcheckking + '" type="checkbox">' +
                                '<label for="bold_' + nameforcheckking + '" style="margin: -10px 45%;"></label></td>';
                            else if (i == 1) html += '<td><input class="widthauto ' + nameforcheckking + '_check" type="number"></td>';
                            else if (i == 2) html += '<td><input class="widthauto ' + nameforcheckking + '_check" type="text"></td>';
                            else html += '<td><select style="max-width: 155px;" class="widthauto ' + nameforcheckking + '_check" type="text">' + generateHtmlSelect() + '</select></td>';
                        } else {
                            html += html2;
                        }
                    }
                    html += '</tr>';
                }
                $('#setting').html(html);
                if (!$(this).is(':checked'))
                    if ($('.check:checked').length == 0) $('#setting').html('');
                    else $('#setting').html(html);
                else $('#setting').html(html);
            });

            $('#updateSetting').click(function () {

                var listSubmit = [], list = [],
                    listSetting = {
                        bold: 0,
                        size: 0,
                        fontfamily: 0,
                        orderDisplay: 0,
                        alias: '',
                        name: '',
                        idAttribute: ''
                    };
                $('.check:checked').each(function () {
                    var dem = 1;
                    var attr = ($(this).attr('id')).substring(0, ($(this).attr('id')).length - 6);
                    listSetting.status = 1;
                    listSetting.idAttribute = $(this).data('idattribute');
                    listSetting.name = attr;
                    $('.' + attr + '_check').each(function () {
                        if (dem == 2) listSetting.bold = $(this).is(':checked') ? 1 : 0;
                        else if (dem == 3) listSetting.size = $(this).val();
                        else if (dem == 4) listSetting.alias = $(this).val();
                        else if (dem == 5) listSetting.fontfamily = $(this).val();
                        dem++;
                    });
                    list.push(JSON.parse(JSON.stringify(listSetting)));
                    listSubmit.push(JSON.parse(JSON.stringify(list)));
                    list = [];
                });

                $('.sendListAttribute').val(JSON.stringify(listSubmit));
            });
        });

        function generateHtmlSelect(info) {
            var listOptionFont = '<option>Chọn</option>' +
                '<option style="font-family: Times New Roman, Times, serif" value="1" ' + (info != undefined && info.fontfamily != undefined && info.fontfamily == 1 ? 'selected' : '') + '> Times New Roman, Times, serif</option>' +
                '<option style="font-family: Arial, Helvetica, sans-serif"  value="2"' + (info != undefined && info.fontfamily != undefined && info.fontfamily == 2 ? 'selected' : '') + '>Arial, Helvetica, sans-serif</option>' +
                '<option style="font-family:Arial Black, Gadget, sans-serif "  value="3"' + (info != undefined && info.fontfamily != undefined && info.fontfamily == 3 ? 'selected' : '') + '>Arial Black, Gadget, sans-serif </option>' +
                '<option style="font-family:Comic Sans MS, cursive, sans-serif "  value="4" ' + (info != undefined && info.fontfamily != undefined && info.fontfamily == 4 ? 'selected' : '') + '>Comic Sans MS, cursive, sans-serif</option>' +
                '<option style="font-family: Impact, Charcoal, sans-serif"  value="5" ' + (info != undefined && info.fontfamily != undefined && info.fontfamily == 5 ? 'selected' : '') + '> Impact, Charcoal, sans-serif</option>' +
                '<option style="font-family:Tahoma, Geneva, sans-serif "  value="6" ' + (info != undefined && info.fontfamily != undefined && info.fontfamily == 6 ? 'selected' : '') + '>Tahoma, Geneva, sans-serif</option>' +
                '<option style="font-family: Verdana, Geneva, sans-serif"  value="7" ' + (info != undefined && info.fontfamily != undefined && info.fontfamily == 7 ? 'selected' : '') + '>Verdana, Geneva, sans-serif</option>';
            return listOptionFont;
        }
    </script>
@endsection
