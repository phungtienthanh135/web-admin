@extends('cpanel.template.layout')
@section('title', 'Danh sách số máy lẻ')
@section('content')
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Danh sách số máy lẻ</h3></div>
            </div>
        </div>
        <div class="khung_lich_ve">
            <div class="date_picker">
                <div style="padding: 7px;" class="widget widget-4">
                    <div class="widget-head">
                        <h4>Tìm số máy lẻ</h4>
                    </div>
                </div>
                <div class="innerLR">
                    <form action="">
                        <div class="row-fluid">
                            <div class="span2">
                                <label>Số máy lẻ</label>
                                <input autocomplete="off" class="w_full isNumber" type="text" name="phoneNumber" id="phoneNumber"
                                       value="{{request('phoneNumber')}}">
                            </div>
                            <div class="span2">
                                <label>Nhân viên</label>
                                <select name="userName" id="userName">
                                    @foreach($listUser as $user)
                                        <option value="{{$user['userId']}}" {{ request('userId') == $user['userId'] ? 'selected' : "" }}>{{$user['fullName']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="span2">
                                <label>&nbsp;</label>
                                <button class="btn btn-info btn-flat-full" id="search">TÌM KIẾM</button>
                            </div>

                            <div class="span2">
                                <label>&nbsp;</label>
                                <button type="button" class="btn btn-warning btn-flat-full" id="addPhoneNumber">THÊM SỐ
                                    MÁY LẺ
                                </button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row-fluid bg_light m_top_10">
            <div class="widget widget-4 bg_light">
                <div class="widget-body">
                    <table class="table table-hover table-vertical-center">
                        <thead>
                        <tr>
                            <th>Mã số</th>
                            <th>Số máy lẻ</th>
                            <th>Nhân viên sử dụng</th>
                            <th>Tùy chọn</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($result as $row)
                            <tr>
                                <td>{{$row['id']}}</td>
                                <td>{{$row['number']}}</td>
                                <td>{{@$row['fullName']}}</td>
                                <td>
                                    <a tabindex="0"
                                       class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                        <ul class="onclick-menu-content">

                                            <li>
                                                <button value="{{$row['id']}}" id="editPhoneNumber"
                                                        data-phone="{{$row['number']}}" data-user="{{$row['userId']}}" data-telecom-config="{{@$row['telecomConfig']}}" data-telecom-company-id="{{@$row['telecomCompanyId']}}">
                                                    Sửa số
                                                </button>
                                            </li>

                                            <li>
                                                <button value="{{$row['id']}}" id="deletePhoneNumber">Xóa số</button>
                                            </li>
                                        </ul>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                    @include('cpanel.template.pagination-without-number',['page'=>$page])
                </div>
            </div>
        </div>
<!-- Modal thêm mới và sửa số điện thoại -->
        <div class="modal modal-custom hide fade" id="modal_editPhoneNumber"
             style="width: 400px; margin-left: -10%;">
            <form action="{{action('SystemController@addOrUpdatePhoneNumber')}}" method="post">
                <div class="modal-header center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 id="title-editPhoneNumbert">CẬP NHẬT THÔNG TIN SỐ MÁY LẺ</h3>
                </div>
                <div class="modal-body center">
                    <p id="lb_message"></p>

                    <div id="frmUpdateInfo" class="row-fluid">
                        <div class="form-horizontal row-fluid" style="text-align: left">
                            <div class="control-group">
                                <label for="slbTelecomCompany text-left">Nhà Cung Cấp</label>
                                <select name="telecomCompanyId" id="slbTelecomCompany" style="width: 100%">
                                    @foreach($telecomCompany as $telCom)
                                        <option @if(isset($result['telecomCompanyId'])&&$telCom['telecomCompanyId']== $result['telecomCompanyId']) selected @endif value="{{ $telCom['telecomCompanyId'] }}">{{ $telCom['telecomCompanyName'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="control-group">
                                <label for="txtPhoneNumber">Số tổng đài</label>
                                <input type="text" name="phoneNumber" id="txtPhoneNumber" style="width: -webkit-fill-available">
                            </div>
                            <div class="control-group">
                                <label for="txtTelecomConfig">Chuỗi Cấu Hình</label>
                                <textarea name="telecomConfig" id="txtTelecomConfig" style="width:-webkit-fill-available;" rows="3"></textarea>
                            </div>
                            {{--<div class="control-group">--}}
                                {{--<label class="control-label" for="txtUserId">Nhân viên</label>--}}
                                {{--<div class="controls">--}}
                                    {{--<select name="userId" id="userId">--}}
                                        {{--<option value="">Không lựa chọn</option>--}}
                                        {{--<option value="{{session('userLogin')['userInfo']['userId']}}">Chính tôi</option>--}}
                                        {{--@foreach($listUser as $user)--}}
                                            {{--<option value="{{$user['userId']}}">{{$user['fullName']}}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="phoneId" value="" id="txtPhoneId">
                        <input type="hidden" name="type" value="" id="type">
                        <a style="cursor: pointer" data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                        <button id="btnUpdatePhoneNumber" class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
                    </div>

                </div>
            </form>
        </div>

        <!-- Modal xóa vé -->
        <div class="modal hide fade" id="modal_delete" style="width: 250px; margin-left: -10%;margin-top: 10%;">
            <div class="modal-body center">

                <p>Bạn có chắc muốn xoá?</p>
            </div>
            <form action="{{action('SystemController@deletePhone')}}" method="post">
                <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="phoneId" value="" id="deletePhone">
                    <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                    <button class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
                </div>
            </form>
        </div>

    </div>
    <script>
        $("#userName").select2();
        $("#userId").select2({
            dropdownParent: $("#modal_editPhoneNumber")
        });
        $('body').on('click', 'button#editPhoneNumber,button#deletePhoneNumber', function (e) {
            if ($(e.currentTarget).is('#editPhoneNumber')) {
                $('#lb_message').text('').hide();
                $('#title-editPhoneNumbert').text('CẬP NHẬT THÔNG TIN SỐ MÁY LẺ');
                $('#txtPhoneNumber').val($(this).data('phone'));
                $('#slbTelecomCompany').val($(this).data('telecom-company-id'));
                var strConfig = $(this).data('telecom-config')!==''?JSON.stringify($(this).data('telecom-config')) :'';
                $('#txtTelecomConfig').val(strConfig);
                $('#userId').val($(this).data('user')).change();
                $('#txtPhoneId').val($(this).val());
                $('#type').val(2);
                $('#modal_editPhoneNumber').modal('show');
            }

            if ($(e.currentTarget).is('#deletePhoneNumber')) {
                $('#deletePhone').val($(this).val());
                $('#modal_delete').modal('show');
            }

        });

        $('#addPhoneNumber').click(function () {
            $('#lb_message').text('').hide();
            $('#title-editPhoneNumbert').text('THÊM MỚI SỐ MÁY LẺ');
            $('#txtPhoneNumber').val('');
            $('#txtTelecomConfig').val('');
            $('#userId').val('').change();
            $('#txtPhoneId').val('');
            $('#type').val(1);
            $('#modal_editPhoneNumber').modal('show');
        });

    </script>
@endsection