@extends('cpanel.template.layout')
@section('title', 'Khuyến mại')

@section('content')
    <style>
        .select2-container {
            z-index: 10002;
            width: 100% !important;
        }

        tr {
        }
    </style>

    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Mã khuyến mãi</h3></div>
            </div>
        </div>
        <div class="khung_lich_ve">
            <div class="date_picker">
                <div style="padding: 7px;" class="widget widget-4">
                    <div class="widget-head">
                        <h4>Tìm mã khuyến mãi</h4>
                    </div>
                </div>
                <div class="row-fluid">
                    <form action="">
                        <div style="margin-left: 5px;" class="span4">
                            <div class="row-fluid">
                                <div class="span12">
                                    <label for="promotionCode">Mã khuyến mãi</label>
                                    <input autocomplete="off" id="promotionCode" type="text" class="w_full"
                                           name="promotionCode">
                                </div>
                                {{-- <div class="span4 p_right_15">
                                     <label for="name_employee">Từ ngày</label>
                                     <div class="input-append">
                                         <input class="span10 datepicker" id="txtTuNgay" type="text">
                                         <button class="btn color_blue" disabled="disabled" type="button"><i
                                                     class="icon-calendar"></i></button>
                                     </div>
                                 </div>
                                 <div class="span4 p_right_15">
                                     <label for="name_employee">Đến ngày</label>
                                     <div class="input-append">
                                         <input class="span10 datepicker" id="txtDenNgay" type="text">
                                         <button class="btn color_blue" disabled="disabled" type="button"><i
                                                     class="icon-calendar"></i></button>
                                     </div>
                                 </div>--}}
                            </div>

                        </div>
                        <div style="margin-left:20px" class="span6">
                            <div class="row-fluid">
                                <div class="span4">
                                    <label for="">&nbsp;</label>
                                    <button class="btn btn-info btn-flat-full" id="search">TÌM MÃ</button>
                                </div>
                                @if(hasAnyRole(CREATE_PROMOTION))
                                    <div class="span4 p_right_15">
                                        <label for="">&nbsp;</label>
                                        <a href="#modal_taomakhuyenmai" data-toggle="modal"
                                           class="btn btn-warning btn-flat-full">TẠO MÃ KHUYẾN MÃI</a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row-fluid bg_light m_top_10">
            <div class="widget widget-4 bg_light">
                <div class="widget-body">
                    <table class="table table-hover table-vertical-center">
                        <thead>
                        <tr>
                            <th>Mã khuyến mãi</th>
                            <th>Mức ưu đãi</th>
                            <th>Loại khuyến mãi</th>
                            <th>Ngày bắt đầu</th>
                            <th>Ngày kết thúc</th>
                            <th>Mô tả</th>
                            <th>Tùy chọn</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($listPromtion as $row)
                            <tr>
                                <td><span class="khung_makhuyenmai">{{$row['promotionCode']}}</span></td>
                                <td>
                                    @if($row['percent']!=-1)
                                        {{$row['percent']*100}}%
                                    @else
                                        @moneyFormat($row['price'])
                                    @endif
                                </td>
                                <td>{{$row['promotionType']}}</td>
                                <td>@dateFormat(strtotime($row['startDate'])*1000)</td>
                                <td>@dateFormat(strtotime($row['endDate'])*1000)</td>
                                <td>{{@$row['note']}}</td>
                                <td>
                                    <a href="" tabindex="0"
                                       class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                        <ul class="onclick-menu-content">
                                            @if(hasAnyRole(UPDATE_PROMOTION))
                                                <li>
                                                    <button data-toggle="modal" data-target="#modal_edit"
                                                            value="{{$row['promotionId']}}" class="btn_edit">Sửa
                                                    </button>
                                                </li>
                                            @endif
                                            @if(hasAnyRole(DELETE_PROMOTION))
                                                <li>
                                                    <button data-toggle="modal" data-target="#modal_delete"
                                                            value="{{$row['promotionId']}}" class="btn_delete">Xoá
                                                    </button>
                                                </li>
                                            @endif
                                        </ul>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    @include('cpanel.template.pagination-without-number',['page'=>$page])
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <!-- End Content -->
    @if(hasAnyRole(CREATE_PROMOTION))
        <!-- Modal inline -->
        <div class="modal hide fade no_border" id="modal_taomakhuyenmai" style="width: 700px">
            <div class="modal-header header_modal center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>TẠO MÃ KHUYẾN MÃI</h3>
            </div>
            <form action="{{action('CouponController@postAdd')}}" method="post">
                <div class="modal-body">
                    <table class="table_in_modal">
                        <tr>
                            <td>Mã khuyến mãi</td>
                            <td colspan="3">
                                <input autocomplete="off" class="span12" name="promotionCode" type="text"
                                       data-validation="custom" data-validation-regexp="^([a-zA-Z0-9]+)$"
                                       data-validation-error-msg="Mã khuyến mại không hợp lệ"
                                       value="{{old('promotionCode')}}"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Giá trị</td>
                            <td>
                                <div class="input-append">
                                    <input value="{{!empty(old('price'))?old('price'):old('percent')}}" id="txt_giatri"
                                           style="width:145px" name="{{!empty(old('percent'))?'percent':'price'}}"
                                           type="number" min="1" placeholder="Bắt buộc nhập">
                                    <select id="sl_donvi" class="btn">
                                        <option {{!empty(old('price'))?'selected':''}} value="1">VNĐ</option>
                                        <option {{!empty(old('percent'))?'selected':''}} value="2">%</option>
                                    </select>
                                </div>
                            </td>
                            <td><p class="pull-right">Số lần</p></td>
                            <td colspan="3">
                                <input class="span12" name="number" type="number" min="0"
                                       value="{{old('number')}}" placeholder="Để trống sẽ không giới hạn"/>

                            </td>
                        </tr>
                        <tr>
                            <td>Ngày bắt đầu</td>
                            <td>
                                <div class="input-append">
                                    <input autocomplete="off" class="span12" id="txtStartDate" type="text"
                                           name="startDate" style="width: 195px;"
                                           value="{{old('startDate',date('d-m-Y',time()))}}">
                                </div>

                            </td>
                            <td><p class="pull-right">Ngày kết thúc</p></td>
                            <td>
                                <div class="input-append">
                                    <input autocomplete="off" style="width:180px" id="txtEndDate" type="text"
                                           name="endDate"
                                           value="{{old('endDate')}}">
                                </div>
                            </td>
                        </tr>


                        <tr>
                            <td>
                                Khuyến mãi dành cho
                            </td>

                            <td colspan="4">
                                <input checked name="promotionType" type="radio" id="FOR_ALL_USER" value="1"/>
                                <span class="m_right_10">Tất cả</span>

                                <input name="promotionType" type="radio" id="FOR_NEW_USER" value="2"/>
                                <span class="m_right_10">Khách hàng mới</span>

                                <input name="promotionType" type="radio" id="FOR_LOYAL_USER" value="3"/>
                                <span class="">Khách hàng thân thiết</span>
                                <input name="promotionType" type="radio" id="FOR_AGENCY" value="4" style="margin-left: 25px"/>
                                <span class="">Đại lý</span>
                            </td>
                        </tr>
                        <tr>
                            <td>Tiền tối thiểu</td>
                            <td colspan="3">
                                <input autocomplete="off" class="span12" placeholder="Tiền vé tối thiểu để được hưởng khuyến mại" name="minPriceApply" type="number" value="{{old('minPriceApply')}}"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" style="margin-top: 10px">Lặp lại vào</label>
                            </td>
                            <td colspan="3">
                                <div class="control-group">

                                    <div style="padding-top:5px" class="controls listdate">

                                        <input {{getdate()['wday']==1?'checked value=1':''}} data-name="T2"
                                               type="checkbox"
                                               id="cl_thu2" class="add"
                                               name="listDate[]"/>
                                        <label class="f_left" for="cl_thu2"></label>
                                        <span class="f_left m_right_10">T2</span>

                                        <input {{getdate()['wday']==2?'checked value=2 ':''}} data-name="T3"
                                               type="checkbox"
                                               id="cl_thu3" class="add"
                                               name="listDate[]"/>
                                        <label class="f_left" for="cl_thu3"></label>
                                        <span class="f_left m_right_10">T3</span>

                                        <input {{getdate()['wday']==3?'checked  value=3':''}} data-name="T4"
                                               type="checkbox"
                                               id="cl_thu4" class="add"
                                               name="listDate[]"/>
                                        <label class="f_left" for="cl_thu4"></label>
                                        <span class="f_left m_right_10">T4</span>

                                        <input {{getdate()['wday']==4?'checked  value=4':''}} data-name="T5"
                                               type="checkbox"
                                               id="cl_thu5" class="add"
                                               name="listDate[]"/>
                                        <label class="f_left" for="cl_thu5"></label>
                                        <span class="f_left m_right_10">T5</span>

                                        <input {{getdate()['wday']==5?'checked value=5':''}} data-name="T6"
                                               type="checkbox"
                                               id="cl_thu6" class="add"
                                               name="listDate[]"/>
                                        <label class="f_left" for="cl_thu6"></label>
                                        <span class="f_left m_right_10">T6</span>

                                        <input {{getdate()['wday']==6?'checked value=6':''}} data-name="T7"
                                               type="checkbox"
                                               id="cl_thu7" class="add"
                                               name="listDate[]"/>
                                        <label class="f_left" for="cl_thu7"></label>
                                        <span class="f_left m_right_10">T7</span>
                                        <input {{getdate()['wday']==0?'checked value=7':''}} data-name="CN"
                                               type="checkbox" id="cl_cn"
                                               class="add"
                                               name="listDate[]"/>
                                        <label class="f_left" for="cl_cn"></label>
                                        <span class="f_left m_right_10">CN</span>
                                    </div>
                                    <div class="controls">(Mặc định tất cả nếu bỏ qua)</div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Phạm vi áp dụng</td>
                            <td colspan="3">
                                <select id="sl_type" name="subjects[]" multiple>
                                    <optgroup label="Tuyến" data-view="1">
                                        @foreach($listRoute as $route)
                                            <option value="{{$route['routeId']}}">{{$route['routeName']}}</option>
                                        @endforeach
                                    </optgroup>
                                    {{--<optgroup label="Xe">--}}
                                    {{--@foreach($listVehicle as $vehicle)--}}
                                    {{--<option value="{{$vehicle['vehicleId']}}">{{$vehicle['numberPlate']}}</option>--}}
                                    {{--@endforeach--}}
                                    {{--</optgroup>--}}
                                    {{--<optgroup label="Chuyến">--}}
                                    {{--@foreach($listschedule as $schedule)--}}
                                    {{--<option value="{{$schedule['scheduleId']}}">{{$schedule['routeName']}}---}}
                                    {{--@timeFormat($schedule['startTime'])--}}
                                    {{--</option>--}}
                                    {{--@endforeach--}}
                                    {{--</optgroup>--}}

                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Mô tả</td>
                            <td colspan="3">
                                <textarea class="span12" id="txtMota" name="note">{{old('note')}}</textarea>
                            </td>
                        </tr>
                    </table>

                </div>
                <div class="modal-footer">
                    <table class="table_in_modal">
                        <tr>
                            <td style="width:23%"></td>
                            <td style="width:48%" colspan="2">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button class="btn btn-warning btn-flat-full">LƯU</button>
                            </td>
                            <td style="text-align:left;padding-left:45px;padding-top:5px;"><a href="#"
                                                                                              data-dismiss="modal">HỦY</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
    @endif
    <!-- // Modal footer END -->
    @if(hasAnyRole(DELETE_PROMOTION))
        <!-- // Modal delete-->
        <div class="modal hide fade" id="modal_delete" style="width: 250px; margin-left: -10%;margin-top: 10%;">
            <div class="modal-body center">
                <p>Bạn có chắc muốn xoá?</p>
            </div>
            <form action="{{action('CouponController@delete')}}" method="post">
                <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="promotionId" value="" id="delete_coupon">
                    <a style="cursor: pointer" data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                    <button class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
                </div>
            </form>
        </div>
    @endif
    {{--Modal edit--}}
    <div class="modal hide fade no_border" id="modal_edit" style="width: 700px">
        <div class="modal-header header_modal center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3>SỬA MÃ KHUYẾN MÃI</h3>
        </div>
        <form action="{{action('CouponController@postEdit')}}" method="post">
            <div class="modal-body">
                <table class="table_in_modal">
                    <tr>
                        <td>Mã khuyến mãi</td>
                        <td colspan="3">
                            <input autocomplete="off" class="span12 promotionEdit" name="promotionCode" type="text"
                                   data-validation="custom" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td>Giá trị</td>
                        <td>
                            <div class="input-append">
                                <input value="" id="editGiatri"
                                       style="width: 115px;" name=""
                                       type="number" min="1" placeholder="Bắt buộc nhập">
                                <select id="donvi" class="btn">
                                    <option value="1">VNĐ</option>
                                    <option value="2">%</option>
                                </select>
                            </div>
                        </td>
                        <td><p class="pull-right">Số lần</p></td>
                        <td colspan="3">
                            <input class="span12 number" name="number" type="number" min="0"
                                   value="" placeholder="Để trống sẽ không giới hạn"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Ngày bắt đầu</td>
                        <td>
                            <div class="input-append">
                                <input autocomplete="off"  style="width: 135%;" class="span12" id="startDateEdit" type="text"
                                       name="startDate"
                                       value="">
                            </div>

                        </td>
                        <td><p class="pull-right">Ngày kết thúc</p></td>
                        <td>
                            <div class="input-append">
                                <input autocomplete="off" style="width:180px" id="endDateEdit" type="text"
                                       name="endDate"
                                       value="">
                            </div>
                        </td>
                    </tr>


                    <tr>
                        <td>
                            Khuyến mãi dành cho
                        </td>

                        <td colspan="4">
                            <input name="promotionType" type="radio" id="allUser" value="1"/>
                            <span class="m_right_10">Tất cả</span>

                            <input name="promotionType" type="radio" id="newUser" value="2"/>
                            <span class="m_right_10">Khách hàng mới</span>

                            <input name="promotionType" type="radio" id="loyalUser" value="3"/>
                            <span class="">Khách hàng thân thiết</span>
                            <input name="promotionType" type="radio" id="agency" value="4" style="margin-left: 25px"/>
                            <span class="">Đại lý</span>
                        </td>
                    </tr>
                    <tr>
                        <td>Tiền tối thiểu</td>
                        <td colspan="3">
                            <input autocomplete="off" class="span12" placeholder="Tiền vé tối thiểu để được hưởng khuyến mại" name="minPriceApply" id="txtMinPriceApply" type="number"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" style="margin-top: 10px">Lặp lại vào</label>
                        </td>
                        <td colspan="3">
                            <div class="control-group">

                                <div style="padding-top:5px" class="controls listdate">

                                    <input data-name="T2" type="checkbox"
                                           id="edit_thu2" class="edit"
                                           name="listDate[]"/>
                                    <label class="f_left" for="edit_thu2"></label>
                                    <span class="f_left m_right_10">T2</span>

                                    <input data-name="T3" type="checkbox"
                                           id="edit_thu3" class="edit"
                                           name="listDate[]"/>
                                    <label class="f_left" for="edit_thu3"></label>
                                    <span class="f_left m_right_10">T3</span>

                                    <input data-name="T4" type="checkbox" id="edit_thu4" class="edit" name="listDate[]"/>
                                    <label class="f_left" for="edit_thu4"></label>
                                    <span class="f_left m_right_10">T4</span>

                                    <input data-name="T5" type="checkbox"
                                           id="edit_thu5" class="edit"
                                           name="listDate[]"/>
                                    <label class="f_left" for="edit_thu5"></label>
                                    <span class="f_left m_right_10">T5</span>

                                    <input data-name="T6" type="checkbox"
                                           id="edit_thu6" class="edit"
                                           name="listDate[]"/>
                                    <label class="f_left" for="edit_thu6"></label>
                                    <span class="f_left m_right_10">T6</span>

                                    <input data-name="T7" type="checkbox"
                                           id="edit_thu7" class="edit" name="listDate[]"/>
                                    <label class="f_left" for="edit_thu7"></label>
                                    <span class="f_left m_right_10">T7</span>
                                    <input data-name="CN" type="checkbox" id="edit_cn"
                                           class="edit" name="listDate[]"/>
                                    <label class="f_left" for="edit_cn"></label>
                                    <span class="f_left m_right_10">CN</span>
                                </div>
                                <div class="controls">(Mặc định tất cả nếu bỏ qua)</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Phạm vi áp dụng</td>
                        <td colspan="3">
                            <select id="listApply" name="subjects[]" multiple>
                                <optgroup label="Tuyến" data-view="1">
                                    @foreach($listRoute as $route)
                                        <option value="{{$route['routeId']}}">{{$route['routeName']}}</option>
                                    @endforeach
                                </optgroup>
                                {{--<optgroup label="Xe">--}}
                                {{--@foreach($listVehicle as $vehicle)--}}
                                {{--<option value="{{$vehicle['vehicleId']}}">{{$vehicle['numberPlate']}}</option>--}}
                                {{--@endforeach--}}
                                {{--</optgroup>--}}
                                {{--<optgroup label="Chuyến">--}}
                                {{--@foreach($listschedule as $schedule)--}}
                                {{--<option value="{{$schedule['scheduleId']}}">{{$schedule['routeName']}}---}}
                                {{--@timeFormat($schedule['startTime'])--}}
                                {{--</option>--}}
                                {{--@endforeach--}}
                                {{--</optgroup>--}}

                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Mô tả</td>
                        <td colspan="3">
                            <textarea class="span12" id="txtNote" name="note"></textarea>
                        </td>
                    </tr>
                </table>
                <input type="hidden" id="promotionId" name="promotionId">
            </div>
            <div class="modal-footer">
                <table class="table_in_modal">
                    <tr>
                        <td style="width:23%"></td>
                        <td style="width:48%" colspan="2">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <button class="btn btn-warning btn-flat-full">LƯU</button>
                        </td>
                        <td style="text-align:left;padding-left:45px;padding-top:5px;"><a href="#" data-dismiss="modal">HỦY</a>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </div>
    <script>

        $(document).ready(function () {
            $('#startDateEdit').datepicker({dateFormat: 'dd-mm-yy'});
            var listPromotion ={!! json_encode($listPromtion) !!};
            $('.btn_edit').click(function () {
                var select = $(this).val();
                var check, html = '';
                var promotion = $.grep(listPromotion, function (e) {
                    return (e != null && e.promotionId == select);
                }); console.log(promotion);
                $('.promotionEdit').val(promotion[0].promotionCode);
                if (promotion[0].price > 0) {
                    $('#editGiatri').val(promotion[0].price);
                    $('#editGiatri').attr({'name': 'price'}).removeAttr('max');
                }
                else {
                    $('#editGiatri').val(promotion[0].percent * 100);
                    $('select#donvi option:eq(1)').attr('selected', 'selected');
                    $('#editGiatri').attr({'name': 'percent', 'max': 100});
                }
                promotion[0].number > 0 ? $('.number').val(promotion[0].number) : '';
                $('#startDateEdit').datepicker('setDate', $.datepicker.parseDate("yymmdd", promotion[0].startDate));
                $('#endDateEdit').datepicker({dateFormat: 'dd-mm-yy'});
                $('#endDateEdit').datepicker('setDate', $.datepicker.parseDate("yymmdd", promotion[0].endDate));
                $('#txtMinPriceApply').val(promotion[0].minPriceApply||'');
                switch (promotion[0].promotionType) {
                    case 1:
                        check = 'allUser';
                        break;
                    case 2:
                        check = 'newUser';
                        break;
                    case 3:
                        check = 'loyalUser';
                        break;
                    case 4:
                        check = 'agency';
                }
                $('input[name=promotionType]#' + check).attr('checked', 'checked');
                var arr = (Object.keys(promotion[0].subjects).toString()).split(',');
                var listRoute ={!! json_encode($listRoute)!!};
                html += '<optgroup label="Tuyến" data-view="1">';
                $.each(listRoute, function () {
                    html += '<option value="' + this.routeId + '">' + this.routeName + '</option>';
                });
                html += ' </optgroup>';
                $('#listApply').html('');
                $('#listApply').html(html);
                $('#listApply optgroup option').each(function () {
                    $(this).removeAttr('selected');
                });
                $.each(arr, function () {
                    $('#listApply optgroup option[value=' + this.toString() + ']').attr('selected', 'selected');

                });
                $('#listApply').hide();
                $('#listApply').show();
                $('#txtNote').val(promotion[0].note);
                $('#promotionId').val(promotion[0].promotionId);
                if(promotion[0].daysOfWeekApplied !=undefined){
                    var inputEdit = $('input.edit');
                    for (var i = 0; i < inputEdit.length; i++) {
                        for (var j = 0; j < (promotion[0].daysOfWeekApplied).length; j++) {
                            if (promotion[0].daysOfWeekApplied[j] == (i + 1)) {
                                $(inputEdit[i]).prop('checked', true);
                                $(inputEdit[i]).val(i + 1);
                            }
                        }
                    }
                }
            });
            $('#sl_donvi').change(function () {
                if ($(this).val() == 1) {
                    $('#txt_giatri').attr({'name': 'price'}).removeAttr('max');
                } else {
                    $('#txt_giatri').attr({'name': 'percent', 'max': 100});
                }
            });
            $('#donvi').change(function () {
                if ($(this).val() == 1) {
                    $('#editGiatri').attr({'name': 'price'}).removeAttr('max');
                } else {
                    $('#editGiatri').attr({'name': 'percent', 'max': 100});
                }
            });
            $('body').on('click', 'input.add , input.edit', function (e) {

                if ($(this).is(':checked')) {
                    var id = parseInt($(this).attr('id').substring(($(this).attr('id')).length - 1));
                    switch (id) {
                        case 2:
                            $(this).val(1);
                            break;
                        case 3:
                            $(this).val(2);
                            break;
                        case 4:
                            $(this).val(3);
                            break;
                        case 5:
                            $(this).val(4);
                            break;
                        case 6:
                            $(this).val(5);
                            break;
                        case 7:
                            $(this).val(6);
                            break;
                        default:
                            $(this).val(7);
                    }
                } else {
                    $(this).val('');
                }

            });
            jQuery(function ($) {
                $('.btn_delete').click(function () {
                    $('#delete_coupon').val($(this).val());
                });
                $("#sl_type").select2();
                $("#listApply").select2();
            });

        });
    </script>
@endsection
