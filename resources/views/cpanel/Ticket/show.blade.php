@extends('cpanel.template.layout')
@section('title', 'Danh sách vé')

@section('content')
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <ul class="hr_list stripe_list">
                        <li><h3><a {!! request('type')==1 || empty(request('type'))?'style="color: #19448a;font-weight: 600;"':'' !!} href="{{action('TicketController@show',['type'=>'1'])}}">Vé
                                    hành khách</a></h3></li>
                        <li><h3><a {!! request('type')==3?'style="color: #19448a;font-weight: 600;"':'' !!} href="{{action('TicketController@show',['type'=>'3'])}}">Vé gửi
                                    đồ</a></h3></li>
                    </ul>
                </div>
            </div>
        </div>
        @includeWhen(request('type')==1 || empty(request('type')), 'cpanel.Ticket.show.listTicket')

        @includeWhen(request('type')==3, 'cpanel.Ticket.show.listTicketGoods')
    </div>


    <!-- End Content -->
@endsection

