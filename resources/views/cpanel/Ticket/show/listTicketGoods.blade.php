<div class="khung_lich_ve">
    <div class="date_picker">
        <div style="padding: 7px;" class="widget widget-4">
            <div class="widget-head">
                <h4>Tìm vé</h4>
            </div>
        </div>
        <div class="innerLR">
            <form action="">
                <div class="row-fluid">
                    <div class="span2">
                        <label for="ticketId">Mã vé</label>
                        <input autocomplete="off" class="w_full" type="text" name="ticketCode" id="ticketId" value="{{request('ticketId')}}">
                    </div>
                    <div class="span2">
                        <label for="phoneNumberReceiver">SĐT người nhận</label>
                        <input autocomplete="off" class="w_full" type="text" name="phoneNumberReceiver" id="phoneNumberReceiver"
                               value="{{request('phoneNumberReceiver')}}">
                    </div>
                    <div class="span3">
                        <label for="txtDate">Thời gian</label>
                        <div class="input-append">
                            <input autocomplete="off" type="text" name="date" id="txtDate" value="{{request('date')}}">
                        </div>
                    </div>
                    <div class="span2">
                        <label for="numberPlate">Biển số xe</label>
                        <input autocomplete="off" class="w_full" type="text" name="numberPlate" id="numberPlate" value="{{request('numberPlate')}}">
                    </div>
                    <div class="span2">
                        <label>&nbsp;</label>
                        <input type="hidden" name="type" value="3">
                        <button class="btn btn-info btn-flat-full" id="search">TÌM VÉ</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row-fluid bg_light m_top_10">
    <div class="widget widget-4 bg_light">
        <div class="widget-body">
           
            <table class="table table-hover table-vertical-center">
                <thead>
                <tr>
                    <th>Biển số</th>
                    <th>Mã vé</th>
                    <th>Họ tên người gửi</th>
                    <th>Họ tên người nhận</th>
                    <th>SĐT người nhận</th>
                    <th>Thời gian đi</th>
                    <th>Thời gian đến</th>
                    <th>Tùy chọn</th>
                </tr>
                </thead>
                <tbody>

                @foreach($result as $row)
                    <tr>
                        <td>{{@$row['numberPlate']}}</td>
                        <td>{{$row['ticketCode']}}</td>
                        <td>{{$row['fullName']}}</td>
                        <td>{{@$row['fullNameReceiver']}}</td>
                        <td>{{$row['phoneNumberReceiver']}}</td>
                        <td>@dateTime($row['getInTimePlan']/1000)</td>
                        <td>@dateTime($row['getOffTimePlan']/1000)</td>
                        <td>
                            <a tabindex="0"
                               class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                <ul class="onclick-menu-content">
                                    <li>
                                        <button type="button" onclick="showInfo(this)">Chi tiết</button>
                                    </li>

                                    <li>
                                        <button value="{{$row['ticketId']}}" id="editTicket">Sửa vé</button>
                                    </li>

                                    <li>
                                        <button value="{{$row['ticketId']}}" id="cancelTicket">Hủy vé</button>
                                    </li>

                                    <li>
                                        <button value="{{$row['ticketId']}}" id="orderTicket">Thanh toán</button>
                                    </li>
                                </ul>
                            </a>
                        </td>
                    </tr>
                    <!--hiển thị chi tiết-->
                    <tr class="info" style="display: none">
                        <td colspan="7">
                            <table class="tttk" cellspacing="20" cellpadding="4">
                                <tbody>
                                <tr>
                                    <td><b>TÊN NGƯỜI NHẬN</b></td>
                                    <td>{{array_get($row,'fullNameReceiver','Chưa xác định')}}</td>
                                    <td class="right"><b>TÊN SẢN PHẨM</b></td>
                                    <td class="left">{{$row['ticketName']}}</td>
                                </tr>
                                <tr>
                                    <td><b>SĐT NGƯỜI NHẬN</b></td>
                                    <td>{{array_get($row,'phoneNumberReceiver','Chưa xác định')}}</td>
                                    <td class="right"><b>TRỌNG LƯỢNG</b></td>
                                    <td class="left">{{$row['weight']}}Kg</td>
                                </tr>
                                <tr>
                                    <td><b>ĐỊA CHỈ NHẬN HÀNG</b></td>
                                    <td>{{array_get($row,'pickUpAddress','Chưa xác định')}}</td>
                                    <td class="right"><b>KÍCH THƯỚC</b></td>
                                    <td>{{$row['dimension']}}</td>
                                </tr>
                                <tr>
                                    <td><b>ĐỊA CHỈ TRẢ HÀNG</b></td>
                                    <td>{{array_get($row,'dropOffAddress','Chưa xác định')}}</td>
                                    <td class="right"><b>MÃ KHUYẾN MÃI</b></td>
                                    <td>{{$row['promotionId']}}</td>
                                </tr>
                                <tr>
                                    <td><b>THỜI GIAN ĐI</b></td>
                                    <td>@dateTime($row['getInTimePlan']/1000)</td>
                                    <td class="right"><b>GIÁ VÉ</b></td>
                                    <td>@moneyFormat($row['paymentTicketPrice'])</td>
                                </tr>
                                <tr>
                                    <td><b>THỜI GIAN ĐẾN</b></td>
                                    <td>@dateTime($row['getOffTimePlan']/1000)</td>
                                    <td class="right"><b>HÌNH ẢNH SẢN PHẨM</b></td>
                                    <td class="relative">
                                        <div class="khung_hinh_thongtinsp khung_absolute">
                                            <img src="{{head($row['listImages'])}}">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>TÊN NGƯỜI GỬI</b></td>
                                    <td colspan="6">{{$row['fullName']}}</td>
                                </tr>
                                <tr>
                                    <td><b>SĐT NGƯỜI GỬI</b></td>
                                    <td colspan="6">{{$row['phoneNumber']}}</td>
                                </tr>
                                <tr>
                                    <td><b>EMAIL</b></td>
                                    <td colspan="6"></td>
                                </tr>

                                <tr>
                                    <td><b>Trạng thái</b></td>
                                    <td colspan="6">@checkTicketStatus($row['ticketStatus'])</td>
                                </tr>
                                <tr>
                                    <td style="width:25%">&nbsp;</td>
                                    <td style="width:25%;padding-left:10px">&nbsp;</td>
                                    <td style="width:25%;text-align:right"><b>&nbsp;</b></td>
                                    <td style="width:25%;text-align:left">&nbsp;</td>
                                </tr>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @include('cpanel.template.pagination-without-number',['page'=>$page])
        </div>
    </div>
</div>

<div class="modal modal-custom hide fade" id="modal_updateTicket" style="width: 400px; margin-left: -10%;margin-top: 10%;">
    <form action="{{action('TicketController@updateStatusTicket')}}" method="post">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="title-update-ticket">CẬP NHẬT THÔNG TIN VÉ</h3>
        </div>
        <div class="modal-body center">
            <p id="lb_message"></p>
            <div id="frmUpdateInfo" class="row-fluid">
                <div class="form-horizontal row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtPhoneNumber">SĐT người gửi</label>
                        <div class="controls">
                            <input autocomplete="off"
                                   data-validation="custom"
                                   data-validation-regexp="^0(([8-9][0-9]{8})|(1[0-9]{9}))$"
                                   data-validation-error-msg-custom="Vui lòng nhập đúng định dạng SĐT"
                                   class="span12" type="number" name="phoneNumber" id="txtPhoneNumber">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="txtFullName">Họ tên người gửi</label>
                        <div class="controls">
                            <input autocomplete="off"
                                   class="span12" type="text" name="fullName" id="txtFullName">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="txtPhoneNumberReceiver">SĐT người nhận</label>
                        <div class="controls">
                            <input autocomplete="off"
                                   data-validation="custom"
                                   data-validation-regexp="^0(([8-9][0-9]{8})|(1[0-9]{9}))$"
                                   data-validation-error-msg-custom="Vui lòng nhập đúng định dạng SĐT"
                                   class="span12" type="number" name="phoneNumberReceiver" id="txtPhoneNumberReceiver">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="txtFullNameReceiver">Họ tên người nhận</label>
                        <div class="controls">
                            <input autocomplete="off"
                                   class="span12" type="text" name="fullNameReceiver" id="txtFullNameReceiver">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="modal-footer">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="ticketId" value="" id="txtTicketId">
                <input type="hidden" name="option" value="" id="txtOption">
                <a style="cursor: pointer" data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button id="btnUpdateStatusTicket" class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
            </div>

        </div>
    </form>
</div>
<script>
    $('body').on('click', 'button#cancelTicket,button#orderTicket,button#editTicket', function (e) {
        if ($(e.currentTarget).is('#orderTicket')) {
            $('#lb_message').text('Bạn có muốn thanh toán vé này?').show();
            $('#title-update-ticket').text('THANH TOÁN VÉ');
            $('#frmUpdateInfo').hide();
            $('#txtOption').val(1)
        }

        if ($(e.currentTarget).is('#cancelTicket')) {
            $('#lb_message').text('Bạn có chắc muốn huỷ vé này?').show();
            $('#title-update-ticket').text('HUỶ VÉ');
            $('#frmUpdateInfo').hide();
            $('#txtOption').val(2)
        }

        if ($(e.currentTarget).is('#editTicket')) {
            $('#lb_message').text('').hide();
            $('#title-update-ticket').text('CẬP NHẬT THÔNG TIN VÉ');
            $('#frmUpdateInfo').show();
            $('#txtOption').val(3)
        }

        $('#txtTicketId').val($(this).val());
        $('#modal_updateTicket').modal('show');
    })
</script>