<style>
    .status:hover {
        color: #000;
    }

    .status3 {
        background: #ed1b24;
        color: #fff
    }

    .status2, status7 {
        background: #ffff00;
    }

    .status4 {
        background: #942a25;
        color: #fff
    }
</style>
<div class="khung_lich_ve">
    <div class="date_picker">
        <div style="padding: 7px;" class="widget widget-4">
            <div class="widget-head">
                <h4>Tìm vé</h4>
            </div>
        </div>
        <div class="innerLR">
            <form action="">
                <div class="row-fluid">
                    <div class="span2">
                        <label>Mã vé</label>
                        <input autocomplete="off" class="w_full" type="text" name="ticketCode" id="ticketId"
                               value="{{request('ticketCode')}}">
                    </div>
                    <div class="span2">
                        <label>Số điện thoại</label>
                        <input autocomplete="off" class="w_full" type="text" name="phoneNumber" id="phoneNumber"
                               value="{{request('phoneNumber')}}">
                    </div>
                    <div class="span3">
                        <label for="txtDate">Thời gian</label>
                        <div class="input-append">
                            <input autocomplete="off" type="text" name="date" id="txtDate" value="{{request('date')}}">
                        </div>
                    </div>

                </div>
                <div class="row-fluid">
                    <div class="span2">
                        <label for="numberPlate">Biển số xe</label>
                        <input autocomplete="off" class="w_full" type="text" name="numberPlate" id="numberPlate"
                               value="{{request('numberPlate')}}">
                    </div>
                    <div class="span3">
                        <label for="txtTrangThai">Trạng thái vé</label>
                        <select name="status">
                            <option value>Tất cả</option>
                            @foreach($ticketStatus as $key => $value)
                                <option value="{{ $key }}" {{(request("status")==$key ? "selected" : "")}}>{{ $value }}</option>

                            @endforeach
                        </select>
                    </div>
                    <div class="span2">
                        <label>&nbsp;</label>
                        <input type="hidden" name="type" value="1">
                        <button class="btn btn-info btn-flat-full" id="search">TÌM VÉ</button>
                    </div>
                    <div class="center ghichu_datghe" style="margin-left: 60px;">
                        <div class="gc_ghe gc_giucho"></div>
                        <div class="title_ghichu_datghe">Đang giữ chỗ</div>
                        <br>
                        <div class="gc_ghe gc_dadat"></div>
                        <div class="title_ghichu_datghe">Đã thanh toán</div>
                        <br>
                        <div class="gc_ghe status4"></div>
                        <div class="title_ghichu_datghe">Đã lên xe</div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row-fluid bg_light m_top_10">
    <div class="widget widget-4 bg_light">
        <div class="widget-body">

            <table class="table table-hover table-vertical-center">
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Biển số</th>
                    <th>Số ghế</th>
                    <th>Mã vé</th>
                    <th>Họ tên</th>
                    <th>Số điện thoại</th>
                    <th>Tuyến</th>
                    <th>Thời gian đi</th>
                    <th>Thời gian đến</th>
                    <th>Tùy chọn</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $stt = ($page - 1) * RECORD_PER_PAGE;
                ?>
                @foreach($result as $row)
                    <?php $stt++ ?>
                    <tr class="status status{{ $row['ticketStatus'] }}">
                        <td>{{ $stt }}</td>
                        <td>{{@$row['numberPlate']}}</td>
                        <td>{{count($row['listSeatId'])}}</td>
                        <td>{{$row['ticketCode']}}</td>
                        <td>{{$row['fullName']}}</td>
                        <td>{{$row['phoneNumber']}}</td>
                        <td>{{$row['routeName']}}</td>
                        <td>@dateTime(($row['getInTimePlan']+7*60*60*1000))</td>
                        <td>
                            @if($row['getOffTimePlan'] !=0)
                                @dateTime(($row['getOffTimePlan']+7*60*60*1000))
                            @endif
                        </td>
                        <td>
                            <a tabindex="0"
                               class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                <ul class="onclick-menu-content">
                                    <li>
                                        <button type="button" onclick="showInfo(this)">Chi tiết</button>
                                    </li>

                                    <li>
                                        <button value="{{$row['ticketId']}}" id="editTicket">Sửa vé</button>
                                    </li>

                                    <li>
                                        <button value="{{$row['ticketId']}}" id="cancelTicket">Hủy vé</button>
                                    </li>
                                    @if($row['ticketStatus']==2||$row['ticketStatus']==7)
                                        <li>
                                            <button value="{{$row['ticketId']}}" id="orderTicket">Thanh toán</button>
                                        </li>
                                    @endif
                                </ul>
                            </a>
                        </td>
                    </tr>
                    <!--hiển thị chi tiết-->
                    <tr class="info" style="display: none">
                        <td colspan="8">
                            <table class="tttk" cellspacing="20" cellpadding="4">
                                <tbody>
                                <tr>
                                    <td><b>TUYẾN</b></td>
                                    <td>{{$row['routeName']}}</td>
                                    <td class="right"><b>VÉ NGƯỜI LỚN</b></td>
                                    <td class="left">{{$row['numberOfAdults']}}</td>
                                </tr>
                                <tr>
                                    <td><b>BẾN LÊN</b></td>
                                    <td>{{array_get($row,'pickUpAddress','Chưa xác định')}}</td>
                                    <td class="right"><b>VÉ TRẺ EM</b></td>
                                    <td class="left">{{$row['numberOfChildren']}}</td>
                                </tr>
                                <tr>
                                    <td><b>BẾN XUỐNG</b></td>
                                    <td>{{array_get($row,'dropOffAddress','Chưa xác định')}}</td>
                                    <td class="right"><b>GIÁ VÉ</b></td>
                                    <td>@moneyFormat($row['paymentTicketPrice'])</td>
                                </tr>
                                <tr>
                                    <td><b>THỜI GIAN ĐI</b></td>
                                    <td>@dateTime(($row['getInTimePlan']+7*60*60*1000))</td>
                                    <td class="right"><b>MÃ KM</b></td>
                                    <td class="right">{{$row['promotionId']}}</td>
                                </tr>

                                <tr>
                                    <td><b>THỜI GIAN ĐẾN</b></td>
                                    <td>@dateTime(($row['getOffTimePlan']+7*60*60*1000))</td>
                                    <td class="right"><b>TRẠNG THÁI VÉ</b></td>
                                    <td class="right">@checkTicketStatus($row['ticketStatus'])</td>
                                </tr>
                                <tr>
                                    <td><b>BIỂN SỐ</b></td>
                                    <td>{{$row['numberPlate']}}</td>
                                    <td class="right"><b>ĐÃ BAO GỒM ĂN</b></td>
                                    <td class="right">{{$row['mealPrice']!=-1?$row['mealPrice']:''}}</td>
                                </tr>
                                <tr>
                                    <td><b>HỌ TÊN</b></td>
                                    <td>{{$row['fullName']}}</td>
                                    <td class="right"><b>SỐ GHẾ</b></td>
                                    <td class="right">{{implode(',',$row['listSeatId'])}}</td>
                                </tr>
                                <tr>
                                    <td style="width:25%">&nbsp;</td>
                                    <td style="width:25%;padding-left:10px">&nbsp;</td>

                                    <td style="width:25%;text-align:right"><b>&nbsp;</b></td>
                                    <td style="width:25%;text-align:left">&nbsp;</td>
                                </tr>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>


            @include('cpanel.template.pagination-without-number',['page'=>$page])
        </div>
        <br>

    </div>
</div>

<div class="modal modal-custom hide fade" id="modal_updateTicket"
     style="width: 400px; margin-left: -10%;margin-top: 10%;">
    <form action="{{action('TicketController@updateStatusTicket')}}" method="post">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="title-update-ticket">CẬP NHẬT THÔNG TIN VÉ</h3>
        </div>
        <div class="modal-body center">
            <p id="lb_message"></p>
            <div id="frmUpdateInfo" class="row-fluid">
                <div class="form-horizontal row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtPhoneNumber">Số điện thoại</label>
                        <div class="controls">
                            <input autocomplete="off"
                                   data-validation="custom"
                                   data-validation-regexp="^0(([8-9][0-9]{8})|(1[0-9]{9}))$"
                                   data-validation-error-msg-custom="Vui lòng nhập đúng định dạng SĐT"
                                   class="span12" type="number" name="phoneNumber" id="txtPhoneNumber">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="txtFullName">Họ tên</label>
                        <div class="controls">
                            <input autocomplete="off"
                                   class="span12" type="text" name="fullName" id="txtFullName">
                        </div>
                    </div>
                </div>
            </div>
            <div id="checksendmsg"></div>
        </div>
        <div class="modal-footer">
            <div class="modal-footer">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="ticketId" value="" id="txtTicketId">
                <input type="hidden" name="option" value="" id="txtOption">
                <a style="cursor: pointer" data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button id="btnUpdateStatusTicket" class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
            </div>

        </div>
    </form>
</div>
<script>
    $('body').on('click', 'button#cancelTicket,button#orderTicket,button#editTicket', function (e) {
        if ($(e.currentTarget).is('#orderTicket')) {
            $('#lb_message').text('Bạn có muốn thanh toán vé này?').show();
            var html = '';
            html += '<br><div class="control-group">' + '<label class="control-label span9"  style="float:right" for="sendSMS"> Gửi tin nhắn cho khách hàng.</label>' + '<div class="controls span3" style="float:right;">' + '<input type="checkbox" id="sendSMS" name="sendSMS">' + '<label for="sendSMS" style="float:right;"></label>' + ' </div>' + '</div>';
            $('#title-update-ticket').html('THANH TOÁN VÉ');
            $('#checksendmsg').html(html);
            $('#frmUpdateInfo').hide();
            $('#txtOption').val(1)
        }

        if ($(e.currentTarget).is('#cancelTicket')) {
            $('#lb_message').text('Bạn có chắc muốn huỷ vé này?').show();
            $('#title-update-ticket').text('HUỶ VÉ');
            $('#frmUpdateInfo').hide();
            $('#txtOption').val(2);
            $('#checksendmsg').html('');
        }

        if ($(e.currentTarget).is('#editTicket')) {
            $('#lb_message').text('').hide();
            $('#title-update-ticket').text('CẬP NHẬT THÔNG TIN VÉ');
            $('#frmUpdateInfo').show();
            $('#checksendmsg').html('');
            $('#txtOption').val(3)
        }

        $('#txtTicketId').val($(this).val());
        $('#modal_updateTicket').modal('show');
    })
</script>