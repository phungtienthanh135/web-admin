<div class="khung_khach_hang" style="display: none">
    <div class="innerLR">

        <div class="print_content row-fluid" id="printDiv">
            <div class="span12" style="padding-top:20px;">
                <div class="span1">
                    <a class="btn print colorprint" onclick="printDiv()" style="cursor: pointer;width: 60px;"><img
                                src="/public/images/printer2.jpg"/></a>
                </div>

                <div class="span2">Chọn loại sơ đồ để in:</div>
                <div class="span2">
                    <label class="radio">
                        <input id="cbList" class="check" checked="checked" type="radio" name="radio" value="2"
                               onclick="showTable(this)">
                        Danh sách hành khách
                    </label>
                </div>
                @include('cpanel.Ticket.constant')
                @if(in_array(session('companyId'), LIST_CUSTOMERS_DROP_COMPANIES))
                    <div class="span3">
                        <label class="radio">
                            <input id="cbListPick" class="check" type="radio" name="radio" value="3"
                                   onclick="showTable(this)">
                            Danh sách trung chuyển đón
                        </label>
                    </div>
                    <div class="span3">
                        <label class="radio">
                            <input id="cbListDrop" class="check" type="radio" name="radio" value="4"
                                   onclick="showTable(this)">
                            Danh sách trung chuyển trả
                        </label>
                    </div>
                @else
                    <div class="span3">
                        <label class="radio">
                            <input id="cbListPick" class="check" type="radio" name="radio" value="3"
                                   onclick="showTable(this)">
                            Danh sách trung chuyển
                        </label>
                    </div>
                @endif
                <div class="span1">
                    <label class="radio">
                        <input id="cbMap" class="check" type="radio" name="radio" value="1" onclick="showTable(this)">
                        Sơ đồ ghế
                    </label>
                </div>
            </div>
            @include('cpanel.Ticket.sell.sell-ver1.listAllCustomer')

            @include('cpanel.Ticket.sell.sell-ver1.listPickupCustomer')

            @include('cpanel.Ticket.constant')
            @if(in_array(session('companyId'), LIST_CUSTOMERS_DROP_COMPANIES))
                @include('cpanel.Ticket.sell.sell-ver1.listDropOffCustomer')
            @endif
            <div id="sodo" style="display: none">
                <h4 align="center" id="title-table">SƠ ĐỒ GHẾ</h4>
                <div id="panel-sell">
                    <div class="widget-body">
                        <div class="tab-content">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <div style="width: 100%;height: 50px">
                                        <div style="float: left;width: 9%;height:100%"></div>
                                        <div style="text-align:left;float: left;width:10%;height:100%">Chuyến : <br>Biển số xe : <br>Giờ chạy
                                            : <br></div>
                                        <div style="float: left;width:17%">
                                            <span style="font-weight: bold;" class="chuyen"></span><br>
                                            <span style="font-weight: bold;" class="bienso"></span><br>
                                            <span class="startTime" style="font-weight: bold"></span>
                                        </div>
                                        <div style="float: left;width:35%;height:100%"></div>
                                        <div style="float: left;text-align: left;width:9%">Lái xe : <br>Phụ xe : <br>Tổng ghế :
                                        </div>
                                        <div  style="float: left;width:20%">
                                            <span style="font-weight: bold; " class="laixe"></span><br>
                                            <span class="phuxe" style="font-weight: bold;"></span><br>
                                            <span class="totalSeat" style="font-weight: bold"></span>
                                        </div>
                                    </div>
                                </tr>
                                </thead>
                            </table>

                            <div id="makeMap">
                            </div>
                        </div>
                    </div>
                </div>
                <br><br>
            </div>
        </div>
    </div>
    <div class="innerLR">

        <div class="row-fuild"
             style="width: <?php if(session('companyId') == 'TC03013FPyeySDW') echo "90"; else echo "90";?>%; margin: auto;">
            <div class="listTicketCancel"></div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var d = new Date();
        var date = d.getDate() + '-' + (parseInt(d.getMonth()) + 1) + '-' + d.getFullYear();
        $(".export-btn a#excel").click(function () {
            var filename = $(this).attr('data-fileName');
            $(".table-condensed").table2excel({
                exclude: ".noExl",
                name: "Excel Document Name",
                filename: filename + '_' + date,
                fileext: ".xls",
                exclude_img: true,
                exclude_links: true,
                exclude_inputs: true
            });
        });
    });

    function printDiv() {
        var chk = $('.check:checked').val();
        if (chk == 1) {
            // $('h4#title-table').hide();
            $("#sodo").printThis({
                pageTitle: '&nbsp;'
            });
        } else {
            if (chk == 2) {
                $('#excel').hide();
                $("#dskhach").printThis({
                    pageTitle: '&nbsp;'
                });
            } else if (chk == 3) {
                $(".trsortpick").hide();
                $("#dskhachdon").printThis({
                    pageTitle: '&nbsp;'
                });
            } else {
                $(".trsortdrop").hide();
                $("#dskhachtra").printThis({
                    pageTitle: '&nbsp;'
                });
            }
        }
        // $(".trsortpick").show();
        // $(".trsortdrop").show();

    }

    function showTable(that) {
        if ($(that).prop('id') == 'cbMap') {
            $('#dskhach').hide();
            $('#dskhachdon').hide();
            $('#sodo').show();
            $('#dskhachtra').hide();
        }
        if ($(that).prop('id') == 'cbList') {
            $('#dskhach').show();
            $('#dskhachdon').hide();
            $('#sodo').hide();
            $('#dskhachtra').hide();
        }
        if ($(that).prop('id') == 'cbListPick') {
            $('#dskhach').hide();
            $('#dskhachdon').show();
            $('#sodo').hide();
            $('#dskhachtra').hide();
        }
        if ($(that).prop('id') == 'cbListDrop') {
            $('#dskhach').hide();
            $('#dskhachdon').hide();
            $('#dskhachtra').show();
            $('#sodo').hide();
        }
    }
</script>