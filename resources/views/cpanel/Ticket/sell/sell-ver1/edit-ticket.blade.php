<div class="modal modal-custom hide fade" id="modal_transfer">
    <form action="{{action('TripController@postTransfer')}}" method="post">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>CHUYỂN VÉ</h3>
        </div>
        <div class="modal-body" style="overflow-y: visible;">
            <div class="row-fluid">
                <div class="form-horizontal row-fluid">
                    <!-- <div class="control-group">
                        <label class="control-label" for="txtListSeatSourceId">Ghế ban đầu</label>
                        <div class="controls">
                            <input class="span12" readonly type="text" name="listSeatSourceId"
                                   id="txtListSeatSourceId">
                        </div>
                    </div> -->

                    <div class="control-group">
                        <label class="control-label" for="cbbRouteDestinationId">Tuyến cần chuyển</label>
                        <div class="controls">
                            <select class="span12" id="cbbRouteDestinationId">
                                @foreach($listRoute as $route)
                                    <option data-startPoint="{{$route['listPoint'][0]['pointId']}}"
                                            data-endPoint="{{last($route['listPoint'])['pointId']}}"
                                            {{request('routeId')==$route['routeId']?'selected':''}}
                                            value="{{$route['routeId']}}">{{$route['routeName']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="txtCalendarDestination">Thời gian cần chuyển</label>
                        <div class="controls">
                            <div class="input-append span12">
                                <input value="{{date('d-m-Y')}}" name="timeChange" readonly class="span11" autocomplete="off" type="text"
                                       id="txtCalendarDestination">
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="cbbTripDestinationId">Chuyến cần chuyển</label>
                        <div class="controls">
                            <select name="tripDestinationId" id="cbbTripDestinationId" class="span12">
                                <option value="">Chuyến</option>
                            </select>
                        </div>
                    </div>
                    <div id="chuyenghe">

                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="modal-footer">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="tripSourceId" value="" id="txtTripSourceId">
                <input type="hidden" name="numberOfGuests" value="1" id="txtNumberOfGuests">
                {{--<input type="hidden" name="timeChange" value="1" id="txtTimeChange">--}}
                <input type="hidden" name="scheduleId" id="txtScheduleId">
                <input type="hidden" name="place" value="1" id="txtPlaceChange">
                <a style="cursor: pointer" data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button onclick="return postTranfer();" type="submit" id="btnChangeSeat"
                        class="btn btn-primary">ĐỒNG Ý
                </button>
            </div>

        </div>
    </form>
</div>


@push('javascrip-sell-ticket')
    <script>
        var listTripDestination = [],//biến toàn cục dùng lưu ds chuyến cần chuyển khi đã chọn lịch
            selectBoxRouteDestination = $('#cbbRouteDestinationId'),//selectbox chọn tuyến cần chuyển
            selectBoxListTripDestination = $('#cbbTripDestinationId'),//selectbox chọn chuyến cần chuyển
            datepickerBoxDestination = $("#txtCalendarDestination"),//lịch cần chuyển
            urlSeachTrip = '{{action('TripController@search')}}',//url tìm lịch cho khách
            listRoute = {!! json_encode($listRoute,JSON_PRETTY_PRINT) !!};//thông tin tuyến dùng để lấy ds điểm dừng
//            console.log(listRoute);
        var oldRouteId;
        var oldTripId;

        (function () {

            datepickerBoxDestination.datepicker({
                minDate: 0,
                dateFormat: "dd-mm-yy",
                onSelect: function () {
                    updateSelectBoxTripDestination();
                },
                showOn: 'both',
                buttonText: "<i class=\"icon-calendar\"></i>",
                numberOfMonths: 2
            });
            $(document).ready(function () {
                $(selectBoxRouteDestination).change(function () {
                    updateSelectBoxTripDestination();
                });
                $(selectBoxListTripDestination).change(function () {
                    transferTo($(this).val(), listTripDestination);
                });
                $('body').on('click', '.btnTransferTicket', function () {
                    oldRouteId = $('#select2-chontuyen-container').text();
                    oldTripId = $('#select2-listTrip-container').text();
                    if ($(this).parents('.ghe').hasClass('ghedangchon')) {
                        htmlapp = '';
                        $('.ghedangchon').each(function () {
                            var id = $(this).find('.seatId').html();
                            htmlapp += '<div class="control-group OneMoveSeat"><label class="control-label" for="cbbSeatIdDestinationId">Ghế cần chuyển</label><div class="controls"><input value="' + id + '" type="hidden" class="listSeatOld" name="listSeatSourceId[]"><input type="hidden" class="changeValueNewSeat cbbSeatIdDestinationId listSeatNew" name="listSeatDestinationId[]" value=""><a disabled class="btn span3 ">' + id + '</a><span class="span1">-->></span><div class="selectseattable span5"></div></div></div>';
                        });
                        $('#chuyenghe').html(htmlapp);
                        $(datepickerBoxDestination).val($(datepickerBox).val());
                        $(selectBoxRouteDestination).val($(selectBoxRoute).val()).change();
                        $('#modal_transfer').modal("show");
                        return false;
                    }
                });
                $('body').on('click', '.canmove', function () {
                    $(this).parents('.OneMoveSeat').find('.changeValueNewSeat').val($(this).text());
                    $(this).parents('.OneMoveSeat').find('.btnchooseSeatMove').text($(this).text());
                });
            });
        })();

        function updateSelectBoxTripDestination() {
            var routeId = $(selectBoxRouteDestination).val();
            var date = $(datepickerBoxDestination).val();
            /*
            * Lấy dữ liệu của tuyến
            * */
            var route = $.grep(listRoute, function (item) {
                return item.routeId == routeId;
            });
            /*Lấy điểm đầu và điểm cuối*/
            var startPointId = route[0].listPoint[0].pointId;
            var endPointId = route[0].listPoint[route[0].listPoint.length - 1].pointId;

            $(selectBoxListTripDestination).html('<option value="">Đang tải dữ liệu...</option>');
            $(".cbbSeatIdDestinationId").prop('disabled', true).html('');
            $.ajax({
                dataType: "json",
                url: urlSeachTrip,
                data: {
                    'routeId': routeId,
                    'date': date,
                    'startPointId': startPointId,
                    'endPointId': endPointId
                },
                success: function (data) {
                    var html = '';
                    listTripDestination = data;
                    data.forEach(function (trip) {
                        var totalSeat = [], seatBooked = [];
                        if (trip.seatMap != null) {
                            totalSeat = $.grep(trip.seatMap.seatList, function (seat) {
                                return seat.seatType == 3 || seat.seatType == 4;
                            });

                            seatBooked = $.grep(totalSeat, function (seat) {
                                return seat.seatStatus == 3 || (seat.seatStatus == 2 && (seat.overTime > Date.now() || seat.overTime == 0));
                            });
                        }
                        var isSelected = tripSelected.tripId === trip.tripId ? 'selected' : '';
                        html += '<option ' + isSelected + ' data-schedule="' + trip.scheduleId + '" value="' + trip.tripId + '">' +
                            trip.numberPlate + ' - ' +
                            getFormattedDate(trip.getInTime, 'time') +
                            '         (Trống: ' + ($(totalSeat).length - $(seatBooked).length) + ')' +
                            '</option>';
                    });
                    if (data.length >= 1) {
                        $(selectBoxListTripDestination).html(html);
                        $(".cbbSeatIdDestinationId").prop('disabled', false);
                    } else {
                        $(selectBoxListTripDestination).html('<option value="">Hiện tại không có chuyến</option>');
                        $('.btnchooseSeatMove').prop('disabled', true);
                    }

                    transferTo($(selectBoxListTripDestination).val(), listTripDestination);
                },
                error: function () {
                    Message("Lỗi!", "Vui lòng tải lại trang", "");
                }
            })
        }

        function transferTo(_tripDestinationId, _listTripDestination) {
            var tripDestinationId = _tripDestinationId;
            var TripData = $.grep(_listTripDestination, function (tripDestination) {
                return tripDestination.tripId === tripDestinationId;
            })[0] || [];

            //set input dataschedule
            $('#txtScheduleId').val($('#cbbTripDestinationId option:selected').data('schedule'));

            var html = '<div class="dropdown dropup"><a data-toggle="dropdown" href="#" class="btn btnchooseSeatMove">Chọn Ghế ... </a><ul class="dropdown-menu" role="menu" aria-labelledby="dLabel"><div style="padding: 2px;height: 200px;width:300px;overflow: scroll;">';
            for (var i = 1; i <= TripData.seatMap.numberOfFloors; i++) {
                html += '<p style="font-weight:bold;">Tầng ' + i + '</p>';
                html += '<table class="seatmaptable">';
                for (var r = 1; r <= TripData.seatMap.numberOfRows; r++) {
                    html += '<tr>';
                    for (var c = 1; c <= TripData.seatMap.numberOfColumns; c++) {
                        var elm = $.grep(TripData.seatMap.seatList, function (seat) {
                            return (seat.floor == i && seat.row == r && seat.column == c);
                        });
                        if (typeof elm[0] === "undefined") {
                            html += '<td></td>';
                        } else {
                            if (checkStatusSeatAndTicket(elm[0].seatStatus, elm[0].overTime).seatStatus === 'ghetrong' &&
                                (elm[0].seatType === 3 || elm[0].seatType === 4)) {
                                html += '<td style="border:1px solid #ddd;" class="canmove">' + elm[0].seatId + '</td>';
                            } else {
                                html += '<td style="border:1px solid #ddd;" class="cantmove">' + elm[0].seatId + '</td>';
                            }
                        }
                    }
                    html += '</tr>';
                }

                html += '</table><hr>';
            }
            html += '</div></ul></div></div>';

            $(".selectseattable").html(html);
            // $(".cbbSeatIdDestinationId").select2({
            //     dropdownCss: {'z-index': '999999'},
            // });
            $("#txtTripSourceId").val(tripSelected.tripData.tripId);
            $("#txtTimeChange").val(TripData.getInTime || '');
            $("#txtPlaceChange").val(TripData.getInPointId || '');
        }

        function postTranfer() {
            var getInPoint = $('#cbb_InPoint').val();
            var getOffPoint = $('#cbb_OffPoint').val();
            var listSeatOld = [],
                listSeatNew = [];
            var flag = {};
            $('.listSeatOld').each(function (i, e) {
                listSeatOld.push($(e).val());
            });
            $('.listSeatNew').each(function (i, e) {
                listSeatNew.push($(e).val());
            });
            var stt = $.grep(listSeatNew, function (i, e) {
                return i != '';
            });
            if (stt.length == 0) {
                Message("Lỗi", "Vui lòng chọn ghế mới", '');
                return false;
            }
            stt.sort();
            for (var j = 0; j < stt.length; j++) {
                if (stt[j] == stt[j + 1]) {
                    Message("Lỗi !", "Các ghế mới không được trùng nhau", '');
                    return false;
                }
            }
            if ($('.cbbSeatIdDestinationId').attr('disabled')) {
                return false;
            }
            $.ajax({
                url: '{{ action("TripController@postTransfer") }}',
                type: "POST",
                dataType: 'json',
                data: {
                    tripSourceId: $("[name='tripSourceId']").val(),
                    tripDestinationId: $("[name='tripDestinationId']").val(),
                    numberOfGuests: $("[name='numberOfGuests']").val(),
                    timeChange: $("[name='timeChange']").val(),
                    place: $("[name='place']").val(),
                    scheduleId: $("#txtScheduleId").val(),
                    listSeatSourceId: listSeatOld.toString(),
                    listSeatDestinationId: listSeatNew.toString(),
                    destinationInPointId: getInPoint,
                    destinationOffPointId: getOffPoint,
                    _token: '{{ csrf_token() }}'
                },
                success: function (result) {
                    $('#modal_transfer').modal('hide');
                    if (result.result.code == 200) {
                        Message("Thông Báo!", "Gửi yêu cầu thành công! vui lòng đợi...", "");
                    } else {
                        Message("Lỗi!", "Có lỗi xảy ra khi gửi yêu cầu. Vui lòng tải lại trang!", "");
                    }

                },
                error: function (err) {
                    Message("Lỗi!", "Gửi yêu cầu thất bại !", "");
                }
            });
            return false;
        }
    </script>
@endpush
