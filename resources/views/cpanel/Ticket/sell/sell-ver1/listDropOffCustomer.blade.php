<div id="dskhachtra" style="display: none">
    <h3 align="center" id="title-table-dropOff">DANH SÁCH TRUNG CHUYỂN TRẢ</h3>
    <div align="center" class="info_trip"></div>

    <div id="panel-sell">
        <div class="widget-body">
            <div class="tab-content">
                <style>
                    .padding {
                        padding-top: 0px !important;
                        border: 1px solid;
                    }
                </style>
                <table class="" style="    margin-top: 25px;width: 100%;font-family: 'Verdana'; font-size: 12px" id="table-list-dropOff">
                    @php
                        $col1 = 1;
                        $col2 = 2;
                        /*if(session('companyId') != 'TC03013FPyeySDW')
                        {
                            $col1 = 8;
                            $col2 = 4;
                        }*/
                    @endphp
                    <thead>
                    <tr>
                        <div style="width: 100%;height: 50px">
                            <div style="float: left;width: 9%;height:100%"></div>
                            <div style="text-align:left;float: left;width:10%;height:100%">Chuyến : <br>Biển số xe : <br>Giờ
                                chạy
                                : <br></div>
                            <div style="float: left;width:17%">
                                <span style="font-weight: bold;" class="chuyen"></span><br>
                                <span style="font-weight: bold;" class="bienso"></span><br>
                                <span class="startTime" style="font-weight: bold"></span>
                            </div>
                            <div style="float: left;width:35%;height:100%"></div>
                            <div style="float: left;text-align: left;width:9%">Lái xe : <br>Phụ xe : <br>Tổng ghế :
                            </div>
                            <div style="float: left;width:20%">
                                <span style="font-weight: bold; " class="laixe"></span><br>
                                <span class="phuxe" style="font-weight: bold;"></span><br>
                                <span class="totalSeat" style="font-weight: bold"></span>
                            </div>
                        </div>
                    </tr>
                    <tr>
                        <th style="text-align: center;border: 1px solid">STT</th>
                        <th style="text-align: center;border: 1px solid">TRẢ</th>
                        <th style="text-align: center;border: 1px solid">HK</th>
                        <th style="text-align: center;border: 1px solid">SĐT</th>
                        <th style="text-align: center;border: 1px solid">GHẾ</th>
                        <th style="text-align: center;border: 1px solid">GHI CHÚ</th>
                        <th style="text-align: center;border: 1px solid">TÀI XẾ TRẢ</th>
                    </tr>
                    </thead>
                    <tbody id="list-customer-dropoff"></tbody>
                </table>
            </div>
        </div>
    </div>
    <br><br>
</div>