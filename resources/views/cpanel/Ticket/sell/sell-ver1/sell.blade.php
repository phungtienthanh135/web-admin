@extends('cpanel.template.layout')
@section('title', 'Bán vé')
@push('activeMenu')
    activeMenu('{{action('TicketController@sell')}}');
@endpush
@section('content')
    <style>
        .w100{width:-webkit-fill-available}
        #listTicketCancel table:not(.info){border : 1px solid #000 !important;border-radius:0}
        #listTicketCancel table:not(.info) th,#listTicketCancel table:not(.info) td{border : 1px solid #000 !important;}
        #listTicketCancel table.doanhthu th{background : #084388;color:#fff}
        #listTicketCancel table.thuchichuyen th{background : #886708;color:#fff}
        #makeMap tr {
            height: 100px !important;
        }
        #makeMap td, #makeMap th{
            border: 1px solid black !important;
        }
        #list-customer-dropoff td,#list-customer-dropoff th{
            border: 1px solid black !important;
            text-align: center;
        }
        #list-customer-pickup td,#list-customer-pickup th{
            border: 1px solid black !important;
            text-align: center;
        }
        #list-customer td,#list-customer th{
            border: 1px solid black !important;
        }
        .seatLock {
            position: absolute;
            width: 100%;
            height: 100%;
            background: #444;
            z-index: 10;
            pointer-events: all;
            top: 0;
            opacity: 0.5;
        }

        .seatLock .userLock {
            color: #fff;
            text-align: center;
            line-height: 30px;
            text-transform: initial;
        }

        .seatLock .iconLock {
            position: absolute;
            top: 40%;
            text-align: center;
            width: 100%;
            color: #fff
        }

        .seatLock .borderIconLock {
            line-height: 40px;
            width: 40px;
            border: 1px solid #fff;
            border-radius: 50%;
            display: inline-block
        }

        .seatLock .borderIconLock:hover {
            line-height: 40px;
            width: 40px;
            border: 1px solid #fff;
            background: #fff;
            color: #444
        }

        #chontuyen + span .select2-selection__rendered {
            font-size: 15px;
        }

        #table-list th.headerSortDown {
            background-image: url('/public/images/sort/desc.gif');
            background-repeat: no-repeat;
            background-position: center right;
            background-color: #1a5da0;
        }

        #table-list th.headerSortUp {
            background-image: url('/public/images/sort/asc.gif');
            background-repeat: no-repeat;
            background-position: center right;
            background-color: #1a5da0;
        }

        .trangthaighe {
            height: 5px;
            margin-top: -3px;
            float: left

        }

        #modalTicketHistory {
            width: 65%;
            margin-left: -30%;
            top: 2%;
        }

        .cus-info {
            border: 1px solid #aaa;
            background: #fff;
        }

        .badge {
            position: absolute;
            top: 10px;
            right: 1px;
            display: inline-block;
            width: 20px;
            padding: 0;
            margin: 0;
            font-size: 12px;
            font-weight: 700;
            line-height: 20px;
            color: #fff;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            background-color: #f16060;
            border-radius: 50%
        }

        .cus-info:hover {
            background-color: #eee;
        }

        .ui-datepicker-today a {
            background: #37a6cd !important;
            color: #fff !important;
        }

        .ui-datepicker-current-day a {
            background: #cf5656 !important;
            color: #fff !important;
        }

        #ui-id-1 {
            width: 32% !important;
            top: 170px !important;
            left: 51% !important;
            position: fixed !important;
            z-index: 99999 !important;
            cursor: pointer;
        }

        .ghe {
            height: 130px;
            overflow: hidden;
            font-size: 1.2em;
            line-height: 1;
            padding: 0;
            border: 3px solid rgba(255, 255, 255, 0);
            border-radius: 5px;
            margin-right: 5px;
            padding-bottom: 5px;
            width: 22vw;
        }

        .ghe:not(.loidi) {
            box-shadow: 0px 3px 19px #0003;
        }

        .ghetrong, .gc_trong, .gc_dachon {
            background: #FFFFFF;
            color: #333333;
        }

        .gc_trong {
            border: 1px solid #333333;
        }

        .ghedichuyen {
            opacity: 0.7;
        }

        .ghegiucho {
            /*background: #FFEEC8;*/
            background: #eeeeee;
            color: #333333;
        }

        .ghechuyenden {
            background: #ddd;
        }

        .ghedadat, .gc_dadat {
            background-color: #eee;
            color: #333333;
        }

        .ghelenxe {
            background: #eee;
        }

        /*.ghelenxe .ticketInfo-getInPoint {*/
        /*background: #002ca6;*/
        /*color: #fff;*/
        /*}*/

        .ghedangchon, .gc_dachon {
            border: 3px solid #ef3409 !important;
            background: #f19a9a;
        }

        .gc_dachon {
            border: 1px solid red !important;
        }

        .thanhtoan {
            background-color: #ffc239 !important;
            color: #137491 !important;
            font-weight: bold;
        }

        .ticketInfo-tool {
            padding: 5px;
            display: flex;
            justify-content: space-between;
        }

        .ticketInfo-tool div:first-child {
            border: 1px solid;
            padding: 5px;
            margin-right: 10px;
        }

        .ticketInfo-tool div:last-child button, .ticketInfo-tool div:last-child a {
            width: 2.5em;
            height: 2.5em;
            border-radius: 20px;
            margin-right: 0.4em;
            border: 1px solid #2E6BD0;
            font-size: 0.7em;
        }

        .ticketInfo-tool div:last-child button:last-child, .ticketInfo-tool div:last-child a:last-child {
            margin-right: 0;
        }

        .ticketInfo-tool .btn {
            padding: 0;
        }

        .ticketInfo-getInPoint {
            height: 20px;
            background-color: #35AE2C;
            text-align: center;
            margin: 0 -3px 10px;
            font-size: 12px;
            color: #FFFFFF;
            text-transform: none;
            padding-top: 5px;
        }

        .btn.btn-primary, .btn.btn-inprimary:hover {
            color: #2E6BD0;
            background: #ffffff;
            border-width: 2px;
            border-color: #2E6BD0;
            text-shadow: none;
        }

        .btn.btn-primary:hover, .btn.btn-inprimary {
            background: #2E6BD0;
            color: #ffffff;
            border-width: 2px;
            border-color: #ffffff;
            text-shadow: none;
        }

        .tripInfo {
            position: relative;
        }

        .tripInfo img.daxuatben {
            position: absolute;
            left: 40%;
            top: -35px;
        }

        .tripInfo div:only-child {
            background-color: #DBE8FF;
            height: 80px;
            padding: 10px;
            border-radius: 5px;
        }

        i.fas.fa-phone {
            transform: rotate(100deg);
        }

        #vehicleInfo p, #ticketInfo p {
            font-size: 14px;
        }

        .ticketInfo-content {
            text-transform: none;

        }

        .ticketInfo-content p {
            margin: 0 10px;
            font-size: 0.7em;
            line-height: 1.4em;
            overflow: auto;
        }

        .ticketInfo-content i.fas.fa-dollar-sign {
            font-size: 20px;

        }

        select, textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            font-size: 14px;
        }

        .input-append button:first-child, .input-append input:first-child {
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
            border-right: none;
        }

        .input-append input {
            text-align: center;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
        }

        .select2-container--default .select2-selection--single {
            border-radius: 4px;
        }

        .heading_top {
            padding: 1px 9px;
            background-color: #2E6BD0
        }

        #ui-datepicker-div {
            z-index: 100002 !important;
        }

        .seatmaptable tr td {
            width: 40px;
            height: 40px;
            text-align: center;
        }

        .cantmove {
            background: #aaa;
        }

        #ui-id-1 {
            width: 32% !important;
            top: 170px !important;
            left: 51% !important;
            position: fixed !important;
            z-index: 99999 !important;
            cursor: pointer;
        }

        @media (max-width: 425px) {
            .ghe {
                width: calc(100% - 20px) !important;
            }
        }

        @media (max-width: 768px) and (min-width: 424px) {
            .ghe {
                width: calc(50% - 20px) !important;
            }
        }

        @media (max-width: 768px) {
            .btnPrintTicket {
                display: none;
            }

            /*.btnFlashSell {*/
            /*display: none;*/
            /*}*/
            /*.btnSellTicket {*/
            /*display: none;*/
            /*}*/
            /*.makeCall {*/
            /*display: none;*/
            /*}*/
            /*.editTicket {*/
            /*display: none;*/
            /*}*/
            .btnTransferTicket {
                display: none;
            }

            /*.btnFlashMoveSeat {*/
            /*display: none;*/
            /*}*/
            .ghe.loidi {
                display: none;
            }

            #sell_ticket {
                width: 90% !important;
                margin-left: 2% !important;
                height: 400px !important;
                overflow-y: scroll !important;
            }

            #sell_ticket .modal-body {
                overflow-y: inherit !important;
                max-height: initial !important;
            }
        }

        .padding40 {
            padding-left: 40px;
        }

        .dropdown-toggle::after {
            display: inline-block;
            width: 0;
            height: 0;
            margin-left: .255em;
            vertical-align: .255em;
            content: "";
            border-top: .3em solid;
            border-right: .3em solid transparent;
            border-bottom: 0;
            border-left: .3em solid transparent;
        }

        .dropdown-item {
            display: block;
            width: 100%;
            padding: .25rem 1.5rem;
            clear: both;
            font-weight: 400;
            color: #212529;
            text-align: inherit;
            white-space: nowrap;
            background-color: transparent;
            border: 0;
        }
    </style>
    <style id="style-seat">

    </style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid m_top_10">
                <a href="{{action('TicketController@printTicket',['ticketId'=>request('ticketId')])}}"
                   id="printTicket"></a>
                <form id="searchTrip" action="">
                    <div class="row-fluid">
                        <div class="span8">
                            <div class="row-fuild">
                                <div class="span6">
                                    <select class="chontuyen" name="routeId" id="chontuyen" style="font-size: 15px">
                                        @foreach($listRoute as $route)
                                            <option data-startPoint="{{$route['listPoint'][0]['pointId']}}"
                                                    data-endPoint="{{last($route['listPoint'])['pointId']}}"
                                                    {{request('routeId')==$route['routeId']?'selected':''}}
                                                    value="{{$route['routeId']}}">{{$route['routeName']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="span6">
                                    <input name="date" class="form-control" autocomplete="off"
                                           style="width: -webkit-fill-available;text-align:center;font-size: 15px"
                                           type="text" id="txtCalendar"
                                           value="{{request('date',date('d-m-Y'))}}">
                                </div>
                                <div class="span4" style="display: none;">
                                    <select name="tripId" id="listTrip" class="span12">
                                        <option value="">Chuyến</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span6">
                                    <select id="cbb_InPoint" class="span12 startPoint" style="font-size: 15px">
                                    </select>
                                </div>
                                <div class="span6">
                                    <select id="cbb_OffPoint" class="span12 endPoint" style="font-size: 15px">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            @if(count(session('userLogin')["userInfo"]['listAgency'])>0)
                                <div class="span6" style="margin-bottom:10px">
                                    <button class="btn btn-inprimary span12" type="button" id="DSChuyen">
                                        <i class="fas fa-list-ol"></i> DS Chuyến
                                    </button>
                                </div>
                                <div class="span6" style="margin-bottom:10px;">
                                    @if(true)
                                        <a href="{{action('ReportController@detail')}}"
                                           class="btn btn-inprimary span12">Báo Cáo</a>
                                    @endif
                                </div>
                                <div class="span6"
                                     style="font-weight: bold;color:#fff;margin-bottom:10px;margin-left: 0">
                                    Hạn mức đại lý : <span id="hanMucDaiLi"><?php
                                        if ($hanmuc['code'] == 200 && count($hanmuc['results']) > 0) {
                                            echo number_format($hanmuc['results']['allowMoney']['existMoney']) . " VND";
                                        }
                                        ?></span>
                                </div>
                            @else
                                <div class="span6">
                                    <button class="btn btn-inprimary span12" type="button" id="DSChuyen">
                                        <i class="fas fa-list-ol"></i> DS Chuyến
                                    </button>
                                </div>
                                <div class="span6" style="margin-bottom:10px;">
                                    <div class="dropdown">
                                        <button class="btn btn-inprimary span12 dropdown-toggle" type="button"
                                                id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                            Điều hành
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2"
                                             style="position: absolute;transform: translate3d(0px, 30px, 0px);top: 0px;left: 0px;will-change: transform;">
                                            @if(hasAnyRole(START_TRIP))
                                                <button class="dropdown-item" type="button" id="btnStartTrip">
                                                    <i class="fas fa-redo-alt"></i> Xuất bến
                                                </button>
                                                <button class="dropdown-item" type="button" id="btnFinishTrip">
                                                    <i class="fas fa-redo-alt"></i> Kết thúc chuyến
                                                </button>
                                            @endif
                                            @if(hasAnyRole(OPEN_START_TRIP))
                                                <button class="dropdown-item" type="button" id="btnOpenTrip"
                                                        style="display: none;">
                                                    <i class="fas fa-redo-alt"></i> Mở xuất bến
                                                </button>
                                            @endif
                                            <button class="dropdown-item btnChangeSeatMap" type="button">
                                                <i class="fas fa-bus"></i> Đổi xe
                                            </button>
                                            {{--<button class="dropdown-item btnVehiclePlus" type="button">--}}
                                            {{--<i class="fas fa-bus"></i> Gộp xe--}}
                                            {{--</button>--}}
                                            <button class="dropdown-item btnSchedulePlus" type="button">
                                                <i class="fas fa-plus-circle"></i> Tăng cường
                                            </button>
                                            <button class="dropdown-item btnActionBill" type="button">
                                                <i class="fas fa-money-bill-alt"></i> Thêm thu chi chuyến
                                            </button>
                                            <button class="dropdown-item" type="button"
                                                    onclick="return window.location.href='{{action('TripController@show')}}'">
                                                <i class="fas fa-calendar-alt"></i> Lịch Chạy
                                            </button>
                                            <a class="dropdown-item btnChotChuyen" type="button" href="{{url('cpanel/vue/ticket?tab=3')}}"><i class="fas fa-calendar-alt"></i> Chốt Chuyến</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="span6" style="margin-left: 0">
                                    <button class="btn btn-inprimary span12" type="button"
                                            id="ticketList">
                                        <i class="fas fa-edit"></i> Bán vé
                                    </button>
                                </div>
                                <div class="span6">
                                    <button class="btn btn-inprimary span12" type="button" id="customerList">
                                        <i class="fas fa-print"></i> In phơi
                                    </button>
                                </div>
                            @endif
                        </div>
                    </div>
                </form>
            </div>

        </div>

        <!-- Modal xuat ben -->
        <div class="modal modal-custom hide fade" id="modalXuatben">
            <div class="modal-header center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Xuất bến</h3>
            </div>
            <div class="modal-body center">
                <div class="row-fluid">
                    <div class="form-horizontal row-fluid">
                        <div class="span6">
                            <div class="form-group">
                                <label for="" style="text-align: left">Tài Xế</label>
                                <select name="" id="addDriver" class="span12 listDriveOption" multiple></select>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="form-group">
                                <label for="" style="text-align: left">Phụ Xe</label>
                                <select name="" id="addAssitant" class="span12 listAssOption" multiple></select>
                            </div>
                        </div>
                        <div class="span12" style="margin-left:0;">
                            <label for="" class="text-left">Ghi chú</label>
                            <textarea name="startTripNote" id="txtStartTripNote" style="width:97%" rows="3"></textarea>
                        </div>
                        <div class="span10" style="margin:10px 0px;">
                            <input type="checkbox" name="sendSMSRound" id="sendSMSStartTrip"/>
                            <label class="f_left" for="sendSMSStartTrip">Gửi tin nhắn đến lái xe</label>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn-warning btn-flat" id="doStartTrip">Xuất bến</button>
            </div>
        </div>
        <!-- End xuat ben -->

        <!--modal lịch sử vé -->
        <div class="modal modal-custom hide fade" id="modalTicketHistory">
            <div class="modal-header center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Lich sử vé</h3>
            </div>
            <div class="modal-body center">
                <div class="row-fluid">
                    <div class="form-horizontal row-fluid">
                        <div style="margin-bottom:10px;">
                            <button class="btn btn-default active btnTabContentHistoryTicket">Lịch sử vé</button>
                            <button class="btn btn-default btnTabContentHistoryTicketSell" data-loading="loading">Lịch
                                sử đặt vé
                            </button>
                        </div>
                        <div id="contentHistoryTicket">

                        </div>
                        <div id="contentHistoryTicketSell">

                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">ĐÓNG</a>
            </div>
        </div>
        <!-- end model lịch sử vé -->
        <!--modal lịch sử vé -->
        <div class="modal modal-custom hide fade" id="modalAddGoods">
            <div class="modal-header center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Thêm Hàng Hóa</h3>
            </div>
            <div class="modal-body center">
                <div class="row-fluid">
                    <div class="span4 form-group">
                        <label for="" style="text-align: left">Người Gửi</label>
                        <input type="text" class="w100" id="txtGoodsSender">
                    </div>
                    <div class="span4 form-group">
                        <label for="" style="text-align: left">SDT</label>
                        <input type="text" class="w100" id="txtGoodsSenderPhone">
                    </div>
                    {{--<div class="span3 form-group">--}}
                        {{--<label for="" style="text-align: left">Điểm gửi</label>--}}
                        {{--<select id="slbGoodsPickPointId" class="goodsSelectPoint"></select>--}}
                    {{--</div>--}}
                    <div class="span4 form-group">
                        <label for="" style="text-align: left">Địa chỉ gửi</label>
                        <input type="text" class="w100" id="txtGoodsPickUpPoint">
                    </div>
                </div>
                <hr style="margin: 5px 0;">
                <div class="row-fluid">
                    <div class="span4 form-group">
                        <label for="" style="text-align: left">Người Nhận</label>
                        <input type="text" class="w100" id="txtGoodsReceiver">
                    </div>
                    <div class="span4 form-group">
                        <label for="" style="text-align: left">SDT</label>
                        <input type="text" class="w100" id="txtGoodsReceiverPhone">
                    </div>
                    {{--<div class="span3 form-group">--}}
                        {{--<label for="" style="text-align: left">Điểm Nhận</label>--}}
                        {{--<select id="slbGoodsDropPointId" class="goodsSelectPoint"></select>--}}
                    {{--</div>--}}
                    <div class="span4 form-group">
                        <label for="" style="text-align: left">Địa chỉ Nhận</label>
                        <input type="text" class="w100" id="txtGoodsDropOffPoint">
                    </div>
                </div>
                <hr style="margin: 5px 0;">
                <div class="row-fluid">
                    <div class="span6 form-group">
                        <label for="" style="text-align: left">Tên Hàng</label>
                        <input type="text" class="w100" id="txtGoodsName">
                    </div>
                    <div class="span6 form-group">
                        <label for="" style="text-align: left">Giá trị hàng</label>
                        <input type="number" class="w100 text-right" id="txtGoodsValue">
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span6 form-group">
                        <label for="" style="text-align: left">Cước phí</label>
                        <input type="number" class="w100 text-right" id="txtGoodsTotalPrice">
                    </div>
                    <div class="span3 form-group">
                        <label for="" style="text-align: left">Đã thu</label>
                        <input type="number" class="w100 text-right" id="txtGoodsPaid">
                    </div>
                    <div class="span3 form-group">
                        <label for="" style="text-align: left">Còn lại</label>
                        <input type="text" readonly class="w100 text-right" id="txtGoodsUnpaid">
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12 form-group">
                        <label for="" style="text-align: left">Ghi chú</label>
                            <textarea name="" id="txtGoodsNote" class="w100" rows="2"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-warning btnCreateGoods">TẠO HÀNG</button>
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">ĐÓNG</a>
            </div>
        </div>
        <!-- end model lịch sử vé -->
        <!--modal lịch sử vé -->
        <div class="modal modal-custom hide fade" id="modalBill">
            <div class="modal-header center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="lbTitleThuChi">Thu / Chi</h3>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span3 text-right">Loại Phiếu</div>
                    <div class="span9 controls">
                        <div class="span4">
                            <label for="rdPhieuThu">
                                <input type="radio" checked name="phieuthuchi" value="1" id="rdPhieuThu">
                                <span> Phiếu thu</span>
                            </label>
                        </div>
                        <div class="span4">
                            <label for="rdPhieuChi">
                                <input type="radio" name="phieuthuchi" value="2" id="rdPhieuChi">
                                <span> Phiếu chi</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3 text-right">Mã Phiếu</div>
                    <div class="span9">
                        <input type="text" class="w100" id="txtDocumentCode"  placeholder="PT000001">
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3 text-right">Người Duyệt</div>
                    <div class="span9">
                        <select name="" id="slbApproverId" class="w100 slbFindAllUser"></select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3 text-right lbSpender">Người giao</div>
                    <div class="span9">
                        <select name="" id="slbSpenderId" class="w100 slbFindAllUser"></select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3 text-right">Người nhận</div>
                    <div class="span9">
                        <select name="" id="slbReceiverId" class="w100 slbFindAllUser"></select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3 text-right">Khoản mục</div>
                    <div class="span9">
                        <select name="" id="slbItem" class="w100"></select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3 text-right">Số Lượng / Đơn vị</div>
                    <div class="span6">
                        <input type="text" class="w100" value="1" id="txtNumber"  placeholder="3">
                    </div>
                    <div class="span3">
                        <select name="" id="slbUnit" class="w100">
                            <option value="KG">KG</option>
                            <option value="L">L</option>
                            <option value="PIECE">Chiếc (piece)</option>
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3 text-right">Thành Tiền</div>
                    <div class="span9">
                        <input type="text" class="w100" value="1" id="txtAmount"  placeholder="3">
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3 text-right">Lí Do</div>
                    <div class="span9">
                        <textarea id="txtReason" rows="3" class="w100"></textarea>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3 text-right">Ghi Chú</div>
                    <div class="span9">
                        <textarea id="txtNoteBill" rows="3" class="w100"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-warning btnSaveBill">Lưu Giao Dịch</button>
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">ĐÓNG</a>
            </div>
        </div>
        <!-- end model lịch sử vé -->

        <!-- Mở xuất bến -->
        <div class="modal hide fade modal_logout" id="modalMoXuatben">
            <div class="modal-body center">
                <p class="text-center">Bạn có muốn bỏ xuất bến ?</p>
            </div>
            <div class="modal-footer">
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn-warning btn-flat" id="doOpenStartTrip">ĐỒNG Ý</button>
            </div>
        </div>
        <!-- Mở xuất bến -->


        <!-- In vé -->
        <div class="hide">
            <div id="divPrintTicket">
                <div id="in">
                    <div style="width: 173px;height: 390px;font-size: 11px;line-height: 15px;
    font-family: sans-serif;font-weight: 100;">
                        <div style="text-align: center;margin: 0 10px 10px 0">
                            <img src="{{session('userLogin')['logo']}}" width="100%" height="100px"
                                 style="    padding-left: 5px;">
                            <div id="title_ticket" style="margin-top: 10px">VÉ ĐẶT CHỖ</div>
                            <div style="margin-bottom: 10px;text-transform: uppercase" id="xePrintTicket"></div>
                            <div>------------------------</div>
                            <div id="barCodePrintTicket"></div>
                            <div style="margin-top: -4px" id="ticketCodePrint"></div>
                        </div>
                        <div id="ticketInfo"></div>
                        <hr>
                        <div style="margin-top: -10px" id="company_note"></div>
                        <div>Hotline : <span style="font-weight:bold;">{{session('userLogin')['telecomPhoneNumber']}}</span></div>
                    </div>
                </div>
            </div>
        </div>


        <!-- List Seat Map -->
    @include('cpanel.Ticket.sell.sell-ver1.listSeatMap')
    <!-- End List Seat Map -->

        <!-- List Seat Map -->
    @include('cpanel.Ticket.sell.sell-ver1.listCustomer')
    <!-- End List Seat Map -->

    </div>
    <!-- End Content -->

    <!-- Modal Book Ticket -->
    @include('cpanel.Ticket.sell.sell-ver1.sell-ticket')
    <!-- End Modal Book Ticket -->

    <!-- Modal Edit Ticket -->
    @include('cpanel.Ticket.sell.sell-ver1.edit-ticket')
    <!-- End Modal Edit Ticket -->

    <!--modal changeSeatMap-->
    @include('cpanel.Ticket.sell.module.change-seatmap')
    <!-- End Modal changeSeatMap -->

    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}

    <!-- In vé waterbus-->
    <div class="hide">
        <div style="width: 80mm">
            <div id="waterbusPrint" style="width: 80mm;font-family: 'Fira Sans Extra Condensed';">
                <div style="width:80mm; float: left;">
                    <div style="width:40.625%;float: left">
                        <div class="row-fluid" style="margin-top: -2mm;padding-left: 2mm;"><a><img
                                        style="height:28mm;width: 24mm;padding-left: 2mm;"
                                        src="{{session('userLogin')['logo']}}"/></a>
                        </div>
                        <div class="sgwb">
                            <div class="row-fluid"
                                 style="text-align: center;margin-top: -25px;font-size: 9pt;font-weight: bold;">SAIGON
                            </div>
                            <div class="row-fluid"
                                 style="text-align: center;    margin-top: -4pt;font-size: 15pt; font-weight: bold;">
                                WATERBUS
                            </div>
                        </div>
                        <div class="row-fluid" style="text-align: center;letter-spacing: 0px;font-size: 7pt">Have a good journey!
                        </div>
                        <div class="row-fluid" style="padding-top: 1mm;padding-left: 5.2mm;">
                            <img id="getQRCode" style="width: 22mm;height: 25mm;">
                            <div id="waterbusQR"></div>
                        </div>
                        <div class="row-fluid" style="padding-top: 6.5mm;padding-left: 5mm;font-size: 10.438pt">
                            Ngày/Date
                        </div>
                        <div class="row-fluid"
                             style="margin-top: -1pt;padding-left: 5mm;    font-size: 16.418pt;font-weight: 600;"
                             id="datePrintWB"></div>
                        <div class="row-fluid" style="padding-top: 6.5mm;padding-left: 5mm;font-size: 10.438pt">
                            Xe / Vehicle
                        </div>
                        <div class="row-fluid"
                             style="margin-top: -1pt;padding-left: 5mm;    font-size: 16.418pt;font-weight: 600;"
                             id="vehiclePrint"></div>
                        <div class="row-fluid" style="margin-top: 2mm;padding-left: 5mm;font-size: 10.438pt">Giờ khởi
                            hành
                        </div>
                        <div class="row-fluid" style="margin-top: -5px;padding-left: 5mm;font-size: 10.438pt">Departure
                            time
                        </div>
                        <div class="row-fluid"
                             style="margin-top: -1pt;padding-left: 5mm;font-size: 16.418pt;font-weight: 600;"
                             id="timePrintWB"></div>
                        <div class="row-fluid printLabelHotline" style="margin-top: 2mm;padding-left: 5mm;font-size: 10.438pt">Hotline:</div>
                        <div class="printHotline" style="margin-top: -1pt;padding-left: 5mm;font-size: 13.418pt;font-weight: 600;"></div>
                    </div>
                    <div style="width:59.35%;float: right;    margin-top: 5mm">
                        <div class="row-fluid left printCompanyName" style="font-size: 7.2pt;line-height: 11px;">Công ty TNHH Thường
                            Nhật
                        </div>
                        <div class="row-fluid left printCompanyAddress" style="font-size: 7.2pt;line-height: 11px;">Số 6 Phan Kế Bính,Phường
                            Đa Kao,Quận 1,
                            TP.HCM
                        </div>
                        <div class="row-fluid left" style="    margin-bottom: -2mm;line-height: 11px;">
                            <div class="left printMTS" style="float: left;font-size: 7.2pt;">MST:0304354924</div>
                            <div class="right" style="float: right;font-size: 7.2pt"><span class="printLabelHotline"></span><span class="printHotline"></span></div>
                        </div>
                        <hr>
                        <div class="row-fluid printTitleTicket" style="font-size: 13.8pt;font-weight: 600;    margin-top: -1mm;">Vé Hành Khách Công Cộng Bằng Đường Thủy
                        </div>
                        <div class="row-fluid printNameTicket" style="    margin-top: 5px;font-size: 15.22pt;">Saigon Waterbus Ticket
                        </div>
                        <div class="row-fluid right" style="margin-top: 2pt;" id="routePrintWB"></div>
                        <div class="row-fluid left printPrice" style="margin-top: 5px;font-size:9.4pt">Giá vé / Fare : <span id="pricePrintWB" style="margin-top: -1pt;font-size: 11.82pt;"></span></div>
                        <div class="row-fluid left" style="margin-top: -1pt;font-size: 11.82pt;"
                             id=""></div>
                        <div class="row-fluid left printBaoHiem" style="margin-top: -3pt;font-size: 7pt;">(Giá vé đã bao gồm bảo hiểm
                            hành khách)
                        </div>
                        <div class="row-fluid left printWebsite" style="margin-top: 0px;font-size: 13.5pt;">www.saigonwaterbus.com
                        </div>
                        <div class="row-fluid left"
                             style="font-weight: 600;text-align:right;margin-top: -1pt;font-size: 13.418pt;margin-top:10px"
                             id="printListSeat"></div>
                        <div class="row-fluid left" style="font-size: 10.438pt;    padding-top: 4mm;padding-left: 26mm">
                            Ga đi/From
                        </div>
                        <div class="row-fluid left" id="pickPrintWB"
                             style="font-weight: 600;text-align:right;margin-top: -1pt;font-size: 13.418pt;"></div>
                        <div class="row-fluid left"
                             style="padding-left: 26mm;    padding-top: 6mm;font-size: 10.438pt;">Ga đến/To
                        </div>
                        <div class="row-fluid left"
                             style="font-weight: 600;text-align:right;margin-top: -1pt;font-size: 13.418pt;"
                             id="dropPrintWB"></div>
                    </div>
                </div>
                <div class="printQuyDinh" style=" width: 80mm;float: left;margin-top: -5px;font-size: 7.9pt; line-height: 17px;padding-left: 5mm;">
                    <hr>
                    <div class="row-fluid left">Hành khách vui lòng tuân thủ các quy định sau:</div>
                    <div class="row-fluid left">1. Vé mua rồi miễn trả lại.</div>
                    <div class="row-fluid left">2. Xếp hàng theo thứ tự, chờ đến lượt lên tàu trước giờ tàu chạy.</div>
                    <div class="row-fluid left">3. Luật an toàn giao thông đường thủy.</div>
                </div>
            </div>
        </div>
    </div>
    <link href='https://fonts.googleapis.com/css?family=Fira Sans Extra Condensed' rel='stylesheet'>
    <script src="/public/javascript/jquery-dateFormat-master/dist/jquery-dateFormat.min.js"
            type="text/javascript"></script>

    <script src="/public/javascript/jquery.printPage.js" type="text/javascript"></script>
    <script src="/public/javascript/printThis.js" type="text/javascript"></script>
    <script>
        var
            tripSelected = {
                ticketPrice: 0,
                childrenTicketRatio: 0,
                tripData: {},
                tripId: '{{request('tripId')}}',
                scheduleId: '{{request('scheduleId')}}'
            },//biến toàn cục dùng để lưu thông tin chuyến khi đã chọn chuyến từ ds chuyến
            listTrip = [],//biến toàn cục dùng lưu ds chuyến khi đã chọn lịch
            datepickerBox = $("#txtCalendar"),//lịch
            selectBoxRoute = $('#chontuyen'),//selectbox chọn tuyến
            selectBoxListTrip = $('#listTrip'),//selectbox chọn tuyến
            txtCustomerPhoneNumber = $('.CustomerPhoneNumber'),//textbox số điện thoại khách hàng
            defaultDateForCalendar = '{{request('date',date('d-m-Y'))}}',//ngày mặc định nếu có param date
            urlSeachTrip = '{{action('TripController@search')}}',//url tìm lịch cho khách
            urlSeachTripById = '{{action('TripController@searchByTripId')}}',//url lấy thông tin chuyến
            urlGetPriceTicketGoods = '{{action('TicketController@getPriceTicketGoods')}}',//url lấy giá vé gửi đồ
            urlCheckPromotionCode = '{{action('CouponController@check')}}',//url lấy thông tin mã khuyến mãi
            urlPrintTrip = '{{action('TripController@printVoidTicket')}}',//url in phơi vé
            urlUpdateStatusTicket = '{{action('TicketController@updateStatusTicket')}}',//url cập nhật thông tin vé
            urlSellTicket = '{{action('TicketController@postSell')}}',//url cập nhật thông tin vé
            route_name_selected = '',
            round_route = '', // tuyến khứ hồi
            urlSeatMapInTrip = '{{action('TripController@getSeatMapByTripId')}}', // url lấy seatmap

            urlGetInfoCustomer = '{{action('CustomerController@getInfoByPhoneNumber')}}',//url lấy thông tin khách hàng qua sdt
            listRoute = {!! json_encode($listRoute,JSON_PRETTY_PRINT) !!};//thông tin tuyến dùng để cập nhật ds điểm dừng
        var RequestFlagStartPoint = true;
        var RequestFlagActiveTicket = true;

        var listSeatReActive = [];

        var additionTickets = [];

        var tripRealtime = {};//luu tru realtime database

        var listConnectionRealTime = [];

        var countSeat = {total: 0, giucho: 0, pay: 0, totalMoney: 0, totalUnPaidMoney: 0};

        var database = firebase.database();
        var base_price;
        var flashMoveSeat = false;
        var moveSeatInfo = {};
        var addSeat = {flag: false, ticketInfo: {}};
        var array_Sort_PickUp = [], array_Sort_DropOff = [], check_to_sort; // mảng sắp xếp trung chuyển, check radio input
        var Seat_Back_Array = [], listPrice = []; // mảng ghế , giá ghế khứ hồi
        var listRoundTrip = [];
        var totalPriceBack, total_Price = 0, trip_Id;
        var html_Round = '';
        var data_generate_listseat, TripData_Round;
        var changeDatePickerBox = true;
        var listSetting ={!!json_encode($listSetting)!!}, blankRow;  // mảng thuộc tính in phơi
        var nameFilter = ''; // tên để lọc phơi
        var listVehicle = [],
            listDriver = [],
            listAss = [];
        (init)();

        function init() {
            getListUser();
            getListVehicle();
            datepickerBox.datepicker({
                todayHighlight: true,
                defaultDate: defaultDateForCalendar,
                minDate: 0 - 0{{session('pastDaysToSellTicket')}},
                dateFormat: "dd-mm-yy",
                onSelect: function (dateStr) {
                    $(this).change();
                    var date_Str = '';
                    for (var i = 0; i < 10; i++) {

                        if (i == 1 || i == 0) {
                            date_Str += dateStr.charAt(i + 3);
                        } else if (i == 3 || i == 4) {
                            date_Str += dateStr.charAt(i - 3);
                        }
                        else date_Str += dateStr.charAt(i);
                    }

                    $('#txtCalendarRoundTrip').datepicker("option", {
                        minDate: new Date(date_Str)
                    });
                },
                numberOfMonths: 1
            });

            selectBoxRoute.select2({
                width: '100%',
                dropdownAutoWidth: true
            });


            selectBoxListTrip.select2({
                width: '100%'
            });
            $.each(listSetting, function (k, v) {
                if (v.idAttribute == 99) blankRow = v;
            });
            $(document).ready(function () {
                close_menu();

                // in sau khi thao tác vé
                $('body').on('change', '#print_after_save', function () {
                    if ($(this).is(':checked'))
                        sessionStorage.setItem('print_after_save', 1);
                    else sessionStorage.setItem('print_after_save', 0);
                });
                // ẩn hiện  danh sách vé hủy , hết hạn
                if ($('.khung_lich_ve').is(':visible')) {
                    $('.listTicketCancel').hide();
                    $('#listTicketCancel').show();
                }
                if ($('.khung_khach_hang').is(':visible')) {
                    $('#listTicketCancel').hide();
                    $('.listTicketCancel').show();
                }
//                js thêm vé
                $('body').on('click', '#themghe', function () {
//                    var ticketIdAdd = $('#txtTicketId').val();
                    addSeat.flag = true;
                    var seatId = $('#themghe').attr('data-seat-click');
                    addSeat.ticketInfo = $.grep(tripSelected.tripData.seatMap.seatList, function (e, i) {
                        return e.seatId == seatId;
                    })[0].ticketInfo[0];
//                    console.log(ticketIdAdd, seatId, addSeat);ticketIdAdd
                    $('#sell_ticket').modal('hide');
                    notyMessage('Chế độ thêm ghế ! Chọn các ghế đang trống để thêm, Chọn ghế đã có vé hoặc bấm Alt + X để về chế độ thường', 'info');
                    return false;
                });
                $('body').on('click', '[data-schedule-go-to]', function () {
                    $('.box-route-and-trip').slideToggle();
                    if ($(window).width() > 780) {
                        $("*").css('cursor', 'wait');
                    }
                    var scheduleId = $(this).data('schedule-go-to');
                    selectBoxListTrip.find("[data-schedule='" + scheduleId + "']").attr("selected", "selected").trigger('change');
                });
//                end thêm vé
                $('body').on('click', '.upListDriver', function () {
                    var transshipmentDriver = {
                            ticketId: '',
                            transshipmentPickUpDriver: '',
                            transshipmentDropOffDriver: ''
                        },
                        listDriver = [];
                    if (['TC03m1MSnhqwtfE', 'TC02orwyNWOVEq','TC07C1pjfHQtyduK'].indexOf('{{session('companyId')}}') >= 0) {
                        if (document.getElementById("dskhachdon").offsetLeft > 0 && document.getElementById("dskhachtra").offsetLeft <= 0) {
                            var selector = document.querySelector('tbody#list-customer-pickup');
                            var listTicketId = (selector.querySelectorAll('tr td.ticketID'));
                            var transshipmentPickUpDriver = selector.querySelectorAll('tr td select.driverPickUp');
                            for (var i = 0; i < listTicketId.length; i++) {
                                transshipmentDriver.ticketId = $(listTicketId[i]).data('val');
                                transshipmentDriver.transshipmentPickUpDriver = $(transshipmentPickUpDriver[i]).attr('value');
                                if (transshipmentDriver.transshipmentPickUpDriver != '')
                                    listDriver.push(JSON.parse(JSON.stringify(transshipmentDriver)));
                            }
                        }
                        if (document.getElementById("dskhachtra").offsetLeft > 0 && document.getElementById("dskhachdon").offsetLeft <= 0) {
                            var listTicketId = (document.querySelectorAll('#list-customer-dropoff td.ticketID'));
                            var transshipmentDropOffDriver = document.querySelectorAll('#list-customer-dropoff td select.driverDropOff');
                            for (var i = 0; i < listTicketId.length; i++) {
                                transshipmentDriver.ticketId = $(listTicketId[i]).data('val');
                                transshipmentDriver.transshipmentDropOffDriver = $(transshipmentDropOffDriver[i]).attr('value');
                                if (transshipmentDriver.transshipmentDropOffDriver != '')
                                    listDriver.push(JSON.parse(JSON.stringify(transshipmentDriver)));
                            }
                        }
                    } else {
                        var listTicketId = (document.querySelectorAll('#list-customer-pickup td.ticketID'));
                        var transshipmentPickUpDriver = document.querySelectorAll('#list-customer-pickup td select.driverPickUp');
                        var transshipmentDropOffDriver = document.querySelectorAll('#list-customer-pickup td select.driverDropOff');
                        for (var i = 0; i < listTicketId.length; i++) {
                            transshipmentDriver.ticketId = $(listTicketId[i]).data('val');
                            transshipmentDriver.transshipmentPickUpDriver = $(transshipmentPickUpDriver[i]).attr('value');
                            transshipmentDriver.transshipmentDropOffDriver = $(transshipmentDropOffDriver[i]).attr('value');
                            if (transshipmentDriver.transshipmentPickUpDriver != '' || transshipmentDriver.transshipmentDropOffDriver != '')
                                listDriver.push(JSON.parse(JSON.stringify(transshipmentDriver)));
                        }
                    }
                    //console.log(listDriver);
                    $.post('{{action('TicketController@addtransshipment')}}', {
                        _token: '{{csrf_token()}}',
                        list: listDriver
                    }, function (data) {
                        notyMessage("Gửi yêu cầu thành công!", 'success');

                    });
                });

                $('body').on('change', 'select.driver', function () {
                    var value = $(this).children(":selected").attr('value');
                    $(this).attr('value', value);

                });
                //an hien ds ve huy va het hạn
                $('body').on('click', '.btnDanhSachVeHuy', function () {
                    $('.danhsachve').removeClass('active');
                    $(this).addClass('active');
                    generateListTicketCancel(tripSelected.tripData.tripId);
                    return false;
                });
                $('body').on('click', '.btnDanhSachVeHetHan', function () {
                    $('.danhsachve').removeClass('active');
                    $(this).addClass('active');
                    generateListTicketExpired(tripSelected.tripData.tripId);
                    return false;
                });
                $('body').on('click', '.btnDanhSachVeXuongXe', function () {
                    $('.danhsachve').removeClass('active');
                    $(this).addClass('active');
                    generateListTicketDownTrip(tripSelected.tripData.tripId);
                    return false;
                });
                $('body').on('click', '.btnDanhSachHangTrenXe', function () {
                    $('.danhsachve').removeClass('active');
                    $(this).addClass('active');
                    generateListGoodsInTrip();
                    return false;
                });
                $('body').on('click', '.btnThuChiChuyen', function () {
                    $('.danhsachve').removeClass('active');
                    $(this).addClass('active');
                    generateThuChiChuyen();
                    return false;
                });

                $('body').on('change', '.check', function () {
                    if ($(this).attr('id') == 'cbListPick') {
                        $('body').on('click', '.sortpick', function () {
                            var list_input = $('#list-customer-pickup').find('.inputSortpick');
                            $.each(list_input,function(){
                                if ($(this).val() == '') {
                                    $(this).attr('value', 0);
                                    array_Sort_PickUp[$(this).data('id') - 1] = 0;
                                }
                            });
                            if (array_Sort_PickUp.length) {
                                var min, array_Sorted = [], listtr = [], a, i, j;
                                for (i = 0; i < array_Sort_PickUp.length; i++)
                                    array_Sorted[i] = array_Sort_PickUp[i];
                                for (i = 0; i < array_Sorted.length - 1; i++)
                                    for (j = i + 1; j < array_Sorted.length; j++)
                                        if (array_Sorted[j] < array_Sorted[i]) {
                                            a = array_Sorted[i];
                                            array_Sorted[i] = array_Sorted[j];
                                            array_Sorted[j] = a;
                                        }
                                var list_tr = $('#list-customer-pickup').find('.tr_sortpick');
                                for (j = 0; j < list_tr.length; j++)
                                    for (i = 0; i < list_tr.length; i++)
                                        if (j == ($(list_tr[i]).data('id') - 1)) listtr[j] = list_tr[i].innerHTML;
                                $('#list-customer-pickup').html('');
                                for (i = 0; i < array_Sort_PickUp.length; i++) {
                                    min = array_Sorted[i];
                                    for (j = 0; j < array_Sort_PickUp.length; j++) {
                                        if (array_Sort_PickUp[j] == min) {
                                            $('#list-customer-pickup').append( "<tr class='tr_sortpick' data-id='" + (j + 1) + "'>" + listtr[j] + "</tr>");
                                            array_Sort_PickUp[j] -= 1;
                                            j = array_Sort_PickUp.length;
                                        }
                                    }
                                }
                                var foot = "<tr class='trsortpick'><td><button class='sortpick'>Sắp xếp</button></td>";
                                if (['TC03m1MSnhqwtfE', 'TC02orwyNWOVEq','TC07C1pjfHQtyduK'].indexOf('{{session('companyId')}}') >= 0) foot += "<td colspan='5'></td>";
                                else if ('{{session('companyId')}}' == 'TC0481HSc6Pl7ko') foot += "<td colspan='6'></td>";
                                else foot += "<td colspan='10'></td>";
                                foot += "<td><button class='upListDriver' style='width: 100%; background-color: #2E6BD0;border: none ;font-weight: bold; '>Chốt danh sách</button></td></tr>";

                                $('#list-customer-pickup').append(foot);
                                for (i = 0; i < array_Sort_PickUp.length; i++) {
                                    array_Sort_PickUp[i] += 1;

                                }
                            }
                        });
                        $('body').on('change', '.inputSortpick', function () {
                            if (parseInt($(this).val()) < 0) {
                                alert('Hãy nhập số dương');
                                $(this).attr('value', 0);
                            } else $(this).attr('value', $(this).val());
                            array_Sort_PickUp[$(this).data('id') - 1] = parseInt($(this).val());
                        });
                    }
                    if ($(this).attr('id') == 'cbListDrop') {
                        $('body').on('click', '.sortdrop', function () {
                            var list_input = $('#list-customer-dropoff').find('.inputSortDrop');
                            $.each(list_input,function(){
                                if ($(this).val() == '') {
                                    $(this).attr('value', 0);
                                    array_Sort_DropOff[$(this).data('id') - 1] = 0;
                                }
                            });
                            if (array_Sort_DropOff.length != 0) {
                                var min, array_Sorted = [], listtr = [], i, j;
                                for (var i = 0; i < array_Sort_DropOff.length; i++)
                                    array_Sorted[i] = array_Sort_DropOff[i];
                                for (i = 0; i < array_Sorted.length - 1; i++)
                                    for (j = i + 1; j < array_Sorted.length; j++)
                                        if (array_Sorted[j] < array_Sorted[i]) {
                                            a = array_Sorted[i];
                                            array_Sorted[i] = array_Sorted[j];
                                            array_Sorted[j] = a;
                                        }
                                var list_tr = $('#list-customer-dropoff').find('.tr_sortdrop');
                                for (j = 0; j < list_tr.length; j++)
                                    for (i = 0; i < list_tr.length; i++)
                                        if (j == ($(list_tr[i]).data('id') - 1)) listtr[j] = list_tr[i].innerHTML;

                                $('#list-customer-dropoff').html('');
                                for (i = 0; i < array_Sort_DropOff.length; i++) {
                                    min = array_Sorted[i];
                                    for (j = 0; j < array_Sort_DropOff.length; j++) {
                                        if (array_Sort_DropOff[j] == min) {
                                            $('#list-customer-dropoff').append("<tr class='tr_sortdrop' data-id='" + (j + 1) + "'>" + listtr[j] + "</tr>");
                                            array_Sort_DropOff[j] -= 1;
                                            j = array_Sort_DropOff.length;
                                        }
                                    }
                                }
                                var foot = "<tr class='trsortdrop'><td><button class='sortdrop'>Sắp xếp</button></td>";
                                if (['TC03m1MSnhqwtfE', 'TC02orwyNWOVEq','TC07C1pjfHQtyduK'].indexOf('{{session('companyId')}}') >= 0) foot += "<td colspan='5'></td>";
                                else if ('{{session('companyId')}}' == 'TC0481HSc6Pl7ko') foot += "<td colspan='6'></td>";
                                else foot += "<td colspan='10'></td>";
                                foot += "<td><button class='upListDriver' style='width: 100%; background-color: #2E6BD0;border: none ;font-weight: bold; '>Chốt danh sách</button></td></tr>";

                                $('#list-customer-dropoff').append(foot);
                                for (i = 0; i < array_Sort_DropOff.length; i++) {
                                    array_Sort_DropOff[i] += 1;

                                }
                            }
                        });
                        $('body').on('change', '.inputSortdrop', function () {
                            if (parseInt($(this).val()) < 0) {
                                alert('Hãy nhập số dương');
                                $(this).attr('value', 0);
                            } else $(this).attr('value', $(this).val());
                            array_Sort_DropOff[$(this).data('id') - 1] = parseInt($(this).val());


                        });
                    }
                });
                $('a.print').click(function () {

                    $('table#title-table').hide();
                });
                $(document).on("change, keyup", "#totalPrice", $('#txt_totalPrice').val($('#totalPrice').val()));

                /*
                * lấy dữ liệu routeId từ session data
                * */

                var routeId = '{{request('routeId')}}';

                if (routeId == '') {
                    routeId = sessionStorage.getItem("routeId");
                }

                if (routeId != null) {
                    selectBoxRoute.val(routeId).change();
                }


                $('#btnStartTrip').click(function () {

                    $('#txtStartTripNote').val(tripSelected.tripData.note || '');
                    $('#addDriver').val(getListNameWithArrayUser(tripSelected.tripData.listDriver).userId).change();
                    $('#addAssitant').val(getListNameWithArrayUser(tripSelected.tripData.listAssistant).userId).change();

                    $('#modalXuatben').modal('show');

                    $('#addDriver, #addAssitant').select2({
                        width: '100%',
                        dropdownParent: $('#modalXuatben'),
                        dropdownAutoWidth: true
                    });
                    // var date = $(datepickerBox.datepicker()).val();
                    // date = date.split("-");
                    // var newDate = date[1] + "/" + date[0] + "/" + date[2];
                    // date = new Date(newDate).getTime();//ngày
                    // $('#doStartTrip').prop("disabled",true);
                    // $('#doStartTrip').text("Đang lấy tài xế phụ xe");
                    // $.ajax({
                    //     dataType: "json",
                    //     url: urlSeatMapInTrip,
                    //     data: {
                    //         'tripId': tripSelected.tripData.tripId,
                    //         'scheduleId': tripSelected.tripData.scheduleId,
                    //         'date': date,
                    //     },
                    //     success: function (data) {
                    //         if(typeof data.listDri!=="undefined"){
                    //             $('#addDriver').val(getListNameWithArrayUser(data.listDri).userId).change();
                    //         }
                    //         if(typeof data.listAss!=="undefined"){
                    //             $('#addAssitant').val(getListNameWithArrayUser(data.listAss).userId).change();
                    //         }
                    //         $('#doStartTrip').prop("disabled",false);
                    //         $('#doStartTrip').text("Xuất bến");
                    //     },
                    //     error: function () {
                    //         $('#doStartTrip').prop("disabled",false);
                    //         $('#doStartTrip').text("Xuất bến");
                    //         notyMessage("lấy tài xế thất bại", "error");
                    //         Loaing(false);
                    //     }
                    // });

                    return false;
                });

                $('#btnFinishTrip').click(function () {
                    if(confirm('Hãy chắc chắn bạn đã nhập đẩy đủ thu chi của chuyến, Thao tác này chỉ có thể thực hiện 1 lần duy nhất.')){
                        SendAjaxWithJson({
                            url: urlDBD('trip/finish'),
                            type : "post",
                            dataType: "json",
                            data: {
                                tripId: tripSelected.tripData.tripId,
                            },
                            async: false,
                            success: function (data) {
                                notyMessage('Đã kết thúc chuyến','success');
                            },
                        })
                    }
                });

                $('#doStartTrip').click(function () {
                    var sendSMS = $('#sendSMSStartTrip').is(':checked') ? true : false;
                    if (tripSelected.tripData.tripStatus != 2) {
                        if (tripSelected.tripData.tripId.length < 3) {
                            notyMessage("Lỗi !chuyến chưa được khởi tạo", 'warning');
                        } else {
                            tripSelected.waitingUpdate == false;
                            SendAjaxWithParam({
                                url: urlDBD('web_trip/start-trip'),
                                type : "post",
                                dataType: "json",
                                data: {
                                    tripId: tripSelected.tripData.tripId,
                                    listDriverRealityId: JSON.stringify($('#addDriver').select2('val')||[]),
                                    listAssistantRealityId: JSON.stringify($('#addAssitant').select2('val')||[]),
                                    note: $('#txtStartTripNote').val(),
                                    sendSMS: sendSMS,
                                },
                                async: false,
                                success: function (data) {
                                    startTripFunction(data, false);
                                },
                            });
                        }
                    } else {
                        notyMessage("chuyến đã xuất bến");
                    }
                    $('#modalXuatben').modal('hide');
                });

                $('#btnOpenTrip').click(function () {
                    $('#modalMoXuatben').modal('show');

                    return false;
                });

                $('#doOpenStartTrip').click(function () {
                    console.log(tripSelected.tripData.tripStatus);
                    if (tripSelected.tripData.tripStatus >= 2) {
                        if (tripSelected.tripData.tripId.length < 3) {
                            notyMessage("Lỗi !chuyến chưa được khởi tạo", 'warning');
                        } else {
                            tripSelected.waitingUpdate == false;
                            SendAjaxWithJson({
                                url: urlDBD('trip/open'),
                                dataType: "json",
                                data: {
                                    tripId: tripSelected.tripData.tripId,
                                },
                                type: "post",
                                async: false,
                                success: function (data) {
                                    setTimeout(function () {
                                        if (tripSelected.waitingUpdate == true) {
                                            updateTripInfo(selectBoxListTrip.val(), selectBoxListTrip.find('option:selected').data('schedule'));
                                        }
                                    }, 1000);
                                    startTripFunction(data, true);
                                },
                            });
                        }
                    } else {
                        notyMessage("chuyến chưa xuất bến", 'warning');
                    }
                    $('#modalMoXuatben').modal('hide');
                });
                $('body').on('change','[name=phieuthuchi]',function(){
                    settingModalThuChi();
                });
                $('body').on('click','.btnChotChuyen',function () {
                    if(typeof tripSelected.tripData==='undefined'&&typeof tripSelected.tripData.tripId==='undefined'){
                        notyMessage('Vui lòng chọn chuyến','warning');
                        return false;
                    }
                    var routeId = $('#chontuyen').val();
                    var date = $('#txtCalendar').val();
                    var history = JSON.parse(sessionStorage.getItem('history')||'{}');
                    history.date = date;
                    history.tripId = tripSelected.tripData.tripId;
                    history.routeId = routeId;
                    sessionStorage.setItem('history',JSON.stringify(history));
                });
                $('body').on('click','.btnActionBill',function(){
                    html='<option disabled selected>Đang Tải dữ liệu...</option>';
                    settingModalThuChi();
                    $('#modalBill').modal('show');
                    SendAjaxWithJson({
                        url : urlDBD('items/getlist'),
                        type : 'post',
                        dataType : 'json',
                        data :  {
                            page:0,
                            count : 100
                        },
                        success : function(data){

                            html = ''
                            if(data.results.result.length<=0){
                                html='<option disabled selected>Không có mục nào</option>';
                            }else{
                                $.each(data.results.result,function(k,v){
                                    html+= '<option value="'+v.itemId+'">'+v.itemName+'</option>'
                                });
                            }
                            $('#slbItem').html(html);
                        },
                        functionIfError : function(res){
                            $('#slbItem').html('<option disabled>Lỗi không lấy được khoản mục</option>');
                        }
                    });
                    return false;
                });
                $('body').on('click','.btnSaveBill',function(){
                    var billType = ($('#rdPhieuChi').prop('checked')===true?2:1),
                        documentCode = $('#txtDocumentCode').val(),
                        spenderId = $('#slbSpenderId').val(),
                        receiverId = $('#slbReceiverId').val(),
                        approverId = $('#slbApproverId').val(),
                        itemId = $('#slbItem').val(),
                        number = $('#txtNumber').val(),
                        unit = $('#slbUnit').val(),
                        amount = $('#txtAmount').val(),
                        reason = $('#txtReason').val(),
                        note = $('#txtNoteBill').val();
                    if(documentCode===''){
                        notyMessage("Mã phiếu không được bỏ trống","error");
                        $('#txtDocumentCode').focus();
                        return false;
                    }
                    if(itemId===''){
                        notyMessage("Khoản mục không được bỏ trống","error");
                        $('#slbItem').focus();
                        return false;
                    }
                    if(amount===''){
                        notyMessage("Thành tiền không được bỏ trống","error");
                        $('#txtAmount').focus();
                        return false;
                    }
                    var dataSend = {
                        billType : billType,
                        documentCode : documentCode,
                        spenderId : spenderId,
                        receiverId : receiverId,
                        approverId : approverId,
                        itemId : itemId,
                        number : number,
                        unit : unit,
                        amount : amount,
                        reason : reason,
                        note : note,
                        tripId : tripSelected.tripData.tripId,
                    };
                    var  url = 'bill/create';

                    SendAjaxWithJson({
                        url : urlDBD(url),
                        type : "post",
                        dataType: 'json',
                        data  : dataSend,
                        success : function (res) {
                            notyMessage("Thêm thành công");
                            $('#modalBill').modal('hide');
                            $('.btnThuChiChuyen').click();
                        },
                        functionIfError : function (res) {

                        }
                    });
                })
                $('.slbFindAllUser').select2({
                    dropdownCss: {'z-index': '999999'},
                    width:'100%',
                    minimumInputLength: 0,
                    placeholder: "bắt buộc chọn",
                    ajax: {
                        url: urlDBD("user/getlist"),
                        contentType : "application/json; charset=utf-8",
                        type: "post",
                        headers: {
                            'DOBODY6969': '{{ session('userLogin')['token']['tokenKey'] }}',
                        },
                        delay: 1000,
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                fullName: params.term,
                                page: 0,
                                count: 20,
                                listUserType: [2,3,4,5,6]
                            };
                            return JSON.stringify(query);
                        },
                        processResults: function (data) {
                            var r = [];
                            if (data.code === 200) {
                                $.map(data.results.result, function (item) {
                                    if (data.results.result.length > 0) {
                                        r.push({text: item.fullName, id: item.userId})
                                    }
                                });
                            }
                            return {
                                results: r
                            };
                        },
                        cache: true,
                    }
                });

                $('.goodsSelectPoint').select2({
                    dropdownCss: {'z-index': '999999'},
                    width: '100%',
                    minimumInputLength: 0,
                    placeholder: "Chọn điểm",
                    allowClear: true,
                    ajax: {
                        url: urlDBD('web_point/getlist'),
                        type : "post",
                        headers : {
                            'DOBODY6969':'{{ session('userLogin')['token']['tokenKey'] }}',
                        },
                        delay: 1000,
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                page :0,
                                count:30,
                                isReceivedGoods : true,
                                keyword : params.term,
                            };
                            return query;
                        },
                        processResults: function (data) {
                            dataRs = data.code===200?data.results.result:[];
                            var r = [];
                            $.map(dataRs, function (item) {
                                r.push({text: item.pointName, id: item.pointId, address: item.address})
                            });
                            return {
                                results: r
                            };
                        },
                        cache: true
                    }
                });


                $('body').on('click','.btnAddGoods',function () {
                    $('#txtGoodsSender').val('');
                    $('#txtGoodsSenderPhone').val('');
                    $('#slbGoodsPickPointId').select2("val", "");
//                    $('#txtGoodsPickUpPoint').val('');
                    $('#txtGoodsReceiver').val('');
                    $('#txtGoodsReceiverPhone').val('');
                    $('#slbGoodsDropPointId').select2("val", "");
//                    $('#txtGoodsDropOffPoint').val('');
                    $('#txtGoodsName').val('');
                    // $('#txtGoodsValue').val('0');
                    // $('#txtGoodsTotalPrice').val('0');
                    // $('#txtGoodsPaid').val('0');
                    // $('#txtGoodsUnpaid').val('0');
                    $('#txtGoodsNote').val('');
                    if (typeof telecomCallingInfo !== 'undefined' && typeof telecomCallingInfo.status !== 'undefined' && (telecomCallingInfo.status === 18 || telecomCallingInfo.status === 28)) {
                        $('#txtGoodsSenderPhone').val(telecomCallingInfo.phoneNumber);
                        $('#txtGoodsReceiverPhone').val(telecomCallingInfo.phoneNumber);
                    }
                    $('#modalAddGoods').modal('show');
                });
                $('body').on('change keyup','#txtGoodsTotalPrice',function(){
                    if($('#txtGoodsTotalPrice').val()===''){
                        $('#txtGoodsTotalPrice').val(0)
                    }
                    var totalPriceGoods = parseInt($('#txtGoodsTotalPrice').val());
                    $('#txtGoodsPaid').val(totalPriceGoods);
                    $('#txtGoodsUnpaid').val(0);
                });
                $('body').on('change keyup','#txtGoodsPaid',function(){
                    if($('#txtGoodsTotalPrice').val()===''){
                        $('#txtGoodsTotalPrice').val(0)
                    }
                    var totalPriceGoods = parseInt($('#txtGoodsTotalPrice').val());
                    var GoodsPaid = parseInt($('#txtGoodsPaid').val());
                    $('#txtGoodsUnpaid').val(totalPriceGoods-GoodsPaid);
                });
                $('body').on('click','.btnCreateGoods',function(){
                    var sender = $('#txtGoodsSender').val();
                    var senderPhone = $('#txtGoodsSenderPhone').val();
                    var pickUpPoint = $('#txtGoodsPickUpPoint').val();
                    var receiver = $('#txtGoodsReceiver').val();
                    var receiverPhone = $('#txtGoodsReceiverPhone').val();
                    if(!receiverPhone){
                        notyMessage("Không được bỏ trống số điện thoại người nhận","error");
                        $('#txtGoodsReceiverPhone').focus();
                        return false;
                    }
                    var dropOffPoint = $('#txtGoodsDropOffPoint').val();
                    var goodsName = $('#txtGoodsName').val();
                    if(!goodsName){
                        notyMessage("Vui lòng nhập tên hàng","error");
                        $('#txtGoodsName').focus();
                        return false;
                    }
                    var goodsValue = ($('#txtGoodsValue').val()||0);
                    var totalPrice = $('#txtGoodsTotalPrice').val();
                    var paid = $('#txtGoodsPaid').val();
                    var note = $('#txtGoodsNote').val();
                    var dataSend = {
                        sender : sender,
                        senderPhone : senderPhone,
                        pickUpPoint : pickUpPoint,
                        receiver : receiver,
                        receiverPhone : receiverPhone,
                        dropOffPoint : dropOffPoint,
                        goodsName: goodsName,
                        goodsValue : goodsValue,
                        totalPrice : totalPrice,
                        paid : paid,
                        unpaid : (totalPrice-paid),
                        notice : note,
                    }
                    if(tripSelected.tripData&&tripSelected.tripData.tripId){
                        dataSend.tripId = tripSelected.tripData.tripId;
                    }
                    $('.btnCreateGoods').prop('disabled',true);
                    SendAjaxWithJson({
                        url : urlDBD('goods/v2/create-booking'),
                        type : 'post',
                        data : dataSend,
                        success: function(res){
                            notyMessage("Tạo hàng thành công","success");
                            $('.btnDanhSachHangTrenXe').click();
                            $('.btnCreateGoods').prop('disabled',false);
                            $('#modalAddGoods').modal('hide');
                        },
                        functionIfError : function (err) {
                            $('.btnCreateGoods').prop('disabled',false);
                            $('#modalAddGoods').modal('hide');
                        }
                    })
                });

                $('body').on('click','.removeGoods',function () {
                    var goodsId = $(this).attr('data-id');
                    if(!confirm("Bạn có chắc chắn muốn xóa")){
                        return false;
                    }
                    let self = this;
                    $(self).prop('disabled',true);
                    SendAjaxWithJson({
                        url : urlDBD("goods/v2/delete"),
                        type : 'post',
                        data : {
                            goodsId : goodsId,
                        },
                        async : false,
                        success: function(data){
                            $('.btnDanhSachHangTrenXe').click();
                            notyMessage("Xóa thành công",'success');
                        },
                        error : function (err) {
                            $(self).prop('disabled',false);
                            notyMessage("Xóa thất bại",'success');
                        }
                    });
                    return false;
                })
                /*
                 * đạt vé thành công sẽ trả lại param action
                 * thực hiện lệnh print nếu đã chọn checkbox in sau khi đạt vé action=print,
                 * đã tuỳ biến lại plugin printPage()
                 * */
                @if(request('action')=='print')
                $('#printTicket').printPage();
                $('#printTicket').click();
                @endif

                /*
                * Tự động chọn vé theo ticketId trên url
                * */
                var ticketId = "{{request('ticketId')}}";

                if (ticketId != '') {
                    $(selectBoxRoute).val("{{request('routeId')}}").change();
                    tripSelected.tripId = "{{request('tripId')}}";
                    $(datepickerBox.datepicker("setDate", "{{request('date')}}"));
                    updateListPoint("{{request('routeId')}}", true);
                } else {
                    updateListPoint($(selectBoxRoute).val(), true);
                }


                $('#loading').addClass('loading');
//                khởi tạo kết nối ghế trống và thay đổi khi thay đổi tuyến
                selectBoxRoute.on('change', function (e) {
                    val = $(selectBoxRoute).val();
                    listColumnTripShowVer1 = JSON.parse(localStorage.getItem('listColumnTripShowVer1') || '[]');
                    flag = false;
                    $.each(listColumnTripShowVer1, function (i, e) {
                        if (e === val) {
                            flag = true;
                        }
                    });
                    if (flag == false) {
                        listColumnTripShowVer1.push(val);
                        localStorage.setItem('listColumnTripShowVer1', JSON.stringify(listColumnTripShowVer1));
                    }
                    sessionStorage.setItem('tripId', '');
                    Loaing(true);
                    reset();
                    updateListPoint($(this).val(), false);
                    // $(".ui-datepicker-trigger").click();
                    saveFormDataLocal();
                    ChangeUrl();
                    Loaing(false);
                });

                $(".oneDayBack").click(function (e) {
                    $(this).next().datepicker('setDate', 'c-1d').change();
                });

                $(".oneDayFwd").click(function (e) {
                    $(this).prev().datepicker('setDate', 'c+1d').change();
                });

                $(datepickerBox).change(function () {
                    if (changeDatePickerBox) {
                        sessionStorage.setItem('tripId', '');
                        reset();
                        changeListTrip();
                        saveFormDataLocal();
                        ChangeUrl();
                        Loaing(false);
                    }
                });

//                $("#cbGetDropOffAddress").change(function () {
//                    $("#dropOffAddress").prop('readonly', !this.checked).val('');
//                });
//
//                $("#cbGetPickUpAddress").change(function () {
//                    $("#pickUpAddress").prop('readonly', !this.checked).val('');
//                });

                $('.CustomerPhoneNumber').bind('paste', function (e) {
                    var pastedData = e.originalEvent.clipboardData.getData('text');
                    var val = pastedData.replace(/[^0-9]/gi, '');
                    $(this).val(val).focus();
                    e.preventDefault();
                });
                /*
                * Hiện danh sách hành khách
                * */
                $('#customerList').click(function () {
                    $('.khung_lich_ve').hide();
                    $('.khung_khach_hang').show();
                });
                /*
                * Hiện danh sách ghế bán vé
                * */
                $('#ticketList').click(function () {
                    $('.khung_lich_ve').show();
                    $('.khung_khach_hang').hide();
                });

                /*
                * khi chọn chuyến
                * */
                selectBoxListTrip.on('change', function (e) {
                    $('#ticketList').click();
                    $('.toggletabtang1').click();
                    newBuildHtmlTrip();
                    saveFormDataLocal();
                    ChangeUrl();
                    // setTimeout(function(){
                    //     $('html, body').animate({
                    //         scrollTop : $('#table_SaoNghe').offset().top
                    //     });
                    // },1000);
                    //$("html, body").animate({scrollTop: $(window).height()}, "slow");
                    changeDatePickerBox = false;
                    $('#txtCalendar').val(formatDate(tripSelected.tripData.startTime));
                    changeDatePickerBox = true;
                    Loaing(false);
                });

                $('body').on('click', '.seatChecked', function () {
                    if ($(this).parents('.ghe').hasClass('ghegiucho')) {//chỉ thực hiện khi dã có vé
                        $(this).parents('.ghe').css('cursor', 'progress ');
                        var isChecked = $(this).hasClass('isChecked') ? false : true;
                        var ticketIdCheck = $(this).parents('[data-ticketid]').data('ticketid');
                        $.ajax({
                            url: '{{ action('TicketController@updateCheckTicket') }}',
                            dataType: 'json',
                            data: {
                                tripId: tripSelected.tripData.tripId,
                                isChecked: isChecked,
                                ticketId: ticketIdCheck,
                                _token: '{{ csrf_token() }}'
                            },
                            type: 'post',
                            success: function (data) {
                                if (data.code == 200) {
                                    notyMessage('Cập nhật kiểm vé thành công', 'success');
                                } else {
                                    Message('Cập nhật kiểm vé thất bại', 'error');
                                }
                                $(this).parents('.ghe').css('cursor', 'pointer');
                            },
                            error: function (data) {
                                $(this).parents('.ghe').css('cursor', 'pointer');
                            }
                        });
                    }
                    return false;
                });

                /*
                * sự kiện khi chọn ghế để bán vé
                * */
                $('body').on('click', 'div.ghe', function (e) {
                    var id = $(this).data('ticketid');
                    if (flashMoveSeat) {
                        if (tripSelected.tripData.tripStatus !== 2) {
                            if (id == 'undefined') {
                                $(this).toggleClass('ghechuyenden');
                                if ($('.ghechuyenden').length == moveSeatInfo.listSeatOld.length && moveSeatInfo.listSeatOld.length > 0) {
                                    var listSeatNew = [];
                                    $.each($('.ghechuyenden'), function (i, e) {
                                        listSeatNew.push($(e).find('.seatId').text());
                                    });
                                    flashMoveSeatFunction(moveSeatInfo.listSeatOld, listSeatNew, 'chuyenghe');
                                    flashMoveSeat = false;
                                }
                            } else {
                                if (moveSeatInfo.ticketId != id) {
                                    $('.ghe').removeClass('ghechuyenden');
                                    flashMoveSeat = false;
                                    $(this).click();
                                } else {
                                    $(this).toggleClass('ghedangchon');
                                    $(this).toggleClass('ghedichuyen');
                                    var listSeatOld = [];
                                    $.each($('.ghedichuyen'), function (i, e) {
                                        listSeatOld.push($(e).find('.seatId').text());
                                    });
                                    moveSeatInfo.listSeatOld = listSeatOld;

                                    if (moveSeatInfo.listSeatOld.length == 0) {
                                        flashMoveSeat = false;
                                    }
                                    if (moveSeatInfo.listSeatOld.length == $('.ghedichuyen').length && moveSeatInfo.listSeatOld.length > 0) {
                                        var listSeatNew = [];
                                        $.each($('.ghechuyenden'), function (i, e) {
                                            listSeatNew.push($(e).find('.seatId').text());
                                        });

                                        flashMoveSeatFunction(moveSeatInfo.listSeatOld, listSeatNew, 'chuyenghe');
                                        flashMoveSeat = false;
                                    }
                                }
                            }
                        } else {
                            notyMessage('vui lòng chọn chuyến chưa xuất bến', 'error');
                        }
                    } else {

                        if ($('.ghedangchon').data('ticketid') != id) {
                            $('.ghe').removeClass('ghedangchon');
                            $('.ghe').removeClass('istrue');
                            $('.ghe').removeClass('ghedichuyen');
                            if (addSeat.flag == true && !$(this).hasClass('ghetrong')) {
                                addSeat.flag = false;
                                addSeat.ticketInfo = {};
                                notyMessage('Rời khỏi chế độ thêm ghế!bạn đang ở chế độ bình thường!');
                            }
                        }
                        if ($(this).hasClass("ghedadat") || $(this).hasClass("ghegiucho") || $(this).hasClass("ghelenxe")) {
                            if ($(this).hasClass("istrue")) {
                                selectSeat(this);
                            } else {

                                jQuery.each($("[data-ticketid='" + id + "']"), function (k, v) {
                                    selectSeat(v);
                                    $(v).addClass("istrue");
                                });
                            }
                        }
                        if ($(this).hasClass("ghetrong")) {
                            selectSeat(this);
                        }
                        updatePrice();
                    }
                    listSeatId = [];
                    $.each($('.ghedangchon'), function (index, element) {
                        listSeatId.push($(element).find('.seatId').text());
                    });
                    listSeatReActive = listSeatId;
                });

                // sự kiện khi chọn ghế đã có vé

                // $('body').delegate('div.ghedadat,div.ghegiucho','click',function(){
                //     if($(this).hasClass("istrue")){
                //         $(this).toggleClass("ghedangchon");
                //     }else{
                //         $('div.ghe').removeClass("ghedangchon");
                //         $('div.ghe').removeClass("istrue");
                //         var id = $(this).data('ticketid');
                //         $("[data-ticketid='"+id+"']").addClass("ghedangchon istrue");
                //     }
                // });

                /*
                * sự kiện khi thay đổi điểm lên hoặc điểm xuống
                * */
                $('#cbb_InPoint,#cbb_OffPoint').change(function (e) {
                    if ($(e.currentTarget).is('#cbb_InPoint')) {
                        var index = $("#cbb_InPoint option:selected").index();
                        var query = document.querySelector('select#cbb_OffPoint');
                        var listOp = query.querySelectorAll('option');
                        for (var i = 0; i < listOp.length; i++) {
                            if (i < index) listOp[i].hidden = true;
                            else listOp[i].hidden = false;
                        }
                        if ($("#cbb_OffPoint option:selected").index() < index) listOp[index].selected = true;
                    }
                    changeListTrip();
                    saveFormDataLocal();
                });


                /*
                * sự kiện nhập mã khuyến mãi
                * */
                $('.btnCheckPromotionCode').click(function () {
                    // $('#tongGiaVeGuiDo').text('Đang cập nhật giá...');
                    var btn = this;

                    if ($(btn).text() === 'X') {
                        $('.salePrice').val(0);
                        $('.promotionId').val('');
                        $('.promotionCode').val('');
                        $('.promotionPercent').val('-1');
                        $('.promotionPrice').val('-1');
                        $(btn).prev('input').removeAttr('readonly').val('');
                        $('#hasPromotionRound').val(-1);
                        $(this).text('Kiểm tra');
                        updateInputPrice(true);
                        updateInputPriceRound();
                    } else {
                        if ($(this).prev('input').val() != '') {
                            $(btn).html('<img src="/public/images/loading/loader_blue.gif" width="19px">');
                            $(btn).prop('disabled', true);
                            $.ajax({
                                'url': urlCheckPromotionCode,
                                'data': {
                                    'scheduleId': $('#txt_scheduleId').val(),
                                    'routeId': $('#chontuyen option:selected').val(),
                                    'tripId': $('#listTrip option:selected').val(),
                                    'promotionCode': $(this).prev('input').val(),
                                    'getInTimePlan': tripSelected.tripData.startTime,
                                    'pricePerSeat' : tripSelected.tripData.ticketPrice,
                                },
                                'dataType': 'json',
                                'success': function (data) {
                                    var result = data.result;
                                    if (result.percent != undefined && result.price != undefined) {
                                        $('.promotionPercent').val(result.percent);
                                        $('.promotionPrice').val(result.price);
                                        $('.promotionId').val(result.promotionId);
                                        $('.promotionCode').val(result.promotionCode);
                                        if (result.percent > 0) {
                                            $(btn).prev('input').val(result.promotionCode + ' - Giảm ' + result.percent * 100 + '%')
                                        }
                                        if (result.price > 0) {
                                            $(btn).prev('input').val(result.promotionCode + ' - Giảm ' + moneyFormat(result.price))
                                        }
                                        $(btn).prev('input').attr('readonly', 'readonly');
                                        $(btn).text('X');
                                        $(btn).prop('disabled', false);
                                    } else {
                                        notyMessage('Mã khuyến mãi không hợp lệ', 'error');
                                        $(btn).text('Kiểm tra');
                                        $(btn).prop('disabled', false);
                                    }
                                    updateInputPrice(true);
                                },
                                'error': function () {
                                    notyMessage('Lấy mã khuyến mãi thất bại! Vui lòng tải lại trang.', 'error');
                                    $(btn).text('Kiểm tra');
                                    $(btn).prop('disabled', false);

                                }
                            });
                            if (totalPriceBack > 0) {
                                $.ajax({
                                    'url': urlCheckPromotionCode,
                                    'data': {
                                        'scheduleId': $('#txt_scheduleId').val(),
                                        'routeId': $('#cbbRoundRouteId option:selected').val(),
                                        'tripId': $('#cbbRoundTripId option:selected').val(),
                                        'promotionCode': $(this).prev('input').val(),
                                        'getInTimePlan': TripData_Round.startTime
                                    },
                                    'dataType': 'json',
                                    'success': function (data) {
                                        var result = data.result;
                                        if (result.percent != undefined && result.price != undefined) {
                                            $('#hasPromotionRound').val(1);
                                            updateInputPriceRound();
                                        }

                                    }
                                });
                            }
                        }

                    }


                });

                /*
                * khi chọn loại thanh toán là payoo
                * */
                $('.cbb_paymentType').change(function () {
                    if ($(this).val() == 5) {
                        $('iframe').attr('src', 'https://www.payoo.vn/diem-giao-dich');
                        $('#Payoo').show();
                    } else {
                        $('#Payoo').hide();
                    }
                });
                // sự kiện click nút in ticket
                $('body').on('click', 'button.btnPrintTicket', function () {
                    printTicket($(this).val(), $(this).data('code'));
                });
                /*

                *sự kiện khi chọn bao gồm ăn hoặc bao gồm bảo hiểm
                * */
                $('#cb_priceInsurrance,#cb_isMeal').change(function () {
                    updateInputPrice(true);
                });

                $('body').on('click', 'button.btnFlashMoveSeat', function () {
                    var ticketId = $(this).parents('.ghe').data('ticketid');
                    if ($('.ghedangchon').data('ticketid') != ticketId) {
                        flashMoveSeat = false;
                    }
                    flashMoveSeat = !flashMoveSeat;
                    $('.ghe').removeClass('ghechuyenden');
                    if (!$(this).parents('.ghe').hasClass("ghedangchon")) {
                        $(this).parents(".ghe").click();
                    }
                    var listSeatMove = [];
                    $('.ghe').removeClass('ghedichuyen');
                    if (flashMoveSeat) {
                        $('.ghetrong').css('cursor', 'move');
                        $.each($('.ghedangchon'), function (i, e) {
                            $(e).addClass('ghedichuyen');
                            listSeatMove.push($(e).find('.seatId').text());
                        });
                        moveSeatInfo.option = "chuyenghe";
                        notyMessage('chọn ' + listSeatMove.length + ' ghế mới hoặc bấm Alt + X để hủy bỏ hành động', 'info');
                    } else {
                        $('.ghetrong').css('cursor', 'pointer');
                        $.each($('.ghedangchon'), function (i, e) {
                            $(e).removeClass('ghedichuyen');
                        });
                    }
                    moveSeatInfo.listSeatOld = listSeatMove;
                    moveSeatInfo.tripOld = tripSelected.tripData.tripId;
                    moveSeatInfo.ticketId = ticketId;
                    return false;
                });

                $('body').on('click', '.cancelTicket', function () {
                    ticketId = $(this).parents('.ghe[data-ticketid]').attr('data-ticketid');
                    $("#datve").find("[name='ticketId']").val(ticketId);
                    // $(this).parents('.ghe').find('.editTicket').click();
                    $('#huyve').click();
                    return false;
                });

                $('body').on('click','.cancelSecondaryTicket',function () {
                    var ticketId = $(this).data('ticketid');
                    if(confirm("bạn chắc chắn muốn xóa vé này ?")){
                        SendAjaxWithJson({
                            type:"post",
                            url : urlDBD('/ticket_secondary_seat/cancel'),
                            dataType : "json",
                            data : {ticketId : ticketId},
                            success :function (res) {
                                notyMessage("Xóa thành công",'success');
                            }
                        })
                    }
                    return false;
                });

                $('.btnTabContentHistoryTicket').click(function () {
                    $('.btnTabContentHistoryTicketSell').removeClass('active');
                    $(this).addClass('active');
                    $('#contentHistoryTicket').show();
                    $('#contentHistoryTicketSell').hide();
                });
                $('.btnTabContentHistoryTicketSell').click(function () {
                    if ($('.btnTabContentHistoryTicketSell').attr('data-loading') === "loading") {
                        $('.btnTabContentHistoryTicketSell').attr('data-loading', 'loaded');
                        var html = '';
                        html += '<table class="table" style="width:100%;">';
                        html += '<tr><td class="text-center" colspan="3">Đang tải dữ liệu...</td></tr>';
                        html += '</table>';
                        $('#contentHistoryTicketSell').html(html);
                        var phoneNumber = $('.btnTabContentHistoryTicketSell').attr('data-phone');
                        $.ajax({
                            dataType: "json",
                            url: "{{action('HomeController@searchPhone')}}",
                            data: {
                                term: phoneNumber,
                                count: 20,
                            },
                            success: function (data) {
                                html = '';
                                html += '<table class="table" style="width:100%;">';
                                html += '<thead><tr><th>Tuyến</th><th>Ngày</th><th>Ghế</th><th>Trạng thái</th></tr></thead>';
                                html += '</tbody>';
//                                console.log(data);
                                if (data.length > 0) {
                                    html += '<tr><td colspan="4" style="font-size:16px">Số vé đã đặt : <span style="color:#00aa00;font-weight:bold">' + (data[0].numberOfTickets || 0) + '</span> , Số vé đã hủy : <span style="color:#942a25;font-weight:bold">' + (data[0].numberCancelTicket || 0) + '</span></td></tr>';
                                    $.each(data, function (k, v) {
                                        if (typeof v.routeName !== 'undefined') {
                                            html += '<tr class="link-phone-phuc-xuyen" title="chuyển đến vé này">';
                                            html += '<td><span class="dataTicket" style="display: none">' + JSON.stringify(v) + '</span>' + v.routeName + '</td>';
                                            html += '<td>' + checkDate(v.getInTimePlan) + '</td>';
                                            html += '<td>' + (typeof v.listSeatId !== 'undefined' && v.listSeatId !== '' ? v.listSeatId.toString() : '') + '</td>';
                                            html += '<td>' + checkStatusTicket(v.ticketStatus) + '</td>';
                                            html += '</tr>';
                                        }
                                    })
                                } else {
                                    html = '<tr><td colspan="3">Không có dữ liệu</td></tr>';
                                }
                                html += '</tbody>';
                                html += '</table>';
                                $('#contentHistoryTicketSell').html(html);
                            },
                            error: function () {
                                notyMessage("Lỗi!Vui lòng tải lại trang", "error");
                                html = '<tr><td class="text-center" colspan="3">Không có dữ liệu</td></tr>';
                                $('#contentHistoryTicket').html(html);
                            }
                        });
                    }
                    $('.btnTabContentHistoryTicket').removeClass('active');
                    $(this).addClass('active');
                    $('#contentHistoryTicketSell').show();
                    $('#contentHistoryTicket').hide();
                });

                $('body').on('click', '.historyTicket', function () {
                    $('.btnTabContentHistoryTicketSell').attr('data-loading', "loading");
                    $('.btnTabContentHistoryTicketSell').removeClass('active');
                    $('.btnTabContentHistoryTicket').addClass('active');
                    $('#contentHistoryTicket').show();
                    $('#contentHistoryTicketSell').hide();
                    var ticketId = $(this).data('ticketid-history');
                    var html = '';
                    html += '<table class="table" style="width:100%;">';
                    html += '<tr><td class="text-center" colspan="3">Đang tải dữ liệu...</td></tr>';
                    html += '</table>';
                    $('#contentHistoryTicket').html(html);
                    //Lay lich su ve
                    SendAjaxWithJson({
                        url: urlDBD('ticket/viewLog'),
                        type: "post",
                        data: {
                            ticketId: ticketId
                        },
                        dataType: "json",
                        success: function (data) {
                            html = '';
                            html += '<table class="table" style="width:100%;">';
                            html += '<thead><tr><th>Thời điểm sửa</th><th>Người Sửa</th><th>Nội dung sửa</th></tr></thead>';
                            html += '</tbody>';
                            if (data.results.logs.length > 0) {
                                $('.btnTabContentHistoryTicketSell').attr('data-phone', getLastPhoneLog(data.results.logs));
                                $.each(data.results.logs, function (k, v) {
                                    html += "<tr>";
                                    html += "<td style='vertical-align:middle'>" + getFormattedDate(v.createdDate) + "</td>";
                                    html += "<td style='vertical-align:middle'>" + v.createdUserName + "</td>";
                                    html += "<td>";
                                    html += '<h5 style="font-weight:bold;color:#bb200f;margin-bottom:0px;">' + checkHistoryType(parseInt(v.logType)) + '</h5>';
                                    html += generateHtmlTicketLog(v.logs);
                                    html += '<div style="text-align:right"><span>Nghe lại cuộc gọi lúc tạo vé</span> <button class="btn btn-warning btnHistoryCallingPX" title="Cuộc gọi gần thời điểm này" data-time="' + v.createdDate + '"><i class="fas fa-phone"></i></button></div>';
                                    html += "</td>";
                                    html += "</tr>";
                                });
                            } else {
                                html = '<tr><td colspan="3">Không có dữ liệu</td></tr>';
                            }
                            html += '</tbody>';
                            html += '</table>';
                            $('#contentHistoryTicket').html(html);
                        },
                        functionIfError: function (data) {
                            html = '<tr><td class="text-center" colspan="3">Không có dữ liệu</td></tr>';
                            $('#contentHistoryTicket').html(html);
                        }
                    });
                    $('#modalTicketHistory').modal('show');
                    return false;
                });


                $('#DSChuyen').click(function () {
                    $('.box-route-and-trip').slideToggle();
                });

                $('body').on('click', '.cloneTicket', function () {
                    if (flashMoveSeat == false) {
                        $('.ghetrong').css('cursor', 'move');
                        flashMoveSeat = true;
                        moveSeatInfo.listSeatOld = $(this).data('listseat').toString().split(',');
                        moveSeatInfo.tripOld = $(this).data('tripid');
                        moveSeatInfo.ticketId = $(this).data('ticketid');
                        moveSeatInfo.option = 'khoiphuc';
                        notyMessage('chế độ phục hồi. Chọn ' + moveSeatInfo.listSeatOld.length + ' ghế mới hoặc bấm Alt + X để hủy bỏ hành động', 'info');
                    } else {
                        if (moveSeatInfo.ticketId == $(this).data('ticketid')) {
                            flashMoveSeat = false;
                            moveSeatInfo = {};
                            notyMessage('Chế độ bình thường', 'info');
                            $('.ghetrong').css('cursor', 'default');
                        } else {
                            $('.ghetrong').css('cursor', 'move');
                            flashMoveSeat = true;
                            moveSeatInfo.listSeatOld = $(this).data('listseat').toString().split(',');
                            moveSeatInfo.tripOld = $(this).data('tripid');
                            moveSeatInfo.ticketId = $(this).data('ticketid');
                            moveSeatInfo.option = 'khoiphuc';
                            notyMessage('chế độ phục hồi. Chọn ' + moveSeatInfo.listSeatOld.length + ' ghế mới hoặc bấm Alt + X để hủy bỏ hành động', 'info');
                        }
                    }
                    return false;
                });
                /*Sự kiện khi click vào nút sửa vé*/
                $('body').on('click', 'button.editTicket', function (e) {
                    $('#round-trip').hide();
                    $('.disableAction').hide();
//                    reset trung chuyển
                    $('.transshipmentInPoint').val('').change();
                    $('.transshipmentOffPoint').val('').change();

                    $('form#datve').attr('action', urlUpdateStatusTicket);
                    $('.banve_thongtintinve').show();
                    $('.modal-footer').show();
                    $('.banve_lichsuve').hide();
                    var ticketIdSeat = $(this).parents('.ghe').data('ticketid');
                    if (!$(this).parents('.ghe').hasClass("ghedangchon")) {
                        $(this).parents(".ghe").click();
                    }
                    var seatIdCurrent = $(this).data('seatid');
                    if (tripSelected.tripData.tripStatus == 2) {
                        $('#themghe').hide();
                    } else {
                        $('#themghe').show();
                    }
                    $('#themghe').attr('data-seat-click', seatIdCurrent);
                    var seat = $.grep(tripSelected.tripData.seatMap.seatList, function (item) {
                        return item.seatId == seatIdCurrent && (item.seatType == 3 || item.seatType == 4);
                    })[0];
                    var ticketInfo = seat['ticketInfo'][0] || [];

                    var ticketPriceValue = '';
                    var listExtraPrice = '';
                    var listExtraTK = '';
                    var listSeatId = [];
                    var lengthItem = $('.ghedangchon').length;
                    $('.ghedangchon').each(function (key, elem) {
                        var extraPrice = $(elem).data('extraprice');
                        var num = $.number(extraPrice + $('#ticketPrice').data("ticketprice"));
                        ticketPriceValue = ticketPriceValue + num;
                        listExtraPrice += extraPrice;
                        if (key != lengthItem - 1) {
                            ticketPriceValue += " , ";
                            listExtraPrice += ",";
                        }
                        listSeatId.push($(elem).find('.seatId').text());
                    });
                    listSeatReActive = listSeatId;
                    $("[data-ticketid='" + ticketIdSeat + "']").each(function (i, e) {
                        listExtraTK += $(e).data('extraprice');
                        if (i != $("[data-ticketid='" + ticketIdSeat + "']").length - 1) {
                            listExtraTK += ",";
                        }
                    });
//                    ẩn khứ hồi
                    $('#divroundtrip').hide();
                    //c gửi sms
//                    if (ticketInfo['sendSMS']) {
//                        $('#send_code_to_phone').prop('checked', true);
//                    }
                    //$('#ticketPrice').val(ticketPriceValue);
                    $('#numberOfChildren').val(ticketInfo['numberOfChildren']);
                    $('#numberOfAdults').val(ticketInfo['numberOfAdults']);
                    $('#lb_numberOfAdults').val(ticketInfo['numberOfAdults'] + ' (' + $('#listSeat').text() + ')');
                    $('#txtListExtraPrice').val(listExtraPrice);
                    $('#txtListExtraTK').val(listExtraTK);
                    $('#txtforeignKey').val(ticketInfo['foreignKey']);
                    $('.CustomerPhoneNumber').val(ticketInfo['phoneNumber']);
                    var timeBookHolder = (ticketInfo['overTime']- new Date().getTime())>0 ? ticketInfo['overTime']- new Date().getTime() : 0;
                    $('#txtTimeBookHolder').val(Math.ceil(timeBookHolder/60000));
//                    cập nhật lại điểm lên xuống của vé
                    $('#dropOffLat').val(ticketInfo['latitudeDown']);
                    $('#dropOffLong').val(ticketInfo['longitudeDown']);
                    $('#pickUpLat').val(ticketInfo['latitudeUp']);
                    $('#pickUpLong').val(ticketInfo['longitudeUp']);
//                   console.log(ticketInfo);
//                    lựa chon trung chuyển
                    if (ticketInfo['pickUpAddress'] !== undefined && ticketInfo['pickUpAddress'] !== '') {
                        if (typeof tripSelected.tripData.transshipmentInPoint !== 'undefined') {
                            pointIn = $.grep(tripSelected.tripData.transshipmentInPoint, function (e, i) {
                                return e.pointName == ticketInfo['pickUpAddress'];
                            })[0] || {};
                            if (typeof pointIn.pointId !== 'undefined') {
                                $('#rdTransshipmentUp').prop('checked', true);
                                $('.transshipmentInPoint').val(pointIn.pointId).trigger('change');
                                $('select.transshipmentInPoint').show();
                                $('#pickUpAddress').hide();
                            } else {
                                $('#rdPickUpAddress').prop('checked', true);
                                $('#pickUpAddress').val(ticketInfo['pickUpAddress']);
                                $('select.transshipmentInPoint').hide();
                                $('#pickUpAddress').show();
                            }
                        } else {
                            $('#rdPickUpAddress').prop('checked', true);
                            $('#pickUpAddress').val(ticketInfo['pickUpAddress']);
                            $('select.transshipmentInPoint').hide();
                            $('#pickUpAddress').show();
                        }
                    } else {
                        if (ticketInfo['alongTheWayAddress'] !== undefined && ticketInfo['alongTheWayAddress'] !== '') {
                            $('#rdAlongTheWayAddress').prop('checked', true);
                            $('#pickUpAddress').val(ticketInfo['alongTheWayAddress']);
                            $('select.transshipmentInPoint').hide();
                            $('#pickUpAddress').show();
                        } else {
                            $('#rdPickUpAddress').prop('checked', true);
                            $('#pickUpAddress').val('');
                            $('select.transshipmentInPoint').hide();
                            $('#pickUpAddress').show();
                        }
                    }
                    if (ticketInfo['dropOffAddress'] !== undefined && ticketInfo['dropOffAddress'] !== '') {
                        if (typeof tripSelected.tripData.transshipmentOffPoint !== 'undefined') {
                            pointOff = $.grep(tripSelected.tripData.transshipmentOffPoint, function (e, i) {
                                return e.pointName == ticketInfo['dropOffAddress'];
                            })[0] || {};
                            if (typeof pointOff.pointId !== 'undefined') {
                                $('#rdTransshipmentDown').prop('checked', true);
                                $('.transshipmentOffPoint').val(pointOff.pointId).trigger('change');
                                $('select.transshipmentOffPoint').show();
                                $('#dropOffAddress').hide();
                            } else {
                                $('#rdDropOffAddress').prop('checked', true);
                                $('#dropOffAddress').val(ticketInfo['dropOffAddress']);
                                $('select.transshipmentOffPoint').hide();
                                $('#dropOffAddress').show();
                            }
                        } else {
                            $('#rdDropOffAddress').prop('checked', true);
                            $('#dropOffAddress').val(ticketInfo['dropOffAddress']);
                            $('select.transshipmentOffPoint').hide();
                            $('#dropOffAddress').show();
                        }
                    } else {
                        if (ticketInfo['dropAlongTheWayAddress'] !== undefined && ticketInfo['dropAlongTheWayAddress'] !== '') {
                            $('#rdDropAlongTheWayAddress').prop('checked', true);
                            $('#dropOffAddress').val(ticketInfo['dropAlongTheWayAddress']);
                            $('select.transshipmentOffPoint').hide();
                            $('#dropOffAddress').show();
                        } else {
                            $('#rdDropOffAddress').prop('checked', true);
                            $('#dropOffAddress').val('');
                            $('select.transshipmentOffPoint').hide();
                            $('#dropOffAddress').show();
                        }
                    }
                    $('.CustomerFullName').val(ticketInfo['fullName']);
                    $('.CustomerEmail').val(ticketInfo['email']);
                    $('#cbbCreatedUser').html('<option value="' + (ticketInfo['agencyUserId'] || '') + '">' + ticketInfo['sourceName'] + '</option>');
//                    $('#cbbCreatedUser').val(ticketInfo['agencyUserId']);
//                    $('#select2-cbbCreatedUser-container').text(ticketInfo['sourceName']);
                    $('#txtNote').val(ticketInfo['note']);
                    ticketInfo['priceInsurrance'] > 0 ? $('#cb_priceInsurrance').prop('checked', true) : $('#cb_priceInsurrance').prop('checked', false);
                    ticketInfo['mealPrice'] > 0 ? $('#cb_isMeal').prop('checked', true) : $('#cb_isMeal').prop('checked', false);
                    if (ticketInfo['mealPrice'] > 0) $('#isMeal').show();
                    if (ticketInfo['priceInsurrance'] > 0) $('#isInsurrance').show();

//                    mã khuyến mại
                    if (ticketInfo.listPromotion&&ticketInfo.listPromotion.length>0) {
                        var promotion = ticketInfo.listPromotion[0];
//                        nếu đã tồn tại mã khuyến mại thì hiển thị và không cho sửa
                        if (promotion.percent > -1) {
                            $('.txtPromotionCode').val(promotion.promotionCode + ' - Giảm : ' + (promotion.percent * 100) + ' % ');
                        } else {
                            $('.txtPromotionCode').val(promotion.promotionCode + ' - Giảm : ' + $.number(promotion.price) + ' VND ');
                        }
                        $('.promotionPercent').val(promotion.percent);
                        $('.promotionPrice').val(promotion.price);
                        $('.txtPromotionCode').prop('readonly', true);
                        $('.btnCheckPromotionCode').hide();
                    } else {
//                        chưa tồn tại thì cho phép sửa
                        $('.promotionPercent').val(-1);
                        $('.promotionPrice').val(-1);
                        $('.promotionId').val('');
                        $('.promotionCode').val('');
                        $('.txtPromotionCode').val('');
                        $('.txtPromotionCode').prop('readonly', false);
                        $('.btnCheckPromotionCode').text('Kiểm tra');
                        $('.btnCheckPromotionCode').show();
                    }

//                    cập nhật commission đại lí
                    if (ticketInfo.commissionAgency != undefined) {
                        $('#txtCommissionType').val((ticketInfo.commissionAgency.commissionType || 1));
                        $('#txtCommissionValue').val((ticketInfo.commissionAgency.commissionValue || 0));
                        console.log(ticketInfo.commissionAgency);
                    }
                    $('#totalPriceShow').val(moneyFormat(ticketInfo['paymentTicketPrice']));
                    $('#realPrice').val(ticketInfo['agencyPrice']);
                    $('#realPriceShow').val(moneyFormat(ticketInfo['agencyPrice']));
                    $('#txtPaidMoney').val(ticketInfo['paidMoney']);
                    $('#txtThuThem').val(ticketInfo['extraPrice'] || 0);
                    $('#txtMoneyOwing').val(moneyFormat(ticketInfo['unPaidMoney']));
                    updateInputPrice(false);
                    var listPrice= [];
                    $.each(ticketInfo['listOption'],function(i,e){
                        listPrice.push(moneyFormat(e.originPrice+e.extraPrice));
                    });
                    $('#listTicketPrice').val(listPrice.toString());
                    if (ticketInfo['ticketStatus'] == 3) {
                        $('#thanhtoan').hide();
                    }
                    else {
                        $('#thanhtoan').show();
                    }

                    $('.isSellTicket').hide();
                    $('.isUpdateStatusTicket').show();
                    if (tripSelected.tripData.tripStatus == 2) {
                        $('#huyve').hide();
                        $('#capnhat').hide();
                    } else {
                        $('#huyve').show();
                        $('#capnhat').show();
                    }

                    if($('#cbbCreatedUser').val()===''){
                        $('.nhaxethuho').hide();
                    }else{
                        $('.nhaxethuho').show();
                    }
                    if(typeof ticketInfo.collectMoneyForAgency!=='undefined'&&ticketInfo.collectMoneyForAgency===true){
                        $("#collectMoneyForAgency").prop('checked',true);
                    }else{
                        $("#collectMoneyForAgency").prop('checked',false);
                    }

                    $('#txtTicketId').val($(this).val());
                    //showHideRoundTrip($('#txtRoundTrip'));
                    if ('{{session('companyId')}}' == 'TC03j1IanqGKIUS') {
                        $('#textChildren').text('Ghế miễn phí');
                        $('#huyve').hide();
                        $('#capnhat').hide();
                        $('#giucho').hide();
                    }
                    $('#sell_ticket').modal('show');
                    return false;
                });
                $("body").on('click', "[aria-hidden='true']", function () {
                    $('.modal-footer').show();
                });
                $('#MoveRoundTrip').click(function () {
                    $('#sell_ticket').on('hidden', function () {
                        var listghe = document.getElementsByClassName('ghe');
                        if (sessionStorage.getItem('seatRound') != '') {
                            $.each(listghe, function (k, v) {
                                if ($(v).data('ticketid') == sessionStorage.getItem('seatRound')) {
                                    $(v.parentElement.parentElement).addClass('active');
                                    $(v).addClass('ghedangchon');
                                    v.scrollIntoView();
                                }
                            });
                        }
                        sessionStorage.setItem('seatRound', '');
                    });
                });
                $('#sell_ticket').on('hidden', function () {
                    if ($('.ghetrong.ghedangchon').length <= 0) {
                        $('.ghedangchon').each(function (key, elem) {
                            selectSeat(elem);
                        });
                    }
                    resetInput();

                });
                $('#sell_ticket').on('click', "[aria-hidden='true']", function () {
                    lockOrUnlockSeat("unlock");
                });
                $('body').on('click', 'button.btnSellTicket', function (e) {
                    $('#sell-ticket').click();
                    $('.disableAction').hide();
                    $('.transshipmentInPoint').val('').change();
                    $('.transshipmentOffPoint').val('').change();
                    $('#rdPickUpAddress').click();
                    $('#rdDropOffAddress').click();
                    $('#dropOffLat').val(tripSelected.tripData.latitudePointEnd);
                    $('#dropOffLong').val(tripSelected.tripData.longitudePointEnd);
                    $('#pickUpLat').val(tripSelected.tripData.latitudePointStart);
                    $('#pickUpLong').val(tripSelected.tripData.longitudePointStart);
                    if (typeof tripSelected.tripData.commission !== 'undefined') {
                        $('#txtOneSeatPrice').val(tripSelected.tripData.ticketPrice);
                        $('#txtCommissionType').val(tripSelected.tripData.commission.commissionType);
                        $('#txtCommissionValue').val(tripSelected.tripData.commission.commissionValue);
                    }

                    $('#total_Price_Back').val(0);
                    if (addSeat.flag) {
                        $('.isAddSeat').show();
                        $('.isntAddSeat').hide();
                        $('#checkRoundTrip').hide();
                        $('.CustomerPhoneNumber').val(addSeat.ticketInfo['phoneNumber'] || '');
                        $('#pickUpAddress').val(addSeat.ticketInfo['pickUpAddress'] || '');
                        $('#dropOffAddress').val(addSeat.ticketInfo['dropOffAddress'] || '');
                        $('.CustomerFullName').val(addSeat.ticketInfo['fullName'] || '');
                        $('.CustomerEmail').val(addSeat.ticketInfo['email'] || '');
                        $('#cbbCreatedUser').val(addSeat.ticketInfo['agencyUserId'] || '');
                        $('#select2-cbbCreatedUser-container').text(addSeat.ticketInfo['sourceName'] || '');
                        $('#txtNote').val(addSeat.ticketInfo['note'] || '');
                        $('#alongTheWayAddress').val(addSeat.ticketInfo['alongTheWayAddress'] || '');
                        $('#dropAlongTheWayAddress').val(addSeat.ticketInfo['dropAlongTheWayAddress'] || '');
//                        cập nhật điểm trung chuyển select
                        if (addSeat.ticketInfo['pickUpAddress'] !== undefined && addSeat.ticketInfo['pickUpAddress'] !== '') {
                            if (typeof tripSelected.tripData.transshipmentInPoint !== 'undefined') {
                                pointIn = $.grep(tripSelected.tripData.transshipmentInPoint, function (e, i) {
                                    return e.pointName == addSeat.ticketInfo['pickUpAddress'];
                                })[0] || {};
                                console.log(pointIn);
                                if (typeof pointIn.pointId !== 'undefined') {
                                    $('#rdTransshipmentUp').prop('checked', true);
                                    $('.transshipmentInPoint').val(pointIn.pointId).trigger('change');
                                    $('select.transshipmentInPoint').show();
                                    $('#pickUpAddress').hide();
                                } else {
                                    $('#rdPickUpAddress').prop('checked', true);
                                    $('#pickUpAddress').val(addSeat.ticketInfo['pickUpAddress']);
                                    $('select.transshipmentInPoint').hide();
                                    $('#pickUpAddress').show();
                                }
                            } else {
                                $('#rdPickUpAddress').prop('checked', true);
                                $('#pickUpAddress').val(addSeat.ticketInfo['pickUpAddress']);
                                $('select.transshipmentInPoint').hide();
                                $('#pickUpAddress').show();
                            }
                        } else {
                            if (addSeat.ticketInfo['alongTheWayAddress'] !== undefined && addSeat.ticketInfo['alongTheWayAddress'] !== '') {
                                $('#rdAlongTheWayAddress').prop('checked', true);
                                $('#pickUpAddress').val(addSeat.ticketInfo['alongTheWayAddress']);
                                $('select.transshipmentInPoint').hide();
                                $('#pickUpAddress').show();
                            } else {
                                $('#rdPickUpAddress').prop('checked', true);
                                $('#pickUpAddress').val('');
                                $('select.transshipmentInPoint').hide();
                                $('#pickUpAddress').show();
                            }
                        }
                        if (addSeat.ticketInfo['dropOffAddress'] !== undefined && addSeat.ticketInfo['dropOffAddress'] !== '') {
                            if (typeof tripSelected.tripData.transshipmentOffPoint !== 'undefined') {
                                pointOff = $.grep(tripSelected.tripData.transshipmentOffPoint, function (e, i) {
                                    return e.pointName == addSeat.ticketInfo['dropOffAddress'];
                                })[0] || {};
                                if (typeof pointOff.pointId !== 'undefined') {
                                    $('#rdTransshipmentDown').prop('checked', true);
                                    $('.transshipmentOffPoint').val(pointOff.pointId).trigger('change');
                                    $('select.transshipmentOffPoint').show();
                                    $('#dropOffAddress').hide();
                                } else {
                                    $('#rdDropOffAddress').prop('checked', true);
                                    $('#dropOffAddress').val(addSeat.ticketInfo['dropOffAddress']);
                                    $('select.transshipmentOffPoint').hide();
                                    $('#dropOffAddress').show();
                                }
                            } else {
                                $('#rdDropOffAddress').prop('checked', true);
                                $('#dropOffAddress').val(addSeat.ticketInfo['dropOffAddress']);
                                $('select.transshipmentOffPoint').hide();
                                $('#dropOffAddress').show();
                            }
                        } else {
                            if (addSeat.ticketInfo['dropAlongTheWayAddress'] !== undefined && addSeat.ticketInfo['dropAlongTheWayAddress'] !== '') {
                                $('#rdDropAlongTheWayAddress').prop('checked', true);
                                $('#dropOffAddress').val(addSeat.ticketInfo['dropAlongTheWayAddress']);
                                $('select.transshipmentOffPoint').hide();
                                $('#dropOffAddress').show();
                            } else {
                                $('#rdDropOffAddress').prop('checked', true);
                                $('#dropOffAddress').val('');
                                $('select.transshipmentOffPoint').hide();
                                $('#dropOffAddress').show();
                            }
                        }

                        if (addSeat.ticketInfo['promotion'] != undefined) {
//                        nếu đã tồn tại mã khuyến mại thì hiển thị và không cho sửa
                            $('.promotionPrice').val(addSeat.ticketInfo.promotion.price);
                            $('.promotionPercent').val(addSeat.ticketInfo.promotion.percent);
                            if (addSeat.ticketInfo.promotion.percent > -1) {
                                $('.txtPromotionCode').val(addSeat.ticketInfo.promotion.promotionCode + ' - Giảm : ' + (addSeat.ticketInfo.promotion.percent * 100) + ' % ');
                                $('.promotionId').val(addSeat.ticketInfo.promotion.promotionId);
                                $('.promotionCode').val(addSeat.ticketInfo.promotion.promotionCode);
                            } else {
                                $('.txtPromotionCode').val(addSeat.ticketInfo.promotion.promotionCode + ' - Giảm : ' + $.number(addSeat.ticketInfo.promotion.price) + ' VND ');
                                $('.promotionId').val(addSeat.ticketInfo.promotion.promotionId);
                                $('.promotionCode').val(addSeat.ticketInfo.promotion.promotionCode);
                            }
                            $('.txtPromotionCode').prop('readonly', true);
                            $('.btnCheckPromotionCode').hide();
                        } else {
//                        chưa tồn tại thì cho phép sửa
                            $('.promotionPrice').val('-1');
                            $('.promotionPercent').val('-1');
                            $('.promotionId').val('');
                            $('.promotionCode').val('');
                            $('.txtPromotionCode').val('');
                            $('.txtPromotionCode').prop('readonly', false);
                            $('.btnCheckPromotionCode').text('Kiểm tra');
                            $('.btnCheckPromotionCode').show();
                        }
//                        commission đại lí
                        if (addSeat.ticketInfo.commissionAgency != undefined) {
                            $('#txtCommissionType').val((addSeat.ticketInfo.commissionAgency.commissionType || 1));
                            $('#txtCommissionValue').val((addSeat.ticketInfo.commissionAgency.commissionValue || 0));
                        }
                    } else {

                        $('#txtTicketId').val('');
                        $('.isAddSeat').hide();
                        $('.isntAddSeat').show();
                        $('#checkRoundTrip').show();

                        $('.promotionId').val('');
                        $('.promotionCode').val('');
                        $('.promotionPrice').val('-1');
                        $('.promotionPercent').val('-1');
                        $('.txtPromotionCode').val('');
                        $('.txtPromotionCode').prop('readonly', false);
                        $('.btnCheckPromotionCode').text('Kiểm tra');
                        $('.btnCheckPromotionCode').show();
                        $('#txtNote').val('');
                    }
                    updateRadioUp();
                    updateRadioDown();

                    $('#txtRealPriceRound').val(0);
                    $('#txtPaidMoneyRound').val(0);
                    $('#txtMoneyOwingRound').val(0);
                    $('form#datve').attr('action', urlSellTicket);
                    //$('#cbGetPickUpAddress').prop('checked', false);
                    if (!$(this).parents('.ghe').hasClass("ghedangchon")) {
                        $(this).parents(".ghe").click();
                    }
                    if (tripSelected.tripData.tripStatus == 2) {
                        $('#thanhtoan1').hide();
                        $('#giucho').hide();
                    } else {
                        $('#thanhtoan1').show();
                        $('#giucho').show();
                    }
                    var ticketPriceValue = '';
                    var listExtraPrice = '', listExtraTK = '';
                    var lengthItem = $('.ghedangchon').length;
                    var listSeatId = [];
                    var hasSeatLock = false;
                    $('.ghedangchon').each(function (key, elem) {
                        var extraPrice = $(elem).data('extraprice');
                        var num = $.number(extraPrice + $('#ticketPrice').data("ticketprice"));
                        ticketPriceValue = ticketPriceValue + num;
                        listExtraPrice += extraPrice;
                        if (key != lengthItem - 1) {
                            ticketPriceValue += " , ";
                            listExtraPrice += ",";
                        }
                        listSeatId.push($(elem).find('.seatId').text());
                        if ($(elem).find('.seatLock').length > 0 || $(elem).attr('data-ticketid') !== "undefined") {
                            hasSeatLock = true;
                        }
                    });
                    if (hasSeatLock) {
                        notyMessage("Tồn tại ghế không hợp lệ ( bị khóa hoặc đã được đặt ). Vui lòng bỏ chọn ghế đó!", "error");
                        return false;
                    }
                    listSeatReActive = listSeatId;
//                    khóa ghế
//                    localLockSeat = JSON.parse(localStorage.getItem("lockSeat")||'{}');
//                    if(typeof localLockSeat.tripId!=="undefined"){//nếu đang tồn tại ghế lock thì mở ra
//                        lockOrUnlockSeat("unlock");
//                    }
                    if(!$(this).parents('.ghe').hasClass('seatFreeSeatMap')){
                        localLockSeat = {tripId: tripSelected.tripData.tripId, listSeatId: listSeatId};
                        sessionStorage.setItem("lockSeat", JSON.stringify(localLockSeat));//lưu lại thông tin ghế lock hiện tại
                        lockOrUnlockSeat("lock");
                    }

                    $('#txtListExtraPrice').val(listExtraPrice);
                    $('#txtPaidMoney').val(0);
                    $('#txtTimeBookHolder').val(0);
                    $('#txtThuThem').val(0);
                    $('.isSellTicket').show();
                    $('.isUpdateStatusTicket').hide();
                    if($('#cbbCreatedUser').val()===''||$('#cbbCreatedUser').val()===null){
                        $('.nhaxethuho').hide();
                    }else{
                        $('.nhaxethuho').show();
                    }

                    $('.salePrice').val(0);
                    $('#realPrice').val(getTotalPrice());
                    if (typeof telecomCallingInfo !== 'undefined' && typeof telecomCallingInfo.status !== 'undefined' && (telecomCallingInfo.status === 18 || telecomCallingInfo.status === 28)) {
                        $('#phoneNumber').val(telecomCallingInfo.phoneNumber);
                        if (typeof telecomCallingInfo.customer !== 'undefined') {
                            $('.CustomerFullName').val(telecomCallingInfo.customer.fullName);
                        } else {
                            $('.CustomerFullName').val('');
                        }
                    }
//                    if(typeof tripSelected.tripData.promotion!=="undefined"&&typeof tripSelected.tripData.promotion.promotionCode!=="undefined"){
//                        $('.txtPromotionCode').val(tripSelected.tripData.promotion.promotionCode);
//                        $('.btnCheckPromotionCode').click();
//                    }
                    updateInputPrice(true);
                    showHideRoundTrip($('#txtRoundTrip'));
                    if ('{{session('companyId')}}' == 'TC03j1IanqGKIUS') {
                        $('#textChildren').text('Ghế miễn phí');
                        $('#huyve').hide();
                        $('#capnhat').hide();
                        $('#giucho').hide();
                    }
                    $("#sell_ticket").modal("show");
                    return false;
                });

                $('body').on('click', '.seatLock .borderIconLock', function () {
                    lockOrUnlockSeat("unlock");
                    $(this).parents('.ghe').css('cursor', 'progress');
                    listSeatId = [];
                    listSeatId.push($(this).parents('.ghe').find('.seatId').text());
                    localLockSeat = {tripId: tripSelected.tripData.tripId, listSeatId: listSeatId};
                    sessionStorage.setItem("lockSeat", JSON.stringify(localLockSeat));
                    lockOrUnlockSeat("unlock");
                    return false;
                });

                $('input#txtPaidMoney,#realPrice').keyup(function () {
                    $('#txtMoneyOwing').val($.number(parseInt($('#realPrice').val()) - $('input#txtPaidMoney').val()));
                });

                $('.btnPrintTrip').click(function () {
                    if (tripSelected.tripData.tripId != '') {
                        $('.khung_lich_ve').hide();
                        $('.khung_khach_hang').show();
                        if (<?php if (session('companyId') == 'TC03013FPyeySDW' || session('companyId') == 'TC03m1MSnhqwtfE'|| session('companyId') == 'TC07C1pjfHQtyduK') echo 'true'; else echo 'false';?>) {
                            $('#dskhach').hide();
                            $('#dskhachdon').hide();
                            $('#dskhachtra').hide();
                            $('#sodo').show();
                            $('#sodo').printThis();
                        } else {
                            if (document.getElementById("dskhach").offsetLeft > 0) {
                                $('#excel').hide();
                                $("#dskhach").printThis({
                                    pageTitle: '&nbsp;'
                                });
                            }
                        }
                        // if (document.getElementById("dskhachdon").offsetLeft > 0) {
                        //     $("#dskhachdon").printThis();
                        // }
                        // if (document.getElementById("dskhachtra").offsetLeft > 0) {
                        //     $("#dskhachtra").printThis();
                        // }
                        // if (document.getElementById("sodo").offsetLeft > 0) {
                        //     $("#sodo").printThis();
                        // }


                    }
                    //window.open(urlPrintTrip + '?tripId=' + tripSelected.tripId);

                    else
                        notyMessage('Vui lòng chọn chuyến trước khi in!', 'warning');
                });
                $('.printDanhSachVe').click(function(){
                    $('#listTicketCancel').printThis({
                        pageTitle: '&nbsp;'
                    });
                    return false;
                })
                $('#huyve').click(function () {
                    var cancelReason = prompt('Lí do hủy vé');
                    $('#txtCancelReason').val(cancelReason);
                    if (cancelReason == null) {
                        return false;
                    }
                });
                /*Sự kiện khi submit*/
                $("form").submit(function (e) {
                    $('#loading').addClass('loading');
                });

//                realtime ticket

                firebase.auth().signInWithEmailAndPassword("ticketSeller@gmail.com", "AnVui@2018")
                    .then(function (firebaseUser) {
                        // changeRealtimeTrip(tripSelected.tripId);
                        console.log('connected');
                    })
                    .catch(function (error) {
                        // Error Handling
                    });

                $('body').on('change', '#phoneNumber', function () {
                    var phn = $('#phoneNumber').val();
                    $.each(tripSelected.tripData.seatMap.seatList, function (i, e) {
                        if (typeof e.ticketInfo !== 'undefined' && phn === e.ticketInfo[0].phoneNumber) {
                            notyMessage('Số điện thoại này đã có vé trong chuyến', 'warning');
                            return false;
                        }
                    });
                });
                $('body').on('click', '[data-call-to-phone]', function () {
                    if(!isCalling){
                        var phn = $(this).attr('data-call-to-phone');
                        $('#box-phone-fixed-left').addClass('open-box-phone');
                        open_menu();
                        $('#txtPhoneNumberCallTelecom').val(phn);
                        sendCallOut();
                        console.log("click call phone");
                        isCalling = true;
                    }
                    return false;
                });

                $('body').on('click', '[go-route]', function () {
                    routeId = $(this).attr('go-route');
                    $(selectBoxRoute).val(routeId).change();
                });
                $('body').on('click', '#doUpdateNoteTrip', function () {
                    var note = $('#updateNoteTrip').find('.updateNoteTrip').val();
                    SendAjaxWithJson({
                        url : urlDBD('trip/update'),
                        type : "post",
                        data : {
                            note : note,
                            tripId : tripSelected.tripData.tripId,
                        },
                        success : function(data){
                            notyMessage("Cập nhật ghi chú thành công","success");
                        },
                        functionIfError : function(err){
                            notyMessage("Cập nhật ghi chú thất bại","error");
                        }
                    })
                });

                $('body').on('click', '.removeInListColumnTripShow', function () {
                    routeId = $(this).attr('data-route-id');
                    $(this).parent().hide();
                    listColumnTripShowVer1 = JSON.parse(localStorage.getItem('listColumnTripShowVer1') || '[]');
                    $.each(listColumnTripShowVer1, function (i, e) {
                        if (e == routeId) {
                            listColumnTripShowVer1.splice(i, 1);
                        }
                    });
                    localStorage.setItem('listColumnTripShowVer1', JSON.stringify(listColumnTripShowVer1));
                    return false;
                })

                $("#phoneNumber").autocomplete({
                    source: "{{ action('HomeController@searchPhone', ['count' => 2]) }}",
                    delay: 500,
                    cache: false
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    // console.log(item);
                    var inner_html = '';
                    if (item.empty) {
                        inner_html = 'Không có dữ liệu';

                        return $("<li class=''></li>")
                            .data("item.autocomplete", item)
                            .append(inner_html)
                            .appendTo(ul);
                    }
//                    var status;
//                    switch (item.ticketStatus) {
//                        case 0:
//                            status = "<span style='color: #F44336'>Đã hủy</span>"; //do
//                            break;
//                        case 1:
//                            status = "<span style='color: #B71C1C'> Hết hạn giữ chỗ"; //do
//                            break;
//                        case 2:
//                            if (item.overTime > Date.now() || item.overTime == 0)
//                                status = "<span style='color: #FF9800'>Đang giữ chỗ</span>"; //cam
//                            else
//                                status = "<span style='color: #B71C1C'> Hết hạn giữ chỗ"; //do
//
//                            break;
//                        case 3:
//                            status = "<span class='code'>Đã thanh toán</span>"; //xanh
//                            break;
//                        case 4:
//                            status = "<span style='color: #0091EA'>Đã lên xe</span>"; //xanh
//
//                            break;
//                        case 5:
//                            status = "Đã hoàn thành";
//
//                            break;
//                        case 6:
//                            status = "<span style='color: #C62828'>Quá giờ giữ chỗ</span>"; //do
//
//                            break;
//                        case 7:
//                            status = "<span style='color: #33691E'>Ưu tiên giữ chỗ</span>";
//
//                            break;
//                        default:
//                            status = '';
//                    }

                    inner_html = '<div class="row cus-info" data-phone="' + item.phoneNumber + '" ' +
                        'data-name="' + item.fullName + '"' +
                        'data-email="' + item.email + '"' +
                        'data-note="' + item.note + '"' +
                        'data-pickup="' + item.pickUpAddress + '"' +
                        'data-dropoff="' + item.dropOffAddress + '">';
                    inner_html += '<div  class="col-md-12 padding40"><strong>' + item.fullName + ' - ' + item.phoneNumber + '</strong></div>';
                    inner_html += '<div class="col-md-12 padding40">Tuyến: ' + item.routeName + ' - ' + checkDate(item.getInTimePlan) + '</div>';
                    inner_html += '<div class="col-md-12 padding40">Đã đặt: <strong style="color:green">' + item.numberOfTickets + '</strong> vé, Đã hủy: <strong style="color:red">' + item.numberCancelTicket + '</strong> vé</div>';

                    inner_html += '</div>';

                    return $("<li class='ui-menu-item'></li>")
                        .data("item.autocomplete", item)
                        .append(inner_html)
                        .appendTo(ul);
                }

            });

            $('body').on('click', '.cus-info', function () {
                $('#ui-id-1').hide();
                var phone = $(this).data('phone');
                var name = $(this).data('name');
                var email = $(this).data('email');
                var note = $(this).data('note');
                var pickup = $(this).data('pickup');
                var dropoff = $(this).data('dropoff');

                $('#phoneNumber').val(phone);
                if (name !== 'undefined') {
                    $('.CustomerFullName').val(name);
                }

                if (email !== 'undefined') {
                    $('.CustomerEmail').val(email);
                }

                if (note !== 'undefined' && $('#txtNote').val() === '') {
                    $('#txtNote').val(note);
                }

                if (pickup !== 'undefined' && $('#pickUpAddress').val() === '') {
                    $('#pickUpAddress').val(pickup);
                }

                if (dropoff !== 'undefined' && $('#dropOffAddress').val() === '') {
                    $('#dropOffAddress').val(dropoff);
                }
                $('#phoneNumber').change();
            });

            $('#phoneNumber').focus(function () {
                if ($(this).val() !== '') {
                    $('#ui-id-1').show();
                }
            });

            $('body').on('click', '.link-phone-phuc-xuyen', function () {
                var jsonstring = $(this).find('.dataTicket').text();
                var ticketInfo = JSON.parse(jsonstring);
                if (typeof ticketInfo.tripId !== "undefined" && ticketInfo.tripId !== '') {
                    setRouteAndDate(ticketInfo);
                    sessionStorage.setItem('tripId', ticketInfo.tripId);
                    setTimeout(function () {
                        $('.ghe[data-ticketid="' + ticketInfo.ticketId + '"]').eq(0).click();
                    }, 3000);
                    $('#modalTicketHistory').modal('hide');
                }
                return false;
            });
            $('body').on('click', '.btnHistoryCallingPX', function () {
                var time = $(this).attr('data-time');
                startTime = parseInt(time) - 600000;
                endTime = parseInt(time) + 600000;
                getHistoryCalling(1, 'thanhcong', new Date(startTime).customFormat('#YYYY#-#MM#-#DD# #hhhh#:#mm#:#ss#'), new Date(endTime).customFormat('#YYYY#-#MM#-#DD# #hhhh#:#mm#:#ss#'));
                $('#box-phone-fixed-left').addClass('open-box-phone');
                open_menu();
                return false;
            });

            function setRouteAndDate(ticketInfo) {
                $('#txtCalendar').val(covertDate(ticketInfo.getInTimePlanInt));
                $('#chontuyen').val(ticketInfo.routeId).change();
                $('#cbb_InPoint').val(ticketInfo.getInPointId);
                $('#cbb_OffPoint').val(ticketInfo.getOffPointId);
            }

        }

        function covertDate(date) {
            if (typeof date !== 'undefined') {
                year = date.toString().substring(0, 4);
                month = date.toString().substring(4, 6);
                day = date.toString().substring(6, 8);
                return day + '-' + month + '-' + year;
            }
        }

        function startTripFunction(data, open) {
            if (data.status == "success") {
                tripSelected.tripData.tripStatus = data.results.trip.tripStatus;
                if (open) {
                    notyMessage('Mở xuát bến thành công', "success");
                } else {
                    notyMessage(data.results.message || "", "success");
                    tripSelected.tripData.listAssistant = data.results.trip.listAss;
                    tripSelected.tripData.listDriver = data.results.trip.listDriver;
                    tripSelected.tripData.note = data.results.trip.note;
                }
                generateHtmlAllTrip();
                generateHtmlTripInfo();

            } else {
                notyMessage("Lỗi", "Có lỗi xảy ra ! Vui lòng thử lại sau hoặc tải lại trang!", "");
            }
        }

        function flashMoveSeatFunction(listSeatOld, listSeatNew) {
            if (listSeatNew.length == listSeatOld.length && listSeatNew.length > 0) {
                $('.ghedangchon').append('<div class="loader"></div>');
                $(".ghedangchon *").attr("disabled", "disabled").off('click');
                $('.ghechuyenden').append('<div class="loader"></div>');
                $(".ghechuyenden *").attr("disabled", "disabled").off('click');
                var getInPoint = $('#cbb_InPoint').val();
                var getOffPoint = $('#cbb_OffPoint').val();
                tripSelected.waitingUpdate = true;
                $.ajax({
                    dataType: "json",
                    url: "{{ action('TripController@postTransfer') }}",
                    type: "POST",
                    async: true,
                    data: {
                        tripSourceId: moveSeatInfo.tripOld,
                        tripDestinationId: $('#listTrip').val(),
                        numberOfGuests: listSeatNew.length,
                        timeChange: getFormattedDate(tripSelected.tripData.startDate, 'date'),
                        place: $('#chontuyen option:selected').data('startpoint'),
                        scheduleId: $('#listTrip option:selected').data('schedule'),
                        listSeatSourceId: listSeatOld,
                        listSeatDestinationId: listSeatNew,
                        ticketId: moveSeatInfo.ticketId,
                        option: moveSeatInfo.option,
                        _token: '{{ csrf_token() }}',
                        destinationInPointId: getInPoint,
                        destinationOffPointId: getOffPoint,
                    },
                    success: function (data) {
                        if (data.result.code == 200) {
                            notyMessage('Thực thi thành công', 'success');
                            if ($('#listTrip').val() === '-1') {
                                location.reload();
                            }
                            listSeatReActive = listSeatNew;
                        } else {
                            listSeatReActive = listSeatOld;
                            generateHtmlTripInfo();
                            notyMessage(data.result.results.message || 'có lỗi xảy ra,vui lòng thử lại!', 'error');
                        }
                        setTimeout(function () {
                            if (tripSelected.waitingUpdate == true) {
                                updateTripInfo(selectBoxListTrip.val(), selectBoxListTrip.find('option:selected').data('schedule'));
                            }
                        }, 2000);

                    },
                    error: function (err) {
                        notyMessage('có lỗi khi gửi yêu cầu!', 'error');
                    }
                });
            } else {
                notyMessage("Số ghế không bằng nhau", "error");
            }
        }

        function makeListConnectionRealTimeAllTrip() {
            $.each(listConnectionRealTime, function (i, e) {
                e.off();
            });
            listConnectionRealTime = [];
            $.each(listTrip, function (i, e) {
                if (e.tripId !== '-1') {
                    var cnn = database.ref().child('trip').child(e.tripId).child('totalEmptySeat');
                    cnn.on('value', function (snapshot) {
                        var data = snapshot.val();
                        if (data !== null) {
                            listTrip[i].totalEmptySeat = data;
                        }

                        $(".totalSeatEmpty" + e.tripId).html('<div class="probar text-center" style="width:' + Math.floor((e.totalSeat - e.totalEmptySeat) * 100 / e.totalSeat) + '%;background:' + (e.totalEmptySeat == 0 ? "red" : "#4CAF50") + ';">&nbsp;</div><div class="textbar text-center" style="position: absolute;height: 30px;width:100%;top:0px;color:#fff;font-weight: bold">Trống : ' + (e.totalEmptySeat || "0") + '</div>');
                        $(selectBoxListTrip).html('<option value="">Toàn bộ các chuyến</option>' + generateOptionListTrip(listTrip));
                    });
                    listConnectionRealTime.push(cnn);
                    var cnnDriver = database.ref().child('trip').child(e.tripId).child('listDriver');
                    cnnDriver.on('value', function (snapshot) {
                        var data = snapshot.val();
                        console.log(e.tripId+'-dri-'+data);
                        if(data){
                            listTrip[i].listDriver = data;
                        }else{
                            listTrip[i].listDriver = [];
                        }
                        generateHtmlAllTrip();
                        if(tripSelected.tripData.tripId == e.tripId){
                            tripSelected.tripData.listDriver = (data || []);
                            if(tripSelected.tripData.seatMap){
                                generateHtmlTripInfo();
                            }
                        }
                    });
                    listConnectionRealTime.push(cnnDriver);
                    var cnnAssistant = database.ref().child('trip').child(e.tripId).child('listAssistant');
                    cnnAssistant.on('value', function (snapshot) {
                        var data = snapshot.val();
                        console.log(e.tripId+'-ass-'+data);
                        if(data){
                            listTrip[i].listAssistant = data;
                        }else{
                            listTrip[i].listAssistant = [];
                        }
                        if(tripSelected.tripData.tripId == e.tripId){
                            tripSelected.tripData.listAssistant = (data || []);
                            if(tripSelected.tripData.seatMap){
                                generateHtmlTripInfo();
                            }
                        }
                    });
                    listConnectionRealTime.push(cnnAssistant);
                }
            });
        }

        function changeRealtimeTrip(_tripId) {
            if (typeof tripRealtime.path != 'undefined') {
                tripRealtime.off();
            }
            tripRealtime = database.ref().child('trip').child(_tripId);
            tripRealtime.on('value', function (snapshot) {
                var data = snapshot.val();
//                var data = null;
                var check = 0;
//                data.seatMap.setlist
                if (data) {
                    if (typeof data.seatMap.seatList != 'undefined') {
                        for (var i = 0; i < (data.seatMap.seatList).length; i++) {
                            if (data.seatMap.seatList[i].listTicketId &&
                                (typeof  data.seatMap.seatList[i].ticketInfo == 'undefined')) {
                                check = 1;
                                break;
                            }
                        }
                    }
                }
                if (check == 0 && data != null && typeof data.seatMap.numberOfRows !== 'undefined') {
                    tripSelected.tripData.seatMap = data.seatMap;
                    tripSelected.seatList = data.seatMap.seatList;
                    tripSelected.numberOfRows = data.seatMap.numberOfRows;
                    tripSelected.numberOfColumns = data.seatMap.numberOfColumns;
                    tripSelected.numberOfFloors = data.seatMap.numberOfFloors;
                    tripSelected.tripData.tripStatus = data.tripStatus;
                    tripSelected.tripData.goods = data.goods;
                    if (data.note != undefined) tripSelected.tripData.note = data.note;
                    tripSelected.waitingUpdate = false;//sau khi bán vé nếu firebase hoạt động thì sẽ trả về false
                } else {
                    $.ajax({
                        dataType: "json",
                        url: urlSeatMapInTrip,
                        data: {
                            'tripId': _tripId,
                            'scheduleId': sessionStorage.getItem('scheduleId'),
                        },
                        async: false,
                        success: function (data) {
                            tripSelected.tripData.seatMap = data.seatMap;
                        },
                        error: function () {
                            notyMessage("Lỗi tải danh sách vé ! Vui lòng tải lại trang", "error");
                        },
                        complete: function () {
                            Loaing(false);
                        }
                    });
                }
//                console.log(tripSelected);
                if(data!=null){
                    additionTickets = data.additionTickets||[];
                }
                base_price = tripSelected.tripData.ticketPrice;
                generateHtmlTripInfo();
            });
        }

        function updateListPoint(routeId, ready) {
            var route = $.grep(listRoute, function (item) {
                return item.routeId == routeId;
            });
            html_listInPoint = '';
            html_listOffPoint = '';

            if (route.length < 1) {
                notyMessage("Chuyến đã bị xóa hoặc không tồn tại", "error");
                window.location.href = '{{action('TicketController@sellVer1')}}';
                return;
            }

            $(route[0].listPoint).each(function (key, item) {
                if (key == 0 || key == route[0].listPoint.length - 1) {
                    if (key == 0) {
                        html_listInPoint += '<option value="' + item.pointId + '">' + item.pointName + '</option>';
                    } else {
                        html_listOffPoint += '<option value="' + item.pointId + '">' + item.pointName + '</option>';
                    }
                } else {
                    html_listInPoint += '<option value="' + item.pointId + '">' + item.pointName + '</option>';
                    html_listOffPoint += '<option value="' + item.pointId + '">' + item.pointName + '</option>';
                }
            });
            $('.startPoint').html(html_listInPoint);
            $('.endPoint').html(html_listOffPoint);
            stp = $('#chontuyen option:selected').attr('data-startpoint');//nếu có request thì lấy theo request lần đầu tiên còn lần sau flag - flase thì không lấy
            ep = $('#chontuyen option:selected').attr('data-endpoint');
            if (RequestFlagStartPoint == true && "{{request('startPointId')}}" !== '') {
                stp = "{{request('startPointId')}}";
            }
            if (RequestFlagStartPoint == true && "{{request('endPointId')}}" !== '') {
                ep = "{{request('endPointId')}}";
            }
            RequestFlagStartPoint = false;
            // mặc định chọn 110A trần nhật duật
            if ($('#chontuyen').val() === 'R04B1WJx8Q1Aw8') {
                $('#cbb_InPoint').val('P03h1HohkW59xU');//bãi cháy
                $('#cbb_OffPoint').val($('#chontuyen option:selected').data('endpoint')).change();
            } else if ($('#chontuyen').val() == 'R02fYyxpuGyyI' || $('#chontuyen').val() == 'R02AaVE2vdxy5' || $('#chontuyen').val() == 'R0771phlfZbz6jL') {
                $('#cbb_InPoint').val('P00zEi0GvGQdl');
                $('#cbb_OffPoint').val('P04spPCfFvji8');
            } else if ($('#chontuyen').val() == 'R02AaSWQAWl25' || $('#chontuyen').val() == 'R02AaV2rLPMly' || $('#chontuyen').val() == 'R0771phlTr3OtMX') {
                $('#cbb_InPoint').val('P02fYymhRtME3');
                $('#cbb_OffPoint').val('P00zEi0GvGQdl');
            } else {
                if ($('#chontuyen').val() !== 'R04r1lxZWSxs3A' && $('#chontuyen').val() !== 'R04t1mtTY5amaS') {//dhsl thì bỏ chonj
                    $('#cbb_InPoint').val(stp);
                    $('#cbb_OffPoint').val(ep);
                }
            }
            // $('#cbb_InPoint option').each(function(index,element){
            //     //alert($(element).attr('value'));
            //     if($(element).attr('value')=='P00zEi0GvGQdl'){
            //         $('#cbb_InPoint').val($(element).attr('value'));
            //     }
            // });
            // $('#cbb_OffPoint').val($('#chontuyen option:selected').data('endpoint')).change();
            //alert($('#cbb_InPoint').val());
            /*
            * lấy dữ liệu startPointId và endPointId từ sessionStoge
            * */
            if (ready) {
                // var startPointId = sessionStorage.getItem("startPointId");
                // var endPointId = sessionStorage.getItem("endPointId");

                // if (startPointId !== null) {
                //     $('#cbb_InPoint').val(startPointId).change();
                // }
                // if (endPointId !== null) {
                //     $('#cbb_OffPoint').val(endPointId).change();
                // }
                var date = '{{request('date')}}';
                if (date == '') {
                    date = sessionStorage.getItem("date");
                }
                if (date !== null) {
                    $(datepickerBox.datepicker("setDate", date));
                }
//                updateSelectBoxTrip(false);
            }
            changeListTrip();
            Loaing(false);

        }

        function updateInputPrice(all) {
            var originalPrice = 0, // tổng giá ghế
                totalPrice = 0, // tổng tiền
                totalExtraPrice = 0, //tổng tiền phụ thu của ghế
                totalSeatPrice = 0,  //tổng tiền ghế chưa phụ thu
                priceSeat = 0,//giá ghế
                PriceExtra = 0,//giá cộng thêm mỗi ghế
                totalSeat = $('.ghedangchon').length;//tổng số ghế dang chọn
            var priceInsurrance = tripSelected.tripData.priceInsurrance > 0 && $('#cb_priceInsurrance').is(':checked') ? totalSeat * tripSelected.tripData.priceInsurrance : 0;
            var mealPrice = tripSelected.tripData.mealPrice > 0 && $('#cb_isMeal').is(':checked') ? totalSeat * tripSelected.tripData.mealPrice : 0;
            var promotionPercent = $('.promotionPercent').val() || 0;
            var promotionPrice = $('.promotionPrice').val() || 0;
            var priceAdults = $('#numberOfAdults').val() * tripSelected.tripData.ticketPrice;
            var priceChildren = $('#numberOfChildren').val() * (tripSelected.tripData.ticketPrice * tripSelected.childrenTicketRatio);
            var commissionType = parseFloat($('#txtCommissionType').val()) || 1;
            var commissionValue = parseFloat($('#txtCommissionValue').val()) || 0;
            var additionPriceType = parseFloat($('#txtAdditionPriceType').val()) || 1;
            var additionPriceAmount = parseFloat($('#txtAdditionPriceAmount').val()) || 0;
            var additionPriceMode = parseFloat($('#txtAdditionPriceMode').val()) || 1;
            var extraPriceTicket = parseFloat($('#txtThuThem').val()) || 0;
            var listPriceShow = [];

            $('.ghedangchon').each(function (index, element) {
                priceSeat = $(element).attr('data-extraprice');
                priceSeat = $.isNumeric(priceSeat) ? parseFloat(priceSeat) : 0;
                totalExtraPrice += priceSeat;
                listPriceShow.push(moneyFormat(tripSelected.tripData.ticketPrice + priceSeat));
            });

            var transshipmentInPointPrice = parseFloat($('#transshipmentInPointPrice').val()) || 0;
            var transshipmentOffPointPrice = parseFloat($('#transshipmentOffPointPrice').val()) || 0;
            // giá ghế = số tiền ghế người lớn chwua tinhsh phụ thu + ghế trẻ em
            totalSeatPrice = priceAdults + priceChildren;
            // tiền ghế
            originalPrice = totalSeatPrice + totalExtraPrice;

            paymentTicketPrice = originalPrice + transshipmentInPointPrice * totalSeat + transshipmentOffPointPrice * totalSeat + mealPrice + priceInsurrance > 0 ? parseFloat(originalPrice + transshipmentInPointPrice * totalSeat + transshipmentOffPointPrice * totalSeat + mealPrice + priceInsurrance) : 0;
            //giá vé sau khi có đại lí
            // kiểu 1 là tiền kiểu còn lại là phần trăm
            priceAfterCommission = commissionType === 1 ? (originalPrice - totalSeat * commissionValue) : (originalPrice - commissionValue * originalPrice);
            //giá vé sau khi đại lí thì trừ khuyến mại
            var priceAfterPromotion = priceAfterCommission;
            if (parseInt(promotionPercent) > -1) {
                priceAfterPromotion = priceAfterCommission - promotionPercent * priceAfterCommission;
            }
            if (parseInt(promotionPrice) > -1) {
                priceAfterPromotion = priceAfterCommission - promotionPrice * totalSeat;
            }

//            tổng tiền  =  tiền sau khi trừ khuyến mại và đại lí cộng thêm các khoản phụ ăn,bảo hiểm,trung chuyển
            totalPrice = (transshipmentInPointPrice * totalSeat + transshipmentOffPointPrice * totalSeat + priceAfterPromotion + mealPrice + priceInsurrance) > 0 ? parseFloat(transshipmentInPointPrice * totalSeat + transshipmentOffPointPrice * totalSeat + priceAfterPromotion + mealPrice + priceInsurrance) : 0;

            if (additionPriceType === 1) {
                totalPrice = totalPrice + additionPriceAmount * totalSeat * additionPriceMode;
            } else if (additionPriceType === 2) {
                totalPrice = totalPrice + totalPrice * additionPriceAmount * additionPriceMode;
            }

            totalPrice = totalPrice + extraPriceTicket;

            // cập nhật vào input
            $('#originalPriceValue').val(originalPrice);//tiền ghế
            $('#cb_priceInsurrance').val(priceInsurrance);//tiền bảo hiểm
            $('#cb_isMeal').val(mealPrice);//tiền ăn
            $('#listTicketPrice').val(listPriceShow.toString());
            $('#txtPaymentPrice').val(totalPrice);
            if (all) {
                $('#totalPriceShow').val(moneyFormat(paymentTicketPrice));
                $('#txtPaymentPrice').val(totalPrice);
                $('#totalPrice').val(paymentTicketPrice);

                $('#realPrice').val(totalPrice);
                $('#realPriceShow').val(moneyFormat(totalPrice));
                $('#txtMoneyOwing').val(moneyFormat($('#realPrice').val() - $('#txtPaidMoney').val()));
            }
        }

        function updateInputPriceRound() {
            var totalSeat = 0;//tổng số ghế dang chọn
            for (var i = 0; i < Seat_Back_Array.length; i++)
                if (Seat_Back_Array[i] != '') {
                    totalSeat++;
                }
            var priceInsurrance = tripSelected.tripData.priceInsurrance > 0 && $('#cb_priceInsurrance').is(':checked') ? totalSeat * tripSelected.tripData.priceInsurrance : 0;
            var mealPrice = tripSelected.tripData.mealPrice > 0 && $('#cb_isMeal').is(':checked') ? totalSeat * tripSelected.tripData.mealPrice : 0;
            var promotionPercent = $('.promotionPercent').val();
            var promotionPrice = $('.promotionPrice').val();
            var priceAdults = totalSeat * tripSelected.tripData.ticketPrice;
            var priceChildren = 0;
            var commissionType = parseFloat($('#txtCommissionType').val());
            var commissionValue = parseFloat($('#txtCommissionValue').attr('value'));


            var transshipmentInPointPrice = parseFloat($('#transshipmentInPointPrice').val()) || 0;
            var transshipmentOffPointPrice = parseFloat($('#transshipmentOffPointPrice').val()) || 0;
            //kiểu 1 là tiền kiểu còn lại là phần trăm
            var priceAfterCommissionRound = commissionType == 1 ? totalPriceBack - totalSeat * commissionValue : totalPriceBack - totalSeat * commissionValue * tripSelected.tripData.ticketPrice;

            //giá vé sau khi đại lí thì trừ khuyến mại
            var priceAfterPromotionRound = priceAfterCommissionRound;
            if ($('#hasPromotionRound').val() == 1) {
                if (parseInt(promotionPercent) > -1) {
                    priceAfterPromotionRound = priceAfterCommissionRound - promotionPercent * (totalPriceBack - totalSeat * tripSelected.tripData.ticketPrice);
                }
                if (parseInt(promotionPrice) > -1) {
                    priceAfterPromotionRound = priceAfterCommissionRound - promotionPrice * totalSeat;
                }
            }
//            tổng tiền  =  tiền sau khi trừ khuyến mại và đại lí cộng thêm các khoản phụ ăn,bảo hiểm,trung chuyển
            var agencyPrice = (transshipmentInPointPrice * totalSeat + transshipmentOffPointPrice * totalSeat + priceAfterPromotionRound + mealPrice + priceInsurrance) > 0 ? parseFloat(transshipmentInPointPrice * totalSeat + transshipmentOffPointPrice * totalSeat + priceAfterPromotionRound + mealPrice + priceInsurrance) : 0;

            // cập nhật vào input
            $('#txtPaidMoneyRound').attr('max', agencyPrice);
            $('#MoneyOwingRound').val(agencyPrice);
            $('#txtRealPriceRound').val(agencyPrice);
            $('#txtMoneyOwingRound').val(agencyPrice);

            $('#priceInsurranceRound').val(priceInsurrance);//tiền bảo hiểm
            $('#mealPriceRound').val(mealPrice);//tiền ăn
            $('#adultsRound').val(totalSeat);
        }

        function updatePrice() {
            if (total_Price > 0) {
                var totalPrice = getTotalPrice();
                total_Price = totalPriceBack + totalPrice;
                $('#totalPrice').val(moneyFormat(total_Price));
                // $('#txtPaidMoney').attr('max', total_Price);
                $('#txt_totalPrice').val(total_Price);
                $('#txtMoneyOwing').val($('#realPrice').val() - parseInt($('#txtPaidMoney').val()));
            }
            else {
                var totalPrice = getTotalPrice();
                $('#totalPrice').val(moneyFormat(totalPrice));
                // $('#txtPaidMoney').attr('max', totalPrice);
                $('#txt_totalPrice').val(totalPrice);
                $('#txtMoneyOwing').val(parseInt($('#realPrice').val()) - parseInt($('#txtPaidMoney').val()));
            }
        }

        /*
        * hàm tính tổng giá tiền
        * */
        function getTotalPrice() {

            var totalTicket = $('.ghedangchon').length;
            var priceInsurrance = $('#cb_priceInsurrance').val() > 0 && $('#cb_priceInsurrance').is(':checked') ? totalTicket * $('#cb_priceInsurrance').val() : 0;
            var mealPrice = $('#cb_isMeal').val() > 0 && $('#cb_isMeal').is(':checked') ? totalTicket * $('#cb_isMeal').val() : 0;
            var priceAdults = $('#numberOfAdults').val() * tripSelected.tripData.ticketPrice;
            var priceChildren = $('#numberOfChildren').val() * (tripSelected.tripData.ticketPrice * tripSelected.childrenTicketRatio);
            var salePrice = $($('.salePrice')[0]).val();
            var totalextra = 0;
            $('.ghedangchon').each(function (index, ele) {
                var price = $(ele).attr('data-extraPrice');
                price = $.isNumeric(price) ? parseFloat(price) : 0;
                totalextra += price;
            });

            if (salePrice < 1) {
                salePrice *= (priceAdults + priceChildren + totalextra);
            }

            totalPrice = ((priceAdults + priceChildren) - salePrice) + mealPrice + priceInsurrance + totalextra;

            totalPrice = totalPrice > 0 ? totalPrice : 0;
            return totalPrice;
        }

        function selectSeat(elem) {
            $(elem).toggleClass('ghedangchon');
            var seatId = $(elem).children('.ticketInfo-tool').children('div').children('.seatId').text();
            String.prototype.replaceAll = function (str1, str2, ignore) {
                return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, "\\$&"), (ignore ? "gi" : "g")), (typeof(str2) == "string") ? str2.replace(/\$/g, "$$$$") : str2);
            };
            seatId = seatId.replaceAll(" ", "-");
            listSeat = '';
            var arr = [];//mảng chỗ ngồi
            $('.ghedangchon').each(function (k, v) {
                arr.push($(v).find(".seatId").text());
            });
            listSeat = arr.toString();
            var totalTicket = $('.ghedangchon').length;
            // if (txtListSeat.indexOf(seatId) !== -1) {
            //     listSeatArray = txtListSeat.replace(seatId, '').split(' ');
            //     newStr = '';
            //     listSeatArray.forEach(function (item) {
            //         if (item != '') {
            //             newStr += item + ' ';
            //         }
            //     });
            //     $('#listSeat').text(newStr);
            //     $('#listSeatId').val(newStr);
            //
            //     if (totalTicket == 0) {
            //         $('#cb_isMeal').removeAttr('checked');
            //         $('#cb_priceInsurrance').removeAttr('checked');
            //     }
            // } else {
            //     $('#listSeat').text(listSeat);
            //     $('#listSeatId').val(listSeat);
            // }
            $('#listSeat').text(listSeat);
            $('#listSeatId').val(listSeat);

            $('#lb_numberOfAdults').val(totalTicket + " (" + $('#listSeat').text() + ")");
            $('#numberOfAdults').val(totalTicket);
            $('#numberOfChildren').val(0);
            if ('{{session('companyId')}}' == 'TC03j1IanqGKIUS') $('.cancelTicket').hide();

        }

        function updateSelectBoxTripRound(autoOpen, callbackIfSuccess) {

            var routeId = $(selectBoxRoute).val();
            var date = (sessionStorage.getItem('timeTrip') != undefined && sessionStorage.getItem('timeTrip') != '') ? sessionStorage.getItem('timeTrip') : $(datepickerBox.datepicker()).val();
            date = date.split("-");
            var newDate = date[1] + "/" + date[0] + "/" + date[2];
            date = new Date(newDate).getTime();
            var startPointId = $('#cbb_InPoint').val();
            var endPointId = $('#cbb_OffPoint').val();
            $('.routeId').val(routeId);
            $(selectBoxListTrip).html('<option value="">Đang tải dữ liệu...</option>');
//            Loaing(true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: urlDBD("web/find-schedule-like-for-admin-new"),
                headers: {
                    'DOBODY6969': '{{ session('userLogin')['token']['tokenKey'] }}',
                },
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    'routeId': routeId,
                    'page': 0,
                    'count': 100,
                    'timeZone': 7,
                    'companyId': '{{ session("companyId") }}',
                    'date': date,
                    'start': startPointId,
                    'end': endPointId,
                    'new': true,
                }),
                success: function (data) {
                    html = '';
                    listTrip = data;
                    // console.log('wfcedv', data);
                    data.forEach(function (trip) {
                        var totalEmptySeat = trip.totalEmptySeat;
                        var isSelected = sessionStorage.getItem('tripIdRound') === trip.tripId ? 'selected' : '';
                        html += '<option ' + isSelected + ' data-time="' + getFormattedDate(trip.getInTime, 'time') + '" data-schedule="' + trip.scheduleId + '" value="' + trip.tripId + '">' +
                            trip.numberPlate + ' - ' +
                            getFormattedDate(trip.getInTime, 'time') + (trip.scheduleType == 1 ? ' (TC) ' : '') +
                            '(Trống: ' + totalEmptySeat + ')' +
                            '</option>';
                    });

                    if (html !== '') {
                        // html = '<option value="tccc">Toàn bộ các chuyến</option>' + html;
//                        console.log(html);
                        $(selectBoxListTrip).html(html);
                        $('#cbbTripDestinationId').html(html);
                        Loaing(false);
                    }
                    else {
                        $(selectBoxListTrip).html(' <option value="">Hiện tại không có chuyến</option>');
                        $('#cbbTripDestinationId').html('<option value="">Hiện tại không có chuyến</option>');
                        Loaing(false);
                    }
                    if (callbackIfSuccess !== undefined) callbackIfSuccess();
                    saveFormDataLocal();
                    // Loaing(false);
                },
                error: function () {
                    notyMessage("Lỗi ! Vui lòng tải lại trang", "error");

                }
            })
        }

        function updateSelectBoxTrip(autoOpen, callbackIfSuccess) {
            var routeId = $(selectBoxRoute).val();
            var date = $(datepickerBox.datepicker()).val();
            date = date.split("-");
            var newDate = date[1] + "/" + date[0] + "/" + date[2];
            date = new Date(newDate).getTime();
            var startPointId = $('#cbb_InPoint').val();
            var endPointId = $('#cbb_OffPoint').val();
            $('.routeId').val(routeId);
            $(selectBoxListTrip).html('<option value="">Đang tải dữ liệu...</option>');
//            Loaing(true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: urlDBD("web/find-schedule-like-for-admin-new"),
                headers: {
                    'DOBODY6969': '{{ session('userLogin')['token']['tokenKey'] }}',
                },
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    'routeId': routeId,
                    'page': 0,
                    'count': 100,
                    'timeZone': 7,
                    'companyId': '{{ session("companyId") }}',
                    'date': date,
                    'start': startPointId,
                    'end': endPointId,
                }),
                success: function (data) {
                    if (data.code === 200) {
                        data = data.results.result;
                        var html = '';
                        listTrip = data;
                        makeListConnectionRealTimeAllTrip();

                        // tripSelected.tripData = listTrip[0];

                        html = generateOptionListTrip(data);

                        if (html !== '') {
                            // html = '<option value="tccc">Toàn bộ các chuyến</option>' + html;
                            $(selectBoxListTrip).html(html);
                            $('#cbbTripDestinationId').html(html);

                            // var tripId = sessionStorage.getItem("tripId");
                            // var scheduleId = sessionStorage.getItem("scheduleId");

                            var tripId = '{{request('tripId')}}';
                            var scheduleId = '{{request('scheduleId')}}';
                            if (tripId == '') {
                                tripId = sessionStorage.getItem("tripId");
                            }

                            if (scheduleId == '') {
                                scheduleId = sessionStorage.getItem("scheduleId");
                            }
                            if (tripId != -1) {
                                selectBoxListTrip.val(tripId).change();
                            } else {
                                selectBoxListTrip.find("[data-schedule='" + scheduleId + "']").attr("selected", "selected").change();
                            }
                            generateHtmlAllTrip();
                            updateTripInfo(selectBoxListTrip.val(), selectBoxListTrip.find('option:selected').data('schedule'));
                            updatePrice();
//                            if (autoOpen === true) {
//                                $(selectBoxListTrip).select2("open");
//                            }

                        } else {
                            $(selectBoxListTrip).html(' <option value="">Hiện tại không có chuyến</option>');
                            $('#cbbTripDestinationId').html('<option value="">Hiện tại không có chuyến</option>');
                            Loaing(false);
                        }
                        if (callbackIfSuccess !== undefined) callbackIfSuccess();
                        saveFormDataLocal();
                    }

                    Loaing(false);
                },
                error: function () {
                    notyMessage("Lỗi!Vui lòng tải lại trang", "error");
                    Loaing(false);
                }
            })
        }

        function generateOptionListTrip(lt) {
            html = '';
            $.each(lt, function (i, trip) {
                var totalEmptySeat = trip.totalEmptySeat;
                var tid = sessionStorage.getItem('tripId'),
                    sid = sessionStorage.getItem('scheduleId');
                var isSelected = tid === trip.tripId ? 'selected' : '';
                if (tid === '-1') {
                    isSelected = sid === trip.scheduleId ? 'selected' : '';
                }
                html += '<option ' + isSelected + ' data-time="' + getFormattedDate(trip.getInTime, 'time') + '" data-schedule="' + trip.scheduleId + '" value="' + trip.tripId + '">' +
                    trip.numberPlate + ' - ' +
                    getFormattedDate(trip.getInTime, 'time') + (trip.scheduleType == 1 ? ' (TC) ' : '') +
                    ' (Trống: ' + totalEmptySeat + ')' +
                    '</option>';
            });
            return html;
        }

        //cập nhật danh sách vé hủy
        function generateListTicketCancel(tripId) {
            $('#listTicketCancel').html("<div class='text-center'>Đang tải dữ liệu...</div>");
            $.ajax({
                dataType: "json",
                url: '{{ action('TicketController@getListTicketCancelWithTripId') }}',
                data: {'tripId': tripSelected.tripData.tripId, 'ticketStatus': [0].toString()},
                success: function (data) {
                    var addressdrop = '', addresspick = '';
                    html = '';
                    html += '<div class="table-responsive">';
                    html += '<table class="table table-hover">';
                    html += '<thead><tr><th width="5%">STT</th><th width="5%">Mã vé</th><th width="10%">Số ghế</th><th width="10%">KH</th><th width="5%">SĐT</th><th width="15%">Đón</th><th width="15%">Trả</th><th width="10%">Đại lý</th><th width="5%">Người hủy</th><th width="10%">Lí do</th><th>Hành động</th></tr></thead>';
                    if (typeof data.ticket[0] != 'undefined') {
                        $.each(data.ticket, function (key, val) {
                            if (typeof val.getInPoint != 'undefined') {
                                if (typeof val['dropAlongTheWayAddress'] != 'undefined') {
                                    addressdrop = val['dropAlongTheWayAddress'];
                                } else {
                                    if (typeof val[' dropOffAddress'] != 'undefined')
                                        addressdrop = val['dropOffAddress'];
                                    else {
                                        addressdrop = val['getOffPoint']['pointName'];
                                    }
                                }
                                if (addressdrop.split(', ').length >= 2) {
                                    addressdrop = addressdrop.split(', ', 2).toString();
                                }
                                if (typeof val['alongTheWayAddress'] != 'undefined') {
                                    addresspick = val['alongTheWayAddress'];
                                } else {

                                    if (typeof val['pickUpAddress'] != 'undefined') {
                                        addresspick = val['pickUpAddress'];
                                    }
                                    else {
                                        addresspick = val['getInPoint']['pointName'];
                                    }
                                }
                                if (addresspick.split(', ').length >= 2) {
                                    addresspick = addresspick.split(', ', 2).toString();
                                }
                            }
                            html += "<tr><td width='5%'>" + (key + 1) + "</td>";
                            html += "<td width='5%'>" + val.ticketCode + "</td>";
                            html += "<td width='10%'>" + val.listSeatCanceled.length + "(" + val.listSeatCanceled.toString() + ")</td>";
                            html += "<td width='10%'>" + val.fullName + "</td>";
                            html += "<td width='5%'>" + val.phoneNumber + "</td>";
                            html += "<td width='15%'>" + addresspick + "</td>";
                            html += "<td width='15%'>" + addressdrop + "</td>";
                            html += "<td width='10%'>" + (val.sourceName || "-") + "</td>";
                            html += "<td width='5%'>" + val.cancelName + '</td>';
                            html += '<td width="10%">' + (typeof val.cancelReason != 'undefined' ? val.cancelReason : "") + '</td>';
                            if (val.listSeatCanceled.length > 0) {
                                html += '<td width="10%"><a href="" class="btn btn-info cloneTicket" data-ticketid="' + val.ticketId + '" data-tripid="' + val.tripId + '" data-listSeat="' + val.listSeatCanceled.toString() + '" title="phục hồi vé"><i class="fas fa-cut"></i></a>' +
                                    ' <a href="" class="btn btn-warning historyTicket" data-ticketid-history="' + val.ticketId + '" data-tripid="' + val.tripId + '" data-listSeat="' + val.listSeatCanceled.toString() + '" title="Lịch sử vé"><i class="fas fa-history"></i></a></td></tr>';
                            } else {
                                html += '<td width="10%"> </td></tr>';
                            }
                            html += '</tr>';

                        });
                    } else {
                        html += '<tr>';
                        html += '<td colspan="11" class="text-center">Không có dữ liệu</td>';
                        html += '</tr>';
                    }

                    html += '</table>';
                    html += '</div></div>';
                    $('#listTicketCancel').html(html);
                },
                error: function () {
                    notyMessage("Lỗi tải danh sách vé hủy! Vui lòng tải lại trang", "error");
                }
            });
        }

        function generateListTicketExpired(tripId) {
            $('#listTicketCancel').html("<div class='text-center'>Đang tải dữ liệu...</div>");
            $.ajax({
                dataType: "json",
                url: '{{ action('TicketController@getListTicketCancelWithTripId') }}',
                data: {'tripId': tripSelected.tripData.tripId, 'ticketStatus': [2, 6].toString()},
                success: function (data) {
                    var addressdrop = '', addresspick = '';
                    html = '';
                    html += '<div class="table-responsive">';
                    html += '<table class="table table-hover">';
                    html += '<thead><tr><th width="5%">STT</th><th width="10%">Mã vé</th><th width="10%">Số ghế</th><th width="10%">KH</th><th width="5%">SĐT</th><th width="20%">Đón</th><th width="20%">Trả</th><th width="10%">Đại lý</th><th></th></tr></thead>';
                    if (typeof data.ticket[0] != 'undefined') {
                        $.each(data.ticket, function (key, val) {
                            if (typeof val.getInPoint != 'undefined') {
                                if (typeof val['dropAlongTheWayAddress'] != 'undefined') {
                                    addressdrop = val['dropAlongTheWayAddress'];
                                } else {
                                    if (typeof val[' dropOffAddress'] != 'undefined')
                                        addressdrop = val['dropOffAddress'];
                                    else {
                                        addressdrop = val['getOffPoint']['address'];
                                    }
                                }
                                if (addressdrop.split(', ').length >= 2) {
                                    addressdrop = addressdrop.split(', ', 2).toString();
                                }
                                if (typeof val['alongTheWayAddress'] != 'undefined') {
                                    addresspick = val['alongTheWayAddress'];
                                } else {

                                    if (typeof val['pickUpAddress'] != 'undefined') {
                                        addresspick = val['pickUpAddress'];
                                    }
                                    else {
                                        addresspick = val['getInPoint']['address'];
                                    }
                                }
                                if (addresspick.split(', ').length >= 2) {
                                    addresspick = addresspick.split(', ', 2).toString();
                                }
                            }
//                            if(val.ticketStatus ==2 &&val.overTime > (new Date).getTime()){ return;}
                            html += "<tr><td width='5%'>" + (key + 1) + "</td>";
                            html += "<td width='5%'>" + val.ticketCode + "</td>";
                            html += "<td width='10%'>" + val.listSeatId.length + "(" + val.listSeatId.toString() + ")</td>";
                            html += "<td width='10%'>" + val.fullName + "</td>";
                            html += "<td width='5%'>" + val.phoneNumber + "</td>";
                            // htmlListCustomer += "<td>" + v.getInPoint.pointName + "</td>";
                            // htmlListCustomer += "<td>" + v.getOffPoint.pointName + "</td>";
                            html += "<td width='15%'>" + addresspick + "</td>";
                            html += "<td width='15%'>" + addressdrop + "</td>";
                            html += "<td width='10%'>" + (val.sourceName || '-') + "</td>";
                            if (val.listSeatId.length > 0) {
                                html += '<td width="10%"><a href="" class="btn btn-info cloneTicket" data-ticketid="' + val.ticketId + '" data-tripid="' + val.tripId + '" data-listSeat="' + val.listSeatId.toString() + '" title="phục hồi vé"><i class="fas fa-cut"></i></a>' +
                                    ' <a href="" class="btn btn-info historyTicket" data-ticketid-history="' + val.ticketId + '" data-tripid="' + val.tripId + '" data-listSeat="' + val.listSeatId.toString() + '" title="Lịch sử vé"><i class="fas fa-history"></i></a></td></tr>';
                            } else {
                                html += '<td width="10%"> - </td></tr>';
                            }
                            html += '</tr>';
                        });
                    } else {
                        html += '<tr>';
                        html += '<td colspan="9" class="text-center">Không có dữ liệu</td>';
                        html += '</tr>';
                    }

                    html += '</table>';
                    html += '</div></div>';
                    $('#listTicketCancel').html(html);
                },
                error: function () {
                    notyMessage("Lỗi tải danh sách vé hủy! Vui lòng tải lại trang", "error");
                }
            });
        }
        function generateListTicketDownTrip() {
            $('#listTicketCancel').html("<div class='text-center'>Đang tải dữ liệu...</div>");
            SendAjaxWithJson({
                dataType: "json",
                url: urlDBD('ticket/getCompleted'),
                type:"post",
                data: {tripId: tripSelected.tripData.tripId,},
                success: function (data) {
                    var addressdrop = '', addresspick = '';
                    html = '';
                    html += '<div class="table-responsive">';
                    html += '<table class="table table-hover">';
                    html += '<thead><tr><th width="5%">STT</th><th width="10%">Mã vé</th><th width="10%">Số ghế</th><th width="10%">KH</th><th width="5%">SĐT</th><th width="20%">Đón</th><th width="20%">Trả</th><th width="10%">Đại lý</th><th></th></tr></thead>';
                    if (data.results.listTicket.length>0) {
                        $.each(data.results.listTicket, function (key, val) {
                            if (typeof val.getInPoint != 'undefined') {
                                if (typeof val['dropAlongTheWayAddress'] != 'undefined') {
                                    addressdrop = val['dropAlongTheWayAddress'];
                                } else {
                                    if (typeof val[' dropOffAddress'] != 'undefined')
                                        addressdrop = val['dropOffAddress'];
                                    else {
                                        addressdrop = val['getOffPoint']['address'];
                                    }
                                }
                                if (addressdrop.split(', ').length >= 2) {
                                    addressdrop = addressdrop.split(', ', 2).toString();
                                }
                                if (typeof val['alongTheWayAddress'] != 'undefined') {
                                    addresspick = val['alongTheWayAddress'];
                                } else {

                                    if (typeof val['pickUpAddress'] != 'undefined') {
                                        addresspick = val['pickUpAddress'];
                                    }
                                    else {
                                        addresspick = val['getInPoint']['address'];
                                    }
                                }
                                if (addresspick.split(', ').length >= 2) {
                                    addresspick = addresspick.split(', ', 2).toString();
                                }
                            }
                            html += "<tr><td width='5%'>" + (key + 1) + "</td>";
                            html += "<td width='5%'>" + val.ticketCode + "</td>";
                            html += "<td width='10%'>" + val.listSeatId.length + "(" + val.listSeatId.toString() + ")</td>";
                            html += "<td width='10%'>" + val.fullName + "</td>";
                            html += "<td width='5%'>" + val.phoneNumber + "</td>";
                            // htmlListCustomer += "<td>" + v.getInPoint.pointName + "</td>";
                            // htmlListCustomer += "<td>" + v.getOffPoint.pointName + "</td>";
                            html += "<td width='15%'>" + addresspick + "</td>";
                            html += "<td width='15%'>" + addressdrop + "</td>";
                            html += "<td width='10%'>" + (val.sourceName || '-') + "</td>";
                            if (val.listSeatId.length > 0) {
                                html += '<td width="10%"> <a href="" class="btn btn-info historyTicket" data-ticketid-history="' + val.ticketId + '" data-tripid="' + val.tripId + '" data-listSeat="' + val.listSeatId.toString() + '" title="Lịch sử vé"><i class="fas fa-history"></i></a></td></tr>';
                            } else {
                                html += '<td width="10%"> - </td></tr>';
                            }
                            html += '</tr>';
                        });
                    } else {
                        html += '<tr>';
                        html += '<td colspan="9" class="text-center">Không có dữ liệu</td>';
                        html += '</tr>';
                    }

                    html += '</table>';
                    html += '</div></div>';
                    $('#listTicketCancel').html(html);
                },
                error: function () {
                    notyMessage("Lỗi tải danh sách vé hủy! Vui lòng tải lại trang", "error");
                }
            });
        }
        function generateListGoodsInTrip() {
            $('#listTicketCancel').html("<div class='text-center'>Đang tải dữ liệu...</div>");
            SendAjaxWithJson({
                dataType: "json",
                url: urlDBD('goods/list'),
                type:"post",
                data: {tripId: tripSelected.tripData.tripId, page :0 , count:1000},
                success: function (data) {
                    html = '';
                    html += '<div class="table-responsive">';
                    html += '<table class="table table-hover">';
                    html += '<thead><tr><th width="5%">STT</th><th width="10%">Mã Hàng</th><th width="10%">Tên Hàng</th><th width="10%">Người Gửi</th><th width="5%">Người Nhận</th><th width="20%">Lên</th><th width="20%">Xuống</th><th width="10%">Tiền</th><th>Đã trả</th><th>Phải Trả</th><th></th></tr></thead>';
                    var total = {
                        price : 0,
                        paid : 0,
                        unpaid : 0
                    }
                    if (typeof data.results.Goods != 'undefined'&&data.results.Goods.length>0) {
                        $.each(data.results.Goods, function (key, val) {
                            html += "<tr>";
                            html+="<td width='5%'>" + (key + 1) + "</td>";
                            html += "<td width='5%'>" + val.goodsCode + "</td>";
                            html += "<td width='10%'>" + val.goodsName + "</td>";
                            html += "<td width='5%'>" + val.sender + ' <br> <span data-call-to-phone="'+ val.senderPhone +'">'+ val.senderPhone +"</span></td>";
                            html += "<td width='10%'>" + val.receiver + ' <br> <span data-call-to-phone="'+ val.receiverPhone +'">'+ val.receiverPhone +"</span></td>";
                            html += "<td width='15%'>" + (val.pickUpPoint||'') + "</td>";
                            html += "<td width='15%'>" + (val.dropOffPoint||'') + "</td>";
                            html += "<td width='10%'>" + $.number(val.totalPrice) + "</td>";
                            html += "<td width='10%'>" + $.number(val.paid) + "</td>";
                            html += "<td width='10%'>" + $.number(val.unpaid) + "</td>";
                            html += '<td><a href="#" class="removeGoods" data-id="'+val.goodsId+'"><i class="fa fa-trash"></i></a></td>';
                            html += '</tr>';
                            total.price+=val.totalPrice;
                            total.paid+=val.paid;
                            total.unpaid+=val.unpaid;
                        });
                        $('.numberOfGoods').text(data.results.Goods.length);
                    } else {
                        html += '<tr>';
                        html += '<td colspan="11" class="text-center">Không có dữ liệu</td>';
                        html += '</tr>';
                    }
                    html+= '<tr>';
                    html+= '<td colspan="7" style="text-align:center;font-weight:bold">Tổng</td><td>'+$.number(total.price)+'</td><td>'+$.number(total.paid)+'</td><td>'+$.number(total.unpaid)+'</td><td></td>';
                    html+='</tr>'
                    var html1 = html;
                    var html2 = html;
                    if(tripSelected.tripData.tripStatus<2){
                        html2+= '<tr>';
                        html2 +='<td colspan="11"><button class="btn btnAddGoods"><i class="fa fa-plus"></i> Thêm hàng mới</button></td>';
                        html2+= '</tr>';
                    }

                    html1 += '</table>';
                    html1 += '</div></div>';
                    html2 += '</table>';
                    html2 += '</div></div>';
                    $('#listTicketCancel').html(html2);
                    $('.listGoodsInTrip').html(html1);
                },
                functionIfError: function () {
                    $('#listTicketCancel').html("<div class='text-center'>Có lỗi xảy ra, vui lòng thử lại.</div>");
                }
            });
        }

        function generateThuChiChuyen() {
            $('#listTicketCancel').html("<div class='text-center'>Đang tải dữ liệu...</div>");
            var html = '';
            html +='<div style="padding:10px;">';
            html += '<h3 class="text-center">THỐNG KÊ CHUYẾN</h3>';
            html += '<table style="width:100%;font-weight:600;margin-bottom:10px" class="info">';
            html += '<tr>';
            html += '<td style="width:50%"><span>BIỂN SỐ : '+(tripSelected.tripData.numberPlate||'')+'</span><br><span>TÀI XẾ : '+getListNameAndPhoneUser(tripSelected.tripData.listDriver).toString()+'</span><br> PHỤ XE : '+getListNameAndPhoneUser(tripSelected.tripData.listAssistant).toString()+'</td>';
            html += '<td style="width:50%"><span>CHUYẾN ĐI : '+(tripSelected.tripData.routeName||'')+'</span><br><span>THỜI GIAN : '+new Date(tripSelected.tripData.getInTime).customFormat('#hhhh#:#mm# #DD#-#MM#-#YYYY#')+'</span><br> ĐIỆN THOẠI : '+(tripSelected.tripData.phoneNumber || '-')+'</td>';
            html += '</tr>';
            html += '</table>';
            html += '<i><h5>Doanh thu bán vé</h5></i>';

            var countSeat = {total: 0, giucho: 0, pay: 0, totalMoney: 0, totalUnPaidMoney: 0,totalPaidMoney :0};
            $.map(tripSelected.tripData.seatMap.seatList, function (seat) {
                if (seat.seatType === 3 || seat.seatType === 4) {
                    countSeat.total++;
                    if (typeof seat.ticketInfo !== 'undefined') {
                        var ticketinfomation = seat.ticketInfo[0];
                        var stt = checkStatusSeatAndTicket(ticketinfomation.ticketStatus, ticketinfomation.overTime);
                        if (stt.seatStatus === 'ghegiucho') {
                            countSeat.giucho++;
                            if (seat['seatId'] == ticketinfomation['listSeatId'][0]) {
                                countSeat.totalMoney += ticketinfomation.agencyPrice;
                                countSeat.totalUnPaidMoney += ticketinfomation.unPaidMoney;
                                countSeat.totalPaidMoney += ticketinfomation.paidMoney;
                            }
                        }
                        if (stt.seatStatus === 'ghedadat' || stt.seatStatus === 'ghelenxe') {
                            countSeat.pay++;
                            if (seat['seatId'] == ticketinfomation['listSeatId'][0]) {
                                countSeat.totalMoney += ticketinfomation.agencyPrice;
                                countSeat.totalUnPaidMoney += ticketinfomation.unPaidMoney;
                                countSeat.totalPaidMoney += ticketinfomation.paidMoney;
                            }
                        }
                    }
                }
            });
            $.each(additionTickets,function (key,ticket) {
                countSeat.total += ticket.listSeatId.length;
                var status = checkStatusSeatAndTicket(ticket.ticketStatus, ticket.overTime);
                if(status.seatStatus ==='ghegiucho'){
                    countSeat.giucho+=ticket.listSeatId.length;
                    countSeat.totalMoney += ticket.agencyPrice;
                    countSeat.totalUnPaidMoney += ticket.unPaidMoney;
                    countSeat.totalPaidMoney += ticket.paidMoney;
                }
                if (status.seatStatus === 'ghedadat' || status.seatStatus === 'ghelenxe') {
                    countSeat.pay+=ticket.listSeatId.length;
                    countSeat.totalMoney += ticket.agencyPrice;
                    countSeat.totalUnPaidMoney += ticket.unPaidMoney;
                    countSeat.totalPaidMoney += ticket.paidMoney;
                }
            });

            html += '<table class="table doanhthu" style="margin-bottom: 20px">';
            html += '<tr>';
            html += '<th style="text-align:center !important">SỐ VÉ</th>';
            html += '<th style="text-align:center !important">TỔNG TIỀN</th>';
            html += '<th style="text-align:center !important">KHÁCH ĐÃ TRẢ</th>';
            html += '<th style="text-align:center !important">THU TRÊN ĐƯỜNG</th>';
            html += '</tr>';
            html += '<tr>';
            html += '<td style="text-align:center !important">'+(countSeat.giucho+countSeat.pay)+'</td>';
            html += '<td style="text-align:center !important">'+$.number(countSeat.totalMoney)+'</td>';
            html += '<td style="text-align:center !important">'+$.number(countSeat.totalPaidMoney)+'</td>';
            html += '<td style="text-align:center !important">'+$.number(countSeat.totalUnPaidMoney)+'</td>';
            html += '</tr>';
            html += '</table>';
            html += '<i><h5>Thu / chi trên chuyến</h5></i>';
            var tongTienChi=0;
            var tongTienThu = 0;
            html += '<table border="1" style="width:100%;margin-bottom:20px" class="table thuchichuyen">';
            html += '<tr>';
            html += '<th>MÃ PHIẾU</th>';
            html += '<th>KHOẢN MỤC</th>';
            html += '<th>LÍ DO</th>';
            html += '<th>SỐ LƯỢNG</th>';
            html += '<th>DVT</th>';
            html += '<th>THÀNH TIỀN</th>';
            html += '<th>GHI CHÚ</th>';
            html += '</tr>';
            SendAjaxWithJson({
                url : urlDBD('bill/getList'),
                type : "post",
                dataType:"json",
                data : {
                    page :0 ,
                    count :100,
                    tripId : tripSelected.tripData.tripId,
                },
                async : false,
                success : function (res) {
                    if(res.results.listBill.length<=0){
                        html+='<tr><td colspan="7">không có dữ liệu</td></tr>'
                    }else{
                        $.each(res.results.listBill,function(k,v){
                            html += '<tr>';
                            html += '<td>'+(v.documentCode||'-')+'</td>';
                            html += '<td>'+(v.item.itemName)+'</td>';
                            html += '<td>'+(v.reason||'')+'</td>';
                            html += '<td>'+(v.number||0)+'</td>';
                            html += '<td>'+(v.unit||'')+'</td>';
                            html += '<td>'+$.number(v.amount||0)+'</td>';
                            html += '<td>'+(v.note||'')+'</td>';
                            html += '</tr>';
                            if(v.billType==1){
                                tongTienThu+=v.amount;
                            }else{
                                tongTienChi+=v.amount;
                            }
                        });
                    }
                },
                functionIfError : function (res) {
                    console.log(res);
                }
            });
            html += '<tr>';
            html += '<td colspan="7"><a href="javascript:void(0)" class="btnActionBill">Thêm chi phí chuyến <i class="fa fa-plus-circle"></i></a></td>';
            html += '</tr>';
            html += '<tr>';
            html += '<td colspan="2" style="text-align: right;font-weight: 600;font-size: 15px">TỔNG</td><td colspan="3"></td><td colspan="2" style="font-size:15px">Thu : '+$.number(tongTienThu)+' - Chi : '+$.number(tongTienChi)+'</td>';
            html += '</tr>';
            html += '</table>';
            html += '<div>Số tiền phải thu tài xế : <span style="font-weight:bold;font-size:20px;">'+$.number(countSeat.totalUnPaidMoney - tongTienChi + tongTienThu)+'</span> VND. ( Âm là số tiền phải trả tài xế )</div>';
            html += '</div>';
            $('#listTicketCancel').html(html);
            return false;
        }

        function generateAdditionTickets () {
            var addressdrop = '', addresspick = '';
            html = '';
            html += '<div class="table-responsive">';
            html += '<table class="table table-hover">';
            html += '<caption><h3>Danh sách ghế bán thêm</h3></caption>'
            html += '<thead><tr><th width="5%">STT</th><th width="10%">Mã vé</th><th width="10%">Số ghế</th><th width="10%">KH</th><th width="5%">SĐT</th><th width="10%">Đón</th><th width="10%">Trả</th><th width="10%">Đại lý</th><th width="7%">Giá vé</th><th width="7%">Đã thu</th><th width="6%">Phải thu</th><th></th></tr></thead>';
            if (additionTickets.length > 0) {
                $.each(additionTickets, function (key, val) {
                    if (typeof val.getInPoint != 'undefined') {
                        if (typeof val['dropAlongTheWayAddress'] != 'undefined') {
                            addressdrop = val['dropAlongTheWayAddress'];
                        } else {
                            if (typeof val[' dropOffAddress'] != 'undefined')
                                addressdrop = val['dropOffAddress'];
                            else {
                                addressdrop = val['getOffPoint']['address'];
                            }
                        }
                        if (addressdrop.split(', ').length >= 2) {
                            addressdrop = addressdrop.split(', ', 2).toString();
                        }
                        if (typeof val['alongTheWayAddress'] != 'undefined') {
                            addresspick = val['alongTheWayAddress'];
                        } else {

                            if (typeof val['pickUpAddress'] != 'undefined') {
                                addresspick = val['pickUpAddress'];
                            }
                            else {
                                addresspick = val['getInPoint']['address'];
                            }
                        }
                        if (addresspick.split(', ').length >= 2) {
                            addresspick = addresspick.split(', ', 2).toString();
                        }
                    }
//                            if(val.ticketStatus ==2 &&val.overTime > (new Date).getTime()){ return;}
                    html += "<tr><td width='5%'>" + (key + 1) + "</td>";
                    html += "<td width='5%'>" + val.ticketCode + "</td>";
                    html += "<td width='10%'>" + val.listSeatId.length + "</td>";
                    html += "<td width='10%'>" + val.fullName + "</td>";
                    html += "<td width='5%'>" + val.phoneNumber + "</td>";
                    // htmlListCustomer += "<td>" + v.getInPoint.pointName + "</td>";
                    // htmlListCustomer += "<td>" + v.getOffPoint.pointName + "</td>";
                    html += "<td width='15%'>" + addresspick + "</td>";
                    html += "<td width='15%'>" + addressdrop + "</td>";
                    html += "<td width='10%'>" + (val.sourceName || '-') + "</td>";
                    html += "<td width='10%'>" + $.number(val.agencyPrice || 0) + "</td>";
                    html += "<td width='10%'>" + $.number(val.paidMoney || 0) + "</td>";
                    html += "<td width='10%'>" + $.number(val.unPaidMoney || 0) + "</td>";
                    if (val.listSeatId.length > 0) {
                        html += '<td width="10%"><a href="" class="btn btn-info cancelSecondaryTicket" data-ticketid="' + val.ticketId + '" data-tripid="' + val.tripId + '" data-listSeat="' + val.listSeatId.toString() + '" title="phục hồi vé"><i class="fas fa-trash"></i></a></td></tr>';
                    } else {
                        html += '<td width="10%"> - </td></tr>';
                    }
                    html += '</tr>';
                });
            } else {
                html += '<tr>';
                html += '<td colspan="9" class="text-center">Không có dữ liệu</td>';
                html += '</tr>';
            }

            html += '</table>';
            html += '</div></div>';
            $('.listAdditionTickets').html(html);
        }


        // caajp nhaajt thông tin của chuyến vào tripdata
        function updateTripInfo(_tripId, scheduleId) {
            lockOrUnlockSeat("unlock");
            listSeatReActive = [];//bỏ chọn tất cả các ghế
            $('#listTicketCancel').html('');
            tripSelected.tripId = _tripId;
            var date = $(datepickerBox.datepicker()).val();
            date = date.split("-");
            var newDate = date[1] + "/" + date[0] + "/" + date[2];
            date = new Date(newDate).getTime();//ngày
            if (tripSelected.tripId == -1) {

                tripSelected.tripData = $.grep(listTrip, function (trip) {
                    return trip.scheduleId == scheduleId;
                })[0];
                $.ajax({
                    dataType: "json",
                    url: urlSeatMapInTrip,
                    data: {
                        'tripId': _tripId,
                        'scheduleId': scheduleId,
                        'date': date,
                    },
                    async: false,
                    success: function (data) {
                        $.each(listTrip, function (k, v) {
                            if (v.scheduleId === scheduleId) {
                                listTrip[k].tripId = data.tripId;
                                listTrip[k].tripStatus = data.tripStatus;
                            }
                        });
                        sessionStorage.setItem('tripId', data.tripId);
                        sessionStorage.setItem('scheduleId', data.scheduleId);
                        newHtmlSelectListTrip();
//                        tripSelected.tripData.seatMap = data;
//                        tripSelected.seatList = data.seatList;
//                        tripSelected.numberOfRows = data.numberOfRows;
//                        tripSelected.numberOfColumns = data.numberOfColumns;
//                        tripSelected.numberOfFloors = data.numberOfFloors;
//                        generateHtmlTripInfo();
                    },
                    error: function () {
                        notyMessage("Lỗi tải sơ đồ ghế! Vui lòng tải lại trang", "error");
                        Loaing(false);
                    }
                });
            } else {
                tripSelected.tripData = $.grep(listTrip, function (trip) {
                    return trip.tripId === tripSelected.tripId;
                })[0];

                $.ajax({
                    dataType: "json",
                    url: urlSeatMapInTrip,
                    data: {
                        'tripId': _tripId,
                        'scheduleId': scheduleId,
                        'date': date,
                    },
                    success : function(res){
                        // gọi để đồng bọ lại thôi
                    }
                });
                changeRealtimeTrip(tripSelected.tripData.tripId);
            }
            Loaing(false);
        }

        function generateHtmlAllTrip() {
            var number = {stt: 0, totalEmptySeat: 0}
            var col = false;
            html = '';
            listColumnTripShowVer1 = JSON.parse(localStorage.getItem('listColumnTripShowVer1') || '{}');
            if (listColumnTripShowVer1.length > 0) {
                html += '<div class="list-route-all-trip" style="">';
                html += '<ul class="list-group" style="margin:0px">';
                $.each(listColumnTripShowVer1, function (k, v) {
                    $.each(listRoute, function (i, e) {
                        if (v === e.routeId) {
                            col = true;
                            active = '';
                            if ($('#chontuyen').val() === e.routeId) {
                                active = 'active';
                            }
                            html += '<li class="list-group-item ' + active + '" go-route="' + e.routeId + '"><i class="fas fa-bus"></i> ';
                            html += e.routeName;
                            html += '<span class="badge removeInListColumnTripShow" data-route-id="' + e.routeId + '">x</span>';
                            html += '</li>';
                        }
                    })
                });
                html += '</ul>';
                html += '</div>';
            }
            $('.box-all-trip .box-route').html(html);
            html = '';
            html += '<style>' +
                '.header-trip{padding:5px 10px;font-weight:bold} .header-trip .time{font-size:20px;line-height: 25px;color:#0b5fc6} .header-trip .number-plate{border:2px solid #333;float:right;padding:0px 5px;font-size:12px;line-height: 20px;border-radius:5px;}' +
                '.infomation-trip{background: #eee;font-size:10px;padding:5px 10px;line-height: 13px}' +
                '.progress{width:100%;background: #aaa;text-align:center;height:20px;position: relative;margin:0;}' +
                '.probar{text-align:center;line-height:20px;color:#fff;font-weight: bold;}' +
                '.textbar{line-height:20px;}' +
                '</style>';
            html += '<div class="list-trip-all-trip" style="background: #525050;"> <table style="border-spacing:2px;" width="100%">';
            html += '<tr><td width="20%"></td><td width="20%"></td><td width="20%"></td><td width="20%"></td><td width="20%"></td></tr><tr>';
            $.each(listTrip, function (i, e) {
                var active = false;
                if (e.tripId === $(selectBoxListTrip).val()) {
                    active = "true"
                };
                html += '<td style="cursor:pointer" title="Chuyển đến chuyến này" data-go-to-trip="' + e.tripId + '" data-schedule-go-to="' + e.scheduleId + '">';
                html += '<div style="background: #fff;border-radius:5px;margin:2px;' + (active ? 'border : 2px solid #ff4a3d' : '') + '">';
                html += '<div class="header-trip" style="' + (active ? 'background : #fec600' : '') + '">';
                html += '<span class="time">' + getFormattedDate(e.getInTime, 'time') + (e.scheduleType == 1 ? ' (TC) ' : '') + '</span><span class="number-plate">' + e.numberPlate + '</span><div class="clearfix"></div>';
                html += '</div>';
                html += '<div class="infomation-trip" style="' + (active ? 'background : #f39b13' : '') + '">';
                html += '<div style="font-size: 12px"><span style="font-weight:bold;">' + getListNameAndPhoneUser(e.listDriver).toString() + '</span></div>';
                html += '<div> <span style="font-weight:bold;">' + getListNameAndPhoneUser(e.listAssistant).toString() + '</span></div>';
                html += '</div>';
                html += '<div class="footer-trip"><div class="progress totalSeatEmpty' + e.tripId + '"><div class="probar text-center" style="width:' + Math.floor((e.totalSeat - e.totalEmptySeat) * 100 / e.totalSeat) + '%;background:' + (e.totalEmptySeat == 0 ? "red" : "#4CAF50") + ';">&nbsp;</div><div class="textbar text-center" style="position: absolute;height: 20px;width:100%;top:0px;color:#fff;font-weight: bold">Trống : ' + (e.totalEmptySeat || "0") + '</div></div>';
                html += '</div>';
                html += '</div>';
                html += '</td>';
                number.stt++;
                number.totalEmptySeat += e.totalEmptySeat;
                if (number.stt % 5 == 0) {
                    html += '</tr><tr>';
                }
            });
            html += '</tr>';
            html += '</table></div>';
            $('.box-all-trip .box-trip').html(html);
            // $('#tang1 .khungxe').html(html);
            //
            // $('.tripInfo').hide();
            // $('.listTicketCancelAndHH').hide();
            // $('#table_SaoNghe .navtabtool ').hide();
            // $('#table_SaoNghe .ghichu_datghe').hide();
            // $('#listTicketCancel').hide();
            // Loaing(false);
            return false;
        }

        //build html của chuyến
        function generateHtmlTripInfo() {
            $('#updateNoteTrip').show();
            $('#dateSchedulePlus').val($('#txtCalendar').val());
            $('#gheThemKhongThuocSeatMap').show();
            if (tripSelected.tripData.tripStatus == 1) {
                $('#btnStartTrip').show();
                $('#btnOpenTrip').hide();
            }
            if (tripSelected.tripData.tripStatus > 1) {
                $('#btnStartTrip').hide();
                $('#btnOpenTrip').show();
            }
            if(tripSelected.tripData.tripStatus==3){
                $('#btnFinishTrip').hide();
            }else{
                $('#btnFinishTrip').show();
            }

            //cap nhat commition
            countSeat = {total: 0, giucho: 0, pay: 0, totalMoney: 0, totalUnPaidMoney: 0};
            if (tripSelected.tripData.seatMap.seatList !== undefined) {
                $.map(tripSelected.tripData.seatMap.seatList, function (seat) {
                    if (seat.seatType === 3 || seat.seatType === 4) {
                        countSeat.total++;
                        if (typeof seat.ticketInfo !== 'undefined') {
                            var ticketinfomation = seat.ticketInfo[0];
                            stt = checkStatusSeatAndTicket(ticketinfomation.ticketStatus, ticketinfomation.overTime);
                            if (stt.seatStatus === 'ghegiucho') {
                                countSeat.giucho++;
                                if (seat['seatId'] == ticketinfomation['listSeatId'][0]) {
                                    countSeat.totalMoney += ticketinfomation.agencyPrice;
                                    countSeat.totalUnPaidMoney += ticketinfomation.unPaidMoney;
                                }
                            }
                            if (stt.seatStatus === 'ghedadat' || stt.seatStatus === 'ghelenxe') {
                                countSeat.pay++;
                                if (seat['seatId'] == ticketinfomation['listSeatId'][0]) {
                                    countSeat.totalMoney += ticketinfomation.agencyPrice;
                                    countSeat.totalUnPaidMoney += ticketinfomation.unPaidMoney;
                                }
                            }
                        }
                    }
                });
                $.each(additionTickets,function (key,ticket) {
                    countSeat.total += ticket.listSeatId.length;
                    var status = checkStatusSeatAndTicket(ticket.ticketStatus, ticket.overTime);
                    if(status.seatStatus ==='ghegiucho'){
                        countSeat.giucho+=ticket.listSeatId.length;
                        countSeat.totalMoney += ticket.agencyPrice;
                        countSeat.totalUnPaidMoney += ticket.unPaidMoney;
                    }
                    if (status.seatStatus === 'ghedadat' || status.seatStatus === 'ghelenxe') {
                        countSeat.pay+=ticket.listSeatId.length;
                        countSeat.totalMoney += ticket.agencyPrice;
                        countSeat.totalUnPaidMoney += ticket.unPaidMoney;
                    }
                });
                $('#table_SaoNghe .navtabtool').show();
                $('#table_SaoNghe .ghichu_datghe').show();
                $('#listTicketCancel').show();
                $('.tripInfo').show();
                var htmlTIP = '<table class="table" width="100%"><tr>' +
                    '<td width="50%">Biển số : <span  style="font-weight:bold;">' + tripSelected.tripData.numberPlate + '( <span style="cursor:pointer" title="Gọi tới SDT này" data-call-to-phone="' + (tripSelected.tripData.phoneNumber || '-') + '"> ' + (tripSelected.tripData.phoneNumber || '-') +
                    '</span>)</span>, Tài xế : <span  style="font-weight:bold;">' + getListNameAndPhoneUser(tripSelected.tripData.listDriver).toString() +
                    '</span>, Phụ Xe : <span  style="font-weight:bold;">' + getListNameAndPhoneUser(tripSelected.tripData.listAssistant).toString() +
                    '</span></td>' +
                    '<td width="50%"><span>Xuất phát : <span  style="font-weight:bold; color:red;font-size: 16px;">' + new Date(tripSelected.tripData.getInTime).customFormat("#hhhh#h:#mm#") + '</span></span>, Tổng vé : <span  style="font-weight:bold;">' + (countSeat.giucho + countSeat.pay) +
                    '</span>, Trống : <span  style="font-weight:bold;">' + (countSeat.total - countSeat.pay - countSeat.giucho) +
                    '</span>, Thanh toán : <span  style="font-weight:bold;">' + countSeat.pay +
                    '</span>, Đặt chỗ : <span  style="font-weight:bold;">' + countSeat.giucho + '</span>' +
                        @if((isset(session("userLogin")["levelOfAgency"])&&isset(session("userLogin")["levelOfAgency"]["isDisplayPrice"])&&session("userLogin")["levelOfAgency"]["isDisplayPrice"]||!isset(session("userLogin")["levelOfAgency"])||isset(session("userLogin")["levelOfAgency"])&&!isset(session("userLogin")["levelOfAgency"]["isDisplayPrice"]))&&count(session("userLogin")["userInfo"]['listAgency'])<=0)
                            ',<span>Tổng Tiền : <span  style="font-weight:bold;">' + moneyFormat(countSeat.totalMoney) + '</span></span>,<span> Tổng hàng : <span class="numberOfGoods">'+(tripSelected.tripData.goods&&tripSelected.tripData.goods.quantity?tripSelected.tripData.goods.quantity:0)+'</span></span>' +
                        @endif
                            '</td></tr></table>';
                $('.tripInfo').html(htmlTIP);
                $('#updateNoteTrip').find('.updateNoteTrip').val(tripSelected.tripData.note);
//                reset();

                if (tripSelected.tripData.seatMap) {
                    loadSeatMap(tripSelected.tripData.seatMap);
                    makeSeatMapHtml(tripSelected);
                }

//                hiển thị lại ghế đang được chọn
                $.each(listSeatReActive, function (i, e) {//listSeatSelected
                    $('.seatIdInfo' + removeUnicode(e)).parents('.ghe').addClass('ghedangchon');
                });

                tripSelected.ticketPrice = tripSelected.tripData.ticketPrice;
                tripSelected.childrenTicketRatio = tripSelected.tripData.childrenTicketRatio;
                if (tripSelected.tripData.tripStatus > 1) {
                    $('.tripInfo').append('<img src="{{ asset('public/images/daxuatben.png') }}" alt="" class="daxuatben">');
                }
                $('.getInTimePlan').val(tripSelected.tripData.startTime);
                $('.getOffTimePlan').val(tripSelected.tripData.getOffTime);
                $('.tripId').val(tripSelected.tripData.tripId);
                $('.scheduleId').val(tripSelected.tripData.scheduleId);
                $('.startDate').val(tripSelected.tripData.startTime);
                $('#cb_priceInsurrance').val(tripSelected.tripData.priceInsurrance);
                $('#cb_isMeal').val(tripSelected.tripData.mealPrice);

                $('#txtGetInPointId').val(tripSelected.tripData.getInPointId);
                $('#txtGetOffPointId').val(tripSelected.tripData.getOffPointId);
                $('#txtGetInPointName').val(tripSelected.tripData.getInPointName);
                $('#txtGetOffPointName').val(tripSelected.tripData.getOffPointName);
                if (typeof tripSelected.tripData.additionPrice !== "undefined") {
                    $('#txtAdditionPriceType').val(tripSelected.tripData.additionPrice.type);
                    $('#txtAdditionPriceAmount').val(tripSelected.tripData.additionPrice.amount);
                    $('#txtAdditionPriceMode').val(tripSelected.tripData.additionPrice.mode || 1);
                } else {
                    $('#txtAdditionPriceType').val(1);
                    $('#txtAdditionPriceAmount').val(0);
                    $('#txtAdditionPriceMode').val(1);
                }
                // var tp ='';//ticket price
                // $('.ghedangchon').each(function(k,v){
                //     var sid=$(v).find('.seatId').text()
                //     tp = parseInt(TripData.ticketPrice + TripData.seatMap.seatList[$(v).index()])
                // });
//                cập nhật điểm trung  chuyển đón trả
                if(!addSeat.flag){
                    if (typeof tripSelected.tripData.transshipmentInPoint !== 'undefined' && tripSelected.tripData.transshipmentInPoint.length > 0) {
                        $('.divRdTransshipmentInPoint').show();
                        listOptiontransshipment = '<option value="">Chọn điểm</option>';
                        $.each(tripSelected.tripData.transshipmentInPoint, function (i, e) {
                            listOptiontransshipment += '<option value="' + e.pointId + '">' + e.pointName + ' ( ' + moneyFormat(e.transshipmentPrice) + ' ) </option>';
                        });
                        $('.transshipmentInPoint').html(listOptiontransshipment);
                    } else {
                        $('.divRdTransshipmentInPoint').hide();
                        $('#rdPickUpAddress').click();
                    }
//                trả
                    if (typeof tripSelected.tripData.transshipmentOffPoint !== 'undefined' && tripSelected.tripData.transshipmentOffPoint.length > 0) {
                        $('.divRdTransshipmentOffPoint').show();
                        listOptiontransshipment = '<option value="">Chọn điểm</option>';
                        $.each(tripSelected.tripData.transshipmentOffPoint, function (i, e) {
                            listOptiontransshipment += '<option value="' + e.pointId + '">' + e.pointName + ' ( ' + moneyFormat(e.transshipmentPrice) + ' ) </option>';
                        });
                        $('.transshipmentOffPoint').html(listOptiontransshipment);
                    } else {
                        $('.divRdTransshipmentOffPoint').hide();
                        $('#rdDropOffAddress').click();
                    }
                }

                $('#ticketPrice').attr("data-ticketPrice", tripSelected.tripData.ticketPrice);
                $('#mealPrice').val($.number(tripSelected.tripData.mealPrice));
                if (tripSelected.tripData.mealPrice == -1) {
                    $('#isMeal').hide();
                } else {
                    $('#isMeal').show();
                }

                $('#priceInsurrance').val($.number(tripSelected.tripData.priceInsurrance));

                if (tripSelected.tripData.priceInsurrance <= 0) {
                    $('#isInsurrance').hide();
                } else {
                    $('#isInsurrance').show();
                }

                $('.routeName').html('<b>' + $('#chontuyen option:selected').text() + "</b> : <b>" + $('#txtCalendar').val() + "</b> : <b>" + $('#listTrip option:selected').data('time') + "</b>");

                $('.InTime').val(getFormattedDate(tripSelected.tripData.getInTime, 'date'));
                $('.OffTime').val(getFormattedDate(tripSelected.tripData.getOffTime, 'date'));
                /*if (Date.now() > TripData.getInTime) {
                Message('Cảnh báo', 'Xe đã khởi hành qua tuyến này, không thể bán vé!', '');
                }*/

                if (flashMoveSeat) {
                    $('.ghetrong').css('cursor', 'move');
                } else {
                    $('.ghetrong').css('cursor', 'pointer');
                }
            }

            //Tạo danh sách khách hàng
            generateCustomerHTML(tripSelected);

            // tạo danh sách vé thêm
            generateAdditionTickets();

            if ('{{ request('ticketStatus') }}' == '0') {
                $("html, body").animate({scrollTop: $(document).height()}, 1000);
            }

            route_name_selected = $('#select2-chontuyen-container').attr('title');


            listRoute.forEach(function (route) {
                var check_space = 0;
                var start_point = '';
                var end_point = '';
                for (var i = 0; i < route['routeName'].length; i++) {
                    if (route['routeName'].charAt(i) == '-') {
                        check_space = 1;
                        i++;
                    }
                    if (check_space == 0) end_point += route['routeName'].charAt(i);
                    else start_point += route['routeName'].charAt(i);

                }

                var roundRoute = $.trim(start_point) + " " + '-' + " " + $.trim(end_point);

                if (route_name_selected == roundRoute)
                    round_route = route;

            });
        }

        function getListNotTicket(seatMap) {
            var ticket = [];
            if (seatMap.seatList != undefined)
                $.each(seatMap.seatList, function (index, e) {
                    if (e.seatType != undefined && (e.seatType == 3 || e.seatType == 4))
                        if (e.listTicketId == undefined ||
                            (e.listTicketId != undefined && e.listTicketId[0] == undefined) ||
                            (e.listTicketId != undefined && e.listTicketId[0] != undefined && e.listTicketId[0] == ''))
                            ticket.push(e);
                });
            return ticket;
        }

        function getListTicket(seatMap) {
            var ticket = [];
            var flag = false;
            if (seatMap.seatList != undefined)
                $.each(seatMap.seatList, function (index, e) {
                    if (e.listTicketId != undefined) {
                        if (e.listTicketId[0] != undefined) {
                            for (var i = 0; i < ticket.length; i++) {
                                if (e.listTicketId[0] == ticket[i].listTicketId[0]) {
                                    flag = true;
                                }
                            }
                            if (!flag) {
                                ticket.push(e);
                            }
                        }
                    }
                    flag = false
                });
            return ticket;
        }

        /*
        * hàm reset khi chọn lại
        *
        * */
        function reset() {
            tripSelected.tripId = '';
            listSeat = [];
            $('#listSeat').text(listSeat);
//            $('input:text').not(datepickerBox).val('');
            $('#tang1 .khungxe,#tang2 .khungxe').html('');
            $('#listSeat,.InTime,.OffTime,.routeName').val('');
            $('#lb_numberOfAdults').val('0');
            $('#numberOfAdults,#numberOfChildren').val('0');
            $('#tongGiaVeGuiDo,#giaVeGuiDoGoc').text(moneyFormat(0));
            $('#totalPrice').val(moneyFormat(0));

        }

        function resetInput() {
            $('#sell_ticket input[type=checkbox]').prop('checked', false);
            $('input[type=checkbox]#send_code_to_phone').prop('checked', false);
            $('#sell_ticket input[type=text],#sell_ticket input[type=number],#sell_ticket input[type=email]')
                .not('input[readonly],#txtTimeBookHolder').val('');
            //$("#dropOffAddress,#pickUpAddress").prop('readonly', true);
        }

        /*
        * hàm tạo sơ đồ ghế
        * */
        function loadSeatMap(seatMap) {
            if ((seatMap.numberOfFloors > 1 && seatMap.seatList.length < 30 && seatMap.seatList.length != 20) && ('{{session('companyId')}}' == 'TC03013FPyeySDW' || '{{session('companyId')}}' == 'TC03m1MSnhqwtfE')) {
                $('.floor').hide();
                $('.SnTt').show();
                $('#tang1 .khungxe,#tang2 .khungxe,#ds_tang1 .khungxe,#ds_tang2 .khungxe,#SnTt').html('');
                var seatHtml = '';
                seatHtml += '<div id="table-map_SaoNghe" ><div style="padding-left: 1.2%">';
                for (var column = 1; column <= (seatMap['numberOfColumns'] * 2); column++) {
                    if (column == 2 || column == 5) {
                        seatHtml += '<div style="width:24.3%;float:left;background-color: #0D8BBD">' +
                            '<strong style="font-size: 14px">' + "Tầng 1" + '</strong></div>';
                    } else if (column == 1 || column == 6) {
                        seatHtml += '<div style="width:24.3% ;float:left;background-color: #0D8BBD">' +
                            '<strong style="font-size:14px">Tầng 2</strong>' + '</div>';
                    }
                }
                seatHtml += '</div><div style="padding-left: 2%">';
                var seat, blank;
                var seatList = seatMap.seatList;
                var count_col = seatMap.numberOfColumns;
                var count_row = seatMap.numberOfRows;
                var startF2 = getSeatIndex(seatMap.seatList);
                var widthOfSeat = 'calc(' + 100 / (count_col + 1) + '% - 20px)';
                $('#style-seat').html('.ghe{ width:' + widthOfSeat + ';position:relative}');
                for (var row = 1; row <= count_row; row++) {
                    for (column = 1; column <= (count_col - 1) * 2; column++) {
                        blank = '<div class="ghe loidi"></div>';
                        //  lòad td cho cột đầu tiên từ trái sang phải( lối đi ở giuwa làm sau)
                        if (column == 1 && row != 1) {
                            seat = seatList[startF2 + (row - 2) * 2];
                            blank = generateSeatHTML(seat);
                        } else
                        /// cột thứ 2
                        if (column == 2) {
                            seat = seatList[(row - 1) * 2];
                            blank = generateSeatHTML(seat);
                        } else
                        // cột thứ 3
                        if (column == 3) {
                            seat = seatList[1 + (row - 1) * 2];
                            if (seat.floor == 1)
                                blank = generateSeatHTML(seat);
                        } else

                        // cột cuối
                        if (column == 4 && row != 7 && row != 1) {
                            if (startF2 + 1 + (row - 2) * 2) {
                                seat = seatList[startF2 + 1 + (row - 2) * 2];
                                blank = generateSeatHTML(seat);
                            }
                        }
                        seatHtml += blank;

                    }
                }
                seatHtml += '</div>';
                $('#SnTt').html('');
                $('#SnTt').html(seatHtml);


            }
            else {
                $('.floor').hide();
                $('#tang1 .khungxe,#tang2 .khungxe,#ds_tang1 .khungxe,#ds_tang2 .khungxe,#SnTt').html('');
                $('.SnTt').hide();
                var html_floor1 = '',
                    html_floor2 = '',
                    Row = seatMap['numberOfRows'],
                    Column = seatMap['numberOfColumns'],
                    cellNull = '<div class="ghe loidi-noboder"></div>',
                    widthOfSeat = 'calc(' + 100 / Column + '% - 20px)';
                road = '<div class="ghe loidi"></div>';
                $('#style-seat').html('.ghe{ width:' + widthOfSeat + ';position:relative;}');
                for (hang = 1; hang <= Row; hang++) {
                    for (cot = 1; cot <= Column; cot++) {
                        seat_floor1 = '<div class="infoSeatPosition-1-' + hang + '-' + cot + '"><div class="ghe loidi"></div></div>';
                        seat_floor2 = '<div class="infoSeatPosition-2-' + hang + '-' + cot + '"><div class="ghe loidi"></div></div>';
//                        seat_floor1 = seat_floor2 = road;
//                        $.each(seatMap['seatList'], function (key, item) {
//                            if (item['column'] === cot && item['row'] === hang) {
//                                if (item['floor'] === 1) {
//                                    /*Tầng 1*/
//                                    seat_floor1 = generateSeatHTML(item);
//                                } else {
//                                    /*Tầng 2*/
//                                    seat_floor2 = generateSeatHTML(item);
//                                }
//                            }
//                        });
                        html_floor1 += seat_floor1;
                        html_floor2 += seat_floor2;
                    }
                }

                $('#tang1 .khungxe,#ds_tang1 .khungxe').html(html_floor1);

                if (seatMap['numberOfFloors'] > 1) {
                    $('.floor').show();
                    $('#tang2 .khungxe,#ds_tang2 .khungxe').html(html_floor2);
                }
                $.each(seatMap['seatList'], function (key, item) {
                    var SeatInfomation = generateSeatHTML(item);
                    $('.infoSeatPosition-' + item.floor + '-' + item.row + '-' + item.column).html(SeatInfomation);
                })
//                $('.floor').hide();
//                $('#tang1 .khungxe,#tang2 .khungxe,#ds_tang1 .khungxe,#ds_tang2 .khungxe,#SnTt').html('');
//                $('.SnTt').hide();
//                var html_floor1 = '',
//                    html_floor2 = '',
//                    Row = seatMap['numberOfRows'],
//                    Column = seatMap['numberOfColumns'],
//                    cellNull = '<div class="ghe loidi-noboder"></div>',
//                    widthOfSeat = 'calc(' + 100 / Column + '% - 20px)';
//                road = '<div class="ghe loidi"></div>';
//                $('#style-seat').html('.ghe{ width:' + widthOfSeat + ';position:relative;}');
//                for (hang = 1; hang <= Row; hang++) {
//                    for (cot = 1; cot <= Column; cot++) {
//
//                        seat_floor1 = seat_floor2 = road;
//                        $.each(seatMap['seatList'], function (key, item) {
//                            if (item['column'] === cot && item['row'] === hang) {
//                                if (item['floor'] === 1) {
//                                    /*Tầng 1*/
//                                    seat_floor1 = generateSeatHTML(item);
//                                } else {
//                                    /*Tầng 2*/
//                                    seat_floor2 = generateSeatHTML(item);
//                                }
//                            }
//                        });
//                        html_floor1 += seat_floor1;
//                        html_floor2 += seat_floor2;
//                    }
//                }
//
//                $('#tang1 .khungxe,#ds_tang1 .khungxe').html(html_floor1);
//
//                if (seatMap['numberOfFloors'] > 1) {
//                    $('.floor').show();
//                    $('#tang2 .khungxe,#ds_tang2 .khungxe').html(html_floor2);
//                }
            }
            if ($(window).width() > 780) {
                $("*").css('cursor', 'default');
            }
            /*
            * Tự động chọn vé theo ticketId trên url
            * */
            var ticketId = "{{request('ticketId')}}";

            if (ticketId != '' && RequestFlagActiveTicket == true) {
                $('.ghe[data-ticketid="' + ticketId + '"]').eq(0).click();
            }

            RequestFlagActiveTicket = false;

            if ('{{session('companyId')}}' == 'TC03j1IanqGKIUS') $('.editTicket').attr('title', 'Xem thông tin vé');
        }

        function getSeatIndex(seatList) {
            for (var i = 0; i < seatList.length; i++)
                if (seatList[i].floor == 2)
                    return i;
        }

        function generateSeatHTML(seat) {
            /*
                DOOR(1),//cửa
                DRIVER_SEAT(2), //ghế cho tài xế
                NORMAL_SEAT(3),//ghế thường
                BED_SEAT(4),//ghế giường nằm
                WC(5),//nhà vê sinh
                AST_SEAT(6), //ghế cho phụ xe
            */
            var ticketInfo = [];
//            console.log(seat,typeof seat['ticketInfo']);
            if (typeof seat['ticketInfo'] !== 'undefined') {
                ticketInfo = seat['ticketInfo'][0];
            }
            switch (seat['seatType']) {
                case 1:
                    seatType = 'cuaxe2';
                    break;
                case 2:
                    seatType = 'taixe2';
                    break;
                case 3:
                case 4:
                    seatType = checkStatusSeatAndTicket(ticketInfo.ticketStatus, seat.overTime).seatStatus;
                    break;
                case 5:
                    seatType = 'nhavesinh';
                    break;
                case 6:
                    seatType = 'ghephuxe';
                    break;
                default:
                    seatType = 'loidi';
                    break;
            }
            if (seatType == 'ghedadat' || seatType == 'ghegiucho' || seatType == 'ghelenxe') {
                if (typeof seat['ticketInfo'] === 'undefined') {
                    $.ajax({
                        dataType: "json",
                        url: urlSeatMapInTrip,
                        data: {
                            'tripId': tripSelected.tripData.tripId,
                            'scheduleId': tripSelected.tripData.scheduleId,
                        },
                        async: false,
                        success: function (data) {
                            tripSelected.tripData.seatMap = data;
                            tripSelected.seatList = data.seatMap.seatList;
                            generateHtmlTripInfo();
                        },
                        error: function () {
                            notyMessage("Lỗi tải danh sách vé! Vui lòng tải lại trang", "");
                            Loaing(false);
                        }
                    });
                }
            }

            var listAgency = {{ count(session("userLogin")["userInfo"]['listAgency']) }};
            var userId = '{{ session("userLogin")["userInfo"]['userId'] }}';

            if (ticketInfo != undefined && (seatType == 'ghedadat' || seatType == 'ghegiucho' || seatType == 'ghelenxe')) {
                if (listAgency < 1 || ticketInfo.agencyUserId == userId) {
                    if (ticketInfo.gotIntoTrip) {
                        seatType = "ghelenxe"
                    }
                    seatHTML = '<div data-ticketId="' + (checkStatusSeatAndTicket(ticketInfo.ticketStatus, seat.overTime).seatStatus === "ghetrong" ? "undefined" : ticketInfo['ticketId']) + '" ' +
                        'data-extraPrice="' + seat['extraPrice'] + '" class="ghe ' + seatType + '">';
                    seatHTML += '<div class="seatTitle">';
                    if (seatType == 'ghedadat') {
                        colClass = 'col-dadat';
                    } else if (seatType == 'ghegiucho') {
                        colClass = 'col-giucho';
                    } else {
                        colClass = 'col-lenxe';
                    }

                    seatHTML += '<div class="seatId ' + colClass + ' seatIdInfo' + removeUnicode(seat['seatId']) + ' historyTicket" data-ticketid-history="' + ticketInfo.ticketId + '">' + seat['seatId'] + '</div>';

//                    var ticketInfo = typeof seat['ticketInfo'] !== 'undefined' ? seat['ticketInfo'][0] : [];
                    var addressdrop = '', addresspick = '';
                    var phoneNumber = ticketInfo['phoneNumber'] !== '' ? formatPhoneNumber(ticketInfo['phoneNumber']) : 'No Number';

                    if (typeof ticketInfo.getInPoint !== 'undefined' || typeof ticketInfo['dropAlongTheWayAddress'] !== 'undefined' || typeof ticketInfo['dropOffAddress'] != 'undefined' && ticketInfo['dropOffAddress'] != '') {
                        if (typeof ticketInfo['dropAlongTheWayAddress'] != 'undefined' && ticketInfo['dropAlongTheWayAddress'] !== '') {
                            addressdrop = ticketInfo['dropAlongTheWayAddress'];
                        } else {
                            if (typeof ticketInfo['dropOffAddress'] != 'undefined' && ticketInfo['dropOffAddress'] !== '') {
                                addressdrop = ticketInfo['dropOffAddress'];
                            } else {
                                addressdrop = ticketInfo['getOffPoint']['pointName'];
                            }
                        }
                        if (addressdrop.split(', ').length >= 2) {
                            addressdrop = addressdrop.split(', ', 2).toString();
                        }
                        if (typeof ticketInfo['alongTheWayAddress'] != 'undefined' && ticketInfo['alongTheWayAddress'] !== '') {
                            addresspick = ticketInfo['alongTheWayAddress'];
                        } else {

                            if (typeof ticketInfo['pickUpAddress'] != 'undefined' && ticketInfo['pickUpAddress'] !== '') {
                                addresspick = ticketInfo['pickUpAddress'];
                            }
                            else {
                                addresspick = ticketInfo['getInPoint']['pointName'];
                            }
                        }
                        if (addresspick.split(', ').length >= 2) {
                            addresspick = addresspick.split(', ', 2).toString();
                        }
                    }

                    if (phoneNumber.length > 0) {
                        seatHTML += '<div class="phoneNumber seatChecked ' + (ticketInfo.isChecked != undefined && ticketInfo.isChecked == true && seatType !== 'ghedadat' && seatType !== 'ghelenxe' ? "isChecked" : "") + ' phone' + seatType + '">' + phoneNumber + '</div>';
                    }

                    seatHTML += '</div>';

                    seatHTML += '<div class="ticketInfoSeat">';
                    seatHTML += '<div class="line-height18"><span class="customerName">' + ticketInfo['fullName'] + '</span>,';
                    seatHTML += '<span class="ticketPrice"><span style="color: #FF0000"> (' + ticketInfo['listSeatId'].length + 'G)</span> <span class="moneyFormat">' + moneyFormat(ticketInfo['agencyPrice']) + '</span></div>';
                    seatHTML += '<div class="line-height18"><span class="sourceName">' + (ticketInfo['sourceName'] || '') + '</span> | <span class="sellerName">' + (ticketInfo['sellerName'] || '') + '/ ' + (ticketInfo['cashierName'] || ticketInfo['sellerName'] || '') + '</span></div>';
                    seatHTML += '<div class="ticketInPoint"> <i class="fa fa-arrow-alt-circle-up"></i> ' + addresspick + '</div>';
                    seatHTML += '<div class="ticketOffPoint">' + addressdrop + ' <i class="fa fa-arrow-alt-circle-down"></i></div>';

                    seatHTML += '<div class="ticketCode">'+ (ticketInfo['foreignKey']!==''?ticketInfo['foreignKey']+' / ':'') + ticketInfo['ticketCode'] + '</div>';
                    seatHTML += '<div class="ticketEdit">';
                    if (seatType == 'ghedadat' || seatType == 'ghegiucho' || seatType == 'ghelenxe') {
                        seatHTML += '<div>' +
                            '<button type="button" class="btn btn-primary ' + colClass + ' btnPrintTicket" title="in vé" data-code="' + (ticketInfo['ticketCode']) + '" value="' + ticketInfo['ticketId'] + '"><i class="fas fa-print"></i></a>' +
                            '<button type="button" class="btn btn-primary ' + colClass + ' makeCall" title="Gọi cho khách hàng" data-call-to-phone="' + ticketInfo['phoneNumber'] + '" value="' + ticketInfo['phoneNumber'] + '"><i class="fas fa-phone"></i></a>' +
                            '<button type="button" data-seatId="' + seat['seatId'] + '" value="' + ticketInfo['ticketId'] + '" title="Cập nhật vé" class="btn editTicket ' + colClass + ' btn-primary"><i class="fas fa-pencil-alt"></i></button>';
                        if (tripSelected.tripData.tripStatus != 2) {
                            seatHTML += '<button type="button" class="btn btn-primary ' + colClass + ' btnFlashMoveSeat" title="chuyển ghế nhanh" value="' + ticketInfo['ticketId'] + '"><i class="fas fa-exchange-alt"></i></a>' +
                                '<button type="button" data-seatId="' + seat['seatId'] + '" title="Chuyển ghế" class="btn btn-primary ' + colClass + ' btnTransferTicket"><i class="fas fa-arrows-alt"></i></button>' +
                                '<button class="btn btn-primary ' + colClass + ' cancelTicket" type="button" title="Hủy vé"><i class="fas fa-trash"></i></button>';
                        }

                        seatHTML += '</div>';
                    }
                    seatHTML += '</div>';
                    if (ticketInfo['note'] !== '') {
                        seatHTML += '<div class="ticketNote">" ' + ticketInfo['note'] + ' "</div>';
                    }
                    seatHTML += '</div>';
                    seatHTML += '</div>';
                } else {
                    /*Dai ly*/
                    seatHTML = '<div data-ticketId="' + (checkStatusSeatAndTicket(ticketInfo.ticketStatus, seat.overTime).seatStatus === "ghetrong" ? "undefined" : ticketInfo['ticketId']) + '" ' +
                        'data-extraPrice="' + seat['extraPrice'] + '" class="ghe ' + seatType + '">';
                    seatHTML += '<div class="seatTitle">';
                    seatHTML += '<div title="Kiểm tra vé" class="seatId isChecked phone' + seatType + ' seatIdInfo' + removeUnicode(seat['seatId']) + '">' + seat['seatId'] + '</div>'

                    seatHTML += '</div>';


                    seatHTML += '<div class="ticketInfoSeat">';
                    seatHTML += '<div class="customerName"></div>';
                    seatHTML += '<div class="ticketPrice"></div>';
                    seatHTML += '<div class="sellerName"></div>';
                    seatHTML += '<div class="sourceName"></div>';
                    seatHTML += '<div class="ticketInPoint"></div>';
                    seatHTML += '<div class="ticketOffPoint"></div>';

                    seatHTML += '<div class="ticketCode"></div>';
                    seatHTML += '<div class="ticketEdit">';
                    seatHTML += '</div>';
                    seatHTML += '<div class="ticketNote"></div>';
                    seatHTML += '</div>';
                    seatHTML += '</div>';
                }
            } else {
                seatHTML = '<div data-ticketId="' + (checkStatusSeatAndTicket(ticketInfo.ticketStatus, seat.overTime).seatStatus === "ghetrong" ? "undefined" : ticketInfo['ticketId']) + '" ' +
                    'data-extraPrice="' + seat['extraPrice'] + '" class="ghe ' + seatType + '">';


                if (seat['seatType'] == 3 || seat['seatType'] == 4) {
                    seatHTML += '<div class="seatTitle">';
                    seatHTML += '<div class="seatId seatIdInfo' + removeUnicode(seat['seatId']) + '">' + seat['seatId'] + '</div>';
                    if (tripSelected.tripData.tripStatus != 2) {
                        seatHTML += '<div class="sellTicket">' +
                            //                        '<button type="button" class="btnFlashSell btn btn-primary" title="Đặt vé nhanh"><i class="fa fa-bolt"></i></button>' +
                            '<button type="button" data-toggle="modal" data-target="#sell_ticket" title="Đặt vé thường" class="btnSellTicket btn btn-primary"><i class="fas fa-plus"></i></button>' +
                            '</div>';
                    }
                }

                seatHTML += '</div>';
                if (seat.seatStatus == 6) {
                    seatHTML += '<div class="seatLock">';
                    seatHTML += '<div class="userLock">'+(seat.userLock || '')+ ' đang sử dụng ghế này</div>';
                    isUserLock = false;
                    if (typeof seat.listUserId !== "undefined") {
                        $.each(seat.listUserId, function (k, v) {
                            if (v === userLogin.userId) {
                                isUserLock = true;
                            }
                        });
                    }
                    if (isUserLock|| {{ session('userLogin')['userInfo']['userType'] }}===7) {
                        seatHTML += '<div class="iconLock" title="Mở khóa"><div class="borderIconLock"><i class="fa fa-lock"></i></div></div>';
                    }
                    seatHTML += '</div>';
                }
                seatHTML += '</div>';
            }
            return seatHTML;
        }

        function refreshUrl() {
            window.history.replaceState({}, document.title, window.location.href.split('?')[0]);
        }

        function checkStatusSeatAndTicket(status, overTime) {

            /*
            *   INVALID(-2),
                CANCELED(0),
                EMPTY(1), // rỗng. hết hạn giữ chỗ sẽ chuyển về trạng thái này
                BOOKED(2), // đã giữ - (SMS khoảng cách <=10 km)
                BOUGHT(3), // đã thanh toán - (SMS khoảng cách <=10 km)
                ON_THE_TRIP(4), // đã lên xe
                COMPLETED(5), // đã hoàn thành
                OVERTIME(6),// quá giờ giữ chỗ
                BOOKED_ADMIN(7) // siêu phụ xe đặt chỗ
            * */
            var result = {seatStatus: '', TicketStatusHTML: '', TicketStatusMessage: ''};

            switch (status) {
                case 0:
                case 1:
                case 5:
                case 6:
                    result.seatStatus = 'ghetrong';
                    result.TicketStatusHTML = 'Hết hạn giữ chỗ';
                    result.TicketStatusMessage = 'Hết hạn giữ chỗ';
                    break;
                case 3:
                    result.seatStatus = 'ghedadat';
                    result.TicketStatusHTML = '<div class="gc_ghe gc_dadat"></div>';
                    result.TicketStatusMessage = 'Đã thanh toán';
                    break;

                case 4:
                    result.seatStatus = 'ghelenxe';
                    result.TicketStatusHTML = '<div class="gc_ghe gc_dadat"></div>';
                    result.TicketStatusMessage = 'Đã lên xe';

                    break;
                case 2:
//                    var timess = (Date.now() > tripSelected.tripData.getInTime) ? tripSelected.tripData.getInTime : Date.now();
                    if (overTime == 0 || overTime > Date.now()) {
                        result.seatStatus = 'ghegiucho';
                        result.TicketStatusHTML = '<div class="gc_ghe gc_giucho"></div>';
                        result.TicketStatusMessage = 'Giữ chỗ';
                    } else {
                        result.seatStatus = 'ghetrong';
                        result.TicketStatusHTML = 'Hết hạn giữ chỗ';
                        result.TicketStatusMessage = 'Hết hạn giữ chỗ';
                    }
                    break;
                case 7:
                    result.seatStatus = 'ghegiucho';
                    result.TicketStatusHTML = '<div class="gc_ghe gc_giucho"></div>';
                    result.TicketStatusMessage = 'Ưu tiên giữ chỗ';
                    break;
                default:
                    result.seatStatus = 'ghetrong';
                    break;
            }
            return result;
        }

        function Loaing(onLoad) {
            $('form#searchTrip select,form#searchTrip button,form#searchTrip input').prop('disabled', onLoad);
            if (onLoad) {
                $('#loading').addClass('loading');
            } else {
                $('#loading').removeClass('loading');
            }
        }

        function printTicket(ticketId, ticketCode) {
            $.ajax({
                url: '{{action('TicketController@printTicket')}}',
                dataType: 'json',
                data: {
                    ticketId: ticketId
                },
                success: function (data) {
                    var listSetting = data.listSettingPrintTicket;
                    var info = data.result;
                    var html = '';
                    if('{{session('companyId')}}' == 'TC03j1IanqGKIUS'){//waterbus
                        $('.printCompanyName').html('Công ty TNHH Thường Nhật');
                        $('.printCompanyAddress').html('Số 6 Phan Kế Bính,Phường Đa Kao,Quận 1,TP.HCM');
                        $('.printMTS').html('MST:0304354924');
                        $('.printHotline').html('Hotline:1900636830');
                        $('.printTitleTicket').html('Vé Hành Khách Công Cộng Bằng Đường Thủy');
                        $('.printNameTicket').html('Saigon Waterbus Ticket');
                        $('.printWebsite').html('www.saigonwaterbus.com');
                        $('#printListSeat').html('');
                        $('#waterbusQR').html('');
                        $('#waterbusQR').qrcode(ticketCode);
                        $('#datePrintWB').text(($('#txtCalendar').attr('value')).replaceAll('-', '/'));
                        $('#timePrintWB').text($('#listTrip option:selected').data('time'));
                        $('#routePrintWB').html('<label style="font-size: 11pt;font-weight:bold;float:right">' + $('#chontuyen option:selected').text() + '</label><label style="font-size: 10pt;float:right;padding-right: 2mm;">Tuyến</label> ');
                        $('#pricePrintWB').text(moneyFormat(tripSelected.ticketPrice) + '/ LƯỢT/ ONE-WAY');
                        $('#pickPrintWB').text($('#cbb_InPoint option:selected').text());
                        $('#dropPrintWB').text($('#cbb_OffPoint option:selected').text());
                        $('.sgwb').show();
                        $('.printQuyDinh').show();
                        $('.printBaoHiem').show();
                        $('.printPrice').show();
                        var canvas = $('#waterbusQR canvas')[0],
                            snapshotImageElement = document.getElementById('getQRCode');
                        var dataUrl = canvas.toDataURL();
                        snapshotImageElement.src = dataUrl;
                        snapshotImageElement.style.display = 'inline';
                        canvas.style.display = 'none';
                        $.each(info.listSeatId, function () {
                            $('#waterbusPrint').printThis({
                                pageTitle: '&nbsp;'
                            });
                        });
                    } else {
                        $.each(listSetting, function (k, v) {
                            html += '<div class="row-fluid"><span style="">' + (v.alias != '' ? v.alias : v.name) + ': </span><span style=";padding-left: 3px;' + generrateStyle(v) + '">';
                            switch (v.idAttribute) {
                                case 1:
                                    html += info.routeName;
                                    break;
                                case 2:
                                    html += (info.fullName != undefined ? info.fullName : '');
                                    break;
                                case 3:
                                    html += '<span style="line-height: '+(v.size-8)+'px" class="printTicketListSeatId">'+(info.listSeatId).join()+'</span>';
                                    break;
                                case 4:
                                    let time=getFormattedDate(info.getInTimePlan, '');
                                    html += '<span style="line-height: '+(v.size-8)+'px">'+time.substr(0,time.indexOf(','))+',</span>' +
                                        '<span style="font-size:'+(v.size > 16 ? (v.size-8) : 12)+'px;line-height: '+(v.size > 16 ? (v.size-8) : 12)+'px;">'+time.substr(time.indexOf(',')+1,time.length)+'</span>';
                                    break;
                                case 5:
                                    html += moneyFormat(((info.agencyUserId != undefined && info.agencyUserId != '') ? info.paymentTicketPrice : info.agencyPrice)) + 'VNĐ';
                                    break;
                                case 6:
                                    html += getNameUpDown(info).up;
                                    break;
                                case 7:
                                    html += getNameUpDown(info).down;
                                    break;
                                case 8:
                                    html += (info.numberPlate);
                                    break;
                                case 9:
                                    html += info.note;
                                    break;
                                case 10 :
                                    html += info.phoneNumber;
                                    break;
                            }
                            html += '</span></div>';
                        });
                        console.log(info);

                        {{--html += '<div class="row-fluid"><label style="float: left">Lưu ý:</lavel><p style="float: left;padding-top: 3px;">' +--}}
                        {{--'Quý khách vui lòng có mặt trước 30 phút' +--}}
                        {{--'Cảm ơn Quý khách đã sử dụng dịch vụ! (Chi tiết liên hệ: ' + '{{session('userLogin')['telecomPhoneNumber']}}' + '</p></div>';--}}

                        $('#xePrintTicket').text('XE ' + info.companyName);
                        $('#barCodePrintTicket').html(data.barcode);
                        $('#ticketCodePrint').text(data.ticketCode);
                        if ('{{session('companyId')}}' == 'TC05zfAUpyzkaB') {//minh tâm
                            $('#barCodePrintTicket').hide();
                            $('#divPrintTicket img').hide();
                            $('#title_ticket').text('PHIẾU GIỮ CHỖ');
                            $('#xePrintTicket').text('');
                            $('#company_note').text('Lưu ý:Quý khách vui lòng có mặt tại trạm trước 10 phút so với giờ xuất bến');
                        }
                        if ('{{session('companyId')}}' == 'TC03m1MSnhqwtfE') {//minh tâm
                            $('#barCodePrintTicket').hide();
                            $('#divPrintTicket img').hide();
                            $('#title_ticket').text('PHIẾU GIỮ CHỖ');
                            $('#company_note').text('');
                        }
                        if ('{{session('companyId')}}' == 'TC08T1qEunQgs8rW') {//đan anh limousine
                            $('#title_ticket').text('PHIẾU GIỮ CHỖ');
                            $('#xePrintTicket').text('');
                        }

                        $('#ticketInfo').html(html);

                        if('{{session('companyId')}}' == 'TC8YA0Sr5xld'){//quêen cafe
                            $('#title_ticket').text('PHIẾU XÁC NHẬN');
                            $('#ticketCodePrint').text(info.ticketCode);
                            $('#company_note').text('Đại lý : '+(info.sourceName||''));
                            $('.printTicketListSeatId').html(info.listSeatId.length + ' Ghế')
                        }
                        if (listSetting.length > 0) {
                            $('#divPrintTicket').printThis({
                                pageTitle: '&nbsp;',
                            });
                        } else Message('Vào mục Hệ thống để cài đặt hiển thị trước khi in');
                    }
                },
                error: function (data) {
                    notyMessage("Gửi yêu cầu thất bại!", "error");
                }

            });
        }

        function generrateStyle(v) {
            var html = '';
            if (v.bold == 1) html += 'font-weight:bold;';
            html += 'font-size:' + (v.size > 0 ? v.size : 12) + 'px;';
            html += 'font-family:';
            if (v.fontfamily == 1) html += ' Times New Roman, Times, serif';
            if (v.fontfamily == 2) html += ' Arial, Helvetica, sans-serif';
            if (v.fontfamily == 3) html += 'Arial Black, Gadget, sans-serif ';
            if (v.fontfamily == 4) html += 'Comic Sans MS, cursive, sans-serif ';
            if (v.fontfamily == 5) html += 'Impact, Charcoal, sans-serif';
            if (v.fontfamily == 6) html += 'Tahoma, Geneva, sans-serif ';
            if (v.fontfamily == 7) html += 'Verdana, Geneva, sans-serif';
            return html;
        }

        function minus() {
            var data = parseInt($("#numberOfChildren").val()),
                numberOfAdults = parseInt($('#numberOfAdults').val());
            if (data != 0) {
                data--;
                if (tripSelected.tripData.childrenTicketRatio != 0) {
                    numberOfAdults++;
                }
                $("#numberOfChildren").val(data);
                $('#lb_numberOfAdults').val(numberOfAdults + " (" + $('#listSeat').text() + ")");
                $('#numberOfAdults').val(numberOfAdults);
                updateInputPrice(true);
            } else {
                $("#numberOfChildren").val(0);
            }
        }

        function plus() {
            var data = parseInt($("#numberOfChildren").val()),
                numberOfAdults = parseInt($('#numberOfAdults').val());

            if (data < $('.ghedangchon').length) {
                data++;
                if (tripSelected.tripData.childrenTicketRatio != 0) {
                    numberOfAdults--;
                }

                $('#lb_numberOfAdults').val(numberOfAdults + " (" + $('#listSeat').text() + ")");
                $("#numberOfChildren").val(data);
                $('#numberOfAdults').val(numberOfAdults);
                updateInputPrice(true);
            } else {
                notyMessage('Vé trẻ em không thể vượt quá vé người lớn', 'warning');
            }
        }

        /*
        * Lưu trạng thái lựa chọn hiện tại
        * */
        function saveFormDataLocal() {

            if (typeof(Storage) !== "undefined") {
                sessionStorage.setItem("routeId", $(selectBoxRoute).val());
                if ($(selectBoxListTrip).val() != '') {
                    sessionStorage.setItem("tripId", $(selectBoxListTrip).val());
                }
                if ($(selectBoxListTrip).find('option:selected').data('schedule') != undefined) {
                    sessionStorage.setItem("scheduleId", $(selectBoxListTrip).find('option:selected').data('schedule'));
                }
                // sessionStorage.setItem("startPointId", $('#cbb_InPoint').val());
                // sessionStorage.setItem("endPointId", $('#cbb_OffPoint').val());
                sessionStorage.setItem("date", $(datepickerBox.datepicker()).val());

            } else {
                alert("Trình duyệt không được hỗ trợ! Hãy cập nhật bản mới nhất.");
            }
        }

        function stringToInt(str) {
            var val = str.replace(/[^0-9]/gi, '');
            return val;
        }

        function ChangeUrl() {
            var url = window.location.href;

            if (url.indexOf('?') != -1) {
                url = url.slice(0, url.indexOf('?'));
            }

            if (typeof (history.pushState) != "undefined") {
                var obj = {Title: 'An vui - Bán vé', Url: url};
                history.pushState(obj, obj.Title, obj.Url);
            } else {
                alert("Browser does not support HTML5.");
            }
        }

        /*
        * In danh sách hành khách
        * */

        function generateCustomerHTML(TripSelected) {
            /*
            * Thông tin chuyến
            * */
            $('.chuyen').html(TripSelected.tripData.routeName);
            $('.bienso').html(TripSelected.tripData.numberPlate);
            $('.laixe').html(getListNameWithArrayUser(TripSelected.tripData.listDriver).fullName.toString() || '-');
            $('.phuxe').html(getListNameWithArrayUser(TripSelected.tripData.listAssistant).fullName.toString() || '-');
            $('.startTime').html(getFormattedDate(TripSelected.tripData.getInTime, '') || '-');
            if (TripSelected.tripData.note != undefined)
                $('.underTitle').text(TripSelected.tripData.note);
            // if ((TripSelected.tripData.note).match(/^[0-9]+$/) != null)
            //     $('.noteTripNumber').text(TripSelected.tripData.note);
            // else $('.noteTripText').text(TripSelected.tripData.note);
            listNotTicket = getListNotTicket(TripSelected.tripData.seatMap);
            if (listNotTicket.length > 0) {
                var htmlNotTicket = '<h5>Ghế chưa bán :<strong> ';
                $.each(listNotTicket, function (k, v) {
                    if (k < listNotTicket.length - 1) htmlNotTicket += v.seatId + ' , ';
                    else htmlNotTicket += v.seatId;
                });
                $('.listBlank').html(htmlNotTicket + '<strong></h5>');
            }
            listTicket = getListTicket(TripSelected.tripData.seatMap);
            var listPickupCustomer = $.grep(listTicket, function (val) {
                if (typeof val.ticketInfo != 'undefined' && typeof val.ticketInfo[0].pickUpAddress != 'undefined')
                    return val.ticketInfo[0].pickUpAddress;
                else return;
            });
            var listDropOffCustomer = $.grep(listTicket, function (val) {
                if (typeof val.ticketInfo != 'undefined' && typeof val.ticketInfo[0].dropOffAddress != 'undefined')
                    return val.ticketInfo[0].dropOffAddress;
                else return;
            });
            var htmlListCustomer = '',
                checkTicket =[], dem = 1, tongghe = 0, phaithu = 0, tongtien = 0, dathu = 0,benTienGiang = 0,
                benBenTre = 0,
                blank = "<td></td>";
            if (listTicket == '') {
                if (blankRow) {
                    var indexStart=0;
                    if($.grep(listSetting,function(v){return v.id==99;})[0]) indexStart=1;
                    for (var i = 1; i <= blankRow.quantity; i++) {
                        htmlListCustomer += '<tr>';
                        for (var j = indexStart; j < listSetting.length; j++) {
                            htmlListCustomer += '<td style="height: 20px"></td>';
                        }
                        htmlListCustomer += '</tr>';
                    }
                }
//                 htmlListCustomer = "<tr><td class='center' colspan='12'>Hiện không có dữ liệu</td></tr>";
            }
            else {
                function analyzeTicket(ticketInfo){
                    if(checkTicket.indexOf(ticketInfo.ticketId)==-1) {
                        phaithu += (ticketInfo.ticketStatus == 3 ? 0 : (ticketInfo.agencyPrice - ticketInfo.paidMoney));
                        dathu += ticketInfo.paidMoney;
                        tongtien += ticketInfo.agencyPrice;
                        tongghe += ticketInfo.listSeatId.length;
                        checkTicket.push(ticketInfo.ticketId);
                    }
                    if ('{{session('companyId')}}' == 'TC05zfAUpyzkaB') {
                        if (ticketInfo.getOffPointId == 'P02Kdxt8WmVWG' || (ticketInfo.dropOffAddress && (ticketInfo.dropOffAddress).includes('Tiền Giang')))
                            benTienGiang += ticketInfo.listSeatId.length;
                        else if (ticketInfo.getOffPointId == 'P05R202iENktmO')
                            benBenTre += ticketInfo.listSeatId.length;
                    }
                    if ('{{session('companyId')}}' == 'TC04J1GKSSth4UY'){
                        for (var i = ticketInfo.listSeatId.length; i > 0; i--) {
                            htmlListCustomer += generateHtmlCustomer(listSetting, ticketInfo, dem);
                            dem++;
                        }
                        dem--;
                    } else {
                        htmlListCustomer += generateHtmlCustomer(listSetting, ticketInfo, dem);
                    }
                    dem++;
                }
                $('#cbb_InPoint option').each(function (k, value) {
                    listTicket.forEach(function (v) {
                        var ticketInfo = v['ticketInfo'][0];
                        if ($(value).val() == ticketInfo.getInPointId)
                            if ((ticketInfo.ticketStatus == 2 && (ticketInfo.overTime > Date.now() || ticketInfo.overTime == 0)) || ticketInfo.ticketStatus == 4 || ticketInfo.ticketStatus == 3 || ticketInfo.ticketStatus == 7) {
                                analyzeTicket(ticketInfo);
                            }
                    });
                });
                listTicket.forEach(function (v) {
                    var ticketInfo = v['ticketInfo'][0];
                    if (checkTicketAndPoint(ticketInfo)) {
                        if ((ticketInfo.ticketStatus == 2 && (ticketInfo.overTime > Date.now() || ticketInfo.overTime == 0)) || ticketInfo.ticketStatus == 4 || ticketInfo.ticketStatus == 3 || ticketInfo.ticketStatus == 7) {
                            analyzeTicket(ticketInfo);
                        }
                    }
                });
                if (blankRow) {
                    var indexStart=0;
                    if($.grep(listSetting,function(v){return v.id==99;})[0]) indexStart=1;
                    for (var i = 1; i <= blankRow.quantity; i++) {
                        htmlListCustomer += '<tr>';
                        for (var j = indexStart; j < listSetting.length; j++) {
                            htmlListCustomer += '<td style="height: 20px"></td>';
                        }
                        htmlListCustomer += '</tr>';
                    }
                }
                if (['TC05wM7dDRQSo6', 'TC04r1lru1vOk3c','TC1OHoeA_ekC','TC03m1MSnhqwtfE'].indexOf('{{ session('companyId') }}') >= 0) {
                    var front_agency_price = 0, after_unpaid = 0, exist_agency_price = '', exist_paid_money = '',
                        exist_unpaid_mmoney = '';
                    front_agency_price = ($.grep(listSetting, function (v, k) {
                        return v.id < 10;
                    })).length;
                    after_unpaid = ($.grep(listSetting, function (v, k) {
                        return v.id > 12;
                    })).length;
                    if ($.grep(listSetting, function (v, k) {
                            return v.id == 10;
                        })) exist_agency_price = '<td><b>' + moneyFormat(tongtien) + '</b></td>';
                    if ($.grep(listSetting, function (v, k) {
                            return v.id == 11;
                        })) exist_paid_money = '<td><b>' + moneyFormat(dathu) + '</b></td>';
                    if ($.grep(listSetting, function (v, k) {
                            return v.id == 12;
                        })) exist_unpaid_mmoney = '<td><b>' + moneyFormat(phaithu) + '</b></td>';
                    htmlListCustomer += '<tr><td colspan="' + front_agency_price + '"></td>';
                    htmlListCustomer += exist_agency_price + exist_paid_money + exist_unpaid_mmoney + (after_unpaid ? ('<td colspan="' + after_unpaid + '"></td>') : '') + '</tr>';
                }
                if ('{{session('companyId')}}' == 'TC05zfAUpyzkaB') {
                    htmlListCustomer += '<tr>' +
                        '<td class="note_minhtam" colspan="' + listSetting.length + '" >Khách xuống - Tiền Giang:<strong>' + $.number(benTienGiang) + '</strong>, Bến Tre:<strong>' + $.number(benBenTre) + '</strong></td>' +
                        '</tr>';
                    $('.underTable').css('margin-left', '80%').html('Nhân viên kí tên');
                }
                if ('{{ session('companyId') }}' == 'TC03h1IzK1jParS') {
                    $('.underTable').html('<div style="width: 100%; text-align: center;">Chú ý :<strong>Đề nghị lái xe giữ chỗ cho khách hàng đã đặt vé qua Tổng đài, App và Website</strong></div>');
                }
                if ('{{session('companyId')}}' == 'TC04J1GKSSth4UY') {
                    htmlListCustomer += '<tr><td colspan="' + listSetting.length + '" >Tổng số:....người . Tổng tiền:  ' + moneyFormat(tongtien) + '</td></tr>';
                }
            }
            $('.totalSeat').html(tongghe);
            var htmlHeaderBinhMinh = '';
            if ('{{ session('companyId') }}' == 'TC04J1GKSSth4UY') {
                var d = new Date(tripSelected.tripData.startDate);
                htmlHeaderBinhMinh += '<div class="text-center">' +
                    '            <div style="width:30%;margin:0px;padding: 0px;float: left;">' +
                    '                <h4 style="font-weight: bold">HỢP TÁC XÃ VẬN TẢI</h4>' +
                    '                <h5 style="font-weight: bold">27/7</h5><br>' +
                    '                <p style="font-size: 15px;    margin-top: 26px;">Số : .........../DSKH</p>' +
                    '            </div>' +
                    '            <div style="width:65%;margin:0px;padding: 0px;float:right;">' +
                    '                <h4 style="font-weight: bold">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</h4>' +
                    '                <h4 style="font-weight: bold">Độc Lập - Tự do - Hạnh Phúc</h4>' +
                    '                <div style="font-weight: bold">--------------------------------------</div>' +
                    '                <div style="margin-top: 25px;padding-left: 200px;">............. ngày...... tháng.....năm 2018</div>' +
                    '                <br>' +
                    '            </div>' +
                    '        </div>';
                var underTitle = '(Kèm theo Hợp đồng vận chuyển số......................../HDVCHK.              ngày......tháng.....năm 2018)';
                var underTable = '<div class="text-center"> ' +
                    '<div style="width:50%;margin:0px;padding: 0px;float: left;">' +
                    ' <h4 style="font-weight: bold">HỢP TÁC XÃ VẬN TẢI 27/7</h4>' +
                    ' <h4 style="font-weight: bold">GIÁM ĐỐC</h4></div>' +
                    '<div style="width:50%;margin:0px;padding: 0px;float: right;">' +
                    ' <h4 style="font-weight: bold">BÊN THUÊ VẬN TẢI</h4>' +
                    ' <p>(Ký tên, đóng dấu(nếu có))</p></div>' +
                    '</div>';
                $('.underTitle').html(underTitle);
                $('.underTable').html(underTable);
                $('.dshkHeader').html(htmlHeaderBinhMinh);
            }

            $('#list-customer').html(htmlListCustomer);
            makeListPickup(listPickupCustomer);
            makeSeatMapHtml(TripSelected);
            $('#table-list').tablesorter();
            
            @include('cpanel.Ticket.constant')
            @if (in_array(session('companyId'), LIST_CUSTOMERS_DROP_COMPANIES))
                var htmlListCustomer = '';
                var count = 0;
                if (listDropOffCustomer[0] == null) {
                    htmlListCustomer = "<tr><td class='center' colspan='8'>Hiện không có dữ liệu</td></tr>";
                }
                else {
                    var listDiver = listDriver;
                    listDropOffCustomer.forEach(function (v) {
                        var ticketInfo = v['ticketInfo'][0],
                            listOpUp = '', listOpDown = '';
                        for (var i = 0; i < listDiver.length; i++) {
                            if ((ticketInfo.transshipmentPickUpDriver != undefined && ticketInfo.transshipmentPickUpDriver == listDiver[i].userId))
                                listOpUp += "<option selected value='" + listDiver[i].userId + "'>" + listDiver[i].fullName + "</option>";
                            else
                                listOpUp += "<option value='" + listDiver[i].userId + "'>" + listDiver[i].fullName + "</option>";
                            if ((ticketInfo.transshipmentDropOffDriver != undefined && ticketInfo.transshipmentDropOffDriver == listDiver[i].userId))
                                listOpDown += "<option selected value='" + listDiver[i].userId + "'>" + listDiver[i].fullName + "</option>";
                            else
                                listOpDown += "<option value='" + listDiver[i].userId + "'>" + listDiver[i].fullName + "</option>";
                        }
                        var stt = "<tr class='tr_sortdrop'data-id='" + count + "'><td style='width: 30px;'><input class='inputSortDrop' data-id='" + count + "' style='width: 30px' type='number'></td>",
                            soghe = "<td>" + (ticketInfo.listSeatId).length + "</td>",
                            mave = "<td class='ticketID' data-val='" + ticketInfo.ticketId + "'>" + ticketInfo.ticketCode + "</td>",
                            mavehide = "<td hidden class='ticketID' data-val='" + ticketInfo.ticketId + "'>" + ticketInfo.ticketCode + "</td>",
                            dsghe = "<td>" + ticketInfo.listSeatId.join(', ') + "</td>",
                            don = "<td>" + (splitString(ticketInfo.pickUpAddress) != '' ? splitString(ticketInfo.pickUpAddress) : (ticketInfo.getInPoint != undefined ? splitString(ticketInfo.getInPoint.pointName) : '')) + "</td>",
                            tra = "<td>" + (splitString(ticketInfo.dropOffAddress) != '' ? splitString(ticketInfo.dropOffAddress) : (ticketInfo.getOffPoint != undefined ? splitString(ticketInfo.getOffPoint.pointName) : '')) + "</td>",
                            hoten = "<td>" + (ticketInfo.fullName) + "</td>",
                            lengthPhone = ticketInfo.phoneNumber.length,
                            sdt = "<td style='width: 85px'>" + (ticketInfo.phoneNumber != '' ? (ticketInfo.phoneNumber.substring(0, lengthPhone - 6) + '' + ticketInfo.phoneNumber.substring(lengthPhone - 6, lengthPhone - 3) + '' + ticketInfo.phoneNumber.substring(lengthPhone - 3)) : '') + "</td>",
                            daili = ticketInfo.sourceName != undefined ? "<td>" + ticketInfo.sourceName + "</td>" : "<td></td>",
                            ghichu = "<td>" + (typeof ticketInfo.note != 'undefined' ? ticketInfo.note : "") + "</td>",
                            taixelen = "<td style='width: 10%'>" + "<select class='driverPickUp' style='width: 150px;' value=''><option></option>" + listOpUp,
                            taixexuong = "<td style='width: 10%'>" + "<select class='driverDropOff' style='width: 150px;' value=''><option></option>" + listOpDown,
                            cuoi = "</select></td></tr>";
                        count++;
                        if ((ticketInfo.ticketStatus == 2 && (ticketInfo.overTime > Date.now())) || ticketInfo.ticketStatus == 4 || ticketInfo.ticketStatus == 3 || ticketInfo.ticketStatus == 7) {
                            htmlListCustomer += stt + mavehide + tra + hoten + sdt + dsghe + ghichu + taixelen + cuoi;
                        }
                    });
                    htmlListCustomer += "<tr class='trsortdrop'><td style='width: 30px'><button class='sortdrop'>Sắp xếp</button>";
                    htmlListCustomer += "<td colspan='5'></td>";
                    htmlListCustomer += "<td><button class='upListDriver' style='width: 100%; background-color: #2E6BD0;border: none ; font-weight: bold; '>Chốt danh sách</button></td></tr>";
                }
                $('#list-customer-dropoff').html(htmlListCustomer);
            @endif
        }

        function checkTicketAndPoint(ticketInfo) {
            var check = 1;
            $('#cbb_InPoint option').each(function () {
                if ($(this).val() == ticketInfo.getInPointId) check = 0;
            });
            return check;
        }

        function generateHtmlCustomer(listSetting, ticketInfo, stt) {
            var html = '<tr>', addressdrop = '', addresspick = '';
            if (ticketInfo.getInPointId != undefined) {
                if (ticketInfo.getInPoint != undefined || ticketInfo.dropAlongTheWayAddress != undefined || ticketInfo.dropOffAddress !=undefined) {
                    if (ticketInfo.dropAlongTheWayAddress !=undefined && ticketInfo.dropAlongTheWayAddress!= '') {
                        addressdrop = ticketInfo.dropAlongTheWayAddress;
                    } else {
                        if (ticketInfo.dropOffAddress != undefined && ticketInfo.dropOffAddress != '') {
                            addressdrop = ticketInfo.dropOffAddress;
                        } else {
                            addressdrop = ticketInfo.getOffPoint.pointName;

                        }
                    }
                    if (addressdrop.split(', ').length >= 2) {
                        addressdrop = addressdrop.split(', ', 2).toString();
                    }
                    if (ticketInfo.dropAlongTheWayAddress !=undefined && ticketInfo.dropAlongTheWayAddress != '')
                        addressdrop = '<strong>' + addressdrop + '</strong>';
                    if (ticketInfo.alongTheWayAddress !=undefined && ticketInfo.alongTheWayAddress != '') {
                        addresspick = ticketInfo.alongTheWayAddress;
                    } else {

                        if (ticketInfo.pickUpAddress != undefined && ticketInfo.pickUpAddress != '') {
                            addresspick = ticketInfo.pickUpAddress;
                        }
                        else {
                            addresspick = ticketInfo.getInPoint.pointName;
                        }
                    }
                    if (addresspick.split(', ').length >= 2) {
                        addresspick = addresspick.split(', ', 2).toString();
                    }

                    if (ticketInfo.alongTheWayAddress !=undefined && ticketInfo.alongTheWayAddress != '') {
                        addresspick = '<strong>' + addresspick + '</strong>';
                    }
                }
            }
            var lengthPhone = ticketInfo.phoneNumber.length;
            $.each(listSetting, function (k, v) {
                if (v.idAttribute != 99) {
                    html += '<td style=" border: 1px solid;text-align: center;' +
                        '                                                font-size: ' + (v.size > 0 ? (v.size + 'px') : '12px') + ';' +
                        '                                               width:' + (v.width > 0 ? (v.width + 'px') : 'auto') + ';' +
                        '                                                height:' + (v.height > 0 ? (v.height + 'px') : 'auto') + ';' +
                        '                                                ' + (v.bold == 1 ? 'font-weight:bold' : '') + ';' +
                        '">';
                    switch (v.idAttribute) {
                        case 0: {
                            html += stt + '</td>';
                            break;
                        }
                        case 1: {
                            html += ticketInfo.ticketCode + '</td>';
                            break;
                        }
                        case 2: {
                            html += ticketInfo.listSeatId.length + '</td>';
                            break;
                        }
                        case 3: {
                            html += ticketInfo.listSeatId.join(', ') + '</td>';
                            break;
                        }
                        case 4: {
                            html += (ticketInfo.fullName != 'Khách vãng lai' ? ticketInfo.fullName : '') + '</td>';
                            break;
                        }
                        case 5: {
                            html += (ticketInfo.phoneNumber !== undefined && ticketInfo.phoneNumber !== '') ? formatPhoneNumber(ticketInfo.phoneNumber) : 'No Number' + '</td>';
                            break;
                        }
                        case 6: {
                            html += addresspick + '</td>';
                            break;
                        }
                        case 7: {
                            html += addressdrop + '</td>';
                            break;
                        }
                        case 8: {
                            html += ticketInfo.sourceName + '</td>';
                            break;
                        }
                        case 9: {
                            if ('{{ session('companyId') }}' == 'TC03m1MSnhqwtfE')
                                if (ticketInfo.ticketStatus == 7)
                                    html += ticketInfo.sourceId == 'TC03m1MSnhqwtfE' ? 'Văn phòng' : (ticketInfo.sourceId == 'anvui' ? 'Chưa thu tiền' : 'Đại lý thu');
                                else
                                    html += ticketInfo.sourceId == 'anvui' ? 'Đã thu' : checkStatusSeatAndTicket(ticketInfo.ticketStatus, ticketInfo.overTime).TicketStatusMessage;
                            else{
                                if(typeof ticketInfo.collectMoneyForAgency!=='undefined'&&ticketInfo.collectMoneyForAgency===false&&ticketInfo.agencyUserId!=undefined){
                                    html+='Đại lý thu tiền';
                                }else{
                                    html += checkStatusSeatAndTicket(ticketInfo.ticketStatus, ticketInfo.overTime).TicketStatusMessage;
                                }
                            }
                            html += '</td>';
                            break;
                        }
                        case 10: {
                            html += $.number(ticketInfo.agencyPrice) + '</td>';
                            break;
                        }
                        case 11: {
                            html += $.number(ticketInfo.paidMoney) + '</td>';
                            break;
                        }
                        case 12: {
                            if(typeof ticketInfo.collectMoneyForAgency!=='undefined'&&ticketInfo.collectMoneyForAgency===false&&ticketInfo.agencyUserId!=undefined){
                                html += ' 0 </td>';
                            }else{
                                html += $.number(ticketInfo.agencyPrice - ticketInfo.paidMoney) + '</td>';
                            }
                            break
                        }
                        case 13: {
                            html += (ticketInfo.note != undefined ? ticketInfo.note : "") + '</td>';
                            break;
                        }
                    }
                }
            });
            html += '</tr>';
            return html;
        }

        function makeListPickup(listTicket) {
            var htmlListCustomer = '', count = 1;
            if (listTicket.length == 0) {
                htmlListCustomer = "<tr><td class='center' colspan='8'>Hiện không có dữ liệu</td></tr>";
            }
            else {
                var listDiver = listDriver;
                listTicket.forEach(function (v) {
                    var ticketInfo = v['ticketInfo'][0],
                        listOpUp = '', listOpDown = '';
                    for (var i = 0; i < listDiver.length; i++) {
                        if ((ticketInfo.transshipmentPickUpDriver != undefined && ticketInfo.transshipmentPickUpDriver == listDiver[i].userId))
                            listOpUp += "<option selected value='" + listDiver[i].userId + "'>" + listDiver[i].fullName + "</option>";
                        else
                            listOpUp += "<option value='" + listDiver[i].userId + "'>" + listDiver[i].fullName + "</option>";
                        if ((ticketInfo.transshipmentDropOffDriver != undefined && ticketInfo.transshipmentDropOffDriver == listDiver[i].userId))
                            listOpDown += "<option selected value='" + listDiver[i].userId + "'>" + listDiver[i].fullName + "</option>";
                        else
                            listOpDown += "<option value='" + listDiver[i].userId + "'>" + listDiver[i].fullName + "</option>";
                    }
                    var stt = "<tr class='tr_sortpick'data-id='" + count + "'><td style='width: 30px'><input class='inputSortpick' data-id='" + count + "' style='width: 30px' type='number'></td>",
                        soghe = "<td>" + (ticketInfo.listSeatId).length + "</td>",
                        mave = "<td class='ticketID' data-val='" + ticketInfo.ticketId + "'>" + ticketInfo.ticketCode + "</td>",
                        mavehide = "<td hidden class='ticketID' data-val='" + ticketInfo.ticketId + "'>" + ticketInfo.ticketCode + "</td>",
                        dsghe = "<td>" + ticketInfo.listSeatId.join(', ') + "</strong></td>",
                        don = "<td>" + (splitString(ticketInfo.pickUpAddress) != '' ? splitString(ticketInfo.pickUpAddress) : (ticketInfo.getInPoint != undefined ? splitString(ticketInfo.getInPoint.pointName) : '')) + "</td>",
                        tra = "<td>" + (splitString(ticketInfo.dropOffAddress) != '' ? splitString(ticketInfo.dropOffAddress) : (ticketInfo.getOffPoint != undefined ? splitString(ticketInfo.getOffPoint.pointName) : '')) + "</td>",
                        hoten = "<td>" + (ticketInfo.fullName) + "</strong></td>",
                        lengthPhone = ticketInfo.phoneNumber.length,
                        sdt = "<td style='width: 85px'>" + (ticketInfo.phoneNumber != '' ? (ticketInfo.phoneNumber.substring(0, lengthPhone - 6) + '' + ticketInfo.phoneNumber.substring(lengthPhone - 6, lengthPhone - 3) + '' + ticketInfo.phoneNumber.substring(lengthPhone - 3)) : '') + "</td>",
                        daili = ticketInfo.sourceName != undefined ? "<td>" + ticketInfo.sourceName + "</td>" : "<td></td>",
                        ghichu = "<td>" + (ticketInfo.note != undefined ? ticketInfo.note : "") + "</td>",
                        taixelen = "<td style='width: 10%'>" + "<select class='driverPickUp' style='width: 150px;' value=''><option></option>" + listOpUp,
                        taixexuong = "<td style='width: 10%'>" + "<select class='driverDropOff' style='width: 150px;' value=''><option></option>" + listOpDown,
                        cuoi = "</select></td></tr>";

                    count++;
                    if ((ticketInfo.ticketStatus == 2 && (ticketInfo.overTime > Date.now())) || ticketInfo.ticketStatus == 4 || ticketInfo.ticketStatus == 3 || ticketInfo.ticketStatus == 7) {
                        if (['TC03m1MSnhqwtfE','TC02orwyNWOVEq','TC07C1pjfHQtyduK'].indexOf('{{session('companyId')}}')>=0) htmlListCustomer += stt + mavehide + don + hoten + sdt + dsghe + ghichu + taixelen + cuoi;
                        else if ('{{session('companyId')}}' == 'TC0481HSc6Pl7ko') htmlListCustomer += stt + hoten + sdt + don + ghichu + cuoi;
                        else htmlListCustomer += stt + mave + soghe + dsghe + hoten + sdt + don + tra + daili + ghichu + taixelen + taixexuong + cuoi;
                    }
                });
                htmlListCustomer += "<tr class='trsortpick'><td style='width: 30px'><button class='sortpick'>Sắp xếp</button>";
                if ('{{session('companyId')}}' == 'TC0481HSc6Pl7ko') htmlListCustomer += "<td colspan='4'></td></tr>";
                else {
                    if (['TC03m1MSnhqwtfE','TC02orwyNWOVEq','TC07C1pjfHQtyduK'].indexOf('{{session('companyId')}}')>=0) htmlListCustomer += "<td colspan='5'></td>";
                    else htmlListCustomer += "<td colspan='10'></td>";
                    htmlListCustomer += "<td><button class='upListDriver' style='width: 100%; background-color: #2E6BD0;border: none ;font-weight: bold; '>Chốt danh sách</button></td></tr>";
                }
            }
            $('.totalSeatPickup').html(count);
            $('#list-customer-pickup').html(htmlListCustomer);
        }

        function makeSeatMapHtml(tripSelected) {
            var ts= tripSelected;
            if (tripSelected.seatList == undefined || tripSelected.tripData.seatMap.seatList != undefined) {
                tripSelected.seatList = tripSelected.tripData.seatMap.seatList;
            }

            if (tripSelected.numberOfColumns == undefined || tripSelected.tripData.seatMap.numberOfColumns != undefined) {
                tripSelected.numberOfColumns = tripSelected.tripData.seatMap.numberOfColumns;
            }

            if (tripSelected.numberOfRows == undefined || tripSelected.tripData.seatMap.numberOfRows != undefined) {
                tripSelected.numberOfRows = tripSelected.tripData.seatMap.numberOfRows;
            }
            if (['TC03m1MSnhqwtfE','TC05zfAUpyzkaB'].indexOf('{{session('companyId')}}')>=0) {
                if ((tripSelected.tripData.seatMap).numberOfFloors > 1 && tripSelected.seatList.length < 30 && tripSelected.seatList.length != 20 && (tripSelected.numberOfColumns <= 3)) {
                    var floorHtml = '';
                    floorHtml += '<div id="table-map_SaoNghe" ><table class="table table-condensed"><thead><tr>';
                    for (var column = 1; column <= (tripSelected.numberOfColumns * 2); column++) {
                        if (column == 2 || column == 5) {
                            if (column == 5) {
                                floorHtml += ' <td style = "width: 4%"></td>';
                            }
                            floorHtml += '<td width="24%" style="border: 1px solid black;margin-top:-10px; margin-bottom:-10px;text-align: center">' +
                                '<strong style="font-size: 16px">' + "Tầng 1" + '</strong></td>';
                        } else if (column == 1 || column == 6) {
                            floorHtml += '<td width="24%" style="border: 1px solid black;margin-top:-10px; margin-bottom:-10px;text-align: center">' +
                                '<strong style="font-size:16px">Tầng 2</strong>' + '</td>';
                        }
                    }
                    floorHtml += '</tr></thead><tbody >';
                    var seat, status;
                    var tripdata = tripSelected.tripData;
                    var seatList = tripSelected.seatList;
                    var count_col = tripSelected.numberOfColumns;
                    var count_row = tripSelected.numberOfRows;
                    evenIndex = ((seatList.length) % 2 == 0 ? (seatList.length) : (seatList.length + 1));
                    oddIndex = ((seatList.length - 1) % 2 == 0 ? (seatList.length - 2) : (seatList.length - 1));
                    for (var row = count_row; row > 1; row--) {
                        floorHtml += '<tr>';
                        for (column = ((count_col - 1) * 2); column > 0; column--) {

                            if (column == (count_col + 1) && row != count_row) {
                                if ((evenIndex - (count_row - row) * 2) <= parseInt(evenIndex)) {
                                    seat = seatList[evenIndex - (count_row - row) * 2];
                                    floorHtml += ' <td style = "border: 1px solid black;width: 24%">';
                                    if ((seat.seatStatus == 3 && seat.ticketInfo != undefined && (seat.ticketInfo[seat.listTicketId[0]].agencyPrice <= seat.ticketInfo[seat.listTicketId[0]].paidMoney)) || (seat.ticketInfo[seat.listTicketId[0]].sourceId != 'TC03m1MSnhqwtfE')
                                    )
                                        floorHtml += '<div class="den"style="float: left; border: 10px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + seatList[evenIndex - (count_row - row) * 2].seatId;
                                    else floorHtml += '<div class="den"style="float: left; border: 1px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + seatList[evenIndex - (count_row - row) * 2].seatId;

                                }
                            } else if (column == count_col && row != count_row) {
                                if ((13 - (count_row - row) * 2) <= 11) {
                                    seat = seatList[13 - (count_row - row) * 2];
                                    floorHtml += ' <td style = "border: 1px solid black;width: 24%">';
                                    if ((seat.seatStatus == 3 && seat.ticketInfo != undefined && (seat.ticketInfo[seat.listTicketId[0]].agencyPrice <= seat.ticketInfo[seat.listTicketId[0]].paidMoney)) || (seat.ticketInfo[seat.listTicketId[0]].sourceId != 'TC03m1MSnhqwtfE')
                                    )
                                        floorHtml += '<div class="den"style="float: left; border: 10px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + seatList[13 - (count_row - row) * 2].seatId;
                                    else
                                        floorHtml += '<div style="float: left; border: 1px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong> ' + seatList[13 - (count_row - row) * 2].seatId;

                                }
                            } else if (column == (count_col - 1)) {
                                floorHtml += ' <td style = "width: 4%"></td>';
                                seat = seatList[12 - (count_row - row) * 2];
                                floorHtml += ' <td style = "border: 1px solid black;width: 24%">';
                                if ((seat.seatStatus == 3 && seat.ticketInfo != undefined && (seat.ticketInfo[seat.listTicketId[0]].agencyPrice <= seat.ticketInfo[seat.listTicketId[0]].paidMoney)) || (seat.ticketInfo[seat.listTicketId[0]].sourceId != 'TC03m1MSnhqwtfE')
                                )
                                    floorHtml += '<div class="den"style="float: left; border: 10px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + seatList[12 - (count_row - row) * 2].seatId;
                                else
                                    floorHtml += '<div style="float: left; border: 1px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong> ' + seatList[12 - (count_row - row) * 2].seatId;

                            } else if (column == 1) {
                                // &&(seat.ticketInfo[seat.listTicketId[0]].paymentTicketPrice <= seat.ticketInfo[seat.listTicketId[0]].paidMoney)
                                if ((oddIndex - (count_row - row) * 2) >= getSeatIndex(seatList)) {
                                    seat = seatList[oddIndex - (count_row - row) * 2] ? seatList[oddIndex - (count_row - row) * 2] : '';
                                    floorHtml += ' <td style = "border: 1px solid black;width: 24%">';
                                    if (seat != '')
                                        if ((seat.seatStatus == 3 && seat.ticketInfo != undefined && (seat.ticketInfo[seat.listTicketId[0]].agencyPrice <= seat.ticketInfo[seat.listTicketId[0]].paidMoney)) || (seat.ticketInfo[seat.listTicketId[0]].sourceId != 'TC03m1MSnhqwtfE')
                                        )
                                            floorHtml += '<div class="den"style="float: left; border: 10px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + seatList[oddIndex - (count_row - row) * 2].seatId;
                                        else
                                            floorHtml += '<div style="float: left; border: 1px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong> ' + seatList[oddIndex - (count_row - row) * 2].seatId;

                                }
                            } else floorHtml += ' <td style = "border: 1px solid black;width: 25%">';
                            if (seat != null && seat.seatStatus != null) {

                                if (seat.listTicketId && seat.listTicketId.length > 0 && (seat.overTime == 0 || seat.overTime > Date.now() || seat.ticketInfo[seat.listTicketId[0]].paidMoney > 0)) {

                                    var dem = 0;
                                    if (seat.ticketInfo[seat.listTicketId[0]].alongTheWayAddress != null && (seat.ticketInfo[seat.listTicketId[0]].alongTheWayAddress != undefined))
                                        var pickUp = '<strong>' + (seat.ticketInfo[seat.listTicketId[0]].alongTheWayAddress) + '</strong>';
                                    else
                                        var pickUp = '';
                                    if (seat.ticketInfo[seat.listTicketId[0]].dropAlongTheWayAddress != null && (seat.ticketInfo[seat.listTicketId[0]].dropAlongTheWayAddress != undefined))
                                        var drofOff = '<strong>' + (seat.ticketInfo[seat.listTicketId[0]].dropAlongTheWayAddress) + '</strong>';
                                    else
                                        var drofOff = '';
                                    if (trip == undefined) {
                                        var trip = {
                                            childrenTicketRatio: 0,
                                            numberOfChildren: 0,
                                            numberOfAdults: 0,
                                            seatList: [],
                                        };

                                        trip.childrenTicketRatio = tripSelected.childrenTicketRatio;
                                        for (var i = 0; i < tripSelected.seatList.length; i++)
                                            trip.seatList[i] = JSON.parse(JSON.stringify(tripSelected.seatList[i]));
                                    }
                                    var priceSeat = getSeatPrice(trip, seat.ticketInfo[seat.listTicketId[0]], seat, seat['listTicketId'][0]);
                                    if ('{{session('companyId')}}' == 'TC03m1MSnhqwtfE')
                                        floorHtml += '</strong></h3></div>Mã vé :<strong>' + seat.ticketInfo[seat.listTicketId[0]].ticketCode + '</strong>';
                                    floorHtml += ($.trim(seat.ticketInfo[seat.listTicketId[0]].fullName) != '' && seat.ticketInfo[seat.listTicketId[0]].fullName != 'Khách vãng lai') ? ('<strong  style="float: left;">' + seat.ticketInfo[seat.listTicketId[0]].fullName + '</strong>') : '';
                                    floorHtml += $.trim(seat.ticketInfo[seat.listTicketId[0]].phoneNumber) != '' ? ('  -  <strong>' + seat.ticketInfo[seat.listTicketId[0]].phoneNumber + '</strong>') : '';
                                    floorHtml += '</br></br><div><span  style="width: 100%">  ' + ($.trim(pickUp) != '' ? pickUp : (seat.ticketInfo[seat.listTicketId[0]].pickUpAddress != undefind ? seat.ticketInfo[seat.listTicketId[0]].pickUpAddress : tripdata.getInPointName)) + '</span>';
                                    floorHtml += '<span style="width: 100%" > ' + ($.trim(drofOff) != '' ? drofOff : (seat.ticketInfo[seat.listTicketId[0]].dropOffAddress != undefind ? seat.ticketInfo[seat.listTicketId[0]].dropOffAddress : tripdata.getOffPointName)) + '</span></div>';
                                    if (seat.ticketInfo[seat.listTicketId[0]].paymentTicketPrice)
                                        floorHtml += '<h5  style="width: 100%;text-align: left;"><strong>' + priceSeat + '</strong></h5>';

                                } else {
                                    floorHtml += '</strong></h3></div>';

                                }
                            }
                            seat = null;
                            floorHtml += '</td>';
                        }
                        floorHtml += '</tr>';
                    }
                    floorHtml += '</tbody></table></div>';
                }
                else {
                    if (tripSelected.numberOfFloors == undefined) {
                        tripSelected = tripSelected.tripData.seatMap;
                    }
                    width = 100 / tripSelected.numberOfColumns;
                    floorHtml = '';
                    for (var floor = 1; floor <= tripSelected.numberOfFloors; floor++) {
                        floorHtml += '<table class="table table-condensed">';
                        floorHtml += '<tbody>';
                        for (var row = tripSelected.numberOfRows; row >= 1; row--) {
                            floorHtml += '<tr>';
                            for (var col = tripSelected.numberOfColumns; col >= 1; col--) {
                                flat = false;
                                $.each(tripSelected.seatList, function (k, v) {
                                    // console.log(v);
                                    if (v.row == row && v.column == col && v.floor == floor) {
                                        if (v.seatType == 1) {
                                            floorHtml += '<td style = "width:' + width + '%"></td>';
                                        } else if (v.seatType == 2) {
                                            floorHtml += '<td style = "width:' + width + '%"></td>';
                                        }
                                        else if (v.seatType == 3 || v.seatType == 4) {
                                            if (v['listTicketId'] != undefined && v['listTicketId'][0] != undefined && v['listTicketId'][0] != '') {
                                                if ((v.seatStatus == 2 && (v.overTime != undefined && (v.overTime > Date.now() || v.overTime == 0))) || v.seatStatus == 3 || v.seatStatus == 4 || (v['ticketInfo'] != undefined && (v['ticketInfo']).length > 0 && v['ticketInfo'][0].ticketStatus != undefined && v['ticketInfo'][0].ticketStatus == 7)) {
                                                    if (typeof v.ticketInfo !== 'undefined') {
                                                        var ticketInfo = v['ticketInfo'][0];
                                                        if (trip == undefined) {
                                                            var trip = {
                                                                childrenTicketRatio: 0,
                                                                numberOfChildren: 0,
                                                                numberOfAdults: 0,
                                                                seatList: [],
                                                            };

                                                            trip.childrenTicketRatio = tripSelected.childrenTicketRatio;
                                                            for (var i = 0; i < tripSelected.seatList.length; i++)
                                                                trip.seatList[i] = JSON.parse(JSON.stringify(tripSelected.seatList[i]));
                                                        }

                                                        var priceSeat = getSeatPrice(trip, ticketInfo, v, v['listTicketId'][0]);
                                                        var drop = (splitString(ticketInfo.dropAlongTheWayAddress) != '' ? splitString(ticketInfo.dropAlongTheWayAddress) : ((ticketInfo.dropOffAddress != undefined && $.trim(ticketInfo.dropOffAddress) != '') ? ticketInfo.dropOffAddress : splitString(ticketInfo.getOffPoint.pointName)));

                                                        var pick = (splitString(ticketInfo.alongTheWayAddress) != '' ? splitString(ticketInfo.alongTheWayAddress) : ((ticketInfo.pickUpAddress != undefined && $.trim(ticketInfo.pickUpAddress) != '') ? ticketInfo.pickUpAddress : splitString(ticketInfo.getInPoint.pointName)));
                                                      floorHtml += '<td style = "border: 2px solid black;width:' + width + '%">';
                                                        if (v.seatStatus == 3 && v.ticketInfo != undefined && (ticketInfo.agencyPrice <= ticketInfo.paidMoney))
                                                            floorHtml += '<div class="den"style="float: left; border: 10px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + v.seatId;
                                                        else floorHtml += '<div class="den"style="float: left; border: 1px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + v.seatId;

                                                        floorHtml += '</strong></h4></div>';
                                                        if ('{{session('companyId')}}' == 'TC03m1MSnhqwtfE') {
                                                            floorHtml += 'Mã vé :<strong>' + ticketInfo.ticketCode + '</strong>';
                                                            floorHtml += '<br><br><br>';
                                                            floorHtml += ($.trim(ticketInfo.fullName) != '' && ticketInfo.fullName != 'Khách vãng lai') ? ('Khách hàng:<strong>' + ticketInfo.fullName + '</strong><br>') : '';
                                                            floorHtml += $.trim(ticketInfo.phoneNumber) != '' ? ('Số điện thoại: ' + ticketInfo.phoneNumber + '<br>') : '';
                                                            floorHtml += 'Điểm lên: ' + (pick == splitString(ticketInfo.alongTheWayAddress) ? ('<strong>' + pick + '</strong>') : pick) + '<br>';

                                                            floorHtml += 'Điểm xuống:' + (drop == splitString(ticketInfo.dropAlongTheWayAddress) ? ('<strong>' + drop + '</strong>') : drop) + '<br>';
                                                            floorHtml += 'Tiền :<strong>' + priceSeat + '</strong></td>';
                                                        } else {
                                                            var html = '',margin=1;
                                                            html += ($.trim(ticketInfo.fullName) != '' && ticketInfo.fullName != 'Khách vãng lai') ? ('<strong>' + ticketInfo.fullName + '</strong>') : '';
                                                            html += $.trim(ticketInfo.phoneNumber) != '' ? ('- <strong style="font-size: 18px">' + ticketInfo.phoneNumber + '</strong>') : '';
                                                             if (html == '') margin=45;
                                                            floorHtml += '<div>'+html;
                                                            floorHtml += '<div style="margin-top:'+margin+'px;"><strong style="font-size: 18px;">'+ pick + '</strong><br>';

                                                            floorHtml += (drop == splitString(ticketInfo.dropAlongTheWayAddress) ? ('<strong>' + drop + '</strong>') : drop) + '<br>';
                                                            floorHtml += '<strong>' + priceSeat + '</strong></div></div></td>';
                                                        }
                                                    }
                                                }
                                            } else if (v.seatStatus == 2) {
                                                floorHtml += '<td style = "border: 2px solid black;width:' + width + '%"><div class="den"style="float: left; border: 1px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + v.seatId + '</strong></h4></div></td>';
                                            }
                                            if (v.seatStatus == 1 && (v.seatType == 4 || v.seatType == 3)) floorHtml += '<td style = "border: 2px solid black;width:' + width + '%"><div class="den"style="float: left; border: 1px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + v.seatId + '</strong></h4></div></td>';
//                                         else {
//                                            floorHtml += '<td><strong>Ghế ' + v.seatId + ': ' + checkStatusSeat(v.seatStatus, v.overTime) + '</strong></td>';
//                                        }

                                        }
                                        else if (v.seatType == 5) {
                                            // floorHtml += '<td><strong>Nhà vệ sinh</strong><br></td>';
                                            floorHtml += '<td style = "width:' + width + '%"></td>';
                                        }
                                        else if (v.seatType == 6) {
                                            // floorHtml += '<td><strong>Ghế phụ xe</strong><br>' +
                                            //     '<strong></strong><br>' +
                                            //     'Tên phụ xe</td>';
                                            floorHtml += '<td style = "width:' + width + '%"></td>';
                                        }
                                        flat = true;
                                    }
                                });
                                if (!flat) {
                                    floorHtml += '<td style = "width:' + width + '%"></td>';
                                }
                            }
                            floorHtml += '</tr>';
                        }
                        floorHtml += '</tbody>';
                        floorHtml += '<table>';
                    }
                }
                floorHtml += '</br><div style="width: 100%;">';
                if('{{session('companyId')}}'!='TC05zfAUpyzkaB') {
                    floorHtml += '<div style="float: left; border: 8px solid;padding: 5px;margin-right: 8px;">';
                    floorHtml += '</div><span>Đã thu tiền</span>';
                    floorHtml += '</br></br><div style="float: left; border: 1px solid;padding: 11px;margin-right: 10px;">';
                    floorHtml += '</div><span>Chưa thu tiền</span>';
                }
                floorHtml += '<div style=" margin: -12px 0 0 80%;">' + $('.underTable').html() + '</div>' +
                    '<div style="margin: -20px 0 0 36%;">' + $('.note_minhtam').html() + '</div>';
                floorHtml += "</div>";
                evenIndex = 0;
                oddIndex = 0;
            }
            else
                if ('{{session('companyId')}}' == 'TC03h1IzK1jParS' && tripSelected.tripData.seatMap.seatList.length <= 10) {
                if (tripSelected.numberOfFloors == undefined) {
                    tripSelected = tripSelected.tripData.seatMap;
                }
                width = 100 / tripSelected.numberOfColumns;
                for (var floor = 1; floor <= tripSelected.numberOfFloors; floor++) {
                    floorHtml = '<table class="" style="border-collapse: separate;width:100%;border-spacing:2px;" border="1">';
                    floorHtml += '<tbody>';
                    for (var row = 1; row <= tripSelected.numberOfRows; row++) {
                        floorHtml += '<tr>';
                        for (var col = 1; col <= tripSelected.numberOfColumns; col++) {
                            flat = false;
                            $.each(tripSelected.seatList, function (k, v) {
                                if (v.row == row && v.column == col && v.floor == floor) {
                                    if (v.seatType == 1) {
                                        floorHtml += '<td style="width:' + width + '%;text-align: center;padding:5px"><strong>Cửa</strong><br></td>';
                                    } else if (v.seatType == 2) {
                                        floorHtml += '<td style="width:' + width + '%;text-align: center;padding:5px"><strong>Ghế tài xế</strong><br></td>';
                                    }
                                    else if (v.seatType == 3 || v.seatType == 4) {
                                        if ((v.seatStatus == 2 && (v.overTime > Date.now() || v.overTime == 0)) || v.seatStatus == 3 || v.seatStatus == 4 || v.ticketStatus == 7) {
                                            if (v['listTicketId'] != undefined && v['listTicketId'][0] != '') {
                                                if (typeof v, ticketInfo !== 'undefined') {
                                                    var ticketInfo = v['ticketInfo'][0];
                                                    if (trip == undefined) {
                                                        var trip = {
                                                            childrenTicketRatio: 0,
                                                            numberOfChildren: 0,
                                                            numberOfAdults: 0,
                                                            seatList: [],
                                                        };
                                                        trip.childrenTicketRatio = tripSelected.childrenTicketRatio;
                                                        for (var i = 0; i < tripSelected.seatList.length; i++)
                                                            trip.seatList[i] = JSON.parse(JSON.stringify(tripSelected.seatList[i]));
                                                    }
                                                    var updown = getNameUpDown(ticketInfo);
//
                                                    floorHtml += '<td style="width:' + width + '%;padding:5px">' +
                                                        '<strong style="font-size: 15px;">' + v.seatId + '</strong></br>' +
                                                        'KH: <strong style="font-size:17px">' + (ticketInfo.phoneNumber != undefined ? formatPhoneNumber(ticketInfo.phoneNumber) : '') + '</strong>, ( ' + ticketInfo.listSeatId.length + 'G ) , ' + moneyFormat(ticketInfo.unPaidMoney) + '</br>';
                                                    floorHtml += (ticketInfo.fullName != undefined && $.trim(ticketInfo.fullName) != '') ? ('Tên: <strong>' + ticketInfo.fullName + '</strong></br>') : '';
                                                    floorHtml += 'Điểm lên : <strong>' + updown.up + '</strong>' + '<br>' +
                                                        'Điểm xuống : <strong>' + updown.down + '</strong><br>';
                                                    floorHtml += (ticketInfo.note != undefined && $.trim(ticketInfo.note) != '') ? ('GC:' + ticketInfo.note) : '';
                                                    floorHtml += '</td>';
                                                }
                                            }
                                        } else if (v.seatStatus == 2) {
                                            floorHtml += '<td style="width:' + width + '%;height:140px;padding:5px;vertical-align: top"><strong> ' + v.seatId + '</strong></td>';
                                        }
                                        if (v.seatStatus == 1 && (v.seatType == 4 || v.seatType == 3))
                                            floorHtml += '<td style="width:' + width + '%; height:140px;vertical-align: top;padding:5px">' + '<strong style="font-size: 15px;">' + v.seatId + '</strong></br>' + '</td>';
//
                                    }
                                    else if (v.seatType == 5) {
                                        floorHtml += '<td style="width:' + width + '%;;text-align: center;padding:5px"><strong>Nhà vệ sinh</strong><br></td>';
                                    }
                                    else if (v.seatType == 6) {
                                        floorHtml += '<td style="width:' + width + '%;;text-align: center;padding:5px"><strong>Ghế phụ xe</strong><br>' +
                                            '<strong></strong><br>' +
                                            'Tên phụ xe</td>';
                                    }
                                    flat = true;
                                }
                            });
                            if (!flat) {
                                floorHtml += '<td style="width:' + width + '%;border:0"></td>';
                            }
                        }
                        floorHtml += '</tr>';
                    }
                    floorHtml += '</tbody>';
                    floorHtml += '<table>';
                }
                floorHtml += '<div style="margin-top:10px">Tổng số ghế : <span style="font-size:16px;font-weight:bold">' + (countSeat.giucho + countSeat.pay) + '</span> ,  Tổng tiền : <span style="font-size:16px;font-weight:bold">' + moneyFormat(countSeat.totalUnPaidMoney) + '</span><br><span style="font-style: italic">Ghi chú : </span><span style="font-weight:bold">' + (ts.tripData.note || '') + '</span></div>';
                $('.underTable').html('<div style="width: 100%; text-align: center;">Chú ý :<strong>Đề nghị lái xe giữ chỗ cho khách hàng đã đặt vé qua Tổng đài, App và Website</strong></div>');
            }
            else if (['TC1OHoeA_ekC','TC1OHnILfV3k'].indexOf('{{session('companyId')}}')>=0) {
                if (tripSelected.numberOfFloors == undefined) {
                    tripSelected = tripSelected.tripData.seatMap;
                }
                width = 100 / tripSelected.numberOfColumns;
                height = 140;
                floorHtml = '';
                for (var floor = 1; floor <= tripSelected.numberOfFloors; floor++) {
                    floorHtml += '<table class="table table-condensed">';
                    floorHtml += '<caption>Tầng ' + floor + '</caption>';
                    floorHtml += '<tbody>';
                    for (var row = 1; row <= tripSelected.numberOfRows; row++) {
                        floorHtml += '<tr>';
                        for (var col = 1; col <= tripSelected.numberOfColumns; col++) {
                            flat = false;
                            $.each(tripSelected.seatList, function (k, v) {
                                if (v.row == row && v.column == col && v.floor == floor) {
                                    if (v.seatType == 1) {
                                        floorHtml += '<td width="' + width + '%" ><strong>Cửa</strong><br></td>';
                                    } else if (v.seatType == 2) {
                                        floorHtml += '<td width="' + width + '%"><strong>Ghế tài xế</strong><br></td>';
                                    }
                                    else if (v.seatType == 3 || v.seatType == 4) {
                                        if ((v.seatStatus == 2 && (v.overTime > Date.now() || v.overTime == 0)) || v.seatStatus == 3 || v.seatStatus == 4 || v.ticketStatus == 7) {
                                            if (v['listTicketId'] != undefined && v['listTicketId'][0] != '') {
                                                if (typeof v, ticketInfo !== 'undefined') {
                                                    var ticketInfo = v['ticketInfo'][0];
                                                    if (trip == undefined) {
                                                        var trip = {
                                                            childrenTicketRatio: 0,
                                                            numberOfChildren: 0,
                                                            numberOfAdults: 0,
                                                            seatList: [],
                                                        };

                                                        trip.childrenTicketRatio = tripSelected.childrenTicketRatio;
                                                        for (var i = 0; i < tripSelected.seatList.length; i++)
                                                            trip.seatList[i] = JSON.parse(JSON.stringify(tripSelected.seatList[i]));
                                                    }
                                                    var priceSeat = getSeatPrice(trip, ticketInfo, v, v['listTicketId'][0]);
                                                    var drop = (splitString(ticketInfo.dropAlongTheWayAddress) != '' ? splitString(ticketInfo.dropAlongTheWayAddress) : (ticketInfo.getOffPoint != undefined ? splitString(ticketInfo.getOffPoint.pointName) : ''));

                                                    var pick = (splitString(ticketInfo.alongTheWayAddress) != '' ? splitString(ticketInfo.alongTheWayAddress) : (ticketInfo.getInPoint != undefined ? splitString(ticketInfo.getInPoint.pointName) : ''));
//
                                                    floorHtml += ' <td>';
                                                    if ('{{session('companyId')}}' == 'TC1OHoeA_ekC') {
                                                        if ((ticketInfo.ticketStatus == 3 && (ticketInfo.agencyPrice <= ticketInfo.paidMoney)) || (ticketInfo.sourceId != 'TC1OHoeA_ekC')
                                                        )
                                                            floorHtml += '<div class="den"style="float: left; border: 10px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + v.seatId;
                                                        else floorHtml += '<div class="den"style="float: left; border: 1px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + v.seatId;
                                                        floorHtml += '</strong></h4></div></br>Mã vé :<strong>' + ticketInfo.ticketCode + '</strong><br>';
                                                        floorHtml += (ticketInfo.fullName != 'Khách vãng lai' && $.trim(ticketInfo.fullName) != '') ? ('Khách hàng: <strong>' + ticketInfo.fullName + '</strong><br>') : '';
                                                        floorHtml += $.trim(ticketInfo.phoneNumber) != '' ? ('Số điện thoại: ' + ticketInfo.phoneNumber + '<br>') : '';
                                                        floorHtml += 'Điểm lên: ' + (pick == splitString(ticketInfo.alongTheWayAddress) ? ('<strong>' + pick + '</strong>') : pick) + '<br>';

                                                        floorHtml += 'Điểm xuống:' + (drop == splitString(ticketInfo.dropAlongTheWayAddress) ? ('<strong>' + drop + '</strong>') : drop) + '<br>';
                                                        floorHtml += 'Tiền:<strong>' + priceSeat + '</strong></td>';
                                                    } else {
                                                        floorHtml += '<div class="row-fluid" style="height: 25px">' +
                                                            '<div style="float: left;font-size: 20px">' + (ticketInfo.ticketStatus == 3 ? ('<strong>' + v.seatId + '</strong>') : v.seatId) + '</div>' +
                                                            '<div style="float: right;font-size: 16px">' + (ticketInfo['cashierName'] || ticketInfo['sellerName']) + ' -<strong>' + (ticketInfo.listSeatId.length) + 'G' + '</strong></div>' +
                                                            '</div>';
                                                        floorHtml += '<div class="row-fluid" style="height: 25px">' +
                                                            '<div style="float: left;font-size: 18px">' + ticketInfo.fullName + '</div>' +
                                                            '<div style="float: right;font-size: 18px">' + (pick == splitString(ticketInfo.alongTheWayAddress) ? ('<strong>' + pick + '</strong>') : pick) + '</div>' +
                                                            '</div>';
                                                        floorHtml += '<div class="row-fluid" style="font-size:18px;height: 25px;text-align:center;font-weight: bold">' +
                                                            ticketInfo.phoneNumber +
                                                            '</div>';
                                                        floorHtml += '<div class="row-fluid">' + ticketInfo.note + '</div></td>';
                                                    }
                                                }
                                            }
                                        } else if (v.seatStatus == 2) {
                                            if ('{{session('companyId')}}' == 'TC1OHoeA_ekC')
                                                floorHtml += '<td width="' + width + '%" height="' + height + 'px">Ghế <strong>' + v.seatId + ': ' + checkStatusSeat(v.seatStatus, v.overTime) + '</strong></td>';
                                            else floorHtml += '<td width="' + width + '%" height="' + height + 'px">' + v.seatId + '</td>';
                                        }
                                        if (v.seatStatus == 1 && (v.seatType == 4 || v.seatType == 3)) {
                                            floorHtml += '<td width="' + width + '%" height="' + height + 'px">';
                                            if ('{{session('companyId')}}' == 'TC1OHoeA_ekC') floorHtml += '<div class="den" style="float: left; border: 1px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + v.seatId + '</strong></h4><//div></td>';
                                            else floorHtml += '<div style="font-size: 18px">' + v.seatId + '</div></td>';
                                        }
                                    }
                                    else if (v.seatType == 5) {
                                        floorHtml += '<td><strong>Nhà vệ sinh</strong><br></td>';
                                    }
                                    else if (v.seatType == 6) {
                                        floorHtml += '<td><strong>Ghế phụ xe</strong><br>' +
                                            '<strong></strong><br>' +
                                            'Tên phụ xe</td>';
                                    }
                                    flat = true;
                                }
                            });
                            if (!flat) {
                                floorHtml += '<td width="' + width + '%" height="' + height + 'px"><br><br></td>';
                            }
                        }
                        floorHtml += '</tr>';
                    }
                    floorHtml += '</tbody>';
                    floorHtml += '<table>';
                    if ('{{session('companyId')}}' == 'TC1OHoeA_ekC') {
                        floorHtml += '</br><div style="float: left;">';
                        floorHtml += '<div style="float: left; border: 8px solid;padding: 5px;margin-right: 8px;">';
                        floorHtml += '</div><span>Đã thu tiền</span>';
                        floorHtml += '</br></br><div style="float: left; border: 1px solid;padding: 11px;margin-right: 10px;">';
                        floorHtml += '</div><span>Chưa thu tiền</span>';
                        floorHtml += "</div>";
                    }
                }
            }
            else {

                if (tripSelected.numberOfFloors == undefined) {
                    tripSelected = tripSelected.tripData.seatMap;
                }
                width = 100 / tripSelected.numberOfColumns;
                floorHtml = '';
                for (var floor = 1; floor <= tripSelected.numberOfFloors; floor++) {
                    floorHtml += '<table class="table table-condensed">';
                    floorHtml += '<caption>Tầng ' + floor + '</caption>';
                    floorHtml += '<tbody>';
                    for (var row = 1; row <= tripSelected.numberOfRows; row++) {
                        floorHtml += '<tr>';
                        for (var col = 1; col <= tripSelected.numberOfColumns; col++) {
                            flat = false;
                            $.each(tripSelected.seatList, function (k, v) {
                                if (v.row == row && v.column == col && v.floor == floor) {
                                    if (v.seatType == 1) {
                                        floorHtml += '<td width="' + width + '%"><strong>Cửa</strong><br></td>';
                                    } else if (v.seatType == 2) {
                                        floorHtml += '<td width="' + width + '%"><strong>Ghế tài xế</strong><br></td>';
                                    }
                                    else if (v.seatType == 3 || v.seatType == 4) {
                                        if ((v.seatStatus == 2 && (v.overTime > Date.now() || v.overTime == 0)) || v.seatStatus == 3 || v.seatStatus == 4 || v.ticketStatus == 7) {
                                            if (v['listTicketId'] != undefined && v['listTicketId'][0] != '') {
                                                if (typeof v, ticketInfo !== 'undefined') {
                                                    var ticketInfo = v['ticketInfo'][0];
                                                    if (trip == undefined) {
                                                        var trip = {
                                                            childrenTicketRatio: 0,
                                                            numberOfChildren: 0,
                                                            numberOfAdults: 0,
                                                            seatList: [],
                                                        };

                                                        trip.childrenTicketRatio = tripSelected.childrenTicketRatio;
                                                        for (var i = 0; i < tripSelected.seatList.length; i++)
                                                            trip.seatList[i] = JSON.parse(JSON.stringify(tripSelected.seatList[i]));
                                                    }
                                                    var priceSeat = getSeatPrice(trip, ticketInfo, v, v['listTicketId'][0]);
                                                    var updown = getNameUpDown(ticketInfo);
                                                    floorHtml += '<td>' +
                                                        'Ghế <strong>' + v.seatId + ': ' + (ticketInfo.ticketStatus != 7 ? checkStatusSeat(v.seatStatus, v.overTime) : 'Giữ chỗ') + '</strong><br>' +
                                                        'Mã vé :' + ticketInfo.ticketCode + '</strong><br>';
                                                    floorHtml += $.trim(ticketInfo.fullName) != '' ? ('Khách hàng: ' + ticketInfo.fullName + '<br>') : '';
                                                    floorHtml += $.trim(ticketInfo.phoneNumber) != '' ? ('Số điện thoại: ' + ticketInfo.phoneNumber + '<br>') : '';
                                                    floorHtml += 'Điểm lên : ' + '<strong>' + updown.up + '</strong>' + '<br>';
                                                    floorHtml += 'Điểm xuống : <strong>' + updown.down + '</strong><br>';
                                                    floorHtml += 'Tiền:<strong>' + priceSeat + '</strong></td>';
                                                }
                                            }
                                        } else if (v.seatStatus == 2) {
                                            floorHtml += '<td width="' + width + '%"><strong>Ghế ' + v.seatId + ': ' + checkStatusSeat(v.seatStatus, v.overTime) + '</strong></td>';
                                        }
                                        if (v.seatStatus == 1 && (v.seatType == 4 || v.seatType == 3)) floorHtml += '<td width="' + width + '%"><strong></strong><br><strong></strong><br></td>';
//                                         else {
//                                            floorHtml += '<td><strong>Ghế ' + v.seatId + ': ' + checkStatusSeat(v.seatStatus, v.overTime) + '</strong></td>';
//                                        }
                                    }
                                    else if (v.seatType == 5) {
                                        floorHtml += '<td><strong>Nhà vệ sinh</strong><br></td>';
                                    }
                                    else if (v.seatType == 6) {
                                        floorHtml += '<td><strong>Ghế phụ xe</strong><br>' +
                                            '<strong></strong><br>' +
                                            'Tên phụ xe</td>';
                                    }
                                    flat = true;
                                }
                            });
                            if (!flat) {
                                floorHtml += '<td width="' + width + '%"><strong></strong><br><strong></strong><br></td>';
                            }
                        }
                        floorHtml += '</tr>';
                    }
                    floorHtml += '</tbody>';
                    floorHtml += '<table>';
                }
            }

            $('#makeMap').html(floorHtml);
        }

        function getSeatPrice(trip, ticketInfo, v, ticketId) {
            var sale = 0;
            var price = 0;
            var numberSeat = ticketInfo.numberOfAdults + ticketInfo.numberOfChildren;
            // if (ticketInfo.promotionCode != undefined && ticketInfo.promotionCode != '')
            //     $.ajax({
            //         'url': urlCheckPromotionCode,
            //         'data': {
            //             'scheduleId': $('#txt_scheduleId').val(),
            //             'promotionCode': ticketInfo.promotionCode
            //         },
            //         'dataType': 'json',
            //         'success': function (data) {
            //             var result = data.result;
            //             if (result.percent != undefined && result.price != undefined) {
            //                 if (result.percent > 0) {
            //                     sale = base_price * result.percent;
            //                 }
            //                 if (result.price > 0) {
            //                     sale = result.price;
            //                 }
            //             }
            //         }
            //     });
            // if (ticketInfo.agencyPrice == (ticketInfo.originalTicketPrice + ticketInfo.mealPrice + ticketInfo.priceInsurrance - sale * numberSeat)) {
            //     if (ticketInfo.numberOfChildren > 0) {
            //         price = base_price * trip.childrenTicketRatio + (ticketInfo.mealPrice + ticketInfo.priceInsurrance) / numberSeat - sale + v.extraPrice;
            //         for (var i = 0; i < trip.seatList.length; i++)
            //             if (trip.seatList[i].listTicketId != undefined && trip.seatList[i].listTicketId[0] != undefined && trip.seatList[i].listTicketId[0] == ticketId)
            //                 trip.seatList[i].ticketInfo.numberOfChildren = trip.seatList[i].ticketInfo.numberOfChildren - 1;
            //     } else if (ticketInfo.numberOfAdults > 0) {
            //         price = base_price + (ticketInfo.mealPrice + ticketInfo.priceInsurrance) / numberSeat - sale + v.extraPrice;
            //
            //         for (var i = 0; i < trip.seatList.length; i++)
            //             if (trip.seatList[i].listTicketId != undefined && trip.seatList[i].listTicketId[0] != undefined && trip.seatList[i].listTicketId[0] == ticketId)
            //                 trip.seatList[i].ticketInfo.numberOfAdults = trip.seatList[i].ticketInfo.numberOfAdults - 1;
            //     }
            //     for (var i = 0; i < trip.seatList.length; i++)
            //         if (trip.seatList[i].listTicketId != undefined && trip.seatList[i].listTicketId[0] != undefined && trip.seatList[i].listTicketId[0] == ticketId) {
            //             trip.seatList[i].ticketInfo.mealPrice = trip.seatList[i].ticketInfo.mealPrice - trip.seatList[i].ticketInfo.mealPrice / numberSeat;
            //             trip.seatList[i].ticketInfo.priceInsurrance = trip.seatList[i].ticketInfo.priceInsurrance - trip.seatList[i].ticketInfo.priceInsurrance / numberSeat;
            //         }
            //
            //    if(ticketInfo.)
            //
            // } else {
            var num = 0, list = '(';
            for (var i = 0; i < trip.seatList.length; i++) {
                if (trip.seatList[i].listTicketId != undefined && trip.seatList[i].listTicketId[0] != undefined && tripSelected.seatList[i].listTicketId[0] == ticketId) {
                    num++;
                    if (list == '(')
                        list = list + tripSelected.seatList[i].seatId;
                    else list = list + ',' + tripSelected.seatList[i].seatId;
                }
            }
            list += ')';
            if (num == 1)
                if ((ticketInfo.agencyPrice - ticketInfo.paidMoney) != 0) {
                    price = moneyFormat(ticketInfo.agencyPrice - ticketInfo.paidMoney);
                }
                else price = "Đã trả tiền";
            else if ((ticketInfo.agencyPrice - ticketInfo.paidMoney) != 0) price = moneyFormat(ticketInfo.agencyPrice - ticketInfo.paidMoney) + '/' + list;
            else price = "Đã trả tiền ";

            // }
            return price;
        }

        function checkStatusSeat(status, overTime) {

            /*
            *   INVALID(-2),
                CANCELED(0),
                EMPTY(1), // rỗng. hết hạn giữ chỗ sẽ chuyển về trạng thái này
                BOOKED(2), // đã giữ - (SMS khoảng cách <=10 km)
                BOUGHT(3), // đã thanh toán - (SMS khoảng cách <=10 km)
                ON_THE_TRIP(4), // đã lên xe
                COMPLETED(5), // đã hoàn thành
                OVERTIME(6),// quá giờ giữ chỗ
                BOOKED_ADMIN(7) // siêu phụ xe đặt chỗ
            * */

            switch (status) {
                case 0:
                case 1:
                    return 'Ghế trống';
                case 5:
                case 6:
                    return 'Hết hạn giữ chỗ';
                case 3:
                    return 'Đã thanh toán';

                case 4:
                    return 'Đã lên xe';

                    break;
                case 2:
                    var timess = (Date.now() > tripSelected.tripData.getInTime) ? tripSelected.tripData.getInTime : Date.now();
                    if (overTime == 0 || overTime > timess) {
                        return 'Giữ chỗ';
                    } else {
                        return 'Hết hạn giữ chỗ';
                    }
                case 7:
                    return 'Ưu tiên giữ chỗ';
                default:
                    return 'Không xác định';
            }
        }

        function formatDate(int) {
            var date = new Date(int);

            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            return dd + '-' + mm + '-' + yyyy;
        }

        function getListNameWithArrayUser(arrUser) {
            console.log(arrUser);
            var arr = {fullName: [], userId: []};
            $.each(arrUser, function (i, e) {
                arr.fullName.push(e.fullName || '');
                arr.userId.push(e.userId);
            });
            return arr;
        }

        function getListNameAndPhoneUser(listUser) {
            var arr = [];
            if (listUser.length <= 0) {
                arr.push('<span style="color:red">Chưa chọn</span>');
                return arr;
            }
            $.each(listUser, function (i, e) {
                try{
                    str = (e.fullName || '') + ' ( <span style="cursor:pointer" title="Gọi tới SDT này" data-call-to-phone="0' + (e.phoneNumber||'') + '">+84 ' + (e.phoneNumber || ' ') + ' </span>) ';
                    arr.push(str);
                }catch(err){
                    console.log(e);
                }
            });
            return arr;
        }

        function formatPhoneNumber(str) {
            str1 = str.substr(-3, 3);
            str2 = str.substr(-6, 3);
            str3 = str.substring(0, str.length - 6);
            return str3 + ' ' + str2 + ' ' + str1;
        }

        function checkHistoryType(type) {
            str = '';
            switch (type) {
                case 1 :
                    str = 'TẠO VÉ';
                    break;
                case 2 :
                    str = 'CẬP NHẬT THÔNG TIN';
                    break;
                case 3 :
                    str = 'THÊM GHẾ';
                    break;
                case 4 :
                    str = 'HỦY GHẾ';
                    break;

                case 5 :
                    str = 'CHUYỂN GHẾ';
                    break;

                default :
                    str = 'TẠO VÉ';
            }
            return str;
        }

        function getNameUpDown(ticketInfo) {
            var addressdrop = '', addresspick = '';
            if (ticketInfo.getInPoint != undefined) {
                if (typeof ticketInfo.getInPoint != 'undefined' || typeof ticketInfo['dropAlongTheWayAddress'] != 'undefined' ||
                    typeof ticketInfo['dropOffAddress'] != 'undefined') {
                    if (typeof ticketInfo['dropAlongTheWayAddress'] != 'undefined' && ticketInfo['dropAlongTheWayAddress'] != '') {
                        addressdrop = ticketInfo['dropAlongTheWayAddress'];
                    } else {
                        if (typeof ticketInfo['dropOffAddress'] != 'undefined' && ticketInfo['dropOffAddress'] != '') {
                            addressdrop = ticketInfo['dropOffAddress'];
                        } else {
                            addressdrop = ticketInfo['getOffPoint']['address'];

                        }
                    }
                    if (addressdrop.split(', ').length >= 2) {
                        addressdrop = addressdrop.split(', ', 2).toString();
                    }
                    if (typeof ticketInfo['dropAlongTheWayAddress'] != 'undefined' && ticketInfo['dropAlongTheWayAddress'] != '')
                        addressdrop = '<strong>' + addressdrop + '</strong>';
                    if (typeof ticketInfo['alongTheWayAddress'] != 'undefined' && ticketInfo['alongTheWayAddress'] != '') {
                        addresspick = ticketInfo['alongTheWayAddress'];
                    } else {

                        if (typeof ticketInfo['pickUpAddress'] != 'undefined' && ticketInfo['pickUpAddress'] != '') {
                            addresspick = ticketInfo['pickUpAddress'];
                        }
                        else {
                            addresspick = ticketInfo['getInPoint']['address'];
                        }
                    }
                    if (addresspick.split(', ').length >= 2) {
                        addresspick = addresspick.split(', ', 2).toString();
                    }

                    if (typeof ticketInfo['alongTheWayAddress'] != 'undefined' && ticketInfo['alongTheWayAddress'] != '') {
                        addresspick = '<strong>' + addresspick + '</strong>';
                    }
                }
            }
            return {up: addresspick, down: addressdrop}
        }

        function checkStatusTicket(stt) {
            status = '';
            switch (stt) {
                case 0:
                    status = "<span style='color: #F44336'>Đã hủy</span>"; //do
                    break;
                case 1:
                    status = "<span style='color: #B71C1C'> Hết hạn giữ chỗ"; //do
                    break;
                case 2:
//                    if (item.overTime > Date.now() || item.overTime == 0)
                    status = "<span style='color: #FF9800'>Đang giữ chỗ</span>"; //cam
//                    else
//                        status = "<span style='color: #B71C1C'> Hết hạn giữ chỗ"; //do

                    break;
                case 3:
                    status = "<span class='code'>Đã thanh toán</span>"; //xanh
                    break;
                case 4:
                    status = "<span style='color: #0091EA'>Đã lên xe</span>"; //xanh

                    break;
                case 5:
                    status = "Đã hoàn thành";

                    break;
                case 6:
                    status = "<span style='color: #C62828'>Quá giờ giữ chỗ</span>"; //do

                    break;
                case 7:
                    status = "<span style='color: #33691E'>Ưu tiên giữ chỗ</span>";

                    break;
                default:
                    status = '';
            }
            return status;
        }

        function newGetListTrip() {
            var routeId = $(selectBoxRoute).val();//tuyến
            var date = $(datepickerBox.datepicker()).val();
            date = date.split("-");
            var newDate = date[1] + "/" + date[0] + "/" + date[2];
            date = new Date(newDate).getTime();//ngày
            var startPointId = $('#cbb_InPoint').val();
            var endPointId = $('#cbb_OffPoint').val();
            $('.routeId').val(routeId);
            $(selectBoxListTrip).html('<option value="">Đang tải dữ liệu...</option>');
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: urlDBD("web/find-schedule-like-for-admin-new"),
                headers: {
                    'DOBODY6969': '{{ session('userLogin')['token']['tokenKey'] }}',
                },
                contentType: "application/json; charset=utf-8",
//                async: false,
                data: JSON.stringify({
                    'routeId': routeId,
                    'page': 0,
                    'count': 100,
                    'timeZone': 7,
                    'companyId': '{{ session("companyId") }}',
                    'date': date,
                    'start': startPointId,
                    'end': endPointId,
                }),
                success: function (data) {
                    if (data.code === 200) {
                        data = data.results.result;
                        listTrip = data;
                        newHtmlSelectListTrip();
                    } else {
                        notyMessage("Không lấy được danh sách chuyến !Vui lòng tải lại trang", "error");
                    }
                },
                error: function () {
                    $(selectBoxListTrip).html('<option value="">Lỗi</option>');
                    notyMessage("Lỗi gửi yêu cầu lấy danh sách chuyến ! Vui lòng tải lại trang", "error");
                }
            })
        }

        function newHtmlOptionListTrip(lt) {
            html = '';
            $.each(lt, function (i, trip) {
                var totalEmptySeat = trip.totalEmptySeat;
                html += '<option data-time="' + getFormattedDate(trip.getInTime, 'time') + '" data-schedule="' + trip.scheduleId + '" value="' + trip.tripId + '">' +
                    trip.numberPlate + ' - ' +
                    getFormattedDate(trip.getInTime, 'time') + (trip.scheduleType == 1 ? ' (TC) ' : '') +
                    ' (Trống: ' + totalEmptySeat + ')' +
                    '</option>';
            });
            return html;
        }

        function newHtmlSelectListTrip() {
            makeListConnectionRealTimeAllTrip();//cập nhật realTime;
            html = '';

            if (listTrip.length > 0) {
                html += '<option value="">Toàn bộ các chuyến</option>';
                html += newHtmlOptionListTrip(listTrip);

                $(selectBoxListTrip).html(html);
                $('#cbbTripDestinationId').html(html);

                // var tripId = sessionStorage.getItem("tripId");
                // var scheduleId = sessionStorage.getItem("scheduleId");

                {{--var tripId = '{{request('tripId')}}';--}}
                        {{--var scheduleId = '{{request('scheduleId')}}';--}}
                    tripId = sessionStorage.getItem("tripId");
                scheduleId = sessionStorage.getItem("scheduleId");

                if (tripId !== null && tripId != -1) {
                    selectBoxListTrip.val(tripId);
                } else if (scheduleId !== null) {
                    selectBoxListTrip.find("[data-schedule='" + scheduleId + "']").attr("selected", "selected");
                }
            } else {
                notyMessage('Hiện tại tuyến này không bán vé ', 'error');
                $(selectBoxListTrip).html(' <option value="">Hiện tại không có chuyến</option>');
                $('#cbbTripDestinationId').html('<option value="">Hiện tại không có chuyến</option>');
            }
            newBuildHtmlTrip();
        }

        function newBuildHtmlTrip() {
            $('.tripInfo').hide();
            // bỏ seatmap
            $('#tang1 .khungxe,#ds_tang1 .khungxe').html("");
            $('#tang2 .khungxe,#ds_tang2 .khungxe').html("");
            $('#updateNoteTrip').hide();
            generateHtmlAllTrip();
            if ($(selectBoxListTrip).val() !== '') {
                $('.tripInfo').show();
                updateTripInfo(selectBoxListTrip.val(), selectBoxListTrip.find('option:selected').data('schedule'));
                updatePrice();
            } else {
                $('.box-route-and-trip').slideDown();
            }
            Loaing(false);
            // if ($(selectBoxListTrip).val() === 'tccc') {
            //     generateHtmlAllTrip();
            // } else {
            //     updateTripInfo(selectBoxListTrip.val(), selectBoxListTrip.find('option:selected').data('schedule'));
            //     updatePrice();
            // }
        }

        function changeListTrip() {
            $('#gheThemKhongThuocSeatMap').hide();//ẩn thêm ghế, hiển thị lại khi load trip html
            tripSelected.tripData={};
            newGetListTrip();
        }

        function generateHtmlTicketLog(arrayLogs) {
            var objLog = {
                fullName: "Tên khách hàng",
                agencyPrice: "Thực thu",
                unPaidMoney: "Chưa thanh toán",
                listSeatId: "Danh sách ghế",
                phoneNumber: "Số điện thoại",
                email: "Email",
                note: "Ghi chú",
                pickUpAddress: "Điểm đón",
                dropOffAddress: "Điểm trả",
                routeName: "Chuyến cũ",
                srcSeatIds: "Ghế cũ",
                desSeatIds: "Ghế mới",
                newTicketCode: "Mã vé mới",
                isChecked : "Kiểm Vé",
                paidMoney : 'Đã trả',
                paymentTicketPrice : 'Giá vé',
                ticketStatus : 'Trạng thái vé'
            };
            html = '';
            if (typeof arrayLogs === "object" && arrayLogs.length > 0) {
                $.each(arrayLogs, function (k, v) {
                    html += '-';
                    html += (objLog[v.field] || v.field) + " : ";
                    if (typeof v.value === "string" || typeof v.value === "number") {
                        html += v.value;
                    } else if (typeof v.value === "object") {
                        html += v.value.toString();
                    } else {
                        html += "";
                    }
                    html += '</br>'
                });
            }
            return html;
        }
        function getLastPhoneLog(listLog){
            var phoneNumber = '';
            $.each(listLog,function (k,v) {
                $.each(v.logs,function (key,log) {
                    if(log.field==="phoneNumber"){
                        phoneNumber = log.value;
                        return false;
                    }
                });
                if(phoneNumber!==''){
                    return false;
                }
            });
            return phoneNumber;
        }
        function settingModalThuChi(){
            if($('#rdPhieuThu').prop('checked')){//nếu là phiếu thu được check
                $('.lbSpender').text('Người Nộp');
                $('.lbTitleThuChi').text('Phiếu Thu');
            }else{
                $('.lbSpender').text('Người Đưa');
                $('.lbTitleThuChi').text('Phiếu Chi');
            }
        }
        function getListUser(){
            SendAjaxWithJson({
                url:urlDBD('user/getlist'),
                contentType : "application/json; charset=utf-8",
                data : {
                    page :0 ,
                    count : 1000,
                    listUserType : [2,3],
                },
                type : 'post',
                async :false,
                success : function(data){
                    var t =tachTXvaPX(data.results.result);
                },
                functionIfError : function (data) {
                    listDriver = [];
                    listAss = [];
                }
            })
        }
        function getListVehicle(){
            SendAjaxWithJson({
                url:urlDBD('vehicle/getlist'),
                data : {
                    page :0 ,
                    count : 1000,
                    vehicleStatus : [2],
                },
                type : 'post',
                async :false,
                success : function(data){
                    listVehicle=data.results.result;
                },
                functionIfError : function (data) {
                    listVehicle=[];
                }
            })
        }
        function tachTXvaPX(listUser) {
            dataReturn={driver : [],assistant : []};
            $.each(listUser,function (k,user) {
                if(user.userType ==2){
                    dataReturn.driver.push(user);
                }
                if(user.userType ==3){
                    dataReturn.assistant.push(user);
                }
            });

            listDriver = dataReturn.driver;
            listAss = dataReturn.assistant;
            console.log(listDriver,listAss);
            return dataReturn
        }

        function MakeUrl(route,date,trip){
            return (window.location.origin+'?routeId='+route+'&date='+date+'&tripId='+trip);
        }

    </script>
    @stack('javascrip-sell-ticket')
    <!-- End Modal Change SeatMap -->
    @include('cpanel.Ticket.sell.module.schedule-plus')
    @include('cpanel.Ticket.sell.sell-ver1.telecom.telecomView')

@endsection

