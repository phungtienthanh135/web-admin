<style>
    .form-horizontal .control-group {
        margin-bottom: 5px;
    }
    .mdbd{max-height: 480px;overflow-y:auto;}

    /*    .select2-selection__arrow b {
            margin-left: 165px !important;
        }*/

</style>
<div class="modal modal-custom hide fade" data-backdrop="static" id="sell_ticket"
     style="width: 65%;margin-left: -30%;top: 2%;">
    <div class="modal-header center">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>THÔNG TIN ĐẶT VÉ</h3>
    </div>
    <form id="datve" action="{{action('TicketController@postSell')}}" method="post">
        <div class="mdbd modal-body">
            <div class="text-right banve_thongtintinve" style="width:100%"><a href="#" id="ticket-history">Lịch sử
                    vé</a></div>
            <div class="pull-right banve_lichsuve" style="display: none"><a href="#" id="sell-ticket">Bán vé</a></div>

            <div class="row-fluid banve_thongtintinve">
                <div class="span6">
                    <h3>THÔNG TIN VÉ</h3>
                    <div class="form-horizontal">
                        <div class="control-group" style="text-align:center">
                            <div class="routeName"></div>
                        </div>
                        <div class="upDownTicket">
                            <div class="control-group">
                                <label for="pickUpAddress" class="control-label">Đón</label>
                                <div class="controls">
                                    <div class="span4">
                                        <label for="rdPickUpAddress">
                                            <input type="radio" checked name="goUp" value="pickup" id="rdPickUpAddress">
                                            <span> Tận nơi</span>
                                        </label>
                                    </div>
                                    <div class="span4">
                                        <label for="rdAlongTheWayAddress">
                                            <input type="radio" name="goUp" value="alongTheWay" id="rdAlongTheWayAddress">
                                            <span> D/đường </span>
                                        </label>
                                    </div>
                                    <div class="span4 divRdTransshipmentInPoint">
                                        <label for="rdTransshipmentUp">
                                            <input type="radio" name="goUp" value="transshipment" id="rdTransshipmentUp">
                                            <span> T/chuyển </span>
                                        </label>
                                    </div>
                                    <div class="inputUp" style="display: inline-block;width: 100%;">
                                        <div style="position: relative">
                                            <input id="pickUpAddress" type="text" placeholder="Nhập địa chỉ đón" name="pickUpAddress" class="m_bottom_0 span12"/>
                                            <div style="position: absolute;width: 100%;z-index: 1;display: none;" class="autocompletePickup">
                                            </div>
                                        </div>
                                        <select name="" style="display:none;" class="transshipmentInPoint span12"></select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="upDownTicket">
                            <div class="control-group">
                                <label for="" class="control-label">Trả</label>
                                <div class="controls">
                                    <div class="span4">
                                        <label for="rdDropOffAddress">
                                            <input type="radio" checked name="goDown" value="dropOff" id="rdDropOffAddress">
                                            <span> Tận nơi</span>
                                        </label>
                                    </div>
                                    <div class="span4">
                                        <label for="rdDropAlongTheWayAddress">
                                            <input type="radio" name="goDown" value="alongTheWay" id="rdDropAlongTheWayAddress">
                                            <span> D/đường </span>
                                        </label>
                                    </div>
                                    <div class="span4 divRdTransshipmentOffPoint">
                                        <label for="rdTransshipmentDown">
                                            <input type="radio" name="goDown" value="transshipment" id="rdTransshipmentDown">
                                            <span> T/chuyển </span>
                                        </label>
                                    </div>
                                    <div class="inputUp" style="display: inline-block;width: 100%;">
                                        <div style="position: relative">
                                            <input id="dropOffAddress" type="text" placeholder="Nhập địa chỉ đón" name="dropOffAddress" class="m_bottom_0 span12"/>
                                            <div style="position: absolute;width: 100%;z-index: 1;display: none;" class="autocompleteDropOff">
                                            </div>
                                        </div>
                                        <select name="" style="display:none;" class="transshipmentOffPoint span12"></select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="control-group"><label for="lb_numberOfAdults" class="control-label">Ghế người
                                lớn</label>
                            <div class="controls">
                                <input readonly type="text" name="" id="lb_numberOfAdults" class="span12 m_bottom_0">
                                <span style="display: none" id="listSeat"></span>
                            </div>
                        </div>
                        <div class="control-group"><label id="textChildren" for="numberOfChildren" class="control-label">Ghế trẻ
                                em</label>
                            <div class="controls">
                                <div class="input-append m_bottom_0 row-fulid">
                                    <button type="button" class="btn" onclick="minus()"><i class="icon-minus "></i>
                                    </button>
                                    <input id="numberOfChildren" type="text" name="numberOfChildren"
                                           readonly="" value="0" class="color_grey bg_light span3"/>
                                    <button type="button" class="btn" onclick="plus()"><i class="icon-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="control-group"><label for="ticketPrice" class="control-label">Giá vé</label>
                            <div class="controls">
                                <div class="input-tips m_bottom_0 span12">
                                    <input style="border-right: none" readonly type="text" value="0Đ"
                                           id="listTicketPrice"
                                           class="span12 m_bottom_0">
                                    <label for="ticketPrice">(VNĐ)</label>
                                </div>
                            </div>
                        </div>
                        <div id="isMeal" style="display:none;" class="control-group"><label for="mealPrice"
                                                                                            class="control-label">Bao
                                gồm ăn</label>
                            <div class="controls">
                                <div class="span2">
                                    <input id="cb_isMeal" type="checkbox" class="sl_baogoman span12" name="isMeal">
                                    <label style="margin-top: 5px" for="cb_isMeal"></label>
                                </div>
                                <div class="input-tips m_bottom_0 span10">
                                    <input readonly style="border-right: none" type="text" id="mealPrice"
                                           class="span12">
                                    <label for="mealPrice">(VNĐ / Ghế)</label>
                                </div>
                            </div>
                        </div>
                        <div id="isInsurrance" style="" class="control-group">
                            <label for="priceInsurrance" class="control-label">Bao gồm bảo hiểm</label>
                            <div class="controls">
                                <div class="span2">
                                    <input id="cb_priceInsurrance" type="checkbox" class="sl_isInsurrance span12"
                                           name="isInsurrance">
                                    <label style="margin-top: 5px" class="f_left" for="cb_priceInsurrance"></label>
                                </div>

                                <div class="input-tips m_bottom_0 span10">
                                    <input readonly style="border-right: none" type="text" id="priceInsurrance"
                                           class="span12">
                                    <label for="priceInsurrance">(VNĐ / Ghế)</label>
                                </div>
                            </div>
                        </div>
                        <div class="control-group"><label for="" class="control-label">Tổng tiền</label>
                            <div class="controls">
                                <input class="m_bottom_0 span12" id="totalPriceShow" disabled>
                                <input type="hidden" id="totalPrice">
                            </div>
                        </div>
                        <div id="isPickUpHome" style="display:none;" class="control-group"><label for="pricePickUpHome"
                                                                                                  class="control-label">Bao
                                gồm đưa đón</label>
                            <div class="controls">
                                <div class="span2">
                                    <input type="checkbox" class="span12" id="cb_isPickUpHome" name="isPickUpHome">
                                    <label style="margin-top: 5px" for="cb_isPickUpHome"></label>
                                </div>
                                <div class="input-tips m_bottom_0 span10">
                                    <input readonly style="border-right: none" type="text" id="pricePickUpHome"
                                           class="span12">
                                    <label for="pricePickUpHome">(VNĐ)</label>
                                </div>
                            </div>
                        </div>

                        <div class="control-group"><label for="" class="control-label">Mã khuyến mại</label>
                            <div class="controls">
                                <input autocomplete="off" class="span8 txtPromotionCode" type="text"
                                       placeholder="Mã khuyến mại" style="width: 50%;"/>
                                <button type="button" class="btn btnCheckPromotionCode">Kiểm tra</button>
                            </div>
                        </div>
                        {{--@if(count(session('userLogin')["userInfo"]['listAgency'])>0)--}}
                        <div class="control-group nhaxethuho">
                            <label for="control-label">&nbsp;</label>
                            <div class="controls">
                                <input type="checkbox" name="collectMoneyForAgency" id="collectMoneyForAgency"/>
                                <label class="f_left" for="collectMoneyForAgency">Nhà xe thu tiền hộ</label>
                            </div>
                        </div>
                        {{--@endif--}}
                        <div id="divroundtrip" style="display: none">
                            <div id="checkRoundTrip" class="control-group"><label for="txtRoundTrip"
                                                                                  class="control-label">Khứ hồi</label>
                                <div class="controls">
                                    <div class="span2">
                                        <input id="txtRoundTrip" type="checkbox" class="roundTrip span12"
                                               name="roundTrip">
                                        <label style="margin-top: 5px" class="f_left" for="txtRoundTrip"></label>
                                    </div>
                                </div>
                            </div>
                            <div>

                            </div>
                        </div>
                        <div hidden class="Info_Round">
                            <div style="padding-top: 40px" class="control-group ">
                                <div>
                                    <h4 for="" class="control-label">Vé khứ hồi</h4>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Chuyến:</label>
                                <div class="controls">
                                    <input type="text" readonly class="m_bottom_0  span10" id="tripDetail" value="">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Thời gian đi:</label>
                                <div class="controls">
                                    <input type="text" readonly class="m_bottom_0  span10" id="createDateRound"
                                           value="">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Ghế đã đặt:</label>
                                <div class="controls">
                                    <input type="text" readonly class="m_bottom_0  span10" id="ticketDetal" value="">
                                </div>

                            </div>
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    <button type="button" id="MoveRoundTrip" class="btn btn-inprimary">Chuyển tới
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <h3>THÔNG TIN KHÁCH HÀNG</h3>
                    <div class="form-horizontal">
                        <div class="control-group"><label for="" class="control-label">SĐT</label>
                            <div class="controls">
                                <input {{--autocomplete="off"--}}
                                       id="phoneNumber"
                                       data-validation="custom"
                                       class="m_bottom_0 CustomerPhoneNumber span12 isNumber" type="text"
                                       value="{{old('phoneNumber')}}"
                                       name="phoneNumber"/>
                            </div>
                        </div>
                        <div class="control-group"><label for="" class="control-label">Họ tên</label>
                            <div class="controls">
                                <input autocomplete="off" class="m_bottom_0 CustomerFullName span12" type="text"
                                       value="{{old('fullName')}}" name="fullName"/>
                            </div>
                        </div>
                        <div class="control-group"><label for="" class="control-label">Email</label>
                            <div class="controls">
                                <input autocomplete="off"
                                       type="email" data-validation-error-msg-email="Vui lòng nhập đúng định dạng"
                                       class="m_bottom_0 span12 CustomerEmail" value="{{old('email')}}" name="email"/>
                            </div>
                        </div>
                        <div class="control-group"><label for="txtNote" class="control-label">Ghi chú</label>
                            <div class="controls">
                                <textarea name="note" id="txtNote" rows="3"
                                          class="m_bottom_0 span12">{{old('note')}}</textarea>
                            </div>
                        </div>
                        <div class="control-group"><label for="cbbCreatedUser"
                                                          class="control-label">@if(hasAnyRole(GET_LIST_AGENCY))Đại
                                lý@endif</label>
                            <div class="controls">
                                @if(hasAnyRole(GET_LIST_AGENCY))
                                    <div class="span12">
                                        <select name="agencyUserId" id="cbbCreatedUser"
                                                class="span12 m_bottom_0 agency">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="txtforeignKey" class="control-label">Mã code</label>
                            <div class="controls">
                                <div class="input-tips m_bottom_0 span12">
                                    <input type="text" name="foreignKey" id="txtforeignKey"
                                           class="span12 m_bottom_0 price">
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="" class="control-label">Gửi SMS</label>
                            <div class="controls">
                                <div class="span1" style="padding-top: 5px;">
                                    <input type="checkbox" name="sendSMS" id="send_code_to_phone"/>
                                    <label class="f_left" for="send_code_to_phone"></label>
                                </div>
                                <span class="span2" style="padding-top: 5px;text-align: right;">In Vé</span>
                                <div class="span1" style="padding-top: 5px;">
                                    <input type="checkbox" id="print_after_save" name="print"/>
                                    <label class="f_left" for="print_after_save"></label>
                                </div>
                                <span class="span3" style="padding-top: 5px;text-align: right;">Giữ chỗ</span>
                                <div class="input-tips m_bottom_0 span5">
                                    <input style="border-right: none" type="number" name="timeBookHolder"
                                           id="txtTimeBookHolder" value="0" class="span12" min="0"/>
                                    <label for="txtTimeBookHolder">Phút</label>
                                </div>
                            </div>
                        </div>
                        <div class="control-group"><label for="realPriceShow" class="control-label">Thực thu</label>
                            <div class="controls">
                                <div class="input-tips m_bottom_0 span5">
                                    <input name="realPriceShow" type="text"
                                           class="m_bottom_0 span12"
                                           id="realPriceShow" {{ session('userLogin')['userInfo']['userType']!=7&&(session('userLogin')['isEditTicketPrice']==0 || count(session("userLogin")["userInfo"]['listAgency'])>0) ?"disabled":"" }} maxlength="15">
                                    {{--<label>(VNĐ)</label>--}}
                                    {{--for="totalPrice"--}}
                                </div>
                                <div class="span1"></div>
                                <div class="input-tips m_bottom_0 span6">
                                    <input type="number" class="span12 moneyTooltip" id="txtThuThem" value="0" title="0 Đ">
                                    <label for="txtThuThem">Thu thêm</label>
                                </div>
                            </div>
                        </div>
                        <div class="control-group"><label for="dathu" class="control-label">Đã thu</label>
                            <div class="controls">
                                <div class="input-tips m_bottom_0 span5">
                                    <input style="border-right: none" type="number" min="0" name="paidMoney"
                                           id="txtPaidMoney"
                                           class="span12 m_bottom_0">
                                    <label for="txtPaidMoney">(VNĐ)</label>
                                </div>
                                <div class="span1"></div>
                                <select name="paymentType" class="cbb_paymentType m_bottom_0 span6 pull-right" id="sellPaymentType">
                                    {{--<option value="1">Thanh toán trực tuyến</option>--}}
                                    <option selected value="3">Thanh toán tiền mặt tại quầy</option>
                                    {{--<option value="5">Thanh toán bằng tiền mặt tại Payoo</option>--}}
                                    <option value="6">Chuyển khoản</option>
                                    @if(session('companyId')=='TC1OHntfnujP'&&count(session("userLogin")["userInfo"]['listAgency'])>0)
                                        {{--đại lí inter thì hiện--}}
                                        <option value="6" data-type="thenoidia">Thẻ ATM nội địa</option>
                                        <option value="6" data-type="thequocte">Thẻ Visa/Master. </option>
                                        <option value="6" data-type="vnp">Thanh toàn QRCode </option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="control-group"><label for="" class="control-label">Phải thu</label>
                            <div class="controls">
                                <div class="input-tips m_bottom_0 span12">
                                    <input style="border-right: none" readonly type="text" value="0"
                                           id="txtMoneyOwing"
                                           class="span12 m_bottom_0">
                                    <label for="txtMoneyOwing">(VNĐ)</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="round" id="round-trip" style="display: none;">
                    <div class="clearfix"></div>
                    <h3 style=""><strong>THÔNG TIN KHỨ HỒI</strong></h3>
                    <div class="span6" style="margin-left: 0px;">
                        <div class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label" for="cbbRoundRouteId">Tuyến khứ hồi</label>
                                <div class="controls">
                                    <select class="span12" id="cbbRoundRouteId">

                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="txtCalendarRoundTrip">Thời gian khứ hồi</label>
                                <div class="controls">
                                    <div class="input-append span12">
                                        <input value="{{date('d-m-Y')}}" readonly class="span10" autocomplete="off"
                                               type="text"
                                               id="txtCalendarRoundTrip">
                                    </div>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="cbbRoundTripId">Chuyến khứ hồi</label>
                                <div class="controls">
                                    <select name="roundTripId" id="cbbRoundTripId" class="span12">
                                        <option value="">Chuyến</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group" id="listSeatRoundTrip">

                            </div>
                            <div style="display: none;">
                                <input type="hidden" name="roundTripScheduleId" class="txtRoundTripScheduleId">
                                <input type="hidden" name="roundTripTripId" class="txtRoundTripTripId">
                            </div>
                            <div id="loading_round" hidden style="width: 100%">
                                <img style="padding-left: 50%" src="/public/images/loading/loading32px.gif" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="form-horizontal">
                            <div class="control-group"><label for="totalPrice" class="control-label">Thực thu khứ
                                    hồi</label>
                                <div class="controls">
                                    <div class="input-tips m_bottom_0 span5">
                                        <input name="realPriceRound"
                                               class="m_bottom_0 span12"
                                               id="txtRealPriceRound" {{ session('userLogin')['userInfo']['userType']!=7&&(session('userLogin')['isEditTicketPrice']==0 || count(session("userLogin")["userInfo"]['listAgency'])>0) ?"disabled":"" }}>
                                        {{--<label>(VNĐ)</label>--}}
                                        {{--for="totalPrice"--}}
                                    </div>
                                    <div class="span1"></div>
                                    <select name="paymentType" class="cbb_paymentType m_bottom_0 span6">
                                        <option value="1">Thanh toán trực tuyến</option>
                                        <option selected value="3">Thanh toán tiền mặt tại quầy</option>
                                        <option value="5">Thanh toán bằng tiền mặt tại Payoo</option>
                                        <option value="6">Chuyển khoản</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group"><label for="dathu" class="control-label">Đã thu</label>
                                <div class="controls">
                                    <div class="input-tips m_bottom_0 span12">
                                        <input style="border-right: none" type="number" min="0" value=""
                                               id="txtPaidMoneyRound"
                                               class="span12 m_bottom_0" value="0">
                                        <label for="txtPaidMoneyRound">(VNĐ)</label>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group"><label for="" class="control-label">Phải thu</label>
                                <div class="controls">
                                    <div class="input-tips m_bottom_0 span12">
                                        <input style="border-right: none" readonly type="text" value="0"
                                               name="txtMoneyOwingRound"
                                               id="txtMoneyOwingRound"
                                               class="span12 m_bottom_0">
                                        <label for="txtMoneyOwingRound">(VNĐ)</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid banve_lichsuve" style="display: none">
                <h3 class="text-center">Lịch sử vé</h3>
                <table class="row-fluid table table-bordered">
                    <thead>
                    <tr>
                        <th>Thời điểm sửa</th>
                        <th>Người sửa</th>
                        <th>Nội dung sửa</th>
                    </tr>
                    </thead>
                    <tbody id="list-history">
                    </tbody>
                </table>
            </div>
            <div style="display: none">
                <input type="hidden" id="numberOfAdults" name="numberOfAdults" value="{{old('numberOfAdults')}}">
                <input type="hidden" name="listExtraPrice" value="{{old('listExtraPrice')}}" id="txtListExtraPrice">
                <input type="hidden" name="listExtraTK" value="{{old('listExtraTK')}}" id="txtListExtraTK">
                <input type="hidden" id="listSeatId" name="listSeat" value="{{old('listSeat')}}">
                <input type="hidden" name="originalPrice" id="originalPriceValue" value="{{old('totalPrice')}}">
                <input type="hidden" name="paymentPrice" id="txtPaymentPrice">
                <input type="hidden" name="realPrice" id="realPrice" value="{{old('totalPrice')}}">
                <input type="hidden" name="getOffTimePlan" class="getOffTimePlan" value="{{old('getOffTimePlan')}}">
                <input type="hidden" name="getInTimePlan" class="getInTimePlan" value="{{old('getInTimePlan')}}">
                <input type="hidden" name="commissionType" id="txtCommissionType">
                <input type="hidden" name="commissionValue" id="txtCommissionValue">
                <input type="hidden" name="additionPriceType" id="txtAdditionPriceType">
                <input type="hidden" name="additionPriceAmount" id="txtAdditionPriceAmount">
                <input type="hidden" name="additionPriceMode" id="txtAdditionPriceMode">
                <input type="hidden" name="oneSeatPrice" id="txtOneSeatPrice">
                <input type="hidden" id="txtGetInPointId" name="getInPointId">
                <input type="hidden" id="txtGetOffPointId" name="getOffPointId">
                <input type="hidden" name="tripId" class="tripId" value="{{old('tripId')}}">
                <input type="hidden" name="scheduleId" class="scheduleId" value="{{old('scheduleId')}}">
                <input type="hidden" name="startDate" class="startDate" value="{{old('startDate')}}">
                <input type="hidden" name="routeId" class="routeId" value="{{old('routeId')}}">
                <input type="hidden" class="promotionId" name="promotionId"/>
                <input type="hidden" class="promotionCode" name="promotionCode"/>
                <input type="hidden" class="promotionPercent" name="promotionPercent" value="-1">
                <input type="hidden" class="promotionPrice" name="promotionPrice" value="-1">
                <input type="hidden" id="txtCancelReason" name="cancelReason"/>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div id="getDropOffAddress">
                    <input type="hidden" id="dropOffLat" name="dropOffLat">
                    <input type="hidden" id="dropOffLong" name="dropOffLong">
                </div>
                <div id="getPickUpAddress">
                    <input type="hidden" id="pickUpLat" name="pickUpLat">
                    <input type="hidden" id="pickUpLong" name="pickUpLong">
                </div>
                <div class="tsmp">
                    <input type="hidden" id="transshipmentInPointPrice" name="transshipmentInPointPrice" value="0">
                    <input type="hidden" id="transshipmentOffPointPrice" name="transshipmentOffPointPrice" value="0">
                </div>
                <div id="updateTicket">
                    <input type="hidden" name="ticketId" value="" id="txtTicketId">
                </div>


                {{--request cho round trip--}}

                <input type="hidden" id="paymentCode" name="paymentCode">
                <input type="hidden" id="trip_Id" name="trip_Id"/>
                <input type="hidden" id="adultsRound" name="adultsRound"/>
                <input type="hidden" id="MoneyOwingRound" name="MoneyOwingRound"/>
                <input type="hidden" id="priceInsurranceRound" name="priceInsurranceRound"/>
                <input type="hidden" id="mealPriceRound" name="mealPriceRound"/>
                <input type="hidden" id="total_Price_Back" name="total_Price_Back"/>
                <input type="hidden" id="list_Seat" name="list_Seat"/>
                <input type="hidden" name="getOffTimePlanRound" id="getOffTimePlanRound"
                       value="{{old('getOffTimePlanRound')}}">
                <input type="hidden" name="getInTimePlanRound" id="getInTimePlanRound"
                       value="{{old('getInTimePlanRound')}}">
                <input type="hidden" id="getInPointIdRound" name="getInPointIdRound">
                <input type="hidden" id="getOffPointIdRound" name="getOffPointIdRound">
                <input type="hidden" name="scheduleIdRound" id="scheduleIdRound" value="">
                <input type="hidden" name="startDateRound" id="startDateRound" value="">
                <input type="hidden" name="routeIdRound" id="routeIdRound" value="}">
                <input type="hidden" id="hasPromotionRound" name="hasPromotionRound"/>
                <input type="hidden" id="promotionCodeRound" name="promotionCodeRound"/>
                <input type="hidden" id="salePrice" name="salePrice"/>
                <input type="hidden" id="dropOffAddressRound" name="dropOffAddressRound"/>
                <input type="hidden" id="pickUpAddressRound" name="pickUpAddressRound"/>
                <input type="hidden" id="paidMoneyRound" name="paidMoneyRound" value="0"/>
                <div id="getDropOffAddressRound">
                    <input type="hidden" id="dropOffLatRound" name="dropOffLatRound">
                    <input type="hidden" id="dropOffLongRound" name="dropOffLongRound">
                </div>
                <div id="getPickUpAddressRound">
                    <input type="hidden" id="pickUpLatRound" name="pickUpLatRound">
                    <input type="hidden" id="pickUpLongRound" name="pickUpLongRound">
                </div>

            </div>

            <div class="row-fluid bg_light p_bottom_20 center" id="Payoo" style="display: none">
                <h4>Tìm điểm thanh toán Payoo</h4>
                <div class="span12">
                    <iframe height="720px" width="90%" src="" style="border: none">
                    </iframe>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="position: relative">
            <div class="disableAction" style="width: 100%;height: 100%;z-index: 5;background: #555;opacity: 0.7;position:absolute;display: none"></div>
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="row-fluid">
                <div class="span8 offset2 isSellTicket text-center">
                    <div class="isAddSeat">
                        <button class="btn btn-warning span4 actionSellTicket" type="submit" id="updateAddSeat"
                                name="submit"
                                value="ADDSEAT">CẬP NHẬT
                        </button>
                    </div>
                    <div class="isntAddSeat">
                        @if(count(session("userLogin")["userInfo"]['listAgency'])==0||session('companyId')=='TC05wM7dDRQSo6'){{--hùng đức--}}
                            <button class="btn btn-inprimary span4 actionSellTicket" type="submit" id="thanhtoan1"
                                    name="submit"
                                    value="THANHTOAN">THANH TOÁN
                            </button>
                        @elseif(session('companyId')!='TC05wM7dDRQSo6')
                            {{--ẩn nếu là inter--}}
                            <button class="btn btn-inprimary span4 actionSellTicket" type="submit"
                                    name="submit"
                                    value="GIUCHO" data-sell-and-pay="true">THANH TOÁN
                            </button>
                        @endif
                        <button class="btn btn-warning span4  actionSellTicket" type="submit" id="giucho" name="submit"
                                value="GIUCHO">GIỮ CHỖ
                        </button>
                    </div>

                    <button class="btn btn-primary span4" type="button" data-dismiss="modal" aria-hidden="true">ĐÓNG
                    </button>
                </div>
                <div class="span12 offset1 isUpdateStatusTicket">
                    <button class="btn btn-inprimary span2" name="option" value="4" type="submit"
                            id="themghe" data-seat-click="">THÊM GHẾ
                    </button>
                    <button class="btn btn-inprimary span2 actionUpdateTicket" name="option" value="3" type="submit"
                            id="capnhat">CẬP
                        NHẬT
                    </button>
                    {{--@if(count(session("userLogin")["userInfo"]['listAgency'])==0)--}}
                        <button class="btn btn-inprimary span2 actionUpdateTicket" name="option" value="1" type="submit"
                                id="thanhtoan" {{ count(session("userLogin")["userInfo"]['listAgency'])>0 ? 'data-sell-and-pay=true':'' }}>THANH TOÁN
                        </button>
                    {{--@endif--}}
                    <button class="btn btn-danger span2 actionUpdateTicket" name="option" value="2" type="submit"
                            id="huyve">HUỶ VÉ
                    </button>
                    <button class="btn btn-primary span2" type="button" data-dismiss="modal" aria-hidden="true">ĐÓNG
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
<style>
    .pac-container {
        z-index: 99999999 !important;
        position: absolute !important;
    }

    .select2-dropdown {
        width: 273px !important;
    }

    .dropdown-menu {
        overflow-y: scroll;
        max-height: 300px;
    }

    .btnchooseSeatMove {
        margin-left: 20px;
    }

    .x {
        font-size: 19px;
        font-weight: bold;
        line-height: 13px;
        margin-left: 2px;
    }

    .cancelSeatRound {
        float: none;
        margin-top: -15px;
        margin-left: -1px;
        background-color: red;
        border: 1px solid #F7464B;
        opacity: 2;
        -webkit-appearance: none;
        border-radius: 7px;
        line-height: 12px;
    }

</style>
<script>


    function showHideRoundTrip(element) {
        return false;
        if (Seat_Back_Array.length == 0) {
            for (var a = 0; a < listSeat.split(',').length; a++) {
                Seat_Back_Array.push("");
                listPrice[a] = 0;
            }
        }
        totalPriceBack = 0;
        total_Price = 0;
        if ($('#datve').attr('action') == urlUpdateStatusTicket) {
            $('#divroundtrip').hide();
        } else {

            // sẽ sử dụng nếu chỉ cho chọn đúng chuyến khứ hồi phù hợp

            // var html='<select  class="span12" id="cbbRouteDestinationId">'+
            //
            //     ' <option data-startPoint="'+ round_route['listPoint'][0]['pointId']+
            //     '"data-endPoint="'+round_route['listPoint']['pointId']+
            //     '"value="'+round_route['routeId']+'">'+round_route['routeName']+'</option></select>';
            // $('#cbbRoundRouteId').html(html);
            $('#divroundtrip').show();
            if ($(element).attr('checked')) {
                $('#cbbRoundRouteId').html($('#cbbRouteDestinationId').html());
                $('#round-trip').show();
            } else {
                $('#round-trip').hide();
            }
        }

    }

    function getTime() {

        return new Date($('#txtCalendar').val());

    }

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    $(document).ready(function () {

//        $('.moneyTooltip').tooltip();
//        $('body').on('keyup','.moneyTooltip',function () {
//            $(this).attr('data-original-title',moneyFormat($(this).val())).focus();
//        })
        /*$('.dropdown-menu').hover(function () {
            alert('vbkjn') ;
        });*/
        var txtCalendarRoundTrip = $('#txtCalendarRoundTrip');
        var selectRoundRouteId = $('#cbbRoundRouteId');
        var selectRoundTripId = $('#cbbRoundTripId');

        $('body').on('change', '.transshipmentInPoint,.transshipmentOffPoint', function () {
//            len
            var tsmip = $('.transshipmentInPoint').val();
            tsmipInfo = $.grep(tripSelected.tripData.transshipmentInPoint || [], function (e, i) {
                return e.pointId == tsmip;
            })[0] || {};
//            cập nhật thông tin InPoint và tiền
            if($("[name='goUp']:checked").val()=='transshipment'){
                $('#pickUpAddress').val(tsmipInfo.pointName || '');
                $('#pickUpLat').val(tsmipInfo.latitude || tripSelected.tripData.latitudePointStart);
                $('#pickUpLong').val(tsmipInfo.longitude || tripSelected.tripData.longitudePointStart);
                $('#transshipmentInPointPrice').val(tsmipInfo.transshipmentPrice || 0);
            }else{
                $('#transshipmentInPointPrice').val(0);
            }


//            xuong
            var tsmop = $('.transshipmentOffPoint').val();
            tsmopInfo = $.grep(tripSelected.tripData.transshipmentOffPoint || [], function (e, i) {
                return e.pointId == tsmop;
            })[0] || {};
            if($("[name='goDown']:checked").val()=='transshipment') {
                $('#dropOffAddress').val(tsmopInfo.pointName || '');
                $('#dropOffLat').val(tsmopInfo.latitude || tripSelected.tripData.latitudePointEnd);
                $('#dropOffLong').val(tsmopInfo.longitude || tripSelected.tripData.longitudePointEnd);
                $('#transshipmentOffPointPrice').val(tsmopInfo.transshipmentPrice || 0);
            }else{
                $('#transshipmentOffPointPrice').val(0);
            }
            //            khú hồi

            $('#pickUpAddressRound').val(tsmipInfo.pointName || '');
            $('#pickUpLatRound').val(tsmipInfo.latitude || tripSelected.tripData.latitudePointStart);
            $('#pickUpLongRound').val(tsmipInfo.longitude || tripSelected.tripData.longitudePointStart);

            $('#dropOffAddressRound').val(tsmopInfo.pointName || '');
            $('#dropOffLatRound').val(tsmopInfo.latitude || tripSelected.tripData.latitudePointEnd);
            $('#dropOffLongRound').val(tsmopInfo.longitude || tripSelected.tripData.longitudePointEnd);

            updateInputPrice(true);
            if(totalPriceBack>0);
            updateInputPriceRound();
        });

        $('body').on('click', '.actionMoveRoundTrip', function () {
            $('#MoveRoundTrip').submit();
        });
        $('body').on('click', '#addSeat', function () {
            var id = [];
            id = document.getElementsByClassName('btnchooseSeatMove');
            if (id.length > 0) {
                var listSave = [];
                var button_add = document.getElementById('addSeat');
                var ghe = $(id[id.length - 1]).attr('id');
                ghe = (ghe).substring(3, ghe.length);
                ghe++;
                for (var i = 0; i < id.length; i++)
                    listSave[i] = id[i].parentElement.parentElement.innerHTML;
                listSave[i] = button_add.parentElement.parentElement.innerHTML;
                for (var i = id.length - 1; i >= 0; i--) {
                    id[i].parentElement.parentElement.remove();
                }
                button_add.parentElement.parentElement.remove();

                var html = generateListSeatRound(data_generate_listseat, ghe);
                for (var i = 0; i < listSave.length; i++) {
                    $("#listSeatRoundTrip").append('<div class="chooseSeat">' + listSave[i] + '</div>');
                    if (i == (listSave.length - 2)) $("#listSeatRoundTrip").append(html);
                }
                Seat_Back_Array[Seat_Back_Array.length] = '';
                listPrice[listPrice.length] = 0;
            } else {
                var html = generateListSeatRound(data_generate_listseat, 1);
                var button_add = document.getElementById('addSeat');
                var but_add = button_add.parentElement.parentElement.innerHTML;
                button_add.parentElement.parentElement.remove();
                $("#listSeatRoundTrip").append(html);
                $("#listSeatRoundTrip").append('<div class="chooseSeat">' + but_add + '</div>');
                Seat_Back_Array[Seat_Back_Array.length] = '';
                listPrice[listPrice.length] = 0;
            }
        });
//        $('body').on('change','#numberOfAdults',function(){
//           updateInputPrice();
//        });
        $('body').on('click', '.cancelSeatRound', function () {
            var id = $(this).data('id');
            id = id.substring(3, id.length);
            if (id) {
                Seat_Back_Array[id] = '';
                totalPriceBack -= listPrice[id];
                total_Price -= listPrice[id];
                listPrice[id] = 0;
// console.log('totalPriceBack',totalPriceBack);
// console.log('list price',listPrice);
//console.log('seat array',Seat_Back_Array);
                if ($('#cb_isMeal').is(':checked')) {
                    $('#txtRealPriceRound').val(totalPriceBack + tripSelected.tripData.mealPrice);
                    $('#total_Price_Back').val(totalPriceBack + tripSelected.tripData.mealPrice);
                    $('#list_Seat').val((Seat_Back_Array.filter(function (e) {
                        return e
                    })).toString());
                    var MoneyOwing = totalPriceBack - ($('#txtPaidMoneyRound').val()) + tripSelected.tripData.mealPrice;
                    $('#txtMoneyOwingRound').val(MoneyOwing);
                    $('#txtPaidMoneyRound').attr('max', totalPriceBack + tripSelected.tripData.mealPrice);
                    $('#MoneyOwingRound').val(MoneyOwing);
                } else {
                    $('#txtRealPriceRound').val(totalPriceBack);
                    $('#total_Price_Back').val(totalPriceBack);
                    $('#list_Seat').val((Seat_Back_Array.filter(function (e) {
                        return e
                    })).toString());
                    var MoneyOwing = totalPriceBack - ($('#txtPaidMoneyRound').val());
                    $('#txtMoneyOwingRound').val(MoneyOwing);
                    $('#txtPaidMoneyRound').attr('max', totalPriceBack);
                    $('#MoneyOwingRound').val(MoneyOwing);
                }
            }
            this.parentElement.parentElement.remove();
            updateInputPriceRound();
        });

        $('body').on('focus', '#realPriceShow', function () {
            $('#realPriceShow').val($('#realPrice').val());
        });

        $('body').on('focusout', '#txtThuThem', function () {
            updateInputPrice(true);
        });

        $('body').on('focusout', '#realPriceShow,#txtRealPriceRound,#txtPaidMoney', function () {
            var pr = stringToInt($('#realPriceShow').val());
            $('#realPrice').val(pr);
            $('#realPriceShow').val(moneyFormat(pr));
            // updateInputPrice(true);
           $('#txtMoneyOwing').val(moneyFormat(parseInt($('#realPrice').val()) - $('input#txtPaidMoney').val()));
           $('#txtMoneyOwingRound').val((parseInt($('#txtRealPriceRound').val()) - parseInt($('input#txtPaidMoneyRound').val())));
        });
        $('body').on('focusout', '#txtRealPriceRound,#txtPaidMoneyRound', function () {
            $('#txtMoneyOwingRound').val((parseInt($('#txtRealPriceRound').val()) - parseInt($('input#txtPaidMoneyRound').val())));
            $('#paidMoneyRound').val($('#txtPaidMoneyRound').val());
        });
        $('body').on('change', '#totalPrice', function () {
            var ttpr = $(this).val();
            var t = ttpr.replace(/[^0-9]/gi, '');
            $('#txt_totalPrice').val(t);
        });
//        cập nhật commission khi thay đổi đại lí
        $('#cbbCreatedUser').change(function () {
            var dataagc = $("#cbbCreatedUser").select2('data')[0];
            var lv = dataagc.level;
            var additionPriceType = dataagc.plusPriceAgencyType;//giá tăng thêm đại li
            var additionPriceAmount = dataagc.plusPriceAgencyAmount;//giá tăng thêm đại li
            var additionPriceMode = dataagc.plusPriceAgencyMode;//giá tăng thêm đại li
            $('#txtAdditionPriceType').val(additionPriceType);
            $('#txtAdditionPriceAmount').val(additionPriceAmount);
            $('#txtAdditionPriceMode').val(additionPriceMode);
//            console.log('t',additionPriceType,'v',additionPriceAmount)
            var commission = null;
            if (tripSelected.tripData.listCommission.length > 0) {
                var commissions = $.grep(tripSelected.tripData.listCommission, function (i) {
                    return i.level == lv;
                });
                if (commissions.length > 0) {
                    commission = commissions[0];
                }
            }
            if (commission != null) {
                $('#txtCommissionType').val(commission.commissionType);
                $('#txtCommissionValue').val(commission.commissionValue);
            } else {
                $('#txtCommissionType').val(1);
                $('#txtCommissionValue').val(0);
            }
            if($(this).val()===''){//neesu bỏ đại lý thì lấy theo tăng giá của khách hàng
                if(typeof tripSelected.tripData.additionPrice!=="undefined"){
                    $('#txtAdditionPriceType').val(tripSelected.tripData.additionPrice.type);
                    $('#txtAdditionPriceAmount').val(tripSelected.tripData.additionPrice.amount);
                }else{
                    $('#txtAdditionPriceType').val(1);
                    $('#txtAdditionPriceAmount').val(0);
                }
            }
            if($(this).val()===''){
                $('.nhaxethuho').hide();
            }else{
                $('.nhaxethuho').show();
            }
            updateInputPrice(true);
            if(totalPriceBack >0)
            updateInputPriceRound();
        });
        $('#sell_ticket').on('hidden.bs.modal', function () {
            totalPriceBack = 0;
            total_Price = 0;
            Seat_Back_Array = [];
            document.getElementById('cbbRoundTripId').selectedIndex = -1;
            $('#cbbCreatedUser').text('');
            $('.Info_Round').hide();
            $("#listSeatRoundTrip").html('');
        });
        $('#sell_ticket').hover(function () {
            var check_Holder = $('.getInTimePlan').val();
            if (parseInt(check_Holder) <= (new Date()).getTime()) {
                //$('#giucho').hide();
                $('#TimeBookHolder').hide();
            }
        });
//        $('#txtPaidMoney').change(function () {
//
//            var paid = $('#txtPaidMoney').val() == '' ? 0 : parseInt($('#txtPaidMoney').val());
//            if (total_Price > 0)
//                $('#txtMoneyOwing').val(total_Price - paid);
//            else
//                $('#txtMoneyOwing').val(totalPrice - paid);
//        });
        $('#ticket-history').click(function () {
            $('.banve_thongtintinve').hide();
            $('.modal-footer').hide();
            $('.banve_lichsuve').show();
            var ticketId = $('#txtTicketId').val();
            var html = '<tr><td class="text-center" colspan="3">Đang tải dữ liệu...</td></tr>';
            $('#list-history').html(html);
            //Lay lich su ve
            $.ajax({
                dataType: "json",
                url: "{{action('TicketController@getTicketHistory')}}",
                data: {
                    ticketId: ticketId
                },
                success: function (data) {
                    html = '';
                    if (data.length > 0) {
                        $.each(data, function (k, v) {
                            // console.log(v);
                            html += "<tr>";
                            html += "<td>" + getFormattedDate(v.createdDate) + "</td>";
                            html += "<td>" + v.createdUsername + "</td>";
                            html += "<td>";
                            html += '<h5 style="font-weight:bold;color:#bb200f;margin-bottom:0px;">'+checkHistoryType(parseInt(v.logType))+'</h5>';
                            if (v.fullName != undefined) {
                                html += "- Tên khách hàng: " + v.fullName + "<br>";
                            }
                            if (v.paidMoney != undefined) {
                                html += "- Giá vé: " + v.agencyPrice + "<br>";
                            }
                            if (v.unPaidMoney != undefined) {
                                html += "- Chưa thanh toán: " + v.unPaidMoney + "<br>";
                            }
                            if (v.listSeatId != undefined) {
                                html += "- Danh sách ghế: " + v.listSeatId.toString() + "<br>";
                            }
                            if (v.phoneNumber != undefined) {
                                html += "- Số điện thoại: " + v.phoneNumber + "<br>";
                            }
                            if (v.email != undefined) {
                                html += "- Email: " + v.email + "<br>";
                            }
                            if (v.note != undefined) {
                                html += "- Ghi chú: " + v.note + "<br>";
                            }
                            if (v.pickUpAddress != undefined) {
                                html += "- Điểm đón: " + v.pickUpAddress + "<br>";
                            }
                            if (v.dropOffAddress != undefined) {
                                html += "- Điểm trả: " + v.dropOffAddress + "<br>";
                            }
                            if (v.routeName != undefined) {
                                html += '- Chuyến cũ : ' + v.routeName + '<br>';
                            }
                            if (v.srcSeatIds != undefined) {
                                html += "- Ghế cũ : " + v.srcSeatIds.toString() + "<br>";
                            }
                            if (v.desSeatIds != undefined) {
                                html += "- Ghế mới : " + v.desSeatIds.toString() + "<br>";
                            }
                            if (v.newTicketCode != undefined) {
                                html += '- Mã vé mới : ' + v.newTicketCode + '<br>';
                            }
                            html += "</td>";
                            html += "</tr>";
                        });
                    } else {
                        html = '<tr><td colspan="3">Không có dữ liệu</td></tr>';
                    }
                    $('#list-history').html(html);
                },
                error: function () {
                    notyMessage("Lỗi!Vui lòng tải lại trang", "error");
                    html = '<tr><td class="text-center" colspan="3">Không có dữ liệu</td></tr>';
                    $('#list-history').html(html);
                }
            })
        });

        $('#sell-ticket').click(function () {
            $('.banve_thongtintinve').show();
            $('.modal-footer').show();
            $('.banve_lichsuve').hide();
        });

        $('body').on('focus keyup','#pickUpAddress',function () {
            if($(this).val()===''&&$('#phoneNumber').val()!==''){
                var pn =$('#phoneNumber').val();
                SendAjaxWithJson({
                    url : urlDBD('address/list'),
                    type :"post",
                    data : {
                        phoneNumber : pn,
                        addressType : 1
                    },
                    success : function (res) {
                        listAddress = res.results.addresses;
                        if(listAddress.length>0){
                            html = '';
                            html+='<div class="list-group">';
                            html+='<span class="closeAutocompletePickup" style="position:absolute;color:red;right:10px;top:-25px;z-index: 2">ĐÓNG</span>';
                            $.each(listAddress,function (i,e) {
                                html+='<span class="list-group-item changePickup" data-lat="'+e.latitude+'" data-lng="'+e.longitude+'">'+e.address+'</span>';
                            });
                            html+='</div>';
                            $('.autocompletePickup').html(html);
                            $('.autocompletePickup').show();
                        }
                    }
                });
            }else{
                $('.autocompletePickup').hide();
            }
        });
        $('body').on('click','.changePickup',function () {
            $('#pickUpAddress').val($(this).text());
            $("#pickUpLat").val($(this).attr('data-lat'));
            $("#pickUpLong").val($(this).attr('data-lng'));
            $('.autocompletePickup').hide();
            console.log($(this).text(),$(this).attr('data-lat'),$(this).attr('data-lng'));
        });
        $('body').on('click','.closeAutocompletePickup',function () {
            $('.autocompletePickup').hide();
            $('#pickUpAddress').focusout();
        });
        $('body').on('focus keyup','#dropOffAddress',function () {
            if($(this).val()===''&&$('#phoneNumber').val()!==''){
                var pn =$('#phoneNumber').val();
                SendAjaxWithJson({
                    url : urlDBD('address/list'),
                    type :"post",
                    data : {
                        phoneNumber : pn,
                        addressType : 2
                    },
                    success : function (res) {
                        listAddress = res.results.addresses;
                        if(listAddress.length>0){
                            html = '';
                            html+='<div class="list-group">';
                            html+='<span class="closeAutocompleteDropOff" style="position:absolute;color:red;right:10px;top:-25px;z-index: 2">ĐÓNG</span>';
                            $.each(listAddress,function (i,e) {
                                html+='<span class="list-group-item changeDropOff" data-lat="'+e.latitude+'" data-lng="'+e.longitude+'">'+e.address+'</span>';
                            });
                            html+='</div>';
                            $('.autocompleteDropOff').html(html);
                            $('.autocompleteDropOff').show();
                        }
                    }
                });
            }else{
                $('.autocompleteDropOff').hide();
            }
        });
        $('body').on('click','.changeDropOff',function () {
            $('#dropOffAddress').val($(this).text());
            $("#dropOffLat").val($(this).attr('data-lat'));
            $("#dropOffLong").val($(this).attr('data-lng'));
            $('.autocompleteDropOff').hide();
        });
        $('body').on('click','.closeAutocompleteDropOff',function () {
            $('.autocompleteDropOff').hide();
            $('#dropOffAddress').focusout();
        });

        //js lịch
        txtCalendarRoundTrip.datepicker({
            dateFormat: "dd-mm-yy",
            defaultDate: new Date(),
            onSelect: function (date_Str) {
                updateSelectBoxTripRound();
                toDate = new Date(date_Str);
            },
            showOn: 'both',
            buttonText: "<i class=\"icon-calendar\"></i>",
            numberOfMonths: 2
        });

        $(selectRoundRouteId).change(function () {
            if ($('#txtCalendarRoundTrip').val() == '') notyMessage('Hãy chọn ngày khứ hồi', 'warning');
            updateSelectBoxTripRound();
        });

        $('.agency').select2({
            dropdownCss: {'z-index': '999999'},
            width: '100%',
            minimumInputLength: 0,
            allowClear: true,
            placeholder: "Chọn đại lý",
            ajax: {
                url: urlDBD('web_admin/agency/search_by_keyword'),
                type : "post",
                headers : {
                    'DOBODY6969':'{{ session('userLogin')['token']['tokenKey'] }}',
                },
                delay: 1000,
                dataType: 'json',
                data: function (params) {
                    var query = {
                        page :0,
                        count:50,
                        keyword: params.term,
                        getInTime : $('.getInTimePlan').val(),
                        routeId :$('#chontuyen').val(),
                        scheduleId : $('#listTrip option:selected').attr('data-schedule'),
                    };
                    return query;
                },
                processResults: function (data) {
                    dataRs = data.code===200?data.results.result:[];
                    dataAdditionPrice = data.code===200&&typeof data.results.additionPrice!=="undefined"?data.results.additionPrice:{type:1,amount : 0,mode : 1};
                    var r = [];
                    $.map(dataRs, function (item) {
                        if (item.listAgency.length > 0) {
                            var agc = null, level = null;
                            var agcs = $.grep(item.listAgency, function (i) {
                                return i.companyId == tripSelected.tripData.companyId;
                            });
                            if (agcs.length > 0) {
                                agc = agcs[0];
                            }
                            if (agc != null) {
                                level = agc.level;
                            }
                            r.push({text: item.fullName, id: item.userId, level: level,plusPriceAgencyType:dataAdditionPrice.type,plusPriceAgencyAmount:dataAdditionPrice.amount,plusPriceAgencyMode : dataAdditionPrice.mode})
                        }
                    });
                    return {
                        results: r
                    };
                },
                cache: true
            }
        });
        $('body').on('click', '.btnchooseSeatMove', function () {
            //update trạng thái danh sách ghế
            var tdTags = document.getElementsByClassName("canmove");
            for (var i = 0; i < Seat_Back_Array.length; i++)
                for (var j = 0; j < tdTags.length; j++) {
                    if (tdTags[j].textContent == Seat_Back_Array[i]) {
                        $(tdTags[j]).addClass('cantmove');
                    }
                }
        });

        $("[name='goUp']").click(function () {
            updateRadioUp();
        });
        $("[name='goDown']").click(function () {
            updateRadioDown();
        });
        $('body').on('click', '.canmove', function () {
            // $(this).parents('.chooseSeat').find('.seatBack').val($(this).text());
            // $(this).parents('.chooseSeat').find('.btnchooseSeatMove').text($(this).text());
            // $(this).parents('.chooseSeat').find('.seatPrice').val($(this).data('price'));
            var tdTags = document.getElementsByClassName("canmove");

            for (var i = 0; i < Seat_Back_Array.length; i++)
                for (var j = 0; j < tdTags.length; j++) {
                    if (tdTags[j].textContent == Seat_Back_Array[i]) {

                        $(tdTags[j]).addClass('cantmove');
                    }
                }
            // xử lí khi chọn ghế khứ hồi
            var val = $(this).text();
            for (var i = 1; i <= Seat_Back_Array.length; i++) {
                var ghe = 'ghe' + i;
                if (check_Seat(Seat_Back_Array, val) && $(this).parents('.chooseSeat').find('.btnchooseSeatMove').attr('id') == ghe) {

                    var addPrice = $(this).data('price');
                    var meal = $('#cb_isMeal').is(':checked') ? tripSelected.tripData.mealPrice : 0;
                    if ($(this).parents('.chooseSeat').find('.btnchooseSeatMove').text() != 'Chọn Ghế ... ') {
                        totalPriceBack = totalPriceBack - listPrice[i] + addPrice;
                        addPrice += meal;
                        listPrice[i] = addPrice;
                        Seat_Back_Array[i - 1] = val;
                    } else {
                        totalPriceBack += addPrice;
                        addPrice += meal;
                        listPrice[i] = addPrice;
                        Seat_Back_Array[i] = val;
                    }

                    i++;
                    // reset lại trạng thái ghế khi chọn lại
                    if ($(this).parents('.chooseSeat').find('.btnchooseSeatMove').text() != 'Chọn Ghế ... ') {
                        var tdTags = document.getElementsByClassName("cantmove");
                        var searchText = $(this).parents('.chooseSeat').find('.btnchooseSeatMove').text();
                        var found;
                        for (var j = 0; j < tdTags.length; j++) {
                            if (tdTags[j].textContent == searchText) {
//                                console.log(tdTags[j]);
                                found = tdTags[j];
                                $(tdTags[j]).removeClass('cantmove');
                                $(tdTags[j]).addClass('canmove');
                            }
                        }
                    }
                    $(this).removeClass('canmove');
                    $(this).addClass('cantmove');
                    $(this).parents('.chooseSeat').find('.btnchooseSeatMove').text(val);
                }
            }
            var totalListPrice = 0;
            for (var i = 0; i < listPrice.length; i++)
                if (listPrice[i] != '') totalListPrice += listPrice[i];
            $('#txtRealPriceRound').val(totalListPrice);
            $('#total_Price_Back').val(totalListPrice);
            $('#list_Seat').val((Seat_Back_Array.filter(function (e) {
                return e
            })).toString());
            var MoneyOwing = totalListPrice - ($('#txtPaidMoneyRound').val());
            $('#txtMoneyOwingRound').val(MoneyOwing);
            $('#txtPaidMoneyRound').attr('max', totalListPrice);
            // $('#paidMoneyRound').attr('value',totalListPrice);
            $('#MoneyOwingRound').val(MoneyOwing);
            if($('input.txtPromotionCode').val()!='')
            $.ajax({
                'url': urlCheckPromotionCode,
                'data': {
                    'scheduleId': $('#txt_scheduleId').val(),
                    'routeId':$('#cbbRoundRouteId option:selected').val(),
                    'tripId':$('#cbbRoundTripId option:selected').val(),
                    'promotionCode': $('input.txtPromotionCode').val(),
                    'getInTimePlan': TripData_Round.startTime
                },
                'dataType': 'json',
                'success': function (data) {
                    var result = data.result;
                    if (result.percent != undefined && result.price != undefined){
                        $('#hasPromotionRound').val(1);
                    }

                }
            });
            updateInputPriceRound();
        });

        // sự kiện chọn khứ hồi khứ hồi
        $('body').on('click', '#txtRoundTrip', function () {
            var date = $('#txtCalendar').val();
            var date_Str = '';
            for (var i = 0; i < 10; i++) {

                if (i == 1 || i == 0) {
                    date_Str += date.charAt(i + 3);
                } else if (i == 3 || i == 4) {
                    date_Str += date.charAt(i - 3);
                }
                else date_Str += date.charAt(i);
            }
            //$('#txtCalendarRoundTrip').val(date_Str);
            $('#txtCalendarRoundTrip').datepicker("option", {
                minDate: new Date(date_Str)
            });
            showHideRoundTrip($(this));
            (document.getElementById('cbbRoundRouteId')).scrollIntoView();
            var paymentCode = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + Date.now();
            $('#paymentCode').val(paymentCode);
            $('#txtPaidMoney').val(stringToInt($('#txtPaidMoney').val()));
        });

//kiem tra ghe dc chon trong danh sach ghe khu hoi
        function check_Seat(Seat_Back_Array, val) {
            for (var i = 0; i < Seat_Back_Array.length; i++)
                if (Seat_Back_Array[i] == val) return false;
            return true;

        }

        // ẩn hiện khứ hồi

        function sellRoundTrip(_tripDestinationId, _listTripDestination, scheduleId) {
            $('#loading_round').show();

            var tripDestinationId = _tripDestinationId;
            TripData_Round = $.grep(_listTripDestination, function (tripDestination) {
                return tripDestination.scheduleId === scheduleId;
            })[0] || [];

            //nạp data cho form khứ hồi
            if (TripData_Round.tripId != null) $('#trip_Id').val(TripData_Round.tripId);
            $('#getInTimePlanRound').val(TripData_Round.startTime);
            $('#getOffTimePlanRound').val(TripData_Round.getOffTime);
            $('#routeIdRound').val(TripData_Round.routeId);
            $('#scheduleIdRound').val(TripData_Round.scheduleId);
            $('#startDateRound').val(TripData_Round.startTime);
            $('#cb_priceInsurranceRound').val(TripData_Round.priceInsurrance);
            $('#cb_isMealRound').val(TripData_Round.mealPrice);
            $('#pickUpLatRound').val(TripData_Round.latitudePointStart);
            $('#pickUpLongRound').val(TripData_Round.longitudePointStart);
            $('#getInPointIdRound').val(TripData_Round.getInPointId);
            $('#getOffPointIdRound').val(TripData_Round.getOffPointId);
            $('#txtGetInPointNameRound').val(TripData_Round.getInPointName);
            $('#txtGetOffPointNameRound').val(TripData_Round.getOffPointName);
            $('#priceInsurranceRound').val($.number(TripData_Round.priceInsurrance));
            $('#dropOffAddressRound').val(TripData_Round.getOffPointAddress);
            $('#pickUpAddressRound').val(TripData_Round.getInPointAddress);
            $('.routeName').val($('#chontuyen option:selected').text());
            $('#dropOffLatRound').val(TripData_Round.latitudePointEnd);
            $('#dropOffLongRound').val(TripData_Round.longitudePointEnd);
            $('.InTimeRound').val(getFormattedDate(TripData_Round.getInTime, 'date'));
            $('.OffTimeRound').val(getFormattedDate(TripData_Round.getOffTime, 'date'));
            $('#txtRealPriceRound').val(0);
            $('#txtPaidMoneyRound').val(0);
            $('#txtMoneyOwingRound').val(0);
            $("#listSeatRoundTrip").hide();
            totalPriceBack = 0;
            listPrice = [];
            listPrice.push('');
            Seat_Back_Array = [];
            Seat_Back_Array.push('');
            $("#listSeatRoundTrip").html('');
            var html = '';
            var dateFormat=($('#txtCalendarRoundTrip').val()).substr(6,4)+'-'+($('#txtCalendarRoundTrip').val()).substr(3,2)+'-'+($('#txtCalendarRoundTrip').val()).substr(0,2);
            $.ajax({
                dataType: "json",
                url: urlSeatMapInTrip,
                data: {
                    'tripId': TripData_Round.tripId,
                    'scheduleId': TripData_Round.scheduleId,
                    'date':(new Date(dateFormat)).getTime()
                },

                success: function (data) {
                    console.log('round',data);
                    data_generate_listseat = data;
                    $('#loading_round').hide();
                    $('#ui-datepicker-div').hide();

                    for (var a = 1; a <= $('.ghedangchon').length; a++) {
                        html += generateListSeatRound(data.seatMap, a);
                    }
                    html += '<div class="chooseSeat"><label class="control-label" for="cbbRoundTripId"></label><div class="dropdown dropup">';
                    html += '<a style="color: firebrick;margin-top: 15px;    margin-left: 10%" data-toggle="dropdown" href="#" id="addSeat"class=" btn ">+</a></div></div>';
                    $("#listSeatRoundTrip").html(html);
                    $("#listSeatRoundTrip").show();
                    $("#txtTripSourceId").val(tripSelected.tripId);
                    $("#txtTimeChange").val(TripData_Round.getInTime || '');
                    $("#txtPlaceChange").val(TripData_Round.getInPointId || '');
                    $('.btnchooseSeatMove').val('Chọn ghế');
                }
            });


        }

        function generateListSeatRound(data, a) {
            var html = '';
            html += '<div class="chooseSeat"><label class="control-label" for="cbbRoundTripId">Chọn ghế khứ hồi</label><div class="dropdown dropup">';
            html += '<a data-toggle="dropdown" href="#" id="ghe' + a + '"class="btn btnchooseSeatMove">Chọn Ghế ... </a>' +
                '<button type="button" data-id="ghe' + a + '" class="close  cancelSeatRound " aria-label="Close">' +
                '  <span class="x">&times;</span>' +
                '</button><input type="hidden" name="seatBack[]" class="seatBack">';
            html += '<input type="hidden" name="seatPrice[]" class="seatPrice">';
            html += '<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">';
            for (var i = 1; i <= data.numberOfFloors; i++) {
                html += '<p style="font-weight:bold;">Tầng ' + i + '</p>';
                html += '<table class="seatmaptable">';
                for (var r = 1; r <= data.numberOfRows; r++) {
                    html += '<tr>';
                    for (var c = 1; c <= data.numberOfColumns; c++) {
                        var elm = $.grep(data.seatList, function (seat) {
                            return (seat.floor == i && seat.row == r && seat.column == c);
                        });
                        if (typeof elm[0] === "undefined") {
                            html += '<td></td>';
                        } else {
                            if (checkStatusSeatAndTicket(elm[0].seatStatus, elm[0].overTime).seatStatus === 'ghetrong' &&
                                (elm[0].seatType === 3 || elm[0].seatType === 4)) {
                                var price = TripData_Round.ticketPrice + elm[0].extraPrice;
                                html += '<td style="border:1px solid #ddd;" class="canmove" data-price="' + price + '">' + elm[0].seatId + '</td>';
                            } else {
                                html += '<td style="border:1px solid #ddd;" class="cantmove">' + elm[0].seatId + '</td>';
                            }
                        }
                    }
                    html += '</tr>';
                }

                html += '</table><hr>';
            }
            html += '</div></ul></div>';

            return html;
        }

        function updateSelectBoxTripRound() {
            $('#loading_round').show();

            for (var i = 0; i < Seat_Back_Array.length; i++) {
                Seat_Back_Array[i] = '';
                listPrice[i] = 0;
            }

            totalPriceBack = 0;
            total_Price = 0;
            var routeId = $(selectRoundRouteId).val();
            var date = $(txtCalendarRoundTrip).val();
            if (date == '') {
                return false;
            }
            /*
            * Lấy dữ liệu của tuyến
            * */
            var route = $.grep(listRoute, function (item) {
                return item.routeId == routeId;
            });
            /*Lấy điểm đầu và điểm cuối*/
            var listPoint=[];
            var startPointId;
            var endPointId;
            $.each(route[0].listPoint,function(v){
                listPoint.push(v.pointId);
            });
            if(listPoint.indexOf($('#cbb_InPoint option:selected').attr('value')) && listPoint.indexOf($('#cbb_OffPoint option:selected').attr('value'))) {
                startPointId=$('#cbb_OffPoint option:selected').attr('value');
                endPointId=$('#cbb_InPoint option:selected').attr('value');
            }else {
                startPointId = route[0].listPoint[0].pointId;
                endPointId = route[0].listPoint[route[0].listPoint.length - 1].pointId;
            }

            $(selectRoundTripId).html('<option value="">Đang tải dữ liệu...</option>');
            $("#cbbRoundTripId").prop('disabled', true).html('');
            $.ajax({
                type: 'GET',
                url: urlSeachTrip,
                headers: {
                    'DOBODY6969': '{{ session('userLogin')['token']['tokenKey'] }}',
                },
                contentType: "application/json; charset=utf-8",
//                async: false,
                data:{
                    'routeId': routeId,
                    'page': 0,
                    'count': 100,
                    'timeZone': 7,
                    'companyId': '{{ session("companyId") }}',
                    'date': date,
                    'startPointId': startPointId,
                    'endPointId': endPointId,
                },
                success: function (data) {
                    console.log('sds',data);
                    $('#loading_round').hide();
                    $('#ui-datepicker-div').hide();
                    var html = '';
                    listRoundTrip =data;
                    listRoundTrip.forEach(function (trip) {
                        var totalSeat = [], seatBooked = [];
                        if (trip.seatMap != null) {
                            totalSeat = $.grep(trip.seatMap.seatList, function (seat) {
                                return seat.seatType == 3 || seat.seatType == 4;
                            });

                            seatBooked = $.grep(totalSeat, function (seat) {
                                return seat.seatStatus == 3 || (seat.seatStatus == 2 && (seat.overTime > Date.now() || seat.overTime == 0));
                            });
                        }
                        if ($('#round-trip').attr('style') != 'display: block;') {
                            var isSelected = tripSelected.tripId === trip.tripId ? 'selected' : '';

                        }
                        else if (round_route['routeId'] && round_route['routeId'] == tripSelected.routeId)
                            var isSelected = 'selected';

                        html += '<option ' + ' value="' + trip.tripId + '" data-schedule="' + trip.scheduleId + '">' +
                            trip.numberPlate + ' - ' +
                            getFormattedDate(trip.getInTime, 'time') +
                            '         (Trống: ' + ($(totalSeat).length - $(seatBooked).length) + ')' +
                            '</option>';
                    });
                    if (listRoundTrip.length >= 1) {
                        $(selectRoundTripId).html(html);
                        $("#cbbRoundTripId").prop('disabled', false);
                    } else {
                        $(selectRoundTripId).html('<option value="">Hiện tại không có chuyến</option>');
                        $('#listSeatRoundTrip').hide();
                        $('#listSeatRoundTrip').html('');
                        $('#listSeatRoundTrip').show();
                        $('.btnchooseSeatMove').prop('disabled', true);
                        return;
                    }
                    sellRoundTrip($(selectRoundTripId).val(), listRoundTrip, $('#cbbRoundTripId option:selected').data('schedule'));
                },
                error: function () {
                    notyMessage("Lỗi!Vui lòng tải lại trang", "error");
                }
            })
        }

        $(selectRoundTripId).change(function () {
            sellRoundTrip($(this).val(), listRoundTrip, $('#cbbRoundTripId option:selected').data('schedule'));
        });

        $('body').on('click', '.actionUpdateTicket', function () {
            tripSelected.waitingUpdate = true;
            var visMeal = '';
            var visPickUpHome = '';
            var visInsurrance = '';
            var vsendSMS = '';
            $('#sell_ticket').modal('hide');
            $('.ghedangchon').append('<div class="loader"></div>');
            $(".ghedangchon *").attr("disabled", "disabled").off('click');

            if ($("[name='sendSMS']").prop('checked') == true) {
                vsendSMS = true;
            } else {
                vsendSMS = false;
            }
            if ($("[name='isMeal']").prop('checked') == true) {
                visMeal = $("[name='isMeal']").val();
            } else {
                visMeal = null;
            }
            if ($("[name='isInsurrance']").prop('checked') == true) {
                visInsurrance = $("[name='isInsurrance']").val();
            } else {
                visInsurrance = null;
            }
            if ($("[name='isPickUpHome']").prop('checked') == true) {
                visPickUpHome = $("[name='isPickUpHome']").val();
            } else {
                visPickUpHome = null;
            }
            var thuho = "true";
            if($('#collectMoneyForAgency').prop('checked')==false){
                thuho='false';
            }
            var daiLyBan = $(this).attr('data-sell-and-pay')=="true" ? true : false;
            var timeBookHolder=$("#datve").find("[name='timeBookHolder']").val();
            if(daiLyBan){
                timeBookHolder = 0{{isset(session('userLogin')['timeBookHolder'])?session('userLogin')['timeBookHolder']:0}};
            }
            var option = $(this).val();
            if(daiLyBan){
                option = '3';
            }
            $('.disableAction').show();
            $.ajax({
                url: '{{ action('TicketController@updateStatusTicket') }}',//url gửi đi
                dataType: 'json',
                type: 'post',
                data: {
                    option: option,
                    ticketId: $("#datve").find("[name='ticketId']").val(),
                    //tripId : tripSelected.tripData.tripId,
                    //scheduleId : tripSelected.tripData.scheduleId,
                    fullName: $("#datve").find("[name='fullName']").val(),
                    phoneNumber: $("#phoneNumber").val(),
                    email: $("#datve").find("[name='email']").val(),
                    priceInsurrance: $("#datve").find("[name='priceInsurrance']").val(),
                    promotionCode: $("#datve").find("[name='promotionCode']").val(),
                    note: $("#datve").find("#txtNote").val(),
                    agencyUserId: $("#datve").find("[name='agencyUserId']").val(),
                    phoneNumberReceiver: $("#datve").find("[name='phoneNumberReceiver']").val(),
                    fullNameReceiver: $("#datve").find("[name='fullNameReceiver']").val(),
                    //isPickUpHome : visPickUpHome,
                    //isInsurrance : visInsurrance,
                    originalPrice: $("#datve").find("[name='originalPrice']").val(),
                    extraPrice  : $('#txtThuThem').val()||0,
                    paymentTicketPrice : $('#txtPaymentPrice').val(),
                    sendSMS: vsendSMS,
                    paidMoney: $("#datve").find("[name='paidMoney']").val(),
                    collectMoneyForAgency : thuho,
                    goUp : $("[name='goUp']:checked").val(),
                    goDown : $("[name='goDown']:checked").val(),
                    pickUpAddress: $("#datve").find("[name='pickUpAddress']").val(),
                    pickUpLat: $("#datve").find("[name='pickUpLat']").val(),
                    pickUpLong: $("#datve").find("[name='pickUpLong']").val(),
                    dropOffAddress: $("[name='dropOffAddress']").val(),
                    dropOffLat: $("#datve").find("[name='dropOffLat']").val(),
                    dropOffLong: $("#datve").find("[name='dropOffLong']").val(),
                    totalPrice: $("#datve").find("[name='totalPrice']").val(),
                    listSeat: $("#datve").find("[name='listSeat']").val(),
                    listExtraPrice: $("#datve").find("[name='listExtraPrice']").val(),
                    listExtraTK: $("#datve").find("[name='listExtraTK']").val().toString(),
                    foreignKey: $("#datve").find("[name='foreignKey']").val(),
                    alongTheWayAddress: $("[name='alongTheWayAddress']").val(),
                    dropAlongTheWayAddress: $("[name='dropAlongTheWayAddress']").val(),
                    realPrice: $("[name='realPrice']").val(),
                    cancelReason: $("[name='cancelReason']").val(),
                    transshipmentInPointId : $('select.transshipmentInPoint').val(),
                    transshipmentOffPointId : $('select.transshipmentOffPoint').val(),
                    transshipmentInPointPrice: $("[name='transshipmentInPointPrice']").val(),
                    transshipmentOffPointPrice: $("[name='transshipmentOffPointPrice']").val(),
                    //salePrice : $("[name='salePrice']").val(),
                    _token: '{{csrf_token()}}',
                },
                success: function (data) {
                    if (data.status === 'success') {
                        sessionStorage.setItem('lockSeat','{}');//khoong goji API mo khoa ghe
                        notyMessage(data.results.message||'Thực thi thành công', 'success');
                        if(data.results!=undefined && data.results.ticket !=undefined &&  sessionStorage.getItem('print_after_save')==1){
                            var info=data.results.ticket;
                            printTicket(info.ticketId, info.ticketCode);
                            sessionStorage.setItem('print_after_save',0);
                        }
                        if(vsendSMS){
                            if(typeof data.results.megaVSMS !== "undefined"&&data.results.megaVSMS.isAvailable ===false){
                                var n = new Noty({
                                    type : "error",
                                    text: 'Gửi tin nhắn thất bại! số tiền trong ví tin nhắn đã hết. Vui lòng nạp thêm tiền . số dư hiện tại : '+moneyFormat(data.results.megaVSMS.currentAmount),
                                    buttons: [
                                        Noty.button('NẠP TIỀN', 'btn btn-success', function () {
                                            window.location.href = '{{ action('ReportController@messageReport') }}';
                                        }, {id: 'button1', 'data-status': 'ok','style':'margin-right:10px'}),
                                        Noty.button('BỎ QUA', 'btn btn-error', function () {
                                            n.close();
                                        })
                                    ]
                                }).show();
                            }
                        }
                        if(daiLyBan){
                            if('{{session('companyId')}}'==='TC1OHntfnujP'){//inter
                                var ticket = data.results.ticket;
                                if($('#sellPaymentType option:selected').data('type')==='thequocte'){
                                    console.log('quocte')
                                    window.open(urlDBD('inter-payment/dopay?vpc_OrderInfo='+ticket.ticketId+'&vpc_Amount='+ticket.agencyPrice+'&phoneNumber='+ticket.phoneNumber+'&packageName={{ url('cpanel/ticket/sell-ver1') }}'), '_blank');
                                }else if($('#sellPaymentType option:selected').data('type')==='thenoidia'){
                                    console.log('noidi')
                                    window.open(urlDBD('interbuslines/dopay?vpc_OrderInfo='+ticket.ticketId+'&vpc_Amount='+ticket.agencyPrice+'&phoneNumber='+ticket.phoneNumber+'&packageName={{ url('cpanel/ticket/sell-ver1') }}'), '_blank');
                                }else if($('#sellPaymentType option:selected').data('type')==='vnp'){
                                    window.open('{{ url("cpanel/ticket/payVnp") }}'+'/'+ticket.ticketId);
                                }
                            }else{
                                SendAjaxWithJson({
                                    url  :urlDBD('ePay/'),
                                    dataType : 'json',
                                    type : 'post',
                                    data : {
                                        ticketId : data.results.ticket.ticketId,
                                        paymentType : 1,
                                    },
                                    success: function (res) {
                                        window.open(res.results.redirect, '_blank');
                                    },
                                    functionIfError : function (res) {
                                        notyMessage('Gửi Yêu cầu thanh toán thất bại! vé được giữ chỗ '+timeBookHolder+' phút, vui lòng thanh toán lại để vé không bị hủy','error');
                                    }
                                })
                            }
                        }
                    }else{
                        if(data.code!==200&&data.results.error.propertyName==='DUPLICATE_PHONE_NUMBER_IN_TRIP'){
                            notyMessage("Số điện thoại đã tồn tại trong chuyến", 'error');
                        }else if(data.results.error.propertyName==='TICKET_000045'){
                            notyMessage("Hạn mức đại lí đã hết, vui lòng tăng hạn mức để tiếp tục", 'error');
                        } else {
                            notyMessage("Có lỗi xảy ra khi gửi yêu cầu! vui lòng tải lại trang", 'error');
                            generateHtmlTripInfo();
                        }
                    }
                    setTimeout(function(){
                        if(tripSelected.waitingUpdate == true){
                            updateTripInfo(selectBoxListTrip.val(), selectBoxListTrip.find('option:selected').data('schedule'));
                        }
                    },2000);
                    $('.disableAction').hide();
                    $("[name='cancelReason']").val('');
                },
                error: function (data) {
                    $('.disableAction').hide();
                    notyMessage("Lỗi!Vui lòng tải lại trang", "error");
                }
            });
            return false;
        });


        $('body').on('click', '.actionSellTicket', function () {
            tripSelected.waitingUpdate = true;
            var visMeal = '';
            var visPickUpHome = '';
            var visInsurrance = '';
            var vsendSMS = '';
            $('#sell_ticket').modal('hide');
            $('.ghedangchon').append('<div class="loader"></div>');
            $(".ghedangchon *").attr("disabled", "disabled").off('click');

            if ($("[name='sendSMS']").prop('checked') == true) {
                vsendSMS = $("[name='sendSMS']").val();
            } else {
                vsendSMS = null;
            }
            if ($("[name='isMeal']").prop('checked') == true) {
                visMeal = $("[name='isMeal']").val();
            } else {
                visMeal = null;
            }
            if ($("[name='isInsurrance']").prop('checked') == true) {
                visInsurrance = $("[name='isInsurrance']").val();
            } else {
                visInsurrance = null;
            }
            if ($("[name='isPickUpHome']").prop('checked') == true) {
                visPickUpHome = $("[name='isPickUpHome']").val();
            } else {
                visPickUpHome = null;
            }

            submit = addSeat.flag ? 'ADDSEAT':$(this).val();

            var ticket_secondary_seat = false;
            if($('.ghedangchon').hasClass('seatFreeSeatMap')){
                ticket_secondary_seat = true;
            }
            var thuho = "true";
            if($('#collectMoneyForAgency').prop('checked')==false){
                thuho='false';
            }
            var daiLyBan =$(this).attr('data-sell-and-pay')==="true" ? true : false;
            var timeBookHolder=$("#datve").find("[name='timeBookHolder']").val();
            if(daiLyBan){
                timeBookHolder = 0{{isset(session('userLogin')['timeBookHolder'])?session('userLogin')['timeBookHolder']:0}};
            }

            $('.ghe').removeClass('ghedangchon');
            $('.disableAction').show();
            $.ajax({
                url: '{{action('TicketController@postSell')}}',
                dataType: 'json',
                type: 'post',
                async: true,
                data: {
                    getInPointId: $('#txtGetInPointId').val(),
                    getOffPointId: $('#txtGetOffPointId').val(),
                    getInTimePlan: $('.getInTimePlan').val(),
                    getOffTimePlan: $('.getOffTimePlan').val(),
                    tripId: tripSelected.tripData.tripId,
                    scheduleId: tripSelected.tripData.scheduleId,
                    fullName: $("#datve").find("[name='fullName']").val(),
                    phoneNumber: $("#phoneNumber").val(),
                    listSeat: $("#datve").find("[name='listSeat']").val(),
                    numberOfAdults: $("#numberOfAdults").val(),
                    numberOfChildren: $("#numberOfChildren").val(),
                    promotionId: $("[name='promotionId']").val(),
                    promotionCode: $("[name='promotionCode']").val(),
                    startDate: $("[name='startDate']").val(),
                    isMeal: visMeal,
                    isPickUpHome: visPickUpHome,
                    isInsurrance: visInsurrance,
                    sendSMS: vsendSMS,
                    paymentType: $("#datve").find("[name='paymentType']").val(),
                    originalPrice: $("#datve").find("[name='originalPrice']").val(),
                    extraPrice  : $('#txtThuThem').val()||0,
                    paymentTicketPrice : $('#txtPaymentPrice').val(),
                    totalPrice : $('#totalPrice').val(),
                    realPrice: $("#datve").find("[name='realPrice']").val(),
                    transshipmentInPointId : $('select.transshipmentInPoint').val(),
                    transshipmentOffPointId : $('select.transshipmentOffPoint').val(),
                    transshipmentInPointPrice: $("#datve").find("[name='transshipmentInPointPrice']").val(),
                    transshipmentOffPointPrice: $("#datve").find("[name='transshipmentOffPointPrice']").val(),
                    paidMoney: $("#datve").find("[name='paidMoney']").val(),
                    timeBookHolder: timeBookHolder,
                    goUp : $("[name='goUp']:checked").val(),
                    goDown : $("[name='goDown']:checked").val(),
                    pickUpAddress: $("#datve").find("[name='pickUpAddress']").val(),
                    pickUpLat: $("#datve").find("[name='pickUpLat']").val(),
                    pickUpLong: $("#datve").find("[name='pickUpLong']").val(),
                    dropOffAddress: $("[name='dropOffAddress']").val(),
                    dropOffLat: $("#datve").find("[name='dropOffLat']").val(),
                    dropOffLong: $("#datve").find("[name='dropOffLong']").val(),
                    note: $("#txtNote").val(),
                    agencyUserId: $("#datve").find("[name='agencyUserId']").val(),
                    createdUser: $("[name='createdUser']").val(),
                    salePrice: $("[name='salePrice']").val(),
                    foreignKey: $("#datve").find("[name='foreignKey']").val(),
                    alongTheWayAddress: $("#datve").find("[name='alongTheWayAddress']").val(),
                    dropAlongTheWayAddress: $("#datve").find("[name='dropAlongTheWayAddress']").val(),
                    submit: submit,
                    collectMoneyForAgency : thuho,
                    _token: '{{csrf_token()}}',
                    list_Seat: $('#list_Seat').val(),
                    roundTrip: $('#txtRoundTrip').val(),
                    total_Price_Back: $('#total_Price_Back').val(),
                    paymentCode: $('#paymentCode').val(),
                    trip_Id: $('#trip_Id').val(),
                    getInPointIdRound: $('#getInPointIdRound').val(),
                    getOffPointIdRound: $('#getOffPointIdRound').val(),
                    getInTimePlanRound: $('#getInTimePlanRound').val(),
                    getOffTimePlanRound: $('#getOffTimePlanRound').val(),
                    scheduleIdRound: $('#scheduleIdRound').val(),
                    adultsRound: $('#adultsRound').val(),
                    startDateRound: $('#startDateRound').val(),
                    pickUpAddressRound: $('#pickUpAddressRound').val(),
                    pickUpLatRound: $('#pickUpLatRound').val(),
                    pickUpLongRound: $('#pickUpLongRound').val(),
                    dropOffAddressRound: $('#dropOffAddressRound').val(),
                    dropOffLatRound: $('#dropOffLatRound').val(),
                    dropOffLongRound: $('#dropOffLongRound').val(),
                    mealPriceRound: $('#mealPriceRound').val(),
                    realPriceRound:$('#txtRealPriceRound').val(),
                    priceInsurranceRound: $('#priceInsurranceRound').val(),
                    ticketId: $('#txtTicketId').val(),
                    ticket_secondary_seat : ticket_secondary_seat,
                },
                success: function (data) {
                    // console.log(data);
                    if(data.start.code === 200) {
                        sessionStorage.setItem('lockSeat','{}');//khoong goji API khoas ghe
//                        var itv = setInterval(function () {
//                            if(tripSelected.waitingUpdate === false){
//                                $("[data-ticketid='"+data.start.results.ticket.ticketId+"']").find('.editTicket').click();
//                                clearInterval(itv);
//                            }
//                        },500);
                        if(data.start.results!=undefined && data.start.results.ticket!=undefined &&  sessionStorage.getItem('print_after_save')==1){
                            var info=data.start.results.ticket;
                            printTicket(info.ticketId, info.ticketCode);
                            sessionStorage.setItem('print_after_save',0);
                        }
                        if(ticket_secondary_seat){
                            $('#gheThemKhongThuocSeatMap').find('.khungxe').html('');
                        }
                        if(daiLyBan){
                            if('{{session('companyId')}}'==='TC1OHntfnujP'){//inter
                                var ticket = data.start.results.ticket;
                                if($('#sellPaymentType option:selected').data('type')==='thequocte'){
                                    console.log('quocte')
                                    window.open(urlDBD('inter-payment/dopay?vpc_OrderInfo='+ticket.ticketId+'&vpc_Amount='+ticket.agencyPrice+'&phoneNumber='+ticket.phoneNumber+'&packageName={{ url('cpanel/ticket/sell-ver1') }}'), '_blank');
                                }else if($('#sellPaymentType option:selected').data('type')==='thenoidia'){
                                    console.log('noidi')
                                    window.open(urlDBD('interbuslines/dopay?vpc_OrderInfo='+ticket.ticketId+'&vpc_Amount='+ticket.agencyPrice+'&phoneNumber='+ticket.phoneNumber+'&packageName={{ url('cpanel/ticket/sell-ver1') }}'), '_blank');
                                }else if($('#sellPaymentType option:selected').data('type')==='vnp'){
                                    console.log('vnp');
                                    window.open('{{ url("cpanel/ticket/payVnp") }}'+'/'+ticket.ticketId);
                                }
                            }else{
                                SendAjaxWithJson({
                                    url  :urlDBD('ePay/'),
                                    dataType : 'json',
                                    type : 'post',
                                    data : {
                                        ticketId : data.start.results.ticket.ticketId,
                                        paymentType : 1,
                                    },
                                    success: function (res) {
                                        window.open(res.results.redirect, '_blank');
                                    },
                                    functionIfError : function (res) {
                                        notyMessage('Gửi Yêu cầu thanh toán thất bại! vé được giữ chỗ '+timeBookHolder+' phút, vui lòng thanh toán lại để vé không bị hủy','error');
                                    }
                                })
                            }
                        }
                    } else if(data.start.results.error.message === 'USR_ERROR_000000045') {
                        notyMessage("Đặt vé thất bại. Bạn đã quá hạn mức công nợ", 'error');
                        generateHtmlTripInfo();
                    } else {
                        if(vsendSMS){
                            if(typeof data.results.megaVSMS !== "undefined"&&data.results.megaVSMS.isAvailable ===false){
                                var n = new Noty({
                                    type : "error",
                                    text: 'Gửi tin nhắn thất bại! số tiền trong ví tin nhắn đã hết. Vui lòng nạp thêm tiền . số dư hiện tại : '+moneyFormat(data.results.megaVSMS.currentAmount),
                                    buttons: [
                                        Noty.button('NẠP TIỀN', 'btn btn-success', function () {
                                            window.location.href = '{{ action('ReportController@messageReport') }}';
                                        }, {id: 'button1', 'data-status': 'ok','style':'margin-right:10px'}),
                                        Noty.button('BỎ QUA', 'btn btn-error', function () {
                                            n.close();
                                        })
                                    ]
                                }).show();
                            }
                        }
                        notyMessage("Có lỗi khi gửi yêu cầu", 'error');
                        generateHtmlTripInfo();
                    }
                    if (tripSelected.tripData.tripId === '-1') {
                        location.reload();
                        generateHtmlTripInfo();
                    }
                    setTimeout(function(){
                        if(tripSelected.waitingUpdate === true){
                            updateTripInfo(selectBoxListTrip.val(), selectBoxListTrip.find('option:selected').data('schedule'));
                        }
                    },2000);
                    $('.disableAction').hide();
                },
                error : function (data) {
                    $('.disableAction').hide();
                    notyMessage("Lỗi!Vui lòng tải lại trang", "error");
                }
            });
            addSeat.flag = false;
            addSeat.ticketInfo = {};
            return false;
        });
    });
    function openNewTab(url) {
        window.open(url, '_blank');
    }

    function updateRadioUp(){
        if($("[name='goUp']:checked").val()=='transshipment'){
            $('select.transshipmentInPoint').show();
            $('#pickUpAddress').hide();
        }else{
            $('select.transshipmentInPoint').val('').change();
            $('select.transshipmentInPoint').hide();
            $('#pickUpAddress').show();
        }
    }
    function updateRadioDown(){
        if($("[name='goDown']:checked").val()=='transshipment'){
            $('select.transshipmentOffPoint').show();
            $('#dropOffAddress').hide();
        }else{
            $('select.transshipmentOffPoint').val('').change();
            $('select.transshipmentOffPoint').hide();
            $('#dropOffAddress').show();
        }
    }

    function initMap() {
        var pickUpInput = document.getElementById('pickUpAddress');
        var autocompletePick = new google.maps.places.Autocomplete(pickUpInput);
        autocompletePick.setComponentRestrictions(
            {'country': ['vn']});
        autocompletePick.addListener('place_changed', function () {
            console.log(autocompletePick);
            var placePick = autocompletePick.getPlace();
            if (!placePick.place_id) {
                return;
            }
            var latPick = placePick.geometry.location.lat(),
                lngPick = placePick.geometry.location.lng();

            $("#pickUpLat").val(latPick);
            $("#pickUpLong").val(lngPick);
        });
        var dropOffInput = document.getElementById('dropOffAddress');
        var autocompleteDropOff = new google.maps.places.Autocomplete(dropOffInput);
        autocompleteDropOff.setComponentRestrictions(
            {'country': ['vn']});
        autocompleteDropOff.addListener('place_changed', function () {
            var placeDropOff = autocompleteDropOff.getPlace();
            if (!placeDropOff.place_id) {
                return;
            }
            var latDropOff = placeDropOff.geometry.location.lat(),
                lngDropOff = placeDropOff.geometry.location.lng();

            $("#dropOffLat").val(latDropOff);
            $("#dropOffLong").val(lngDropOff);

        });
    }

</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key={!! API_GOOGLE_KEY !!}&callback=initMap&libraries=places&language=vn"
        type="text/javascript"></script>

