<style>
    .seatTitle {
        padding: 0;
        background-color: #fff;
        width: 100%;
    }
    .seatId {
        font-weight: bold;
        font-size: 16px;
        width: 30px;
        padding: 7px 0 7px 2px;
        margin-left: 10px;
    }
    .phoneghedadat{
        background: #137391 !important;
        color :#fff !important;
    }
    .phoneghelenxe{
        background: #00aa00 !important;
        color :#fff !important;
    }

    .isChecked {
        background: #f9685e;
        color :#fff !important;
    }

    .phoneNumber {
        color: #000;
        margin-top: 2px;
        border: 1px solid #000;
        padding: 5px 0 14px 0px;
        float: right;
        position: absolute;
        right: 0;
        top: 0;
        width: 130px;
        height: 5px;
        text-align: center;
        font-weight: bold;
        border-radius: 5px;
        font-size: 16px;
    }

    .ticketInfoSeat {
        font-family: sans-serif;
        padding: 2px;
        font-weight: 500;
        font-size: 12px !important;
        text-transform: none;
    }

    .ticketPrice, .sellerName, .sourceName {
        margin-top: 5px;
    }

    /*.moneyFormat {*/
        /*font-weight: bold;*/
    /*}*/

    .sellerName {
        font-size: 12px;
        font-weight: 500;
        margin-top: 5px;
    }

    .line-height18{
        line-height: 18px;
    }

    .sourceName {
        font-size: 12px;
        font-weight: 500;
        margin-top: 0;
    }

    .sourceName {
        margin-top: 5px;
        color: #29942d;
        font-weight: 600;
    }

    .ticketInPoint, .ticketOffPoint {
        text-align: right;
        font-size: 18px;
        margin:3px 0;
    }

    .ghedadat .ticketInPoint, .ghedadat .ticketOffpoint {
        color: #000;
    }

    .ticketInPoint {
        margin-top: 3px;
    }
    .ticketOffpoint {
        font-size: 16px;
    }

    .ticketEdit{
        z-index: 1;
        text-align:center;
        position: absolute;
        bottom:-30px;
        opacity: 0;
        transition: bottom 0.3s , opacity 0.3s;
    }
    .ghedangchon .ticketEdit{
        bottom: 10px;
        opacity: 1;
    }

    .gc_dachon{
        border:1px solid red;
    }

    .gc_giucho {
        background: #fe837a;
    }

    .gc_dadat {
        background: #147291;
    }

    .gc_ghelenxe{
        background: #00aa00 !important;
    }

    .ticketEdit .btn{
        border:none;
        margin: 0px 3px;
    }

    .ticketCode {
        margin-top: 5px;
        font-size: 14px;
        text-align: right;
        font-weight: bold;
    }

    .customerName{
        font-size: 14px;
        font-weight: bold;
    }

    .ticketNote {
        position: absolute;
        bottom:0;
        width: 100%;
        text-align: center;
        color:red;
        margin-top: 5px;
        font-size: 12px;
        font-style: italic;
    }

    .sellTicket{
        margin-top: 5px;
        margin-right: 5px;
        float: right;
        position: absolute;
        right: 0;
        top: 0;
    }

    .col-giucho {
        color: #f96026 !important;
        border-color: #f96026 !important;
    }

    .col-dadat {
        color: #137491 !important;
        border-color: #137491 !important;
    }

    .col-lenxe {
        color: #00aa00 !important;
        border-color: #00aa00 !important;
    }

    .btn.col-giucho {
        color: #fff !important;
        background-color: #7f5748 !important;
    }
    .btn.col-giucho:hover {
        background-color: #823b20 !important;
    }

    .btn.col-dadat {
        color: #fff !important;
        background-color: #558290 !important;
    }
    .btn.col-dadat:hover {
        background-color: #167795 !important;
    }

    .btn.col-lenxe {
        color: #fff !important;
        background-color: #54a254 !important;
    }
    .btn.col-lenxe:hover {
        background-color: #16a716 !important;
    }

</style>
<div class="khung_lich_ve" id="width_screen">
    <div class="innerLR">
        <div class="row-fluid">
            <div id="panel-sell">
                <div class="widget-body">
                    <div class="tab-content">

                        {{--tab bán vé--}}
                        <div class="tab-pane active" id="BanVeChoHanhKhach">
                            <div class="row-fluid bg_light p_bottom_20">
                                <div class="span12">
                                    <div id="loading">
                                        <img src="/public/images/loading/loader_blue.gif" alt="">
                                    </div>
                                    <div class="widget widget-2 widget-tabs widget-tabs-2 no_border">
                                        <div class="widget-body">
                                            <div style="background: #525050" class="box-route-and-trip">
                                                <div class="box-all-trip">
                                                    <div class="span2 box-route" style="padding-top:7px;padding-left:5px;"></div>
                                                    <div class="span10 box-trip"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="row-fluid tripInfo">
                                                <table class="table" width="100%">
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="row-fluid" id="table_SaoNghe" style="overflow:hidden">
                                                <ul class="nav nav-pills navtabtool floor" style="margin-bottom:-10px;">
                                                    <li class="active text-center" style="width:50%;font-weight:bold;">
                                                        <a href="#tang1" class="toggletabtang1" data-toggle="tab"
                                                           style="margin-bottom:0px;border-radius:0px">Tầng 1</a>
                                                    </li>
                                                    <li class="text-center" style="width:50%;font-weight:bold;"><a
                                                                href="#tang2"
                                                                style="margin-bottom:0px;border-radius:0px"
                                                                data-toggle="tab">Tầng 2</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content SnTt clearfix" style="background: #e1e1e1;">
                                                    <div class="tab-pane active">
                                                        <div id="SnTt" style="width:100%">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-content clearfix">
                                                    <div class="tab-pane active" id="tang1">
                                                        <div class="khungxe" style="width:100%">
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tang2">
                                                        <div class="khungxe" style="width:100%">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="ghichu_datghe">
                                                    <div class="gc_ghe gc_giucho"></div>
                                                    <div class="title_ghichu_datghe">Ghế giữ chỗ</div>
                                                    <div class="gc_ghe gc_dachon"></div>
                                                    <div class="title_ghichu_datghe">Ghế đang chọn</div>
                                                    <div class="gc_ghe gc_dadat"></div>
                                                    <div class="title_ghichu_datghe">Ghế đã đặt</div>
                                                    <div class="gc_ghe gc_trong"></div>
                                                    <div class="title_ghichu_datghe">Ghế trống</div>
                                                    <div class="gc_ghe gc_ghelenxe"></div>
                                                    <div class="title_ghichu_datghe">Ghế đã lên xe</div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div id="gheThemKhongThuocSeatMap" style="display: none">
                                                    <h4>Bán thêm ghế / Hàng hóa<button class="btn btn-inprimary addFreeSeat"><i class="fa fa-plus-circle"></i></button></h4>
                                                    <div class="khungxe">
                                                    </div>
                                                    <div class="listAdditionTickets" style="padding-bottom: 10px">
                                                        <table class="table">
                                                            <caption><h3>Danh Sách ghế bán thêm</h3></caption>
                                                            <tr>
                                                                <td>stt</td>
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <a href="#" class="printSecondaryTickets"><i class="fa fa-print" ></i> In danh sách ghế bán thêm</a>
                                                </div>
                                                <div class="row-fuild" style="margin-top: 10px;" id="updateNoteTrip">
                                                    <div class="span10" style="background:#fff">
                                                        <textarea class="updateNoteTrip" style="width:100%; background:#f1eaae" rows="1" placeholder="Nhập ghi chú"></textarea>
                                                    </div>
                                                    <div class="span2">
                                                        <button class="btn" id="doUpdateNoteTrip" style="width: -webkit-fill-available;">Lưu ghi chú chuyến</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="text-center">
                                        <button type="button" class="btn btn-default danhsachve btnDanhSachVeHuy">Xem danh sách vé hủy</button>
                                        <button type="button" class="btn btn-default danhsachve btnDanhSachVeHetHan">Xem danh sách vé hết hạn</button>
                                        <button type="button" class="btn btn-default danhsachve btnDanhSachVeXuongXe">Vé đã xuống xe</button>
                                        <button type="button" class="btn btn-default danhsachve btnDanhSachHangTrenXe">Xem danh sách đồ</button>
                                        <button type="button" class="btn btn-default danhsachve btnThuChiChuyen">Thống kê chuyến</button>
                                        <button type="button" class="btn btn-default printDanhSachVe">IN</button>
                                    </div>
                                    <div id="listTicketCancel">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.addFreeSeat').click(function () {
            seatHTML = '<div data-ticketId="undefined" data-extraPrice="0" class="ghe ghetrong seatFreeSeatMap">';//seatFreeSeatMap để check không khóa ghế
            seatHTML += '<div class="seatTitle">';
            seatHTML += '<div class="seatId seatIdInfo-1">A-1</div>';
            if (tripSelected.tripData.tripStatus != 2) {
                seatHTML += '<div class="sellTicket">' +
                    '<button type="button" title="Xóa Ghế" class="btnRemoveSeatFree btn btn-primary"><i class="fas fa-trash"></i></button> ' +
                    '<button type="button" data-toggle="modal" data-target="#sell_ticket" title="Đặt vé thường" class="btnSellTicket btn btn-primary"><i class="fas fa-plus"></i></button> ' +
                    '</div>';
            }
            seatHTML += '</div>';
            seatHTML += '</div>';
            $('#gheThemKhongThuocSeatMap').find('.khungxe').append(seatHTML);
        });
        $('body').on('click','.btnRemoveSeatFree',function () {
            $(this).parents('.ghe').remove();
        });
        $('body').on('click','.printSecondaryTickets',function () {
            $('#gheThemKhongThuocSeatMap .listAdditionTickets').printThis();
            return false;
        })
    })
</script>