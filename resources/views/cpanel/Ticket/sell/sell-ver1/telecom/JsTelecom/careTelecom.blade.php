{{--<script src="https://capi.caresoft.vn/js/embed/jquery.min.js" type="text/javascript"></script>--}}
<script src="https://capi.caresoft.vn/js/embed/jssip-3.0.7.js" type="text/javascript"></script>
<script src="https://capi.caresoft.vn/js/embed/web.push.js" type="text/javascript"></script>
<script src="https://capi.caresoft.vn/js/embed/cs_const.js" type="text/javascript"></script>
{{--<script src="https://capi.caresoft.vn/js/embed/cs_voice.js" type="text/javascript"></script>--}}

<script type="text/javascript" src="{{asset('public/javascript/careTelecom/cs_voice.js')}}"></script>
<script type="text/javascript" src="{{asset('public/javascript/careTelecom/core.js')}}"></script>
<script type="text/javascript" src="{{asset('public/javascript/careTelecom/hmac.js')}}"></script>
<script type="text/javascript" src="{{asset('public/javascript/careTelecom/sha256.js')}}"></script>
<script type="text/javascript" src="{{asset('public/javascript/careTelecom/enc-base64.js')}}"></script>
<script type="text/javascript">
    // params  :
    //domain : tên miền lấy  lịch sử
    // secret :
    //  phone number :
    // contextName:
    // companyCode : mã của khách hàng vs tổng đài
    function webSocketInit()
    {
        var data = {
            "ipphone": telecomConfig.telecomInfo.number
        };
        var header = {
            "alg": "HS256",
            "typ": "JWT"
        };
        var stringifiedHeader = CryptoJS.enc.Utf8.parse(JSON.stringify(header));
        var encodedHeader = base64url(stringifiedHeader);

        var stringifiedData = CryptoJS.enc.Utf8.parse(JSON.stringify(data));
        var encodedData = base64url(stringifiedData);

        var token = encodedHeader + "." + encodedData;
        var secret = telecomConfig.telecomInfo.telecomConfig.secret;//"7s564qDQgQtW-Eg";

        var signature = CryptoJS.HmacSHA256(token, secret);
        signature = base64url(signature);

        var signedToken = token + "." + signature;
        console.log("signedToken",signedToken);
        login(signedToken);
        //initCall(signedToken);

    }

    function login(signedToken)
    {

        $.ajax({
            type: 'POST',
            url: 'https://capi.caresoft.vn/'+telecomConfig.telecomInfo.telecomConfig.companyCode+'/thirdParty/login',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: {
                token : signedToken,
            },
            /**
             * @param response {"status":1,"message":"成功"}
             */
            success: function (response) {
                console.log('response: '+JSON.stringify(response));
                initCall(signedToken);

            },
            error: function (error) {
                console.log("error ",error)
            }
        })
    }
    function base64Encoded(source) {
        var wordArray = CryptoJS.enc.Utf8.parse(source);
        var result = CryptoJS.enc.Base64.stringify(wordArray);
        return result;
    }
    function base64Decoded(source) {
        var wordArray = CryptoJS.enc.Utf8.parse(source);
        var result = wordArray.toString(CryptoJS.enc.Utf8);
        return result;
    }
    function base64url(source) {
        // Encode in classical base64
        encodedSource = CryptoJS.enc.Base64.stringify(source);

        // Remove padding equal characters
        encodedSource = encodedSource.replace(/=+$/, '');

        // Replace characters according to base64url specifications
        encodedSource = encodedSource.replace(/\+/g, '-');
        encodedSource = encodedSource.replace(/\//g, '_');

        return encodedSource;
    }
    function initCall(signedToken)
    {
        csInit(signedToken,telecomConfig.telecomInfo.telecomConfig.companyCode);//csShowDeviceType."vantai"
        //createWs(signedToken);
        csShowDeviceType = function(phone)
        {
            console.log("csShowDeviceType",phone);
            output.innerHTML = "csShowDeviceType " + phone + "<br/>" +  output.innerHTML;
        }
        csShowEnableVoice = function(phone)
        {
            console.log("csShowEnableVoice",phone);
            output.innerHTML = "csShowEnableVoice " + phone + "<br/>" +  output.innerHTML;
        }
        csShowCallStatus = function(phone)
        {
            changeDevice(4);
            csEnableCall();
        }
        csCallRinging = function(phone)
        {
            console.log("csCallRinging",phone);
            ringingCallIn(phone);
        }
        csCurrentCallId = function(phone)
        {
            console.log("csCurrentCallId",phone);
            output.innerHTML = "csCurrentCallId " + phone + "<br/>" +  output.innerHTML;
        }
        csCustomerAccept = function()
        {
            console.log("csCustomerAccept");
            listeningCallOut();
        }
        csEndCall = function()
        {
            console.log("csEndCall");
            endCallIn("");
            endCallOut('');
        }
        showCalloutInfo = function(phone)
        {
            console.log("phone",phone);
            ringingCallOut(phone);
        }
        csAcceptCall = function()
        {
            console.log("Đang nghe ",1);
            listeningCallIn("");
        }
    }

    function getHistoryTelecomCall(type)
    {
        dateStartSend = getCurentDate;
        dateEndSend = dateStartSend;
        if($('#txtStartDateTongDai').val()!==''&&$('#txtStartTimeTongDai').val()!==''){
            dateStartSend = $('#txtStartDateTongDai').val()+'T'+$('#txtStartTimeTongDai').val()+':00';
        }
        if($('#txtEndDateTongDai').val()!==''&&$('#txtEndTimeTongDai').val()!==''){
            dateEndSend = $('#txtEndDateTongDai').val()+'T'+$('#txtEndTimeTongDai').val()+':00';
        }
        if (typeof dateStart !== 'undefined') {
            dateStartSend = dateStart;
        }
        if (typeof dateEnd !== 'undefined') {
            dateEndSend = dateEnd;
        }
        if(new Date(dateEndSend).getTime() > new Date().getTime()){
            notyMessage("Thời gian kết thúc không được quá giờ hiện tại","error");
        }
        $('#history-calling').html('Đang tải dữ liệu ... ');

        var typeSend = 0;
        if(type === "goidi"){
            typeSend=1;
        }
        sdt = $('#txtPhoneNumberTongDai').val();

        $('#history-calling').html('Đang tải dữ liệu ... ');
        $.ajax({
            type: 'GET',
            url: 'https://api.caresoft.vn/'+telecomConfig.telecomInfo.telecomConfig.companyCode+'/api/v1/calls',//phone number :0948710444
            beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer '+telecomConfig.telecomInfo.telecomConfig.secret); },
            /**
             * @param response {"status":1,"message":"成功"}
             */
            type : "get",
            data : {
                page : 1,
                count :500,
                start_time_since : dateStartSend,
                start_time_to : dateEndSend,
                call_type :typeSend,
            },
            success: function (response) {
                renderListCall(response.calls,type);
            },
            error: function (error) {
                console.log("error ",error)
            }
        })
    }
    function sendCallOut() {//goi ra ngoài
        phoneNumber = $('#txtPhoneNumberCallTelecom').val();
        phoneNumber = phoneNumber.replace(/[^0-9]/gi, '');
        $('#txtPhoneNumberCallTelecom').val(phoneNumber);
        if (phoneNumber.toString().length > 11 || phoneNumber.toString().length < 10) {
            notyMessage("Số điện thoại không đúng định dạng", "error");
            return false;
        }
        csCallout(phoneNumber);
    }
    function sendEndCallIn() {
        endCall();
    }
    function sendEndCallOut() {
        console.log('endcallout');
        endCall();
    }
    function renderListCall(listCall, type) {
        somay = $('#txtSoMayLe').val();
        sdt = $('#txtPhoneNumberTongDai').val();
        html = '';
        html += '<ul class="list-group">';
        $.each(listCall, function (i, e) {
            if(e.caller.toString().indexOf(sdt)===-1){
                return;
            }
            switch (type) {
                case 'goidennho':
                    if(e.call_type===0&&e.call_status==='miss'){
                        html += '<li class="list-group-item">';
                        html += '<span style="color:#942a25"><i class="fas fa-times"></i></span> &nbsp;';
                        html += '<span class="phone">' + e.agent_id + '</span> -> <span>'+e.caller+'</span><br>';
                        html += '<span class="time">' + e.start_time + '</span><br>';
                        html += '</li>';
                    }
                    break;
                case 'goidenthanhcong':
                    if(e.call_type===0&&e.call_status==='meetAgent'){
                        html += '<li class="list-group-item">';
                        html += '<span style="color:green"><i class="fas fa-check"></i></span> &nbsp;';
                        html += '<span class="phone">' + e.agent_id + '</span> -> <span>'+e.caller+'</span><br>';
                        html += '<span class="time">' + e.start_time + '</span><br>';
                        html += '<audio controls style="width:-webkit-fill-available">' +
                            '<source src="'+e.path+'" type="audio/ogg">' +
                            '<source src="'+e.path+'" type="audio/mpeg">' +
                            'Your browser does not support the audio element.' +
                            '</audio>';
                        html += '</li>';
                    }
                    break;

                case 'goidi' :
                    html += '<li class="list-group-item">';
                    if(e.call_status === "miss"){
                        html += '<span style="color:#942a25"><i class="fas fa-times"></i></span> &nbsp;';
                    }else{
                        html += '<span style="color:green"><i class="fas fa-check"></i></span> &nbsp;';
                    }
                    html += '<span>' + e.agent_id + '</span> -> <span class="phone">'+e.caller+'</span><br>';
                    html += '<span class="time">' + e.start_time + '</span><br>';
                    if(e.call_status !== "miss"){
                        html += '<audio controls style="width:-webkit-fill-available">' +
                            '<source src="'+e.path+'" type="audio/ogg">' +
                            '<source src="'+e.path+'" type="audio/mpeg">' +
                            'Your browser does not support the audio element.' +
                            '</audio>';
                    }
                    html += '</li>';
                    break;

                default :
                    html += '';
            }
        });
        html += '</ul>';
        $('#history-calling').html(html);
    }
    $(window).bind('unload', function () {
        csUnregister();
        if (csVoice.enableVoice) {
            reConfigDeviceType();
        }
    });
</script>
