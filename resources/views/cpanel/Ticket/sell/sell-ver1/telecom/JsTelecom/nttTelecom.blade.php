<script>
    function webSocketInit() {
//        telecomConfig.telecomInfo.telecomConfig = {
//            urlConnectWebsocket : 'https://sip87-7.cloudpbx.vn:8083',
//            urlReport : "https://sip87-7.cloudpbx.vn/api/api_report_cdr.php",
//            organization : "phucxuyen.cloudpbx.vn",
//            token :"rzaUiedmsGPoUvm4voCXqccKZTwCHlH5vx9K2YIwz18",
//        };
        if (typeof webSocket !== "undefined" && webSocket !== null) {
            webSocket.emit("disconnect");
            webSocket.disconnect();
        }

//        if (typeof  localStorage.getItem('soMayLe') !== 'undefined' && localStorage.getItem('soMayLe') !== '') {
//            $('#txtSoMayLe').val(localStorage.getItem('soMayLe'));
//        }

        somay = $('#txtSoMayLe').val();

        if ("WebSocket" in window) {
            if (typeof somay !== 'undefined' && somay !== '') {
                // Let us open a web socket https://sip87-7.cloudpbx.vn
                webSocket = io.connect(telecomConfig.telecomInfo.telecomConfig.urlConnectWebsocket);//https://sip87-7.cloudpbx.vn:8083
                webSocket.on('connect', function (data) {
//                            console.log("connect",somay);

                    var data = {
                        organization: telecomConfig.telecomInfo.telecomConfig.organization,//
                        token: telecomConfig.telecomInfo.telecomConfig.token,//
                        extension: somay//
                    };
                    webSocket.emit('authen', data);
                });
                webSocket.on('disconnect', function (data) {
                    console.log("check connection :" + data);
                });

                webSocket.on("message", function (data) {
                    console.log(data);
//                  nếu đang nghe thì chỉ nhận sự kiện kết thúc
                    if(typeof telecomCallingInfo.status!=="undefined"&&(telecomCallingInfo.status ===18||telecomCallingInfo.status === 28)&&data.code!=='320'&&data.code!=='322'&&data.code!=='328'&&data.code!=='330'){
                        return false;
                    }
                    if (data.code === "316") {
                        ringingCallIn(data.data['phone_number']);
                    }
                    // đang nghe
                    if (data.code === "318") {//||data.code === "326"
                        listeningCallIn();
                    }
                    //cuộc gọi đến kết thúc hoặc không được tiếp nhận
                    if (data.code === "320" || data.code === "322") {
                        endCallIn(data.data['phone_number']);
                    }
                    //cuộc gọi đi
                    if (data.code === "314") {
                        ringingCallOut(data.data['phone_number']||'')
                    }
//                            sự kiện khi keest thuc cuộc gọi
                    if (data.code === "330" || data.code === '328') {
                        endCallOut(data.data['phone_number']||'');
                    }
                    if(data.code==="356"&&data.data.status==="DEVICE_NOT_INUSE"&&data.data.extension === somay){
                        endCallOut(data.data['phone_number']||'');
                    }
//                        console.log("Received: "+ JSON.stringify(data));
                });
            }

        } else {
            // The browser doesn't support WebSocket
            alert("WebSocket NOT supported by your Browser!");
        }
    }

    function sendCallOut() {
        phoneNumber = $('#txtPhoneNumberCallTelecom').val();
        phoneNumber = phoneNumber.replace(/[^0-9]/gi, '');
        $('#txtPhoneNumberCallTelecom').val(phoneNumber);
        if (phoneNumber.toString().length > 11 || phoneNumber.toString().length < 10) {
            notyMessage("Số điện thoại không đúng định dạng", "error");
            return false;
        }
        webSocket.emit('click2call', {phone_number: phoneNumber});
    }
    function sendEndCallIn() {
        somay = $('#txtSoMayLe').val();
        webSocket.emit('hangup', {extension: somay, organization: telecomConfig.telecomInfo.telecomConfig.organization});
    }
    function sendEndCallOut() {
        console.log("send-End call out");
        somay = $('#txtSoMayLe').val();
        webSocket.emit('hangup', {extension: somay});
        $('#box-phone-fixed-left').find('.click-to-call-out').show();
        $('#box-phone-fixed-left').find('.click-to-end-call-out').hide();
    }
    function getHistoryTelecomCall(type) {
        dateStartSend = getCurentDate;
        dateEndSend = dateStartSend;
        if($('#txtStartDateTongDai').val()!==''&&$('#txtStartTimeTongDai').val()!==''){
            dateStartSend = $('#txtStartDateTongDai').val()+' '+$('#txtStartTimeTongDai').val()+':00';
        }
        if($('#txtEndDateTongDai').val()!==''&&$('#txtEndTimeTongDai').val()!==''){
            dateEndSend = $('#txtEndDateTongDai').val()+' '+$('#txtEndTimeTongDai').val()+':00';
        }
        if (typeof dateStart !== 'undefined') {
            dateStartSend = dateStart;
        }
        if (typeof dateEnd !== 'undefined') {
            dateEndSend = dateEnd;
        }
        $('#history-calling').html('Đang tải dữ liệu ... ');

        var typeSend = 1;
        if(type === "goidi"){
            typeSend=0;
        }

        $.ajax({
            url: '{{ action('TicketController@getHistoryCall') }}',
            type: "GET",
            data: {
                token:telecomConfig.telecomInfo.telecomConfig.token,
                organization:telecomConfig.telecomInfo.telecomConfig.organization,
                start_date: dateStartSend,
                end_date: dateEndSend,
                type: typeSend,
            },
            dataType:"json",
            error: function (xhr, status, error) {
                $('#history-calling').html('Lỗi!');
                console.log(status + '; ' + error);
            },
            success: function (data) {
//                    console.log(data);
                if(data.result==='failed'){
                    $('#history-calling').html('Không có dữ liệu');
                }
                if (typeof data.message === 'undefined') {
                    renderListCall(data[0].data, type);
                }
            }

        });
    }
    function renderListCall(listCall, type) {
        somay = $('#txtSoMayLe').val();
        sdt = $('#txtPhoneNumberTongDai').val();
        html = '';
        html += '<ul class="list-group">';
        $.each(listCall, function (i, e) {
            if(e.number_phone.toString().indexOf(sdt)===-1){
                return;
            }
            switch (type) {
                case 'goidennho':
                    if (parseInt(e.talk_time) === 0) {
                        html += '<li class="list-group-item">';
                        html += '<span style="color:#942a25"><i class="fas fa-times"></i></span> &nbsp;';
                        html += '<span class="phone">' + e.number_phone + '</span><br>';
                        html += '<span class="time">' + e.date_time + '</span><br>';
                        html += '</li>';
                    }
                    break;
                case 'goidenthanhcong':
                    if (parseInt(e.talk_time) > 0) {
                        html += '<li class="list-group-item">';
                        html += '<span style="color:green"><i class="fas fa-check"></i></span> &nbsp;';
                        html += '<span class="phone">' + e.number_phone + '</span><a href="' + e.recordingfile + '" target="_blank" class="btn btn-warning btn-mini pull-right" title="Nghe lại : ' + e.talk_time + ' Giây tới số '+e.extension+'"><i class="fas fa-headphones"></i></a><br>';
                        html += '<span class="time">' + e.date_time + '</span><br>';
                        html += '</li>';
                    }
                    break;

                case 'goidi' :
                        html += '<li class="list-group-item">';
                        html += '<span style="color:green"><i class="fas fa-check"></i></span> &nbsp;';
                        html += '<span class="phone">' + e.number_phone + '</span><a href="' + e.recordingfile + '" target="_blank" class="btn btn-warning btn-mini pull-right"><i class="fas fa-headphones"></i></a><br>';
                        html += '<span class="time">' + e.date_time + '</span><br>';
                        html += '</li>';
                    break;

                default :
                    html += '';
            }
        });
        html += '</ul>';
        $('#history-calling').html(html);
    }
</script>