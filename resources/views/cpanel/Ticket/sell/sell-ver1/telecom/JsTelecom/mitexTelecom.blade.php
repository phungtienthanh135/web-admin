<script type="text/javascript">
    var fbwsincount = 0;
    function webSocketInit() {
        // thông tin cáu hình
        // "{url : 'https:// api-popupcontact-02.mipbx.vn:5443 /api/v1/',secret : 'a1e9c38b4bdbc3daca1cc726707e602f'}"
        if ( webSocket!==null && typeof webSocket.path != 'undefined') {
            webSocket.off(); // thực ch ất là firebase .. websocket chỉ là tên biến
        }
        var somay = $('#txtSoMayLe').val();
        if(somay === 'none'){
            return false;
        }
        webSocket = database.ref().child('call_log').child('mipbx').child(somay);
        webSocket.on('value', function (snapshot) {
            var data = snapshot.val();
            console.log(data);
            if (fbwsincount>0){
                switch (data.event){
                    case 'ringing' :
                        if(data.value.extension !== data.value.fromnumber){//gọi vào
                            console.log('ringingCallIn');
                            ringingCallIn(data.value.fromnumber);
                        }else {
                            console.log('ringingCallout');
                            ringingCallOut(data.value.tonumber);
                        }
                        break;
                    case 'answered' :
                        if(data.value.extension !== data.value.fromnumber){//gọi vào
                            listeningCallIn(data.value.fromnumber);
                        }else {
                            listeningCallOut(data.value.tonumber);
                        }
                        break;
                    case 'hangup' :
                        if(data.value.extension !== data.value.fromnumber){//gọi vào
                            endCallIn(data.value.fromnumber);
                        }else {
                            endCallOut(data.value.tonumber);
                        }
                        break;
                    default :
                        if(data.value.extension !== data.value.fromnumber){//gọi vào
                            endCallIn(data.value.fromnumber);
                        }else {
                            endCallOut(data.value.tonumber);
                        }
                        break;
                }
            }
            fbwsincount++;
        });
    }
    function sendCallOut() {
        var somay = $('#txtSoMayLe').val();
        phoneNumber = $('#txtPhoneNumberCallTelecom').val();
        phoneNumber = phoneNumber.replace(/[^0-9]/gi, '');
        $('#txtPhoneNumberCallTelecom').val(phoneNumber);
        if (phoneNumber.toString().length > 11 || phoneNumber.toString().length < 10) {
            notyMessage("Số điện thoại không đúng định dạng", "error");
            return false;
        }
        $.ajax({
            url : 'https://api-popupcontact-02.mipbx.vn:5443/api/v1/call/clicktocall',
            type : "get",
            dataType : 'json',
            data : {
                secret : telecomConfig.telecomInfo.telecomConfig.secret,
                extension : somay,
                phone: phoneNumber,
            },
            success : function (res) {
                notyMessage("Gọi ra","success");
                console.log(res);
            },
            error : function (res) {
                notyMessage("gọi ra thất bại","error");
            }
        })
    }
    function sendEndCallIn() {
        var somay = $('#txtSoMayLe').val();
    }
    function sendEndCallOut() {
        console.log("send-End call out");
        var somay = $('#txtSoMayLe').val();
        $('#box-phone-fixed-left').find('.click-to-call-out').show();
        $('#box-phone-fixed-left').find('.click-to-end-call-out').hide();
    }
    function getHistoryTelecomCall(type) {
        dateStartSend = getCurentDate;
        dateEndSend = dateStartSend;
        if($('#txtStartDateTongDai').val()!==''&&$('#txtStartTimeTongDai').val()!==''){
            dateStartSend = $('#txtStartDateTongDai').val()+' '+$('#txtStartTimeTongDai').val()+':00';
        }
        if($('#txtEndDateTongDai').val()!==''&&$('#txtEndTimeTongDai').val()!==''){
            dateEndSend = $('#txtEndDateTongDai').val()+' '+$('#txtEndTimeTongDai').val()+':00';
        }
        if (typeof dateStart !== 'undefined') {
            dateStartSend = dateStart;
        }
        if (typeof dateEnd !== 'undefined') {
            dateEndSend = dateEnd;
        }
        $('#history-calling').html('Đang tải dữ liệu ... ');
        var somay = $('#txtSoMayLe').val();

        $.ajax({
            //http://124.158.15.41:1101/webservice/getcallslog?contextName=agentqueue&fromDate=2018-07-30&toDate=2018-08-04&expType=CSV&expmode=all
            url: 'https://api-popupcontact-02.mipbx.vn:5443/api/v1/cdr/getCallsLog ',
            type: "post",
            data: {
                secret : telecomConfig.telecomInfo.telecomConfig.secret,
                startDate: dateStartSend,
                endDate: dateEndSend,
                extensions : [somay]
            },
            error: function (xhr, status, error) {
                $('#history-calling').html('Lỗi!');
                console.log(status + '; ' + error);
            },
            success: function (data) {
                renderListCall(data,type);
            }

        });
    }

    function renderListCall(listCall, type) {
        somay = $('#txtSoMayLe').val();
        sdt = $('#txtPhoneNumberTongDai').val();
        html = '';
        html += '<ul class="list-group">';
        $.each(listCall, function (i, e) {
            var p = somay===e.dst ? 'src':'dst';

            if(e[p].toString().indexOf(sdt)===-1){
                return;
            }
            switch (type) {
                case 'goidennho':
                    if(somay===e.dst&&e.callStatus ==="NO ANSWER"){
                        html += '<li class="list-group-item">';
                        html += '<span style="color:#942a25"><i class="fas fa-times"></i></span> &nbsp;';
                        html += '<span class="phone">' + e.src + '</span> -> <span>'+e.dst+'</span><br>';
                        html += '<span class="time">' + e.callStartTime + '</span><br>';
                        html += '</li>';
                    }
                    break;
                case 'goidenthanhcong':
                    if(somay===e.dst&&e.callStatus ==="ANSWERED"){
                        html += '<li class="list-group-item" title="'+e.billSec+' Giây">';
                        html += '<span style="color:green"><i class="fas fa-check"></i></span> &nbsp;';
                        html += '<span class="phone">' + e.src + '</span> -> <span>'+e.dst+'</span><a href="' + e.recFile + '" target="_blank" class="btn btn-warning btn-mini pull-right" title="Nghe lại ghi âm"><i class="fas fa-headphones"></i></a><br>';
                        html += '<span class="time">' + e.callStartTime + '</span><br>';
//                        html += '<audio controls style="width:-webkit-fill-available">' +
//                            '<source src="'+e.path+'" type="audio/ogg">' +
//                            '<source src="'+e.path+'" type="audio/mpeg">' +
//                            'Your browser does not support the audio element.' +
//                            '</audio>';
                        html += '</li>';
                    }
                    break;

                case 'goidi' :
                    if(somay===e.src){
                        html += '<li class="list-group-item" title="'+e.billSec+' Giây">';
                        if(e.callStatus !== "ANSWERED"){
                            html += '<span style="color:#942a25"><i class="fas fa-times"></i></span> &nbsp;';
                        }else{
                            html += '<span style="color:green"><i class="fas fa-check"></i></span> &nbsp;';
                        }
                        html += '<span>' + e.src + '</span> -> <span class="phone">'+e.dst+'</span><a href="' + e.recFile + '" target="_blank" class="btn btn-warning btn-mini pull-right" title="Nghe lại ghi âm"><i class="fas fa-headphones"></i></a><br>';
                        html += '<span class="time">' + e.callStartTime + '</span><br>';
//                    if(e.call_status !== "miss"){
//                        html += '<audio controls style="width:-webkit-fill-available">' +
//                            '<source src="'+e.path+'" type="audio/ogg">' +
//                            '<source src="'+e.path+'" type="audio/mpeg">' +
//                            'Your browser does not support the audio element.' +
//                            '</audio>';
//                    }
                        html += '</li>';
                    }
                    break;

                default :
                    html += '';
            }
        });
        html += '</ul>';
        $('#history-calling').html(html);
    }
</script>
