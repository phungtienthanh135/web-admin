<script src="https://web.vcc-vinaphone.com.vn/js/embed/jssip-3.0.7.js"></script>
<script src="https://web.vcc-vinaphone.com.vn/js/embed/web.push.js"></script>
<script src="https://web.vcc-vinaphone.com.vn/js/embed/cs_const.js"></script>
<script type="text/javascript" src="{{asset('public/javascript/vccTelecom/core.js')}}"></script>
<script type="text/javascript" src="{{asset('public/javascript/vccTelecom/hmac.js')}}"></script>
<script type="text/javascript" src="{{asset('public/javascript/vccTelecom/sha256.js')}}"></script>
<script type="text/javascript" src="{{asset('public/javascript/vccTelecom/enc-base64.js')}}"></script>
<script type="text/javascript" src="{{asset('public/javascript/vccTelecom/cs_voice.js')}}"></script>
<script type="text/javascript">
    function webSocketInit() {
//        console.log("init");
//        telecomConfig.telecomInfo.telecomConfig = {
//            phoneNumber :"0889033777",
//            secretkey : 'Q9Abbbe4A0LPVmc'
//        };
        // vcc cần phoneNumber và secretkey
        // open the connection if one does not exist
        var data = {
            "ipphone": telecomConfig.telecomInfo.number

        };
        var header = {
            "alg": "HS256",
            "typ": "JWT"
        };
        var stringifiedHeader = CryptoJS.enc.Utf8.parse(JSON.stringify(header));
        var encodedHeader = base64url(stringifiedHeader);

        var stringifiedData = CryptoJS.enc.Utf8.parse(JSON.stringify(data));
        var encodedData = base64url(stringifiedData);

        var token = encodedHeader + "." + encodedData;
        var secret = telecomConfig.telecomInfo.telecomConfig.secretKey;//"MKstYfGNjvFtq_A"

        var signature = CryptoJS.HmacSHA256(token, secret);
        signature = base64url(signature);

        var signedToken = token + "." + signature;
        console.log("signedToken",signedToken);
        login(signedToken);
    }
    function login(signedToken)
    {
        $.ajax({
            type: 'POST',
            url: 'https://web.vcc-vinaphone.com.vn/'+telecomConfig.telecomInfo.telecomConfig.phoneNumber+'/thirdParty/login',//0948710444
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: {
                token : signedToken,
            },
            /**
             * @param response {"status":1,"message":"成功"}
             */
            success: function (response) {
                initCall(signedToken);
                console.log("loged");
            },
            error: function (error) {
                notyMessage("Lỗi kết nối tổng đài","eror");
                console.log("error ",error);
            }
        })
    }

    function base64Encoded(source) {
        var wordArray = CryptoJS.enc.Utf8.parse(source);
        var result = CryptoJS.enc.Base64.stringify(wordArray);
        return result;
    }
    function base64Decoded(source) {
        var wordArray = CryptoJS.enc.Utf8.parse(source);
        var result = wordArray.toString(CryptoJS.enc.Utf8);
        return result;
    }
    function base64url(source) {
        // Encode in classical base64
        encodedSource = CryptoJS.enc.Base64.stringify(source);

        // Remove padding equal characters
        encodedSource = encodedSource.replace(/=+$/, '');

        // Replace characters according to base64url specifications
        encodedSource = encodedSource.replace(/\+/g, '-');
        encodedSource = encodedSource.replace(/\//g, '_');

        return encodedSource;
    }

    function initCall(signedToken)
    {
        csInit(signedToken,telecomConfig.telecomInfo.telecomConfig.phoneNumber);//csShowDeviceType
        csShowDeviceType = function(phone)
        {
            console.log("csShowDeviceType",phone);
        };
        csShowEnableVoice = function(phone)
        {
            console.log("csShowEnableVoice",phone);

            reConfigDeviceType();
        };
        csShowCallStatus = function(phone)
        {
            console.log("csShowCallStatus",phone);
            //
            changeDevice(4);
            csEnableCall();
        };
        csCallRinging = function(phone)
        {
            console.log("csCallRinging",phone);
            ringingCallIn(phone);
        };
        csCurrentCallId = function(phone)
        {
            reConfigDeviceType();
            console.log("csCurrentCallId",phone);
        };
        csCustomerAccept = function()
        {
            console.log("csCustomerAccept");
            listeningCallOut();
        };
        csEndCall = function()
        {
            console.log("csEndCall");
            endCallIn("");
            endCallOut('');
        };
        showCalloutInfo = function(phone)
        {
            console.log("phone",phone);
            ringingCallOut(phone);
        };
        csAcceptCall = function()
        {
            console.log("Đang nghe ",1);
            listeningCallIn("");
        }
    }

    function sendCallOut() {//goi ra ngoài
        var phoneNumber = $('#txtPhoneNumberCallTelecom').val();
        phoneNumber = phoneNumber.replace(/[^0-9]/gi, '');
        $('#txtPhoneNumberCallTelecom').val(phoneNumber);
        if (phoneNumber.toString().length > 11 || phoneNumber.toString().length < 10) {
            notyMessage("Số điện thoại không đúng định dạng", "error");
            return false;
        }
        csCallout(phoneNumber);
    }
    function sendEndCallIn() {
        endCall();
    }
    function sendEndCallOut() {
        endCall();
    }
    function getHistoryTelecomCall(type) {
        dateStartSend = getCurentDate;
        dateEndSend = dateStartSend;
        if($('#txtStartDateTongDai').val()!==''&&$('#txtStartTimeTongDai').val()!==''){
            dateStartSend = $('#txtStartDateTongDai').val()+'T'+$('#txtStartTimeTongDai').val()+':00';
        }
        if($('#txtEndDateTongDai').val()!==''&&$('#txtEndTimeTongDai').val()!==''){
            dateEndSend = $('#txtEndDateTongDai').val()+'T'+$('#txtEndTimeTongDai').val()+':00';
        }
        if (typeof dateStart !== 'undefined') {
            dateStartSend = dateStart;
        }
        if (typeof dateEnd !== 'undefined') {
            dateEndSend = dateEnd;
        }
        if(new Date(dateEndSend).getTime() > new Date().getTime()){
            notyMessage("Thời gian kết thúc không được quá giờ hiện tại","error");
        }
        $('#history-calling').html('Đang tải dữ liệu ... ');

        var typeSend = 0;
        if(type === "goidi"){
            typeSend=1;
        }
        sdt = $('#txtPhoneNumberTongDai').val();

        $('#history-calling').html('Đang tải dữ liệu ... ');
        $.ajax({
            type: 'GET',
            url: 'https://vcc-web1.vinaphone.com.vn/'+telecomConfig.telecomInfo.telecomConfig.phoneNumber+'/api/v1/calls',//phone number :0948710444
            beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer '+telecomConfig.telecomInfo.telecomConfig.secretKey); },
            /**
             * @param response {"status":1,"message":"成功"}
             */
            type : "get",
            data : {
                page : 1,
                count :500,
                start_time_since : dateStartSend,
                end_time_since : dateEndSend,
                call_type :typeSend,
            },
            success: function (response) {
                renderListCall(response.calls,type);
            },
            error: function (error) {
                console.log("error ",error)
            }
        })
    }
    function renderListCall(listCall, type) {
        somay = $('#txtSoMayLe').val();
        sdt = $('#txtPhoneNumberTongDai').val();
        html = '';
        html += '<ul class="list-group">';
        $.each(listCall, function (i, e) {
//            if(e.number_phone.toString().indexOf(sdt)===-1){
//                return;
//            }
            switch (type) {
                case 'goidennho':
                    if(e.call_type==="0"&&e.call_status==='miss'){
                        html += '<li class="list-group-item">';
                        html += '<span style="color:#942a25"><i class="fas fa-times"></i></span> &nbsp;';
                        html += '<span class="phone">' + e.agent_id + '</span> -> <span>'+e.caller+'</span><br>';
                        html += '<span class="time">' + e.start_time + '</span><br>';
                        html += '</li>';
                    }
                    break;
                case 'goidenthanhcong':
                    if(e.call_type==="0"&&e.call_status==='meetAgent'){
                        html += '<li class="list-group-item">';
                        html += '<span style="color:green"><i class="fas fa-check"></i></span> &nbsp;';
                        html += '<span class="phone">' + e.agent_id + '</span> -> <span>'+e.caller+'</span><br>';
                        html += '<span class="time">' + e.start_time + '</span><br>';
                        html += '<audio controls style="width:-webkit-fill-available">' +
                            '<source src="'+e.path+'" type="audio/ogg">' +
                            '<source src="'+e.path+'" type="audio/mpeg">' +
                            'Your browser does not support the audio element.' +
                            '</audio>';
                        html += '</li>';
                    }
                    break;

                case 'goidi' :
                    html += '<li class="list-group-item">';
                    if(e.call_status === "miss"){
                        html += '<span style="color:#942a25"><i class="fas fa-times"></i></span> &nbsp;';
                    }else{
                        html += '<span style="color:green"><i class="fas fa-check"></i></span> &nbsp;';
                    }
                    html += '<span>' + e.agent_id + '</span> -> <span class="phone">'+e.caller+'</span><br>';
                    html += '<span class="time">' + e.start_time + '</span><br>';
                    if(e.call_status !== "miss"){
                        html += '<audio controls style="width:-webkit-fill-available">' +
                            '<source src="'+e.path+'" type="audio/ogg">' +
                            '<source src="'+e.path+'" type="audio/mpeg">' +
                            'Your browser does not support the audio element.' +
                            '</audio>';
                    }
                    html += '</li>';
                    break;

                default :
                    html += '';
            }
        });
        html += '</ul>';
        $('#history-calling').html(html);
    }
    $(window).bind('unload', function () {
        csUnregister();
        if (csVoice.enableVoice) {
            reConfigDeviceType();
        }
    });
</script>
<script type="text/javascript" src="{{asset('public/javascript/vccTelecom/cs_voice.js')}}"></script>
