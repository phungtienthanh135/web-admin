<script type="text/javascript" src="{{asset('public/javascript/reconnecting-websocket.js')}}"></script>
<script>
    function webSocketInit() {
//        telecomConfig.telecomInfo.telecomConfig = {
//            urlConnectWebsocket :"ws://14.238.82.116:2104/",
//            urlHttpService  : 'http://14.238.82.116:2101/',
//            contextName : "KHAnVui",
//        };
        // open the connection if one does not exist
        if (webSocket !== undefined && webSocket!==null && webSocket.readyState !== WebSocket.CLOSED) {
            closeSocket();
        }
//        if (typeof  localStorage.getItem('soMayLe') !== 'undefined' && localStorage.getItem('soMayLe') !== '') {
//            $('#txtSoMayLe').val(localStorage.getItem('soMayLe'));
//        }

        somay = $('#txtSoMayLe').val();
        // Create a websocket
        webSocket = new ReconnectingWebSocket(telecomConfig.telecomInfo.telecomConfig.urlConnectWebsocket);
        webSocket.timeoutInterval = 5400;

        webSocket.onopen = function(event) {
            webSocket.send(telecomConfig.telecomInfo.telecomConfig.contextName);
        };

        webSocket.onmessage = function(event) {
            data = JSON.parse(event.data);
            console.log(data);
//            nếu có sự kiện kết nối thành công thì lưu lại token
            if(data.eventName === "ClientConnected"&&data.status === "Success"){
                telecomConfig.telecomInfo.telecomConfig.token = data.token;
            }
            //nếu đang nghe thì chỉ nhận sự kiện kết thúc
            if(typeof telecomCallingInfo.status!=="undefined"&&(telecomCallingInfo.status ===18||telecomCallingInfo.status === 28)&&data.eventName !== "CdrLog"&&data.eventName !=="clicktocallhangup"&&data.eventName !=="clicktocallring"){
                console.log(404);
                return false;
            }
            if(data.eventName === "IncomingCall" && data.dst === somay){ // dst trong lắng nghe là extension tring cuộc gọi
                console.log("goi den");
                ringingCallIn(data.src);
            }

            if(data.eventName === "clicktocallring" && data.src === somay){ // dst trong lắng nghe là extension tring cuộc gọi
                //nghe cuoc goi đi
                console.log("xx");
                ringingCallOut(data.dst);
            }


            if(data.eventName === "ExtensionStatus" &&data.status===2 && data.extension === somay){
                listeningCallIn();
            }
            //nghe cuộc gọi đến
            if(data.eventName==="clicktocallpickup"&&data.src === somay){
                listeningCallOut(data.dst);
            }
            //kết thúc cuộc goi đến
            if(data.eventName ==="CdrLog" && data.dst===somay){
                endCallIn("");
            }
            // kết thúc cuộc gọi đi
            if(data.eventName === "clicktocallhangup" && data.src === somay){
                endCallIn("");
                endCallOut("");
            }

//            if(data.eventName === "CdrLog"){
//                if(data.calltype==="IN"){
//                    if(data.dst === somay){
//                        endCallIn(data.src);
//                    }
//                }else{
//                    if(data.src === somay){
//                        endCallOut(data.dst);
//                    }
//                }
//            }
        };

//        webSocket.onclose = function(event) {
//            setTimeout(webSocketInit, 0);
//        };
    }

    function closeSocket() {
        webSocket.close();
    }

    function sendCallOut() {
        console.log("trianh sendCallOut");
        phoneNumber = $('#txtPhoneNumberCallTelecom').val();
        phoneNumber = phoneNumber.replace(/[^0-9]/gi, '');
        $('#txtPhoneNumberCallTelecom').val(phoneNumber);
        if (phoneNumber.toString().length > 11 || phoneNumber.toString().length < 10) {
            notyMessage("Số điện thoại không đúng định dạng", "error");
            return false;
        }
        somay = $('#txtSoMayLe').val();
        if(somay===""){
            notyMessage("Số máy lẻ chưa được chọn", "error");
            return false;
        }
        $.ajax({
            type: 'POST',
            url: telecomConfig.telecomInfo.telecomConfig.urlHttpService+'webservice/clickToCall',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: {
                username : "tasclick",
                passwd : "4f50d543d71566e3f6f4e744b2eeloteriaeesdfdfd",
                srcTimeOut : 15000,
                src : somay,
                dst : phoneNumber,
                token : telecomConfig.telecomInfo.telecomConfig.token
            },
            /**
             * @param response {"status":1,"message":"æˆåŠŸ"}
             */
            success: function (response) {
                console.log('response: '+response.message)
            },
            error: function (error) {
                console.log("error ",error)
            }
        })
    }
    function sendEndCallIn() {
        console.log("trianh sendEndCallIn");
        somay = $('#txtSoMayLe').val();
        if(somay===""){
            notyMessage("Số máy lẻ chưa được chọn", "error");
            return false;
        }
        $.ajax({
            type: 'POST',
            url:  telecomConfig.telecomInfo.telecomConfig.urlHttpService+'webservice/callHangUp',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: {
                extension : somay,
            },
            /**
             * @param response {"status":1,"message":"æˆåŠŸ"}
             */
            success: function (response) {
                console.log('response: '+response.message)
            },
            error: function (error) {
                console.log("error ",error)
            }
        });
    }
    function sendEndCallOut() {
        console.log("trianh sendEndCallOut");
        somay = $('#txtSoMayLe').val();
        if(somay===""){
            notyMessage("Số máy lẻ chưa được chọn", "error");
            return false;
        }
        $.ajax({
            type: 'POST',
            url:  telecomConfig.telecomInfo.telecomConfig.urlHttpService+'webservice/callHangUp',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: {
                extension : somay,
            },
            /**
             * @param response {"status":1,"message":"æˆåŠŸ"}
             */
            success: function (response) {
                console.log('response: '+response.message)
            },
            error: function (error) {
                console.log("error ",error)
            }
        });
    }
    function getHistoryTelecomCall(type) {
        dateStartSend = getCurentDate;
        dateEndSend = dateStartSend;
        if($('#txtStartDateTongDai').val()!==''&&$('#txtStartTimeTongDai').val()!==''){
            dateStartSend = $('#txtStartDateTongDai').val();
        }
        if($('#txtEndDateTongDai').val()!==''&&$('#txtEndTimeTongDai').val()!==''){
            dateEndSend = $('#txtEndDateTongDai').val();
        }
        if (typeof dateStart !== 'undefined') {
            dateStartSend = dateStart;
        }
        if (typeof dateEnd !== 'undefined') {
            dateEndSend = dateEnd;
        }
        $('#history-calling').html('Đang tải dữ liệu ... ');

        $.ajax({
            //http://124.158.15.41:1101/webservice/getcallslog?contextName=agentqueue&fromDate=2018-07-30&toDate=2018-08-04&expType=CSV&expmode=all
            url: '{{ action('TicketController@getHistoryCall') }}',
            type: "GET",
            data: {
                contextName : telecomConfig.telecomInfo.telecomConfig.contextName,
                fromDate: dateStartSend,
                toDate: dateEndSend,
                expType: "CSV",
                expmode : "all",
            },
            error: function (xhr, status, error) {
                $('#history-calling').html('Lỗi!');
                console.log(status + '; ' + error);
            },
            success: function (data) {
                data = (data||"").split("\n");
                arrKey = data[0].split(',');
                data.splice(data.length-1,1);
//                data.splice(0,1);
                var newData=[];
                $.each(data,function (k,v) {
                    newData.push(v.split(","));
                });

                renderListCall(newData,type);
            }

        });
    }
    function renderListCall(listCall, type) {
        somay = $('#txtSoMayLe').val();
        sdt = $('#txtPhoneNumberTongDai').val();
        html = '';
        html += '<ul class="list-group">';
        $.each(listCall, function (i, e) {
//            if(e.number_phone.toString().indexOf(sdt)===-1){
//                return;
//            }
            switch (type) {
                case 'goidennho':
                    if((e[3]==="IN" ||e[3]==="IN-TRANSFER") && e[18]<=0 && e[5].toString().indexOf(sdt)!==-1){
                        html += '<li class="list-group-item">';
                        html += '<span style="color:#942a25"><i class="fas fa-times"></i></span> &nbsp;';
                        html += '<span class="phone">' + e[5] + '</span> -> <span>'+e[9]+'</span><br>';
                        html += '<span class="time">' + e[14] + '</span><br>';
                        html += '</li>';
                    }
                    break;
                case 'goidenthanhcong':
                    if(somay==="" && (e[3]==="IN" ||e[3]==="IN-TRANSFER") && e[18]>0 && e[5].toString().indexOf(sdt)!==-1){
                        html += '<li class="list-group-item">';
                        html += '<span style="color:green"><i class="fas fa-check"></i></span> &nbsp;';
                        html += '<span class="phone">' + e[5] + '</span> -> <span>'+e[9]+'</span><br>';
                        html += '<span class="time">' + e[14] + '</span><br>';
                        html += '<audio controls style="width:-webkit-fill-available">' +
                            '<source src="'+e[20]+'" type="audio/ogg">' +
                            '<source src="'+e[20]+'" type="audio/mpeg">' +
                            'Your browser does not support the audio element.' +
                            '</audio>';
                        html += '</li>';
                    }else if (somay!=="" && (e[3]==="IN" ||e[3]==="IN-TRANSFER") && e[18]>0 &&somay===e[9] && e[5].toString().indexOf(sdt)!==-1) {
                        html += '<li class="list-group-item">';
                        html += '<span style="color:green"><i class="fas fa-check"></i></span> &nbsp;';
                        html += '<span class="phone">' + e[5] + '</span> -> <span>'+e[9]+'</span><br>';
                        html += '<span class="time">' + e[14] + '</span><br>';
                        html += '<audio controls style="width:-webkit-fill-available">' +
                            '<source src="'+e[20]+'" type="audio/ogg">' +
                            '<source src="'+e[20]+'" type="audio/mpeg">' +
                            'Your browser does not support the audio element.' +
                            '</audio>';
                        html += '</li>';
                    }
                    break;

                case 'goidi' :
                    if (somay==="") {
                        if ((e[3] === "OUT")&& e[9].toString().indexOf(sdt) !== -1){
                            html += '<li class="list-group-item">';
                            if(e[18]<=0){
                                html += '<span style="color:#942a25"><i class="fas fa-times"></i></span> &nbsp;';
                            }else{
                                html += '<span style="color:green"><i class="fas fa-check"></i></span> &nbsp;';
                            }
                            html += '<span>' + e[5] + '</span> -> <span class="phone">'+e[9]+'</span><br>';
                            html += '<span class="time">' + e[14] + '</span><br>';
                            html += '<audio controls style="width:-webkit-fill-available">' +
                                '<source src="'+e[20]+'" type="audio/ogg">' +
                                '<source src="'+e[20]+'" type="audio/mpeg">' +
                                'Your browser does not support the audio element.' +
                                '</audio>';
                            html += '</li>';
                        }
                    }else{
                        if((e[3] === "OUT") &&somay===e[5] && e[9].toString().indexOf(sdt)!==-1){
                            html += '<li class="list-group-item">';
                            if(e[18]<=0){
                                html += '<span style="color:#942a25"><i class="fas fa-times"></i></span> &nbsp;';
                            }else{
                                html += '<span style="color:green"><i class="fas fa-check"></i></span> &nbsp;';
                            }
                            html += '<span>' + e[5] + '</span> -> <span class="phone">'+e[9]+'</span><br>';
                            html += '<span class="time">' + e[14] + '</span><br>';
                            html += '<audio controls style="width:-webkit-fill-available">' +
                                '<source src="'+e[20]+'" type="audio/ogg">' +
                                '<source src="'+e[20]+'" type="audio/mpeg">' +
                                'Your browser does not support the audio element.' +
                                '</audio>';
                            html += '</li>';
                        }
                    }
                    break;

                default :
                    html += '';
            }
        });
        html += '</ul>';
        $('#history-calling').html(html);
    }
</script>