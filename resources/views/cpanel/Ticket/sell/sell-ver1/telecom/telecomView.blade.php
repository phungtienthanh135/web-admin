@if(is_array(session('userLogin')['telecomNumber'])&&count(session('userLogin')['telecomNumber'])>0&&count(session('userLogin')["userInfo"]['listAgency'])<=0)
<style>
    .fixed-bot-left{
        height: 100vh;
        position: fixed;
        width: 234px;
        bottom:0;
        left:-234px;
        background: #525050;
        transition: left 0.5s;
        z-index: 100001;
    }
    .fixed-bot-left *::-webkit-scrollbar {
        width: 0;
    }
    .fixed-bot-left hr{
        border-top:1px solid #676464;
        border-bottom:1px solid #676464;
    }
    .open-box-phone{left: 0;transition: left 0.5s}
    .box-phone-content{height: 100%;overflow-y: scroll}
    .open-close-phone{cursor:pointer;position: absolute;color:#fff;top:50%;right:-30px;height: 30px;width: 30px; background: #e40d47;line-height: 30px;border-top: 1px;border-right:1px;border-bottom:1px;text-align: center;border-radius: 0 15px 15px 0}
    .open-close-phone:hover{background: #d6de65;color:#e40d47}
    .customer-calling-status{color:#ddd}
</style>

<div class="fixed-bot-left" id="box-phone-fixed-left">
    <div class="open-close-phone">
        <i class="fa fa-phone"></i>
    </div>
    <div class="box-phone-content" style="padding: 10px">
        <label for="" style="color:#ddd">Số Máy Lẻ</label>
        <select name="" id="txtSoMayLe" style="width: 100%">
            <option value="none">Chọn máy lẻ</option>
            @foreach(session('userLogin')['telecomNumber'] as $sml)
                <option value="{{$sml['number']}}" @if($soMayLe!=''&&$sml['number']==$soMayLe['number']) selected @endif>{{$sml['number']}}</option>
            @endforeach
        </select>
        <div class="cuocGoiDen" style="display:none;">
            <div class="customer-calling-status"></div>
            <h3 class="text-center phone-number-customer-calling" style="color:#ffff00"></h3>
            <div class="customer-info-calling" style="padding-top: 10px;padding-bottom: 10px;"></div>
            <div class="text-center ChapNhanCuocGoi">
                <button class="btn btn-danger  input-block-level" id="cancelCallingTelecom" onclick="return sendEndCallIn();">Đã kết thúc</button>
            </div>
        </div>
        <hr>
        <div class="cuocGoiDi">
            <input type="text" id="txtPhoneNumberCallTelecom" style="width: -webkit-fill-available" placeholder="Nhập Số điện thoại">
            <div class="text-center">
                <button class="btn btn-success click-to-call-out input-block-level" onclick="return sendCallOut();">Gọi</button>
                <button class="btn btn-danger click-to-end-call-out input-block-level" style="display: none;" onclick="return sendEndCallOut();">Kết
                    Thúc
                </button>
            </div>
        </div>
        <hr>
        <div class="row-fluid text-center">
            <div class="span12" title="Thời gian bắt đầu">
                <div class="input-append" style="width: 100%">
                    <input type="text" id="txtStartTimeTongDai" value="00:00" style="width: 18%" readonly>
                    <input autocomplete="off" value="{{request('startDate',date('Y-m-d',time()))}}"
                           id="txtStartDateTongDai" type="text" readonly style="width:70%;">
                </div>
            </div>
            <div class="span12" style="margin:0" title="Thời gian kết thúc">
                <div class="input-append" style="width: 100%">
                    <input type="text" id="txtEndTimeTongDai" value="23:59" style="width: 18%" readonly>
                    <input autocomplete="off" value="{{request('endDate',date('Y-m-d'))}}"
                           id="txtEndDateTongDai" type="text" readonly style="width: 70%">
                </div>
            </div>
            <div>
                <input type="text" id="txtPhoneNumberTongDai" style="width:-webkit-fill-available" placeholder="Số điện thoại cần tìm">
            </div>
            <div class="span4" style="margin:0">
                <button class="btn btn-success input-block-level historyIncommingCallSuccess" onclick="return getHistoryTelecomCall('goidenthanhcong')" title="lịch sử cuộc gọi"><i class="fas fa-exchange-alt"></i></button>
            </div>
            <div class="span4">
                <button class="btn btn-danger input-block-level historyIncommingCallFail" onclick="return getHistoryTelecomCall('goidennho')" title="Gọi nhỡ"><i class="fas fa-microphone-slash"></i></button>
            </div>
            <div class="span4">
                <button class="btn btn-warning input-block-level historyCallAway" onclick="return getHistoryTelecomCall('goidi')" title="Cuộc gọi đi"><i class="fas fa-phone"></i></button>
            </div>
        </div>
        <div id="history-calling">
        </div>
    </div>
</div>

<script>
    var telecomConfig = {
        listPhone : {!! json_encode(session('userLogin')['telecomNumber'],JSON_PRETTY_PRINT) !!},
        telecomInfo : {!! json_encode($soMayLe,JSON_PRETTY_PRINT) !!},
    };
    var isCalling = false;
    /*
    * telecomCallingInfo.status
    * 11 : cuộc gọi đến
    * 18 : đang nghe cuộc gọi đến :
    * 14 : kết thúc cuộc gọi đến :
    * 21 : cuộc gọi đi :
    * 28 : đang nghe cuộc gọi đi :
    * 24 : kết thúc cuộc gọi đi :
    * */
    var telecomCallingInfo = {};
    var webSocket = null;

    $(document).ready(function(){
        if(typeof telecomConfig.telecomInfo.telecomConfig === "string"&&telecomConfig.telecomInfo.telecomConfig !==''){
            telecomConfig.telecomInfo.telecomConfig = JSON.parse(telecomConfig.telecomInfo.telecomConfig);
        }
        @if($soMayLe!=''&&isset($soMayLe['telecomCompanyId']))
        $('#txtEndTimeTongDai').val(new Date().customFormat('#hhhh#:#mm#'));
//        generateListTelecomNumber();
        webSocketInit();//khởi tạo kết nối đến tổng đài đối với nhà xe đó
        $('#history-calling').on('click', '.phone', function () {
            $('#txtPhoneNumberCallTelecom').val($(this).text());
        });

        $('body').on('click','.toggleShowListLastTicketInfo',function(){
            $('.listLastTicketInfo').toggleClass('hide');
        });
        @endif

        $('#txtSoMayLe').change(function () {
            if(typeof(Storage) !== "undefined") {
                // Có thể sử dụng localStorage/sessionStorage.
                var sml = $(this).val();
//                setCookie('somayle',sml,5);
                var url = window.location.pathname;
                if (url.indexOf('?') > -1){
                    url += '&SoMayLe='+sml;
                }else{
                    url += '?SoMayLe='+sml;
                }
                window.location.href = url;
//                localStorage.setItem('soMayLe',sml);
//                notyMessage("Cập nhật số máy lẻ thành công",'success');
//                webSocketInit();
            } else {
                // Trình duyệt không hỗ trợ Local storage.
                notyMessage("Trình duyệt không hỗ trợ!","error");
            }
        });

        $('#txtStartTimeTongDai,#txtEndTimeTongDai').clockpicker({autoclose:true});
        $('#txtStartDateTongDai,#txtEndDateTongDai').datepicker({dateFormat : 'yy-mm-dd'});
    });
    function generateListTelecomNumber() {
                html = "";
                html+= '<option value="">Chọn máy lẻ</option>';
                $.each(telecomConfig.listPhone,function (k,v) {
                    html+='<option value="'+v.number+'" data-pass="'+v.telecomPassword+'">'+v.number+'</option>';
                });
                $('#txtSoMayLe').html(html);
    }

    function ringingCallIn(phoneNumber) {//cuộc gọi đến
        if(telecomCallingInfo.status===18||telecomCallingInfo.status===28){
            return false
        }
        telecomCallingInfo.phoneNumber = phoneNumber;
        telecomCallingInfo.status = 11;
        $('.cuocGoiDen').show();
        $('#box-phone-fixed-left').find('.customer-calling-status').html('Cuộc gọi đến');
        $('#cancelCallingTelecom').text('Từ Chối Cuộc Gọi');
        $('#txtPhoneNumberCallTelecom').val(phoneNumber);
        $('#box-phone-fixed-left').addClass('open-box-phone');
        open_menu();
        SendAjaxWithJson({
            url : urlDBD('company_customer/find_info_by_phone_number'),
            type  : "post",
            dataType : "json",
            data :{count:8,page : 0,phoneNumber : phoneNumber},
            success : function(result){
                console.log('ifcs',result);
                inner_html = '';
                lastTicketInfo = typeof result.results.lastTicketInfo ? result.results.lastTicketInfo:[];
                if(lastTicketInfo.length>0){
                    item = lastTicketInfo[0];
                    telecomCallingInfo.customer = item;
                    inner_html += '<div style="background: #fff;border:1px dotted green;margin-bottom: 15px;padding:10px">';
                    inner_html += '<div  class="col-md-12" style="text-transform: uppercase;font-size: 18px;"><strong>' + item.fullName + '</div>';
                    inner_html += '<a style="height:50px" class="link link-phone-phuc-xuyen" href="#" ><span class="hide dataTicket">' + JSON.stringify(item) + '</span>';
                    inner_html += '<div class="col-md-12">Tuyến: ' + (item.routeName || "") + ' ( '+checkStatusSeatAndTicket(item.ticketStatus||0,0).TicketStatusMessage+' ) <br> ' + checkDate(item.getInTimePlan) + '</div>';
                    inner_html += '</a>';
                    inner_html += '<div class="col-md-12 toggleShowListLastTicketInfo">Đã đặt: <strong style="color:green">' + item.numberOfTickets + '</strong> vé, Đã hủy: <strong style="color:red">' + item.numberCancelTicket + '</strong> vé <br>Ghế : '+(item.listSeatId||[]).toString()+'</div>';
                    inner_html += '</div>';
                    inner_html+='<div class="listLastTicketInfo hide">';
                    $.each(lastTicketInfo,function(k,item){
                        if(k!==0){
                            inner_html += '<div style="background: #fff;border:1px dotted green;margin-bottom: 15px;padding:10px">';
                            inner_html += '<a style="height:50px" class="link link-phone-phuc-xuyen" href="#" ><span class="hide dataTicket">' + JSON.stringify(item) + '</span>';
                            inner_html += '<div class="col-md-12">Tuyến: ' + (item.routeName || "") + ' ( '+checkStatusSeatAndTicket(item.ticketStatus||0,0).TicketStatusMessage+' ) <br> ' + checkDate(item.getInTimePlan) + '</div>';
                            inner_html += '</a>';
                            inner_html += '<div class="col-md-12 toggleShowListLastTicketInfo">Ghế : '+(item.listSeatId||[]).toString()+'</div>';
                            inner_html += '</div>';
                        }
                    });
                    inner_html+='</div>';
                }else{
                    inner_html = '<div><h4 style="color :#ddd">Khách mới</h4></div>';
                    delete telecomCallingInfo.customer;
                }
                $('#box-phone-fixed-left').find('.phone-number-customer-calling').html(formatPhoneNumber(phoneNumber));
                $('#box-phone-fixed-left').find('.customer-info-calling').html(inner_html);
                open_menu();
            },
            functionIfError : function(result){
                delete telecomCallingInfo.customer;
                $('#box-phone-fixed-left').find('.phone-number-customer-calling').html(formatPhoneNumber(phoneNumber));
                $('#box-phone-fixed-left').find('.customer-info-calling').html('');
            }
        });
    }
    function listeningCallIn() {//đang nghe cuộc gọi đến
        telecomCallingInfo.status = 18; // trạng thái đang nghe cuộc gọi đén
//        $('#box-phone-fixed-left').find('.phone-number-customer-calling').html();
        $('#box-phone-fixed-left').find('.customer-calling-status').html('Đang nghe ...');
        $('#cancelCallingTelecom').text('Kết Thúc Cuộc Gọi');
        isCalling=true;
    }
    function endCallIn(phoneNumber) { //cuộc gọi đến kết thúc
        telecomCallingInfo.status = 14;
        telecomCallingInfo.phoneNumber = phoneNumber;
        delete telecomCallingInfo.customer;
        $('#box-phone-fixed-left').find('.customer-calling-status').html('Đã kết thúc');
        $('#cancelCallingTelecom').text('Đã Kết thúc');
        $('#box-phone-fixed-left').removeClass('open-box-phone');//đong form cuốc gọi
        close_menu();
        isCalling=false;
    }
    function ringingCallOut(phoneNumber) {
        console.log("callout");
        telecomCallingInfo.status = 21;
        telecomCallingInfo.phoneNumber = phoneNumber;
        $('#box-phone-fixed-left').find('.click-to-call-out').hide();
        $('#box-phone-fixed-left').find('.click-to-end-call-out').show();
        isCalling=true;
    }
    function listeningCallOut(phoneNumber) {
        telecomCallingInfo.status = 28; // trạng thái đang nghe cuộc gọi đén
        telecomCallingInfo.phoneNumber = phoneNumber; // trạng thái đang nghe cuộc gọi đén
        $('#box-phone-fixed-left').find('#txtPhoneNumberCallTelecom').html(formatPhoneNumber(phoneNumber));
        isCalling=true;
    }
    function endCallOut(phoneNumber) {
        telecomCallingInfo.status = 24;
        telecomCallingInfo.phoneNumber = phoneNumber;
        delete telecomCallingInfo.customer;
        $('#box-phone-fixed-left').find('.click-to-call-out').show();
        $('#box-phone-fixed-left').find('.click-to-end-call-out').hide();
        isCalling=false;
    }
</script>
@if($soMayLe!=''&&isset($soMayLe['telecomCompanyId']))
    @if($soMayLe['telecomCompanyId']=="TC06I2JslMujIwm")
        @include('cpanel.Ticket.sell.sell-ver1.telecom.JsTelecom.triAnhTelecom')
    @elseif($soMayLe['telecomCompanyId']=="TC06I2JslovcpeG")
        @include('cpanel.Ticket.sell.sell-ver1.telecom.JsTelecom.nttTelecom')
    @elseif($soMayLe['telecomCompanyId']=="TC06z1peVXFpIAXd")
        @include('cpanel.Ticket.sell.sell-ver1.telecom.JsTelecom.vccTelecom')
    @elseif($soMayLe['telecomCompanyId']=="TC07P1ponfPZSceO")
        @include('cpanel.Ticket.sell.sell-ver1.telecom.JsTelecom.careTelecom')
    @elseif($soMayLe['telecomCompanyId']=="TC07g1pvaUTVFRuO")
        {{--mitex--}}
        @include('cpanel.Ticket.sell.sell-ver1.telecom.JsTelecom.mitexTelecom')
    @endif
@endif

    {{--@if(session('userLogin')['telecomInfo']['telecomCompanyId']=="TC06I2JslMujIwm")--}}
        {{--tri anh--}}
        {{--@include('cpanel.Ticket.sell.sell-ver1.telecom.JsTelecom.triAnhTelecom')--}}
    {{--@elseif(session('userLogin')['telecomInfo']['telecomCompanyId']=="TC06I2JslovcpeG")--}}
        {{--đồng nhất--}}
        {{--@include('cpanel.Ticket.sell.sell-ver1.telecom.JsTelecom.nttTelecom')--}}
    {{--@elseif(session('userLogin')['telecomInfo']['telecomCompanyId']=="TC06z1peVXFpIAXd")--}}
        {{--vNPT VCC--}}
        {{--@include('cpanel.Ticket.sell.sell-ver1.telecom.JsTelecom.vccTelecom')--}}
    {{--@endif--}}
@endif