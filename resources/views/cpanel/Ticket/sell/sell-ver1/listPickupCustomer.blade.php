<div id="dskhachdon" style="display: none">
    <h3 align="center" id="title-table">
        @include('cpanel.Ticket.constant')
        @if(in_array(session('companyId'), LIST_CUSTOMERS_DROP_COMPANIES)) DANH SÁCH TRUNG CHUYỂN ĐÓN
        @else DANH SÁCH TRUNG CHUYỂN
        @endif</h3>
    <div align="center" class="info_trip" style="font-weight: bold"></div>
    <br>
    <div id="panel-sell">
        <div class="widget-body">
            <div class="tab-content">
                <style>
                    .padding {
                        padding-top: 0px !important;
                        border: 1px solid;
                    }
                </style>
                <table class="" style="    margin-top: 25px;width: 100%;font-family: 'Verdana'; font-size: 12px" id="table-list">
                    @php
                        $col1 = 1;
                        $col2 = 2;
                        /*if(session('companyId') != 'TC03013FPyeySDW')
                        {
                            $col1 = 8;
                            $col2 = 4;
                        }*/
                    @endphp
                    <thead>
                    <tr>
                        <div style="width: 100%;height: 50px">
                            <div  style="float: left;width: 9%;height:100%"></div>
                            <div style="text-align:left;float: left;width:10%;height:100%">Chuyến : <br>Biển số xe : <br>Giờ chạy
                                : <br></div>
                            <div style="float: left;width:17%">
                                <span style="font-weight: bold;" class="chuyen"></span><br>
                                <span style="font-weight: bold;" class="bienso"></span><br>
                                <span class="startTime" style="font-weight: bold"></span>
                            </div>
                            <!--                                        --><?php //if(session('companyId') != 'TC03013FPyeySDW' ||  session('companyId') !='TC03m1MSnhqwtfE'){ ?>
                            <div style="float: left;width:35%;height:100%"></div>
                            <div style="float: left;text-align: left;width:9%">Lái xe : <br>Phụ xe : <br>Tổng ghế :
                            </div>
                            <div  style="float: left;width:20%">
                                <span style="font-weight: bold; " class="laixe"></span><br>
                                <span class="phuxe" style="font-weight: bold;"></span><br>
                                <span class="totalSeat" style="font-weight: bold"></span>
                            </div>
                            <!--                                        --><?php //}?>
                        </div>
                    </tr>
                    <tr style="border-top: 1px solid #ddd">
                        @if(session('companyId')=='TC03m1MSnhqwtfE' ||session('companyId') =='TC02orwyNWOVEq')
                            <th style="text-align: center;border: 1px solid">STT</th>
                            <th style="text-align: center;border: 1px solid">ĐÓN</th>
                            <th style="text-align: center;border: 1px solid">HK</th>
                            <th style="text-align: center;border: 1px solid">SĐT</th>
                            <th style="text-align: center;border: 1px solid">MÃ GHẾ</th>
                            <th style="text-align: center;border: 1px solid">GHI CHÚ</th>
                            <th style="text-align: center;border: 1px solid">TÀI XẾ ĐÓN</th>

                        @else
                            @if (session('companyId') == 'TC0481HSc6Pl7ko')
                                <th style="text-align: center;border: 1px solid">STT</th>
                                <th style="text-align: center;border: 1px solid">KH</th>
                                <th style="text-align: center;border: 1px solid">SĐT</th>
                                <th style="text-align: center;border: 1px solid">ĐÓN</th>
                                <th style="text-align: center;border: 1px solid">GHI CHÚ</th>
                            @else
                                <th style="text-align: center;border: 1px solid">STT</th>
                                <th style="text-align: center;border: 1px solid">MÃ VÉ</th>
                                <th style="text-align: center;border: 1px solid">SỐ GHẾ</th>
                                <th style="text-align: center;border: 1px solid">GHẾ</th>
                                <th style="text-align: center;border: 1px solid">KH</th>
                                <th style="text-align: center;border: 1px solid">SĐT</th>
                                <th style="text-align: center;border: 1px solid">ĐÓN</th>
                                <th style="text-align: center;border: 1px solid">TRẢ</th>
                                <th style="text-align: center;border: 1px solid">ĐẠI LÍ</th>
                                <th style="text-align: center;border: 1px solid">GHI CHÚ</th>
                                <th style="text-align: center;border: 1px solid">TÀI XẾ ĐÓN</th>
                                <th style="text-align: center;border: 1px solid">TÀI XẾ TRẢ</th>
                            @endif
                        @endif
                    </tr>
                    </thead>
                    <tbody id="list-customer-pickup"></tbody>
                </table>
            </div>
        </div>
    </div>
    <br><br>
</div>