<div id="dskhach">
    <div style="padding-top: 10px" class="btn-group export-btn">
        <a style="color: #1d9838;width: 60px" id="excel" class="btn btn-default btn-flat"
           data-fileName="danh_sach_hanh_khach">Xuất Excel</a>
    </div>
    <div class="dshkHeader">

    </div>
    <div class="clear"></div>
    <h3 align="center" id="title-table" style="margin-bottom: 0">DANH SÁCH HÀNH KHÁCH</h3>
    <p class="underTitle" style="text-align: center;font-size: 14px;font-style: italic;margin-bottom: 20px"></p>
    <div id="panel-sell">
        <div class="widget-body">
            <div class="tab-content">
                <style>
                    .padding {
                        padding-top: 0px!important;
                    }
                </style>

                <table class="table table-condensed tablesorter"
                       style="font-family: 'Verdana'; font-size: 12px;    margin-top: 25px;" id="table-list">
                    @php
                        $col1 = 1;
                        $col2 = 2;
                        /*if(session('companyId') != 'TC03013FPyeySDW')
                        {
                            $col1 = 8;
                            $col2 = 4;
                        }*/
                    @endphp
                    <thead style="width: 100%;border-bottom: 1px solid #ddd">
                    <tr>
                        <div style="width: 100%;height: 50px">
                            <div style="float: left;width: 9%;height:100%"></div>
                            <div style="text-align:left;float: left;width:10%;height:100%">Chuyến : <br>Biển số xe :
                                <br>Giờ chạy
                                : <br></div>
                            <div style="float: left;width:17%">
                                <span style="font-weight: bold;" class="chuyen"></span><br>
                                <span style="font-weight: bold;" class="bienso"></span><br>
                                <span class="startTime" style="font-weight: bold"></span>
                            </div>
                            <!--                                        --><?php //if(session('companyId') != 'TC03013FPyeySDW' ||  session('companyId') !='TC03m1MSnhqwtfE'){ ?>
                            <div style="float: left;width:35%;height:100%"><div class="noteTripNumber" style="line-height:50px;text-align: center"></div></div>
                            <div style="float: left;text-align: left;width:9%">
                                @if(session('companyId')!='TC0481HSc6Pl7ko') Lái xe : <br>Phụ xe : <br>Tổng ghế :
                                @else
                                    Lái xe : <br>Tiếp viên :<br>Tổng ghế :
                                @endif
                            </div>
                            <div style="float: left;width:20%">
                                <span style="font-weight: bold; " class="laixe"></span><br>
                                <span class="phuxe" style="font-weight: bold;"></span><br>
                                <span class="totalSeat" style="font-weight: bold"></span>
                            </div>
                        </div>

                        <div class="listBlank" style="text-align: center;margin-bottom: -20px;"></div>
                    </tr>
                    <tr style=' border: 1px solid'>
                        @foreach($listSetting as $row)
                            @if($row['idAttribute']!=99)
                            <th data-id="{{$row['idAttribute']}}" class="sortAttribute"
                                style="text-align: center;border: 1px solid;
                                        font-size: {{$row['size']>0?$row['size'].'px':'12px'}};
                                        width:{{$row['width']>0?$row['width'].'px':'auto'}};
                                        height:{{$row['height']>0?$row['height'].'px':'auto'}};
                                        ">{{trim($row['alias'],'')!=''?$row['alias']:$row['name']}}</th>
                            @endif
                        @endforeach
                    </tr>
                    </thead>
                    <tbody id="list-customer" style=' border: 1px solid'></tbody>
                </table>
                <div class="underTable" style="margin-top: 30px"></div>
            </div>
        </div>
    </div>
    <br><br>
    <div class="listAdditionTickets hideButton"></div>
    <br>
    <div class="listGoodsInTrip hideButton"></div>
    <br>
    <div class="row-fluid tableKhoanMucHungDuc" style="display: none;">
        <div style="float: left;width: calc(60% - 20px);padding:10px">
            <table class="khoanMucChi le" style="width: 48%;float: left" border="1">
                <thead>
                <tr>
                    <th>Khoản Mục Chi</th>
                    <th>Thành Tiền</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
            <table class="khoanMucChi chan" style="width: 48%;float: left" border="1">
                <thead>
                <tr>
                    <th>Khoản Mục Chi</th>
                    <th>Thành Tiền</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <div style="float: left;width: calc(40% - 20px);padding:10px">
            <table class="khoanMucThu" style="width: 100%" border="1">
                <thead>
                <tr>
                    <th>Khoản Mục Thu</th>
                    <th>Thành Tiền</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <div style="clear:both"></div> 
        <div style="float: left;text-align: center ;width: calc(50% - 20px);padding:10px">
            <b>Nhân viên văn phòng</b>
        </div>
        <div style="float: left;width: calc(50% - 20px);padding:10px;text-align: center ;">
            <b>Lái phụ xe</b>
        </div>
    </div>
</div>
<style>
    .hideButton .cancelSecondaryTicket, .hideButton .removeGoods{display: none}
</style>
<script type="text/javascript" src="/public/javascript/jquery-table2excel/jquery.table2excel.min.js"></script>
<script>
    $(document).ready(function () {
        if('{{ session("companyId") }}'==='TC05wM7dDRQSo6'){//hùng đức
            SendAjaxWithJson({
                url : urlDBD('items/getlist'),
                data : {
                    page:0,count:1000,
                },
                type : "post",
                success : function (data) {
                    renderKhoanMucHungDuc(data.results.result);
                },
            });
            $('.tableKhoanMucHungDuc').show();
        }
    });
    function renderKhoanMucHungDuc(data) {
        var htmlThu = '';
        var htmlChiChan = '';
        var htmlChiLe = '';
        var i=0;
        $.each(data,function (k,v) {
            if(v.itemReceiptPayment===1){
                htmlThu += '<tr style="text-align: center">';
                htmlThu += '<td>'+v.itemName+'</td>';
                htmlThu += '<td><div style="border-bottom:1px dotted #000">&nbsp;</div></td>';
                htmlThu += '</tr>';
                i++;
                return;
            }
            if(v.itemReceiptPayment===2&&i%2===0){
                htmlChiChan += '<tr style="text-align: center">';
                htmlChiChan += '<td>'+v.itemName+'</td>';
                htmlChiChan += '<td><div style="border-bottom:1px dotted #000">&nbsp;</div></td>';
                htmlChiChan += '</tr>';
                i++;
                return;
            }
            if(v.itemReceiptPayment===2&&i%2!==0){
                htmlChiLe += '<tr style="text-align: center">';
                htmlChiLe += '<td>'+v.itemName+'</td>';
                htmlChiLe += '<td><div style="border-bottom:1px dotted #000">&nbsp;</div></td>';
                htmlChiLe += '</tr>';
                i++;
                return;
            }
//            htmlThu += '<tr style="text-align: center">';
//            htmlThu += '<td>'+v.itemName+'</td>';
//            htmlThu += '<td><div style="border-bottom:1px dotted #000">&nbsp;</div></td>';
//            htmlThu += '</tr>';
//            htmlChi += '<tr style="text-align: center">';
//            htmlChi += '<td>'+v.itemName+'</td>';
//            htmlChi += '<td><div style="border-bottom:1px dotted #000">&nbsp;</div></td>';
//            htmlChi += '</tr>';
        });
        $('.khoanMucChi.chan').find('tbody').html(htmlChiChan);
        $('.khoanMucChi.le').find('tbody').html(htmlChiLe);
        $('.khoanMucThu').find('tbody').html(htmlThu);
    }
</script>