<div class="row-fluid">
    <div class="span4" style="margin-top: -58px;">
        <div class="widget widget-2 widget-tabs widget-tabs-2 no_border">
            <div class="widget-head">
                <ul class="chontang">
                    <li class="active">
                        <a href="#ds_tang1" data-toggle="tab">
                            <i></i>Tầng 1
                        </a>
                    </li>
                    <li>
                        <a href="#ds_tang2" data-toggle="tab">
                            <i></i>Tầng 2
                        </a>
                    </li>
                </ul>
            </div>
            <div class="widget-body">
                <div class="tab-content" style="overflow:hidden">
                    <div class="tab-pane active" id="ds_tang1">
                        <h5></h5>
                        <div class="khungxe" style="width:294px;">
                        </div>
                    </div>
                    <div class="tab-pane" id="ds_tang2">
                        <h5></h5>
                        <div class="khungxe" style="width:294px;">
                        </div>
                    </div>
                    <div class="center ghichu_datghe">
                        <div class="gc_ghe gc_giucho"></div>
                        <div class="title_ghichu_datghe">Ghế giữ chỗ</div>
                        <div class="gc_ghe gc_dachon"></div>
                        <div class="title_ghichu_datghe">Ghế đang chọn</div>
                        <div class="gc_ghe gc_dadat"></div>
                        <div class="title_ghichu_datghe">Ghế đã đặt</div>
                        <div class="gc_ghe gc_trong"></div>
                        <div class="title_ghichu_datghe">Ghế trống</div>
                        <div class="gc_ghe ghelenxe"></div>
                        <div class="title_ghichu_datghe">Ghế đã lên xe</div>
                        <div class="gc_ghe ghephuxe"></div>
                        <div class="title_ghichu_datghe">Ghế phụ xe</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="span8">
        <div class="pull-right"><button id="btnPrintTrip" class="btn btn-default"><i class="icon-print"></i>In phơi vé</button></div>
        <table class="m_top_10 table table-hover table-vertical-center">
            <thead>
            <tr>
                <th>Số lượng ghế</th>
                <th>Mã vé</th>
                <th>Họ tên</th>
                <th>SĐT</th>
                <th>Trạng thái</th>
                <th>Tuỳ chọn</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>