<style>
    .active-change-vehicle{
        background: #0b5fc6;
        color:#fff;
        font-weight: bold;
    }
    .changeVehicleType{
        cursor: pointer;
    }
    .seatPosition{padding:3px;line-height:30px;height:30px;overflow:hidden;text-align:center}
    .divListSeatOldChangeSeatMap{
        width: 100%;
        border:2px dashed #0000CC;
    }
    .seatOldChangeSeatMap{
        border :1px solid #888;
        background: #fff;
        color:#000;
        cursor:move;
        height:30px;
        line-height: 30px;
        padding: 0 10px;
        margin:5px;
        float:left;
        font-weight: 600;
    }
    .divSeatMapChangeNew{
        width: 100%;
    }
    .seatOldChangeSeatMap:hover{
        box-shadow: 2px 2px 2px #aaa;
    }
    .cantMoveChangeSeatMap{background: #9eff75 !important;}
    .borderSeat{
        border:1px solid #ddd;
    }
    .disableSeat{
        background: #999;
        color : #fff;
    }
    .bold{font-weight: bold ;background: yellow}
</style>
<div class="modal modal-custom hide fade" id="modalChangeSeatMap">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>CHUYỂN XE</h3>
        </div>
        <div class="modal-body" style="overflow-y: scroll;">
            <div class="row-fluid">
                <div class="form-horizontal row-fluid">
                    <div class="control-group span4">
                        <label for="">Xe</label>
                        <select name="" id="txtIdVehicleChange" class="span12 listVehicleOption" data-validation-error-msg="Vui lòng chọn xe" data-validation="required"></select>
                    </div>
                    <div class="control-group span4">
                        <label for="">Tài Xế</label>
                        <select name="" id="txtDriverVehicleChange" class="span12 listDriveOption" multiple></select>
                    </div>
                    <div class="control-group span4">
                        <label for="">Phụ Xe</label>
                        <select name="" id="txtAssVehicleChange" class="span12 listAssOption" multiple></select>
                    </div>
                    <div class="span12" style="margin-left:0">Danh sách ghế cần chuyển</div>
                    <div class="span12 divListSeatOldChangeSeatMap" style="margin-left:0"></div>
                    <div class="span12" style="margin-left:0">Kéo ghế xuống vị trí muốn chuyển tới,click vào ghế và di chuyển để chuyển sang vị trí khác</div>
                    <div class="clearfix"></div>
                    <div class="span12 divSeatMapChangeNew" style="margin-left: 0">
                        <style class="styleSeatMapChange"></style>
                        <ul class="nav nav-tabs toggleTabFloor" style="width: 100%">
                            <li class="active text-center" style="width: 50%"><a href="#tabfloor1change" data-toggle="tab">Tầng 1</a></li>
                            <li style="width: 50%" class="text-center"><a href="#tabfloor2change" data-toggle="tab">Tầng 2</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active changeSeatMapFloor1" style="margin-left:0;" id="tabfloor1change"></div>
                            <div class="tab-pane changeSeatMapFloor2" id="tabfloor2change"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="modal-footer">
                <input type="hidden" name="numberOfSeat" id="numberOfSeat">
                <a style="cursor: pointer" data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button onclick="return changeSeatMapInfo();" type="button" id="btnChangeSeatMap" class="btn btn-primary">ĐỒNG Ý</button>
            </div>

        </div>
</div>

<script>
    var changeSeatMap = {
        listSeatOld : [],
        listSeatNew : [],
        vehicleInfo : {}
    };
    $(document).ready(function () {
        $('#txtIdVehicleChange,#txtDriverVehicleChange,#txtAssVehicleChange').select2({
            width: '100%',
            dropdownParent: $('#modalChangeSeatMap'),
            dropdownAutoWidth : true
        });

        $('#txtDateVehicleChange').val($('#txtCalendar').val());
        $('body').on('click', '.btnChangeSeatMap', function () {
//            lấy seatMap hiện tại
            buildSeatMap();
            getListSeatOld();
            $('.divListSeatOldChangeSeatMap').html(generateListSeatOld());
            $('#txtAssVehicleChange').val(getListNameWithArrayUser(tripSelected.tripData.listAssistant).userId||'').change();
            $('#txtDriverVehicleChange').val(getListNameWithArrayUser(tripSelected.tripData.listDriver).userId||'').change();
            setEventDrag();
            $('#modalChangeSeatMap').modal('show');
        });

        $('body').on('click','[data-seat-id-new-change-seat-map]',function(){
            seatId = $(this).attr('data-seat-id-new-change-seat-map');
            console.log($("[data-seat-id-old='"+seatId+"']").text());
            $("[data-seat-id-old='"+seatId+"']").eq(0).fadeIn();
            $(this).removeClass('bold moved').addClass('seatEmptyChangeSeatMap').attr('title','').attr('data-seat-id-new-change-seat-map','').text($(this).data('seat-id-old-change-seat-map'));
        });

        $('#txtIdVehicleChange').change(function () {
            $('#txtAssVehicleChange').val(getListNameWithArrayUser(tripSelected.tripData.listAssistant).userId||'').change();
            $('#txtDriverVehicleChange').val(getListNameWithArrayUser(tripSelected.tripData.listDriver).userId||'').change();
            buildSeatMap();
            $('.divListSeatOldChangeSeatMap').html(generateListSeatOld());
            setEventDrag();
        });
    });

    function buildSeatMap(){
        vehicleIdCurrent=$('#txtIdVehicleChange').val();
        changeSeatMap.vehicleInfo=$.grep(listVehicle,function(e,i){
            return e.vehicleId===vehicleIdCurrent;
        })[0];
        generateSeatMapChange(changeSeatMap.vehicleInfo.seatMap);
    }

    function generateSeatMapChange(seatMap){
        tang1='';
        tang2='';
        widthSeat = Math.floor(100/seatMap.numberOfColumns);
        if(seatMap.numberOfFloors<=1){
            $('.toggleTabFloor').hide();
        }else{
            $('.toggleTabFloor').show();
        }
        for(var floor = 1;floor<=seatMap.numberOfFloors;floor++){
            if(floor===1){
                tang1+=' <table class="seatMapFloor1" style="width: 100%">';
                for(var row=1;row<=seatMap.numberOfRows;row++){
                    tang1+='<tr>';
                    for(var column =1;column<=seatMap.numberOfColumns;column++){
                        tang1+='<td style="width: '+widthSeat+'%"><div class="seatPosition seatPosition'+floor+row+column+'"></div></td>';
                    }
                    tang1+='</tr>';
                }
                tang1+='</table>';
            }
            if(floor===2){
                tang2+=' <table class="seatMapFloor2" style="width: 100%">'
                for(var row=1;row<=seatMap.numberOfRows;row++){
                    tang2+='<tr>';
                    for(var column =1;column<=seatMap.numberOfColumns;column++){
                        tang2+='<td style="width: '+widthSeat+'%"><div class="seatPosition seatPosition'+floor+row+column+'"></div></td>';
                    }
                    tang2+='</tr>';
                }
                tang2+='</table>';
            }
        }
        $('.changeSeatMapFloor1').html(tang1);
        $('.changeSeatMapFloor2').html(tang2);
        fillSeatInfoToSeatMap(seatMap.seatList);
    }

    function fillSeatInfoToSeatMap(seatList){
        $.each(seatList,function(i,e){
            html='';
            if(e.seatType===3||e.seatType===4){
                $('.seatPosition'+e.floor+e.row+e.column).addClass('borderSeat seatEmptyChangeSeatMap').attr('data-seat-id-old-change-seat-map',e.seatId).html(e.seatId);
            }else{
                $('.seatPosition'+e.floor+e.row+e.column).addClass('borderSeat disableSeat').html(e.seatId);
            }

        });
    }

    function setEventDrag() {
        $(".seatOldChangeSeatMap").draggable({
            revert: "invalid", // when not dropped, the item will revert back to its initial position
            // helper:'clone',
            start: function () {
                $('.seatEmptyChangeSeatMap').addClass('cantMoveChangeSeatMap');
            },
            stop : function () {
                $('.seatPosition').removeClass('cantMoveChangeSeatMap');
            }
        });

        $('.seatEmptyChangeSeatMap').droppable({
            accept: ".seatOldChangeSeatMap",
            drop: function( event, ui ) {
                if($(this).hasClass("seatEmptyChangeSeatMap")){
                    $(this).removeClass('seatEmptyChangeSeatMap').addClass('bold moved').attr('title',$(this).data('seat-id-old-change-seat-map')+' -> '+ui.draggable.text()).attr('data-seat-id-new-change-seat-map',ui.draggable.text()).text(ui.draggable.text());
                    $(ui.draggable).fadeOut();
                }
                else{
                    $(ui.draggable).animate({top:0,left:0});
                }
            }
        });

        $('.divListSeatOldChangeSeatMap').droppable({
            accept: ".seatOldChangeSeatMap",
            drop: function( event, ui ) {
                $(ui.draggable).animate({top:0,left:0});
            }
        });
    }

    function getListSeatOld(){
        changeSeatMap.listSeatOld = $.grep(tripSelected.tripData.seatMap.seatList, function (item) {
            if(item.ticketInfo === undefined) {
                return false;
            } else {
                ticketStatus = item.ticketInfo[0].ticketStatus;
                overTime = item.ticketInfo[0].overTime;
                return ((ticketStatus == 2 && (overTime > Date.now() || overTime == 0)) || ticketStatus == 3 || ticketStatus == 7)
            }
        });
    }

    function generateListSeatOld(){
        html='';
        $.each(changeSeatMap.listSeatOld,function(i,e){
            html+='<div class="seatOldChangeSeatMap" data-seat-id-old="'+e.seatId+'">'+e.seatId+'</div>';
        });
        return html;
    }

    function changeSeatMapInfo() {
        var listSeatOldChange = [],
            listSeatNewChange = [];

        $('.moved').each(function (i, e) {
            listSeatOldChange.push($(e).attr('data-seat-id-new-change-seat-map'));
            listSeatNewChange.push($(e).attr('data-seat-id-old-change-seat-map'));
        });

        if(listSeatOldChange.length!==changeSeatMap.listSeatOld.length){
            notyMessage("Vui lòng chuyển hết ghế vào sơ đồ ghế mới",'warning');
            return false
        }

        if(listSeatOldChange.length!==listSeatNewChange.length){
            notyMessage("Có lỗi xảy ra vui lòng thử lại",'warning');
            return false
        }

        $.ajax({
            url: '{{ action("TripController@postTransfer") }}',
            type: "POST",
            dataType: 'json',
            async:true,
            data: {
                tripSourceId: tripSelected.tripData.tripId,
                tripDestinationId: tripSelected.tripData.tripId,
                numberOfGuests: changeSeatMap.listSeatOld.length,
                timeChange: covertDate(tripSelected.tripData.startDate),
                place: tripSelected.tripData.getInPointId || '',
                scheduleId: tripSelected.tripData.scheduleId,
                listSeatSourceId: listSeatOldChange.toString(),
                listSeatDestinationId: listSeatNewChange.toString(),
                vehicleId: $('#txtIdVehicleChange').val(),
                listDriverId : ($('#txtDriverVehicleChange').select2("val")||'').toString(),
                listAssistantId :($('#txtAssVehicleChange').select2("val")||'').toString(),
                _token: '{{ csrf_token() }}'
            },
            success: function (result) {
                $('#modalChangeSeatMap').modal('hide');
                if (result.result.code == 200) {
                    // location.reload();
                    tripSelected.tripData.listAssistant = result.result.results.ChangeVehicle.listAssistant;
                    tripSelected.tripData.listDriver = result.result.results.ChangeVehicle.listDriver;
                    notyMessage("Thông Báo! Gửi yêu cầu thành công! vui lòng đợi...", "success");
                } else {
                    notyMessage("Lỗi! Có lỗi xảy ra khi gửi yêu cầu. Vui lòng tải lại trang!", "error");
                }

            },
            error: function (err) {
                notyMessage("Lỗi! Gửi yêu cầu thất bại !", "error");
            }
        });
        return false;
    }
//    function changeVehicleType(element) {
//        $('.changeVehicleType').removeClass('active-change-vehicle');
//        $(element).addClass('active-change-vehicle');
//        if($(element).data('type')==='gop'){
//            $('.divGopChuyen').show();
//        }else{
//            $('.divGopChuyen').hide();
//        }
//    }
//    function covertDate(unixTime) {
//        var date = $.format.parseDate(unixTime);
//        return date.dayOfMonth+'-'+date.month+'-'+date.year;
//    }
//
//    function generateHtmlVehicle(selectedValue) {
//        html='';
//        $.each(listVehicle,function (i,e) {
//            select = selectedValue===e.numberPlate?"selected":"";
//            html+='<option value="'+e.vehicleId+'" '+select+'>'+e.numberPlate +' ('+e.numberOfSeats+' chỗ)</option>'
//        })
//        $('.listVehicleOption').html(html);
//    }
//    function generateHtmlDriver(selectedValue) {
//        html='';
//        $.each(listDriver,function (i,e) {
//            select = selectedValue===e.phoneNumber?"selected":"";
//            html+='<option value="'+e.userId+'" '+select+'>'+e.fullName+'</option>'
//        });
//        $('.listDriveOption').html(html);
//    }
//
//    function generateHtmlAss(selectedValue) {
//        html='';
//        $.each(listAss,function (i,e) {
//            select = selectedValue===e.phoneNumber?"selected":"";
//            html+='<option value="'+e.userId+'" '+select+'>'+e.fullName+'</option>'
//        });
//        $('.listAssOption').html(html);
//    }
//
//    function initChangeVehicleForm() {
//        generateHtmlVehicle(tripSelected.tripData.numberPlate||'');
//        generateHtmlDriver(tripSelected.tripData.driverPhoneNumber || '');
//        generateHtmlAss(tripSelected.tripData.assPhoneNumber || '');
//        generateListRoute();
//
//        $('#dateSchedulePlus,#txtDateVehicleChange').datepicker({
//            dateFormat : "dd-mm-yy",
//        });
//        $('#dateSchedulePlus,#txtDateVehicleChange').val($('#txtCalendar').val());
//    }
</script>