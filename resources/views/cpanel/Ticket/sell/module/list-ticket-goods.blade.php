<div class="row-fluid">
    <div class="span4">
        <div class="banve_thongtinkhachhang">
            <div class="widget widget-2 widget-tabs widget-tabs-2 no_border">
                <p class="center m_top_10">Hình ảnh sản phẩm</p>
                <div class="khung_hinh_thongtinsp center">
                    <img id="anhsanpham" src="/public/images/anhsanpham.png" onerror="this.src='/public/images/anhsanpham.png'" />
                </div>
            </div>
        </div>
    </div>
    <div class="span8">
        <table class="m_top_10 table table-hover table-vertical-center">
            <thead>
            <tr>
                <th>Mã vé</th>
                <th>Người nhận</th>
                <th>SĐT người nhận</th>
                <th>Tên sản phẩm</th>
                <th>Tên người gửi</th>
                <th>Tuỳ chọn</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>