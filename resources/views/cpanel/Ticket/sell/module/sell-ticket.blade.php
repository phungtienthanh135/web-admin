<form id="datve" action="{{action('TicketController@postSell')}}" method="post">
    <div class="row-fluid">
        <div class="span6">
            <h3>THÔNG TIN VÉ</h3>
            <table class="tb_banve_thongtintinve" cellpadding="4">
                <tr>
                    <td>TUYẾN</td>
                    <td class="routeName"></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="checkbox" id="checkPoint"/>
                        <label title="Chọn vào đây để sửa điểm đón trả" for="checkPoint"></label>
                        <span style="margin-left: 25px">Đưa đón tại nhà</span>
                    </td>
                </tr>
                <tr>
                    <td>ĐIỂM ĐÓN</td>
                    <td>
                        <div id="getInPoint">
                            <span id="txtGetInPointName"></span>
                            <input hidden id="txtGetInPointId" name="getInPointId">
                        </div>
                        <div id="getPickUpAddress" style="display: none">
                            <input id="pickUpAddress" type="text"
                                   placeholder="Nhập địa chỉ để tìm kiếm" name="pickUpAddress"/>
                            <input type="hidden" id="pickUpLat" name="pickUpLat">
                            <input type="hidden" id="pickUpLong" name="pickUpLong">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>ĐIỂM TRẢ</td>
                    <td>

                        <div id="getOffPoint">
                            <span id="txtGetOffPointName"></span>
                            <input hidden id="txtGetOffPointId" name="getOffPointId">
                        </div>
                        <div id="getDropOffAddress" style="display: none">
                            <input id="dropOffAddress" type="text"
                                   placeholder="Nhập địa chỉ để tìm kiếm" name="dropOffAddress"/>
                            <input type="hidden" id="dropOffLat" name="dropOffLat">
                            <input type="hidden" id="dropOffLong" name="dropOffLong">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>THỜI GIAN ĐI</td>
                    <td class="InTime">
                    </td>
                </tr>
                <tr>
                    <td>THỜI GIAN ĐẾN</td>
                    <td class="OffTime"></td>
                </tr>
                <tr>
                    <td>VÉ NGƯỜI LỚN</td>
                    <td>
                        <b class="color_dark" id="lb_numberOfAdults">0</b>
                    </td>
                </tr>
                <tr>
                    <td>VÉ TRẺ EM</td>
                    <td>
                        <div class="wrapper d_inline_m fs_medium r_corners quantity type_2 clearfix">
                            <button type="button" class="f_left bg_light_3" onclick="minus()">
                                <i class="icon-minus "></i>
                            </button>
                            <input autocomplete="off" id="numberOfChildren" type="text" name="numberOfChildren" readonly="" value="0" class="f_left color_grey bg_light">
                            <button type="button" class="f_left bg_light_3" onclick="plus()">
                                <i class="icon-plus"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>VỊ TRÍ</td>
                    <td id="listSeat"></td>
                </tr>
                <tr>
                    <td>GIÁ VÉ</td>
                    <td id="ticketPrice"  style="">0đ</td>

                </tr>
                <tr id="isMeal">
                    <td>BAO GỒM ĂN</td>
                    <td>
                        <span id="mealPrice"></span>
                        <label class="switch">
                            <input id="cb_isMeal" type="checkbox" class="sl_baogoman" name="isMeal">
                            <div class="slider round"></div>
                        </label>
                    </td>
                </tr>
                <tr id="isInsurrance">
                    <td>BAO GỒM BẢO HIỂM</td>
                    <td>
                        <span id="priceInsurrance"></span>
                        <label class="switch">
                            <input id="cb_priceInsurrance" type="checkbox" class="sl_isInsurrance" name="isInsurrance">
                            <div class="slider round"></div>
                        </label>
                    </td>
                </tr>
                <tr id="isPickUpHome" style="display:none;">
                    <td>BAO GỒM ĐƯA ĐÓN</td>
                    <td>
                        <span id="pricePickUpHome"></span>
                        <label class="switch">
                            <input type="checkbox" class="sl_isPickUpHome" name="isPickUpHome">
                            <div class="slider round"></div>
                        </label>
                    </td>
                </tr>
                <tr class="ht_tongtien">
                    <td class="color_blue_2 fs_large fw_ex_bold">TỔNG TIỀN</td>
                    <td class="color_red fs_large fw_ex_bold" id="totalPrice"></td>
                </tr>
            </table>
        </div>
        <div class="span6">
            <div class="banve_thongtinkhachhang">
                <h3>THÔNG TIN KHÁCH HÀNG</h3>
                <table class="tb_banve_thongtinkhachhang" cellpadding="4">
                    <tr>
                        <td>SĐT</td>
                        <td>
                            <input autocomplete="off"
                                    data-validation="custom"
                                    class="m_bottom_0 CustomerPhoneNumber isNumber" type="text" value="{{old('phoneNumber')}}" name="phoneNumber"/>
                        </td>
                    </tr>
                    <tr>
                        <td>HỌ TÊN</td>
                        <td>
                            <input autocomplete="off" class="m_bottom_0 CustomerFullName" type="text" value="{{old('fullName')}}" name="fullName" />
                        </td>
                    </tr>

                    <tr>
                        <td>EMAIL</td>
                        <td>
                            <input autocomplete="off"
                                   type="email" data-validation-error-msg-email="Vui lòng nhập đúng định dạng"
                                   class="m_bottom_0"  value="{{old('email')}}" name="email" />
                        </td>
                    </tr>
                    @if(hasAnyRole(GET_LIST_AGENCY))
                        <tr>
                            <td>ĐẠI LÝ</td>
                            <td>
                                <select name="agencyUserId" id="cbbCreatedUser" class="span9 m_bottom_0 agency">
                                </select>
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td>GHI CHÚ</td>
                        <td>
                            <textarea autocomplete="off" id="txtNote" class="m_bottom_0" name="note"/>{{old('note')}}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>MÃ KM</td>
                        <td>
                            <div class="input-append">
                                <input autocomplete="off" class="m_bottom_0 span9" type="text"/>
                                <button type="button" class="btn btnCheckPromotionCode" id="btnCheckPromotionCode">CHECK</button>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td>Mã code</td>
                        <td>
                            <input type="text" name="foreignKey"
                                   id="txtforeignKey"
                                   class="span12 m_bottom_0 price">
                        </td>
                    </tr>
                    <tr>
                        <td class="center" colspan="2">
                            <div style="margin-left: 50px;" class="f_left m_right_10">
                                Gửi mã vé về số điện thoại
                            </div>
                            <input type="checkbox" name="sendSMS" id="send_code_to_phone" />
                            <label class="f_left" for="send_code_to_phone"></label>
                        </td>
                    </tr>
                    <tr>
                        <td class="center" colspan="2">
                            <div style="margin-left: 68px;" class="f_left m_right_10">
                                Cho phép in sau khi lưu
                            </div>
                            <input type="checkbox" id="print_after_save" name="print"/>
                            <label class="f_left" for="print_after_save"></label>
                        </td>
                    </tr>
                    <tr>
                        <td class="center" colspan="2">
                            <div style="margin-left: 35px;" class="f_left m_right_10">Thời gian giữ chỗ (phút)
                            </div>
                            <input type="number" name="timeBookHolder" value="{{array_get(session('companyInfo'),'timeBookHolder',50)}}" class="m_bottom_0 span3" min="0" />
                        </td>
                    </tr>
                    <tr>
                        <td>Đã Thu</td>
                        <td>
                            <input autocomplete="off"
                                   type="number" min="0" data-validation-error-msg-email="Vui lòng nhập đúng định dạng"
                                   class="m_bottom_0"  value="0{{old('paidMoney')}}" id="paidMoney" name="paidMoney" placeholder="VND"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="center" colspan="2">
                            <select name="paymentType" class="cbb_paymentType m_bottom_0 w_full">
                                {{--<option value="1">Thanh toán trực tuyến</option>--}}
                                <option selected value="3">Thanh toán tiền mặt tại quầy</option>
                                <option value="2">Lái xe thu tiền</option>
                                <option value="5">Thanh toán bằng tiền mặt tại Payoo</option>
                                <option value="6">Chuyển khoản</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="center" colspan="2">
                            <button class="btncustom w_full m_top_0 btn-submit" type="submit" name="submit" value="THANHTOAN">THANH TOÁN</button>
                        </td>
                    </tr>
                    <tr>
                        <td class="center" colspan="2">
                            <button class="btn btn-warning btn-flat-full btn-submit" id="giucho" type="submit" name="submit" value="GIUCHO">GIỮ CHỖ</button>
                        </td>
                    </tr>
                    <tr class="hidden">
                        <td class="p_0 ">
                            <input type="hidden" id="numberOfAdults" name="numberOfAdults" value="{{old('numberOfAdults')}}">
                            <input type="hidden" id="listSeatId" name="listSeat" value="{{old('listSeat')}}">
                            <input type="hidden" name="realPrice" id="txt_totalPrice" value="{{old('totalPrice')}}">
                            <input type="hidden" name="originalPrice" id="txtOriginalPrice" value="{{old('originalPrice')}}">
                            <input type="hidden" name="paymentTicketPrice" id="txtPaymentTicketPrice" value="{{old('paymentTicketPrice')}}">
                            <input type="hidden" name="getOffTimePlan" class="getOffTimePlan" value="{{old('getOffTimePlan')}}">
                            <input type="hidden" name="getInTimePlan" class="getInTimePlan" value="{{old('getInTimePlan')}}">
                            <input type="hidden" name="tripId"  class="tripId" value="{{request('tripId')}}">
                            <input type="hidden" name="scheduleId" class="scheduleId" value="{{old('scheduleId')}}">
                            <input type="hidden" name="startDate" class="startDate" value="{{old('startDate')}}">
                            <input type="hidden" name="routeId" class="routeId" value="{{old('routeId')}}">
                            <input type="hidden" name="index" class="index" value="{{request('index')}}">
                            <input  type="hidden" class="promotionId" name="promotionId" />
                            <input  type="hidden" class="promotionCode" name="promotionCode" />
                            <input  autocomplete="off" type="text" class="salePrice" name="salePrice"/>
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="submit" class="btn btn-primary btn_dongy w_full m_top_10" value="LƯU" />
                        </td>
                        <td>
                            <button type="reset" class="m_top_10 d_block" style="background: #fff;border: none;" href="">HỦY
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</form>
<script>


    $(document).ready(function () {

        $('#cbbCreatedUser').select2({
            placeholder: "Chọn đại lý",
            minimumInputLength: 2,
            ajax: {
                url: "{{action('AgencyController@getListAgency')}}",
                delay: 1000,
                dataType: 'json',
                data: function (params) {
                    var query = {
                        keyword: params.term
                    };
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.fullName,
                                id: item.userId
                            }
                        })
                    };
                },
                cache: true
            }
        });
        $("#checkPoint").change(function(){
            if(this.checked)
            {
                $("#getInPoint").hide();
                $("#getPickUpAddress").show();

                $("#getOffPoint").hide();
                $("#getDropOffAddress").show();
                return;
            }
            $("#getInPoint").show();
            $("#getPickUpAddress").hide();

            $("#getOffPoint").show();
            $("#getDropOffAddress").hide();
        });
        // $('#txt_totalPrice').change(function(){
        //     $('#paidMoney').val($(this).val());
        // });
    });

    function initMap() {
        /*Su dung o cho ban ve*/
        var pickUpInput = document.getElementById('pickUpAddress');
        var autocompletePick = new google.maps.places.Autocomplete(pickUpInput);
        autocompletePick.setComponentRestrictions(
            {'country': ['vn']});
        autocompletePick.addListener('place_changed', function () {
            var placePick = autocompletePick.getPlace();
            if (!placePick.place_id) {
                return;
            }
            var latPick = placePick.geometry.location.lat(),
                lngPick = placePick.geometry.location.lng();

            $("#pickUpLat").val(latPick);
            $("#pickUpLong").val(lngPick);

        });
        var dropOffInput = document.getElementById('dropOffAddress');
        var autocompleteDropOff = new google.maps.places.Autocomplete(dropOffInput);
        autocompleteDropOff.setComponentRestrictions(
            {'country': ['vn']});
        autocompleteDropOff.addListener('place_changed', function () {
            var placeDropOff = autocompleteDropOff.getPlace();
            if (!placeDropOff.place_id) {
                return;
            }
            var latDropOff = placeDropOff.geometry.location.lat(),
                lngDropOff = placeDropOff.geometry.location.lng();

            $("#dropOffLat").val(latDropOff);
            $("#dropOffLong").val(lngDropOff);

        });
        /*Ket thuc phan ban ve*/

        /*Su dung cho sua ve*/
        var pickUpEdit = document.getElementById('pickUpAdd');
        var autocompletePickEdit = new google.maps.places.Autocomplete(pickUpEdit);
        autocompletePickEdit.setComponentRestrictions(
            {'country': ['vn']});
        autocompletePickEdit.addListener('place_changed', function () {
            var placePick = autocompletePickEdit.getPlace();
            if (!placePick.place_id) {
                return;
            }
            var latPick = placePick.geometry.location.lat(),
                lngPick = placePick.geometry.location.lng();

            $("#pickUpLatt").val(latPick);
            $("#pickUpLongt").val(lngPick);

        });
        var dropOffEdit = document.getElementById('pickOffAdd');
        var autocompleteDropEdit = new google.maps.places.Autocomplete(dropOffEdit);
        autocompleteDropEdit.setComponentRestrictions(
            {'country': ['vn']});
        autocompleteDropEdit.addListener('place_changed', function () {
            var placeDropOff = autocompleteDropEdit.getPlace();
            if (!placeDropOff.place_id) {
                return;
            }
            var latDropOff = placeDropOff.geometry.location.lat(),
                lngDropOff = placeDropOff.geometry.location.lng();

            $("#dropOffLatt").val(latDropOff);
            $("#dropOffLongt").val(lngDropOff);

        });
        /*Ket thuc sua ve sua ve*/
    }

</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key={!! API_GOOGLE_KEY !!}&callback=initMap&libraries=places&language=vn"
        type="text/javascript"></script>