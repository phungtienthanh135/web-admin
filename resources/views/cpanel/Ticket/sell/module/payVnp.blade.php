@extends('cpanel.template.layout')
@section('title', 'Danh sách chuyến đi')
@section('content')
    <link href="/public/bootstrap/css/bootstrap-grid.min.css" type="text/css" rel="stylesheet">
    <div id="content">
        <div class="heading_top">
            <div class="row">
                <div class="col-md-12">
                    THANH TOÁN QUA VNP
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($banks as $bank)
                <a class="col-md-3" style="padding:10px;margin-bottom: 20px;" href="https://dobody-anvui.appspot.com/vnp/pay?vnp_OrderInfo={{ $ticketId }}&packageName=phanmemnhaxe.com&bankCode={{$bank['bankCode']}}&companyId={{session('companyId')}}">
                    <img src="{{$bank['logo']}}" alt="" class="img-responsive" style="margin: auto">
                    <div class="text-center" style="font-weight: bold;">{{ $bank['name'] }}</div>
                </a>
            @endforeach
        </div>
    </div>

@endsection