<style>
    .popover {
        z-index: 215000000 !important;
    }
</style>
<div class="modal modal-custom hide fade" id="schedulePlus">
    <div class="modal-header center">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>TẠO LỊCH TĂNG CƯỜNG</h3>
    </div>
    <div class="modal-body" style="overflow-y: visible;">
        <div class="row-fluid">
            <div class="form-horizontal row-fluid">
                <div class="span6">
                    {{--dữ liệu từ change seatmap đổ vào--}}
                    <div class="form-group">
                        <label for="">Tuyến</label>
                        <select name="" id="routeSchedulePlus"></select>
                    </div>
                    <div class="form-group">
                        <label for="">Ngày</label>
                        <input type="text" id="dateSchedulePlus" required class="span12" placeholder="dd-mm-yy">
                    </div>
                    <div class="form-group">
                        <label for="">Tài Xế</label>
                        <select name="" id="driveSchedulePlus" class="span12 listDriveOption" multiple></select>
                    </div>
                </div>
                <div class="span6">
                    <div class="form-group">
                        <label for="">Xe</label>
                        <select name="" id="vehicleSchedulePlus" class="span12 listVehicleOption"></select>
                    </div>
                    <div class="form-group">
                        <label for="">Giờ Chạy</label>
                        <input type="text" id="timeStartSchedulePlus" required class="span12" placeholder="hh:mm">
                    </div>
                    <div class="form-group">
                        <label for="">Phụ Xe</label>
                        <select name="" id="assSchedulePlus" class="span12 listAssOption" multiple></select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="modal-footer">
            <a style="cursor: pointer" data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
            <button type="button" id="btnSchedulePlusSend"
                    class="btn btn-primary">ĐỒNG Ý
            </button>
        </div>

    </div>
</div>

<script>
    $(document).ready(function(){
        initSchedulePlus();
        $('body').on('click','.btnSchedulePlus',function(){
            $('#dateSchedulePlus').val($(datepickerBox).val());
            $('#schedulePlus').modal('show');
        });
        $('body').on('click','#btnSchedulePlusSend',function(){
            if($('#dateSchedulePlus').val()===''){
                Message('Cảnh báo' , 'Vui lòng chọn ngày','');
                $('#dateSchedulePlus').focus();
                return false;
            }
            if($('#timeStartSchedulePlus').val()===''){
                Message('Cảnh báo' , 'Vui lòng chọn giờ','');
                $('#timeStartSchedulePlus').focus();
                return false;
            }
            $('#btnSchedulePlusSend').prop('disabled',true);
            var listDate = [];
            listDate.push($('#dateSchedulePlus').val());
            $.ajax({
                url : '{{ action('ScheduleController@postAdd') }}',
                dataType : 'json',
                type : 'post',
                data : {
                    _token : '{{ csrf_token() }}',
                    scheduleType : 1 ,
                    startDate : $('#dateSchedulePlus').val(),
                    startTime : $('#timeStartSchedulePlus').val(),
                    routeId : $('#routeSchedulePlus').select2('val'),
                    listAssistantId : $('#assSchedulePlus').select2('val'),
                    listDriverId : $('#driveSchedulePlus').select2('val'),
                    vehicleId : $('#vehicleSchedulePlus').select2('val'),
                    listDate : listDate,
                },
                success : function (data) {
                    if(data.code==200){
                        Message('Thông báo','Tạo chuyến tăng cường thành công','');
                        sessionStorage.setItem('tripId','tccc');
                        $('#ticketList').click();
                        datepickerBox.val($('#dateSchedulePlus').val());
                        updateSelectBoxTrip(false);
                        $('#schedulePlus').modal('hide');
                    }else{
                        Message('Lỗi','Tạo chuyến thất bại','');
                    }
                    $('#btnSchedulePlusSend').prop('disabled',false);
                } ,
                error : function (err) {
                    Message('Lỗi','Có lỗi xảy ra khi gửi yêu cầu','');
                    $('#btnSchedulePlusSend').prop('disabled',false);
                }
            });
        });
    });

    function generateListRoute(){
        html='';
        $.each(listRoute,function(i,e){
            html+='<option value="'+ e.routeId+'" '+(e.routeId===$('#chontuyen').val()?"selected":"")+'>'+e.routeName+'</option>';
        });
        $('#routeSchedulePlus').html(html);
    }

    function generateHtmlVehicle() {
        html='';
        $.each(listVehicle,function (i,e) {
            html+='<option value="'+e.vehicleId+'">'+e.numberPlate +' ('+e.numberOfSeats+' chỗ)</option>'
        });
        $('.listVehicleOption').html(html);
    }
    function generateHtmlDriver() {
        html='';
        $.each(listDriver,function (i,e) {
            html+='<option value="'+e.userId+'">'+e.fullName+'</option>'
        });
        $('.listDriveOption').html(html);
    }

    function generateHtmlAss() {
        html='';
        $.each(listAss,function (i,e) {
            html+='<option value="'+e.userId+'">'+e.fullName+'</option>'
        });
        $('.listAssOption').html(html);
    }

    function initSchedulePlus(){
        $('#routeSchedulePlus,#vehicleSchedulePlus,#driveSchedulePlus,#assSchedulePlus').select2({
            width: '100%',
            dropdownParent: $('#schedulePlus'),
            dropdownAutoWidth : true
        });
        $('#dateSchedulePlus').datepicker({
            autoclose: true,
            dateFormat: 'dd-mm-yy',
            zIndexOffset: 10000000,
        });
        $('#timeStartSchedulePlus').clockpicker({
            autoclose: true
        });
//        $('#dateSchedulePlus').val($('#txtCalendar').val());

//        $('#dateSchedulePlus').datepicker({
//            dateFormat : "dd-mm-yy"
//        });

        generateListRoute();
        generateHtmlVehicle();
        generateHtmlDriver();
        generateHtmlAss();
    }


</script>
