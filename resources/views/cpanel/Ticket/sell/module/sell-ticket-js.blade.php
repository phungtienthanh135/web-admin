<script>
    var
        tripSelected = {
            ticketPrice: 0,
            childrenTicketRatio: 0,
            tripData: {},
            tripId: '{{request('tripId')}}',
            scheduleId: '{{request('scheduleId')}}'
        },//biến toàn cục dùng để lưu thông tin chuyến khi đã chọn chuyến từ ds chuyến
        listTrip = [],//biến toàn cục dùng lưu ds chuyến khi đã chọn lịch
        datepickerBox = $("#txtCalendar"),//lịch
        selectBoxRoute = $('#chontuyen'),//selectbox chọn tuyến
        selectBoxListTrip = $('#listTrip'),//selectbox chọn tuyến
        txtCustomerPhoneNumber = $('.CustomerPhoneNumber'),//textbox số điện thoại khách hàng
        defaultDateForCalendar = '{{request('date',date('d-m-Y'))}}',//ngày mặc định nếu có param date
        urlSeachTrip = '{{action('TripController@search')}}',//url tìm lịch cho khách
        urlSeachTripById = '{{action('TripController@searchByTripId')}}',//url lấy thông tin chuyến
        urlGetPriceTicketGoods = '{{action('TicketController@getPriceTicketGoods')}}',//url lấy giá vé gửi đồ
        urlCheckPromotionCode = '{{action('CouponController@check')}}',//url lấy thông tin mã khuyến mãi
        urlPrintTrip = '{{action('TripController@printVoidTicket')}}',//url in phơi vé
        urlUpdateStatusTicket = '{{action('TicketController@updateStatusTicket')}}',//url cập nhật thông tin vé
        urlSellTicket = '{{action('TicketController@postSell')}}',//url cập nhật thông tin vé
        route_name_selected = '',
        round_route = '', // tuyến khứ hồi
        urlSeatMapInTrip = '{{action('TripController@getSeatMapByTripId')}}', // url lấy seatmap
        urlGetInfoCustomer = '{{action('CustomerController@getInfoByPhoneNumber')}}',//url lấy thông tin khách hàng qua sdt
        listRoute = {!! json_encode($listRoute,JSON_PRETTY_PRINT) !!};//thông tin tuyến dùng để cập nhật ds điểm dừng
    var widthOfSeat;
    var tripRealtime = {};//luu tru realtime database
    var database = firebase.database();
    var base_price;
    var flashMoveSeat = false;
    var moveSeatInfo = {};
    var addSeat = {flag: false, ticketInfo: {}};
    var array_Sort_PickUp = [], array_Sort_DropOff = [], check_to_sort; // mảng sắp xếp trung chuyển, check radio input
    var Seat_Back_Array = [], listPrice = []; // mảng ghế , giá ghế khứ hồi
    var listRoundTrip = [];
    var totalPriceBack, total_Price = 0, trip_Id;
    var html_Round = '';
    var data_generate_listseat, TripData_Round;

    var listVehicle = {!! json_encode($listVehicle,JSON_PRETTY_PRINT) !!},
        listDriver = {!! json_encode($listDriver,JSON_PRETTY_PRINT) !!},
        listAss = {!! json_encode($listAss,JSON_PRETTY_PRINT) !!};

    (init)();

    function init() {
        datepickerBox.datepicker({
            defaultDate: defaultDateForCalendar,
            minDate: 0 - 0{{session('pastDaysToSellTicket')}},
            dateFormat: "dd-mm-yy",
            onSelect: function (dateStr) {
                $(this).change();
                var date_Str = '';
                for (var i = 0; i < 10; i++) {

                    if (i == 1 || i == 0) {
                        date_Str += dateStr.charAt(i + 3);
                    } else if (i == 3 || i == 4) {
                        date_Str += dateStr.charAt(i - 3);
                    }
                    else date_Str += dateStr.charAt(i);
                }

                $('#txtCalendarRoundTrip').datepicker("option", {
                    minDate: new Date(date_Str)
                });
            },
            numberOfMonths: 1
        });

        selectBoxRoute.select2({
            width: '100%'
        });


        selectBoxListTrip.select2({
            width: '100%'
        });


        $(document).ready(function () {
            close_menu();

//                js thêm vé
            $('body').on('click', '#themghe', function () {
                var ticketIdAdd = $('#txtTicketId').val();
                addSeat.flag = true;
                var seatId = $(this).data('seat-click');
                addSeat.ticketInfo = $.grep(tripSelected.tripData.seatMap.seatList, function (e, i) {
                    return e.seatId == seatId;
                })[0].ticketInfo[ticketIdAdd];
                $('#sell_ticket').modal('hide');
                Message('Chế độ thêm ghế', 'Chọn các ghế đang trống để thêm, Chọn ghế đã có vé để về chế độ thường', '');
                return false;
            });
//                end thêm vé
            $('body').on('click', '.upListDriver', function () {
                var transshipmentDriver = {
                        ticketId: '',
                        transshipmentPickUpDriver: '',
                        transshipmentDropOffDriver: ''
                    },
                    listDriver = [];
                if ('{{session('companyId')}}' == 'TC03m1MSnhqwtfE') {
                    if (document.getElementById("dskhachdon").offsetLeft > 0 && document.getElementById("dskhachtra").offsetLeft <= 0) {
                        var selector = document.querySelector('tbody#list-customer-pickup');
                        var listTicketId = (selector.querySelectorAll('tr td.ticketID'));
                        var transshipmentPickUpDriver = selector.querySelectorAll('tr td select.driverPickUp');
                        for (var i = 0; i < listTicketId.length; i++) {
                            transshipmentDriver.ticketId = $(listTicketId[i]).data('val');
                            transshipmentDriver.transshipmentPickUpDriver = $(transshipmentPickUpDriver[i]).attr('value');
                            if (transshipmentDriver.transshipmentPickUpDriver != '')
                                listDriver.push(JSON.parse(JSON.stringify(transshipmentDriver)));
                        }
                    }
                    if (document.getElementById("dskhachtra").offsetLeft > 0 && document.getElementById("dskhachdon").offsetLeft <= 0) {
                        var selector = document.querySelector('tbody#list-customer-dropoff');
                        var listTicketId = (selector.querySelectorAll('tr td.ticketID'));
                        var transshipmentDropOffDriver = selector.querySelectorAll('tr td select.driverDropOff');
                        for (var i = 0; i < listTicketId.length; i++) {
                            transshipmentDriver.ticketId = $(listTicketId[i]).data('val');
                            transshipmentDriver.transshipmentDropOffDriver = $(transshipmentDropOffDriver[i]).attr('value');
                            if (transshipmentDriver.transshipmentDropOffDriver != '')
                                listDriver.push(JSON.parse(JSON.stringify(transshipmentDriver)));
                        }
                    }
                } else {
                    var selector = document.querySelector('tbody#list-customer-pickup');
                    var listTicketId = (selector.querySelectorAll('tr td.ticketID'));
                    var transshipmentPickUpDriver = selector.querySelectorAll('tr td select.driverPickUp');
                    var transshipmentDropOffDriver = selector.querySelectorAll('tr td select.driverDropOff');
                    for (var i = 0; i < listTicketId.length; i++) {
                        transshipmentDriver.ticketId = $(listTicketId[i]).data('val');
                        transshipmentDriver.transshipmentPickUpDriver = $(transshipmentPickUpDriver[i]).attr('value');
                        transshipmentDriver.transshipmentDropOffDriver = $(transshipmentDropOffDriver[i]).attr('value');
                        if (transshipmentDriver.transshipmentPickUpDriver != '' || transshipmentDriver.transshipmentDropOffDriver != '')
                            listDriver.push(JSON.parse(JSON.stringify(transshipmentDriver)));
                    }
                }
                //console.log(listDriver);
                $.post('{{action('TicketController@addtransshipment')}}', {
                    _token: '{{csrf_token()}}',
                    list: listDriver
                }, function (data) {
                    Message("Thông báo", "Gửi yêu cầu thành công!");

                });
            });

            $('body').on('change', 'select.driver', function () {
                var value = $(this).children(":selected").attr('value');
                $(this).attr('value', value);

            });
            //an hien ds ve huy va het hạn
            $('body').on('click', '.nav-list-ticket', function () {
                $('.nav-list-ticket').removeClass('title-list-ticket-active');
                $(this).addClass('title-list-ticket-active');
                var classShow = '.' + $(this).data('item-show');
                $('.item-list-ticket').hide();
                $(classShow).show();
                return false;
            });

            $('body').on('change', '.check', function () {
                if ($(this).attr('id') == 'cbListPick') {
                    $('body').on('click', '.sortpick', function () {
                        var list_input = $('#list-customer-pickup').find('.inputSortpick');
                        //console.log(list_input);
                        for (var i = 0; i < list_input.length; i++)
                            if ($(list_input[i]).val() == '') {
                                $(list_input[i]).attr('value', 0);
                                array_Sort_PickUp[$(list_input[i]).data('id') - 1] = 0;
                            }
                        if (array_Sort_PickUp.length != 0) {
                            var min, array_Sorted = [], listtr = [], a, i, j;
                            for (i = 0; i < array_Sort_PickUp.length; i++)
                                array_Sorted[i] = array_Sort_PickUp[i];
                            for (i = 0; i < array_Sorted.length - 1; i++)
                                for (j = i + 1; j < array_Sorted.length; j++)
                                    if (array_Sorted[j] < array_Sorted[i]) {
                                        a = array_Sorted[i];
                                        array_Sorted[i] = array_Sorted[j];
                                        array_Sorted[j] = a;
                                    }
                            var list_tr = $('#list-customer-pickup').find('.tr_sortpick');
                            for (j = 0; j < list_tr.length; j++)
                                for (i = 0; i < list_tr.length; i++)
                                    if (j == ($(list_tr[i]).data('id') - 1)) listtr[j] = list_tr[i].innerHTML;
                            $('#list-customer-pickup').html('');
                            for (i = 0; i < array_Sort_PickUp.length; i++) {
                                min = array_Sorted[i];
                                for (j = 0; j < array_Sort_PickUp.length; j++) {
                                    if (array_Sort_PickUp[j] == min) {
                                        var tr = "<tr class='tr_sortpick' data-id='" + (j + 1) + "'>" + listtr[j] + "</tr>";
                                        $('#list-customer-pickup').append(tr);
                                        array_Sort_PickUp[j] -= 1;
                                        j = array_Sort_PickUp.length;
                                    }
                                }
                            }
                            var foot = "<tr class='trsortpick'><td><button class='sortpick'>Sắp xếp</button></td>";
                            if ('{{session('companyId')}}' == 'TC03m1MSnhqwtfE') foot += "<td></td><td></td><td></td><td></td><td></td>";
                            else if ('{{session('companyId')}}' == 'TC0481HSc6Pl7ko') foot += "<td></td><td></td><td></td><td></td><td></td><td></td>";
                            else foot += "<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>";
                            foot += "<td><button class='upListDriver' style='width: 100%; background-color: #2E6BD0;border: none ;font-weight: bold; '>Chốt danh sách</button></td></tr>";

                            $('#list-customer-pickup').append(foot);
                            for (i = 0; i < array_Sort_PickUp.length; i++) {
                                array_Sort_PickUp[i] += 1;

                            }
                        }
                    });
                    $('body').on('change', '.inputSortpick', function () {
                        if (parseInt($(this).val()) < 0) {
                            alert('Hãy nhập số dương');
                            $(this).attr('value', 0);
                        } else $(this).attr('value', $(this).val());
                        array_Sort_PickUp[$(this).data('id') - 1] = parseInt($(this).val());


                    });
                }
                if ($(this).attr('id') == 'cbListDrop') {
                    $('body').on('click', '.sortdrop', function () {
                        var list_input = $('#list-customer-dropoff').find('.inputSortDrop');

                        for (var i = 0; i < list_input.length; i++)
                            if ($(list_input[i]).val() == '') {
                                $(list_input[i]).attr('value', 0);
                                array_Sort_DropOff[$(list_input[i]).data('id') - 1] = 0;
                            }
                        if (array_Sort_DropOff.length != 0) {
                            var min, array_Sorted = [], listtr = [], i, j;
                            for (var i = 0; i < array_Sort_DropOff.length; i++)
                                array_Sorted[i] = array_Sort_DropOff[i];
                            for (i = 0; i < array_Sorted.length - 1; i++)
                                for (j = i + 1; j < array_Sorted.length; j++)
                                    if (array_Sorted[j] < array_Sorted[i]) {
                                        a = array_Sorted[i];
                                        array_Sorted[i] = array_Sorted[j];
                                        array_Sorted[j] = a;
                                    }
                            var list_tr = $('#list-customer-dropoff').find('.tr_sortdrop');
                            for (j = 0; j < list_tr.length; j++)
                                for (i = 0; i < list_tr.length; i++)
                                    if (j == ($(list_tr[i]).data('id') - 1)) listtr[j] = list_tr[i].innerHTML;

                            $('#list-customer-dropoff').html('');
                            for (i = 0; i < array_Sort_DropOff.length; i++) {
                                min = array_Sorted[i];
                                for (j = 0; j < array_Sort_DropOff.length; j++) {
                                    if (array_Sort_DropOff[j] == min) {
                                        var tr = "<tr class='tr_sortdrop' data-id='" + (j + 1) + "'>" + listtr[j] + "</tr>";
                                        $('#list-customer-dropoff').append(tr);
                                        array_Sort_DropOff[j] -= 1;
                                        j = array_Sort_DropOff.length;
                                    }
                                }
                            }
                            var foot = "<tr class='trsortdrop'><td><button class='sortdrop'>Sắp xếp</button></td>";
                            if ('{{session('companyId')}}' == 'TC03m1MSnhqwtfE') foot += "<td></td><td></td><td></td><td></td><td></td>";
                            else if ('{{session('companyId')}}' == 'TC0481HSc6Pl7ko') foot += "<td></td><td></td><td></td><td></td><td></td><td></td>";
                            else foot += "<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>";
                            foot += "<td><button class='upListDriver' style='width: 100%; background-color: #2E6BD0;border: none ;font-weight: bold; '>Chốt danh sách</button></td></tr>";

                            $('#list-customer-dropoff').append(foot);
                            for (i = 0; i < array_Sort_DropOff.length; i++) {
                                array_Sort_DropOff[i] += 1;

                            }

                            // console.log(listtr);
                            // console.log(array_Sorted);
                            // console.log(array_Sort);
                        }
                    });
                    $('body').on('change', '.inputSortdrop', function () {
                        if (parseInt($(this).val()) < 0) {
                            alert('Hãy nhập số dương');
                            $(this).attr('value', 0);
                        } else $(this).attr('value', $(this).val());
                        array_Sort_DropOff[$(this).data('id') - 1] = parseInt($(this).val());


                    });
                }
            });
            $(document).on("change, keyup", "#totalPrice", $('#txt_totalPrice').val($('#totalPrice').val()));

            /*
            * lấy dữ liệu routeId từ session data
            * */

            var routeId = '{{request('routeId')}}';

            if (routeId == '') {
                routeId = sessionStorage.getItem("routeId");
            }

            if (routeId != null) {
                selectBoxRoute.val(routeId).change();
            }


            $('#btnStartTrip').click(function () {
                $('#modalXuatben').modal('show');
                return false;

            });
            $('#btnOpenTrip').click(function () {
                $('#modalMoXuatben').modal('show');
                return false;

            });
            $('#doStartTrip').click(function () {
                var sendSMS = $('#sendSMSStartTrip').is(':checked') ? true : false;
                if (tripSelected.tripData.tripStatus != 2) {
                    if (tripSelected.tripData.tripId.length < 3) {
                        Message('Lỗi !', "chuyến chưa được khởi tạo", '');
                    } else {
                        $.ajax({
                            url: "{{ action('TripController@startTrip') }}",
                            dataType: "json",
                            data: {
                                tripId: tripSelected.tripData.tripId,
                                sendSMS: sendSMS,
                                _token: '{{ csrf_token() }}'
                            },
                            type: "GET",
                            async: false,
                            success: function (data) {
                                startTripFunction(data);
                            },
                            error: function (data) {
                                Message("Thông báo", "Gửi yêu cầu thất bại!", "");
                            }
                        });
                    }
                } else {
                    Message('Thông báo !', "chuyến đã xuất bến", '');
                }
                $('#modalXuatben').modal('hide');
            });
            $('#doOpenStartTrip').click(function () {
                if (tripSelected.tripData.tripStatus == 2) {
                    if (tripSelected.tripData.tripId.length < 3) {
                        Message('Lỗi !', "chuyến chưa được khởi tạo", '');
                    } else {
                        $.ajax({
                            url: "{{ action('TripController@openStartTrip') }}",
                            dataType: "json",
                            data: {
                                tripId: tripSelected.tripData.tripId,
                                _token: '{{ csrf_token() }}'
                            },
                            type: "GET",
                            async: false,
                            success: function (data) {
                                startTripFunction(data);
                            },
                            error: function (data) {
                                Message("Thông báo", "Gửi yêu cầu thất bại!", "");
                            }
                        });
                    }
                } else {
                    Message('Thông báo !', "chuyến chưa xuất bến", '');
                }
                $('#modalMoXuatben').modal('hide');
            });

            /*
             * đạt vé thành công sẽ trả lại param action
             * thực hiện lệnh print nếu đã chọn checkbox in sau khi đạt vé action=print,
             * đã tuỳ biến lại plugin printPage()
             * */
            @if(request('action')=='print')
            $('#printTicket').printPage();
            $('#printTicket').click();
            @endif

            /*
            * Tự động chọn vé theo ticketId trên url
            * */
            var ticketId = "{{request('ticketId')}}";

            if (ticketId != '') {
                $(selectBoxRoute).val("{{request('routeId')}}").change();
                tripSelected.tripId = "{{request('tripId')}}";
                $(datepickerBox.datepicker("setDate", "{{request('date')}}"));
                updateListPoint("{{request('routeId')}}", true);
                $('#cbb_InPoint').val("{{request('startPointId')}}").change();
                $('#cbb_OffPoint').val("{{request('endPointId')}}").change();
            } else {
                updateListPoint($(selectBoxRoute).val(), true);
            }


            $('#loading').addClass('loading');


            selectBoxRoute.on('select2:select', function (e) {
                Loaing(true);
                reset();
                updateListPoint($(this).val(), false);
                saveFormDataLocal();
                ChangeUrl();
            });

            $(".oneDayBack").click(function (e) {
                $(this).next().datepicker('setDate', 'c-1d').change();
            });

            $(".oneDayFwd").click(function (e) {
                $(this).prev().datepicker('setDate', 'c+1d').change();
            });

            $(datepickerBox).change(function () {
                reset();
                updateSelectBoxTrip(true);
                saveFormDataLocal();
                ChangeUrl();
            });


            $('.CustomerPhoneNumber').bind('paste', function (e) {
                var pastedData = e.originalEvent.clipboardData.getData('text');
                var val = pastedData.replace(/[^0-9]/gi, '');
                $(this).val(val);
                e.preventDefault();
            });

            /*
            * Hiện danh sách hành khách
            * */
            $('#customerList').click(function () {
                $('.khung_lich_ve').hide();
                $('.khung_khach_hang').show();
                $(this).hide();
            });
            /*
            * Hiện danh sách ghế bán vé
            * */
            $('#ticketList').click(function () {
                $('.khung_lich_ve').show();
                $('.khung_khach_hang').hide();
                $('#customerList').show();
                $(this).hide();
            });

            /*
            * khi chọn chuyến
            * */
            selectBoxListTrip.on('change', function (e) {
                if ($(this).val() === 'tccc') {
                    refreshUrl();
                    if (sessionStorage.getItem('tripId') !== '') {
                        updateSelectBoxTrip(false);
                    }
                    sessionStorage.setItem('tripId', '');
                    sessionStorage.setItem('scheduleId', '');
                    initChangeVehicleForm();
                    generateHtmlAllTrip();
                } else {
                    var _tripId = $(this).val();
                    var scheduleId = $(this).children('option:selected').data('schedule');
                    Loaing(true);
                    updateTripInfo(_tripId, scheduleId);
                    saveFormDataLocal();
                    ChangeUrl();
                    //$("html, body").animate({scrollTop: $(window).height()}, "slow");
                }
            });

            /*
            * sự kiện khi chọn ghế để bán vé
            * */
            $('body').on('click', '[data-schedule-go-to]', function () {
                $('[data-schedule-go-to]').css('cursor', 'progress');
                var scheduleId = $(this).data('schedule-go-to');
                selectBoxListTrip.find("[data-schedule='" + scheduleId + "']").attr("selected", "selected").trigger('change');
//                    $('[data-schedule-go-to]').css('cursor','default');
            });

            $('body').on('click', '.seatChecked', function () {
                if($(this).parents('.ghe').hasClass('ghegiucho')){//chỉ thực hiện khi dã có vé
                    $(this).parents('.ghe').css('cursor','progress ');
                    var isChecked = $(this).hasClass('isChecked')?false:true;
                    var ticketIdCheck = $(this).parents('[data-ticketid]').data('ticketid');
                    $.ajax({
                        url :'{{ action('TicketController@updateCheckTicket') }}',
                        dataType:'json',
                        data : {tripId : tripSelected.tripData.tripId,isChecked : isChecked,ticketId:ticketIdCheck,_token:'{{ csrf_token() }}'},
                        type:'post',
                        success : function (data) {
                            if(data.code==200){
                                Message('Thông Báo', 'Cập nhật kiểm vé thành công','');
                            }else{
                                Message('Thông Báo', 'Cập nhật kiểm vé thất bại','');
                            }
                            $(this).parents('.ghe').css('cursor','progress');
                        },
                        error : function (data) {
                            $(this).parents('.ghe').css('cursor','default');
                        }
                    });
                }

            });

            $('body').on('click', 'div.ghe', function (e) {
                var id = $(this).data('ticketid');
                if (flashMoveSeat) {
                    if (id == 'undefined') {
                        $(this).toggleClass('ghechuyenden');
                        if ($('.ghechuyenden').length == moveSeatInfo.listSeatOld.length && moveSeatInfo.listSeatOld.length > 0) {
                            var listSeatNew = [];
                            $.each($('.ghechuyenden'), function (i, e) {
                                listSeatNew.push($(e).find('.seatId').text());
                            });
                            flashMoveSeatFunction(moveSeatInfo.listSeatOld, listSeatNew);
                            flashMoveSeat = false;
                        }
                    } else {
                        if (moveSeatInfo.ticketId != id) {
                            $('.ghe').removeClass('ghechuyenden');
                            flashMoveSeat = false;
                            $(this).click();
                        } else {
                            $(this).toggleClass('ghedangchon');
                            $(this).toggleClass('ghedichuyen');
                            var listSeatOld = [];
                            $.each($('.ghedichuyen'), function (i, e) {
                                listSeatOld.push($(e).find('.seatId').text());
                            });
                            moveSeatInfo.listSeatOld = listSeatOld;

                            if (moveSeatInfo.listSeatOld.length == 0) {
                                flashMoveSeat = false;
                            }
                            if (moveSeatInfo.listSeatOld.length == $('.ghedichuyen').length && moveSeatInfo.listSeatOld.length.length > 0) {
                                var listSeatNew = [];
                                $.each($('.ghechuyenden'), function (i, e) {
                                    listSeatNew.push($(e).find('.seatId').text());
                                });

                                flashMoveSeatFunction(moveSeatInfo.listSeatOld, listSeatNew);
                                flashMoveSeat = false;
                            }
                        }
                    }

                } else {

                    if ($('.ghedangchon').data('ticketid') != id) {
                        $('.ghe').removeClass('ghedangchon');
                        $('.ghe').removeClass('istrue');
                        $('.ghe').removeClass('ghedichuyen');
                        if (addSeat.flag == true && !$(this).hasClass('ghetrong')) {
                            addSeat.flag = false;
                            addSeat.ticketInfo = {};
                            Message('Rời khỏi chế độ thêm ghế!', "bạn đang ở chế độ bình thường!", '');
                        }
                    }
                    if ($(this).hasClass("ghedadat") || $(this).hasClass("ghegiucho")) {
                        if ($(this).hasClass("istrue")) {
                            selectSeat(this);
                        } else {

                            jQuery.each($("[data-ticketid='" + id + "']"), function (k, v) {
                                selectSeat(v);
                                $(v).addClass("istrue");
                            });
                        }
                    }
                    if ($(this).hasClass("ghetrong")) {
                        selectSeat(this);
                    }
                    updatePrice();
                }
            });

            /*
            * sự kiện khi thay đổi điểm lên hoặc điểm xuống
            * */
            $('body').on('change', '#cbb_InPoint,#cbb_OffPoint', function (e) {
                if ($(e.currentTarget).is('#cbb_InPoint')) {
                    var index = $("#cbb_InPoint option:selected").index();
                    var query = document.querySelector('select#cbb_OffPoint');
                    var listOp = query.querySelectorAll('option');
                    for (var i = 0; i < listOp.length; i++) {
                        if (i < index) listOp[i].hidden = true;
                        else listOp[i].hidden = false;
                    }
                    if ($("#cbb_OffPoint option:selected").index() < index) listOp[index].selected = true;
                }
                updateSelectBoxTrip(false, function () {
                    if (listTrip.length === 0) {
                        Message('Cảnh báo', 'Hiện tại tuyến này không bán vé!', '');
                        listSeat = [];
                        $('input:text').not(datepickerBox).val('');
                        $('#tang1 .khungxe,#tang2 .khungxe').html('');
                        $('#listSeat,.InTime,.OffTime,.routeName').val('');
                        $('#lb_numberOfAdults').val('0');
                        $('#numberOfAdults,#numberOfChildren').val('0');
                        $('#tongGiaVeGuiDo,#giaVeGuiDoGoc').text(moneyFormat(0));
                        $('#ticketPrice,#totalPrice').val(moneyFormat(0));
                        $('#isMeal,#isInsurrance,#isPickUpHome').hide();
                    }
                });
                saveFormDataLocal();

            });


            /*
            * sự kiện nhập mã khuyến mãi lượt đi
            * */
            $('.btnCheckPromotionCode').click(function () {
                // $('#tongGiaVeGuiDo').text('Đang cập nhật giá...');
                var btn = this;

                if ($(btn).text() === 'X') {
                    $('.salePrice').val(0);
                    $('.promotionId').val('');
                    $('.promotionCode').val('');
                    $(btn).prev('input').removeAttr('readonly').val('');
                    $(this).text('Kiểm tra');
                    updateInputPrice();
                } else {
                    if ($(this).prev('input').val() != '') {
                        $(btn).html('<img src="/public/images/loading/loader_blue.gif" width="19px">');
                        $(btn).prop('disabled', true);
                        $.ajax({
                            'url': urlCheckPromotionCode,
                            'data': {
                                'scheduleId': $('#txt_scheduleId').val(),
                                'promotionCode': $(this).prev('input').val()
                            },
                            'dataType': 'json',
                            'success': function (data) {
                                var result = data.result;
                                if (result.percent != undefined && result.price != undefined) {
                                    if (result.percent > 0) {
                                        $('.salePrice').val(result.percent * $('#originalPriceValue').val());
                                        /*Message('Thông báo', 'Kiểm tra mã khuyến mãi thành công<br>Được giảm: ' + result.percent * 100 + '%', '');*/
                                        $(btn).prev('input').val(result.promotionCode + ' - Giảm ' + result.percent * 100 + '%')
                                    }

                                    if (result.price > 0) {
                                        $('.salePrice').val(result.price);
                                        /*Message('Thông báo', 'Kiểm tra mã khuyến mãi thành công<br>Được giảm: ' + moneyFormat(result.price), '');*/
                                        $(btn).prev('input').val(result.promotionCode + ' - Giảm ' + moneyFormat(result.price))
                                    }

                                    $('.promotionId').val(result.promotionId);
                                    $('.promotionCode').val(result.promotionCode);
                                    $(btn).prev('input').attr('readonly', 'readonly');
                                    $(btn).text('X');
                                    $(btn).prop('disabled', false);
                                } else {
                                    Message('Thông báo', 'Mã khuyến mãi không hợp lệ', '');
                                    $(btn).text('Kiểm tra');
                                    $(btn).prop('disabled', false);
                                }
                                updateInputPrice();
                            },
                            'error': function () {
                                Message('Thông báo', 'Lấy mã khuyến mãi thất bại! Vui lòng tải lại trang.', '');
                                $(btn).text('Kiểm tra');
                                $(btn).prop('disabled', false);

                            }
                        });
                    }

                }


            });
            /*
                           * sự kiện nhập mã khuyến mãi khứ hồi
                           * */
            $('.btnCheckPromotionCodeRound').click(function () {
                // $('#tongGiaVeGuiDo').text('Đang cập nhật giá...');
                var btn = this;
                if ($(this).text() === 'X') {
                    $('#salePriceValRound').attr('value', 0);
                    $('#promotionIdRound').val('');
                    $('#promotionCodeRound').val('');
                    $(this).prev('input').removeAttr('readonly').val('');
                    $(this).text('Kiểm tra');
                    updateInputPrice();
                } else {
                    if ($(this).prev('input').val() != '') {
                        $(this).html('<img src="/public/images/loading/loader_blue.gif" width="19px">');
                        $(this).prop('disabled', true);
                        $.ajax({
                            'url': urlCheckPromotionCode,
                            'data': {
                                'scheduleId': $('#scheduleIdRound').attr('value'),
                                'promotionCode': $(btn).prev('input').val()
                            },
                            'dataType': 'json',
                            'success': function (data) {
                                var result = data.result;
                                if (result.percent != undefined && result.price != undefined) {
                                    if (result.percent > 0) {
                                        $('#salePriceValRound').val(result.percent * $('#originalPriceValueRound').val());
                                        /*Message('Thông báo', 'Kiểm tra mã khuyến mãi thành công<br>Được giảm: ' + result.percent * 100 + '%', '');*/

                                        $(btn).prev('input').val(result.promotionCode + ' - Giảm ' + result.percent * 100 + '%');

                                    }

                                    if (result.price > 0) {
                                        $('#salePriceValRound').val(result.price);
                                        /*Message('Thông báo', 'Kiểm tra mã khuyến mãi thành công<br>Được giảm: ' + moneyFormat(result.price), '');*/
                                        $(btn).prev('input').val(result.promotionCode + ' - Giảm ' + moneyFormat(result.price));
                                    }

                                    $('#promotionIdRound').val(result.promotionId);
                                    $('#promotionCodeRound').val(result.promotionCode);
                                    $(btn).prev('input').attr('readonly', 'readonly');
                                    $(btn).text('X');
                                    $(btn).prop('disabled', false);
                                } else {
                                    Message('Thông báo', 'Mã khuyến mãi không hợp lệ', '');
                                    $(btn).text('Kiểm tra');
                                    $(btn).prop('disabled', false);
                                }
                                updateInputPriceRound();
                            },
                            'error': function () {
                                Message('Thông báo', 'Lấy mã khuyến mãi thất bại! Vui lòng tải lại trang.', '');
                                $(btn).text('Kiểm tra');
                                $(btn).prop('disabled', false);

                            }
                        });
                    }

                }


            });

            /*
            * khi chọn loại thanh toán là payoo
            * */
            $('.cbb_paymentType').change(function () {
                if ($(this).val() == 5) {
                    $('iframe').attr('src', 'https://www.payoo.vn/diem-giao-dich');
                    $('#Payoo').show();
                } else {
                    $('#Payoo').hide();
                }
            });
            // sự kiện click nút in ticket
            $('body').on('click', 'button.btnPrintTicket', function () {
                printTicket($(this).val());
            });
            /*

            *sự kiện khi chọn bao gồm ăn hoặc bao gồm bảo hiểm
            * */
            $('#cb_priceInsurranceRound,#cb_isMealRound').change(function () {
                updateInputPriceRound();
            });
            $('#cb_priceInsurrance,#cb_isMeal').change(function () {
                updateInputPrice();
            });

            $('body').on('click', 'button.btnFlashMoveSeat', function () {
                var ticketId = $(this).parents('.ghe').data('ticketid');
                if ($('.ghedangchon').data('ticketid') != ticketId) {
                    flashMoveSeat = false;
                }
                flashMoveSeat = !flashMoveSeat;
                $('.ghe').removeClass('ghechuyenden');
                if (!$(this).parents('.ghe').hasClass("ghedangchon")) {
                    $(this).parents(".ghe").click();
                }
                var listSeatMove = [];
                $('.ghe').removeClass('ghedichuyen');
                if (flashMoveSeat) {
                    Message('Thông báo', 'chọn ghế mới hoặc bấm Ctrl + Q để hủy bỏ hành động', '');
                    $('.ghetrong').css('cursor', 'move');
                    $.each($('.ghedangchon'), function (i, e) {
                        $(e).addClass('ghedichuyen');
                        listSeatMove.push($(e).find('.seatId').text());
                    });
                } else {
                    $('.ghetrong').css('cursor', 'default');
                    $.each($('.ghedangchon'), function (i, e) {
                        $(e).removeClass('ghedichuyen');
                    });
                }
                moveSeatInfo.listSeatOld = listSeatMove;
                moveSeatInfo.tripOld = tripSelected.tripData.tripId;
                moveSeatInfo.ticketId = ticketId;
                return false;
            });

            $('body').on('click', '.cloneTicket', function () {
                if (flashMoveSeat == false) {
                    flashMoveSeat = true;
                    moveSeatInfo.listSeatOld = $(this).data('listseat').split(',');
                    moveSeatInfo.tripOld = $(this).data('tripid');
                    moveSeatInfo.ticketId = $(this).data('ticketid');
                    Message('Thông báo', 'chọn ghế mới hoặc bấm Alt + Q để hủy bỏ hành động', '');
                } else {
                    if (moveSeatInfo.ticketId == $(this).data('ticketid')) {
                        flashMoveSeat = false;
                        moveSeatInfo = {};
                    } else {
                        flashMoveSeat = true;
                        moveSeatInfo.listSeatOld = $(this).data('listseat').split(',');
                        moveSeatInfo.tripOld = $(this).data('tripid');
                        moveSeatInfo.ticketId = $(this).data('ticketid');
                        Message('Thông báo', 'chọn ghế mới hoặc bấm Alt + Q để hủy bỏ hành động', '');
                    }
                }
                return false;
            });

            /*Sự kiện khi click vào nút sửa vé*/
            $('body').on('click', 'button.editTicket', function (e) {
                var ticket_Id = $(this).attr('value');
                var ticketView = '';
                $('.Info_Round').hide();
                $.post('{{action('TicketController@getTicketInfo')}}', {
                    _token: '{{csrf_token()}}',
                    ticketId: ticket_Id
                }, function (data) {
                    console.log('ticketview', data);
                    ticketView = data.ticket;
                    if ((ticketView.ticketBack != undefined) && ((ticketView.ticketBack.tripId != undefined && ticketView.ticketBack.tripId != '') || (ticketView.ticketBack.scheduleId != undefined && ticketView.ticketBack.scheduleId != ''))) {
                        var date = new Date(parseInt(ticketView.ticketBack.getInTimePlan));
                        $('#tripDetail').val(ticketView.ticketBack.routeName);
                        $('#createDateRound').val(getFormattedDate(date, ''));
                        $('#ticketDetal').val(ticketView.ticketBack.listSeatId.toString());
                        $('.Info_Round').show();
                        sessionStorage.setItem('seatRound', ticketView.ticketBack.ticketId);
                    } else {
                        $('.Info_Round').hide();
                    }
                });
                $('#MoveRoundTrip').click(function () {
                    $('#sell_ticket').hide();
                    if ((ticketView.ticketBack != undefined) && ((ticketView.ticketBack.tripId != undefined && ticketView.ticketBack.tripId != '') || (ticketView.ticketBack.scheduleId != undefined && ticketView.ticketBack.scheduleId != ''))) {
                        $.ajax({
                            dataType: "json",
                            url: urlSeatMapInTrip,
                            data: {
                                'tripId': ticketView.ticketBack.tripId,
                                'scheduleId': ticketView.ticketBack.scheduleId,
                            },
                            async: false,
                            success: function (data) {
                                if (data.listGuestPerPoint) tripSelected.tripData.seatMap = data.seatMap;
                                else
                                    tripSelected.tripData.seatMap = data;


                            },
                            error: function () {
                                Message("Lỗi tải danh sách vé hủy!", "Vui lòng tải lại trang", "");
                                Loaing(false);
                            }
                        });
                        $('#sell_ticket').modal("hide");
                        base_price = tripSelected.tripData.ticketPrice;
                        var queryDate = $.format.date(ticketView.ticketBack.getInTimePlan, 'yyyy/MM/dd');
                        queryDate = queryDate.replace('/', '-').replace('/', '-');
                        dateParts = queryDate.match(/(\d+)/g);
                        realDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
                        $(datepickerBox).datepicker({dateFormat: 'yy-mm-dd'}); // format to show
                        $(datepickerBox).datepicker('setDate', realDate);
                        sessionStorage.setItem('tripIdRound', ticketView.ticketBack.tripId);
                        sessionStorage.setItem('timeTrip', ($.format.date(ticketView.ticketBack.getInTimePlan, 'dd/MM/yyyy')).replace('/', '-').replace('/', '-'));

                        var html = '', route, option;
                        $('#chontuyen option').each(function () {
                            if ($(this).text() == ticketView.ticketBack.routeName) {
                                option = $(this).val();
                                $.each(listRoute, function (k, v) {
                                    if (v.routeId == option) route = v;
                                });
                            }
                        });
                        $.each(listRoute, function (k, v) {
                            html += '<option ' + (v.routeId == route.routeId ? 'selected' : '') + ' data-startPoint="' + v.listPoint[0].pointId + '"data-endPoint="' + v.listPoint[v.listPoint.length - 1].pointId + '"value="' + v.routeId + '">' + v.routeName + '</option>';

                        });
                        $('#chontuyen').html(html);
                        reset();
                        var html_listInPoint = '';
                        var html_listOffPoint = '';
                        $(route.listPoint).each(function (key, item) {
                            if (key == 0 || key == route.listPoint.length - 1) {
                                if (key == 0) {
                                    html_listInPoint += '<option value="' + item.pointId + '"' + (ticketView.ticketBack.getInPoint.pointId == item.pointId ? 'selected' : '') + '>' + item.pointName + '</option>';
                                } else {
                                    html_listOffPoint += '<option value="' + item.pointId + '"' + (ticketView.ticketBack.getOffPoint.pointId == item.pointId ? 'selected' : '') + '>' + item.pointName + '</option>';
                                }
                            } else {
                                html_listInPoint += '<option value="' + item.pointId + '"' + (ticketView.ticketBack.getInPoint.pointId == item.pointId ? 'selected' : '') + '>' + item.pointName + '</option>';
                                html_listOffPoint += '<option value="' + item.pointId + '"' + (ticketView.ticketBack.getOffPoint.pointId == item.pointId ? 'selected' : '') + '>' + item.pointName + '</option>';
                            }
                        });
                        $('.startPoint').html(html_listInPoint);
                        $('.endPoint').html(html_listOffPoint);
                        tripSelected.tripId = ticketView.ticketBack.tripId;
                        tripSelected.tripData.tripId = ticketView.ticketBack.tripId;
                        updateSelectBoxTripRound();
                        generateHtmlTripInfo();
                        sessionStorage.setItem('timeTrip', '');
                        // sessionStorage.setItem('tripIdRound','');

                    }
                    else ;
                });
                $('#round-trip').hide();
                $('form#datve').attr('action', urlUpdateStatusTicket);
                $('.banve_thongtintinve').show();
                $('.modal-footer').show();
                $('.banve_lichsuve').hide();
                var ticketIdSeat = $(this).parents('.ghe').data('ticketid');
                if (!$(this).parents('.ghe').hasClass("ghedangchon")) {
                    $(this).parents(".ghe").click();
                }
                var seatIdCurrent = $(this).data('seatid');
                if (tripSelected.tripData.tripStatus == 2) {
                    $('#themghe').hide();
                } else {
                    $('#themghe').show();
                }
                $('#themghe').attr('data-seat-click', seatIdCurrent);
                var seat = $.grep(tripSelected.tripData.seatMap.seatList, function (item) {
                    return item.seatId == seatIdCurrent && (item.seatType == 3 || item.seatType == 4);
                })[0];
//                    var lastPointId = '';
//                    for (key in seat['ticketInfo']) {
//                        lastPointId = key;
//                    }

                var ticketInfo = seat['ticketInfo'][seat['listTicketId'][0]];
//                    if (ticketInfo['needPickUpHome']) {
//                        $('#cbGetPickUpAddress').prop('checked', true);
//                    } else {
//                        $('#cbGetPickUpAddress').prop('checked', false);
//                    }

                var ticketPriceValue = '';
                var listExtraPrice = '';
                var listExtraTK = '';
                var lengthItem = $('.ghedangchon').length;
                $('.ghedangchon').each(function (key, elem) {
                    var extraPrice = $(elem).data('extraprice');
                    var num = $.number(extraPrice + $('#ticketPrice').data("ticketprice"));
                    ticketPriceValue = ticketPriceValue + num;
                    listExtraPrice += extraPrice;
                    if (key != lengthItem - 1) {
                        ticketPriceValue += " , ";
                        listExtraPrice += ",";
                    }
                });
                $("[data-ticketid='" + ticketIdSeat + "']").each(function (i, e) {
                    listExtraTK += $(e).data('extraprice');
                    if (i != $("[data-ticketid='" + ticketIdSeat + "']").length - 1) {
                        listExtraTK += ",";
                    }
                });
//                    ẩn khứ hồi
                $('#divroundtrip').hide();
                //c gửi sms
                if (ticketInfo['sendSMS']) {
                    $('#send_code_to_phone').prop('checked', true);
                }
                var arr = [];//mảng ghế chọn
                $('.ghedangchon').each(function (k, v) {
                    arr.push($(v).find(".seatId").text());
                });
                $('#listSeat').text(arr.toString());
                $('#lb_numberOfAdults').val(ticketInfo['numberOfAdults'] + ' (' + $('#listSeat').text() + ')');
                $('#numberOfChildren').val(ticketInfo['numberOfChildren']);
                $('#numberOfAdults').val(ticketInfo['numberOfAdults']);
                $('#txtListExtraPrice').val(listExtraPrice);
                $('#txtListExtraTK').val(listExtraTK);
                $('#txtPaidMoney').val(formatNumber(ticketInfo['paidMoney']));
                $('#txtforeignKey').val(ticketInfo['foreignKey']);
                $('.CustomerPhoneNumber').val(ticketInfo['phoneNumber']);
//                    cập nhật lại điểm lên xuống của vé
                $('#dropOffLat').val(ticketInfo['latitudeDown']);
                $('#dropOffLong').val(ticketInfo['longitudeDown']);
                $('#pickUpLat').val(ticketInfo['latitudeUp']);
                $('#pickUpLong').val(ticketInfo['longitudeUp']);
                if (ticketInfo['pickUpAddress'] != undefined && ticketInfo['pickUpAddress'] != '') {
                    $('#pickUpAddress').val(ticketInfo['pickUpAddress']);
                    $('#rdpickUpAddress').prop('checked', true);
                } else {
                    if (ticketInfo['alongTheWayAddress'] != undefined && ticketInfo['alongTheWayAddress'] != '') {
                        $('#pickUpAddress').val(ticketInfo['alongTheWayAddress']);
                        $('#rdAlongTheWayAddress').prop('checked', true);
                    }
                }
                if (ticketInfo['dropOffAddress'] != undefined && ticketInfo['dropOffAddress'] != '') {
                    $('#dropOffAddress').val(ticketInfo['dropOffAddress']);
                    $('#rdDropOffAddress').prop('checked', true);
                } else {
                    if (ticketInfo['dropAlongTheWayAddress'] != undefined && ticketInfo['dropAlongTheWayAddress'] != '') {
                        $('#dropOffAddress').val(ticketInfo['dropAlongTheWayAddress']);
                        $('#rdDropAlongTheWayAddress').prop('checked', true);
                    }
                }

//                    $('#pickUpAddress').val(ticketInfo['pickUpAddress']);
//                    $('#dropOffAddress').val(ticketInfo['dropOffAddress']);
                $('.CustomerFullName').val(ticketInfo['fullName']);
                $('.CustomerEmail').val(ticketInfo['email']);
                $('#cbbCreatedUser').val(ticketInfo['agencyUserId']);
                $('#select2-cbbCreatedUser-container').text(ticketInfo['sourceName']);
                $('#txtNote').val(ticketInfo['note']);
                ticketInfo['priceInsurrance'] > 0 ? $('#cb_priceInsurrance').prop('checked', true) : $('#cb_priceInsurrance').prop('checked', false);
                ticketInfo['mealPrice'] > 0 ? $('#cb_isMeal').prop('checked', true) : $('#cb_isMeal').prop('checked', false);
//                    $('#alongTheWayAddress').val(ticketInfo['alongTheWayAddress']);
//                    $('#dropAlongTheWayAddress').val(ticketInfo['dropAlongTheWayAddress']);
                if (ticketInfo['mealPrice'] > 0) $('#isMeal').show();
                if (ticketInfo['priceInsurrance'] > 0) $('#isInsurrance').show();

                if (typeof ticketInfo.listPromotion != 'undefined' && typeof ticketInfo.listPromotion[0] != 'undefined') {
                    var txtPromotion = ticketInfo.listPromotion[0].promotionCode;
                    if (ticketInfo.listPromotion[0].price >= 0) {
                        txtPromotion += " - Giảm " + ticketInfo.listPromotion[0].price;
                    } else {
                        txtPromotion += " - Giảm " + (ticketInfo.listPromotion[0].percent * 100) + "%";
                    }
                    $('.txtPromotionCode').val(txtPromotion);
                    $('.promotionId').val(ticketInfo.listPromotion[0].promotionId);
                    $('.promotionCode').val(ticketInfo.listPromotion[0].promotionCode);
                    $('.btnCheckPromotionCode').prev('input').attr('readonly', 'readonly');
                    $('.btnCheckPromotionCode').hide();
                } else {
                    $('.txtPromotionCode').val('');
                    $('.promotionId').val('');
                    $('.promotionCode').val('');
                    $('.btnCheckPromotionCode').prev('input').removeAttr('readonly');
                    $('.btnCheckPromotionCode').text('Kiểm tra').show();
                }

                $('#realPrice').val(ticketInfo['agencyPrice']);
                $('#realPriceShow').val(formatNumber(ticketInfo['agencyPrice']));
                updateInputPrice();
                //console.log(ticketInfo);
                if (ticketInfo['ticketStatus'] == 3) {
                    $('#thanhtoan').hide();
                }
                else {
                    $('#thanhtoan').show();
                }


                $('.isSellTicket').hide();
                $('.isUpdateStatusTicket').show();
                if (tripSelected.tripData.tripStatus == 2) {
                    $('#huyve').hide();
                    $('#capnhat').hide();
                } else {
                    $('#huyve').show();
                    $('#capnhat').show();
                }

                $('#txtTicketId').val($(this).val());
                //showHideRoundTrip($('#txtRoundTrip'));
                $('#sell_ticket').modal('show');
                return false;
            });
            $('#MoveRoundTrip').click(function () {
                $('#sell_ticket').on('hidden', function () {
                    var listghe = document.getElementsByClassName('ghe');
                    if (sessionStorage.getItem('seatRound') != '') {
                        $.each(listghe, function (k, v) {
                            if ($(v).data('ticketid') == sessionStorage.getItem('seatRound')) {
                                $(v.parentElement.parentElement).addClass('active');
                                $(v).addClass('ghedangchon');
                                v.scrollIntoView();
                            }
                        });
                    }
                    sessionStorage.setItem('seatRound', '');
                });
            });
            $('#sell_ticket').on('hidden', function () {
                if ($('.ghetrong.ghedangchon').length <= 0) {
                    $('.ghedangchon').each(function (key, elem) {
                        selectSeat(elem);
                    });
                }

                resetInput();

            });
            $('body').on('click', 'button.btnSellTicket', function (e) {
                $('#total_Price_Back').val(0);
                if (addSeat.flag) {
                    $('.isAddSeat').show();
                    $('.isntAddSeat').hide();
                    $('#checkRoundTrip').hide();

                    $('.CustomerPhoneNumber').val(addSeat.ticketInfo['phoneNumber']);
                    $('#pickUpAddress').val(addSeat.ticketInfo['pickUpAddress']);
                    $('#dropOffAddress').val(addSeat.ticketInfo['dropOffAddress']);
                    $('.CustomerFullName').val(addSeat.ticketInfo['fullName']);
                    $('.CustomerEmail').val(addSeat.ticketInfo['email']);
                    $('#cbbCreatedUser').val(addSeat.ticketInfo['agencyUserId']);
                    $('#select2-cbbCreatedUser-container').text(addSeat.ticketInfo['sourceName']);
                    $('#txtNote').val(addSeat.ticketInfo['note']);
                    $('#alongTheWayAddress').val(addSeat.ticketInfo['alongTheWayAddress']);
                    $('#dropAlongTheWayAddress').val(addSeat.ticketInfo['dropAlongTheWayAddress']);
                } else {
                    $('#txtTicketId').val('');
                    $('.isAddSeat').hide();
                    $('.isntAddSeat').show();
                    $('#checkRoundTrip').show();

                    $('#txtNote').val('');
                }
                $('#txtRealPriceRound').val(0);
                $('#txtPaidMoneyRound').val(0);
                $('#txtMoneyOwingRound').val(0);
                $('form#datve').attr('action', urlSellTicket);
                $('.btnCheckPromotionCode').show();
                //$('#cbGetPickUpAddress').prop('checked', false);
                if (!$(this).parents('.ghe').hasClass("ghedangchon")) {
                    $(this).parents(".ghe").click();
                }
                var ticketPriceValue = '';
                var listExtraPrice = '', listExtraTK = '';
                var lengthItem = $('.ghedangchon').length;
                $('.ghedangchon').each(function (key, elem) {
                    var extraPrice = $(elem).data('extraprice');
                    var num = $.number(extraPrice + $('#ticketPrice').data("ticketprice"));
                    ticketPriceValue = ticketPriceValue + num;
                    listExtraPrice += extraPrice;
                    if (key != lengthItem - 1) {
                        ticketPriceValue += " , ";
                        listExtraPrice += ",";
                    }
                });
                $('#txtListExtraPrice').val(listExtraPrice);
                $('#txtPaidMoney').val(0);
                $('.isSellTicket').show();
//                    cập nhật tọa độ điểm đón trả
                $('#dropOffLat').val(tripSelected.tripData.latitudePointEnd);
                $('#dropOffLong').val(tripSelected.tripData.longitudePointEnd);
                $('#pickUpLat').val(tripSelected.tripData.latitudePointStart);
                $('#pickUpLong').val(tripSelected.tripData.longitudePointStart);
                $('#pickUpAddress').val('');
                $('#dropOffAddress').val('');

                $('.txtPromotionCode').val('');
                $('.promotionId').val('');
                $('.promotionCode').val('');
                $('.btnCheckPromotionCode').prev('input').removeAttr('readonly');
                $('.btnCheckPromotionCode').text('Kiểm tra').show();

                if (tripSelected.tripData.tripStatus == 2) {
                    $('#thanhtoan1').hide();
                    $('#giucho').hide();
                } else {
                    $('#thanhtoan1').show();
                    $('#giucho').show();
                }

                $('.isUpdateStatusTicket').hide();
                $('.salePrice').val(0);
                $('#realPrice').val(getTotalPrice());
                updateInputPrice();
                showHideRoundTrip($('#txtRoundTrip'));
                $("#sell_ticket").modal("show");
                return false;
            });


            //bán vé nhanh
            {{--$('body').on('click', '.btnFlashSell', function () {--}}
            {{--Loaing(true);--}}
            {{--if (!$(this).parents('.ghe').hasClass("ghedangchon")) {--}}
            {{--$(this).parents(".ghe").click();--}}
            {{--}--}}

            {{--var listSeatFlash = [],--}}
            {{--totalPrice = 0;--}}
            {{--$.each($('.ghedangchon'), function (i, e) {--}}
            {{--listSeatFlash.push($(e).find('.seatId').text());--}}
            {{--totalPrice += $(e).data('extraprice');--}}
            {{--});--}}
            {{--totalPrice = totalPrice + listSeatFlash.length * tripSelected.tripData.ticketPrice;//tính tổng giá vé--}}
            {{--$.ajax({--}}
            {{--url: "{{ action('TicketController@flashSell') }}",--}}
            {{--dataType: "json",--}}
            {{--type: "POST",--}}
            {{--data: {--}}
            {{--getInPointId: tripSelected.tripData.getInPointId,--}}
            {{--getOffPointId: tripSelected.tripData.getOffPointId,--}}
            {{--getInTimePlan: tripSelected.tripData.getInTime,--}}
            {{--getOffTimePlan: tripSelected.tripData.getOffTime,--}}
            {{--tripId: tripSelected.tripData.tripId,--}}
            {{--scheduleId: tripSelected.tripData.scheduleId,--}}
            {{--listSeatId: listSeatFlash.toString(),--}}
            {{--totalPrice: totalPrice,--}}
            {{--startDate: $('#txtCalendar').val(),--}}
            {{--_token: '{{ csrf_token() }}'--}}
            {{--},--}}
            {{--success: function (data) {--}}
            {{--Loaing(false);--}}
            {{--Message("Thông báo", data.results.message, '');--}}
            {{--if (tripSelected.tripData.tripId == -1) {--}}
            {{--location.reload();--}}
            {{--}--}}
            {{--},--}}
            {{--error: function (err) {--}}
            {{--Loaing(false);--}}
            {{--Message("Lỗi", "Gửi yêu cầu thất bại", '');--}}
            {{--}--}}
            {{--});--}}

            {{--return false;--}}
            {{--});--}}

            $('input#txtPaidMoney,#realPrice').keyup(function () {
                $('#txtMoneyOwing').val($.number(parseInt($('#realPrice').val()) - $('input#txtPaidMoney').val()));
            });
            $('input#txtPaidMoneyRound,#realPriceRound').keyup(function () {
                $('#txtMoneyOwingRound').val($.number(parseInt($('#realPriceRound').val()) - $('input#txtPaidMoneyRound').val()));
            });

            $('.btnPrintTrip').click(function () {
                if (tripSelected.tripData.tripId != '') {
                    $('.khung_lich_ve').hide();
                    $('.khung_khach_hang').show();
                    $('#customerList').hide();
                    $('#dskhach').hide();
                    $('#dskhachdon').hide();
                    $('#dskhachtra').hide();
                    $('#sodo').show();
                    $('#cbMap').prop("checked", true);
                    $('#sodo').printThis({
                        pageTitle: '&nbsp;'
                    });
                }

                else
                    Message('Cảnh báo', 'Vui lòng chọn chuyến trước khi in!', '')
            });

            $('#huyve').click(function () {
                var cancelReason = prompt('Lí do hủy vé');
                $('#txtCancelReason').val(cancelReason);
                if (cancelReason == null) {
                    return false;
                }
            });
            /*Sự kiện khi submit*/
            $("form").submit(function (e) {
                $('#loading').addClass('loading');
            });

            //                realtime ticket

            firebase.auth().signInWithEmailAndPassword("quochoanbk44@gmail.com", "123456789")
                .then(function (firebaseUser) {
                    changeRealtimeTrip(tripSelected.tripId);

                })
                .catch(function (error) {
                    // Error Handling
                });

            $("#phoneNumber").autocomplete({
                source: "{{ action('HomeController@searchPhone', ['count' => 2]) }}",
                delay: 500,
                cache: false
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                var inner_html = '';
                if (item.empty) {
                    inner_html = 'Không có dữ liệu';

                    return $("<li class='borderlist'></li>")
                        .data("item.autocomplete", item)
                        .append(inner_html)
                        .appendTo(ul);
                }
                var status;
                switch (item.ticketStatus) {
                    case 0:
                        status = "<span style='color: #F44336'>Đã hủy</span>"; //do
                        break;
                    case 1:
                        status = "<span style='color: #B71C1C'> Hết hạn giữ chỗ"; //do
                        break;
                    case 2:
                        if (item.overTime > Date.now() || item.overTime == 0)
                            status = "<span style='color: #FF9800'>Đang giữ chỗ</span>"; //cam
                        else
                            status = "<span style='color: #B71C1C'> Hết hạn giữ chỗ"; //do

                        break;
                    case 3:
                        status = "<span class='code'>Đã thanh toán</span>"; //xanh
                        break;
                    case 4:
                        status = "<span style='color: #0091EA'>Đã lên xe</span>"; //xanh

                        break;
                    case 5:
                        status = "Đã hoàn thành";

                        break;
                    case 6:
                        status = "<span style='color: #C62828'>Quá giờ giữ chỗ</span>"; //do

                        break;
                    case 7:
                        status = "<span style='color: #33691E'>Ưu tiên giữ chỗ</span>";

                        break;
                    default:
                        status = '';
                }

                inner_html = '<div class="row cus-info" data-phone="' + item.phoneNumber + '" ' +
                    'data-name="' + item.fullName + '"' +
                    'data-email="' + item.email + '"' +
                    'data-note="' + item.note + '"' +
                    'data-pickup="' + item.pickUpAddress + '"' +
                    'data-dropoff="' + item.dropOffAddress + '">';
                inner_html += '<div  class="col-md-12 padding40">KH: ' + item.fullName + ' - ' + item.phoneNumber + '</div>';
                inner_html += '<div class="col-md-12 padding40">Tuyến: ' + item.routeName + ' - ' + checkDate(item.getInTimePlan) + '</div>';
                inner_html += '<div class="col-md-12 padding40">Ghế: ';
                inner_html += item.listSeatId.toString();
                inner_html += '</div>';
                inner_html += '<div class="col-md-12 padding40">' + status + '</div>';
                inner_html += '<div class="col-md-12 padding40">Số vé đã đặt: ' + item.numberOfTickets + ' vé</div>';
                inner_html += '<div class="col-md-12 padding40">Số vé đã hủy: ' + item.numberCancelTicket + ' vé</div>';

                inner_html += '</div>';

                return $("<li class='borderlist ui-menu-item'></li>")
                    .data("item.autocomplete", item)
                    .append(inner_html)
                    .appendTo(ul);
            }

        });

        $('body').on('click', '.cus-info', function () {
            $('#ui-id-1').hide();
            var phone = $(this).data('phone');
            var name = $(this).data('name');
            var email = $(this).data('email');
            var note = $(this).data('note');
            var pickup = $(this).data('pickup');
            var dropoff = $(this).data('dropoff');

            $('#phoneNumber').val(phone);
            if (name !== 'undefined') {
                $('.CustomerFullName').val(name);
            }

            if (email !== 'undefined') {
                $('.CustomerEmail').val(email);
            }

            if (note !== 'undefined' && $('#txtNote').val() === '') {
                $('#txtNote').val(note);
            }

            if (pickup !== 'undefined' && $('#pickUpAddress').val() === '') {
                $('#pickUpAddress').val(pickup);
            }

            if (dropoff !== 'undefined' && $('#dropOffAddress').val() === '') {
                $('#dropOffAddress').val(dropoff);
            }

        });

        $('#phoneNumber').focus(function () {
            if ($(this).val() !== '') {
                $('#ui-id-1').show();
            }
        });

    }

    function checkDate(date) {
        var datetime = new Date(date);
        var dd = datetime.getDate();
        var mm = datetime.getMonth() + 1;
        var yyyy = datetime.getFullYear();
        var hour = datetime.getHours();
        var minutes = datetime.getMinutes();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        if (hour < 10) {
            hour = '0' + hour
        }

        if (minutes < 10) {
            minutes = '0' + minutes
        }

        datetime = dd + '-' + mm + '-' + yyyy + ' ' + hour + ':' + minutes;
        return datetime
    }

    function startTripFunction(data) {
        if (data.status == "success") {
            tripSelected.tripData.tripStatus = 2;
            generateHtmlTripInfo();
            if (typeof data.results.message !== 'undefined') {
                Message("Thông báo", data.results.message, "");
            }
        } else {
            Message("Lỗi", "Có lỗi xảy ra ! Vui lòng thử lại sau hoặc tải lại trang!", "");
        }
    }

    function flashMoveSeatFunction(listSeatOld, listSeatNew) {
        if (listSeatNew.length == listSeatOld.length && listSeatNew.length > 0) {
            Loaing(true);
            $.ajax({
                dataType: "json",
                url: "{{ action('TripController@postTransfer') }}",
                type: "POST",
                data: {
                    tripSourceId: moveSeatInfo.tripOld,
                    tripDestinationId: $('#listTrip').val(),
                    numberOfGuests: listSeatNew.length,
                    timeChange: new Date(),
                    place: $('#chontuyen option:selected').data('startpoint'),
                    scheduleId: $('#listTrip option:selected').data('schedule'),
                    listSeatSourceId: listSeatOld,
                    listSeatDestinationId: listSeatNew,
                    ticketId: moveSeatInfo.ticketId,
                    _token: '{{ csrf_token() }}'
                },
                success: function (data) {
                    refreshUrl();
                    Loaing(false);
                },
                error: function (err) {
                    Loaing(false);
                }
            });
        } else {
            Message("Lỗi", "Số ghế không bằng nhau", "");
        }
    }

    function changeRealtimeTrip(_tripId) {
        if (typeof tripRealtime.path != 'undefined') {
            tripRealtime.off();
        }
        tripRealtime = database.ref().child('trip').child(_tripId);
        tripRealtime.on('value', function (snapshot) {
            var data = snapshot.val();
            var check = 0;
            /// data.seatMap.setlist
            if (data) {
                if (typeof data.seatMap.seatList != 'undefined') {
                    for (var i = 0; i < (data.seatMap.seatList).length; i++) {
                        if (data.seatMap.seatList[i].listTicketId &&
                            (typeof  data.seatMap.seatList[i].ticketInfo == 'undefined')) {
                            check = 1;
                            break;
                        }
                    }
                }
            }
            if (check == 0 && data != null) {
                tripSelected.tripData.seatMap = data.seatMap;
                tripSelected.seatList = data.seatMap.seatList;
                tripSelected.numberOfRows = data.seatMap.numberOfRows;
                tripSelected.numberOfColumns = data.seatMap.numberOfColumns;
                tripSelected.numberOfFloors = data.seatMap.numberOfFloors;
                tripSelected.tripData.tripStatus = data.tripStatus;

            } else {
                $.ajax({
                    dataType: "json",
                    url: urlSeatMapInTrip,
                    data: {
                        'tripId': _tripId,
                        'scheduleId': "",
                    },
                    async: false,
                    success: function (data) {
                        if (data.listGuestPerPoint) tripSelected.tripData.seatMap = data.seatMap;
                        else
                            tripSelected.tripData.seatMap = data;
                    },
                    error: function () {
                        Message("Lỗi tải danh sách vé!", "Vui lòng tải lại trang", "");
                    },
                    complete: function () {
                        Loaing(false);
                    }
                });
            }
            base_price = tripSelected.tripData.ticketPrice;
            generateHtmlTripInfo();
        });
    }

    function updateListPoint(routeId, ready) {
        var route = $.grep(listRoute, function (item) {
            return item.routeId == routeId;
        });
        html_listInPoint = '';
        html_listOffPoint = '';
        $(route[0].listPoint).each(function (key, item) {
            if (key == 0 || key == route[0].listPoint.length - 1) {
                if (key == 0) {
                    html_listInPoint += '<option value="' + item.pointId + '">' + item.pointName + '</option>';
                } else {
                    html_listOffPoint += '<option value="' + item.pointId + '">' + item.pointName + '</option>';
                }
            } else {
                html_listInPoint += '<option value="' + item.pointId + '">' + item.pointName + '</option>';
                html_listOffPoint += '<option value="' + item.pointId + '">' + item.pointName + '</option>';
            }
        });
        $('.startPoint').html(html_listInPoint);
        $('.endPoint').html(html_listOffPoint);

        // mặc định chọn 110A trần nhật duật
        if ($('#chontuyen').val() === 'R04B1WJx8Q1Aw8') {
            $('#cbb_InPoint').val('P03h1HohkW59xU');//bãi cháy
            $('#cbb_OffPoint').val($('#chontuyen option:selected').data('endpoint')).change();
        } else if ($('#chontuyen').val() == 'R02fYyxpuGyyI' || $('#chontuyen').val() == 'R02AaVE2vdxy5') {
            $('#cbb_InPoint').val('P00zEi0GvGQdl');
            $('#cbb_OffPoint').val('P02fYymhRtME3').change();
        } else if ($('#chontuyen').val() == 'R02AaSWQAWl25' || $('#chontuyen').val() == 'R02AaV2rLPMly') {
            $('#cbb_InPoint').val('P02fYymhRtME3');
            $('#cbb_OffPoint').val('P00zEi0GvGQdl').change();
        } else {
            $('#cbb_InPoint').val($('#chontuyen option:selected').data('startpoint'));
            $('#cbb_OffPoint').val($('#chontuyen option:selected').data('endpoint')).change();
        }

        //alert($('#cbb_InPoint').val());
        /*
        * lấy dữ liệu startPointId và endPointId từ sessionStoge
        * */
        if (ready) {
            var date = '{{request('date')}}';
            if (date == '') {
                date = sessionStorage.getItem("date");
            }
            if (date !== null) {
                $(datepickerBox.datepicker("setDate", date));
            }
            updateSelectBoxTrip(false);
        }
        Loaing(false);


    }

    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

    function updateInputPrice() {
        var originalPrice = 0, // tổng giá ghế
            totalPrice = 0, // tổng tiền
            totalExtraPrice = 0, //tổng tiền phụ thu của ghế
            totalSeatPrice = 0,  //tổng tiền ghế chưa phụ thu
            priceSeat = 0,
            PriceExtra = 0,
            totalSeat = $('.ghedangchon').length;
        var priceInsurrance = tripSelected.tripData.priceInsurrance > 0 && $('#cb_priceInsurrance').is(':checked') ? totalSeat * tripSelected.tripData.priceInsurrance : 0;
        var mealPrice = tripSelected.tripData.mealPrice > 0 && $('#cb_isMeal').is(':checked') ? totalSeat * tripSelected.tripData.mealPrice : 0;
        var salePrice = $('#salePriceVal').val();
        var priceAdults = $('#numberOfAdults').val() * tripSelected.tripData.ticketPrice;
        var priceChildren = $('#numberOfChildren').val() * (tripSelected.tripData.ticketPrice * tripSelected.childrenTicketRatio);
        var commissionType = parseFloat($('#txtCommissionType').val());
        var commissionValue = parseFloat($('#txtCommissionValue').val());
        var listPriceShow = [];

        $('.ghedangchon').each(function (index, element) {
            priceSeat = $(element).data('extraprice');
            priceSeat = $.isNumeric(priceSeat) ? parseFloat(priceSeat) : 0;
            totalExtraPrice += priceSeat;
            listPriceShow.push(moneyFormat(tripSelected.tripData.ticketPrice + priceSeat));
        });
        totalSeatPrice = priceAdults + priceChildren;
        originalPrice = totalSeatPrice + totalExtraPrice;
        totalPrice = (originalPrice + mealPrice + priceInsurrance - salePrice) > 0 ? parseFloat(originalPrice + mealPrice + priceInsurrance - salePrice) : 0;
        var totalPriceShow = (originalPrice + mealPrice + priceInsurrance) > 0 ? parseFloat(originalPrice + mealPrice + priceInsurrance) : 0;


        // cập nhật vào input
        $('#originalPriceValue').val(originalPrice);
        $('#cb_priceInsurrance').val(priceInsurrance);
        $('#cb_isMeal').val(mealPrice);
        $('#totalPriceShow').val(moneyFormat(totalPriceShow));
        $('#listTicketPrice').val(listPriceShow.toString());
        if ($('form#datve').attr('action') == urlSellTicket && (parseInt($('#realPrice').val()) == totalPrice || parseInt($('#realPrice').val()) == originalPrice)) {
            if (commissionType == 1) {
                $('#realPrice').val(totalPrice - totalSeat * commissionValue);
                $('#realPriceShow').val(formatNumber(totalPrice - totalSeat * commissionValue));
            } else {
                $('#realPrice').val(totalPrice - totalSeat * commissionValue * totalPrice);
                $('#realPriceShow').val(formatNumber(totalPrice - totalSeat * commissionValue * totalPrice));
            }
        }
        $('#txtMoneyOwing').val(formatNumber($('#realPrice').val() - stringToInt($('#txtPaidMoney').val())));
        return false;
    }

    function minus(that) {
        if (!$(that).hasClass('round')) {
            var data = parseInt($("#numberOfChildren").val()),
                numberOfAdults = parseInt($('#numberOfAdults').val());
            if (data != 0) {
                data--;
                if (tripSelected.childrenTicketRatio != 0) {
                    numberOfAdults++;
                }
                $("#numberOfChildren").val(data);
                $('#lb_numberOfAdults').text(numberOfAdults);
                $('#numberOfAdults').val(numberOfAdults);
                updatePrice();
            } else {
                $("#numberOfChildren").val(0);
            }
        } else {
            var adults = parseInt($('#numberOfAdultsRound').val() ? $('#numberOfAdultsRound').val() : 0);
            var childs = parseInt($('#numberOfChildrenRound').val() ? $('#numberOfChildrenRound').val() : 0);
            if (childs > 0) {
                $('#numberOfAdultsRound').val(adults + 1);
                $('#numberOfChildrenRound').attr('value', childs - 1);
                $('#lb_numberOfAdultsRound').val((adults + 1) + "(" + (Seat_Back_Array.filter(function (e) {
                    return e
                })).toString() + ")");
                updateInputPriceRound();
            }
        }
    }

    function plus(that) {
        if (!$(that).hasClass('round')) {
            var data = parseInt($("#numberOfChildren").val()),
                numberOfAdults = parseInt($('#numberOfAdults').val());

            if (data < $('.ghedangchon').length) {
                data++;
                if (tripSelected.childrenTicketRatio != 0) {
                    numberOfAdults--;
                }

                $('#lb_numberOfAdults').text(numberOfAdults);
                $("#numberOfChildren").val(data);
                $('#numberOfAdults').val(numberOfAdults);
                updatePrice();
            } else {
                Message('Cảnh báo', 'Vé trẻ em không thể vượt quá vé người lớn', '');
            }
        } else {
            var adults = parseInt($('#numberOfAdultsRound').val() ? $('#numberOfAdultsRound').val() : 0);
            var childs = parseInt($('#numberOfChildrenRound').val() ? $('#numberOfChildrenRound').val() : 0);
            if ((adults - childs) > 1) {
                $('#numberOfAdultsRound').val(adults - 1);
                $('#numberOfChildrenRound').attr('value', childs + 1);
                $('#lb_numberOfAdultsRound').val((adults - 1) + "(" + (Seat_Back_Array.filter(function (e) {
                    return e
                })).toString() + ")");
                updateInputPriceRound();
            } else
                Message('Cảnh báo', 'Vé trẻ em không thể vượt quá vé người lớn', '');
        }
    }

    function updateInputPriceRound() {
        var originalPriceRound = 0, // tổng giá ghế
            totalPriceRound = 0, // tổng tiền
            totalExtraPriceRound = 0, //tổng tiền phụ thu của ghế
            totalSeatPriceRound = 0,  //tổng tiền ghế chưa phụ thu
            priceSeatRound = 0,
            PriceExtraRound = 0,
            totalSeatRound = (Seat_Back_Array.filter(function (e) {
                return e
            })).length;


        var priceInsurranceRound = TripData_Round.priceInsurrance > 0 && $('#cb_priceInsurranceRound').is(':checked') ? totalSeatRound * TripData_Round.priceInsurrance : 0;
        var mealPriceRound = TripData_Round.mealPrice > 0 && $('#cb_isMealRound').is(':checked') ? totalSeatRound * TripData_Round.mealPrice : 0;
        var salePriceRound = parseFloat($('#salePriceValRound').attr('value')) > 0 ? parseFloat($('#salePriceValRound').attr('value')) : 0;
        var priceAdultsRound = $('#numberOfChildrenRound').val() * TripData_Round.ticketPrice;
        var priceChildrenRound = $('#numberOfChildrenRound').val() * (TripData_Round.ticketPrice * TripData_Round.childrenTicketRatio);
        var commissionTypeRound = parseFloat($('#txtCommissionType').val());
        var commissionValueRound = parseFloat($('#txtCommissionValue').val());
        if (typeof sessionStorage.getItem('extraCancel') == 'undefined') sessionStorage.setItem('extraCancel', 0);
        listPrice[0] = 0;
        originalPriceRound = listPrice.reduce(function getSum(total, num) {
            return total + parseInt(num);
        }) - priceAdultsRound + priceChildrenRound;
        totalPriceRound = (originalPriceRound + mealPriceRound + priceInsurranceRound - salePriceRound * totalSeatRound) > 0 ? parseFloat(originalPriceRound + mealPriceRound + priceInsurranceRound - salePriceRound * totalSeatRound) : 0;

        $('#originalTicketPriceRound').val(originalPriceRound);
        $('#cb_priceInsurranceRound').val(priceInsurranceRound);
        $('#priceInsurranceRound').val(priceInsurranceRound);
        $('#cb_isMealRound').val(mealPriceRound);
        $('#totalPriceShowRound').val(moneyFormat(originalPriceRound + mealPriceRound + priceInsurranceRound));
        $('#total_Price_Back').val(originalPriceRound);
        $('#listTicketPriceRound').val(listPrice.filter(function (e) {
            return e
        }).toString());
        $('#salePriceRound').val(salePriceRound * (Seat_Back_Array.reduce(function getSum(total, num) {
            return total + num;
        })));
        if ($('form#datve').attr('action') == urlSellTicket) {
            if (!isNaN(commissionTypeRound)) {
                if (commissionTypeRound == 1) {
                    $('#txtrealPriceRound').val(moneyFormat(totalPriceRound - totalSeatRound * commissionValueRound));
                    $('#realPriceRound').val(totalPriceRound - totalSeatRound * commissionValueRound);
                } else {
                    $('#txtrealPriceRound').val(moneyFormat(totalPriceRound - totalSeatRound * commissionValueRound * totalPriceRound));
                    $('#realPriceRound').val(totalPriceRound - totalSeatRound * commissionValueRound * totalPriceRound);
                }
            } else {
                $('#txtrealPriceRound').val(moneyFormat(totalPriceRound));
                $('#realPriceRound').val(totalPriceRound);
            }
        }
        $('#txtMoneyOwingRound').val(moneyFormat($('#realPriceRound').val() - $('#txtPaidMoneyRound').val()));

        sessionStorage.setItem('extraCancel', 0);
        return false;
    }

    function updatePrice() {
        if (total_Price > 0) {
            var totalPrice = getTotalPrice();
            total_Price = totalPriceBack + totalPrice;
            $('#totalPrice').val(moneyFormat(total_Price));
            // $('#txtPaidMoney').attr('max', total_Price);
            $('#txt_totalPrice').val(total_Price);
            $('#txtMoneyOwing').val($('#realPrice').val() - stringToInt($('#txtPaidMoney').val()));
        }
        else {
            var totalPrice = getTotalPrice();
            $('#totalPrice').val(moneyFormat(totalPrice));
            // $('#txtPaidMoney').attr('max', totalPrice);
            $('#txt_totalPrice').val(totalPrice);
            $('#txtMoneyOwing').val(parseInt($('#realPrice').val()) - stringToInt($('#txtPaidMoney').val()));
        }
    }

    /*
    * hàm tính tổng giá tiền
    * */
    function getTotalPrice() {

        var totalTicket = $('.ghedangchon').length;
        var priceInsurrance = $('#cb_priceInsurrance').val() > 0 && $('#cb_priceInsurrance').is(':checked') ? totalTicket * $('#cb_priceInsurrance').val() : 0;
        var mealPrice = $('#cb_isMeal').val() > 0 && $('#cb_isMeal').is(':checked') ? totalTicket * $('#cb_isMeal').val() : 0;
        var priceAdults = $('#numberOfAdults').val() * tripSelected.tripData.ticketPrice;
        var priceChildren = $('#numberOfChildren').val() * (tripSelected.tripData.ticketPrice * tripSelected.childrenTicketRatio);
        var salePrice = $($('.salePrice')[0]).val();
        var totalextra = 0;
        $('.ghedangchon').each(function (index, ele) {
            var price = $(ele).attr('data-extraPrice');
            price = $.isNumeric(price) ? parseFloat(price) : 0;
            totalextra += price;
        });

        if (salePrice < 1) {
            salePrice *= (priceAdults + priceChildren + totalextra);
        }

        totalPrice = ((priceAdults + priceChildren) - salePrice) + mealPrice + priceInsurrance + totalextra;

        totalPrice = totalPrice > 0 ? totalPrice : 0;
        return totalPrice;
    }

    function selectSeat(elem) {
        $(elem).toggleClass('ghedangchon');
        var seatId = $(elem).children('.ticketInfo-tool').children('div').children('.seatId').text();
        String.prototype.replaceAll = function (str1, str2, ignore) {
            return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, "\\$&"), (ignore ? "gi" : "g")), (typeof(str2) == "string") ? str2.replace(/\$/g, "$$$$") : str2);
        };
        seatId = seatId.replaceAll(" ", "-");
        listSeat = '';
        var arr = [];//mảng chỗ ngồi
        $('.ghedangchon').each(function (k, v) {
            arr.push($(v).find(".seatId").text());
        });
        listSeat = arr.toString();
        var totalTicket = $('.ghedangchon').length;

        $('#listSeat').text(listSeat);
        $('#listSeatId').val(listSeat);

        $('#lb_numberOfAdults').val(totalTicket + " (" + $('#listSeat').text() + ")");
        $('#numberOfAdults').val(totalTicket);
        $('#numberOfChildren').val(0);
    }

    function updateSelectBoxTripRound(autoOpen, callbackIfSuccess) {

        var routeId = $(selectBoxRoute).val();
        var date = (sessionStorage.getItem('timeTrip') != undefined && sessionStorage.getItem('timeTrip') != '') ? sessionStorage.getItem('timeTrip') : $(datepickerBox.datepicker()).val();
        var startPointId = $('#cbb_InPoint').val();
        var endPointId = $('#cbb_OffPoint').val();
        $('.routeId').val(routeId);
        $(selectBoxListTrip).html('<option value="">Đang tải dữ liệu...</option>');
        Loaing(true);

        $.ajax({
            dataType: "json",
            url: urlSeachTrip,
            data: {
                'routeId': routeId,
                'date': date.replace('/', '-').replace('/', '-'),
                'startPointId': startPointId,
                'endPointId': endPointId,
                'new': true,
            },
            success: function (data) {
                console.log(data);
                html = '';
                listTrip = data;

                data.forEach(function (trip) {
                    var totalEmptySeat = trip.totalEmptySeat;
                    var isSelected = sessionStorage.getItem('tripIdRound') === trip.tripId ? 'selected' : '';
                    html += '<option ' + isSelected + ' data-time="' + getFormattedDate(trip.getInTime, 'time') + '" data-schedule="' + trip.scheduleId + '" value="' + trip.tripId + '">' +
                        trip.numberPlate + ' - ' +
                        getFormattedDate(trip.getInTime, 'time') +
                        '         (Trống: ' + totalEmptySeat + ')' +
                        '</option>';
                });

                if (html !== '') {
                    html = '<option value="tccc">Toàn bộ các chuyến</option>' + html;
                    console.log(html);
                    $(selectBoxListTrip).html(html);
                    $('#cbbTripDestinationId').html(html);
                    Loaing(false);
                }
                else {
                    $(selectBoxListTrip).html(' <option value="">Hiện tại không có chuyến</option>');
                    $('#cbbTripDestinationId').html('<option value="">Hiện tại không có chuyến</option>');
                    Loaing(false);
                }
                if (callbackIfSuccess !== undefined) callbackIfSuccess();
                saveFormDataLocal();
                // Loaing(false);
            },
            error: function () {
                Message("Lỗi!", "Vui lòng tải lại trang", "");

            }
        })
    }

    function updateSelectBoxTrip(autoOpen, callbackIfSuccess) {

        var routeId = $(selectBoxRoute).val();
        var date = (sessionStorage.getItem('timeTrip') != undefined && sessionStorage.getItem('timeTrip') != '') ? sessionStorage.getItem('timeTrip') : $(datepickerBox.datepicker()).val();
        // console.log(date,sessionStorage.getItem('timeTrip'));

        var startPointId = $('#cbb_InPoint').val();
        var endPointId = $('#cbb_OffPoint').val();
        $('.routeId').val(routeId);
        $(selectBoxListTrip).html('<option value="">Đang tải dữ liệu...</option>');
        Loaing(true);

        $.ajax({
            dataType: "json",
            url: urlSeachTrip,
            data: {
                'routeId': routeId,
                'date': date.replace('/', '-').replace('/', '-'),
                'startPointId': startPointId,
                'endPointId': endPointId,
                'new': true,
            },
            success: function (data) {
                console.log(data);
                html = '';
                listTrip = data;

                data.forEach(function (trip) {
                    var totalEmptySeat = trip.totalEmptySeat;

                    var isSelected = tripSelected.tripId === trip.tripId ? 'selected' : '';
                    html += '<option ' + isSelected + ' data-time="' + getFormattedDate(trip.getInTime, 'time') + '" data-schedule="' + trip.scheduleId + '" value="' + trip.tripId + '">' +
                        trip.numberPlate + ' - ' +
                        getFormattedDate(trip.getInTime, 'time') +
                        '         (Trống: ' + totalEmptySeat + ')' +
                        '</option>';
                });

                if (html !== '') {
                    html = '<option value="tccc">Toàn bộ các chuyến</option>' + html;
                    $(selectBoxListTrip).html(html);
                    $('#cbbTripDestinationId').html(html);
                    var tripId = '{{request('tripId')}}';
                    var scheduleId = '{{request('scheduleId')}}';
                    if (tripId == '') {
                        tripId = sessionStorage.getItem("tripId");
                    }

                    if (scheduleId == '') {
                        scheduleId = sessionStorage.getItem("scheduleId");
                    }


                    if (tripId == null) {
                        tripId = listTrip[0].tripId;
                    } else {
                        if (tripId != -1) {
                            selectBoxListTrip.val(tripId).change();
                        } else {
                            selectBoxListTrip.find("[data-schedule='" + scheduleId + "']").attr("selected", "selected").change();
                        }
                    }
                    if ($(selectBoxListTrip).val() === 'tccc') {
                        generateHtmlAllTrip();
                    } else {
                        updateTripInfo(selectBoxListTrip.val(), selectBoxListTrip.find('option:selected').data('schedule'));
                        updatePrice();
//                            if (autoOpen === true) {
//                                $(selectBoxListTrip).select2("open");
//                            }
                    }

                }
                else {
                    $(selectBoxListTrip).html(' <option value="">Hiện tại không có chuyến</option>');
                    $('#cbbTripDestinationId').html('<option value="">Hiện tại không có chuyến</option>');
                    Loaing(false);
                }
                if (callbackIfSuccess !== undefined) callbackIfSuccess();
                saveFormDataLocal();
                // Loaing(false);
            },
            error: function () {
                Message("Lỗi!", "Vui lòng tải lại trang", "");
                Loaing(false);
            }
        })
    }

    //cập nhật danh sách vé hủy
    function generateListTicketCancel(tripId) {
        $('.dsVeHuy').html('<div class="text-center">Đang tải dữ liệu ...</div>');
        if (tripSelected.tripData.tripId != undefined)
            $.ajax({
                dataType: "json",
                url: '{{ action('TicketController@getListTicketCancelWithTripId') }}',
                data: {'tripId': tripSelected.tripData.tripId, 'ticketStatus': [0].toString()},
                success: function (data) {
                    var addressdrop = '', addresspick = '';
                    html = '<div class="table-responsive">';
                    html += '<table class="table table-hover">';
                    html += '<thead><tr><th width="5%">STT</th><th width="5%">Mã vé</th><th width="10%">Số ghế</th><th width="10%">KH</th><th width="10%">SĐT</th><th width="15%">Đón</th><th width="15%">Trả</th><th width="10%">Đại lý</th><th width="5%">Người hủy</th><th width="10%">Lí do</th><th width="5%">Phục hồi</th></tr></thead>';
                    if (typeof data.ticket[0] != 'undefined') {
                        $.each(data.ticket, function (key, val) {
                            var ticketCancel = val.listSeatCanceled.concat(val.listSeatId);
                            if (typeof val.getInPoint != 'undefined') {
                                if (typeof val['dropAlongTheWayAddress'] != 'undefined') {
                                    addressdrop = val['dropAlongTheWayAddress'];
                                } else {
                                    if (typeof val[' dropOffAddress'] != 'undefined')
                                        addressdrop = val['dropOffAddress'];
                                    else {
                                        addressdrop = val['getOffPoint']['pointName'];
                                    }
                                }
                                if (addressdrop.split(', ').length >= 2) {
                                    addressdrop = addressdrop.split(', ', 2).toString();
                                }
                                if (typeof val['alongTheWayAddress'] != 'undefined') {
                                    addresspick = val['alongTheWayAddress'];
                                } else {

                                    if (typeof val['pickUpAddress'] != 'undefined') {
                                        addresspick = val['pickUpAddress'];
                                    }
                                    else {
                                        addresspick = val['getInPoint']['pointName'];
                                    }
                                }
                                if (addresspick.split(', ').length >= 2) {
                                    addresspick = addresspick.split(', ', 2).toString();
                                }
                            }
                            html += "<tr><td width='5%'>" + (key + 1) + "</td>";
                            html += "<td width='5%'>" + val.ticketCode + "</td>";
                            html += "<td width='10%'>" + ticketCancel.length + "(" + ticketCancel.toString() + ")</td>";
                            html += "<td width='10%'>" + val.fullName + "</td>";
                            html += "<td width='10%'>" + val.phoneNumber + "</td>";
                            // htmlListCustomer += "<td>" + v.getInPoint.pointName + "</td>";
                            // htmlListCustomer += "<td>" + v.getOffPoint.pointName + "</td>";
                            html += "<td width='15%'>" + addresspick + "</td>";
                            html += "<td width='15%'>" + addressdrop + "</td>";
                            if (val.sourceName != undefined) {
                                html += "<td width='10%'>" + val.sourceName + "</td>";
                            } else {
                                html += "<td width='10%'> - </td>";
                            }
                            html += '<td width="10%">' + val.cancelName + '</td>';
                            html += '<td width="10%">' + (typeof val.cancelReason != 'undefined' ? val.cancelReason : "") + '</td>';
                            if (val.listSeatCanceled.length > 0) {
                                html += '<td width="5%"><a href="" class="btn btn-info cloneTicket" data-ticketid="' + val.ticketId + '" data-tripid="' + val.tripId + '" data-listSeat="' + val.listSeatCanceled.toString() + '" title="phục hồi vé"><i class="fas fa-cut"></i></a></td></tr>';
                            } else {
                                html += '<td width="5%"> </td></tr>';
                            }
                        });
                    }

                    html += '</table>';
                    html += '</div>';
                    if (data.ticket.length <= 0) html = '<div class="text-center">không có dữ liệu!</div>';
                    $('.dsVeHuy').html(html);
                },
                error: function () {
                    $('.dsVeHuy').html("không gửi được yêu cầu lấy danh sách! vui lòng tải lại trang");
                }
            });
    }

    function generateListTicketExpired(tripId) {
        $('.dsVeHetHan').html('<div class="text-center">Đang tải dữ liệu ...</div>');
        if (tripSelected.tripData.tripId != undefined)
            $.ajax({
                dataType: "json",
                url: '{{ action('TicketController@getListTicketCancelWithTripId') }}',
                data: {'tripId': tripSelected.tripData.tripId, 'ticketStatus': [2, 6].toString()},
                success: function (data) {
                    var addressdrop = '', addresspick = '';
                    html = '<div class="table-responsive">';
                    html += '<table class="table table-hover">';
                    html += '<thead><tr><th width="5%">STT</th><th width="10%">Mã vé</th><th width="10%">Số ghế</th><th width="10%">KH</th><th width="10%">SĐT</th><th width="20%">Đón</th><th width="20%">Trả</th><th width="10%">Đại lý</th><th width="5%">Phục hồi</th></tr></thead>';
                    if (typeof data.ticket[0] != 'undefined') {
                        $.each(data.ticket, function (key, val) {
                            if (typeof val.getInPoint != 'undefined') {
                                if (typeof val['dropAlongTheWayAddress'] != 'undefined') {
                                    addressdrop = val['dropAlongTheWayAddress'];
                                } else {
                                    if (typeof val[' dropOffAddress'] != 'undefined')
                                        addressdrop = val['dropOffAddress'];
                                    else {
                                        addressdrop = val['getOffPoint']['address'];
                                    }
                                }
                                if (addressdrop.split(', ').length >= 2) {
                                    addressdrop = addressdrop.split(', ', 2).toString();
                                }
                                if (typeof val['alongTheWayAddress'] != 'undefined') {
                                    addresspick = val['alongTheWayAddress'];
                                } else {

                                    if (typeof val['pickUpAddress'] != 'undefined') {
                                        addresspick = val['pickUpAddress'];
                                    }
                                    else {
                                        addresspick = val['getInPoint']['address'];
                                    }
                                }
                                if (addresspick.split(', ').length >= 2) {
                                    addresspick = addresspick.split(', ', 2).toString();
                                }
                            }
//                            if(val.ticketStatus ==2 &&val.overTime > (new Date).getTime()){ return;}
                            html += "<tr><td width='5%'>" + (key + 1) + "</td>";
                            html += "<td width='5%'>" + val.ticketCode + "</td>";
                            html += "<td width='10%'>" + val.listSeatId.length + "(" + val.listSeatId.toString() + ")</td>";
                            html += "<td width='10%'>" + val.fullName + "</td>";
                            html += "<td width='10%'>" + val.phoneNumber + "</td>";
                            // htmlListCustomer += "<td>" + v.getInPoint.pointName + "</td>";
                            // htmlListCustomer += "<td>" + v.getOffPoint.pointName + "</td>";
                            html += "<td width='15%'>" + addresspick + "</td>";
                            html += "<td width='15%'>" + addressdrop + "</td>";
                            html += "<td width='10%'>" + (val.sourceName || '-') + "</td>";
                            if (val.listSeatId.length > 0) {
                                html += '<td width="5%"><a href="" class="btn btn-info cloneTicket" data-ticketid="' + val.ticketId + '" data-tripid="' + val.tripId + '" data-listSeat="' + val.listSeatId.toString() + '" title="phục hồi vé"><i class="fas fa-cut"></i></a></td></tr>';
                            } else {
                                html += '<td width="5%"> - </td></tr>';
                            }
                        });
                    }

                    html += '</table>';
                    html += '</div>';
                    if (data.ticket.length <= 0) html = '<div class="text-center">không có dữ liệu!</div>';
                    $('.dsVeHetHan').html(html);
                },
                error: function () {
                    $('.dsVeHetHan').html('Lỗi! gửi yêu cầu thất bại, không lấy được danh sách');
                }
            });
    }


    // caajp nhaajt thông tin của chuyến vào tripdata
    function updateTripInfo(_tripId, scheduleId) {
        if (_tripId !== 'tccc' && _tripId !== '') {
            tripSelected.tripId = _tripId;
            if (tripSelected.tripId == -1) {
                tripSelected.tripData = $.grep(listTrip, function (trip) {
                    return trip.scheduleId == scheduleId;
                })[0];
                $.ajax({
                    dataType: "json",
                    url: urlSeatMapInTrip,
                    data: {
                        'tripId': _tripId,
                        'scheduleId': scheduleId,
                    },
                    async: false,
                    success: function (data) {
                        tripSelected.tripData.seatMap = data;
                        tripSelected.seatList = data.seatList;
                        tripSelected.numberOfRows = data.numberOfRows;
                        tripSelected.numberOfColumns = data.numberOfColumns;
                        tripSelected.numberOfFloors = data.numberOfFloors;
                        generateHtmlTripInfo();
                    },
                    error: function () {
                        Message("Lỗi tải danh sách vé hủy!", "Vui lòng tải lại trang", "");
                        Loaing(false);
                    }
                });
            } else {
                tripSelected.tripData = $.grep(listTrip, function (trip) {
                    return trip.tripId == tripSelected.tripId;
                })[0];
                changeRealtimeTrip(tripSelected.tripData.tripId);
            }
            Loaing(false);
        }
    }

    function generateHtmlAllTrip() {
        var number = {stt: 0, totalEmptySeat: 0}
        html = '';
        html += '<style>' +
            '.header-trip{padding:5px 10px;font-weight:bold} .header-trip .time{font-size:20px;line-height: 35px;color:#0b5fc6} .header-trip .number-plate{border:2px solid #333;float:right;padding:0px 5px;font-size:17px;line-height: 35px;border-radius:5px;}' +
            '.infomation-trip{background: #eee;padding:5px 10px;}' +
            '.progress{width:100%;background: #aaa;text-align:center;height:30px;position: relative}' +
            '.probar{text-align:center;line-height:30px;color:#fff;font-weight: bold;}' +
            '.textbar{line-height:30px;}' +
            '</style>';
        html += '<table style="border-spacing:2px;" width="100%">';
        html += '<tr><td width="20%"></td><td width="20%"></td><td width="20%"></td><td width="20%"></td><td width="20%"></td></tr><tr>';
        $.each(listTrip, function (i, e) {
            html += '<td style="cursor:pointer" title="Chuyển đến chuyến này" data-go-to-trip="' + e.tripId + '" data-schedule-go-to="' + e.scheduleId + '">';
            html += '<div style="background: #fff;border-radius:5px;margin:2px;">';
            html += '<div class="header-trip">';
            html += '<span class="time">' + getFormattedDate(e.getInTime, 'time') + '</span><span class="number-plate">' + e.numberPlate + '</span><div class="clearfix"></div>';
            html += '</div>';
            html += '<div class="infomation-trip">';
            html += '<div>Tài xế : <span style="font-weight:bold;">' + (e.driverName || "Chưa chọn") + '</span></div>';
            html += '<div>Phụ xe : <span style="font-weight:bold;">' + (e.assName || "Chưa chọn") + '</span></div>';
            html += '</div>';
            html += '<div class="footer-trip"><div class="progress"><div class="probar text-center" style="width:' + Math.floor((e.totalSeat - e.totalEmptySeat) * 100 / e.totalSeat) + '%;background:' + (e.totalEmptySeat == 0 ? "red" : "#4CAF50") + ';">&nbsp;</div><div class="textbar text-center" style="position: absolute;height: 30px;width:100%;top:0px;color:#fff;font-weight: bold">Trống : ' + (e.totalEmptySeat || "0") + ' / ' + (e.totalSeat || "0") + '</div></div>';
            html += '</div>';
            html += '</div>';
            html += '</td>';
            number.stt++;
            number.totalEmptySeat += e.totalEmptySeat;
            if (number.stt % 5 == 0) {
                html += '</tr><tr>';
            }
        });
        html += '</tr>';
        html += '</table>';
        $('#tang1 .khungxe').html(html);

        $('.tripInfo').hide();
        $('.listTicketCancelAndHH').hide();
        $('#table_SaoNghe .navtabtool ').hide();
        $('#table_SaoNghe .ghichu_datghe').hide();
        $('#listTicketCancel').hide();
        Loaing(false);
        return false;
    }

    //build html của chuyến
    function generateHtmlTripInfo() {
        // Loaing(true);
        if (tripSelected.tripData.getInTime < Date.now()) {
            $('#giucho').hide();
        }
        else {
            $('#giucho').show();
        }
        if (tripSelected.tripData.seatMap.numberOfFloors == 1) $('.floor').hide();
        else $('.floor').show();

        if (tripSelected.tripData.tripStatus == 1) {
            $('#btnStartTrip').show();
            $('#btnOpenTrip').hide();
        }
        if (tripSelected.tripData.tripStatus == 2) {
            $('#btnStartTrip').hide();
            if ({{ session('userLogin')['userInfo']['userType']==7?'true':'false' }}) {
                $('#btnOpenTrip').show();
            }
        }
        //cap nhat commition
        if (typeof tripSelected.tripData.commission != 'undefined') {
            $('#txtOneSeatPrice').val(tripSelected.tripData.ticketPrice);
            $('#txtCommissionType').val(tripSelected.tripData.commission.commissionType);
            $('#txtCommissionValue').val(tripSelected.tripData.commission.commissionValue);
        }

        if (tripSelected.tripData.seatMap.seatList !== undefined) {
            var countSeat = {total: 0, giucho: 0, pay: 0};
            $.map(tripSelected.tripData.seatMap.seatList, function (seat) {
                if (seat.seatType === 3 || seat.seatType === 4) {
                    countSeat.total++;
                    if (typeof seat.ticketInfo != 'undefined') {
                        var ticketinfomation = seat.ticketInfo[seat.listTicketId[0]];
                        stt = checkStatusSeatAndTicket(ticketinfomation.ticketStatus, ticketinfomation.overTime);
                        if (stt.seatStatus == 'ghegiucho') {
                            countSeat.giucho++;
                        }
                        if (stt.seatStatus == 'ghedadat' || stt.seatStatus == 'ghelenxe') {
                            countSeat.pay++;
                        }
                    }
                }
            });
            $('#table_SaoNghe .navtabtool').show();
            $('#table_SaoNghe .ghichu_datghe').show();
            $('#listTicketCancel').show();
            $('.tripInfo').show();
            $('.tripInfo').html(
                '<table class="table" width="100%"><tr><td width="50%">Biển số : <span  style="font-weight:bold;">' + tripSelected.tripData.numberPlate +
                '</span>, Tài xế : <span  style="font-weight:bold;">' + (tripSelected.tripData.driverName || 'Chưa chọn') + ' (+84 ' + ((tripSelected.tripData.phoneNumber) || tripSelected.tripData.driverPhoneNumber || ' ') + ')' +
                '</span>, Phụ Xe : <span  style="font-weight:bold;">' + (tripSelected.tripData.assName || 'Chưa chọn') + ' ( +84 ' + ((tripSelected.tripData.assPhoneNumber) || ' ') + ')' +
                '</span></td><td width="50%">Tổng số vé đã đặt : <span  style="font-weight:bold;">' + (countSeat.giucho + countSeat.pay) +
                '</span>, Số ghế trống : <span  style="font-weight:bold;">' + (countSeat.total - countSeat.pay - countSeat.giucho) +
                '</span>, Thanh toán : <span  style="font-weight:bold;">' + countSeat.pay +
                '</span>, Đặt chỗ : <span  style="font-weight:bold;">' + countSeat.giucho + '</span></td></tr></table>'
            );
            $('option[value='+tripSelected.tripData.tripId+']').text(tripSelected.tripData.numberPlate + ' - ' +
                getFormattedDate(tripSelected.tripData.getInTime, 'time') +
                '         (Trống: ' +  (countSeat.total - countSeat.pay - countSeat.giucho) + ')');
            $('option[value='+tripSelected.tripData.tripId+']').hide();
            $('option[value='+tripSelected.tripData.tripId+']').show();
            reset();

            if (tripSelected.tripData.seatMap) {
                loadSeatMap(tripSelected.tripData.seatMap);
                makeSeatMapHtml(tripSelected);
            }

            tripSelected.ticketPrice = tripSelected.tripData.ticketPrice;
            tripSelected.childrenTicketRatio = tripSelected.tripData.childrenTicketRatio;
            if (tripSelected.tripData.tripStatus == 2) {
                $('.tripInfo').append('<img src="{{ url('public/images/daxuatben.png') }}" alt="" class="daxuatben">');
            }
            $('.getInTimePlan').val(tripSelected.tripData.startTime);
            $('.getOffTimePlan').val(tripSelected.tripData.getOffTime);
            $('.tripId').val(tripSelected.tripData.tripId);
            $('.scheduleId').val(tripSelected.tripData.scheduleId);
            $('.startDate').val(tripSelected.tripData.startTime);
            $('#cb_priceInsurrance').val(tripSelected.tripData.priceInsurrance);
            $('#cb_isMeal').val(tripSelected.tripData.mealPrice);

            $('#txtGetInPointId').val(tripSelected.tripData.getInPointId);
            $('#txtGetOffPointId').val(tripSelected.tripData.getOffPointId);
            $('#txtGetInPointName').val(tripSelected.tripData.getInPointName);
            $('#txtGetOffPointName').val(tripSelected.tripData.getOffPointName);
            // var tp ='';//ticket price
            // $('.ghedangchon').each(function(k,v){
            //     var sid=$(v).find('.seatId').text()
            //     tp = parseInt(TripData.ticketPrice + TripData.seatMap.seatList[$(v).index()])
            // });
            $('#ticketPrice').attr("data-ticketPrice", tripSelected.tripData.ticketPrice);
            $('#mealPrice').val($.number(tripSelected.tripData.mealPrice));
            if (tripSelected.tripData.mealPrice == -1) {
                $('#isMeal').hide();
            } else {
                $('#isMeal').show();
            }

            $('#priceInsurrance').val($.number(tripSelected.tripData.priceInsurrance));

            if (tripSelected.tripData.priceInsurrance <= 0) {
                $('#isInsurrance').hide();
            } else {
                $('#isInsurrance').show();
            }

            $('.routeName').html('<b>' + $('#chontuyen option:selected').text() + "</b> : <b>" + $('#txtCalendar').val() + "</b> : <b>" + $('#listTrip option:selected').data('time') + "</b>");

            $('.InTime').val(getFormattedDate(tripSelected.tripData.getInTime, 'date'));
            $('.OffTime').val(getFormattedDate(tripSelected.tripData.getOffTime, 'date'));

            if (flashMoveSeat) {
                $('.ghetrong').css('cursor', 'move');
            } else {
                $('.ghetrong').css('cursor', 'default');
            }
            initChangeVehicleForm();
        }

        //Tạo danh sách khách hàng
        generateCustomerHTML(tripSelected);
//            //cập nhật danh sách vé hủy;
        generateListTicketCancel(tripSelected.tripData.tripId);
        generateListTicketExpired(tripSelected.tripData.tripId);

        route_name_selected = $('#select2-chontuyen-container').attr('title');


        listRoute.forEach(function (route) {
            var check_space = 0;
            var start_point = '';
            var end_point = '';
            for (var i = 0; i < route['routeName'].length; i++) {
                if (route['routeName'].charAt(i) == '-') {
                    check_space = 1;
                    i++;
                }
                if (check_space == 0) end_point += route['routeName'].charAt(i);
                else start_point += route['routeName'].charAt(i);

            }

            var roundRoute = $.trim(start_point) + " " + '-' + " " + $.trim(end_point);

            if (route_name_selected == roundRoute)
                round_route = route;
        });

    }

    function getListTicket(seatMap) {
        var ticket = [];
        var flag = false;
        $.each(seatMap.seatList, function (index, e) {
            if (e.listTicketId != undefined) {
                if (e.listTicketId[0] != undefined) {
                    for (var i = 0; i < ticket.length; i++) {
                        if (e.listTicketId[0] == ticket[i].listTicketId[0]) {
                            flag = true;
                        }
                    }
                    if (!flag) {
                        ticket.push(e);
                    }
                }
            }
            flag = false
        });
        return ticket;
    }

    /*
    * hàm reset khi chọn lại
    *
    * */
    function reset() {
        tripSelected.tripId = '';
        listSeat = [];
        $('#listSeat').text(listSeat);
        $('input:text').not(datepickerBox).val('');
        $('#tang1 .khungxe,#tang2 .khungxe').html('');
        $('#listSeat,.InTime,.OffTime,.routeName').val('');
        $('#lb_numberOfAdults').val('0');
        $('#numberOfAdults,#numberOfChildren').val('0');
        $('#tongGiaVeGuiDo,#giaVeGuiDoGoc').text(moneyFormat(0));
        $('#totalPrice').val(moneyFormat(0));

    }

    function resetInput() {
        $('#sell_ticket input[type=checkbox]').prop('checked', false);
        $('input[type=checkbox]#send_code_to_phone').prop('checked', false);
        //$('#sell_ticket button.btnCheckPromotionCode').click();
        $('#sell_ticket input[type=text],#sell_ticket input[type=number],#sell_ticket input[type=email]')
            .not('input[readonly],#txtTimeBookHolder').val('');
        //$("#dropOffAddress,#pickUpAddress").prop('readonly', true);
    }

    /*
    * hàm tạo sơ đồ ghế
    * */
    function loadSeatMap(seatMap) {
        if ((seatMap.numberOfFloors > 1) && ('{{session('companyId')}}' == 'TC03013FPyeySDW' || '{{session('companyId')}}' == 'TC03m1MSnhqwtfE')) {
            $('.floor').hide();
            $('.SnTt').show();
            $('#tang1 .khungxe,#tang2 .khungxe,#ds_tang1 .khungxe,#ds_tang2 .khungxe,#SnTt').html('');
            var seatHtml = '';
            seatHtml += '<div id="table-map_SaoNghe"><div style="padding-left: 1.2%">';
            for (var column = 1; column <= (seatMap['numberOfColumns'] * 2); column++) {
                if (column == 2 || column == 5) {
                    seatHtml += '<div  style="width:24.3%;float:left;border: 1px solid black;text-align: center;background-color: #0D8BBD">' +
                        '<strong style="font-size: 14px">' + "Tầng 1" + '</strong></div>';
                } else if (column == 1 || column == 6) {
                    seatHtml += '<div style="width:24.3% ;float:left;border: 1px solid black;text-align: center;background-color: #0D8BBD">' +
                        '<strong style="font-size:14px">Tầng 2</strong>' + '</div>';
                }
            }
            seatHtml += '</div><div style="padding-left: 2%">';
            var seat, blank;
            var seatList = seatMap.seatList;
            var count_col = seatMap.numberOfColumns;
            var count_row = seatMap.numberOfRows;
            var startF2 = getSeatIndex(seatMap.seatList);
            widthOfSeat = 'calc(' + 100 / (count_col + 1) + '% - 12px)';
            $('#style-seat').html('.ghe{ width:' + widthOfSeat + '}');
            for (var row = 1; row <= count_row; row++) {
                for (column = 1; column <= (count_col - 1) * 2; column++) {
                    blank = '<div class="ghe loidi" style="width:' + widthOfSeat + '"></div>';
                    if (column == 1) {
                        $.each(seatMap['seatList'], function (key, item) {
                            if ((item['column'] == 1 || (item['column'] == 2 && item['row'] == count_row)) && item['floor'] == 2 && item['row'] == row) {
                                blank = generateSeatHTML(item);
                                return false;
                            }
                        });
                    } else
                    /// cột thứ 2
                    if (column == 2) {
                        $.each(seatMap['seatList'], function (key, item) {
                            if ((item['column'] == 1 || (item['column'] == 2 && item['row'] == count_row)) && item['floor'] == 1 && item['row'] == row) {
                                blank = generateSeatHTML(item);
                                return false;
                            }
                        });
                    } else
                    // cột thứ 3
                    if (column == 3) {
                        $.each(seatMap['seatList'], function (key, item) {
                            if ((item['column'] == 3) && item['floor'] == 1 && item['row'] == row) {
                                blank = generateSeatHTML(item);
                                return false;
                            }
                        });
                    } else

                    // cột cuối
                    if (column == 4) {
                        $.each(seatMap['seatList'], function (key, item) {
                            if ((item['column'] == 3) && item['floor'] == 2 && item['row'] == row) {
                                blank = generateSeatHTML(item);
                                return false;
                            }
                        });
                    }
                    seatHtml += blank;

                }
            }
            seatHtml += '</div>';
            $('#SnTt').html('');
            $('#SnTt').html(seatHtml);


        }
        else {
            $('#tang1 .khungxe,#tang2 .khungxe,#ds_tang1 .khungxe,#ds_tang2 .khungxe,#SnTt').html('');
            $('.SnTt').hide();
            var html_floor1 = '',
                html_floor2 = '',
                Row = seatMap['numberOfRows'],
                Column = seatMap['numberOfColumns'],
                cellNull = '<div class="ghe loidi-noboder"></div>',
                widthOfSeat = 'calc(' + 100 / Column + '% - 12px)';
            road = '<div class="ghe loidi" style="width:' + widthOfSeat + '"></div>';
            $('#style-seat').html('.ghe{ width:' + widthOfSeat + '}');
            for (hang = 1; hang <= Row; hang++) {
                for (cot = 1; cot <= Column; cot++) {
                    seat_floor1 = seat_floor2 = road;
                    $.each(seatMap['seatList'], function (key, item) {
                        if (item['column'] === cot && item['row'] === hang) {
                            if (item['floor'] === 1) {
                                /*Tầng 1*/
                                seat_floor1 = generateSeatHTML(item, widthOfSeat);
                            } else {
                                /*Tầng 2*/
                                seat_floor2 = generateSeatHTML(item, widthOfSeat);
                            }
                        }
                    });
                    html_floor1 += seat_floor1;
                    html_floor2 += seat_floor2;
                }
            }
            $('#tang1 .khungxe,#ds_tang1 .khungxe').html(html_floor1);

            if (seatMap['numberOfFloors'] > 1) {
                $('#tang2 .khungxe,#ds_tang2 .khungxe').html(html_floor2);
            }

        }
        /*
        * Tự động chọn vé theo ticketId trên url
        * */
        var ticketId = "{{request('ticketId')}}";

        if (ticketId != '') {
//                $('.ghe[data-ticketid="' + ticketId + '"]').addClass('ghedangchon');
            for (var i = 1; i <= $('.ghe[data-ticketid="' + ticketId + '"]').length; i++)
                $('.ghe[data-ticketid="' + ticketId + '"]').click();
        }
    }

    function getSeatIndex(seatList) {
        for (var i = 0; i < seatList.length; i++)
            if (seatList[i].floor == 2)
                return i;
    }

    function generateSeatHTML(seat, widthOfSeat) {
        /*
            DOOR(1),//cửa
            DRIVER_SEAT(2), //ghế cho tài xế
            NORMAL_SEAT(3),//ghế thường
            BED_SEAT(4),//ghế giường nằm
            WC(5),//nhà vê sinh
            AST_SEAT(6), //ghế cho phụ xe
        */
        var lastPointId = '';
        for (key in seat['ticketInfo']) {
            lastPointId = key;
        }
        var ticketInfo = typeof seat['ticketInfo'] != 'undefined' ? seat['ticketInfo'][lastPointId] : [];
        switch (seat['seatType']) {
            case 1:
                seatType = 'cuaxe2';
                break;
            case 2:
                seatType = 'taixe2';
                break;
            case 3:
            case 4:
                seatType = checkStatusSeatAndTicket(ticketInfo.ticketStatus, seat.overTime).seatStatus;

                break;
            case 5:
                seatType = 'nhavesinh';
                break;
            case 6:
                seatType = 'ghephuxe';
                break;
            default:
                seatType = 'loidi';
                break;
        }
        if (seatType == 'ghedadat' || seatType == 'ghegiucho' || seatType == 'ghelenxe') {
            if (typeof seat['ticketInfo'] == 'undefined') {
                $.ajax({
                    dataType: "json",
                    url: urlSeatMapInTrip,
                    data: {
                        'tripId': tripSelected.tripData.tripId,
                        'scheduleId': tripSelected.tripData.scheduleId,
                    },
                    async: false,
                    success: function (data) {
                        tripSelected.tripData.seatMap = data;
                        tripSelected.seatList = data.seatMap.seatList;
                        generateHtmlTripInfo();
                    },
                    error: function () {
                        Message("Lỗi tải danh sách vé!", "Vui lòng tải lại trang", "");
                        Loaing(false);
                    }
                });
            }
        }
        var seatHTML = '<div ';
        seatHTML += ' data-ticketId="' + (checkStatusSeatAndTicket(ticketInfo.ticketStatus, seat.overTime).seatStatus === "ghetrong" ? "undefined" : ticketInfo['ticketId']) + '"';
        seatHTML += ' data-extraPrice="' + seat['extraPrice'] + '"';
        seatHTML += ' class="ghe ' + seatType + '" style="width:' + widthOfSeat + '">';
        seatHTML += '<div class="ticketInfo-tool">';
        seatHTML += '<div title="Kiểm tra vé" class="seatChecked '+(ticketInfo.isChecked!=undefined&&ticketInfo.isChecked==true?"isChecked":"")+'"><strong class="seatId">' + seat['seatId'] + '</strong></div>';
        var ticketInfo = typeof seat['ticketInfo'] != 'undefined' ? seat['ticketInfo'][lastPointId] : [];
        var addressdrop = '', addresspick = '';
        if (typeof ticketInfo.getInPoint != 'undefined' || typeof ticketInfo['dropAlongTheWayAddress'] != 'undefined' ||
            typeof ticketInfo['dropOffAddress'] != 'undefined' && ticketInfo['dropOffAddress'] != '') {
            if (typeof ticketInfo['dropAlongTheWayAddress'] != 'undefined') {
                addressdrop = ticketInfo['dropAlongTheWayAddress'];
            } else {
                if (typeof ticketInfo['dropOffAddress'] != 'undefined' && ticketInfo['dropOffAddress'] != '') {
                    addressdrop = ticketInfo['dropOffAddress'];
                } else {
                    addressdrop = ticketInfo['getOffPoint']['pointName'];

                }
            }
            if (addressdrop.split(', ').length >= 2) {
                addressdrop = addressdrop.split(', ', 2).toString();
            }
            if (typeof ticketInfo['alongTheWayAddress'] != 'undefined' && ticketInfo['alongTheWayAddress'] != '') {
                addresspick = ticketInfo['alongTheWayAddress'];
            } else {

                if (typeof ticketInfo['pickUpAddress'] != 'undefined' && ticketInfo['pickUpAddress'] != '') {
                    addresspick = ticketInfo['pickUpAddress'];
                }
                else {
                    addresspick = ticketInfo['getInPoint']['pointName'];
                }
            }
            if (addresspick.split(', ').length >= 2) {
                addresspick = addresspick.split(', ', 2).toString();
            }
        }

        if ( {{ count(session("userLogin")["userInfo"]['listAgency']) }} >
        0
    )
        {
            if (seatType == 'ghetrong' && tripSelected.tripData.tripStatus != 2) {
                seatHTML += '<div>' +
                    //                        '<button type="button" class="btnFlashSell btn btn-primary" title="Đặt vé nhanh"><i class="fa fa-bolt"></i></button>' +
                    '<button type="button" data-toggle="modal" data-target="#sell_ticket" title="Đặt vé thường" class="btnSellTicket btn btn-primary"><i class="fas fa-plus"></i></button>' +
                    '</div>';
            }

            if (ticketInfo.ticketId != undefined && ticketInfo.agencyUserId == '{{ session("userLogin")["userInfo"]['userId'] }}') {


                if (seatType == 'ghedadat' || seatType == 'ghegiucho' || seatType == 'ghelenxe') {
                    seatHTML += '<div>' +
                        '<button type="button" class="btn btn-primary btnPrintTicket" title="in vé" value="' + ticketInfo['ticketId'] + '"><i class="fas fa-print"></i></a>' +
                        '<button type="button" class="btn btn-primary makeCall" value="' + ticketInfo['phoneNumber'] + '"><i class="fas fa-phone"></i></a>' +
                        '<button type="button" data-seatId="' + seat['seatId'] + '" value="' + ticketInfo['ticketId'] + '" class="btn editTicket btn-primary"><i class="fas fa-pencil-alt"></i></button>';
                    if (tripSelected.tripData.tripStatus != 2) {
                        seatHTML += '<button type="button" class="btn btn-primary btnFlashMoveSeat" title="chuyển ghế nhanh" value="' + ticketInfo['ticketId'] + '"><i class="fas fa-exchange-alt"></i></a>' +
                            '<button type="button" data-seatId="' + seat['seatId'] + '" class="btn btn-primary btnTransferTicket"><i class="fas fa-arrows-alt"></i></button>';
                    }

                    seatHTML += '</div>';
                }
                seatHTML += '</div>';


                if (seatType == 'ghedadat' || seatType == 'ghegiucho' || seatType == 'ghelenxe') {

                    if (seatType == 'ghedadat') {
                        seatHTML += '<div class="ticketInfo-getinpoint agencyName thanhtoan visible-desktop">' + (ticketInfo['sourceName'] || '') + '</div>';
                    } else {
                        seatHTML += '<div class="ticketInfo-getinpoint agencyName visible-desktop">' + (ticketInfo['sourceName'] || '') + '</div>';
                    }

                    seatHTML += '<div class="ticketInfo-content visible-desktop">';
                    seatHTML += '<p><i class="fas fa-ticket-alt m_rigth_5"></i> ' + ticketInfo['ticketCode'] + '</p>';
                    seatHTML += '<p><i class="fas fa-dollar-sign m_rigth_5" ></i> ' + moneyFormat(ticketInfo['agencyPrice']) + '</p>';


                    if (ticketInfo['promotionCode'] != "" && ticketInfo['promotionCode'] != undefined) {
                        seatHTML += '<p><i class="fas fa-gift m_rigth_5" ></i> ' + ticketInfo['promotionCode'] + '</p>';
                    }
                    seatHTML += '<p><i class="fas fa-clock m_rigth_5" ></i> B: ' + (ticketInfo['sellerName'] || '') + '  / P: ' + (ticketInfo['sellerName'] || '') + '</p>';
                    if (seatType == 'ghedadat') {
                        seatHTML += '<p><i class="fas fa-user m_rigth_5"></i> ' + ticketInfo['fullName'] + ' <span style="color: #f2ff85">(' + ticketInfo['listSeatId'].length + 'G)</span></p>';
                    } else {
                        seatHTML += '<p><i class="fas fa-user m_rigth_5"></i> ' + ticketInfo['fullName'] + ' <span style="color: #FF0000">(' + ticketInfo['listSeatId'].length + 'G)</span></p>';
                    }
                    var phoneNumber = ticketInfo['phoneNumber'] !== '' ? ('0' + $.number(ticketInfo['phoneNumber'])) : '';
                    seatHTML += '<p><i class="fas fa-phone m_rigth_5"></i> ' + phoneNumber + '</p>';
                    seatHTML += '<p><i class="fas fa-sticky-note m_rigth_5"></i> ' + ticketInfo['note'] + '</p>';
                    if (seatType == 'ghedadat') {
                        seatHTML += '<p style="color: #f2ff85; font-weight: bold"><i class="fas fa-arrow-up m_rigth_5"></i> ' + addresspick + '</p>';
                    } else {
                        seatHTML += '<p style="color: #000000; font-weight: bold"><i class="fas fa-arrow-up m_rigth_5"></i> ' + addresspick + '</p>';
                    }
                    seatHTML += '<p><i class="fas fa-arrow-down m_rigth_5"></i> ' + addressdrop + '</p>';

                    seatHTML += '</div>';
                }

                seatHTML += '</div>';
            } else {
                seatHTML += '</div>';
                if (seatType == 'ghedadat' || seatType == 'ghegiucho' || seatType == 'ghelenxe') {
                    seatHTML += '<img style="max-width:100px;padding-top:60px;padding-left:80px;" src="{{ asset('public/images/DaDatVe.png')  }}" class="img-responsive">';
                }
                seatHTML += '</div>';
            }
        }
    else
        {
            if (seatType == 'ghetrong' && tripSelected.tripData.tripStatus != 2) {
                seatHTML += '<div>' +
                    //                        '<button type="button" class="btnFlashSell btn btn-primary" title="Đặt vé nhanh"><i class="fa fa-bolt"></i></button>' +
                    '<button type="button" data-toggle="modal" data-target="#sell_ticket" class="btnSellTicket btn btn-primary"><i class="fas fa-plus"></i></button>' +
                    '</div>';
            }

            if (ticketInfo.ticketId != undefined && (seatType == 'ghedadat' || seatType == 'ghegiucho' || seatType == 'ghelenxe')) {
                seatHTML += '<div>' +
                    '<button type="button" class="btn btn-primary btnPrintTicket" title="in vé" value="' + ticketInfo['ticketId'] + '"><i class="fas fa-print"></i></a>' +
                    '<button type="button" class="btn btn-primary makeCall" value="' + ticketInfo['phoneNumber'] + '"><i class="fas fa-phone"></i></a>' +
                    '<button type="button" data-seatId="' + seat['seatId'] + '" value="' + ticketInfo['ticketId'] + '" class="btn editTicket btn-primary"><i class="fas fa-pencil-alt"></i></button>';
                if (tripSelected.tripData.tripStatus != 2) {
                    seatHTML += '<button type="button" class="btn btn-primary btnFlashMoveSeat" title="chuyển ghế nhanh" value="' + ticketInfo['ticketId'] + '"><i class="fas fa-exchange-alt"></i></a>' +
                        '<button type="button" data-seatId="' + seat['seatId'] + '" class="btn btn-primary btnTransferTicket"><i class="fas fa-arrows-alt"></i></button>';
                }

                seatHTML += '</div>';
            }
            seatHTML += '</div>';


            if (ticketInfo.ticketId != undefined && (seatType == 'ghedadat' || seatType == 'ghegiucho' || seatType == 'ghelenxe')) {

                if (seatType == 'ghedadat') {
                    seatHTML += '<div class="ticketInfo-getinpoint agencyName thanhtoan visible-desktop">' + (ticketInfo['sourceName'] || '') + '</div>';
                } else {
                    seatHTML += '<div class="ticketInfo-getinpoint agencyName visible-desktop">' + (ticketInfo['sourceName'] || '') + '</div>';
                }

                seatHTML += '<div class="ticketInfo-content visible-desktop">';
                seatHTML += '<p><i class="fas fa-ticket-alt m_rigth_5"></i> ' + ticketInfo['ticketCode'] + '</p>';
                seatHTML += '<p><i class="fas fa-dollar-sign m_rigth_5" ></i> ' + moneyFormat(ticketInfo['agencyPrice']) + '</p>';
                if (ticketInfo['promotionCode'] != "" && ticketInfo['promotionCode'] != undefined) {
                    seatHTML += '<p><i class="fas fa-gift m_rigth_5" ></i> ' + ticketInfo['promotionCode'] + '</p>';
                }
                seatHTML += '<p><i class="fas fa-clock m_rigth_5" ></i> Bán: ' + (ticketInfo['sellerName'] || '') + '  / Thu: ' + (ticketInfo['sellerName'] || '') + '</p>';
                if (seatType == 'ghedadat') {
                    seatHTML += '<p><i class="fas fa-user m_rigth_5"></i> ' + ticketInfo['fullName'] + ' <span style="color: #f2ff85">(' + ticketInfo['listSeatId'].length + 'G)</span></p>';
                } else {
                    seatHTML += '<p><i class="fas fa-user m_rigth_5"></i> ' + ticketInfo['fullName'] + ' <span style="color: #FF0000">(' + ticketInfo['listSeatId'].length + 'G)</span></p>';
                }
                var phoneNumber = ticketInfo['phoneNumber'] !== '' ? ('0' + $.number(ticketInfo['phoneNumber'])) : '';
                seatHTML += '<p><i class="fas fa-phone m_rigth_5"></i> ' + phoneNumber + '</p>';
                seatHTML += '<p><i class="fas fa-sticky-note m_rigth_5"></i> ' + ticketInfo['note'] + '</p>';
                if (seatType == 'ghedadat') {
                    seatHTML += '<p style="color: #f2ff85; font-weight: bold"><i class="fas fa-arrow-up m_rigth_5"></i> ' + addresspick + '</p>';
                } else {
                    seatHTML += '<p style="color: #000000; font-weight: bold"><i class="fas fa-arrow-up m_rigth_5"></i> ' + addresspick + '</p>';
                }
                seatHTML += '<p><i class="fas fa-arrow-down m_rigth_5"></i> ' + addressdrop + '</p>';
                //seatHTML += '<p>Trạng thái: ' + checkStatusSeatAndTicket(seat.seatStatus, seat.overTime).TicketStatusMessage + '</p>';
                //seatHTML += '<p>BL: ' + ticketInfo['pickUpAddress'] + '</p>';
                //seatHTML += '<p>BX: ' + ticketInfo['dropOffAddress'] + '</p>';
                seatHTML += '</div>';
            }

            seatHTML += '</div>';
        }

        return seatHTML;
    }

    function refreshUrl() {
        window.history.replaceState({}, document.title, window.location.href.split('?')[0]);
    }

    function checkStatusSeatAndTicket(status, overTime) {

        /*
        *   INVALID(-2),
            CANCELED(0),
            EMPTY(1), // rỗng. hết hạn giữ chỗ sẽ chuyển về trạng thái này
            BOOKED(2), // đã giữ - (SMS khoảng cách <=10 km)
            BOUGHT(3), // đã thanh toán - (SMS khoảng cách <=10 km)
            ON_THE_TRIP(4), // đã lên xe
            COMPLETED(5), // đã hoàn thành
            OVERTIME(6),// quá giờ giữ chỗ
            BOOKED_ADMIN(7) // siêu phụ xe đặt chỗ
        * */
        var result = {seatStatus: '', TicketStatusHTML: '', TicketStatusMessage: ''};

        switch (status) {
            case 0:
            case 1:
            case 5:
            case 6:
                result.seatStatus = 'ghetrong';
                result.TicketStatusHTML = 'Hết hạn giữ chỗ';
                result.TicketStatusMessage = 'Hết hạn giữ chỗ';
                break;
            case 3:
                result.seatStatus = 'ghedadat';
                result.TicketStatusHTML = '<div class="gc_ghe gc_dadat"></div>';
                result.TicketStatusMessage = 'Thanh toán';
                break;

            case 4:
                result.seatStatus = 'ghelenxe';
                result.TicketStatusHTML = '<div class="gc_ghe gc_dadat"></div>';
                result.TicketStatusMessage = 'Đã lên xe';

                break;
            case 2:
                var timess = (Date.now() > tripSelected.tripData.getInTime) ? tripSelected.tripData.getInTime : Date.now();
                if (overTime == 0 || overTime > timess) {
                    result.seatStatus = 'ghegiucho';
                    result.TicketStatusHTML = '<div class="gc_ghe gc_giucho"></div>';
                    result.TicketStatusMessage = 'Giữ chỗ';
                } else {
                    result.seatStatus = 'ghetrong';
                    result.TicketStatusHTML = 'Hết hạn giữ chỗ';
                    result.TicketStatusMessage = 'Hết hạn giữ chỗ';
                }
                break;
            case 7:
                result.seatStatus = 'ghegiucho';
                result.TicketStatusHTML = '<div class="gc_ghe gc_giucho"></div>';
                result.TicketStatusMessage = 'Ưu tiên giữ chỗ';
                break;
            default:
                result.seatStatus = 'ghetrong';
                break;
        }
        return result;
    }

    function Loaing(onLoad) {
        $('form#searchTrip select,form#searchTrip button,form#searchTrip input').prop('disabled', onLoad);
        if (onLoad) {
            $('#loading').addClass('loading');
        } else {
            $('#loading').removeClass('loading');
        }
    }

    function printTicket(ticketId) {
        window.open("/cpanel/ticket/print/" + ticketId, null,
            "height=300,width=600,status=yes,toolbar=no,menubar=no,location=no");
    }

    /*
    * Lưu trạng thái lựa chọn hiện tại
    * */
    function saveFormDataLocal() {

        if (typeof(Storage) !== "undefined") {
            if ($(selectBoxRoute).val() != '' && $(selectBoxRoute).val() != undefined) sessionStorage.setItem("routeId", $(selectBoxRoute).val());
            if ($(selectBoxListTrip).val() != '' && $(selectBoxListTrip).val() != undefined) sessionStorage.setItem("tripId", $(selectBoxListTrip).val());
            if ($(selectBoxListTrip).find('option:selected').data('schedule') != '' && $(selectBoxListTrip).find('option:selected').data('schedule') != undefined) sessionStorage.setItem("scheduleId", $(selectBoxListTrip).find('option:selected').data('schedule'));
            if ($(datepickerBox.datepicker()).val() != '' && $(datepickerBox.datepicker()).val() != undefined) sessionStorage.setItem("date", $(datepickerBox.datepicker()).val());

        } else {
            alert("Trình duyệt không được hỗ trợ! Hãy cập nhật bản mới nhất.");
        }
    }

    function stringToInt(str) {
        var val = str.replace(/[^0-9]/gi, '');
        return val;
    }

    function ChangeUrl() {
        var url = window.location.href;

        if (url.indexOf('?') != -1) {
            url = url.slice(0, url.indexOf('?'));
        }

        if (typeof (history.pushState) != "undefined") {
            var obj = {Title: 'An vui - Bán vé', Url: url};
            history.pushState(obj, obj.Title, obj.Url);
        } else {
            alert("Browser does not support HTML5.");
        }
    }

    /*
    * In danh sách hành khách
    * */

    function generateCustomerHTML(TripSelected) {
        /*
        * Thông tin chuyến
        * */
        $('.chuyen').html(TripSelected.tripData.routeName);
        $('.bienso').html(TripSelected.tripData.numberPlate);
        $('.laixe').html(TripSelected.tripData.driverName || '-');
        $('.phuxe').html(TripSelected.tripData.assName || '-');
        $('.startTime').html(getFormattedDate(TripSelected.tripData.getInTime, 'date') || '-');
        listTicket = getListTicket(TripSelected.tripData.seatMap);
        var listPickupCustomer = $.grep(listTicket, function (val) {
            if (typeof val.ticketInfo[val.listTicketId[0]] != 'undefined' && typeof val.ticketInfo[val.listTicketId[0]].pickUpAddress != 'undefined')
                return val.ticketInfo[val.listTicketId[0]].pickUpAddress;
            else return;
        });
        var listDropOffCustomer = $.grep(listTicket, function (val) {
            if (typeof val.ticketInfo[val.listTicketId[0]] != 'undefined' && typeof val.ticketInfo[val.listTicketId[0]].dropOffAddress != 'undefined')
                return val.ticketInfo[val.listTicketId[0]].dropOffAddress;
            else return;
        });

        var htmlListCustomer = '',
            count = 0, dem = 1, tongghe = 0, phaithu = 0,
            blank = "<td style=' border: 1px solid;text-align: center;text-align: center'></td>";
        if (listTicket == '') {
            // htmlListCustomer = "<tr><td class='center' colspan='12'>Hiện không có dữ liệu</td></tr>";
        }
        else {
            listTicket.forEach(function (v) {
                var lastPointId = '';
                for (key in v['ticketInfo']) {
                    lastPointId = key;
                }
                console.log(v);
                var ticketInfo = v['ticketInfo'][lastPointId], addressdrop = '', addresspick = '';
                if (typeof ticketInfo.getInPoint != 'undefined' || typeof ticketInfo['dropAlongTheWayAddress'] != 'undefined' ||
                    typeof ticketInfo['dropOffAddress'] != 'undefined') {
                    if (typeof ticketInfo['dropAlongTheWayAddress'] != 'undefined' && ticketInfo['dropAlongTheWayAddress'] != '') {
                        addressdrop = ticketInfo['dropAlongTheWayAddress'];
                    } else {
                        if (typeof ticketInfo['dropOffAddress'] != 'undefined' && ticketInfo['dropOffAddress'] != '') {
                            addressdrop = ticketInfo['dropOffAddress'];
                        } else {
                            addressdrop = ticketInfo['getOffPoint']['pointName'];

                        }
                    }
                    if (addressdrop.split(', ').length >= 2) {
                        addressdrop = addressdrop.split(', ', 2).toString();
                    }
                    if (typeof ticketInfo['dropAlongTheWayAddress'] != 'undefined' && ticketInfo['dropAlongTheWayAddress'] != '')
                        addressdrop = '<strong>' + addressdrop + '</strong>';
                    if (typeof ticketInfo['alongTheWayAddress'] != 'undefined' && ticketInfo['alongTheWayAddress'] != '') {
                        addresspick = ticketInfo['alongTheWayAddress'];
                    } else {

                        if (typeof ticketInfo['pickUpAddress'] != 'undefined' && ticketInfo['pickUpAddress'] != '') {
                            addresspick = ticketInfo['pickUpAddress'];
                        }
                        else {
                            addresspick = ticketInfo['getInPoint']['pointName'];
                        }
                    }
                    if (addresspick.split(', ').length >= 2) {
                        addresspick = addresspick.split(', ', 2).toString();
                    }

                    if (typeof ticketInfo['alongTheWayAddress'] != 'undefined' && ticketInfo['alongTheWayAddress'] != '') {
                        addresspick = '<strong>' + addresspick + '</strong>'; moneyFormat()
                    }
                }
                var ma = "<tr><td style=' border: 1px solid;text-align: center;text-align: center'>" + ticketInfo.ticketCode + "</td>",
                    stt = "<tr><td style=' border: 1px solid;text-align: center;text-align: center'>" + dem + "</td>",
                    soghe = "<td style=' border: 1px solid;text-align: center;text-align: center'>" + ticketInfo.listSeatId.length + "</td>",
                    tenghe = "<td style=' border: 1px solid;text-align: center;text-align: center'>" + ticketInfo.listSeatId.join(', ') + "</td>",
                    kh = "<td style=' border: 1px solid;text-align: center;text-align: center'>" + (ticketInfo.fullName != 'Khách vãng lai' ? ticketInfo.fullName : '') + "</td>",
                    sdt = "<td style=' border: 1px solid;text-align: center;text-align: center'>" + (ticketInfo.phoneNumber != '' ? (ticketInfo.phoneNumber.substring(0,3)+'.'+ticketInfo.phoneNumber.substring(3,7)+'.'+ticketInfo.phoneNumber.substring(7)) : '') + "</td>",
                    don = "<td style=' border: 1px solid;text-align: center;text-align: center'>" + addresspick + "</td>",
                    tra = "<td style=' border: 1px solid;text-align: center;text-align: center'>" + addressdrop + "</td>",
                    daili = ticketInfo.sourceName != undefined ? "<td style=' border: 1px solid;text-align: center;text-align: center'>" + ticketInfo.sourceName + "</td>" : "<td style=' border: 1px solid;text-align: center;text-align: center'></td>",
                    trangthai = "<td style=' border: 1px solid;text-align: center;text-align: center'>" + checkStatusSeatAndTicket(ticketInfo.ticketStatus, ticketInfo.overTime).TicketStatusMessage + "</td>",
                    thanhtien = "<td style=' border: 1px solid;text-align: center;text-align: center'>" + $.number(ticketInfo.agencyPrice) + "</td>",
                    dathu = "<td style=' border: 1px solid;text-align: center;text-align: center'>" + $.number(ticketInfo.paidMoney) + "</td>",
                    thutien = "<td style=' border: 1px solid;text-align: center;text-align: center'><strong>" + ((ticketInfo.sourceId != 'TC03m1MSnhqwtfE' || ticketInfo.ticketStatus == 3) ? 0 : $.number(ticketInfo.agencyPrice - ticketInfo.paidMoney)) + "</strong></td>",
                    conlai = "<td style=' border: 1px solid;text-align: center;text-align: center'><strong>" + $.number(ticketInfo.agencyPrice - ticketInfo.paidMoney) + "</strong></td>",
                    ghichu = "<td style=' border: 1px solid;text-align: center;text-align: center'>" + (typeof ticketInfo.note != 'undefined' ? ticketInfo.note : "") + "</td></tr>"


                ;

                if ((ticketInfo.ticketStatus == 2 && (ticketInfo.overTime > Date.now() || ticketInfo.overTime == 0)) || ticketInfo.ticketStatus == 4 || ticketInfo.ticketStatus == 3 || ticketInfo.ticketStatus == 7) {
                    phaithu += ((ticketInfo.sourceId != 'TC03m1MSnhqwtfE' || ticketInfo.ticketStatus == 3) ? 0 : $.number(ticketInfo.agencyPrice - ticketInfo.paidMoney));
                    if ('{{session('companyId')}}' == 'TC0481HSc6Pl7ko') {
                        for (var i = ticketInfo.listSeatId.length; i > 0; i--) {
                            stt = "<tr><td style=' border: 1px solid;text-align: center;text-align: center'>" + dem + "</td>";
                            dem++;
                            htmlListCustomer += stt + kh + sdt + don + tra + trangthai + ghichu;
                        }
                        dem--;
                    }
                    else if ('{{ session('companyId') }}' == 'TC03m1MSnhqwtfE') {
                        if (ticketInfo.ticketStatus == 7) {
                            if (ticketInfo.sourceId == 'TC03m1MSnhqwtfE')
                                daili += "<td style=' border: 1px solid;text-align: center;text-align: center'>Văn phòng</td>";
                            else
                                daili += "<td style=' border: 1px solid;text-align: center;text-align: center'>Đại lí thu</td>";
                        } else
                            daili += "<td style=' border: 1px solid;text-align: center;text-align: center'>" + checkStatusSeatAndTicket(ticketInfo.ticketStatus, ticketInfo.overTime).TicketStatusMessage + "</td>";

                        htmlListCustomer += ma + soghe + tenghe + kh + sdt + don + tra + daili + thutien + ghichu;
                    }
                    else
                        if('{{session('companyId')}}'=='TC03h1IzK1jParS') htmlListCustomer +=ma+soghe+tenghe+sdt+don+tra+thanhtien+dathu+conlai+ghichu;
                           else htmlListCustomer += ma + soghe + tenghe + kh + sdt + don + tra + daili + trangthai + ghichu;
                    tongghe += ticketInfo.listSeatId.length;
                    dem++;

                }
            });
            if ('{{ session('companyId') }}' == 'TC03m1MSnhqwtfE') {
                var td_tongghe = "<td><strong>" + tongghe + " Ghế</strong></td>";
                var td_phaithu = "<td style='text-align: center'><strong>" + moneyFormat(phaithu) + "</strong></td>";
                htmlListCustomer += "<tr style='border: 1px solid;'><td><strong>Tổng cộng :</strong></td>" + td_tongghe + "<td></td><td></td><td></td><td></td><td></td><td></td><td></td>" + td_phaithu + "<td></td>";
            }
        }
        if ('{{session('companyId')}}' == 'TC0481HSc6Pl7ko') {
            while (dem <= 23) {
                var tdSTT = "<td style=' border: 1px solid;text-align: center;text-align: center'>" + dem + "</td>";
                var trBlank = "<tr>" + tdSTT + blank + blank + blank + blank + blank + blank + "</tr>";
                htmlListCustomer += trBlank;
                dem++;
            }
        }
        $('.totalSeat').html(tongghe);
        var htmlHeaderPhucLocTho = '';
        if ('{{ session('companyId') }}' == 'TC0481HSc6Pl7ko') {
            var d = new Date(tripSelected.tripData.startDate);
            htmlHeaderPhucLocTho += '<div class="text-center">' +
                '            <div style="width:30%;margin:0px;padding: 0px;float: left;">' +
                '                <h4 style="font-weight: bold">PHÚC LỘC THỌ</h4>' +
                '                <h4 style="font-weight: bold">LIMOUSINE</h4><br>' +
                '                <p style="font-size: 15px;">Số : ...............</p>' +
                '            </div>\n' +
                '            <div style="width:65%;margin:0px;padding: 0px;float:right;">' +
                '                <h4 style="font-weight: bold">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</h4>' +
                '                <h4 style="font-weight: bold">Độc Lập - Tự do - Hạnh Phúc</h4>' +
                '                <div style="font-weight: bold">--------------------------------------</div>' +
                '                <div>Nam Định, ngày ' + d.getDate() + ' tháng ' + parseInt(d.getMonth() + 1) + ' Năm ' + d.getFullYear() + '</div>' +
                '                <br>' +
                '            </div>' +
                '        </div>';
        }
        $('.dshkHeader').html(htmlHeaderPhucLocTho);
        $('#list-customer').html(htmlListCustomer);
        makeListPickup(listPickupCustomer);
        makeSeatMapHtml(TripSelected);

        /// danh sách trung chuyen tra
        if ('{{session('companyId')}}' == 'TC03m1MSnhqwtfE') {
            var htmlListCustomer = '', count = 1;
            if (listDropOffCustomer[0] == null) {
                htmlListCustomer = "<tr><td class='center' colspan='8'>Hiện không có dữ liệu</td></tr>";
            }
            else {
                var listDiver = {!!json_encode($listDriver)!!};
                listDropOffCustomer.forEach(function (v) {
                    var lastPointId = '';
                    for (key in v['ticketInfo']) {
                        lastPointId = key;
                    }
                    var ticketInfo = v['ticketInfo'][lastPointId],
                        listOpUp = '', listOpDown = '';
                    for (var i = 0; i < listDiver.length; i++) {
                        if ((ticketInfo.transshipmentPickUpDriver != undefined && ticketInfo.transshipmentPickUpDriver == listDiver[i].userId))
                            listOpUp += "<option selected value='" + listDiver[i].userId + "'>" + listDiver[i].fullName + "</option>";
                        else
                            listOpUp += "<option value='" + listDiver[i].userId + "'>" + listDiver[i].fullName + "</option>";
                        if ((ticketInfo.transshipmentDropOffDriver != undefined && ticketInfo.transshipmentDropOffDriver == listDiver[i].userId))
                            listOpDown += "<option selected value='" + listDiver[i].userId + "'>" + listDiver[i].fullName + "</option>";
                        else
                            listOpDown += "<option value='" + listDiver[i].userId + "'>" + listDiver[i].fullName + "</option>";
                    }
                    var stt = "<tr class='tr_sortdrop'data-id='" + count + "'><td style='width: 30px; border: 1px solid ;'><input class='inputSortDrop' data-id='" + count + "' style='width: 30px' type='number'></td>",
                        soghe = "<td style=' border: 1px solid;text-align: center'>" + (ticketInfo.listSeatId).length + "</td>",
                        mave = "<td class='ticketID' data-val='" + ticketInfo.ticketId + "'style='border: 1px solid;text-align: center'>" + ticketInfo.ticketCode + "</td>",
                        mavehide = "<td hidden class='ticketID' data-val='" + ticketInfo.ticketId + "'style='border: 1px solid;text-align: center'>" + ticketInfo.ticketCode + "</td>",
                        dsghe = "<td style=' border: 1px solid;text-align: center'>" + ticketInfo.listSeatId.join(', ') + "</td>",
                        don = "<td style='border: 1px solid;text-align: center'>" + (splitString(ticketInfo.pickUpAddress) != '' ? splitString(ticketInfo.pickUpAddress) : splitString(ticketInfo.getInPoint.pointName)) + "</td>",
                        tra = "<td style='border: 1px solid;text-align: center'>" + (splitString(ticketInfo.dropOffAddress) != '' ? splitString(ticketInfo.dropOffAddress) : splitString(ticketInfo.getOffPoint.pointName)) + "</td>",
                        hoten = "<td style=' border: 1px solid;text-align: center'>" + (ticketInfo.fullName) + "</td>",
                        sdt = "<td style=' border: 1px solid;text-align: center'>" + (ticketInfo.phoneNumber != '' ? (ticketInfo.phoneNumber.substring(0,3)+'.'+ticketInfo.phoneNumber.substring(3,7)+'.'+ticketInfo.phoneNumber.substring(7)) : '') + "</td>",
                        daili = ticketInfo.sourceName != undefined ? "<td style='border: 1px solid;text-align: center'>" + ticketInfo.sourceName + "</td>" : "<td style='border: 1px solid;text-align: center'></td>",
                        ghichu = "<td style=' border: 1px solid;text-align: center'>" + (typeof ticketInfo.note != 'undefined' ? ticketInfo.note : "") + "</td>",
                        taixelen = "<td style='width: 10%; border: 1px solid;text-align: center;'>" + "<select class='driverPickUp' style='width: 150px;' value=''><option></option>" + listOpUp,
                        taixexuong = "<td style='width: 10%; border: 1px solid;text-align: center;'>" + "<select class='driverDropOff'  style='width: 150px;' value=''><option></option>" + listOpDown,
                        cuoi = "</select></td></tr>";
                    ;
                    count++;
                    if ((ticketInfo.ticketStatus == 2 && (ticketInfo.overTime > Date.now() || ticketInfo.overTime == 0)) || ticketInfo.ticketStatus == 4 || ticketInfo.ticketStatus == 3 || ticketInfo.ticketStatus == 7) {
                        htmlListCustomer += stt + mavehide + tra + hoten + sdt + dsghe + ghichu + taixelen + cuoi;
                    }
                });
                htmlListCustomer += "<tr class='trsortdrop'><td style='width: 30px'><button class='sortdrop'>Sắp xếp</button>";
                htmlListCustomer += repeatTd(5);
                htmlListCustomer += "<td><button class='upListDriver' style='width: 100%; background-color: #2E6BD0;border: none ; font-weight: bold; '>Chốt danh sách</button></td></tr>";
            }
        }
        $('.totalSeatPickup').html(count);
        $('#list-customer-dropoff').html(htmlListCustomer);
    }
    function repeatTd(number) {
        var html='';
        for(var i=1;i<=number;i++){
            html+='<td></td>';
        }
        return html;
    }
    function makeListPickup(listTicket) {
        var htmlListCustomer = '', count = 1;
        if (listTicket.length == 0) {
            htmlListCustomer = "<tr><td class='center' colspan='12'>Hiện không có dữ liệu</td></tr>";
        }
        else {
            var listDiver = {!!json_encode($listDriver)!!};
            listTicket.forEach(function (v) {
                var lastPointId = '';
                for (key in v['ticketInfo']) {
                    lastPointId = key;
                }
                var ticketInfo = v['ticketInfo'][lastPointId],
                    listOpUp = '', listOpDown = '';
                for (var i = 0; i < listDiver.length; i++) {
                    if ((ticketInfo.transshipmentPickUpDriver != undefined && ticketInfo.transshipmentPickUpDriver == listDiver[i].userId))
                        listOpUp += "<option selected value='" + listDiver[i].userId + "'>" + listDiver[i].fullName + "</option>";
                    else
                        listOpUp += "<option value='" + listDiver[i].userId + "'>" + listDiver[i].fullName + "</option>";
                    if ((ticketInfo.transshipmentDropOffDriver != undefined && ticketInfo.transshipmentDropOffDriver == listDiver[i].userId))
                        listOpDown += "<option selected value='" + listDiver[i].userId + "'>" + listDiver[i].fullName + "</option>";
                    else
                        listOpDown += "<option value='" + listDiver[i].userId + "'>" + listDiver[i].fullName + "</option>";
                }
                var stt = "<tr class='tr_sortpick'data-id='" + count + "'><td style='width: 30px; border: 1px solid ;'><input class='inputSortpick' data-id='" + count + "' style='width: 30px' type='number'></td>",
                    soghe = "<td style=' border: 1px solid;text-align: center'>" + (ticketInfo.listSeatId).length + "</td>",
                    mave = "<td class='ticketID' data-val='" + ticketInfo.ticketId + "'style='border: 1px solid;text-align: center'>" + ticketInfo.ticketCode + "</td>",
                    mavehide = "<td hidden class='ticketID' data-val='" + ticketInfo.ticketId + "'style='border: 1px solid;text-align: center'>" + ticketInfo.ticketCode + "</td>",
                    dsghe = "<td style=' border: 1px solid;text-align: center'>" + ticketInfo.listSeatId.join(', ') + "</td>",
                    don = "<td style='border: 1px solid;text-align: center'>" + (splitString(ticketInfo.pickUpAddress) != '' ? splitString(ticketInfo.pickUpAddress) : splitString(ticketInfo.getInPoint.pointName)) + "</td>",
                    tra = "<td style='border: 1px solid;text-align: center'>" + (splitString(ticketInfo.dropOffAddress) != '' ? splitString(ticketInfo.dropOffAddress) : splitString(ticketInfo.getOffPoint.pointName)) + "</td>",
                    hoten = "<td style=' border: 1px solid;text-align: center'>" + (ticketInfo.fullName) + "</td>",
                    sdt = "<td style=' border: 1px solid;text-align: center'>" + (ticketInfo.phoneNumber != '' ? (ticketInfo.phoneNumber.substring(0,3)+'.'+ticketInfo.phoneNumber.substring(3,7)+'.'+ticketInfo.phoneNumber.substring(7)) : '') + "</td>",
                    daili = ticketInfo.sourceName != undefined ? "<td style='border: 1px solid;text-align: center'>" + ticketInfo.sourceName + "</td>" : "<td style='border: 1px solid;text-align: center'></td>",
                    ghichu = "<td style=' border: 1px solid;text-align: center'>" + (typeof ticketInfo.note != 'undefined' ? ticketInfo.note : "") + "</td>",
                    taixelen = "<td style='width: 10%; border: 1px solid;text-align: center;'>" + "<select class='driverPickUp' style='width: 150px;' value=''><option></option>" + listOpUp,
                    taixexuong = "<td style='width: 10%; border: 1px solid;text-align: center;'>" + "<select class='driverDropOff' style='width: 150px;' value=''><option></option>" + listOpDown,
                    cuoi = "</select></td></tr>";
                ;
                count++;
                if ((ticketInfo.ticketStatus == 2 && (ticketInfo.overTime > Date.now())) || ticketInfo.ticketStatus == 4 || ticketInfo.ticketStatus == 3 || ticketInfo.ticketStatus == 7) {
                    if ('{{session('companyId')}}' == 'TC03m1MSnhqwtfE') htmlListCustomer += stt + mavehide + don + hoten + sdt + dsghe + ghichu + taixelen + cuoi;
                    else if ('{{session('companyId')}}' == 'TC0481HSc6Pl7ko') htmlListCustomer += stt + mavehide + hoten + sdt + don + tra + ghichu + taixelen + taixexuong + cuoi;
                    else htmlListCustomer += stt + mave + soghe + dsghe + hoten + sdt + don + tra + daili + ghichu + taixelen + taixexuong + cuoi;
                }
            });
            htmlListCustomer += "<tr class='trsortpick'><td style='width: 30px'><button class='sortpick'>Sắp xếp</button>";
            if ('{{session('companyId')}}' == 'TC03m1MSnhqwtfE') htmlListCustomer += repeatTd(5);
            else if ('{{session('companyId')}}' == 'TC0481HSc6Pl7ko') htmlListCustomer += repeatTd(6);
            else htmlListCustomer += repeatTd(10);
            htmlListCustomer += "<td><button class='upListDriver' style='width: 100%; background-color: #2E6BD0;border: none ;font-weight: bold; '>Chốt danh sách</button></td></tr>";
        }
        $('.totalSeatPickup').html(count);
        $('#list-customer-pickup').html(htmlListCustomer);
    }

    function makeSeatMapHtml(tripSelected) {
        if (tripSelected.seatList == undefined || tripSelected.tripData.seatMap.seatList != undefined) {
            tripSelected.seatList = tripSelected.tripData.seatMap.seatList;
        }

        if (tripSelected.numberOfColumns == undefined || tripSelected.tripData.seatMap.numberOfColumns != undefined) {
            tripSelected.numberOfColumns = tripSelected.tripData.seatMap.numberOfColumns;
        }

        if (tripSelected.numberOfRows == undefined || tripSelected.tripData.seatMap.numberOfRows != undefined) {
            tripSelected.numberOfRows = tripSelected.tripData.seatMap.numberOfRows;
        }

        if (('{{session('companyId')}}' == 'TC03013FPyeySDW' || '{{session('companyId')}}' == 'TC03m1MSnhqwtfE')) {
            if ((tripSelected.tripData.seatMap).numberOfFloors > 1 && (tripSelected.numberOfColumns <= 3)) {
                var floorHtml = '';
                floorHtml += '<div id="table-map_SaoNghe" ><table class="table table-condensed"><thead><tr>';
                for (var column = 1; column <= (tripSelected.numberOfColumns * 2); column++) {
                    if (column == 2 || column == 5) {
                        if (column == 5) {
                            floorHtml += ' <td style = "width: 4%"></td>';
                        }
                        floorHtml += '<td width="24%" style="border: 1px solid black;margin-top:-10px; margin-bottom:-10px;text-align: center">' +
                            '<strong style="font-size: 16px">' + "Tầng 1" + '</strong></td>';
                    } else if (column == 1 || column == 6) {
                        floorHtml += '<td width="24%" style="border: 1px solid black;margin-top:-10px; margin-bottom:-10px;text-align: center">' +
                            '<strong style="font-size:16px">Tầng 2</strong>' + '</td>';
                    }
                }
                floorHtml += '</tr></thead><tbody >';
                var seat, blank;
                var tripdata = tripSelected.tripData;
                var seatList = tripSelected.seatList;
                var count_col = tripSelected.numberOfColumns;
                var count_row = tripSelected.numberOfRows;
                evenIndex = ((seatList.length) % 2 == 0 ? (seatList.length) : (seatList.length + 1));
                oddIndex = ((seatList.length - 1) % 2 == 0 ? (seatList.length - 2) : (seatList.length - 1));
                for (var row = count_row; row > 1; row--) {
                    floorHtml += '<tr style="height: 120px">';
                    for (column = ((count_col - 1) * 2); column > 0; column--) {
                        blank = ' <td style = "border: 1px solid black;width: 25%">';
                        if (column == (count_col + 1)) {
                            $.each(seatList, function (key, item) {
                                if ((item['column'] == 3) && item['floor'] == 2 && item['row'] == (row)) {
                                    seat = item;
                                    blank = ' <td style = "border: 1px solid black;width: 24%">';
                                    if (seat.seatStatus == 3 && typeof seat.ticketInfo != 'undefined' && (seat.ticketInfo[seat.listTicketId[0]].agencyPrice <= seat.ticketInfo[seat.listTicketId[0]].paidMoney))
                                        blank += '<div class="den"style="float: left; border: 10px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + seat.seatId;
                                    else blank += '<div class="den"style="float: left; border: 1px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + seat.seatId;
                                    return false;
                                }
                            });
                        } else if (column == count_col) {
                            $.each(seatList, function (key, item) {
                                if ((item['column'] == 3) && item['floor'] == 1 && item['row'] == (row)) {
                                    seat = item;
                                    blank = ' <td style = "border: 1px solid black;width: 24%">';
                                    if (seat.seatStatus == 3 && typeof seat.ticketInfo != 'undefined' && (seat.ticketInfo[seat.listTicketId[0]].agencyPrice <= seat.ticketInfo[seat.listTicketId[0]].paidMoney))
                                        blank += '<div class="den"style="float: left; border: 10px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + seat.seatId;
                                    else blank += '<div class="den"style="float: left; border: 1px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + seat.seatId;
                                    return false;
                                }
                            });
                        } else if (column == (count_col - 1)) {
                            blank = ' <td style = "width: 4%"></td>';
                            $.each(seatList, function (key, item) {
                                if ((item['column'] == 1 || (item['column'] == 2 && item['row'] == count_row)) && item['floor'] == 1 && item['row'] == (row)) {
                                    seat = item;
                                    blank += ' <td style = "border: 1px solid black;width: 24%">';
                                    if (seat.seatStatus == 3 && typeof seat.ticketInfo != 'undefined' && (seat.ticketInfo[seat.listTicketId[0]].agencyPrice <= seat.ticketInfo[seat.listTicketId[0]].paidMoney))
                                        blank += '<div class="den"style="float: left; border: 10px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + seat.seatId;
                                    else blank += '<div class="den"style="float: left; border: 1px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + seat.seatId;
                                    return false;
                                }
                            });
                        } else if (column == 1) {
                            $.each(seatList, function (key, item) {
                                if ((item['column'] == 1 || (item['column'] == 2 && item['row'] == count_row)) && item['floor'] == 2 && item['row'] == (row)) {
                                    seat = item;
                                    blank = ' <td style = "border: 1px solid black;width: 24%">';
                                    if (seat.seatStatus == 3 && typeof seat.ticketInfo != 'undefined' && (seat.ticketInfo[seat.listTicketId[0]].agencyPrice <= seat.ticketInfo[seat.listTicketId[0]].paidMoney))
                                        blank += '<div class="den"style="float: left; border: 10px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + seat.seatId;
                                    else blank += '<div class="den"style="float: left; border: 1px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + seat.seatId;
                                    return false;
                                }
                            });
                        } else floorHtml += ' <td style = "border: 1px solid black;width: 25%">';
                        if (seat != null && seat.seatStatus != null) {

                            if (seat.listTicketId && seat.listTicketId.length > 0 && (seat.overTime == 0 || seat.overTime > Date.now() || seat.ticketInfo[seat.listTicketId[0]].paidMoney > 0)) {

                                var dem = 0;
                                if (seat.ticketInfo[seat.listTicketId[0]].alongTheWayAddress != null && (typeof seat.ticketInfo[seat.listTicketId[0]].alongTheWayAddress != 'undefined'))
                                    var pickUp = '<strong>' + (seat.ticketInfo[seat.listTicketId[0]].alongTheWayAddress) + '</strong>';
                                else
                                    var pickUp = '';
                                if (seat.ticketInfo[seat.listTicketId[0]].dropAlongTheWayAddress != null && (typeof seat.ticketInfo[seat.listTicketId[0]].dropAlongTheWayAddress != 'undefined'))
                                    var drofOff = '<strong>' + (seat.ticketInfo[seat.listTicketId[0]].dropAlongTheWayAddress) + '</strong>';
                                else
                                    var drofOff = '';
                                if (trip == undefined) {
                                    var trip = {
                                        childrenTicketRatio: 0,
                                        numberOfChildren: 0,
                                        numberOfAdults: 0,
                                        seatList: [],
                                    };

                                    trip.childrenTicketRatio = tripSelected.childrenTicketRatio;
                                    for (var i = 0; i < tripSelected.seatList.length; i++)
                                        trip.seatList[i] = JSON.parse(JSON.stringify(tripSelected.seatList[i]));
                                }
                                var priceSeat = getSeatPrice(trip, seat.ticketInfo[seat.listTicketId[0]], seat, seat['listTicketId'][0]);
                                blank += '</strong></h3></div>Mã vé :<strong>' + seat.ticketInfo[seat.listTicketId[0]].ticketCode + '</strong><br><br>';
                                blank += '</br></br><strong  style="float: left;">' + seat.ticketInfo[seat.listTicketId[0]].fullName + '</strong>';
                                blank += '  -  <strong>' + seat.ticketInfo[seat.listTicketId[0]].phoneNumber + '</strong>';
                                blank += '</br></br><div><span  style="width: 100%">  ' + (pickUp != '' ? pickUp : tripdata.getInPointName) + '</span>';
                                //floorHtml += '</br></br><div><span  style="width: 100%">  ' + pickUp + '</span>';

                                blank += '<span style="width: 100%" > ' + (drofOff != '' ? drofOff : tripdata.getOffPointName) + '</span></div>';
                                //floorHtml += '<span style="width: 100%" > ' + drofOff + '</span></div>';
                                if (seat.ticketInfo[seat.listTicketId[0]].paymentTicketPrice)
                                    blank += 'Giá vé :<h5  style="width: 100%;text-align: left;"><strong>' + priceSeat + '</strong></h5>';
                                else ;
                            } else {
                                blank += '</strong></h3></div>';

                            }

                        }
                        seat = null;
                        floorHtml += blank + '</td>';
                    }
                    floorHtml += '</tr>';
                }
                floorHtml += '</tbody></table></div>';
            } else {
                if (tripSelected.numberOfFloors == undefined) {
                    tripSelected = tripSelected.tripData.seatMap;
                }
                width = 100 / tripSelected.numberOfColumns;
                floorHtml = '';
                for (var floor = 1; floor <= tripSelected.numberOfFloors; floor++) {
                    floorHtml += '<table class="table table-condensed">';
                    floorHtml += '<tbody>';
                    for (var row = tripSelected.numberOfRows; row >= 1; row--) {
                        floorHtml += '<tr style="height: 150px">';
                        for (var col = tripSelected.numberOfColumns; col >= 1; col--) {
                            flat = false;
                            $.each(tripSelected.seatList, function (k, v) {
                                if (v.row == row && v.column == col && v.floor == floor) {
                                    if (v.seatType == 1) {
                                        //floorHtml += '<td width="' + width + '%"  height="150px"><strong>Cửa</strong><br></td>';
                                        floorHtml += '<td style = "width:' + width + '%"></td>';
                                    } else if (v.seatType == 2) {
                                        //floorHtml += '<td width="' + width + '%"  height="150px"><strong>Ghế tài xế</strong><br></td>';
                                        floorHtml += '<td style = "width:' + width + '%"></td>';
                                    }
                                    else if (v.seatType == 3 || v.seatType == 4) {
                                        if ((v.seatStatus == 2 && (v.overTime > Date.now() || v.overTime == 0)) || v.seatStatus == 3 || v.seatStatus == 4) {
                                            var lastPointId = v['listTicketId'][0];

                                            if (lastPointId != '' && lastPointId != null) {
                                                var ticketInfo = v['ticketInfo'][lastPointId];
                                                if (trip == undefined) {
                                                    var trip = {
                                                        childrenTicketRatio: 0,
                                                        numberOfChildren: 0,
                                                        numberOfAdults: 0,
                                                        seatList: [],
                                                    };

                                                    trip.childrenTicketRatio = tripSelected.childrenTicketRatio;
                                                    for (var i = 0; i < tripSelected.seatList.length; i++)
                                                        trip.seatList[i] = JSON.parse(JSON.stringify(tripSelected.seatList[i]));
                                                }

                                                var priceSeat = getSeatPrice(trip, ticketInfo, v, v['listTicketId'][0]);
                                                var drop = (splitString(ticketInfo.dropAlongTheWayAddress) != '' ? splitString(ticketInfo.dropAlongTheWayAddress) : splitString(ticketInfo.getOffPoint.pointName));

                                                var pick = (splitString(ticketInfo.alongTheWayAddress) != '' ? splitString(ticketInfo.alongTheWayAddress) : splitString(ticketInfo.getInPoint.pointName));
//
                                                floorHtml += '<td style = "border: 2px solid black;width:' + width + '%">';
                                                if (v.seatStatus == 3 && typeof v.ticketInfo != 'undefined' && (ticketInfo.agencyPrice <= ticketInfo.paidMoney))
                                                    floorHtml += '<div class="den"style="float: left; border: 10px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + v.seatId;
                                                else floorHtml += '<div class="den"style="float: left; border: 1px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + v.seatId;

                                                floorHtml += '</strong></h4></div>Mã vé :<strong>' + ticketInfo.ticketCode + '</strong><br><br>' +
                                                    'Khách hàng:<strong>' + ticketInfo.fullName + '</strong><br>' +
                                                    'Số điện thoại: ' + ticketInfo.phoneNumber + '<br>';

                                                floorHtml += 'Điểm lên: ' + (pick == splitString(ticketInfo.alongTheWayAddress) ? ('<strong>' + pick + '</strong>') : pick) + '<br>';

                                                floorHtml += 'Điểm xuống:' + (drop == splitString(ticketInfo.dropAlongTheWayAddress) ? ('<strong>' + drop + '</strong>') : drop) + '<br>';
                                                floorHtml += 'Giá ghế:<strong>' + priceSeat + '</strong><br></td>';
                                            }
                                        } else if (v.seatStatus == 2) {
                                            floorHtml += '<td style = "border: 2px solid black;width:' + width + '%"><div class="den"style="float: left; border: 1px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + v.seatId + '</strong></h4></div></td>';
                                        }
                                        if (v.seatStatus == 1 && (v.seatType == 4 || v.seatType == 3)) floorHtml += '<td style = "border: 2px solid black;width:' + width + '%"><div class="den"style="float: left; border: 1px solid;padding: 5px;margin-right: 10px; "><h4 class="trangthaighe"><strong class="trang">' + v.seatId + '</strong></h4></div></td>';
//                                         else {
//                                            floorHtml += '<td><strong>Ghế ' + v.seatId + ': ' + checkStatusSeat(v.seatStatus, v.overTime) + '</strong></td>';
//                                        }
                                    }
                                    else if (v.seatType == 5) {
                                        // floorHtml += '<td><strong>Nhà vệ sinh</strong><br></td>';
                                        floorHtml += '<td style = "width:' + width + '%"></td>';
                                    }
                                    else if (v.seatType == 6) {
                                        floorHtml += '<td style = "width:' + width + '%"></td>';
                                    }
                                    flat = true;
                                }
                            });
                            if (!flat) {
                                floorHtml += '<td style = "width:' + width + '%"></td>';
                            }
                        }
                        floorHtml += '</tr>';
                    }
                    floorHtml += '</tbody>';
                    floorHtml += '<table>';
                }
            }
            floorHtml += '</br><div style="float: left;">';
            floorHtml += '<div style="float: left; border: 8px solid;padding: 5px;margin-right: 8px;">';
            floorHtml += '</div><span>Đã thu tiền</span>';
            floorHtml += '</br></br><div style="float: left; border: 1px solid;padding: 11px;margin-right: 10px;">';
            floorHtml += '</div><span>Chưa thu tiền</span>';
            floorHtml += "</div>";
            evenIndex = 0;
            oddIndex = 0;
        } else {

            if (tripSelected.numberOfFloors == undefined) {
                tripSelected = tripSelected.tripData.seatMap;
            }
            width = 100 / tripSelected.numberOfColumns;
            floorHtml = '';
            for (var floor = 1; floor <= tripSelected.numberOfFloors; floor++) {
                floorHtml += '<table class="table table-condensed">';
                floorHtml += '<caption>Tầng ' + floor + '</caption>';
                floorHtml += '<thead><tr>';
                for (var column = 1; column <= tripSelected.numberOfColumns; column++) {
                    floorHtml += '<td width="' + width + '%"></td>';
                }
                floorHtml += '</thead></tr>';
                floorHtml += '<tbody>';
                for (var row = 1; row <= tripSelected.numberOfRows; row++) {
                    floorHtml += '<tr>';
                    for (var col = 1; col <= tripSelected.numberOfColumns; col++) {
                        flat = false;
                        $.each(tripSelected.seatList, function (k, v) {
                            if (v.row == row && v.column == col && v.floor == floor) {
                                if (v.seatType == 1) {
                                    floorHtml += '<td width="' + width + '%"  height="150px"><strong>Cửa</strong><br></td>';
                                } else if (v.seatType == 2) {
                                    floorHtml += '<td width="' + width + '%"  height="150px"><strong>Ghế tài xế</strong><br></td>';
                                }
                                else if (v.seatType == 3 || v.seatType == 4) {
                                    if(v['listTicketId'] !=undefined && v['listTicketId'][0] !=undefined && v['listTicketId'][0] !=''){
                                        if ((v.seatStatus == 2 && (v.overTime!=undefined && (v.overTime > Date.now() || v.overTime == 0))) || v.seatStatus == 3 || v.seatStatus == 4 ||(v['ticketInfo'] != undefined && (v['ticketInfo']).length >0 && v['ticketInfo'][v['listTicketId'][0]].ticketStatus != undefined && v['ticketInfo'][v['listTicketId'][0]].ticketStatus==7)) {
                                            var lastPointId = v['listTicketId'][0];

                                            if (lastPointId != '' && lastPointId != null) {
                                                var ticketInfo = v['ticketInfo'][lastPointId];
                                                if (trip == undefined) {
                                                    var trip = {
                                                        childrenTicketRatio: 0,
                                                        numberOfChildren: 0,
                                                        numberOfAdults: 0,
                                                        seatList: [],
                                                    };

                                                    trip.childrenTicketRatio = tripSelected.childrenTicketRatio;
                                                    for (var i = 0; i < tripSelected.seatList.length; i++)
                                                        trip.seatList[i] = JSON.parse(JSON.stringify(tripSelected.seatList[i]));
                                                }

                                                var priceSeat = getSeatPrice(trip, ticketInfo, v, v['listTicketId'][0]);
                                                var drop = (splitString(ticketInfo.dropAlongTheWayAddress) != '' ? splitString(ticketInfo.dropAlongTheWayAddress) : splitString(ticketInfo.getOffPoint.pointName));

                                                var pick = (splitString(ticketInfo.alongTheWayAddress) != '' ? splitString(ticketInfo.alongTheWayAddress) : splitString(ticketInfo.getInPoint.pointName));
//
                                                floorHtml += '<td>' +
                                                    '<strong>Ghế ' + v.seatId + ': ' + checkStatusSeat(v.seatStatus, v.overTime) + '</strong><br>' +
                                                    'Mã vé :' + ticketInfo.ticketCode + '</strong><br>' +
                                                    'Khách hàng: ' + ticketInfo.fullName + '<br>' +
                                                    'Số điện thoại: ' + ticketInfo.phoneNumber + '<br>';

                                                floorHtml += 'Điểm lên: ' + (pick == splitString(ticketInfo.alongTheWayAddress) ? ('<strong>' + pick + '</strong>') : pick) + '<br>';

                                                floorHtml += 'Điểm xuống:' + (drop == splitString(ticketInfo.dropAlongTheWayAddress) ? ('<strong>' + drop + '</strong>') : drop) + '<br>';
                                                floorHtml += 'Giá ghế:<strong>' + priceSeat + '</strong><br></td>';
                                            }
                                        }
                                    } else if (v.seatStatus == 2) {
                                        floorHtml += '<td width="' + width + '%" height="150px"><strong>Ghế ' + v.seatId + ': ' + checkStatusSeat(v.seatStatus, v.overTime) + '</strong></td>';
                                    }
                                    if (v.seatStatus == 1 && (v.seatType == 4 || v.seatType == 3)) floorHtml += '<td width="' + width + '%" height="150px"><strong></strong><br><strong></strong><br></td>';

                                }
                                else if (v.seatType == 5) {
                                    floorHtml += '<td><strong>Nhà vệ sinh</strong><br></td>';
                                }
                                else if (v.seatType == 6) {
                                    floorHtml += '<td><strong>Ghế phụ xe</strong><br>' +
                                        '<strong></strong><br>' +
                                        'Tên phụ xe</td>';
                                }
                                flat = true;
                            }
                        });
                        if (!flat) {
                            floorHtml += '<td width="' + width + '%" height="150px"><strong></strong><br><strong></strong><br></td>';
                        }
                    }
                    floorHtml += '</tr>';
                }
                floorHtml += '</tbody>';
                floorHtml += '<table>';
            }
        }

        $('#makeMap').html(floorHtml);
    }

    function getSeatPrice(trip, ticketInfo, v, ticketId) {
        var sale = 0;
        var price = 0;
        var numberSeat = ticketInfo.numberOfAdults + ticketInfo.numberOfChildren;

        var num = 0, list = '(';
        for (var i = 0; i < trip.seatList.length; i++) {
            if (trip.seatList[i].listTicketId != undefined && trip.seatList[i].listTicketId[0] != undefined && tripSelected.seatList[i].listTicketId[0] == ticketId) {
                num++;
                if (list == '(')
                    list = list + tripSelected.seatList[i].seatId;
                else list = list + ',' + tripSelected.seatList[i].seatId;
            }
        }
        list += ')';
        if (num == 1)
            if ((ticketInfo.agencyPrice - ticketInfo.paidMoney) != 0) {
                price = moneyFormat(ticketInfo.agencyPrice - ticketInfo.paidMoney);
            }
            else price = "Đã trả tiền";
        else if ((ticketInfo.agencyPrice - ticketInfo.paidMoney) != 0) price = moneyFormat(ticketInfo.agencyPrice - ticketInfo.paidMoney) + '/' + list;
        else price = "Đã trả tiền ";

        return price;
    }

    function checkStatusSeat(status, overTime) {

        /*
        *   INVALID(-2),
            CANCELED(0),
            EMPTY(1), // rỗng. hết hạn giữ chỗ sẽ chuyển về trạng thái này
            BOOKED(2), // đã giữ - (SMS khoảng cách <=10 km)
            BOUGHT(3), // đã thanh toán - (SMS khoảng cách <=10 km)
            ON_THE_TRIP(4), // đã lên xe
            COMPLETED(5), // đã hoàn thành
            OVERTIME(6),// quá giờ giữ chỗ
            BOOKED_ADMIN(7) // siêu phụ xe đặt chỗ
        * */

        switch (status) {
            case 0:
            case 1:
                return 'Ghế trống';
            case 5:
            case 6:
                return 'Hết hạn giữ chỗ';
            case 3:
                return 'Thanh toán';
            case 4:
                return 'Đã lên xe';
                break;
            case 2:
                var timess = (Date.now() > tripSelected.tripData.getInTime) ? tripSelected.tripData.getInTime : Date.now();
                if (overTime == 0 || overTime > timess) {
                    return 'Giữ chỗ';
                } else {
                    return 'Hết hạn giữ chỗ';
                }
            case 7:
                return 'Ưu tiên giữ chỗ';
            default:
                return 'Không xác định';
        }
    }
</script>