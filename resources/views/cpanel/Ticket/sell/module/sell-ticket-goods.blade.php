
<form id="frmSellTicketGoods" action="{{action('TicketController@postSellTicketGood')}}" method="post"
      enctype="multipart/form-data">
    <div class="span8 banve_thongtintinve">
        <div class="widget widget-2 widget-tabs widget-tabs-2 no_border">
            <h3>THÔNG TIN VÉ GỬI ĐỒ</h3>
            <div class="span8">
                <table class="tb_banve_thongtintinve" cellpadding="4">
                    <tr>
                        <td>SĐT NGƯỜI NHẬN</td>
                        <td><input autocomplete="off"
                                   data-validation="custom required"
                                   data-validation-regexp="^0(([8-9][0-9]{8})|(1[0-9]{9}))$"
                                   data-validation-error-msg-custom="Vui lòng nhập đúng định dạng SĐT"
                                   data-validation-error-msg="Vui lòng nhập số điện thoại người nhận"
                                   class="m_bottom_0 isNumber" type="text"
                                   name="phoneNumberReceiver"/></td>
                    </tr>
                    <tr>
                        <td>NGƯỜI NHẬN</td>
                        <td><input autocomplete="off" class="m_bottom_0" type="text"
                                   name="fullNameReceiver"/></td>
                    </tr>

                    <tr>
                        <td>ĐC NHẬN HÀNG</td>
                        <td>
                            <select name="getInPointId" class="m_bottom_0 startPoint"
                                    id="getInPointId_good">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>ĐC TRẢ HÀNG</td>
                        <td>
                            <select name="getOffPointId" class="m_bottom_0 endPoint"
                                    id="getOffPointId_good">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>TÊN SP</td>
                        <td>
                            <input autocomplete="off" data-validation="required" class="m_bottom_0" type="text"
                                   name="ticketName" data-validation-error-msg="Vui lòng nhập tên sản phẩm"/>
                        </td>
                    </tr>
                    <tr>
                        <td>TRỌNG LƯỢNG (kg)</td>
                        <td>
                            <input autocomplete="off" placeholder="Vd: 1.5" data-validation="required number" data-validation-allowing="float" id="txt_weight" class="m_bottom_0" type="text"
                                   name="weight" value="0"
                                   data-validation-error-msg-required="Vui lòng nhập trọng lượng"
                                   data-validation-error-msg-number="Trọng lượng phải là số và lớn hơn 0. Vd: 1.5"/>
                        </td>
                    </tr>
                    <tr>
                        <td>KÍCH THƯỚC (m3)</td>
                        <td>
                            <input autocomplete="off" placeholder="Vd: 3x6x7" id="txt_dimension" class="m_bottom_0" type="text"
                                   value="0"
                                   name="dimension"/>
                        </td>
                    </tr>
                    <tr>
                        <td>GIÁ VÉ</td>
                        <td id="giaVeGuiDoGoc">
                            0Đ
                        </td>
                    </tr>
                </table>
            </div>
            <div class="frm_chonanh span4">
                <div class="chonanh">
                    <img id="blah" src="/public/images/anhsanpham.png"/>
                </div>
                <input name="img" type="file" id="selectedFile" style="display: none;"/>
                <input type="button" style="width:172px" value="CHỌN ẢNH"
                       class="btncustom"
                       onclick="document.getElementById('selectedFile').click();"/>
            </div>
            <div class="span12">
                <div class="ht_tongtien">
                    <div class="span3 color_blue_2 fs_large fw_ex_bold">TỔNG TIỀN</div>
                    <div class="span9 color_red fs_large fw_ex_bold"
                         id="tongGiaVeGuiDo">0Đ
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="span4 banve_thongtinkhachhang">
        <div class="widget widget-2 widget-tabs widget-tabs-2 no_border">
            <h3>THÔNG TIN NGƯỜI GỬI</h3>
            <table class="tb_banve_thongtinkhachhang" cellpadding="4">
                <tr>
                    <td>SĐT</td>
                    <td><input autocomplete="off"
                               data-validation="custom required"
                               data-validation-regexp="^0(([8-9][0-9]{8})|(1[0-9]{9}))$"
                               data-validation-error-msg-custom="Vui lòng nhập đúng định dạng SĐT"
                               data-validation-error-msg="Vui lòng nhập số điện thoại người gửi"
                               class="m_bottom_0 CustomerPhoneNumber isNumber" type="number"
                               value="{{old('phoneNumber')}}" name="phoneNumber"/>
                    </td>
                </tr>
                <tr>
                    <td>HỌ TÊN</td>
                    <td><input autocomplete="off" class="m_bottom_0 CustomerFullName" type="text"
                               value="{{old('fullName')}}"
                               name="fullName"/></td>
                </tr>

                <tr>
                    <td>EMAIL</td>
                    <td><input autocomplete="off"
                               type="email" data-validation-error-msg-email="Vui lòng nhập đúng định dạng"
                               class="m_bottom_0"
                               value="{{old('email')}}"
                               name="email"/></td>
                </tr>
                <tr>
                    <td>MÃ KM</td>
                    <td>
                        <div class="input-append">
                            <input autocomplete="off" class="m_bottom_0 span9" type="text"/>
                            <button type="button" class="btn btnCheckPromotionCode">CHECK</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="center" colspan="2">
                        <div style="margin-left: 105px;" class="f_left m_right_10">Người
                            nhận
                            trả tiền
                        </div>
                        <input autocomplete="off" type="checkbox" id="nguoi_nhan_tra_tien"
                               name="paidMoney"/>
                        <label class="f_left" for="nguoi_nhan_tra_tien"></label>
                    </td>
                </tr>
                {{-- <tr>
                     <td class="center" colspan="2">
                         <div style="margin-left: 62px;" class="f_left m_right_10">Gửi mã số cho người nhận
                         </div>
                         <input checked type="checkbox" name="sendSMS" id="sent_code_to_receive"/>
                         <label class="f_left" for="sent_code_to_receive"></label>
                     </td>
                 </tr>--}}
                <tr>
                    <td class="center" colspan="2">
                        <div style="margin-left: 77px;" class="f_left m_right_10">In vé
                            sau
                            khi
                            thanh toán
                        </div>
                        <input type="checkbox" id="print_ticket_after_save" name="print"/>
                        <label class="f_left" for="print_ticket_after_save"></label>
                    </td>
                </tr>
                <tr>
                    <td class="center" colspan="2">
                        <select name="paymentType" class="cbb_paymentType m_bottom_0 w_full">
                            <option value="1">Thanh toán trực tiếp</option>
                            <option value="2">Thanh toán tiền mặt cho phụ xe</option>
                            <option value="3">Thanh toán tiền mặt tại quầy</option>
                            <option value="4">Thanh toán bằng tiền mặt khi người nhận nhận đồ</option>
                            <option value="5">Thanh toán bằng tiền mặt tại Payoo</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="center" colspan="2">
                        <input type="hidden" name="listImages[]" id="listImages">
                        <input type="hidden" name="getInTimePlan" class="getInTimePlan"
                               value="{{old('getInTimePlan')}}">
                        <input type="hidden" name="getOffTimePlan"
                               class="getOffTimePlan"
                               value="{{old('getOffTimePlan')}}">
                        <input type="hidden" name="tripId" class="tripId"
                               value="{{old('tripId')}}" id="txt_tripId">
                        <input type="hidden" name="scheduleId" class="scheduleId"
                               value="{{old('scheduleId')}}" id="txt_scheduleId">
                        <input type="hidden" name="startDate" class="startDate"
                               value="{{old('startDate')}}">
                        <input type="hidden" name="routeId" class="routeId" value="{{old('routeId')}}">
                        <input  type="hidden" class="promotionId" value="{{old('promotionId')}}" name="promotionId" />
                        <input  type="hidden" class="promotionCode" value="{{old('promotionCode')}}" name="promotionCode" />
                        <input type="hidden" class="salePrice" name="salePrice"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input id="btnSellTicketGoods" class="btncustom w_full m_top_0" type="submit"
                               value="THANH TOÁN"
                               name="submit">
                    </td>
                </tr>
            </table>
        </div>
    </div>
</form>