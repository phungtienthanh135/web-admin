@extends('cpanel.template.layout')
@section('title', 'Loại đồ')

@section('content')
<style>
    td,th,thead,tbody,tr{
        border: 1px solid !important;
    }
</style>
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Loại đồ</h3></div>
            </div>
        </div>

        <div class="separator bottom"></div>
        <div class="innerLR">
            <div class="row-fluid">
                <div class="control-box">
                    <div class="row-fluid">
                        <h4>Tìm loại đồ</h4>
                    </div>
                    <form action="">
                        <div class="row-fluid">
                            <div class="span3">
                                <label class="center" for="cbb_shipType">Tên loại đồ</label>
                                <select class="span12" name="shipType" id="cbb_shipType" value="">
                                    <option value="">Chọn tên</option>
                                    @foreach($result as $v)
                                        <option {{request('shipType')==$v['goodsTypeName']?'selected':''}}   value="{{$v['goodsTypeName']}}">
                                            {{$v['goodsTypeName']}}
                                        </option>
                                    @endforeach
                                </select>

                            </div>

                            {{--<div class="span3">--}}
                            {{--<label class="center" for="txtCode">Mã loại đồ</label>--}}
                            {{--<input type="text" name="codeShip" id="txtCode"--}}
                            {{--value="{{request('codeShip')}}">--}}
                            {{--</div>--}}

                            <div class="span2">
                                <label class="separator hidden-phone" for="search"></label>
                                <button class="btn btn-info btn-flat-full" id="search">TÌM LOẠI ĐỒ</button>
                            </div>
                            <div class="span2">
                                <label class="separator hidden-phone" for="add"></label>
                                <button data-toggle="modal" data-target="#modal_add"
                                        class="btn btn-warning btn-flat-full" id="add">THÊM LOẠI ĐỒ
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row-fluid">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        {{--<th class="center">Mã</th>--}}
                        <th class="center">Tên</th>
                        <th class="center">Giá (VNĐ)</th>
                        <th class="center">Ngày tạo</th>
                        {{--<th class="center">ID người tạo</th>--}}
                        <th class="center">Tuỳ chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($result as $row)
                        <tr>
                            {{--<td class="center" id="goodsTypeId_{{@$row['goodsTypeId']}}">{{@$row['goodsTypeId']}}</td>--}}
                            <td id="goodsTypeName_{{@$row['goodsTypeId']}}">{{@$row['goodsTypeName']}}</td>
                            <td class="right" id="goodsTypePrice_{{@$row['goodsTypeId']}}">{{@number_format($row['goodsTypePrice'])}}</td>
                            <td>{{date('d-m-Y',$row['createdDate'] /1000)}}</td>
                            {{--<td class="center">{{@$row['createdUserId']}}</td>--}}
                            <td class="center">
                                <a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                    <ul class="onclick-menu-content">
                                        <li>
                                            <button data-toggle="modal" data-target="#modal_edit"
                                                    value="{{$row['goodsTypeId']}}" class="_edit">Sửa
                                            </button>
                                        </li>
                                        <li>
                                            <button data-toggle="modal" data-target="#modal_delete"
                                                    value="{{$row['goodsTypeId']}}" class="_delete">Xóa
                                            </button>
                                        </li>
                                    </ul>
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td class="center" colspan="6">Hiện tại không có dữ liệu</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal modal-custom hide fade" id="modal_add">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="center">thêm loại đồ</h3>
        </div>
        <form action="{{action('ShipController@addTypeShip')}}" method="post" class="form-horizontal" id="form_add">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtTenLoaiDo">Tên loại đồ</label>
                        <div class="controls">
                            <input required class="span6" type="text"
                                   name="goodsTypeName" id="txtTenLoaiDo">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="txtGia">Giá đồ</label>
                        <div class="controls form-inline">
                            <input required min="1" class="span6" type="number"
                                   name="goodsTypePrice" id="txtGia">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="control-group">
                    <div class="controls">
                        <div class="row-fluid">
                            <div class="span6">
                                <button class="btn btn-warning btn-flat-full">THÊM MỚI</button>
                            </div>
                            <div class="span4">
                                <button type="reset" data-dismiss="modal" aria-hidden="true"
                                        class="btn btn-default btn-flat-full no_border">HỦY
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>


    <div class="modal modal-custom hide fade" id="modal_edit">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="center">SỬA LOẠI ĐỒ </h3>
        </div>
        <form action="{{action('ShipController@repairTypeShip')}}" method="post" class="form-horizontal">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="goodsTypeId" id="goodsTypeId_edit">
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="goodsTypeName_edit">Tên loại đồ</label>
                        <div class="controls">
                            <input class="span6" type="text" name="goodsTypeName" id="goodsTypeName_edit">
                        </div>
                    </div>
                    <div class="control-group">

                        <label class="control-label" for="goodsTypePrice_edit">Giá</label>
                        <div class="controls form-inline">
                            <input class="span6" type="number" name="goodsTypePrice" id="goodsTypePrice_edit">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="control-group">
                    <div class="controls">
                        <div class="row-fluid">
                            <div class="span6">
                                <button class="btn btn-warning btn-flat-full">SỬA</button>
                            </div>
                            <div class="span4">
                                <button data-dismiss="modal" aria-hidden="true"
                                        class="btn btn-default btn-flat-full no_border">HỦY
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>



    <div class="modal hide fade" id="modal_delete" style="width: 250px; margin-left: -10%;margin-top: 10%;">
        <div class="modal-body center">

            <p>Bạn có chắc muốn xoá?</p>
        </div>
        <form action="{{action('ShipController@deleteTypeShip')}}" method="post">
            <div class="modal-footer">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="goodsTypeId" value="" id="delete_goodsType">
                <a data-dismiss="modal" style="cursor:pointer" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
            </div>
        </form>
    </div>

    <script>
        $('._delete').click(function () {
            $('#delete_goodsType').val($(this).val());
        });
        $('#modal_add').on('hidden', function () {
            $('#form_add')[0].reset();
        });
        $('._edit').click(function () {
            $('#goodsTypeId_edit').val($(this).val());
            $('#goodsTypeName_edit').val($('#goodsTypeName_' + $(this).val()).text());
            $('#goodsTypePrice_edit').val(($('#goodsTypePrice_' + $(this).val()).text()).replace(/,/g,''));

            // $('#description_edit').val($('#description_' + $(this).val()).text());
            // $('#numberOfSeats_edit').val($('#numberOfSeats_' + $(this).val()).text());
        })
    </script>
    <!-- End Content -->

@endsection