@extends('cpanel.template.layout')
@section('title', 'ship')
@section('content')
    <div id="content" >

        <div class="row-fluid bg_light m_top_10">
            <div class="widget widget-4 bg_light">
                <div class="widget-body">
                            @include('cpanel.Ship.show.step-1-ship')
                            @include('cpanel.Ship.show.step-2-ship')
                            @include('cpanel.Ship.show.step-3-ship')

                </div>
            </div>
        </div>

</div>
 <script>

        $(document).ready(function(){
       $('#step2').hide();
       $('#step1').show();
       $('#step3').hide();
        window.scrollBy(0,0);
       $('[next-step]').click(function () {
            bienso = $('td.choose').data('bienso') ? $('td.choose').data('bienso') :'';
             step=$(this).attr("next-step");
            if(step==3&&bienso===""){
                alert("vui lòng chọn xe");
            }else if(step==2 && $('.ticketId:checked').length == 0){
                alert("vui lòng chọn hàng");
            }else{
                $('#step2').hide();
                $('#step1').hide();
                $('#step3').hide();
                $("#step"+step).show();

            }
        });
            window.scrollBy(0,0);
       $('body').on('click','.checkok', function(e){
           if($(e.currentTarget).is('.checkok')) {
               var tuyen = $('#chontuyen option:selected').text();
               var html = '';
               var listTicket =[];
               var tripId=sessionStorage.getItem("trip");
               var bienso= sessionStorage.getItem("bienso");
               var thoigianchay= sessionStorage.getItem("starttime");

               $('.ticketId:checked').each(function () {
                   html += '<tr>';
                   html += '<td>' + $(this).val() + '</td>';
                   html += '<td>' + $(this).data('ticketname') + '</td>';
                   html += '<td>' + $(this).data('phone') + '</td>';
                   html += '<td>' + $(this).data('fullnamereceiver') + '</td>';
                   html += '<td>' + tuyen + '</td>';
                   html += '<td>' + bienso + '</td>';
                   html += '<td>' + thoigianchay + '</td>';
                   html += '</tr>';
                   listTicket.push($(this).data('ticketid'));
//                 listTicket += '<input type="hidden" name="listTicket[]" value="' + $(this).val() + '">';
               });
               $('#ship').html(html);
               $('#listTicketIds').val(listTicket);
               $('#tripId').val(tripId);
               $('#getOffPointId').val($('#cbb_OffPoint').val());
               $('#getInPointId').val($('#cbb_InPoint').val());
           }
        });

    });
    var json = <?= json_encode($listShip); ?>;
    $("#shipall").change(function(){

    var status = this.checked;
    if(status){
        $('.ticketId').prop("checked", true);
        return;
    }
    else
        $('.ticketId').prop("checked", false);
        return;
    });
</script>
@endsection
