
<div id="step2">
    <style>
        #trip_body_tb tr {
            cursor: pointer;
        }
    </style>
    <div id="content" style="width:100% !important;">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Chọn tuyến xe và xe (2/3)</h3> <a
                            href="{{action('TicketController@printTicket',['ticketId'=>request('ticketId')])}}"
                            id="printTicket"></a></div>
            </div>
        </div>
        <div class="khung_lich_ve">
            <div class="row-fluid">
                <div class="pull-left span5 date_picker">
                    <div class="widget widget-4">
                        <div class="widget-head">
                            <h4>Tuyến</h4>
                        </div>
                        <div class="widget-body">
                            <div class="row-fluid">
                                <select class="chontuyen" name="routeId" id="chontuyen" >
                                    @foreach($listRoute as $route)
                                        <option data-startPoint="{{$route['listPoint'][0]['pointId']}}"
                                                data-endPoint="{{last($route['listPoint'])['pointId']}}" 
                                                data-name="{{$route['routeName']}}" 
                                                {{request('routeId')==$route['routeId']?'selected':''}}
                                                value="{{$route['routeId']}}">{{$route['routeName']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row-fluid">
                                <div class="span5">
                                    <select id="cbb_InPoint" class="span12 startPoint">
                                    </select>
                                </div>
                                <div class="span2">
                                    <button disabled type="button" class="span12 btn btn-flat"><i
                                                class="icon-arrow-right"></i></button>
                                </div>
                                <div class="span5">
                                    <select id="cbb_OffPoint" class="span12 endPoint">
                                    </select>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="control-group">
                                    <div class="controls">
                                        <div id="datepicker-inline"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pull-left span7 date_picker" style="margin-top: 50px">
                    <div class="widget widget-4 bg_light" style="padding-bottom: 30px;">
                        <div class="widget-body" style="max-height: 360px;overflow-y: auto">
                            <h4>Chọn xe</h4>
                            <table class="table table-hover-ticket table-vertical-center table-fixed">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">Giờ đi</th>
                                    <th style="width: 35%;" class="center">Tài xế</th>
                                    <th style="width: 10%;" >Lấp đầy</th>
                                    <th style="width: 20%; ">Biển số</th>
                                    <th style="width: 30%; " class="center"> Phụ xe</th>
                                </tr>
                                </thead>
                                <tbody id="trip_body_tb">

                                </tbody>
                            </table>
                        </div>
                        <div class="center ghichu_datghe" style="margin-top: 5px;">
                            <div class="gc_ghe ketthuc"></div>
                            <div class="title_ghichu_datghe">Kết thúc</div>
                            <div class="gc_ghe dangchay"></div>
                            <div class="title_ghichu_datghe">Đang chạy</div>
                            <div class="gc_ghe chuanbi"></div>
                            <div class="title_ghichu_datghe">Chuẩn bị chạy</div>
                            <div class="gc_ghe chuachay"></div>
                            <div class="title_ghichu_datghe">Chưa chạy</div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="panel-sell">
                <div id="loading">
                    {{--<img src="/public/images/loading/giphy-downsized.gif" alt="">--}}
                </div>
                <div class="row-fluid bg_light m_top_10 border_bottom">
                    <div class="span9 offset3">
                        <div class="widget widget-2 widget-tabs widget-tabs-2 no_border p_bottom_0">
                        </div>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="tab-content">
                        <div class="row-fluid m_top_15">
                            <button type="button" next-step="3" class="span4 btn btn-warning btn-flat checkok next-check">
                                TIẾP TỤC
                            </button>
                             <a href="#" next-step="1" class="span3 btn btn-default btn-flat no_border bg_light">QUAY LẠI</a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- End Content -->


    <script src="/public/javascript/jquery-dateFormat-master/dist/jquery-dateFormat.min.js"
            type="text/javascript"></script>

    <script src="/public/javascript/jquery.printPage.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/public/javascript/jquery.datepicker.lunar/jquery.datepicker.lunar.css">

    <script src="/public/javascript/jquery.datepicker.lunar/jquery.datepicker.lunar.js" type="text/javascript"></script>

    <script>
        var
            tripSelected = {
                ticketPrice: 0,
                index: '{{request('index')}}',
                childrenTicketRatio: 0,
                tripId: '{{request('tripId')}}'
            },//biến toàn cục dùng để lưu thông tin chuyến khi đã chọn chuyến từ ds chuyến
            listTrip = [],//biến toàn cục dùng lưu ds chuyến khi đã chọn lịch
            datepickerBox = $("#datepicker-inline"),//lịch
            selectBoxRoute = $('#chontuyen'),//selectbox chọn tuyến
            defaultDate = '{{request('date')}}',//ngày mặc định nếu có param date
            urlSeachTrip = '{{action('TripController@search')}}',//url tìm lịch cho khách
            urlSeachTripById = '{{action('TripController@searchByTripId')}}',//url lấy thông tin chuyến
            urlGetPriceTicketGoods = '{{action('TicketController@getPriceTicketGoods')}}',//url lấy giá vé gửi đồ
            urlCheckPromotionCode = '{{action('CouponController@check')}}',//url lấy thông tin khuyến mãi
            urlGetInfoCustomer = '{{action('CustomerController@getInfoByPhoneNumber')}}',//url lấy thông tin khách hàng qua sdt
            listRoute = {!! json_encode($listRoute,JSON_PRETTY_PRINT) !!};//thông tin tuyến dùng để cập nhật ds điểm dừng

        datepickerBox.datepicker({
            defaultDate: defaultDate,
            minDate: 0,
            dateFormat: "dd-mm-yy",
            onSelect: function () {
                reset();
                updateTripTable(function () {
                });
                saveFormDataLocal();
            }
            //showOtherMonths: true,
            //selectOtherMonths: true
        });
        $(document).ready(function () {
            /*
            * callback truyền vào updateListPoint() chỉ mang tính chất chung gian
            * callback để truyền vào updateTripTable() kích hoạt sau  gọi ajax thành công
            * */
            updateListPoint($(selectBoxRoute).val(), function () {
                makeURLByTrip('{{request('tripId')}}');
            });


            $('#loading').addClass('loading');

            selectBoxRoute.select2();

            selectBoxRoute.on('select2:select', function (e) {
                reset();
                updateListPoint($(this).val(), function () {
                });
            });

            /*
            * sự kiện khi chọn ghế để bán vé
            * nếu đang ở tab ds vé thì chuyển qua tab bán vé
            * */
            $('body').on('click', '#DanhSachVe div.ghetrong,#DanhSachVe div.giuongnam', function () {
                $('.chonbanve a:first').tab('show');
            });

            /*
            *sự kiện khi click vào ghế đã đạt hoặc giữ chỗ sẽ hiện thông tin ghế
            * */
            $('body').on('click', 'div.ghedadat,div.ghegiucho', function () {
                var ticketId = $(this).attr('data-ticketId');
                showInfoSeat(ticketId, 'ticket');
            });

            /*
            * sự kiện khi thay đổi điểm lên hoặc điểm xuống
            * */
            $('#cbb_InPoint,#cbb_OffPoint').change(function () {
                updateTripTable(function () {
                    if (listTrip.length == 0) {
                        Message('Cảnh báo', 'Hiện tại tuyến này không bán vé!', '');
                        listSeat = [];
                        $('input:text').val('');
                        $('#tang1 .khungxe,#tang2 .khungxe').html('');
                        $('#listSeat,.InTime,.OffTime,.routeName').text('');
                        $('#lb_numberOfAdults').text('0');
                        $('#numberOfAdults,#numberOfChildren').val('0');
                        $('#ticketPrice,#totalPrice,#tongGiaVeGuiDo,#giaVeGuiDoGoc').text(moneyFormat(0));
                        $('#isMeal,#isInsurrance,#isPickUpHome').hide();
                    }
                });
                saveFormDataLocal();
                //$("html, body").animate({scrollTop: 0}, "slow");
            });

            /*
             * sự kiện nhập cân nặng, kích thước gửi đồ
             * */
            $('#txt_weight').on('keyup', function (e) {
                if ($(this).val() > 0) {
                    $('#tongGiaVeGuiDo').text('Đang cập nhật giá...');
                    $.get(urlGetPriceTicketGoods, {
                            'getInPointId': $('#getInPointId_good').val(),
                            'getOffPointId': $('#getOffPointId_good').val(),
                            'tripId': $('#txt_tripId').val(),
                            'scheduleId': $('#txt_scheduleId').val(),
                            'promotionId': $('#txt_promotionId').val(),
                            'weight': $('#txt_weight').val(),
                            'dimension': $('#txt_dimension').val()
                        },
                        function (data) {
                            $('#tongGiaVeGuiDo').text(moneyFormat(data.pricePayment));
                            $('#giaVeGuiDoGoc').text(moneyFormat(data.priceOrigin));
                            /*console.log(data);*/
                        }, "json");
                }
            });
            /*
            * sự kiện nhập mã khuyến mãi
            * */
            $('.btnCheckPromotionCode').click(function () {
                // $('#tongGiaVeGuiDo').text('Đang cập nhật giá...');
                var btn = this;

                if ($(btn).text() === 'X') {
                    $('.salePrice').val(0);
                    $('.promotionId').val('');
                    $('.promotionCode').val('');
                    $(btn).prev('input').removeAttr('readonly').val('').removeClass('span12').addClass('span9');
                    $(this).text('CHECK');
                    updatePrice();
                } else {
                    if ($(this).prev('input').val() != '') {
                        $(btn).html('<img src="/public/images/loading/loader_blue.gif" width="19px">');
                        $(btn).prop('disabled', true);
                        $.ajax({
                            'url': urlCheckPromotionCode,
                            'data': {
                                'scheduleId': $('#txt_scheduleId').val(),
                                'promotionCode': $(this).prev('input').val()
                            },
                            'dataType':'json',
                            'success': function (data) {
                                    var result = data.result;
                                    if (result.percent != undefined && result.price != undefined) {
                                        if (result.percent > 0) {
                                            $('.salePrice').val(result.percent);
                                            Message('Thông báo', 'Kiểm tra mã khuyến mãi thành công<br>Được giảm: ' + result.percent * 100 + '%', '');
                                            $(btn).prev('input').val(result.promotionCode + ' - Giảm ' + result.percent * 100 + '%')
                                                .removeClass('span9').addClass('span12');
                                        }

                                        if (result.price > 0) {
                                            $('.salePrice').val(result.price);
                                            Message('Thông báo', 'Kiểm tra mã khuyến mãi thành công<br>Được giảm: ' + moneyFormat(result.price), '');
                                            $(btn).prev('input').val(result.promotionCode + ' - Giảm ' + moneyFormat(result.price))
                                                .removeClass('span9').addClass('span12');
                                        }


                                        $('.promotionId').val(result.promotionId);
                                        $('.promotionCode').val(result.promotionCode);
                                        $(btn).prev('input').attr('readonly', 'readonly');
                                        $(btn).text('X');
                                        $(btn).prop('disabled', false);
                                    } else {
                                        Message('Thông báo', 'Mã khuyến mãi không hợp lệ', '');
                                        $(btn).text('CHECK');
                                        $(btn).prop('disabled', false);
                                    }
                                    updatePrice();
                                },
                            'error': function () {
                                Message('Thông báo', 'Lấy mã khuyến mãi thất bại! Vui lòng tải lại trang.', '');
                                $(btn).text('CHECK');
                                $(btn).prop('disabled', false);

                            }
                        });
                    }

                }


            });
            /*
            * khi chọn loại thanh toán là payoo
            * */
            $('.cbb_paymentType').change(function () {
                if ($(this).val() == 5) {
                    $('iframe').attr('src', 'https://www.payoo.vn/diem-giao-dich');
                    $('#Payoo').show();
                } else {
                    $('#Payoo').hide();
                }
            });
            /*
            *sự kiện khi chọn bao gồm ăn hoặc bao gồm bảo hiểm
            * */
            $('#cb_priceInsurrance,#cb_isMeal').change(function () {
                updatePrice();
            });

            $('#nguoi_nhan_tra_tien').change(function () {
                if ($(this).is(':checked')) {
                    $('#btnSellTicketGoods').val('HOÀN THÀNH')
                } else {
                    $('#btnSellTicketGoods').val('THANH TOÁN')
                }
            });
            /*xem chi tiết thông tin vé*/

            $('body').on('click', 'table.table > tbody > tr:not(.info)', function (e) {
                if ($(e.target).is('td:last-child, button')) {
                    e.preventDefault();
                    return;
                }
                $('.info').not($(this).next('tr.info')).hide();
                $(this).next('tr.info').toggle();
                if ($(this).attr('data-images') != undefined) {
                    $('img#anhsanpham').attr('src', $(this).attr('data-images'));
                }
            });
            /*
            * khi chọn chuyến
            * */
            $('body').on('click', '#trip_body_tb tr', function (e) {
                sessionStorage.setItem("starttime",$(this).data('starttime'));
                sessionStorage.setItem("bienso", $(this).data('bienso'));
                sessionStorage.setItem("trip",$(this).data('trip'));
                var _tripId = $(this).attr('id');
                $('#trip_body_tb tr').removeClass('active');
                $(this).addClass('active');
                updateTripInfo(_tripId);

                //$("html, body").animate({scrollTop: $(window).height()}, "slow");
                makeURLByTrip(_tripId)

            });
            /*Preview ảnh*/

            $("#selectedFile").change(function () {
                UploadImage();
            });

            /*$('button:submit:not(#btnUpdateStatusTicket)').click(function (e) {
                if ($('.ghedangchon').length <= 0) {
                    e.preventDefault();
                    Message('Cảnh báo', 'Vui lòng chọn chỗ ngồi', '');
                }
            });*/


        });

        function updateListPoint(routeId, callback) {
            var route = $.grep(listRoute, function (item) {
                return item.routeId == routeId;
            });
            html_listInPoint = '';
            html_listOffPoint = '';
            $(route[0].listPoint).each(function (key, item) {
                if (key == 0 || key == route[0].listPoint.length - 1) {
                    if (key == 0) {
                        html_listInPoint += '<option selected value="' + item.pointId + '">' + item.pointName + '</option>';
                    } else {
                        html_listOffPoint += '<option selected value="' + item.pointId + '">' + item.pointName + '</option>';
                    }
                } else {
                    html_listInPoint += '<option value="' + item.pointId + '">' + item.pointName + '</option>';
                    html_listOffPoint += '<option value="' + item.pointId + '">' + item.pointName + '</option>';
                }
            });

            $('.startPoint').html(html_listInPoint);
            $('.endPoint').html(html_listOffPoint);

            /*
            * lấy dữ liệu startPointId và endPointId từ local
            * */
            var startPointId = localStorage.getItem("startPointId");
            var endPointId = localStorage.getItem("endPointId");
            var date = localStorage.getItem("date");
            if(startPointId !== null)
            {
                $('#cbb_InPoint').val(startPointId).change();
            }
            if(endPointId !== null)
            {
                $('#cbb_OffPoint').val(endPointId).change();
            }
            if(date !== null)
            {
                $(datepickerBox.datepicker("setDate", date));
            }

            updateTripTable(callback);
        }

        function updatePrice() {
            var totalPrice = getTotalPrice();
            $('#totalPrice').text(moneyFormat(totalPrice));
            $('#txt_totalPrice').val(totalPrice);

        }

        /*
        * hàm tính tổng giá tiền
        * */
        function getTotalPrice() {
            var totalTicket = $('.ghedangchon').length;
            var priceInsurrance = $('#cb_priceInsurrance').val() > 0 && $('#cb_priceInsurrance').is(':checked') ? totalTicket * $('#cb_priceInsurrance').val() : 0;
            var mealPrice = $('#cb_isMeal').val() > 0 && $('#cb_isMeal').is(':checked') ? totalTicket * $('#cb_isMeal').val() : 0;
            var priceAdults = $('#numberOfAdults').val() * tripSelected.ticketPrice;
            var priceChildren = $('#numberOfChildren').val() * (tripSelected.ticketPrice * tripSelected.childrenTicketRatio);
            var salePrice = $($('.salePrice')[0]).val();

            var totalextra = 0;

            $('.ghedangchon').each(function (index, ele) {
                var price = $(ele).attr('data-extraPrice');
                price = $.isNumeric(price) ? parseFloat(price) : 0;
                totalextra += price;
            });

            if (salePrice < 1) {
                salePrice *= (priceAdults + priceChildren + totalextra);
            }

            totalPrice = ((priceAdults + priceChildren) - salePrice) + mealPrice + priceInsurrance + totalextra;

            totalPrice = totalPrice > 0 ? totalPrice : 0;
            return totalPrice;
        }

        function updateTripTable(callback) {

            var routeId = $(selectBoxRoute).val();
            var date = $(datepickerBox.datepicker()).val();
            var startPointId = $('#cbb_InPoint').val();
            var endPointId = $('#cbb_OffPoint').val();
            $('.routeId').val(routeId);

            $('#trip_body_tb').html('<tr align="center"><td class="center" colspan="5" ><img src="/public/images/loading/loader_blue.gif" alt=""></td></tr>');

            $.get(urlSeachTrip,
                {
                    'routeId': routeId,
                    'date': date.replace('/', '-').replace('/', '-'),
                    'startPointId': startPointId,
                    'endPointId': endPointId
                },
                function (data) {
//                    listTrip = $.grep(data, function (trip) {
//                        return trip.getOffTime > Date.now();
//                    });

                    listTrip = data;
                    $('#trip_body_tb').html('');

                    // check chuan bi
                    var chuanbi = false;
                    listTrip.forEach(function (trip) {
                        var totalSeat = [], seatBooked = [];
                        if (trip.seatMap != null) {
                            totalSeat = $.grep(trip.seatMap.seatList, function (seat) {
                                return seat.seatType == 3 || seat.seatType == 4;
                            });
                            seatBooked = $.grep(totalSeat, function (seat) {
                                return seat.seatStatus == 3 || (seat.seatStatus == 2 && seat.overTime > Date.now());
                            });
                        }
                        var html = '';
                        var status = '';

                        if(trip.startTime < Date.now() && trip.getOffTime > Date.now())
                        {
                            status = 'dangchay';
                        } else if(trip.getOffTime < Date.now()){
                            status = 'ketthuc';
                        }

                        if(trip.startTime > Date.now() && !chuanbi)
                        {
                            chuanbi =  true;
                            status = 'chuanbi';
                        }

                        var driverPhoneNumber = trip.driverPhoneNumber !== undefined ? ' - 0' + trip.driverPhoneNumber : '';
                        var assPhoneNumber = trip.assPhoneNumber !== undefined ? ' - 0' + trip.assPhoneNumber : '';
                        html += '<tr id="' + i + '" data-schedule="'+ trip.scheduleId +'" data-trip="' + trip.tripId +
                            '" data-bienso="'+trip.numberPlate +'" data-startTime="'+getFormattedDate(trip.getInTime, 'time') +
                            '" data-intime="'+trip.getInTime+'" data-offtime="'+trip.getOffTime+'" class="'+  status + '">';
                        html += '<td data-trip="' + trip.tripId +'" data-bienso="'+trip.numberPlate +'" data-startTime="'+getFormattedDate(trip.getInTime, 'time')+'"><strong>' + getFormattedDate(trip.getInTime, 'time') + '</strong></td>';
                        html += '<td data-trip="' + trip.tripId +'"class="center"  data-bienso="'+trip.numberPlate +'" data-startTime="'+getFormattedDate(trip.getInTime, 'time')+'">' + (trip.driverName || '') + driverPhoneNumber + '</td>';
                        html += '<td  data-trip="' + trip.tripId +'"data-bienso="'+trip.numberPlate +'" data-startTime="'+getFormattedDate(trip.getInTime, 'time')+'">' + $(seatBooked).length + '/' + $(totalSeat).length + '</td>';
                        html += '<td data-trip="' + trip.tripId +'" data-bienso="'+trip.numberPlate +'" data-startTime="'+getFormattedDate(trip.getInTime, 'time')+'"><span class="bienso_xe">' + trip.numberPlate + '</span></td>';
                        html += '<td data-trip="' + trip.tripId +'" class="center"  data-bienso="'+trip.numberPlate +'" data-startTime="'+getFormattedDate(trip.getInTime, 'time')+'" >' + (trip.assName || '') + assPhoneNumber + '</td>';
                        html += '</tr>';
                        $('#trip_body_tb').append(html);
                    });
                    if (listTrip.length !== 0) {
                        /*nếu đã chọn chuyến rồi sẽ tự động cập nhật lại thông tin chuyến*/
                        if (tripSelected.tripId != '') {
                            updateTripInfo(tripSelected.tripId, tripSelected.index);
                            updatePrice();
                        }
                        /*if (listTrip.length >= 10) {
                            $("html, body").animate({scrollTop: 150}, "slow");
                        }*/
                    } else {
                        $('#trip_body_tb').html('<tr align="center"><td class="center" colspan="5" >Không có dữ liệu</td></tr>');
                    }
                    callback();
                }, "json")
        }

        function updateTripInfo(_tripId,id) {
//
//            $('#trip_body_tb tr').removeClass('active');
//            $($('#trip_body_tb tr#' + id)[0]).addClass('active');
//            $('.index').val(id);
          $('#trip_body_tb tr > td').addClass('choose');
         // ($('#trip_body_tb tr#' + _tripId)[0]).addClass('active');


            var TripData = $.grep(listTrip, function (trip) {
                return trip.tripId == _tripId;
            })[0];

            reset();
            loadSeatMap(TripData.seatMap);
            tripSelected.tripId = _tripId;
            tripSelected.ticketPrice = TripData.ticketPrice;
            tripSelected.childrenTicketRatio = TripData.childrenTicketRatio;

            $('.getInTimePlan').val(TripData.startTime);
            $('.getOffTimePlan').val(TripData.getOffTime);
            $('.tripId').val(TripData.tripId);
            $('.scheduleId').val(TripData.scheduleId);
            $('.startDate').val(TripData.startTime);
            $('#cb_priceInsurrance').val(TripData.priceInsurrance);
            $('#cb_isMeal').val(TripData.mealPrice);

            $('#txtGetInPointId').val(TripData.getInPointId);
            $('#txtGetOffPointId').val(TripData.getOffPointId);
            $('#txtGetInPointName').text(TripData.getInPointName);
            $('#txtGetOffPointName').text(TripData.getOffPointName);

            $('#ticketPrice').text(moneyFormat(TripData.ticketPrice));
            $('#mealPrice').text(moneyFormat(TripData.mealPrice));

            if (TripData.mealPrice == -1) {
                $('#isMeal').hide();
            } else {
                $('#isMeal').show();
            }

            $('#priceInsurrance').text(moneyFormat(TripData.priceInsurrance));

            if (TripData.priceInsurrance <= 0) {
                $('#isInsurrance').hide();
            } else {
                $('#isInsurrance').show();
            }

            $('.routeName').text($('#chontuyen option:selected').text());

            $('.InTime').text(getFormattedDate(TripData.getInTime, 'date'));
            $('.OffTime').text(getFormattedDate(TripData.getOffTime, 'date'));
//            if (Date.now() > TripData.getInTime) {
//                Message('Cảnh báo', 'Xe đã khởi hành qua tuyến này, không thể bán vé!', '');
//                reset();
//            }

            updateTableUserTicket(_tripId);
            $('#loading').removeClass('loading');
        }

        /*
        * hàm lấy thông tin vé cho bảng danh sách vé
        * */
        function updateTableUserTicket(_tripId) {
            $('#DanhSachVeGuiDo tbody').html('<tr><td class="center" colspan="6">Đang tải danh sách...</td></tr>');
            $('#DanhSachVeHanhKhach tbody').html('<tr><td class="center" colspan="6">Đang tải danh sách...</td></tr>');
            $.get(urlSeachTripById, {'tripId': _tripId},
                function (datauser) {console.log(datauser);
                    if (datauser.listGuestPerPoint == undefined || datauser.listGuestPerPoint.length == 0) {
                        $('#DanhSachVeGuiDo tbody').html('<tr><td class="center" colspan="6">Hiện không có dữ liệu</td></tr>');
                        $('#DanhSachVeHanhKhach tbody').html('<tr><td class="center" colspan="6">Hiện không có dữ liệu</td></tr>');
                    } else {
                        html_listUser = '';
                        html_listGoods = '';
                        for (_key in datauser.listGuestPerPoint) {

                            $(datauser.listGuestPerPoint[_key]).each(function (key, _user) {
                                if (_user.ticketType == 1) {
                                    var overTime = _user.overTime != undefined ? _user.overTime : 0;

                                    html_listUser += '<tr><td>' + _user.listSeatId.length + '</td>';
                                    html_listUser += '<td>' + _user.ticketCode + '</td>';
                                    html_listUser += '<td>' + _user.fullName + '</td>';
                                    html_listUser += '<td>' + _user.phoneNumber + '</td>';
                                    html_listUser += '<td>' + checkStatusSeatAndTicket(_user.ticketStatus, overTime).TicketStatusHTML + '</td>';
                                    html_listUser += '<td> <a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i><ul class="onclick-menu-content">';
                                    html_listUser += '<li> <button type="button" onclick="showInfo(this)">Chi tiết</button> </li>';
                                    if (checkStatusSeatAndTicket(_user.ticketStatus, overTime).seatStatus != 'ghetrong') {
                                        html_listUser += '<li> <button type="button" onclick="printTicket(\'' + _user.ticketId + '\')">In vé</button> </li>';
                                        html_listUser += '<li> <button value="' + _user.ticketId + '" id="editTicket">Sửa vé</button> </li>';
                                        html_listUser += '<li> <button value="' + _user.ticketId + '" id="cancelTicket">Hủy vé</button> </li>';
                                        if (checkStatusSeatAndTicket(_user.ticketStatus, overTime).seatStatus == 'ghegiucho')
                                            html_listUser += '<li> <button value="' + _user.ticketId + '" id="orderTicket">Thanh toán</button> </li>';
                                    }

                                    html_listUser += '</ul></a></td></tr>';
                                    /*chi tiết*/
                                    html_listUser += '<tr id="' + _user.ticketId + '" class="info" style="display: none;"><td colspan="6"><table class="tttk" cellspacing="20" cellpadding="4"><tbody>';
                                    html_listUser += '<tr><td><b>TUYẾN</b></td><td colspan="3">' + datauser.routeName + '</td></tr>';


                                    html_listUser += '<tr><td><b>ĐIỂM ĐÓN</b></td> <td  colspan="3">' + _user.getInPoint.pointName + ' - Đón tại: ' + _user.pickUpAddress + '</td>';

                                    html_listUser += '<tr><td><b>ĐIỂM TRẢ</b></td> <td colspan="3">' + _user.getOffPoint.pointName + ' - Trả tại: ' + _user.dropOffAddress + '</td></tr>';

                                    
                                    html_listUser += '<tr><td><b>THỜI GIAN ĐI - ĐẾN</b></td><td colspan="3">' + getFormattedDate(_user.getInTimePlan, 'date') + ' - ' + getFormattedDate(_user.getInTimePlan + datauser.runTimeReality, 'date') + '</td></tr>';
                                    html_listUser += ' <tr>' +
                                        '<td style="width:25%"><b>VÉ NGƯỜI LỚN</b></td><td style="width:25%;padding-left:10px">' + _user.numberOfAdults + '</td>' +
                                        '<td style="width:25%;text-align:right"><b>VÉ TRẺ EM</b></td> <td style="width:25%;text-align:left">' + _user.numberOfChildren + '</td>' +
                                        '</tr>';
                                    html_listUser += '<tr><td><b>VỊ TRÍ</b></td><td colspan="3">' + _user.listSeatId.join('-') + '</td></tr>';

                                    html_listUser += '<tr>' +
                                        '<td><b>TRẠNG THÁI VÉ</b></td><td>' + checkStatusSeatAndTicket(_user.ticketStatus, overTime).TicketStatusMessage + '</td>' +
                                        '<td style="width:25%;text-align:right"><b>GIÁ VÉ</b></td><td>' + moneyFormat(_user.paymentTicketPrice) + '</td>' +
                                        '</tr>';
                                    html_listUser += '<tr> <td><b>ĐÃ BAO GỒM ĂN</b></td> <td colspan="3"></td></tr>';
                                    html_listUser += '<tr><td><b>MÃ KHUYẾN MÃI</b></td><td colspan="3">' + _user.promotionCode + '</td></tr>';
                                    html_listUser += '</tbody></table></td></tr>';
                                }


                                if (_user.ticketType == 3) {
                                    html_listGoods += '<tr data-images="' + _user.listImages[0] + '"><td>' + _user.ticketCode + '</td>';
                                    html_listGoods += '<td>' + _user.fullNameReceiver + '</td>';
                                    html_listGoods += '<td>' + _user.phoneNumberReceiver + '</td>';
                                    html_listGoods += '<td>' + _user.ticketName + '</td>';
                                    html_listGoods += '<td>' + _user.fullName + '</td>';
                                    html_listGoods += '<td> <a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i><ul class="onclick-menu-content">';
                                    html_listGoods += '<li> <button type="button" onclick="showInfo(this)">Chi tiết</button> </li>';
                                    html_listGoods += '<li> <button value="' + _user.ticketId + '" id="cancelTicket">Hủy vé</button> </li>';
                                    html_listGoods += '<li> <button value="' + _user.ticketId + '" id="orderTicket">Thanh toán</button> </li>';
                                    html_listGoods += '</ul></a></td></tr>';

                                    /*chi tiết*/
                                    html_listGoods += '<tr id="' + _user.ticketId + '" class="info" style="display: none;"><td colspan="6"><table class="tttk" cellspacing="20" cellpadding="4"><tbody>';
                                    html_listGoods += '<tr><td><b>TUYẾN</b></td><td colspan="3">' + datauser.routeName + '</td></tr>';
                                    html_listGoods += '<tr><td><b>ĐỊA CHỈ NHẬN HÀNG</b></td><td  colspan="3">' + _user.getInPoint.pointName + '</td></tr>';
                                    html_listGoods += '<tr><td><b>ĐỊA CHỈ TRẢ HÀNG</b></td> <td colspan="3">' + _user.getOffPoint.pointName + '</td></tr>';
                                    html_listGoods += '<tr><td><b>THỜI GIAN ĐI - ĐẾN</b></td><td colspan="3">' + getFormattedDate(_user.getInTimePlan, 'date') + ' - ' + getFormattedDate(_user.getInTimePlan + datauser.runTimeReality, 'date') + '</td></tr>';
                                    html_listGoods += '<tr>' +
                                        '<td style="width:25%"><b>TÊN NGƯỜI NHẬN</b></td><td style="width:25%;padding-left:10px">' + _user.fullNameReceiver + '</td>' +
                                        '<td style="width:25%;text-align:right"><b>SỐ ĐIỆN THOẠI NGƯỜI NHẬN</b></td> <td style="width:25%;text-align:left">' + _user.phoneNumberReceiver + '</td>' +
                                        '</tr>';
                                    html_listGoods += ' <tr>' +
                                        '<td style="width:25%"><b>TÊN NGƯỜI GỬI</b></td><td style="width:25%;padding-left:10px">' + _user.fullName + '</td>' +
                                        '<td style="width:25%;text-align:right"><b>SỐ ĐIỆN THOẠI NGƯỜI GỬI</b></td> <td style="width:25%;text-align:left">' + _user.phoneNumber + '</td>' +
                                        '</tr>';
                                    html_listGoods += ' <tr>' +
                                        '<td style="width:25%"><b>MÃ KHUYẾN MÃI</b></td><td style="width:25%;padding-left:10px">' + _user.promotionCode + '</td>' +
                                        '<td style="width:25%;text-align:right"><b>GIÁ VÉ</b></td> <td style="width:25%;text-align:left">' + moneyFormat(_user.paymentTicketPrice) + '</td>' +
                                        '</tr>';
                                    html_listGoods += ' <tr>' +
                                        '<td style="width:25%"><b>TRỌNG LƯỢNG SP</b></td><td style="width:25%;padding-left:10px">' + _user.weight + ' Kg</td>' +
                                        '<td style="width:25%;text-align:right"><b>KÍCH THƯỚC</b></td> <td style="width:25%;text-align:left">' + _user['dimension'] + '</td>' +
                                        '</tr>';
                                    html_listGoods += '</tbody></table></td></tr>';
                                }

                            });
                        }

                        $('#DanhSachVeGuiDo tbody').html(html_listGoods);
                        $('#DanhSachVeHanhKhach tbody').html(html_listUser);
                    }

                }, "json");
        }

        function makeURLByTrip(_tripId) {
            var params = {routeId: (selectBoxRoute).val(), date: $(datepickerBox.datepicker()).val(), tripId: _tripId};
            window.history.replaceState({}, document.title, window.location.href.split('?')[0] + '?' + $.param(params));
        }

        /*
        * hàm show thông tin vé ghế, gửi đồ
        * @param type = ticket: hiển thị tab vé hành khách,
        *        type = goods: hiên thị tab vé đồ
        * */
        function showInfoSeat(ticketId, type) {
            if (type == 'ticket') {
                /*nếu chọn type là vé hành khách*/
                $('.chonbanve a:last').tab('show');
                $('.chonloaidanhsach a:first').tab('show');
                $('.info').hide();
                $('body').find('.info#' + ticketId).show();
            } else {
                /*nếu type là vé gửi đồ  = 'goods'*/
                $('.chonbanve a:last').tab('show');
                $('.chonloaidanhsach a:last').tab('show');
                $('.info').hide();
                $('body').find('.info#' + ticketId).show();
            }
        }

        /*
        * hàm reset khi chọn lại
        *
        * */
        function reset() {
            tripSelected.tripId = '';
            listSeat = [];
            $('input:text').val('');
            $('#tang1 .khungxe,#tang2 .khungxe').html('');
            $('#listSeat,.InTime,.OffTime,.routeName').text('');
            $('#lb_numberOfAdults').text('0');
            $('#numberOfAdults,#numberOfChildren').val('0');
            $('#totalPrice,#tongGiaVeGuiDo,#giaVeGuiDoGoc').text(moneyFormat(0));

            $('#loading').addClass('loading');
        }

        /*
        * hàm tạo sơ đồ ghế
        * */
        function loadSeatMap(result) {
            $('#tang1 .khungxe,#tang2 .khungxe,#ds_tang1 .khungxe,#ds_tang2 .khungxe').html('');
            var html_floor1 = '',
                html_floor2 = '',
                Row = result['numberOfRows'],
                Column = result['numberOfColumns'],
                cellNull = '<div class="ghe loidi-noboder"></div>';
            for (hang = 1; hang <= Row; hang++) {
                for (cot = 1; cot <= Column; cot++) {
                    seat_floor1 = '<div class="ghe loidi"></div>';
                    seat_floor2 = '<div class="ghe loidi"></div>';
                    $.each(result['seatList'], function (key, item) {
                        if (item['column'] == cot && item['row'] == hang) {
                            if (item['floor'] == 1) {
                                /*Tầng 1*/
                                seat_floor1 = generateSeatHTML(item);
                            } else {
                                /*Tầng 2*/
                                seat_floor2 = generateSeatHTML(item);
                            }
                        }
                    });
                    html_floor1 += seat_floor1;
                    html_floor2 += seat_floor2;
                }
                //xử lý tràn ghế trong div html
                if (Column < 7) {
                    for (i = 0; i < 7 - Column; i++) {
                        html_floor1 += cellNull;
                        html_floor2 += cellNull;
                    }
                }
            }

            $('#tang1 .khungxe,#ds_tang1 .khungxe').html(html_floor1);

            if (result['numberOfFloors'] > 1) {
                $('#tang2 .khungxe,#ds_tang2 .khungxe').html(html_floor2);
            }


        }

        function generateSeatHTML(seat) {
            /*
                DOOR(1),//cửa
                DRIVER_SEAT(2), //ghế cho tài xế
                NORMAL_SEAT(3),//ghế thường
                BED_SEAT(4),//ghế giường nằm
                WC(5),//nhà vê sinh
                AST_SEAT(6), //ghế cho phụ xe
            */
            switch (seat['seatType']) {
                case 1:
                    seatType = 'cuaxe';
                    break;
                case 2:
                    seatType = 'taixe';
                    break;
                case 3:
                case 4:
                    seatType = checkStatusSeatAndTicket(seat.seatStatus, seat.overTime).seatStatus;
                    break;
                case 5:
                    seatType = 'nhavesinh';
                    break;
                case 6:
                    seatType = 'ghephuxe';
                    break;
                default:
                    seatType = 'loidi';
                    break;
            }
            var seatHTML = '<div ';
            seatHTML += ' data-ticketId="' + seat['listTicketId'][0] + '"';
            seatHTML += ' data-extraPrice="' + seat['extraPrice'] + '"';
            seatHTML += ' class="ghe ' + seatType + '">';
            seatHTML += seat['seatId'];
            seatHTML += '</div>';
            return seatHTML;
        }

        function checkStatusSeatAndTicket(Status, overTime) {

            /*
            *   INVALID(-2),
                CANCELED(0),
                EMPTY(1), // rỗng. hết hạn giữ chỗ sẽ chuyển về trạng thái này
                BOOKED(2), // đã giữ - (SMS khoảng cách <=10 km)
                BOUGHT(3), // đã thanh toán - (SMS khoảng cách <=10 km)
                ON_THE_TRIP(4), // đã lên xe
                COMPLETED(5), // đã hoàn thành
                OVERTIME(6),// quá giờ giữ chỗ
                BOOKED_ADMIN(7) // siêu phụ xe đặt chỗ
            * */
            var result = {seatStatus: '', TicketStatusHTML: '', TicketStatusMessage: ''};

            switch (Status) {
                case 0:
                case 1:
                case 5:
                case 6:
                    result.seatStatus = 'ghetrong';
                    result.TicketStatusHTML = 'Vé trống';
                    result.TicketStatusMessage = 'Vé trống';
                    break;
                case 3:
                case 4:
                    result.seatStatus = 'ghedadat';
                    result.TicketStatusHTML = '<div class="gc_ghe gc_dadat"></div>';
                    result.TicketStatusMessage = 'Đã thanh toán';

                    break;
                case 2:
                    if (overTime > Date.now()) {
                        result.seatStatus = 'ghegiucho';
                        result.TicketStatusHTML = '<div class="gc_ghe gc_giucho"></div>';
                        result.TicketStatusMessage = 'Giữ chỗ';
                    } else {
                        result.seatStatus = 'ghetrong';
                        result.TicketStatusHTML = '<div class="gc_ghe gc_hethan"></div>';
                        result.TicketStatusMessage = 'Hết hạn giữ chỗ';
                    }
                    break;
                case 7:
                    result.seatStatus = 'ghegiucho';
                    result.TicketStatusHTML = '<div class="gc_ghe gc_giucho"></div>';
                    result.TicketStatusMessage = 'Giữ chỗ';
                    break;
                default:
                    result.seatStatus = 'ghetrong';
                    break;
            }
            return result;
        }


        function printTicket(ticketId) {
            window.open("/cpanel/ticket/print/" + ticketId, null,
                "height=300,width=600,status=yes,toolbar=no,menubar=no,location=no");
        }

        function minus() {
            var data = parseInt($("#numberOfChildren").val()),
                numberOfAdults = parseInt($('#numberOfAdults').val());
            if (data != 0) {
                data--;
                if (tripSelected.childrenTicketRatio != 0) {
                    numberOfAdults++;
                }
                $("#numberOfChildren").val(data);
                $('#lb_numberOfAdults').text(numberOfAdults);
                $('#numberOfAdults').val(numberOfAdults);
                updatePrice();
            } else {
                $("#numberOfChildren").val(0);
            }
        }

        function plus() {
            var data = parseInt($("#numberOfChildren").val()),
                numberOfAdults = parseInt($('#numberOfAdults').val());

            if (data < $('.ghedangchon').length) {
                data++;
                if (tripSelected.childrenTicketRatio != 0) {
                    numberOfAdults--;
                }

                $('#lb_numberOfAdults').text(numberOfAdults);
                $("#numberOfChildren").val(data);
                $('#numberOfAdults').val(numberOfAdults);
                updatePrice();
            } else {
                Message('Cảnh báo', 'Vé trẻ em không thể vượt quá vé người lớn', '');
            }
        }


        function UploadImage() {
            var form = new FormData($("#frmSellTicketGoods")[0]);
            $('#blah').attr('src', '/public/images/loading/loader_blue.gif');
            $.ajax({
                type: 'POST',
                url: '/cpanel/system/upload-image',
                data: form,
                dataType: 'json',
                processData: false,
                contentType: false,
                cache: false
            }).done(function (data) {
                $('#blah').attr('src', data.url);
                $('#listImages').val(data.url);
            });
        }
        
        function saveFormDataLocal() {
            if (typeof(Storage) !== "undefined") {
                localStorage.setItem("routeId", $(selectBoxRoute).val());
                localStorage.setItem("startPointId", $('#cbb_InPoint').val());
                localStorage.setItem("endPointId", $('#cbb_OffPoint').val());
                localStorage.setItem("date", $(datepickerBox.datepicker()).val());

            } else {
                alert("Trình duyệt không được hỗ trợ! Hãy cập nhật bản mới nhất.");
            }
        }


    </script>