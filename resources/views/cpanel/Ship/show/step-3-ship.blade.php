<div class="col-md-5" id="step3">
	<h4>Danh sách hàng được chọn lên xe (3/3)</h4>
	<div class="panel-heading" style="font-size: 18px"></div>
	<div class="panel-body">
		<table class="table table-hover ">
			<tr>
				<th style="width: 10px">Mã vé</th>
				<th style="width: 80px;">Tên Hàng</th>
				<th style="width: 100px">SĐT nhận</th>
				<th style="width: 100px;">Tên người nhận</th>
				<th style="width: 100px;">Tuyến xe</th>
				<th style="width: 100px;">Biển số</th>
				<th style="width: 100px;">Thời gian chạy</th>
			</tr>
			<tbody id="ship">
				
			</tbody>
		</table>
	</div>
	<div class="row-fluid m_top_15">
		<form action="{{ action('ShipController@postGoodInTrip') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="listTicketIds" id="listTicketIds" value="">
			<input type="hidden" name="tripId" id="tripId" value="">
			<input type="hidden" name="getInPointId" value="" id="getInPointId">
			<input type="hidden" name="getOffPointId" id="getOffPointId">
			<button type="submit" id="btn_conf" class="span4 btn btn-warning btn-flat"> XÁC NHẬN</button>
		</form>
        <a href="#" next-step="2" class="span3 btn btn-default btn-flat no_border bg_light">HỦY BỎ</a> 
    </div>
</div>
<script>


</script>