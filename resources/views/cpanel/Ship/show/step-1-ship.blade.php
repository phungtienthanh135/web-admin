<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
<style>
    .table-hover tbody tr:hover td{
        color: #000;
    }
    .ticket0{
        background: #5C5C5C;
        color: #000;
    }
    .ticket2{
        background: #0B90C4;
        color: #fff;
    }
    .ticket4{
        background: #00aa00;
        color: #000;
    }
    .ticket5{
        background: #0d4c99;
        color: #fff;
    }
    .thisok {
        background: #009688;
        color: #fff;
    }

    .thisok:hover {
        color: #000;
    }

    .done {
        background: forestgreen;
        color: #fff;
    }

    .done:hover {
        color: #000;
    }
.labelCenter{
    text-align: center;
    padding: 5px 0 0 0;
}
    .widthdiv{
        width: 15%;
        height: 100%;
    }
</style>
<div id="step1">
    <div class="col-md-5">
        <div class="panel-body">
            <div class="heading_top">
                <div class="row-fluid">
                    <div class="pull-left span8"><h3>Chọn hàng đưa lên xe</h3></div>
                </div>
            </div>
            <div class="khung_lich_ve">
                <div class="date_picker">
                    <div style="padding: 7px;" class="widget widget-4">
                        <div class="widget-head">
                            <h4>Tìm hàng (1/3)</h4>
                        </div>
                    </div>
                    <?php //dev(request('routeId'))?>
                    <div class="innerLR">
                        <form id="timve" action="{{action('ShipController@show')}}">
                            <div class="row-fluid" style="height: 100px">
                                <div class="span3">
                                    <label class="span4 labelCenter">Tuyến xe</label>
                                    <select class="span8" name="routeId" id="txtMaGiaoDich" value="">
                                        <option value="">Tất cả</option>
                                        @foreach ($listRoute as $route)
                                            <option @php echo(request('routeId')===$route['routeId']? "selected" :""); @endphp value="{{$route['routeId']}}"  >
                                                {{$route['routeName']}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="span3">
                                    <label class="labelCenter span4" for="txtStartDate">Từ ngày</label>
                                    <div  class="input-append span8">
                                        <input class="span9" data-validation="required" autocomplete="off" type="text" name="startDate"
                                               id="txtStartDate"
                                               value="{{request('startDate')}}">
                                    </div>
                                </div>

                                <div class="span3" >
                                        <label class="labelCenter span4" for="txtEndDate">Đến ngày</label>
                                    <div class="input-append span8">
                                        <input class="span9" data-validation="required" autocomplete="off" type="text" name="endDate"
                                               id="txtEndDate"
                                               value="{{request('endDate')}}">
                                    </div>
                                </div>
                            </div>
                            <div  class="row-fluid" style="height: 100px">
                                <div class="span3"  style="float: left">
                                    <label class="span4 labelCenter">SĐT Nhận</label>
                                    <input class="span8" id="phoneNumberReceiver" name="phoneNumberReceiver"
                                           value="{{request('phoneNumberReceiver')}}">
                                </div>
                                {{--<div class="span3" style="width: 100%"></div>--}}
                                <div class="span3" style="float: left">
                                    <label class="span4 labelCenter">Mã vé</label>
                                    <input class="span8" name="ticketId" id="ticketId" value="{{request('ticketId')}}">
                                </div>
                                <div class="span3">
                                    <label class="span4 labelCenter" for="txtTrangThai">Trạng thái</label>
                                    <select name="status" class="span8">
                                        <option value="">Tất cả</option>
                                        @foreach($ticketStatus as $key => $value)
                                            <option value="{{ $key }}" {{(request("status")==$key ? "selected" : "")}}>{{ $value }}</option>

                                        @endforeach
                                    </select>
                                </div>
                                <div class="span3"  style="float: right">
                                    <label class="span4 labelCenter"></label>
                                    <button class="span8 labelCenter" style="background-color: #084388;    border: none;color: white"
                                            class="btn btn-info hidden-phone"
                                            id="search">TÌM KIẾM
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <table id="table" class="table table-hover" style="display: block;width:100%;">
                <thead>
                <tr style="background-color: #aebbe0">
                    <th style="width: 10px;vertical-align: unset;">
                        <input type="checkbox" name="ticketId[]" id="shipall" value="{{@$ship['ticketId']}}">
                        <label class="f_left" for="shipall"></label>
                    </th>
                    <th style="width: 7%;text-align: center;">Tuyến</th>
                    <th style="width: 8%;text-align: center;">Mã Vé</th>
                    <th style="width: 12%;text-align: center;">Giờ đi</th>
                    <th style="width: 9%;text-align: center;">Từ Ngày</th>
                    <th style="width: 9%;text-align: center;">Đến Ngày</th>
                    <th style="width: 6%;text-align: center;">SĐT nhận</th>
                    <th style="width: 15%;text-align: center;">Địa chỉ nhận</th>
                    <th style="width: 12%;text-align: center;">Tên người nhận</th>
                    <th style="width: 12%;text-align: center;">Tên Hàng</th>
                    <th style="width: 12%;text-align: center;">Phụ Xe</th>
                    <th style="width: 6%;text-align: center;">Tùy chọn</th>

                </tr>

                </thead>
                <tbody class="listship">
                @foreach($listShip as $ship)

                    <tr class="ticket<?= $ship['ticketStatus']; ?>  item">
                        <td>
                            @php
                            $ship['startTime']-=7*60*60*1000;
                            $ship['getInTimePlan']-=7*60*60*1000;
                            $ship['getOffTimePlan']-=7*60*60*1000;
                            @endphp
                            @if($ship['ticketStatus'] !== 5 && $ship['ticketStatus'] !== 0)
                            <input type="checkbox" name="ship" value="{{$ship['ticketId']}}" id="{{$ship['ticketId']}}"
                                   data-routename="{{@$ship['routeName']}}"
                                   data-intime="@dateTime($ship['getInTimePlan'])"
                                   data-offtime="{{$ship['getOffTimePlan']}}" data-ticketname="{{$ship['ticketName']}}"
                                   data-ticketid="{{$ship['ticketId']}}"
                                   data-fullname="{{$ship['fullName']}}" data-phone="{{$ship['phoneNumberReceiver']}}"
                                   data-fullNameReceiver="{{$ship['fullNameReceiver']}}"
                                   data-dropoff="{{@$ship['dropOffAddress']}}" class="ticketId">
                            <label class="f_left" for="{{$ship['ticketId']}}"></label>
                            @endif
                        </td>
                        <td style="width: 7%;">{{@$ship['routeName']}}</td>
                        <td style="width: 8%;text-align: center;">{{$ship['ticketId']}}</td>
                        <td style="width: 4%;text-align: center;">@dateTime($ship['startTime'])</td>
                        <td style="width: 9%;text-align: center;">{{date('d/m/Y',$ship['getInTimePlan']/1000)}}</td>
                        <td style="width: 9%;text-align: center;">{{date('d/m/Y',$ship['getOffTimePlan']/1000)}}</td>
                        <td style="width: 6%;">{{$ship['phoneNumberReceiver']}}</td>
                        <td style="width: 15%;text-align: center;">{{@$ship['pickUpAddress']}}</td>
                        <td style="width: 12%;text-align: center;">{{$ship['fullNameReceiver']}}</td>
                        <td style="width: 12%;text-align: center;">{{$ship['ticketName']}}</td>
                        <td style="width: 12%;text-align: center;"><?php if ($ship['listAss'] != null) {
                                echo $ship['listAss'][0]['fullName'];
                            } else {?> <strong style="color: #ff5151"><?php echo "Chưa có";} ?></strong></td>
                        <td style="width: 50px;text-align: center;">
                            <?php
                            if($ship['ticketStatus'] !== 5 && $ship['ticketStatus'] !== 0) {  ?>
                            <a class="glyphicon glyphycon-option-vertical onclick-menu" tabindex="0">
                                .:. <i></i>
                                <ul class="onclick-menu-content">
                                    <li>
                                        <button id="cancelTicket" data-target="#modal_cancel " data-toggle="modal"
                                                type="button"
                                                class="btn btn-flat " value="{{$ship['ticketId']}}">Hủy gửi đồ
                                        </button>
                                    </li>
                                    <li>
                                        <button class="returnGoods btn btn-flat" data-target="#modal_return "
                                                data-toggle="modal" type="button"
                                                value="{{$ship['ticketId']}}"><label
                                                    style="font-weight:bold ;font-size: 100%">Trả đồ</label></button>
                                    </li>
                                </ul>
                            </a>
                            <?php } ?>
                        </td>


                    </tr>

                    <tr class="hide">
                        <td colspan="9">
                            <table class="table tttk">
                                <tbody>
                                <tr>
                                    <td><b>ĐỊA CHỈ TRẢ HÀNG</b></td>
                                    <td colspan="3">{{@$ship['dropOffAddress']}}</td>
                                </tr>
                                <tr>
                                    <td><b>THỜI GIAN ĐI - ĐẾN</b></td>
                                    <td colspan="3">@dateTime($ship['getInTimePlan']) -
                                        @dateTime($ship['getOffTimePlan'])
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:25%"><b>TÊN NGƯỜI NHẬN</b></td>
                                    <td style="width:25%;padding-left:10px">{{$ship['fullNameReceiver']}}</td>
                                    <td style="width:25%;text-align:right"><b>SỐ ĐIỆN THOẠI NGƯỜI NHẬN</b></td>
                                    <td style="width:25%;text-align:left">{{$ship['phoneNumberReceiver']}}</td>
                                </tr>
                                <tr>
                                    <td style="width:25%"><b>TÊN NGƯỜI GỬI</b></td>
                                    <td style="width:25%;padding-left:10px">{{$ship['fullName']}}</td>
                                    <td style="width:25%;text-align:right"><b>SỐ ĐIỆN THOẠI NGƯỜI GỬI</b></td>
                                    <td style="width:25%;text-align:left">{{$ship['phoneNumber']}}</td>
                                </tr>
                                <tr>
                                    <td style="width:25%"><b>MÃ KHUYẾN MÃI</b></td>
                                    <td style="width:25%;padding-left:10px">{{$ship['promotionId']}}</td>
                                    <td style="width:25%;text-align:right"><b>GIÁ VÉ</b></td>
                                    <td style="width:25%;text-align:left">{{$ship['paymentTicketPrice']}} VNĐ</td>
                                </tr>

                                <tr>
                                    <td style="width:25%"><b>TRỌNG LƯỢNG SẢN PHẨM</b></td>
                                    <td style="width:25%;padding-left:10px">{{$ship['weight']}} Kg</td>
                                    <td style="width:25%;text-align:right"><b>KÍCH THƯỚC</b></td>
                                    <td style="width:25%;text-align:left">{{$ship['dimension']}}</td>
                                </tr>
                                <tr>
                                    <td style="width:25%"><b>THÔNG TIN TÀI XẾ VẬN CHUYỂN</b></td>
                                    <td style="width:25%;padding-left:10px">TÊN TÀI XẾ:
                                        <?php if ($ship['listDriver'] != null) {
                                            echo $ship['listDriver'][0]['fullName'];
                                        } else {?> <strong style="color: #ff5151"><?php echo "Chưa có";} ?></strong>
                                    </td>
                                    <td style="width:25%;text-align:right">

                                    </td>

                                    <td style="width:25%;text-align:left">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:25%"></td>
                                    <td style="width:25%;padding-left:10px">
                                        SDT: <?php if ($ship['listDriver'] != null) {
                                            echo $ship['listDriver'][0]['phoneNumber'];
                                        } else {?> <strong style="color: #ff5151"><?php echo "Chưa có";}?></strong></td>
                                    <td style="width:25%;text-align:right"></td>
                                    <td style="width:25%;text-align:left"></td>
                                </tr>
                                <tr>
                                    <td style="width:25%;text-align:right"><b>BIỂN SỐ XE:</b></td>
                                    <td style="width:25%;text-align:left">
                                        <?php if (isset($ship['numberPlate']) && $ship['numberPlate'] != "") {
                                            echo $ship['numberPlate'];
                                        } else {?> <strong style="color: #ff5151"><?php echo "Chưa có";} ?></strong>
                                    </td>
                                    <td style="width:25%"></td>
                                    <td style="width:25%;padding-left:10px"></td>
                                </tr>
                                <tr>
                                    <td style="width:25%;text-align:right"><b>PHỤ XE:</b></td>
                                    <td style="width:25%;text-align:left">
                                        <?php if ($ship['listAss'] != null) {
                                            echo $ship['listAss'][0]['fullName'];
                                        } else {?> <strong style="color: #ff5151"><?php echo "Chưa có";} ?></strong>
                                    </td>
                                    <td style="width:25%"></td>
                                    <td style="width:25%;padding-left:10px"></td>
                                </tr>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <!-- <tr id="{{$ship['ticketId']}}" style="display: none">
                            <td colspan="8">
                                <table class="tttk" cellspacing="20" cellpadding="4">
                                    <tbody>
                                        <tr>
                                            <td><b>TUYẾN</b></td>
                                            <td>AAAA</td>
                                            <td class="right"><b>VÉ NGƯỜI LỚN</b></td>
                                            <td class="left">AAAA</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr> -->
                @endforeach
                </tbody>

            </table>
            <div class="row-fluid m_top_15">
                <button type="button" next-step="2"
                        class="span4 btn btn-warning btn-flat">
                    TIẾP TỤC
                </button>
            </div>
        </div>
        <div style="padding-top: 50px; float:right" class="span3">
            <div class="gc_ghe ticket5"></div>
            <div class="">Đã Trả</div>
            <div class="gc_ghe ticket4"></div>
            <div class="">Đã có mã chuyến</div>
            <div class="gc_ghe ticket2"></div>
            <div class="">Chưa có mã chuyến</div>
            <div class="gc_ghe  ticket0"></div>
            <div class="">Đã hủy</div>
        </div>
    </div>
</div>

<div class="modal modal-custom hide fade" id="cancel_or_return"  style="width: 400px; margin-left: -10%;margin-top: 10%;">
    <form action="{{action('ShipController@updateStatusTicket')}}" method="post">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="title-update-ticket">CẬP NHẬT THÔNG TIN VÉ</h3>
        </div>
        <div class="modal-body center">
            <p id="lb_message"></p>
            {{--<label  name="fullname"  id="labelName">Khách hàng :{{@$ship['fullName']}}</label>--}}
            {{--<label  name="dropOfAddress" id="dropOfAddress">Địa chỉ nhận :{{@$ship['dropOffAddress']}} </label>--}}
            <div id="frmUpdateInfo" class="row-fluid">
                <div class="form-horizontal row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtPhoneNumber">Số điện thoại</label>
                        <div class="controls">
                            <input autocomplete="off"
                                   data-validation="custom"
                                   data-validation-regexp="^0(([8-9][0-9]{8})|(1[0-9]{9}))$"
                                   data-validation-error-msg-custom="Vui lòng nhập đúng định dạng SĐT"
                                   class="span12" type="number" name="phoneNumber" id="txtPhoneNumber">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="txtFullName">Họ tên</label>
                        <div class="controls">
                            <input autocomplete="off"
                                   class="span12" type="text" name="fullName" id="txtFullName">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="modal-footer">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="ticketId" value="" id="txtTicketId">
                <input type="hidden" name="fullName" value="{{@$ship['fullName']}}" id="txtFullName">
                <input type="hidden" name="dropOffAddress" value="{{@$ship['dropOffAddress']}}" id="txtDropOffAddress">
                <input type="hidden" name="option" value="" id="txtOption">
                <a style="cursor: pointer" data-dismiss="modal" aria-hidden="true">HỦY</a>
                <button id="cancel" class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
            </div>
        </div>
    </form>
</div>

<script>
    var startDate = $("#txtStartDate"),
        endDate = $("#txtEndDate"),
        variables = '',
        defaultDateForCalendar = '{{request('date',date('d-m-Y'))}}',
        urlSeachShip = '{{action('ShipController@show')}}';

    $(document).ready(function () {
        $('#ticketId').change(function () {
           $(this).attr('value',$(this).val());

        });
        if ('{{request('startDate')}}' == '' && '{{request('endDate')}}' == '') {
            endDate.datepicker().datepicker("setDate", new Date());
            startDate.datepicker().datepicker("setDate", new Date());
        }
        $('#search').click(function () {
            $('#txtStartDate').attr('value',$('#txtStartDate').val());
            $('#txtEndDate').attr('value',$('#txtEndDate').val());

            var start= $('#txtStartDate').val();
            var end= $('#txtEndDate').val();
            var start_Str='';
            var end_Str='';
            for (var i = 0; i < 10; i++) {

                if (i ==1 || i == 0) {
                    start_Str += start.charAt(i+3);
                    end_Str += end.charAt(i+3);
                }else
                if (i ==3 || i == 4) {
                    start_Str += start.charAt(i-3);
                    end_Str += end.charAt(i-3);
                }
                else {
                    start_Str += start.charAt(i);
                    end_Str += end.charAt(i);
                }
            }
            var start= (new Date(start_Str)).getTime();
            var end= (new Date(end_Str)).getTime();

            if(start > end) {
                alert("Yêu cầu ngày bắt đầu phải nhỏ hơn!");
                reset();
            }
        });
        function reset(){
            $('#txtStartDate').attr('value',$('#txtStartDate').val(''));
            $('#txtEndDate').attr('value',$('#txtEndDate').val(''));
            $('#txtStartDate').val('');
            $('#txtEndDate').val('');
        }

        $('#routeName').select2();
//        $(datepickerBox).change(function () {
//             updateListShip(true);
//        });

//        function updateListShip() {
//            var start = $(startDate.datepicker()).val();
//            var end = $(endDate.datepicker()).val();
//            Loaing(true);
//            $.ajax({
//                dataType: "json",
//                url: urlSeachShip,
//                data: {
//                    'start': start.replace('/', '-').replace('/', '-'),
//                    'end': end.replace('/', '-').replace('/', '-'),
//
//                },
//                success: function (data) {
//
//                },
//                error: function () {
//                    Message("Lỗi!", "Vui lòng tải lại trang", "");
//                    Loaing(false);
//                }
//            })
//        }

        $('tbody.listship > tr.item').click(function () {
            $(this).next().toggleClass("hide");
        });
        $('.returnGoods').click(function () {
            $('#lb_message').text('Bạn muốn trả đồ của vé này').show();
            $('#title-update-ticket').text('Trả đồ');
            $('#frmUpdateInfo').hide();
            $('#txtOption').val(4);
            $('#txtTicketId').val($(this).val());
            $('#cancel_or_return').modal('show');

        });

    });
    $('body').on('click', 'button#cancelTicket', function (e) {
        if ($(e.currentTarget).is('#cancelTicket')) {
            $('#lb_message').text('Bạn có chắc muốn huỷ vé này?').show();
            $('#title-update-ticket').text('HUỶ VÉ');
            $('#frmUpdateInfo').hide();
            $('#txtOption').val(2);

        }

        $('#txtTicketId').val($(this).val());
        $('#cancel_or_return').modal('show');
    });


</script>

