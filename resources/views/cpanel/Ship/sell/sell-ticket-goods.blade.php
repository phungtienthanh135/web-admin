<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>

<form id="frmSellTicketGoods" action="{{action('ShipController@postSellTicketGood')}}" method="post"
      enctype="multipart/form-data">
    <div class="span8 banve_thongtintinve">
        <div class="widget widget-2 widget-tabs widget-tabs-2 no_border">
            <h3>THÔNG TIN NGƯỜI NHẬN</h3>
            <div id="ve" class="span8">
                <table class="tb_banve_thongtintinve" cellpadding="4">
                    <tr>
                        <td>TÊN NGƯỜI NHẬN</td>
                        <td><input autocomplete="off" class="m_bottom_0" type="text"
                                   value="{{request('fullNameReceiver')}}" name="fullNameReceiver"/></td>
                    </tr>
                    <tr>
                        <td>SĐT NGƯỜI NHẬN
                        </td>
                        <td><input autocomplete="off" value="{{request('phoneNumberReceiver')}}"
                                   {{--							data-validation="custom required"
                                                               data-validation-regexp="^0(([8-9][0-9]{8})|(1[0-9]{9}))$"
                                                               data-validation-error-msg-custom="Vui lòng nhập đúng định dạng SĐT"
                                                               data-validation-error-msg="Vui lòng nhập số điện thoại người nhận"--}}
                                   class="m_bottom_0" type="number"
                                   name="phoneNumberReceiver"/>
                        </td>
                    </tr>
                    {{--<tr>--}}
                        {{--<td>ĐỊA CHỈ NHẬN HÀNG</td>--}}
                        {{--<td>--}}
                            {{--<input type="text" value="{{request('pickUpAddress')}}" name="pickUpAddress"--}}
                                   {{--id="pickUpAddress_good" class="m_bottom_0 startPoint">--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td>ĐỊA CHỈ TRẢ HÀNG</td>--}}
                        {{--<td>--}}
                            {{--<input type="text" value="{{request('dropOffAddress')}}" name="dropOffAddress"--}}
                                   {{--id="dropOffAddress_good" class="m_bottom_0 startPoint">--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td>TÊN SP
                        </td>
                        <td>
                            <input  class="m_bottom_0" type="text" name="ticketName"
                                   value="{{request('ticketName')}}"/>
                        </td>
                    </tr>
                    <tr>
                        <td>TRỌNG LƯỢNG Kg
                        </td>
                        <td>
                            <input  autocomplete="off" placeholder="Vd: 1.5"
                                    type="number" data-validation-allowing="float" id="txt_weight"
                                    value="{{request('weight')}}"
                                   name="weight" class="m_bottom_0"/>
                        </td>
                    </tr>
                    {{--<tr>--}}
                    {{--<td>KÍCH THƯỚC (m3)</td>--}}
                    {{--<td>--}}
                    {{--<input value="{{request('dimension')}}" autocomplete="off" placeholder="Vd: 3x6x7" id="txt_dimension"--}}
                    {{--class="m_bottom_0" type="text" name="dimension"/>--}}
                    {{--</td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td>LOẠI ĐỒ(giá cố định)</td>
                        <td>
                            <select value="{{request('goodTypeId')}}" id="goodTypeId" class="m_bottom_0"
                                    name="goodTypeId">
                                <option></option>
                                @foreach($listTypeShip as $row)
                                    <option value="{{@$row['goodsTypeId']}}" @php echo ( $row['goodsTypeId']== request('goodTypeId') ? 'selected':''); @endphp >
                                        {{@$row['goodsTypeName']}}
                                    </option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>NHÂN VIÊN NHẬN HÀNG</td>
                        <td>
                            <select class="chonnv" name="fullNameEmployee" id="chonnv"
                                    value="{{request('fullNameEmployee')}}">
                                <option></option>
                                @foreach($result as $row)
                                    <option value="{{@$row['fullName']}}" @php echo ( $row['fullName']== request('fullNameEmployee') ? 'selected':''); @endphp >
                                        {{$row['fullName']}}
                                    </option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>TUYẾN
                        </td>
                        <td>
                            <select name="routeId" id="routeId" value="{{request('routeId')}}">
                                <option value=""></option>
                                @foreach($listRoute as $row)
                                    <option value="{{$row['routeId']}}" @php echo ( $row['routeId']== request('routeId') ? 'selected':''); @endphp>
                                        {{$row['routeName']}}
                                    </option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    {{--<tr>--}}
                    {{--<td>GIÁ VÉ</td>--}}
                    {{--<td id="giaVeGuiDoGoc">--}}
                    {{--<input type="number" value="{{request('paymentTicketPrice')}}" required min="0"--}}
                    {{--id="paymentTicketPriceInput" autocomplete="off"--}}
                    {{--data-validation-error-msg-required="Vui lòng nhập giá vé"--}}
                    {{--data-validation="required number" data-validation-allowing="float">--}}
                    {{--<input type="hidden" name="paymentTicketPrice" id="paymentTicketPrice"--}}
                    {{--value="{{request('paymentTicketPrice')}}">--}}
                    {{--</td>--}}
                    {{--</tr>--}}
                    <tr id="nhan">
                        <td>ĐIỂM NHẬN</td>
                        <td>
                            <select name="getInPointId" id="getInPointId" value="{{request('getInPointId')}}">
                                <option value=""></option>
                            </select>
                        </td>
                    </tr>
                    <tr id="tra">
                        <td>ĐIỂM TRẢ</td>
                        <td>
                            <select name="getOffPointId" id="getOffPointId" value="{{request('getOffPointId')}}">
                                <option value=""></option>
                                {{--@foreach($listRoute as $v)--}}
                                {{--@foreach($v['listPoint'] as $row)--}}
                                {{--<option value="{{$row['pointId']}}">--}}
                                {{--{{$row['pointName']}}--}}
                                {{--</option>--}}
                                {{--@endforeach--}}
                                {{--@endforeach--}}
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="frm_chonanh span4">
                <div class="chonanh">
                    <img id="blah" src="/public/images/anhsanpham.png"/>
                </div>
                <input name="img" type="file" id="selectedFile" style="display: none;"/>
                <input type="button" style="width:172px" value="CHỌN ẢNH"
                       class="btncustom"
                       onclick="document.getElementById('selectedFile').click();"/>
            </div>
            <div id="totalPrice" class="span12">
                <div class="ht_tongtien">
                    <div class="span3 color_blue_2 fs_large fw_ex_bold">TỔNG TIỀN</div>
                    <div id="loading" hidden style="width: 100%">
                        <img style="padding-left: 50%" src="/public/images/loading/loading32px.gif" alt="">
                    </div>
                    <div class="span9 color_red fs_large fw_ex_bold" style="margin-top: -5px;">
                        <input required class="right" type="number" name="paymentTicketPrice" id="tongGiaVeGuiDo" value="{{request('paymentTicketPrice')}}" placeholder="0 VNĐ" style="border: none;font-size:large;background-color: #af0e0e;color: white;">
                        {{--<span style=" padding-left: 15px;">VNĐ</span>--}}
                        {{--<input type="hidden" name="paymentTicketPrice" id="paymentTicketPrice" value="">--}}
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div id="khack" class="span4 banve_thongtinkhachhang">
        <div class="widget widget-2 widget-tabs widget-tabs-2 no_border">
            <h3>THÔNG TIN NGƯỜI GỬI</h3>
            <table class="tb_banve_thongtinkhachhang" cellpadding="4">
                <td>SĐT
                </td>
                <td><input autocomplete="off" required
                           {{--					data-validation="custom required"
                                               data-validation-regexp="^0(([8-9][0-9]{8})|(1[0-9]{9}))$"
                                               data-validation-error-msg-custom="Vui lòng nhập đúng định dạng SĐT"
                                               data-validation-error-msg="Vui lòng nhập số điện thoại người gửi"--}}
                           class="m_bottom_0 CustomerPhoneNumber" type="number"
                           value="{{request('phoneNumber')}}" name="phoneNumber"/>
                </td>
                </tr>
                <tr>
                    <td>HỌ TÊN</td>
                    <td><input autocomplete="off" class="m_bottom_0 CustomerFullName" type="text"
                               value="{{request('fullName')}}"
                               name="fullName"/></td>
                </tr>
                <tr>
                    <td>EMAIL</td>
                    <td><input autocomplete="off"
                               type="email" data-validation-error-msg-email="Vui lòng nhập đúng định dạng"
                               class="m_bottom_0"
                               value="{{request('email')}}"
                               name="email"/></td>
                </tr>
                <tr>
                    <td>NGÀY GỬI
                    </td>
                    <td>
                        <input readonly autocomplete="off" placeholder="Ngày - Tháng - Năm" id="txtStartDate"
                               name="getInTimePlan" type="text" value="{{request('getInTimePlan')}}">
                        <style type="text/css" media="screen">
                            .ui-datepicker-trigger {
                                display: none;
                            }
                        </style>
                    </td>
                </tr>
                <tr>
                    <td>NGÀY NHẬN DỰ KIẾN
                    </td>
                    <td>
                        <input readonly autocomplete="off"  placeholder="Ngày - Tháng - Năm" id="txtEndDate"
                               name="getOffTimePlan" type="text" value="{{request('getOffTimePlan')}}">
                        <style type="text/css" media="screen">
                            .ui-datepicker-trigger {
                                display: none;
                            }
                        </style>
                    </td>
                </tr>
                <tr>
                    <td>MÃ KM</td>
                    <td>
                        <div class="input-append">
                            <input autocomplete="off" data-val="0" id="promotionId" class="m_bottom_0 span9"
                                   name="promotionId" type="text" value="{{request('promotionId')}}"/>
                            <button type="button" class="btn btnCheckPromotionCode">CHECK</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="center" colspan="2">
                        <div style="padding-top: 30px;width: 100%;"></div>
                        <div style="margin-left: 105px;" class="f_left m_right_10">Người nhận trả tiền
                        </div>
                        <input autocomplete="off" type="checkbox" id="nguoi_nhan_tra_tien"
                               name="paidMoney"/>
                        <label class="f_left" for="nguoi_nhan_tra_tien"></label>
                    </td>
                </tr>
                {{-- <tr>
                    <td class="center" colspan="2">
                        <div style="margin-left: 62px;" class="f_left m_right_10">Gửi mã số cho người nhận
                        </div>
                        <input checked type="checkbox" name="sendSMS" id="sent_code_to_receive"/>
                        <label class="f_left" for="sent_code_to_receive"></label>
                    </td>
                </tr>--}}
                <tr>
                    {{--<td class="center" colspan="2">--}}
                    {{--<div style="margin-left: 77px;" class="f_left m_right_10">--}}
                    {{--In vé sau khi thanh toán--}}
                    {{--</div>--}}
                    {{--<input type="checkbox" id="print_ticket_after_save" name="print"/>--}}
                    {{--<label class="f_left" for="print_ticket_after_save"></label>--}}
                    {{--</td>--}}
                </tr>
                <tr>
                    <td class="center" colspan="2">
                        <div style="margin-left: 147px;" class="f_left m_right_10">Gửi tin nhắn
                        </div>
                        <input type="checkbox" id="sendSMS" name="sendSMS"/>
                        <label class="f_left" for="sendSMS"></label>

                    </td>
                </tr>
                <tr>
                    <td class="center" colspan="2">
                        <div style="padding-top: 30px;width: 100%;"></div>
                        <select name="paymentType" class="cbb_paymentType m_bottom_0 w_full">
                            <option value="1">Thanh toán trực tiếp</option>
                            <option value="2">Thanh toán tiền mặt cho phụ xe</option>
                            <option value="3">Thanh toán tiền mặt tại quầy</option>
                            <option value="4">Thanh toán bằng tiền mặt khi người nhận nhận đồ</option>
                            <option value="5">Thanh toán bằng tiền mặt tại Payoo</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="center" colspan="2">
                        <input type="hidden" name="listImages[]" id="listImages">

                        <input type="hidden" name="tripId" class="tripId" value="{{old('tripId')}}" id="txt_tripId">
                        <input type="hidden" name="scheduleId" class="scheduleId" value="{{old('scheduleId')}}"
                               id="txt_scheduleId">
                        <input type="hidden" class="promotionId" value="{{old('promotionId')}}" name="promotionId"/>
                        <input type="hidden" class="promotionCode" value="{{old('promotionCode')}}"
                               name="promotionCode"/>
                        <input type="hidden" class="salePrice" name="salePrice"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input name="print" type="hidden" value="print">
                        <input id="btnSellTicketGoods" href="" class="btncustom w_full m_top_0" type="submit"
                               value="THANH TOÁN" name="submit">

                    </td>
                </tr>
            </table>
        </div>
    </div>
</form>
<div class="span2" style="float: right">
    <button style="float: right;margin-right: 20px" id="printTicket" class="btncustom">In Vé</button>

</div>
<script src="/public/javascript/jquery.printPage.js" type="text/javascript"></script>
<script src="/public/javascript/printThis.js" type="text/javascript"></script>
