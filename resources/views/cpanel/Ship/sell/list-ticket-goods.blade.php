<div class="row-fluid">
    <div class="span12">
        <table class="m_top_10 table table-hover table-vertical-center" style="display: block; width: 100%;">
            <thead>
                <tr>
                    <th style="width:100px">Mã vé</th>
                    <th style="width:100px" >Người nhận</th>
                    <th style="width:100px" >SĐT người nhận</th>
                    <th style="width:100px" >Tên sản phẩm</th>
                    <th style="width:100px" >Tên người gửi</th>
                    <th style="width:150px" >Ngày Gửi</th>
                    <th style="width:150px" >Ngày Nhận Dự Kiến</th>
                    <th style="width:100px" >Giá Vé</th>
                    <th style="width:50px" >Tuỳ chọn</th>
                </tr>
            </thead>
            <tbody style="overflow: scroll; display: block; width: 100%; height: 500px" >
                @foreach($arr as $row)
                <tr>
                    <td style="width:100px">{{$row['ticketCode']}}</td>
                    <td style="width:100px">{{$row['fullNameReceiver']}}</td>
                    <td style="width:100px">{{$row['phoneNumberReceiver']}}</td>
                    <td style="width:100px">{{@$row['ticketName']}}</td>
                    <td style="width:100px">{{@$row['fullName']}}</td>
                    <td style="width:150px">@date($row['getInTimePlan']/1000)</td>
                    <td style="width:150px">@date($row['getOffTimePlan']/1000)</td>
                    <td style="width:100px">{{$row['paymentTicketPrice']}}</td>
                    <td style="width:50px">
                        <a tabindex="0"
                            class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                            <ul class="onclick-menu-content">
                                <li>
                                    <button type="button" onclick="showInfo(this)">Chi tiết</button>
                                </li>
                                <li>
                                    <button value="{{$row['ticketId']}}" id="editTicket">Sửa vé</button>
                                </li>
                                <li>
                                    <button value="{{$row['ticketId']}}" id="cancelTicket">Hủy vé</button>
                                </li>
                                <li>
                                    <button value="{{$row['ticketId']}}" id="orderTicket">Thanh toán</button>
                                </li>
                            </ul>
                        </a>
                    </td>
                </tr>
                <!--hiển thị chi tiết-->
                <tr class="info" style="display: none">
                    <td colspan="6">
                        <table class="tttk" cellspacing="20" cellpadding="4">
                            <tbody>
                                <tr>
                                    <td><b>TÊN NGƯỜI NHẬN</b></td>
                                    <td>{{array_get($row,'fullNameReceiver','Chưa xác định')}}</td>
                                </tr>
                                <tr>
                                    <td class="right"><b>TÊN SẢN PHẨM</b></td>
                                    <td class="left">{{$row['ticketName']}}</td>
                                </tr>
                                <tr>
                                    <td><b>SĐT NGƯỜI NHẬN</b></td>
                                    <td>{{array_get($row,'phoneNumberReceiver','Chưa xác định')}}</td>
                                </tr>
                                <tr>
                                    <td class="right"><b>TRỌNG LƯỢNG</b></td>
                                    <td class="left">{{$row['weight']}}Kg</td>
                                </tr>
                                <tr>
                                    <td><b>ĐỊA CHỈ NHẬN HÀNG</b></td>
                                    <td>{{array_get($row,'pickUpAddress','Chưa xác định')}}</td>
                                </tr>
                                <tr>
                                    <td class="right"><b>KÍCH THƯỚC</b></td>
                                    <td>{{$row['dimension']}}</td>
                                </tr>
                                <tr>
                                    <td><b>ĐỊA CHỈ TRẢ HÀNG</b></td>
                                    <td>{{array_get($row,'dropOffAddress','Chưa xác định')}}</td>
                                </tr>
                                <tr>
                                    <td class="right"><b>MÃ KHUYẾN MÃI</b></td>
                                    <td>{{$row['promotionId']}}</td>
                                </tr>
                                <tr>
                                    <td><b>THỜI GIAN ĐI</b></td>
                                    <td>@date($row['getInTimePlan']/1000)</td>
                                </tr>
                                <tr>
                                    <td class="right"><b>GIÁ VÉ</b></td>
                                    <td>@moneyFormat($row['paymentTicketPrice'])</td>
                                </tr>
                                <tr>
                                    <td><b>THỜI GIAN ĐẾN</b></td>
                                    <td>@date($row['getOffTimePlan']/1000)</td>
                                </tr>
                                <tr>
                                    <td><b>TÊN NGƯỜI GỬI</b></td>
                                    <td colspan="6">{{$row['fullName']}}</td>
                                </tr>
                                <tr>
                                    <td><b>SĐT NGƯỜI GỬI</b></td>
                                    <td colspan="6">{{$row['phoneNumber']}}</td>
                                </tr>
                                <tr>
                                    <td><b>EMAIL</b></td>
                                    <td colspan="6"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td colspan="2">
                        <table >
                            <tr>
                                <td class="relative">
                                    <div class="khung_hinh_thongtinsp">
                                        <img src="{{head($row['listImages'])}}">
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="modal modal-custom hide fade" id="modal_updateTicket" style="width: 400px; margin-left: -10%;margin-top: 10%;">
    <form action="{{action('TicketController@updateStatusTicket')}}" method="post">
        <div class="modal-header center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="title-update-ticket">CẬP NHẬT THÔNG TIN VÉ</h3>
        </div>
        <div class="modal-body center">
            <p id="lb_message"></p>
            <div id="frmUpdateInfo" class="row-fluid">
                <div class="form-horizontal row-fluid">
                    <div class="control-group">
                        <label class="control-label" for="txtPhoneNumber">SĐT người gửi</label>
                        <div class="controls">
                            <input autocomplete="off"
                            data-validation="custom"
                            data-validation-regexp="^0(([8-9][0-9]{8})|(1[0-9]{9}))$"
                            data-validation-error-msg-custom="Vui lòng nhập đúng định dạng SĐT"
                            class="span12" type="number" name="phoneNumber" id="txtPhoneNumber">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="txtFullName">Họ tên người gửi</label>
                        <div class="controls">
                            <input autocomplete="off"
                            class="span12" type="text" name="fullName" id="txtFullName">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="txtPhoneNumberReceiver">SĐT người nhận</label>
                        <div class="controls">
                            <input autocomplete="off"
                            data-validation="custom"
                            data-validation-regexp="^0(([8-9][0-9]{8})|(1[0-9]{9}))$"
                            data-validation-error-msg-custom="Vui lòng nhập đúng định dạng SĐT"
                            class="span12" type="number" name="phoneNumberReceiver" id="txtPhoneNumberReceiver">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="txtFullNameReceiver">Họ tên người nhận</label>
                        <div class="controls">
                            <input autocomplete="off"
                            class="span12" type="text" name="fullNameReceiver" id="txtFullNameReceiver">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="modal-footer">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="ticketId" value="" id="txtTicketId">
                <input type="hidden" name="option" value="" id="txtOption">
                <a style="cursor: pointer" data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                <button id="btnUpdateStatusTicket" class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
            </div>
        </div>
    </form>
</div>
<script>
$('body').on('click', 'button#cancelTicket,button#orderTicket,button#editTicket', function (e) {
if ($(e.currentTarget).is('#orderTicket')) {
$('#lb_message').text('Bạn có muốn thanh toán vé này?').show();
$('#title-update-ticket').text('THANH TOÁN VÉ');
$('#frmUpdateInfo').hide();
$('#txtOption').val(1)
}
if ($(e.currentTarget).is('#cancelTicket')) {
$('#lb_message').text('Bạn có chắc muốn huỷ vé này?').show();
$('#title-update-ticket').text('HUỶ VÉ');
$('#frmUpdateInfo').hide();
$('#txtOption').val(2)
}
if ($(e.currentTarget).is('#editTicket')) {
$('#lb_message').text('').hide();
$('#title-update-ticket').text('CẬP NHẬT THÔNG TIN VÉ');
$('#frmUpdateInfo').show();
$('#txtOption').val(3)
}
$('#txtTicketId').val($(this).val());
$('#modal_updateTicket').modal('show');
})
</script>