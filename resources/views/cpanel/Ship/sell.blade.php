@extends('cpanel.template.layout')
@section('title', 'Bán vé gửi đồ')
@section('content')
    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Thông tin vé gửi đồ</h3></div>
            </div>
        </div>
        <div class="ticket">
            @include('cpanel.Ship.sell.sell-ticket-goods')
        </div>
        {{--<div class="heading_top" style="margin-top: 50px">
            <div class="row-fluid">
                <div class="pull-left span8"><h3>Danh sách vé</h3></div>
            </div>
        </div>
        <div class="list_ticket">
            @include('cpanel.Ship.sell.list-ticket-goods')
        </div>--}}
    </div>

    <script src="/public/javascript/anvui.js?v=1.3" type="text/javascript"></script>
    <script>
        // $('#chonnv').select2();
        $('#paymentTicketPriceInput').on(function () {
            setTotalPrice();
        });
        @if(isset($msg))
        Message('Thông báo', '{{$msg}}', '')
        @endif
        $(document).ready(function () {
            $('#txtStartDate').datepicker().datepicker("setDate", new Date());
            $('#nhan').hide();
            $('#tra').hide();
            $('#printTicket').click(function () {
                @if(isset($code) && $code ==200)
                $('#frmSellTicketGoods').printThis({
                    pageTitle: '&nbsp;'
                });
                @else
                Message('Thông báo', 'Thanh toán trước khi in', '')
                @endif
            });
            $('body').on('change', 'select#routeId', function () {
                var listRoute ={!! json_encode($listRoute)!!};
                var html = '<option></option>';
                $('#nhan').show();
                $('#tra').show();
                for (var i = 0; i < listRoute.length; i++) {
                    if (listRoute[i].routeId == this.value) {
                        for (var j = 0; j < listRoute[i].listPoint.length; j++)
                            html += '<option value="' + listRoute[i].listPoint[j].pointId + '">' + listRoute[i].listPoint[j].pointName + '</option>';
                        i = listRoute.length;
                    }
                }
                $('#getInPointId').html('');
                $('#getOffPointId').html('');
                $('#getInPointId').append(html);
                $('#getOffPointId').append(html);

            });
            $('body').on('change', 'select#goodTypeId', function () {
                if (this.value != '') {
                    $('#loading').show();
                    // $('#getInPointId').val('');
                    // $('#getOffPointId').val('');
                    $.post('{{action('ShipController@getPriceShip')}}', {
                        _token: '{{csrf_token()}}',
                        goodTypeId: this.value
                    }, function (data) {
                        if (data.code == 200) {
                            $('#loading').hide();
                            $('#paymentTicketPrice').val(data.results.price);
                            $('#tongGiaVeGuiDo').val(parseInt(data.results.price));
                            Message("Thông báo", "Cập nhật giá cố định ");
                        } else
                            Message("Thông báo", "Vui lòng thử lại");
                    });

                    $('#loading').hide();
                } else {
                    $('#nhan').show();
                    $('#tra').show();
                    $('#tongGiaVeGuiDo').html('');
                }
            });
            $('body').on('change', 'select#getInPointId,select#getOffPointId', function () {
                if ($('#getInPointId').val() != '' && $('#getOffPointId').val() != '')
                    if ($('#getInPointId option:selected').index() < $('#getOffPointId option:selected').index()) {
                        if (($('#getInPointId').val() != $('#getOffPointId').val())) {
                            if ($('select#goodTypeId').val() == '') {
                                $('#loading').show();
                                $.post('{{action('ShipController@getPriceShip')}}', {
                                    _token: '{{csrf_token()}}',
                                    routeId: $('#routeId').val(),
                                    getInPointId: $('#getInPointId').val(),
                                    getOffPointId: $('#getOffPointId').val(),

                                }, function (data) {
                                    console.log(data);
                                    if (data.code == 200) {

                                        $('#loading').hide();
                                        $('#paymentTicketPrice').val(data.results.price);
                                        $('#tongGiaVeGuiDo').val(parseInt(data.results.price));
                                        Message("Thông báo", "Cập nhật giá thành công!");
                                    } else
                                        Message("Thông báo", "Vui lòng thử lại");
                                });
                            }
                            $('#loading').hide();
                        } else {
                            $(this).val('');
                            Message("Thông báo", "Vui lòng chọn điểm khác nhau");
                        }

                    } else {
                        $('#getInPointId').val('');
                        $('#getOffPointId').val('');
                        Message("Thông báo", "Điểm nhận trả sai chiều");
                    }
            });
        });

        function setTotalPrice() {
            var gia = $('#paymentTicketPrice').val();
            var km = $('#promotionId').attr("data-val");
            if (km <= 1) km *= gia;
            if (gia > km) {
                gia = Math.floor(gia - km);
                $('#tongGiaVeGuiDo').html($.number(gia));
                $('#paymentTicketPrice').val((gia));
            } else {
                $('#tongGiaVeGuiDo').html($.number(gia));
                $('#paymentTicketPrice').val((gia));
            }
        }

        var urlCheckPromotionCode = '{{action('CouponController@check')}}',//url lấy thông tin khuyến mãi
            urlGetPriceTicketGoods = '{{action('ShipController@getPriceTicketGoods')}}',//url lấy giá vé gửi đồ
            selectBoxRoute = $('#chontuyen');//selectbox chọn tuyến


        /*
        /*
        * sự kiện nhập mã khuyến mãi
        * */
        $('.btnCheckPromotionCode').click(function () {
// $('#tongGiaVeGuiDo').text('Đang cập nhật giá...');
            var btn = this;
            if ($(btn).text() === 'X') {
                $('.salePrice').val(0);
                $('.promotionId').val('');
                $('#promotionId').attr("data-val", "0");
                $('.promotionCode').val('');
                $(btn).prev('input').removeAttr('readonly').val('').removeClass('span12').addClass('span9');
                $(this).text('CHECK');
                setTotalPrice();
            } else {
                if ($(this).prev('input').val() != '') {
                    $(btn).html('<img src="/public/images/loading/loader_blue.gif" width="19px">');
                    $(btn).prop('disabled', true);
                    $.ajax({
                        'url': urlCheckPromotionCode,
                        'data': {
                            'scheduleId': $('#txt_scheduleId').val(),
                            'promotionCode': $(this).prev('input').val()
                        },
                        'dataType': 'json',
                        'success': function (data) {
                            var result = data.result;
                            console.log(result);
                            if (result.percent != undefined && result.price != undefined) {
                                if (result.percent > 0) {
                                    $('.salePrice').val(result.percent);
                                    Message('Thông báo', 'Kiểm tra mã khuyến mãi thành công<br>Được giảm: ' + result.percent * 100 + '%', '');
                                    $(btn).prev('input').val(result.promotionCode + ' - Giảm ' + result.percent * 100 + '%')
                                        .removeClass('span9').addClass('span12');
                                    if ($('#paymentTicketPrice').val() > 0) {
                                        var sale = $('#paymentTicketPrice').val() * result.percent;
                                    } else var sale = result.percent;

                                    $('.salePrice').val(sale);
                                    $('#promotionId').attr("data-val", sale);
                                    setTotalPrice();
                                }
                                if (result.price > 0) {
                                    $('.salePrice').val(result.price);
                                    Message('Thông báo', 'Kiểm tra mã khuyến mãi thành công<br>Được giảm: ' + result.price + 'VNĐ', '');
                                    $(btn).prev('input').val(result.promotionCode + ' - Giảm ' + result.price + 'VNĐ')
                                        .removeClass('span9').addClass('span12');
                                    var sale = result.price;

                                    $('.salePrice').val(sale);
                                    $('#promotionId').attr("data-val", sale);
                                    setTotalPrice();
                                }
                                $('.promotionId').val(result.promotionId);
                                $('.promotionCode').val(result.promotionCode);
                                $(btn).prev('input').attr('readonly', 'readonly');
                                $(btn).text('X');
                                $(btn).prop('disabled', false);
                            } else {
                                Message('Thông báo', 'Mã khuyến mãi không hợp lệ', '');
                                $(btn).text('CHECK');
                                $(btn).prop('disabled', false);
                            }
                        },
                        'error': function () {
                            Message('Thông báo', 'Lấy mã khuyến mãi thấ bại! Vui lòng tải lại trang.', '');
                            $(btn).text('CHECK');
                            $(btn).prop('disabled', false);
                        }
                    });
                }
            }
        });
        $('#nguoi_nhan_tra_tien').change(function () {
            if ($(this).is(':checked')) {
                $('#btnSellTicketGoods').val('HOÀN THÀNH')
            } else {
                $('#btnSellTicketGoods').val('THANH TOÁN')
            }
        });

        /*Preview ảnh*/

        $("#selectedFile").change(function () {
            UploadImage();
        });
        $('#tongGiaVeGuiDo').change(function () {
            $('#paymentTicketPrice').val(($('#tongGiaVeGuiDo').val()).replace(/,/g, ''));
        });

        function UploadImage() {
            var form = new FormData($("#frmSellTicketGoods")[0]);
            $('#blah').attr('src', '/public/images/loading/loader_blue.gif');
            $.ajax({
                type: 'POST',
                url: '/cpanel/system/upload-image',
                data: form,
                dataType: 'json',
                processData: false,
                contentType: false,
                cache: false
            }).done(function (data) {
                $('#blah').attr('src', data.url);
                $('#listImages').val(data.url);
            });
        }

        // $('#paymentTicketPriceInput').change(function () {
        //     setTotalPrice();

    </script>
@endsection
