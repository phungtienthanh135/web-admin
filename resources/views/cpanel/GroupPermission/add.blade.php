@extends('cpanel.template.layout')
@section('title', 'Thêm mới nhóm quyền')
@section('content')
    <form action="" method="post">
        <div id="content">
            <div class="heading_top">
                <div class="row-fluid">
                    <div class="pull-left span8">
                        <h3>Thêm Mới Nhóm Quyền</h3></div>
                </div>
            </div>
            <div class="separator bottom"></div>
            <div class="innerLR">
                <div class="row-fluid">
                    <div class="span4">
                        <label for="groupPermissionName">Tên nhóm quyền</label>
                        <input type="text" name="groupPermissionName" id="groupPermissionName"
                               value="{{old('groupPermissionName')}}">
                    </div>

                    <div class="span4">
                        <label for="groupPermissionDescription">Mô tả nhóm quyền</label>
                        <input type="text" name="groupPermissionDescription" id="groupPermissionDescription"
                               value="{{old('groupPermissionDescription')}}">
                    </div>
                </div>
                <div class="row-fluid">
                    @foreach($reponse_list_permit as $groupPermission)
                        @if(!empty(array_values($groupPermission)[0]))
                            <div class="span3">
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse"
                                           href="#{{array_keys($groupPermission)[0]}}">{{$labelCategory[array_keys($groupPermission)[0]]}}
                                        </a>
                                        <span class="glyphicon-arrow-up"></span>
                                    </div>
                                    <div id="{{array_keys($groupPermission)[0]}}" class="accordion-body collapse in">
                                        <div class="accordion-inner">
                                            <ul class="unstyled">
                                                @foreach(array_values($groupPermission)[0] as $permission)
                                                    <li>
                                                        <input type="checkbox" name="listFuncCode[]"
                                                               id="{{$permission['funcId']}}"
                                                               value="{{$permission['funcCode']}}">
                                                        <label for="{{$permission['funcId']}}">{{$permission['funcName']}}</label>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach

                </div>
                <div class="control-box">
                    <div class="row-fluid">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="span3">
                            <button type="submit" class="btn btn-warning btn-flat-full">LƯU</button>
                        </div>

                        <div class="span2">
                            <a href="{{action('GroupPermissionController@show')}}"
                               class="btn btn-default btn-flat-full">Hủy</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </form>
    <script>
        function List_link_permision_group() {
            var ids = [];
            $('#level_group1>accordion-inner>ul>li').each(function (index, elem) {
                elem.push($(this).attr('id'));
            });
        alert(ids);
        }
    </script>
    <!-- End Content -->

@endsection