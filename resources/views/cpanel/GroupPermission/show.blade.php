@extends('cpanel.template.layout')
@section('title', 'Quản Lý Nhóm Quyền')
@section('content')

    <div id="content">
        <div class="heading_top">
            <div class="row-fluid">
                <div class="pull-left span8">
                    <h3>Danh Sách Nhóm Quyền</h3></div>
            </div>
        </div>

        <div class="separator bottom"></div>
        <div class="innerLR">
            <div class="row-fluid">
                <div class="control-box">
                    <form action="">
                        <div class="row-fluid">

                            <div class="span6">
                                <label for="name_level">Tên nhóm quyền</label>
                                <input type="text" name="groupName" id="name_level">
                            </div>
                            <div class="span2">
                                <label class="separator hidden-phone" for="add"></label>
                                <button class="btn btn-info btn-flat-full" id="search">TÌM QUYỀN</button>
                            </div>
                            @if(hasAnyRole(CREATE_GROUP_PERMISSION))
                            <div class="span2">
                                <label class="separator hidden-phone" for="add"></label>
                                <a href="{{action('GroupPermissionController@getadd')}}"
                                   class="btn btn-warning btn-flat-full" id="add">THÊM QUYỀN</a>
                            </div>
                            @endif

                        </div>
                    </form>
                </div>
            </div>

            <div class="row-fluid">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        {{--<th>Mã nhóm quyền</th>--}}
                        <th>Tên nhóm quyền</th>
                        <th>Thời gian tạo quyền</th>
                        <th class="center">Tùy chọn</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($result as $row)
                        <tr>
                            {{--<td>{{$row['groupPermissionId']}}</td>--}}
                            <td>{{$row['groupPermissionName']}}</td>
                            <td>@dateFormat($row['createdDate'])</td>
                            <td class="center">
                                <a tabindex="0" class="glyphicon glyphicon-option-vertical onclick-menu"><i></i>
                                    <ul class="onclick-menu-content">
                                        @if(hasAnyRole(UPDATE_GROUP_PERMISSION))

                                        <li>
                                            <!-- <button onclick="showUpdate('{{$row['groupPermissionId']}}');">Sửa</button> -->
                                            <button class="editPermission" data-toggle="modal" data-id="{{$row['groupPermissionId']}}" data-target="#myModal">Sửa</button>
                                        </li>
                                        @endif
                                        @if(hasAnyRole(DELETE_GROUP_PERMISSION))
                                        <li>
                                            <button data-toggle="modal" data-target="#modal_delete"
                                                    value="{{$row['groupPermissionId']}}"
                                                    onclick="$('#delete_group_id').val($(this).val());">Xóa
                                            </button>
                                        </li>
                                        @endif
                                    </ul>
                                </a>
                            </td>
                        </tr>

                    @empty
                        <tr>
                            <td class="center" colspan="6">Hiện không có dữ liệu</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                @include('cpanel.template.pagination-without-number',['page'=>$page])
            </div>
            <div class="separator bottom"></div>
            <!-- Modal -->
            <div class="container-fluid">
           
            <div id="myModal" class="modal fade" role="dialog" style="width: 1100px;margin-left: -600px">
            
                  <div class="modal-dialog modal-lg">
                      <form action="{{action('GroupPermissionController@updatePermission')}}" method="post">
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Chỉnh sửa nhóm quyền</h4>
                      </div>
                      <div class="modal-body">
                        @if(hasAnyRole(UPDATE_GROUP_PERMISSION))
                <div class="row-fluid" id="info">

                    <div class="row-fluid">
                    <div class="span4" style="margin-left: 30px">
                        <label for="groupPermissionName">Tên nhóm quyền</label>
                        <input type="text" name="groupPermissionName" id="groupPermissionName">
                    </div>

                    <div class="span4" style="margin-left: 100px">
                        <label for="groupPermissionDescription">Mô tả nhóm quyền</label>
                    <input type="text" name="groupPermissionDescription" id="groupPermissionDescription">
                    </div>
                </div>

                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="groupPermissionId" id="groupPermissionId">
                    @foreach($reponse_list_permit as $groupPermission)
                        @if(!empty(array_values($groupPermission)[0]))
                            <div class="span5">
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse"
                                           href="#{{array_keys($groupPermission)[0]}}">{{$labelCategory[array_keys($groupPermission)[0]]}}
                                        </a>
                                    </div>
                                    <div id="{{array_keys($groupPermission)[0]}}" class="accordion-body collapse in">
                                        <div class="accordion-inner">

                                            <ul class="unstyled">
                                                @foreach(array_values($groupPermission)[0] as $permission)
                                                    <li>
                                                        <input type="checkbox" class="editFunc" name="listFuncCode[]"
                                                               id="{{$permission['funcId']}}"
                                                               value="{{$permission['funcCode']}}">
                                                        <label for="{{$permission['funcId']}}">{{$permission['funcName']}}</label>
                                                    </li>
                                                @endforeach
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach

                </div>
                <script type="text/javascript">
                    var listGroupPermission = {!!json_encode($result)!!}
                    $(document).ready(function(){
                        $('#disable-form').click(function () {
                            $('#myModal').modal('hide');
                        });
                        $('.editPermission').click(function(){
                            $(".listFuncCode").prop('checked', false);
                            var groupId = $(this).data('id');
                            e = $.grep(listGroupPermission,function (v,k) {
                                return v.groupPermissionId === groupId;
                            })[0];
                            $('#groupPermissionId').val(e.groupPermissionId);
                            $('#groupPermissionName').val(e.groupPermissionName);
                            $('#groupPermissionDescription').val(e.groupPermissionDescription);
                            $('.editFunc').prop('checked',false);
                            $.each(e.listFunction, function (key, value) {
                                $('#' + value.funcId).prop('checked', true);
                            });
                            $('#info').show();
                            $('#submit-form').show();
                        });
                    })
                </script>
            @endif
                      </div>
                      <div class="modal-footer">
                          <div class="clearfix"></div>
                          <div class="control-box" id="submit-form">
                              <div class="row-fluid">
                                  <div class="span5">
                                      <button type="submit" class="btn btn-warning btn-flat-full">LƯU</button>
                                  </div>

                                  <div class="span2">
                                      <button id="disable-form" type="button" class="btn btn-default btn-flat-full">HỦY</button>
                                  </div>
                              </div>
                              <div class="clearfix"></div>
                          </div>
                      </div>
                    </div>

                      </form>
                  </div>
                </div>

            <!-- end Modal -->
        </div>
    </div>
</div>

    @if(hasAnyRole(DELETE_GROUP_PERMISSION))
        <div class="modal hide fade" id="modal_delete" style="width: 250px; margin-left: -10%;margin-top: 10%;">
            <div class="modal-body center">
                <p>Bạn có chắc muốn xoá?</p>
            </div>
            <form action="{{action('GroupPermissionController@delete')}}" method="post">
                <div class="modal-footer">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="groupPermissionId" value="" id="delete_group_id">
                    <a style="cursor: pointer" data-dismiss="modal" aria-hidden="true" class="btn_huy_modal">HỦY</a>
                    <button class="btn btn btn-warning btn-flat">ĐỒNG Ý</button>
                </div>
            </form>
        </div>
    @endif

    <!-- End Content -->


@endsection