$(document).ready(function () {
    var d = new Date();
    var date = d.getDate() + '-' + (parseInt(d.getMonth()) + 1) + '-' + d.getFullYear();
    $(".export-btn a#excel").click(function () {
        var filename = $(this).attr('data-fileName');
        $("#tb_report").table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: filename + '_' + date,
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });

        // xuất excel hành khách cho inter
        $("#table-list").table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: filename + '_' + date,
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    });

    $(".export-btn a#pdf").click(function () {
        var filename = $(this).attr('data-fileName');
      
        var doc = jsPDF('p', 'pt', 'letter');

         doc.addHTML($('#tb_report')[0], 5, 15, function () {
             doc.save(filename + '_' + date);
         });

    });
});
