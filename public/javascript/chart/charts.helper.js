/* ==========================================================
 * AdminPlus v2.0
 * charts.helper.js
 * 
 * http://www.mosaicpro.biz
 * Copyright MosaicPro
 *
 * Built exclusively for sale @Envato Marketplaces
 * ========================================================== */
var primaryColor = '#4a8bc2',
    dangerColor = '#b55151',
    successColor = '#609450',
    warningColor = '#ab7a4b',
    inverseColor = '#202020',
	themerPrimaryColor = '#da5500';


var charts = 
{
	// init charts on Charts page
	initCharts: function()
	{
		/*báo cáo theo tháng*/
		this.chart_lines_fill_nopoints.init();
        /*Báo cáo công nợ*/
        this.cong_no.init();
        /*Báo cáo theo ngày*/
        this.chart_by_date.init();
        /*Báo cáo theo tuần*/
        this.chart_week.init();

	},

	// utility class
	utility:
	{
		chartColors: [ themerPrimaryColor, "#444", "#777", "#999", "#DDD", "#EEE" ],
		chartBackgroundColors: ["transparent", "transparent"],

		applyStyle: function(that)
		{
			that.options.colors = charts.utility.chartColors;
			that.options.grid.backgroundColor = { colors: charts.utility.chartBackgroundColors };
			that.options.grid.borderColor = charts.utility.chartColors[0];
			that.options.grid.color = charts.utility.chartColors[0];
		}
	},
    // lines chart with fill & without points
    chart_lines_fill_nopoints:
        {
            // chart data
            data:
                {
                    d1: charts_data.data.d1,
                    d2: charts_data.data.d2
                },

            // will hold the chart object
            plot: null,

            // chart options
            options:
                {
                    grid: {
                        show: true,
                        aboveData: true,
                        color: "#3f3f3f",
                        labelMargin: 5,
                        axisMargin: 0,
                        borderWidth: 0,
                        borderColor:null,
                        minBorderMargin: 5 ,
                        clickable: true,
                        hoverable: true,
                        autoHighlight: true,
                        mouseActiveRadius: 20,
                        backgroundColor : { }
                    },
                    series: {
                        grow: {active:false},
                        lines: {
                            show: true,
                            fill: true,
                            lineWidth: 2,
                            steps: false
                        },
                        points: {show:false}
                    },
                    legend: { position: "nw", backgroundColor: null, backgroundOpacity: 0 },
                    yaxis: { min: 0,tickFormatter:function (number) {
                        return DOCSO.doc(number);
                    } },
                    xaxis: {mode: "time",
                        tickLength: 5,timezone: "Asia/Ho_Chi_Minh"},

                    colors: [],
                    shadowSize:1,
                    tooltip: true,
                    tooltipOpts: {
                        content: "%s: %y",
                        shifts: {
                            x: -30,
                            y: -50
                        },
                        defaultTheme: false
                    }
                },

            // initialize
            init: function()
            {
                // apply styling
                charts.utility.applyStyle(this);


                // make chart
                this.plot = $.plot(
                    '#chart_lines_fill_nopoints',
                    [{
                        label: "Số lượt tìm kiếm",
                        data: this.data.d1,
                        lines: {fillColor: "rgba(0,0,0,0.01)"},
                        points: {fillColor: "#88bbc8"}
                    },
                        {
                            label: "Số vé đã đặt",
                            data: this.data.d2,
                            lines: {fillColor: "rgba(0,0,0,0.1)"},
                            points: {fillColor: "#ed7a53"}
                        }],
                    this.options);
            }
        },
    chart_week:
        {
            // chart data
            data:
                {
                    d1: charts_data.data.d1,
                    d2: charts_data.data.d2
                },

            // will hold the chart object
            plot: null,

            // chart options
            options:
                {
                    grid: {
                        show: true,
                        aboveData: true,
                        color: "#3f3f3f",
                        labelMargin: 5,
                        axisMargin: 0,
                        borderWidth: 0,
                        borderColor:null,
                        minBorderMargin: 5 ,
                        clickable: true,
                        hoverable: true,
                        autoHighlight: true,
                        mouseActiveRadius: 20,
                        backgroundColor : { }
                    },
                    series: {
                        grow: {active:false},
                        lines: {
                            show: true,
                            fill: true,
                            lineWidth: 2,
                            steps: false
                        },
                        points: {show:false}
                    },
                    legend: { position: "nw", backgroundColor: null, backgroundOpacity: 0 },
                    yaxis: { min: 0,tickFormatter:function (number) {
                        return DOCSO.doc(number);
                    } },
                    xaxis: {ticks: [[0,'Thứ 2'],[1,'Thứ 2'],[2,'Thứ 3'],[3,'Thứ 4'],[4,'Thứ 5'],[5,'Thứ 6'],[6,'Thứ 7'],[7,'CN']]},
                    colors: [],
                    shadowSize:1,
                    tooltip: true,
                    tooltipOpts: {
                        content: "%s: %y",
                        shifts: {
                            x: -30,
                            y: -50
                        },
                        defaultTheme: false
                    }
                },

            // initialize
            init: function()
            {
                // apply styling
                charts.utility.applyStyle(this);


                // make chart
                this.plot = $.plot(
                    '#chart_week',
                    [{
                        label: "Lợi nhuận",
                        data: this.data.d1,
                        lines: {fillColor: "rgba(0,0,0,0.01)"},
                        points: {fillColor: "#88bbc8"}
                    },
                        {
                            label: "Doanh thu",
                            data: this.data.d2,
                            lines: {fillColor: "rgba(0,0,0,0.1)"},
                            points: {fillColor: "#ed7a53"}
                        }],
                    this.options);
            }
        },
    chart_day:
        {
        },
    cong_no:
        {
            // chart data
            data:
                {
                    d1: charts_data.data.d1,
                    d2: charts_data.data.d2
                },

            // will hold the chart object
            plot: null,

            // chart options
            options:
                {
                    grid: {
                        show: true,
                        aboveData: true,
                        color: "#3f3f3f",
                        labelMargin: 10,
                        axisMargin: 0,
                        borderWidth: 0,
                        borderColor:null,
                        minBorderMargin: 5 ,
                        clickable: true,
                        hoverable: true,
                        autoHighlight: true,
                        mouseActiveRadius: 20,
                        backgroundColor : { }
                    },
                    series: {
                        grow: {active:false},
                        lines: {
                            show: true,
                            fill: true,
                            lineWidth: 2,
                            steps: false
                        },
                        points: {show:false}
                    },
                    legend: { position: "nw", backgroundColor: null, backgroundOpacity: 0 },
                    yaxis: { min: 0 },
                    xaxis: {mode: "time" /*ticks: 11, tickDecimals:0*/},
                    colors: [],
                    shadowSize:1,
                    tooltip: true,
                    tooltipOpts: {
                        content: "%s : %y.0",
                        shifts: {
                            x: -30,
                            y: -50
                        },
                        defaultTheme: false
                    }
                },

            // initialize
            init: function()
            {
                // apply styling
                charts.utility.applyStyle(this);


                // make chart
                this.plot = $.plot(
                    '#cong_no',
                    [{
                        label: "Tổng tiền :"+ moneyFormat(this.data.d1),
                        data: this.data.d1,
                        lines: {fillColor: "rgba(0,0,0,0.01)"},
                        points: {fillColor: "#88bbc8"}
                    },
                        {
                            label: "Cần thu",
                            data: this.data.d2,
                            column: {fillColor: "rgba(0,0,0,0.1)"},
                            points: {fillColor: "#ed7a53"}
                        }],
                    this.options);
            }
        },
    chart_by_date:
        {
            // chart data
            data:
                {
                    d1: charts_data.data.d1,
                    d2: charts_data.data.d2
                },

            // will hold the chart object
            plot: null,

            // chart options
            options:
                {
                    grid: {
                        show: true,
                        aboveData: true,
                        color: "#3f3f3f",
                        labelMargin: 5,
                        axisMargin: 0,
                        borderWidth: 0,
                        borderColor:null,
                        minBorderMargin: 5 ,
                        clickable: true,
                        hoverable: true,
                        autoHighlight: true,
                        mouseActiveRadius: 20,
                        backgroundColor : { }
                    },
                    series: {
                        grow: {active:false},
                        lines: {
                            show: true,
                            fill: true,
                            lineWidth: 2,
                            steps: false
                        },
                        points: {show:false}
                    },
                    legend: { position: "nw", backgroundColor: null, backgroundOpacity: 0 },
                    yaxis: { min: 0},
                    xaxis:  {mode: "time",
                        tickLength: 5,timezone: "Asia/Ho_Chi_Minh"},
                    colors: [],
                    shadowSize:1,
                    tooltip: true,
                    tooltipOpts: {
                        content: "%s : %y.0",
                        shifts: {
                            x: -30,
                            y: -50
                        },
                        defaultTheme: false
                    }
                },

            // initialize
            init: function()
            {
                // apply styling
                charts.utility.applyStyle(this);


                // make chart
                this.plot = $.plot(
                    '#chart_by_date',
                    [{
                        label: "Số vé",
                        data: this.data.d1,
                        lines: {fillColor: "rgba(0,0,0,0.01)"},
                        points: {fillColor: "#88bbc8"}
                    },
                    {
                        label: "Doanh thu",
                        data: this.data.d2,
                        lines: {fillColor: "rgba(0,0,0,0.1)"},
                        points: {fillColor: "#ed7a53"}
                    }],
                    this.options);
            }
        }
};