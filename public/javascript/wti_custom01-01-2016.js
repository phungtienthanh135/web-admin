wti.func = {
	conf:{		
		variable:{sumprice:0,sumqty:0},
		steps:{step:0}
	},
	/*------------------ Load lich khoi hanh--------------*/
	datve:{
		xekhoihanh:function(selectedDate, tuyenduong){
			wti.ajax_load_data('xekhoihanh.ajax',"POST",{selectedDate:selectedDate, tuyenduong:tuyenduong},
			function (data) {
				$(".danhsachghe").html("");
				$(".xekhoihanh").html(data);
			});
		},
		danhsachghe:function( lichchay_id ){
			wti.ajax_load_data('danhsachghe.ajax',"POST",{lichchay_id:lichchay_id},
			function (data) {
				$(".danhsachghe").html(data);
				$('#print_sodokhach').prop('href', './print/sodokhach/'+lichchay_id+'.html');
				$('#print_lenhxuatben').prop('href', './print/lenhxuatben/'+lichchay_id+'.html');
			});
		},
		datve:function(){
			// datve value
			var datve_hoten			= wti.util_trim(jQuery('#datve_hoten').val());
			var datve_sodienthoai	= wti.util_trim(jQuery('#datve_sodienthoai').val());
			var datve_diemdi		= wti.util_trim(jQuery('#datve_diemdi').val());
			var datve_diemden		= wti.util_trim(jQuery('#datve_diemden').val());
			// datve chi tiet value
			var chitiet_kyhieuve	= $('input[name="chitiet_kyhieuve[]"]').map(function(){return $(this).val();}).get();
			var chitiet_giave		= $('input[name="chitiet_giave[]"]').map(function(){return $(this).val();}).get();			
			var chitiet_hoten		= $('input[name="chitiet_hoten[]"]').map(function(){return $(this).val();}).get();			
			var chitiet_sodienthoai	= $('input[name="chitiet_sodienthoai[]"]').map(function(){return $(this).val();}).get();
			var chitiet_namsinh		= $('input[name="chitiet_namsinh[]"]').map(function(){return $(this).val();}).get();
			var chitiet_diemdi		= $('select[name="chitiet_diemdi[]"]').map(function(){return $(this).val();}).get();
			var chitiet_diemden		= $('select[name="chitiet_diemden[]"]').map(function(){return $(this).val();}).get();
			var chitiet_dcdon		= $('input[name="chitiet_dcdon[]"]').map(function(){return $(this).val();}).get();
			var chitiet_dctrungchuyen = $('input[name="chitiet_dctrungchuyen[]"]').map(function(){return $(this).val();}).get();
			var chitiet_ghichu 		  = $('input[name="chitiet_ghichu[]"]').map(function(){return $(this).val();}).get();
			
			var chitiet_soghe 		= $('input[name="chitiet_soghe[]"]').map(function(){return $(this).val();}).get();
			var chitiet_soghe_id 	= $('input[name="chitiet_soghe_id[]"]').map(function(){return $(this).val();}).get();
			
			var xekhoihanhid 		= wti.util_trim(jQuery('#xekhoihanhid').val());	

			if(datve_sodienthoai == ""){				
				bootbox.alert("Số điện thoại không được bỏ trống !", function(result) { return; });
				return false;				
			} else if(datve_hoten == "") {
				bootbox.alert("Họ tên không được bỏ trống !", function(result) { return; });
				return false;
			} else if(datve_diemdi == 0) {
				bootbox.alert("Vui lòng chọn điểm đi !", function(result) { return; });
				return false;
			} else if(datve_diemden == 0) {
				bootbox.alert("Vui lòng chọn điểm đến !", function(result) { return; });
				return false;
			} else if(jQuery('#chitiet_dcdon1').val() == "") {
				bootbox.alert("Vui lòng nhập điểm đón khách cho ghế "+ jQuery('#chitiet_soghe1').val() +" !", function(result) { return; });
				return false;
			} else {

				var dataString = {act: "datve.add", datve_sodienthoai: datve_sodienthoai, 
									datve_hoten: datve_hoten, 
									datve_diemdi: datve_diemdi, 
									datve_diemden: datve_diemden,
									chitiet_hoten: chitiet_hoten, 
									chitiet_sodienthoai: chitiet_sodienthoai, 
									chitiet_namsinh: chitiet_namsinh, 
									chitiet_dcdon: chitiet_dcdon, 
									chitiet_dctrungchuyen: chitiet_dctrungchuyen, 
									chitiet_soghe_id: chitiet_soghe_id, 
									chitiet_kyhieuve: chitiet_kyhieuve, 
									chitiet_soghe: chitiet_soghe, 
									chitiet_giave: chitiet_giave,
									chitiet_diemdi: chitiet_diemdi,
									chitiet_diemden: chitiet_diemden,
									chitiet_dcdon: chitiet_dcdon,
									chitiet_dctrungchuyen: chitiet_dctrungchuyen,
									chitiet_ghichu: chitiet_ghichu,
									xekhoihanhid: xekhoihanhid};
									
				wti.ajax_popup('datve.ajax', "POST", dataString, function (data) {
					if (data.err == 0 && data.msg == 'success') {						
						//bootbox.alert("Yêu cầu đặt vé đã được lưu !", function(result) { return; });
						wti.func.datve.danhsachghe($("#xekhoihanhid").val());
						wti.func.datve.editFrmDatve(data.ticketid);
					} else {						
						bootbox.alert("Đặt vé không thực hiện được !", function(result) { return; });
					}
				});

			}
		},
		dieuchinhve:function(){
			
			// datve value
			var datve_hoten			  = wti.util_trim(jQuery('#datve_hoten').val());
			var datve_sodienthoai	  = wti.util_trim(jQuery('#datve_sodienthoai').val());
			var datve_diemdi		  = wti.util_trim(jQuery('#datve_diemdi').val());
			var datve_diemden		  = wti.util_trim(jQuery('#datve_diemden').val());
			// datve chi tiet value
			var chitiet_kyhieuve	  = $('input[name="chitiet_kyhieuve[]"]').map(function(){return $(this).val();}).get();
			var chitiet_giave		  = $('input[name="chitiet_giave[]"]').map(function(){return $(this).val();}).get();			
			var chitiet_hoten		  = $('input[name="chitiet_hoten[]"]').map(function(){return $(this).val();}).get();			
			var chitiet_sodienthoai	  = $('input[name="chitiet_sodienthoai[]"]').map(function(){return $(this).val();}).get();
			var chitiet_namsinh		  = $('input[name="chitiet_namsinh[]"]').map(function(){return $(this).val();}).get();
			var chitiet_diemdi		  = $('select[name="chitiet_diemdi[]"]').map(function(){return $(this).val();}).get();
			var chitiet_diemden		  = $('select[name="chitiet_diemden[]"]').map(function(){return $(this).val();}).get();
			var chitiet_dcdon		  = $('input[name="chitiet_dcdon[]"]').map(function(){return $(this).val();}).get();
			var chitiet_dctrungchuyen = $('input[name="chitiet_dctrungchuyen[]"]').map(function(){return $(this).val();}).get();
			var chitiet_ghichu 		  = $('input[name="chitiet_ghichu[]"]').map(function(){return $(this).val();}).get();
			
			var Vechitiet_id 		  = $('input[name="Vechitiet_id[]"]').map(function(){return $(this).val();}).get();
			var Khach_id 		   	  = $('input[name="Khach_id[]"]').map(function(){return $(this).val();}).get();
			
			if(datve_sodienthoai == ""){				
				bootbox.alert("Số điện thoại không được bỏ trống !", function(result) { return; });
				return false;				
			} else if(datve_hoten == "") {
				bootbox.alert("Họ tên không được bỏ trống !", function(result) { return; });
				return false;
			} else if(datve_diemdi == 0) {
				bootbox.alert("Vui lòng chọn điểm đi !", function(result) { return; });
				return false;
			} else if(datve_diemden == 0) {
				bootbox.alert("Vui lòng chọn điểm đến !", function(result) { return; });
				return false;
			} else if(jQuery('#chitiet_dcdon1').val() == "") {
				bootbox.alert("Vui lòng nhập điểm đón khách cho ghế "+ jQuery('#chitiet_soghe1').val() +" !", function(result) { return; });
				return false;
			} else {

				var dataString = {act: "datve.edit", datve_sodienthoai: datve_sodienthoai, 
									datve_hoten: datve_hoten, 
									datve_diemdi: datve_diemdi, 
									datve_diemden: datve_diemden,
									chitiet_hoten: chitiet_hoten, 
									chitiet_sodienthoai: chitiet_sodienthoai, 
									chitiet_namsinh: chitiet_namsinh, 
									chitiet_dcdon: chitiet_dcdon, 
									chitiet_dctrungchuyen: chitiet_dctrungchuyen,
									chitiet_kyhieuve: chitiet_kyhieuve, 
									chitiet_giave: chitiet_giave,
									chitiet_diemdi: chitiet_diemdi,
									chitiet_diemden: chitiet_diemden,
									chitiet_dcdon: chitiet_dcdon,
									chitiet_dctrungchuyen: chitiet_dctrungchuyen,
									chitiet_ghichu: chitiet_ghichu,
									Vechitiet_id: Vechitiet_id,
									Khach_id: Khach_id};
									
				wti.ajax_popup('datve.ajax', "POST", dataString, function (data) {
					if (data.err == 0 && data.msg == 'success') {						
						bootbox.alert("Yêu cầu chỉnh sữa vé đã được lưu !", function(result) { return; });
						wti.func.datve.danhsachghe($("#xekhoihanhid").val());
						wti.func.datve.editFrmDatve(data.ticketid);
					} else {						
						bootbox.alert("Chỉnh sữa vé không thực hiện được !", function(result) { return; });
					}
				});
				
			}
		},
		banve:function(){
			
			var sodienthoai		= wti.util_trim(jQuery('#sodienthoai').val());
			var hoten			= wti.util_trim(jQuery('#hoten').val());
			var diemdi			= wti.util_trim(jQuery('#diemdi').val());
			var diemden			= wti.util_trim(jQuery('#diemden').val());
			var dcdon			= wti.util_trim(jQuery('#dcdon').val());
			var dctrungchuyen	= wti.util_trim(jQuery('#dctrungchuyen').val());
			
			var ghichu			= wti.util_trim(jQuery('#ghichu').val());			
			
			var ghexe_id		= wti.util_trim(jQuery('#ghexe_id').val());
			var soghe			= wti.util_trim(jQuery('#soghe').val());
			var giave			= wti.util_trim(jQuery('#giave').val());
			
			var xekhoihanhid 	= wti.util_trim(jQuery('#xekhoihanhid').val());
			
			if(sodienthoai == ""){			
				bootbox.alert("Số điện thoại không được bỏ trống !", function(result) { return; });
				return false;
			} else if(hoten == "") {
				bootbox.alert("Họ tên không được bỏ trống !", function(result) { return; });
				return false;
			} else if(diemdi == 0) {
				bootbox.alert("Vui lòng chọn điểm đi !", function(result) { return; });
				return false;
			} else if(diemden == 0) {
				bootbox.alert("Vui lòng chọn điểm đến !", function(result) { return; });
				return false;
			} else {
				var dataString = {act: "datve.banve", sodienthoai: sodienthoai, hoten: hoten, diemdi: diemdi, diemden: diemden,
									dcdon: dcdon, dctrungchuyen: dctrungchuyen, ghichu: ghichu, ghexe_id: ghexe_id, soghe: soghe,
									giave: giave, xekhoihanhid: xekhoihanhid};
									
				wti.ajax_popup('datve.ajax', "POST", dataString, function (data) {
					if (data.err == 0 && data.msg == 'success') {						
						bootbox.alert("Yêu cầu đặt vé đã được lưu !", function(result) { return; });
						wti.func.datve.danhsachghe($("#xekhoihanhid").val());
						wti.func.datve.editFrmDatve(data.ve_id);
					} else {						
						bootbox.alert("Đặt vé không thực hiện được !", function(result) { return; });
					}
				});
			}
		},		
		xuatve:function(){
						
			var ve_id			= wti.util_trim(jQuery('#ve_id').val());			
			var kyhieuve		= wti.util_trim(jQuery('#kyhieuve').val());			
			
			if(kyhieuve == ""){			
				bootbox.alert("ký hiệu vé không được bỏ trống !", function(result) { return; });
				return false;
			} else {
				var dataString = {act: "datve.xuatve", kyhieuve:kyhieuve, ve_id:ve_id};
									
				wti.ajax_popup('datve.ajax', "POST", dataString, function (data) {
					if (data.err == 0 && data.msg == 'success') {						
						bootbox.alert("Yêu cầu xuất vé đã được lưu !", function(result) { return; });
						wti.func.datve.danhsachghe($("#xekhoihanhid").val());
						wti.func.datve.editFrmDatve(data.ve_id);
					} else {						
						bootbox.alert("Xuất vé không thực hiện được !", function(result) { return; });
					}
				});
			}
		},
		frmdatve:function( ticketid ){
			wti.ajax_load_data('frmdatve.ajax',"POST",{ticketid: ticketid},
			function (data) {
				//wti.func.datve.danhsachghe($("#xekhoihanhid").val());
				$(".thongtinve").html(data);
			});
		},
		newFrmDatve:function( Ghexe_id, Soghe, price ){
			wti.ajax_popup('frmdatve.ajax',"POST",{act: "frmdatve.new", Ghexe_id: Ghexe_id},
			function (data) {
				if (data.err == 0 && data.msg == 'success') {
					
					wti.func.conf.steps.step += 1;
					
					var optValues = data.diemdi;
					var optValue  = optValues.split(',');
					
					var tab_head = wti.join
					('<li><a class="glyphicons car" href="#chair-tab'+ Ghexe_id +'" data-toggle="tab"><i></i>'+ Soghe +'</a></li>')();
					
					var tab_content = wti.join
					('<div class="tab-pane" id="chair-tab'+ Ghexe_id +'">')
						('<div class="span12">')							
							('<div class="control-group span7">')
								('<label class="control-label control-label-fix" for="chitiet_kyhieuve">Ký hiệu vé: </label>')
								('<div class="controls controls-fix">')
									('<input class="span12" id="chitiet_kyhieuve'+wti.func.conf.steps.step+'" name="chitiet_kyhieuve[]" type="text" value="" />')
								('</div>')
							('</div>')
							('<div class="control-group span5">')
								('<label class="control-label control-label-fix" style="width:55px;margin-left:10px;" for="chitiet_giave">Giá vé: </label>')
								('<div class="controls controls-fix" style="margin-left:75px;">')
									('<input class="span12" style="width: 147px;" id="chitiet_giave'+wti.func.conf.steps.step+'" name="chitiet_giave[]" type="text" readonly value="'+ wti.numberFormat(data.gia) +'" />')
								('</div>')
							('</div>')
							('<div class="control-group">')
								('<label class="control-label control-label-fix" for="chitiet_hoten">Họ tên: <span class="required">*</span></label>')
								('<div class="controls controls-fix">')
									('<input class="span12" id="chitiet_hoten'+wti.func.conf.steps.step+'" name="chitiet_hoten[]" type="text" value="" />')
								('</div>')
							('</div>')
							('<div class="control-group span7">')
								('<label class="control-label control-label-fix" for="chitiet_sodienthoai">Số ĐT: <span class="required">*</span></label>')
								('<div class="controls controls-fix">')
								   (' <input class="span12 intformat" id="chitiet_sodienthoai'+wti.func.conf.steps.step+'" name="chitiet_sodienthoai[]" maxlength="11" type="text" value="" />')
								('</div>')
							('</div>')
							('<div class="control-group span5">')
								('<label class="control-label control-label-fix" style="width:60px;margin-left:10px;" for="chitiet_namsinh">Năm sinh: </label>')
								('<div class="controls controls-fix" style="margin-left:75px;">')
									('<input class="span12" style="width: 147px;" id="chitiet_namsinh'+wti.func.conf.steps.step+'" name="chitiet_namsinh[]" type="text" value="" />')
								('</div>')
							('</div>')
							('<div class="control-group span7">')
								('<label class="control-label control-label-fix" style="width:65px;margin-left:5px;" for="chitiet_diemdi">Đi từ: <span class="required">*</span></label>')
								('<div class="controls controls-fix">')
									('<select name="chitiet_diemdi[]" id="chitiet_diemdi'+wti.func.conf.steps.step+'" class="span12">')
									('<option value="0">Chọn điểm đi</option>')();
									for (key in optValue) {
									  if (typeof (optValue[key]) == 'string') {
										var items = optValue[key].split(':');
										tab_content += '<option value="'+items[0]+'">'+items[1]+'</option>';
									  }
									}
									tab_content += wti.join
									('</select>')
								('</div>')
							('</div>')
							('<div class="control-group span5">')
								('<label class="control-label control-label-fix" style="width:40px;margin-left:5px;" for="chitiet_diemden">Đến: <span class="required">*</span></label>')
								('<div class="controls controls-fix" style=" margin-left: 45px;">')
									('<select name="chitiet_diemden[]" id="chitiet_diemden'+wti.func.conf.steps.step+'" class="span12" style="width: 177px;">')
									('<option value="0">Chọn điểm đến</option>')();									
									for (key in optValue) {
									  if (typeof (optValue[key]) == 'string') {
										var items = optValue[key].split(':');
										tab_content += '<option value="'+items[0]+'">'+items[1]+'</option>';
									  }
									}
									tab_content += wti.join
									('</select>')
								('</div>')
							('</div>')
							('<div class="control-group">')
								('<label class="control-label control-label-fix" for="chitiet_dcdon">Điểm đón: </label>')
								('<div class="controls controls-fix">')
									('<input class="span12" id="chitiet_dcdon'+wti.func.conf.steps.step+'" name="chitiet_dcdon[]" type="text" value=""/>')
								('</div>')
							('</div>')
							('<div class="control-group">')
								('<label class="control-label control-label-fix" for="chitiet_dctrungchuyen">Trung chuyễn: </label>')
								('<div class="controls controls-fix">')
									('<input class="span12" id="chitiet_dctrungchuyen'+wti.func.conf.steps.step+'" name="chitiet_dctrungchuyen[]" type="text" value=""/>')
								('</div>')
							('</div>')
							('<div class="control-group">')
								('<label class="control-label control-label-fix" for="chitiet_ghichu">Ghi chú: </label>')
								('<div class="controls controls-fix">')
									('<input class="span12" id="chitiet_ghichu'+wti.func.conf.steps.step+'" name="chitiet_ghichu[]" type="text" value=""/>')
								('</div>')
							('</div>')
						('</div>')
					('</div>')
					('<input id="chitiet_soghe'+wti.func.conf.steps.step+'" name="chitiet_soghe[]" type="hidden" value="'+ Soghe +'"/>')
					('<input id="chitiet_soghe_id'+wti.func.conf.steps.step+'" name="chitiet_soghe_id[]" type="hidden" value="'+ Ghexe_id +'"/>')();
					
					var recTab_head = tab_head;
					var recTab_content = tab_content;
					
					if( $('.chair_tab_head ul li.default').length > 0 ){						
						$('.chair_tab_head ul').html(recTab_head);
						$('.chair_tab_content').html(recTab_content);
						$('.chair_tab_head ul li').addClass("active");
						$('.chair_tab_content').find(".tab-pane").addClass("active");
						wti.func.datve.clearForm();
					} else {
						//$('.chair_tab_head ul li').removeClass("active");
						//$('.chair_tab_content').find(".tab-pane").removeClass("active");						
						$('.chair_tab_head ul').append(recTab_head);
						$('.chair_tab_content').append(recTab_content);							
					}
										
					wti.func.conf.variable.sumprice = Math.abs(parseInt( wti.func.conf.variable.sumprice ) + parseInt(data.gia));
					wti.func.conf.variable.sumqty   += 1;
					
					$('#tongtienve').html(wti.numberFormat(wti.func.conf.variable.sumprice) + " VNĐ" + " ("+ wti.func.conf.variable.sumqty +" vé)");
					
					$('#btn_group').html('<button onclick="wti.func.datve.datve();" id="btn_datve" type="button" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Đặt vé</button>&nbsp;&nbsp;<button onclick="wti.func.datve.banve();" id="btn_banve" type="button" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Bán vé</button>&nbsp;&nbsp;');				
					
				}  else {						
					bootbox.alert("Không kết nối được dữ liệu !", function(result) { return; });
				}
			});			
		},
		editFrmDatve:function( Datve_id ){
			wti.ajax_load_data('frmdatve.ajax',"POST",{act: "frmdatve.edit", Datve_id: Datve_id},
			function (data) {
				if (data != "") {
					wti.func.conf.variable.sumprice = 0;
					wti.func.conf.variable.sumqty	= 0;
					wti.func.conf.steps.step 		= 0;
					$(".thongtinve").html(data);					
				}  else {						
					bootbox.alert("Không kết nối được dữ liệu !", function(result) { return; });
				}
			});			
		},
		huyve:function( veid ){			
			bootbox.confirm("Bạn có chắc chắn hủy vé được chọn hay không !", function(result)
			{
				if(result){						
					wti.ajax_popup('datve.ajax',"POST",{act: "datve.remove", veid:veid},
					function (data) {
						if (data.err == 0 && data.msg == 'success') {
							wti.func.datve.danhsachghe($("#xekhoihanhid").val());
							$(".thongtinve").html("<strong>Chọn vé cần đặt</strong>");
						} else {						
							bootbox.alert("Hủy vé không thực hiện được !", function(result) { return; });
						}
					});	
				}
			});						
		},
		clearForm:function (){			
			$('#datve_sodienthoai, #datve_hoten').val('');
			$('#frmdatve checkbox, :radio').prop('checked', false);
			
			$('#frmdatve input, select').not(':button, :submit, :reset, :hidden, :checkbox, :radio').prop("disabled",false);
			
			if(jQuery('#datve_diemdi').val() != 0){ $('#datve_diemdi').prepend('<option selected val="0">Chọn điểm đi</option>'); }
			if(jQuery('#datve_diemden').val() != 0){ $('#datve_diemden').prepend('<option selected val="0">Chọn điểm đến</option>'); }				
		}
	}
};

$('.moneyformat').keyup(function() {
	$(this).formatCurrency({ symbol: '', colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 0 });
}).keypress(function(e) {
	if (String.fromCharCode(e.charCode).match(/[^0-9]/g)) return false;
});
$('.intformat').keypress(function(e) {
	if (String.fromCharCode(e.charCode).match(/[^0-9]/g)) return false;
});

function func_notyfy(layout, type, message){
		
	var notification = []; notification[type] = message;							
							
	var self = $(this);	
	var data_layout = layout;
	var data_type 	= type;	// success | alert | error | warning | information | confirm

	notyfy({
		text: notification[data_type],
		type: data_type,
		dismissQueue: true,
		layout: data_layout,
		buttons: (data_type != 'confirm') ? false : [{
			addClass: 'btn btn-success btn-small btn-icon glyphicons ok_2',
			text: '<i></i> Ok',
			onClick: function ($notyfy) {
				$notyfy.close();
				notyfy({
					force: true,
					text: 'You clicked "<strong>Ok</strong>" button',
					type: 'success',
					layout: data_layout
				});
			}
		}, {
			addClass: 'btn btn-danger btn-small btn-icon glyphicons remove_2',
			text: '<i></i> Cancel',
			onClick: function ($notyfy) {
				$notyfy.close();
				notyfy({
					force: true,
					text: '<strong>You clicked "Cancel" button<strong>',
					type: 'error',
					layout: data_layout
				});
			}
		}]
	});
	return false;
}