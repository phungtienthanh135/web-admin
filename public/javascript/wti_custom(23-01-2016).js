wti.func = {
	conf:{		
		variable:{sumprice:0,sumqty:0},
		steps:{step:0}
	},
	/*------------------ Load lich khoi hanh--------------*/
	datve:{
		xekhoihanh:function(selectedDate, tuyenduong){
			wti.ajax_load_data('xekhoihanh.ajax',"POST",{selectedDate:selectedDate, tuyenduong:tuyenduong},
			function (data) {
				$(".danhsachghe").html("");
				$(".xekhoihanh").html(data);
			});
		},
		danhsachghe:function( lichchay_id, view ){
			wti.ajax_load_data('danhsachghe.ajax',"POST",{lichchay_id:lichchay_id, view:view},
			function (data) {
				$(".danhsachghe").html(data);
			});
		},
		datve:function( tinhtrang ){

			// datve chi tiet value
			var chitiet_kyhieuve	= $('input[name="chitiet_kyhieuve[]"]').map(function(){return $(this).val();}).get().join("|");
			var chitiet_giave		= $('input[name="chitiet_giave[]"]').map(function(){return $(this).val();}).get().join("|");			
			var chitiet_hoten		= $('input[name="chitiet_hoten[]"]').map(function(){return $(this).val();}).get().join("|");			
			var chitiet_sodienthoai	= $('input[name="chitiet_sodienthoai[]"]').map(function(){return $(this).val();}).get().join("|");
			var chitiet_namsinh		= $('input[name="chitiet_namsinh[]"]').map(function(){return $(this).val();}).get().join("|");
			var chitiet_diemdi		= $('select[name="chitiet_diemdi[]"]').map(function(){return $(this).val();}).get().join("|");
			var chitiet_diemden		= $('select[name="chitiet_diemden[]"]').map(function(){return $(this).val();}).get().join("|");
			var chitiet_dcdon		= $('input[name="chitiet_dcdon[]"]').map(function(){return $(this).val();}).get().join("|");
			var chitiet_dctrungchuyen = $('input[name="chitiet_dctrungchuyen[]"]').map(function(){return $(this).val();}).get().join("|");
			var chitiet_dontrenduong = $('input[name="chitiet_dontrenduong[]"]').map(function(){return $(this).val();}).get().join("|");
			var chitiet_ghichu 		  = $('input[name="chitiet_ghichu[]"]').map(function(){return $(this).val();}).get().join("|");
			
			var chitiet_soghe 		= $('input[name="chitiet_soghe[]"]').map(function(){return $(this).val();}).get().join("|");
			var chitiet_soghe_id 	= $('input[name="chitiet_soghe_id[]"]').map(function(){return $(this).val();}).get().join("|");
			
			var xekhoihanhid 		= wti.util_trim(jQuery('#xekhoihanhid').val());	

			if(tinhtrang == 2){		
				var soghe = new Array();
				$("input[name='chitiet_soghe[]']").each(function(){ soghe.push($(this).val()); });
				var i = 0;
				$('input[name="chitiet_kyhieuve[]"]').each(function(){					
					if($(this).val() == ""){
						bootbox.alert("Nhập ký hiệu vé cho ghế "+ soghe[i] +" !", function(result) { return; });
						return false;				
					}
					i++;
				});				
			} 
			
			if($('input[name="chitiet_sodienthoai[]"]').val() == ""){
				bootbox.alert("Số điện thoại không được bỏ trống !", function(result) { return; });
				return false;				
			} else if($('input[name="chitiet_hoten[]"]').val() == "") {
				bootbox.alert("Họ tên không được bỏ trống !", function(result) { return; });
				return false;
			} else if($('input[name="chitiet_diemdi[]"]').val() == 0) {
				bootbox.alert("Vui lòng chọn điểm đi !", function(result) { return; });
				return false;
			} else if($('input[name="chitiet_diemden[]"]').val() == 0) {
				bootbox.alert("Vui lòng chọn điểm đến !", function(result) { return; });
				return false;
			} else if($('input[name="chitiet_dcdon[]"]').val() == "") {
				bootbox.alert("Vui lòng nhập điểm đón khách cho ghế "+ $('input[name="chitiet_soghe[]"]').val() +" !", function(result) { return; });
				return false;
			} else {
				var dataString = {act: "datve.add", chitiet_hoten: chitiet_hoten, 
									chitiet_sodienthoai: chitiet_sodienthoai, 
									chitiet_namsinh: chitiet_namsinh, 
									chitiet_dcdon: chitiet_dcdon, 
									chitiet_dctrungchuyen: chitiet_dctrungchuyen, 
									chitiet_dontrenduong: chitiet_dontrenduong,
									chitiet_soghe_id: chitiet_soghe_id, 
									chitiet_kyhieuve: chitiet_kyhieuve, 
									chitiet_soghe: chitiet_soghe, 
									chitiet_giave: chitiet_giave,
									chitiet_diemdi: chitiet_diemdi,
									chitiet_diemden: chitiet_diemden,
									chitiet_dcdon: chitiet_dcdon,
									chitiet_dctrungchuyen: chitiet_dctrungchuyen,
									chitiet_ghichu: chitiet_ghichu,
									tinhtrang: tinhtrang,									
									xekhoihanhid: xekhoihanhid};
									
				wti.ajax_popup('datve.ajax', "POST", dataString, function (data) {
					if (data.err == 0 && data.msg == 'success') {
						
						var lichchay_id = wti.util_trim(jQuery("#xekhoihanhid").val());
						
						// load lai danh sach ghe
						wti.ajax_load_data('danhsachghe.ajax',"POST",{lichchay_id:lichchay_id, view:0, ticketid:data.ticketid},
						function (data) { $(".danhsachghe").html(data); });						
						wti.func.datve.editFrmDatve(data.ticketid, lichchay_id);
					
						// load lai danh sach ghe
						var data = '{"act":"datve","ticketid":-2,"view":0,"lichchay_id":'+ lichchay_id +',"gheid":-2}';
						socket.emit('messages', data);
						
					} else {						
						bootbox.alert("Đặt vé không thực hiện được !", function(result) { return; });
					}
				});
			}			
		},
		
		xuatve:function(){
			
			// datve value
			var Datve_id	= wti.util_trim(jQuery('#Datve_id').val());
			var ve_id 		= $('input[name="Vechitiet_id[]"]').map(function(){return $(this).val();}).get().join("|");
			var kyhieuve 	= $('input[name="chitiet_kyhieuve[]"]').map(function(){return $(this).val();}).get().join("|");	
			
			var soghe = new Array();
			$("input[name='Soghe_id[]']").each(function(){ soghe.push($(this).val()); });
			var i = 0; var check = true;
			$('input[name="chitiet_kyhieuve[]"]').each(function(){				
				if($(this).val() == ""){
					bootbox.alert("Nhập ký hiệu vé cho ghế "+ soghe[i] +" !", function(result) { return; });
					check = false;
					return false;
				}
				i++;
			});	

			if(check == true){
				var dataString = {act: "datve.xuatve", kyhieuve:kyhieuve, ve_id:ve_id, Datve_id: Datve_id};
										
				wti.ajax_popup('datve.ajax', "POST", dataString, function (data) {
					if (data.err == 0 && data.msg == 'success') {
						bootbox.alert("Yêu cầu xuất vé đã được lưu !", function(result) { return; });
						// load lai danh sach ghe
						wti.ajax_load_data('danhsachghe.ajax',"POST",{lichchay_id:$("#xekhoihanhid").val(), view:0, ticketid:data.ticketid},
						function (data) { $(".danhsachghe").html(data); });
						// load lai form dieu chinh ve
						wti.func.datve.editFrmDatve(data.ticketid, $("#xekhoihanhid").val());
					} else {						
						bootbox.alert("Xuất vé không thực hiện được !", function(result) { return; });
					}
				});
			}

		},
		
		dieuchinhve:function(){
			
			// datve value
			var Datve_id			  = wti.util_trim(jQuery('#Datve_id').val());
			var xekhoihanhid 		  = wti.util_trim(jQuery('#xekhoihanhid').val());
			
			// datve chi tiet value
			var chitiet_kyhieuve	  = $('input[name="chitiet_kyhieuve[]"]').map(function(){return $(this).val();}).get().join("|");
			var chitiet_giave		  = $('input[name="chitiet_giave[]"]').map(function(){return $(this).val();}).get().join("|");			
			var chitiet_hoten		  = $('input[name="chitiet_hoten[]"]').map(function(){return $(this).val();}).get().join("|");			
			var chitiet_sodienthoai	  = $('input[name="chitiet_sodienthoai[]"]').map(function(){return $(this).val();}).get().join("|");
			var chitiet_namsinh		  = $('input[name="chitiet_namsinh[]"]').map(function(){return $(this).val();}).get().join("|");
			var chitiet_diemdi		  = $('select[name="chitiet_diemdi[]"]').map(function(){return $(this).val();}).get().join("|");
			var chitiet_diemden		  = $('select[name="chitiet_diemden[]"]').map(function(){return $(this).val();}).get().join("|");
			var chitiet_dcdon		  = $('input[name="chitiet_dcdon[]"]').map(function(){return $(this).val();}).get().join("|");
			var chitiet_dctrungchuyen = $('input[name="chitiet_dctrungchuyen[]"]').map(function(){return $(this).val();}).get().join("|");
			var chitiet_ghichu 		  = $('input[name="chitiet_ghichu[]"]').map(function(){return $(this).val();}).get().join("|");
			
			var Vechitiet_id 		  = $('input[name="Vechitiet_id[]"]').map(function(){return $(this).val();}).get().join("|");
			var Khach_id 		   	  = $('input[name="Khach_id[]"]').map(function(){return $(this).val();}).get().join("|");							
			
			if($('input[name="chitiet_sodienthoai[]"]').val() == ""){
				bootbox.alert("Số điện thoại không được bỏ trống !", function(result) { return; });
				return false;				
			} else if($('input[name="chitiet_hoten[]"]').val() == "") {
				bootbox.alert("Họ tên không được bỏ trống !", function(result) { return; });
				return false;
			} else if($('input[name="chitiet_diemdi[]"]').val() == 0) {
				bootbox.alert("Vui lòng chọn điểm đi !", function(result) { return; });
				return false;
			} else if($('input[name="chitiet_diemden[]"]').val() == 0) {
				bootbox.alert("Vui lòng chọn điểm đến !", function(result) { return; });
				return false;
			} else if($('input[name="chitiet_dcdon[]"]').val() == "") {
				bootbox.alert("Vui lòng nhập điểm đón khách cho ghế "+ $('input[name="chitiet_soghe[]"]').val() +" !", function(result) { return; });
				return false;
			} else {

				var dataString = {act: "datve.edit", chitiet_hoten: chitiet_hoten, 
									chitiet_sodienthoai: chitiet_sodienthoai, 
									chitiet_namsinh: chitiet_namsinh, 
									chitiet_dcdon: chitiet_dcdon, 
									chitiet_dctrungchuyen: chitiet_dctrungchuyen,
									chitiet_kyhieuve: chitiet_kyhieuve, 
									chitiet_giave: chitiet_giave,
									chitiet_diemdi: chitiet_diemdi,
									chitiet_diemden: chitiet_diemden,
									chitiet_dcdon: chitiet_dcdon,
									chitiet_dctrungchuyen: chitiet_dctrungchuyen,
									chitiet_ghichu: chitiet_ghichu,
									Datve_id: Datve_id,
									Vechitiet_id: Vechitiet_id,
									Khach_id: Khach_id,
									xekhoihanhid: xekhoihanhid};
									
				wti.ajax_popup('datve.ajax', "POST", dataString, function (data) {
					if (data.err == 0 && data.msg == 'success') {						
						bootbox.alert("Yêu cầu chỉnh sữa vé đã được lưu !", function(result) { return; });
						
						// load lai danh sach ghe
						wti.ajax_load_data('danhsachghe.ajax',"POST",{lichchay_id:$("#xekhoihanhid").val(), view:0, ticketid:data.ticketid},
						function (data) { $(".danhsachghe").html(data); });						
						
						// load lai form dieu chinh ve
						wti.func.datve.editFrmDatve(data.ticketid, $("#xekhoihanhid").val());
					} else {						
						bootbox.alert("Chỉnh sữa vé không thực hiện được !", function(result) { return; });
					}
				});
				
			}
		},
		frmdatve:function( ticketid ){
			wti.ajax_load_data('frmdatve.ajax',"POST",{ticketid: ticketid},
			function (data) {
				//wti.func.datve.danhsachghe($("#xekhoihanhid").val());
				$(".thongtinve").html(data);
			});
		},
		newFrmDatve:function( Ghexe_id, Soghe, price ){
			wti.ajax_popup('frmdatve.ajax',"POST",{act: "frmdatve.new", Ghexe_id: Ghexe_id},
			function (data) {
				if (data.err == 0 && data.msg == 'success') {
					
					wti.func.conf.steps.step += 1;
					
					var benkhoihanh = data.benkhoihanh;
					var benketthuc = data.benketthuc;
					var optValues = data.diemdi;
					var optValue  = optValues.split(',');
					
					var tab_head = wti.join
					('<li id="chair-head'+ Ghexe_id +'"><span onclick="wti.func.datve.removeseat('+ Ghexe_id +','+ data.gia +');" class="seat_del"></span><a class="glyphicons car" href="#chair-tab'+ Ghexe_id +'" data-toggle="tab"><i></i>'+ Soghe +'</a></li>')();
					
					var tab_content = wti.join
					('<div class="tab-pane" id="chair-tab'+ Ghexe_id +'">')
						('<div class="span12">')							
							('<div class="control-group span7">')
								('<label class="control-label control-label-fix" for="chitiet_kyhieuve">Ký hiệu vé: </label>')
								('<div class="controls controls-fix">')
									('<input class="span12" id="chitiet_kyhieuve'+wti.func.conf.steps.step+'" name="chitiet_kyhieuve[]" type="text" value="" />')
								('</div>')
							('</div>')
							('<div class="control-group span5">')
								('<label class="control-label control-label-fix" style="width:55px;margin-left:10px;" for="chitiet_giave">Giá vé: </label>')
								('<div class="controls controls-fix" style="margin-left:75px;">')
									('<input class="span12" style="width: 147px;" id="chitiet_giave'+wti.func.conf.steps.step+'" name="chitiet_giave[]" type="text" readonly value="'+ wti.numberFormat(data.gia) +'" />')
								('</div>')
							('</div>')							
							('<div class="control-group span7">')
								('<label class="control-label control-label-fix" for="chitiet_sodienthoai">Số ĐT: <span class="required">*</span></label>')
								('<div class="controls controls-fix">')
								   (' <input class="span12 intformat chitiet_sodienthoai" id="chitiet_sodienthoai'+wti.func.conf.steps.step+'" name="chitiet_sodienthoai[]" maxlength="11" type="text" value="" />')
								('</div>')
							('</div>')
							('<div class="control-group span5">')
								('<label class="control-label control-label-fix" style="width:60px;margin-left:10px;" for="chitiet_namsinh">Năm sinh: </label>')
								('<div class="controls controls-fix" style="margin-left:75px;">')
									('<input class="span12" style="width: 147px;" id="chitiet_namsinh'+wti.func.conf.steps.step+'" name="chitiet_namsinh[]" type="text" value="" />')
								('</div>')
							('</div>')
							('<div class="control-group">')
								('<label class="control-label control-label-fix" for="chitiet_hoten">Họ tên: <span class="required">*</span></label>')
								('<div class="controls controls-fix">')
									('<input class="span12" id="chitiet_hoten'+wti.func.conf.steps.step+'" name="chitiet_hoten[]" type="text" value="" />')
								('</div>')
							('</div>')
							('<div class="control-group span7">')
								('<label class="control-label control-label-fix" style="width:65px;margin-left:5px;" for="chitiet_diemdi">Đi từ: <span class="required">*</span></label>')
								('<div class="controls controls-fix">')
									('<select name="chitiet_diemdi[]" id="chitiet_diemdi'+wti.func.conf.steps.step+'" class="span12">')();
									for (key in optValue) {
									  if (typeof (optValue[key]) == 'string') {
										var items = optValue[key].split(':');
										tab_content += '<option value="'+items[0]+'"'+(items[0] == benkhoihanh ? ' selected':'')+'>'+items[1]+'</option>';
									  }
									}
									tab_content += wti.join
									('</select>')
								('</div>')
							('</div>')
							('<div class="control-group span5">')
								('<label class="control-label control-label-fix" style="width:40px;margin-left:5px;" for="chitiet_diemden">Đến: <span class="required">*</span></label>')
								('<div class="controls controls-fix" style=" margin-left: 45px;">')
									('<select name="chitiet_diemden[]" id="chitiet_diemden'+wti.func.conf.steps.step+'" class="span12" style="width: 177px;">')();
									for (key in optValue) {
									  if (typeof (optValue[key]) == 'string') {
										var items = optValue[key].split(':');
										tab_content += '<option value="'+items[0]+'"'+(items[0] == benketthuc ? ' selected':'')+'>'+items[1]+'</option>';
									  }
									}
									tab_content += wti.join
									('</select>')
								('</div>')
							('</div>')
							('<div class="control-group">')
								('<label class="control-label control-label-fix" for="chitiet_dcdon">Điểm đón: <span class="required">*</span></label>')
								('<div class="controls controls-fix">')
									('<input class="span12" id="chitiet_dcdon'+wti.func.conf.steps.step+'" name="chitiet_dcdon[]" type="text" value=""/>')
								('</div>')
							('</div>')
							('<div class="control-group">')
								('<label class="control-label control-label-fix" for="chitiet_dctrungchuyen">Trung chuyễn: </label>')
								('<div class="controls controls-fix">')
									('<input class="span12" id="chitiet_dctrungchuyen'+wti.func.conf.steps.step+'" name="chitiet_dctrungchuyen[]" type="text" value=""/>')
								('</div>')
							('</div>')
							('<div class="control-group">')
								('<label class="control-label control-label-fix" for="chitiet_dctrungchuyen">Đón trên đường: </label>')
								('<div class="controls controls-fix">')
									('<input class="span12" id="chitiet_dontrenduong'+wti.func.conf.steps.step+'" name="chitiet_dontrenduong[]" type="text" value=""/>')
								('</div>')
							('</div>')
							('<div class="control-group">')
								('<label class="control-label control-label-fix" for="chitiet_ghichu">Ghi chú: </label>')
								('<div class="controls controls-fix">')
									('<input class="span12" id="chitiet_ghichu'+wti.func.conf.steps.step+'" name="chitiet_ghichu[]" type="text" value=""/>')
								('</div>')
							('</div>')
						('</div>')
						('<input id="chitiet_soghe'+wti.func.conf.steps.step+'" name="chitiet_soghe[]" type="hidden" value="'+ Soghe +'"/>')
						('<input id="chitiet_soghe_id'+wti.func.conf.steps.step+'" name="chitiet_soghe_id[]" type="hidden" value="'+ Ghexe_id +'"/>')
					('</div>')
					('<script language="javascript">')
						('var options = {')
							('url: "./khachhang.json.ajax",')
							('getValue: "sdt",')
							('requestDelay: 500,')
							('list: {')
								('onChooseEvent: function(){')
									('var hoten = $("#chitiet_sodienthoai'+wti.func.conf.steps.step+'").getSelectedItemData().name;')
									('var dcdon = $("#chitiet_sodienthoai'+wti.func.conf.steps.step+'").getSelectedItemData().dcdon;')
									('var dctrungchuyen = $("#chitiet_sodienthoai'+wti.func.conf.steps.step+'").getSelectedItemData().dctrungchuyen;')
									('$("#chitiet_hoten'+wti.func.conf.steps.step+'").val(hoten);')
									('$("#chitiet_dcdon'+wti.func.conf.steps.step+'").val(dcdon);')
									('$("#chitiet_dctrungchuyen'+wti.func.conf.steps.step+'").val(dctrungchuyen);')
								('},')
								('maxNumberOfElements: 10,')
								('match: {')
									('enabled: true')
								('}')
							('}	')
						('};')						
						('$("#chitiet_sodienthoai'+wti.func.conf.steps.step+'").easyAutocomplete(options);')
					('</script>')
					('')();
					
					var recTab_head = tab_head;
					var recTab_content = tab_content;
					
					if( $('.chair_tab_head ul li.default').length > 0 ){
						$('.chair_tab_head ul').html(recTab_head);
						$('.chair_tab_content').html(recTab_content);
						$('.chair_tab_head ul li').addClass("active");
						$('.chair_tab_content').find(".tab-pane").addClass("active");
						wti.func.datve.clearForm();
					} else {
						//$('.chair_tab_head ul li').removeClass("active");
						//$('.chair_tab_content').find(".tab-pane").removeClass("active");						
						$('.chair_tab_head ul').append(recTab_head);
						$('.chair_tab_content').append(recTab_content);							
					}
										
					wti.func.conf.variable.sumprice = Math.abs(parseInt( wti.func.conf.variable.sumprice ) + parseInt(data.gia));
					wti.func.conf.variable.sumqty   += 1;
					
					$('#tongtienve').html(wti.numberFormat(wti.func.conf.variable.sumprice) + " VNĐ" + " ("+ wti.func.conf.variable.sumqty +" vé)");
					
					$('#btn_group').html('<button onclick="wti.func.datve.datve(1);" id="btn_datve" type="button" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Đặt vé</button>&nbsp;&nbsp;<button onclick="wti.func.datve.datve(2);" id="btn_banve" type="button" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Bán vé</button>&nbsp;&nbsp;');				
					
				}  else {						
					bootbox.alert("Không kết nối được dữ liệu !", function(result) { return; });
				}
			});			
		},
		editFrmDatve:function( Datve_id, lichchay_id ){
			wti.ajax_load_data('frmdatve.ajax',"POST",{act: "frmdatve.edit", Datve_id: Datve_id, lichchay_id:lichchay_id,},
			function (data) {
				if (data != "") {
					wti.func.conf.variable.sumprice = 0;
					wti.func.conf.variable.sumqty	= 0;
					wti.func.conf.steps.step 		= 0;
					$(".thongtinve").html(data);					
				}  else {						
					bootbox.alert("Không kết nối được dữ liệu !", function(result) { return; });
				}
			});			
		},
		
		removeseat:function( gheid, gia ){
			$('#chair-head'+gheid).remove();
			$('#chair-tab'+gheid).remove();
			$('.chair_tab_head ul li:first').addClass("active");
			$('.chair_tab_content').find(".tab-pane:first").addClass("active");

			wti.func.conf.variable.sumprice = Math.abs(parseInt( wti.func.conf.variable.sumprice ) - parseInt(gia));
			wti.func.conf.variable.sumqty   -= 1;
			
			$('#tongtienve').html(wti.numberFormat(wti.func.conf.variable.sumprice) + " VNĐ" + " ("+ wti.func.conf.variable.sumqty +" vé)");
					
			// load lai danh sach ghe
			var data = '{"act":"removeseat","ticketid":-2,"lichchay_id":'+ $("#xekhoihanhid").val() +',"gheid":'+ gheid +'}';
			socket.emit('messages', data);
			/*
			wti.ajax_load_data('danhsachghe.ajax',"POST",{lichchay_id:$("#xekhoihanhid").val(), view:1, gheid:gheid},
			function (data) { $(".soghetrenxe").html(data); });
			
			if( $(".chair_tab_head ul li").length < 1){
				wti.func.datve.danhsachghe( $("#xekhoihanhid").val() );
			}*/
		},
		
		huyghe:function( gheid ){
			bootbox.confirm("Bạn có chắc chắn hủy ghế được chọn hay không !", function(result)
			{
				if(result){
					
					var ticketid = wti.util_trim(jQuery('#Datve_id').val());
					var lichchay_id = wti.util_trim(jQuery("#xekhoihanhid").val());
					
					wti.ajax_popup('datve.ajax',"POST",{act: "ghe.remove", gheid:gheid, lichchay_id:lichchay_id},
					function (data) {
						if (data.err == 0 && data.msg == 'success') {							
							
							//wti.ajax_load_data('danhsachghe.ajax',"POST",{lichchay_id:lichchay_id, view:0, ticketid:ticketid},
							//function (data) { $(".danhsachghe").html(data); });
							wti.func.datve.editFrmDatve( ticketid, lichchay_id );
						
							// load lai danh sach ghe
							var data = '{"act":"huyghe","ticketid":'+ ticketid +',"view":0,"lichchay_id":'+ lichchay_id +',"gheid":-2}';
							socket.emit('messages', data);
						
						} else {						
							bootbox.alert("Hủy ghế không thực hiện được !", function(result) { return; });
						}
					});	
				}
			});						
		},
		
		huyve:function( Datve_id ){
			bootbox.confirm("Bạn có chắc chắn hủy vé được chọn hay không !", function(result)
			{
				if(result){
					var lichchay_id = wti.util_trim(jQuery("#xekhoihanhid").val());
					wti.ajax_popup('datve.ajax',"POST",{act: "datve.remove", Datve_id:Datve_id, lichchay_id: lichchay_id},
					function (data) {
						if (data.err == 0 && data.msg == 'success') {							
							// load lai danh sach ghe
							var data = '{"act":"huyve","ticketid":-2,"view":0,"lichchay_id":'+ lichchay_id +',"gheid":-2}';
							socket.emit('messages', data);
							
						} else {						
							bootbox.alert("Hủy vé không thực hiện được !", function(result) { return; });
						}
					});	
				}
			});						
		},
		clearForm:function (){			
			$('#datve_sodienthoai, #datve_hoten').val('');
			$('#frmdatve checkbox, :radio').prop('checked', false);
			
			$('#frmdatve input, select').not(':button, :submit, :reset, :hidden, :checkbox, :radio').prop("disabled",false);
			
			if(jQuery('#datve_diemdi').val() != 0){ $('#datve_diemdi').prepend('<option selected val="0">Chọn điểm đi</option>'); }
			if(jQuery('#datve_diemden').val() != 0){ $('#datve_diemden').prepend('<option selected val="0">Chọn điểm đến</option>'); }				
		}
		
	}
};
/*
	$(document).on('keyup', '.moneyformat', function(){
		$(this).formatCurrency({ symbol: '', colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 0 });
	}).keypress(function(e) {
		if (String.fromCharCode(e.charCode).match(/[^0-9]/g)) return false;
	});
	
	$(document).on('keyup', '.intformat', function(e){	
		if (String.fromCharCode(e.charCode).match(/[^0-9]/g)) return false;
	});
*/
function func_notyfy(layout, type, message){
		
	var notification = []; notification[type] = message;							
							
	var self = $(this);	
	var data_layout = layout;
	var data_type 	= type;	// success | alert | error | warning | information | confirm

	notyfy({
		text: notification[data_type],
		type: data_type,
		dismissQueue: true,
		layout: data_layout,
		buttons: (data_type != 'confirm') ? false : [{
			addClass: 'btn btn-success btn-small btn-icon glyphicons ok_2',
			text: '<i></i> Ok',
			onClick: function ($notyfy) {
				$notyfy.close();
				notyfy({
					force: true,
					text: 'You clicked "<strong>Ok</strong>" button',
					type: 'success',
					layout: data_layout
				});
			}
		}, {
			addClass: 'btn btn-danger btn-small btn-icon glyphicons remove_2',
			text: '<i></i> Cancel',
			onClick: function ($notyfy) {
				$notyfy.close();
				notyfy({
					force: true,
					text: '<strong>You clicked "Cancel" button<strong>',
					type: 'error',
					layout: data_layout
				});
			}
		}]
	});
	return false;
}