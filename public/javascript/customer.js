mt24h.customer = {
	logout:function(){
		if(confirm("Bạn có chắc chắn là bạn thoát khỏi website không ?")){
			mt24h.ajax_popup('logout',"POST",{},
			function (j) {
				mt24h.cookie.set('login', '', -86400*7, '/');
				location.reload();
			});
			return false;
		}		
	},
	
	slidingDiv:function(id){
		jQuery('.content_sub'+id).slideToggle("slow");
	},

	anchor:function(id){
		var target_offset = jQuery("#"+id).offset();
		var target_top = target_offset.top;
		jQuery('html, body').animate({scrollTop:target_top}, 1100);
	},	
	
	setCity:function(id,safe_title){
		mt24h.cookie.set('city', id+"|"+safe_title, 86400*7,'/');
		window.location.href = BASE_URL+safe_title+".mt24h";
	},

	/*------------------don-hang----------------*/
	don_hang:{
		don_hang_chi_tiet:function(email,code){
			mt24h.ajax_load_data('ajax_chi_tiet_don_hang',"POST",{email: email,code:code},
			function (j) {
				if (j != "") {
					mt24h.show_overlay_popup('mua24h-don_hang_chi_tiet', '',
					mt24h.customer.don_hang.theme.don_hang_chi_tiet('mua24h-don_hang_chi_tiet',j),
					{
						background: {'background-color' : 'transparent'},
						border: {
							'background-color' : 'transparent',
							'padding' : '0px'
						},
						title: {'display' : 'none'},
						content: {
							'padding' : '0px',
							'width' : '600px'
						},
						release:function(){
							//mt24h.enter('#forgot_email', mt24h.header.password.submit);
						}
					});
				}else{
					mt24h.show_popup_message(j.msg, "Thao tác thất bại", -1);
				}
			});
			return true;
		},	
		theme:{
			don_hang_chi_tiet:function(id,content){
				return mt24h.join
				('<div class="cungmua24h-popup" style="margin:0 auto">')
					('<div class="cungmua24h-sub-popup" style="margin:0 auto">')
					('<div class="cungmua24h-sub-popup_1" style="margin:0 auto">')
					('<div class="cungmua24h-popup-title"><div class="bgl"><div class="bg">')
						('<div class="fl"><div class="text_fl">Đơn hàng chi tiết<a href="javascript:void(0)" class="cungmua24h-popup-close" title="Đóng" onclick="mt24h.hide_overlay_popup(\''+id+'\')"><img src="templates/cungmua24h/images/icons/cungmua24h_close_icons.png"></a></div></div>')
					('</div></div></div>')
					('<div class="content resendPassword">'+content+'')
						
					('</div>')
					('<div class="bottom"><div class="bgl"><div class="bg"></div></div></div>')
					('</div>')
					('</div>')
				('</div>')();
			}
		}
	},
	/*------------Login Submit-----------*/
	loginForm_submit: function(){
		mt24h.customer.Login_form.submit();
		return false;
	},
	/*---------------Login on Form------------*/
	Login_form:{
		submit: function(){
			var email = mt24h.util_trim(jQuery('#txt_email').val());
			var password = mt24h.util_trim(jQuery('#txt_password').val());
			var check_cookie = mt24h.util_trim(jQuery('#check_cookie').val());
			if(email == '' || email == 'Email đăng nhập')
			{
				mt24h.show_popup_message("Vui lòng nhập địa chỉ Email của bạn", "Lỗi đăng nhập", -1);
				return false;
			}else if(!mt24h.is_email(email))
			{
				mt24h.show_popup_message("Địa chỉ Email không hợp lệ", "Lỗi đăng nhập", -1);
				return false;
			}else  if(password == '')
			{
				mt24h.show_popup_message("Vui lòng nhập mật khẩu của bạn", "Lỗi đăng nhập", -1);
				return false;
			}else
			{
				mt24h.ajax_popup('ajax_Login',"POST",{email: email,password: password,check_cookie:check_cookie},
				function (j) {
					if (j.err == 0 && j.msg == 'success') {
						if(j.check_cookie == "on")
						{
							mt24h.cookie.set('login', j.Id+"|"+j.mathanhvien+"|"+j.email+"|"+j.phone+"|"+j.fullname+"|"+j.gender+"|"+j.avatar+"|"+j.diachi+"|"+j.permission, 86400*7, '/');
						}
						location.reload();
					}else{
						mt24h.show_popup_message("Đăng nhập thất bại", "Lỗi đăng nhập", -1);
					}
				});
				
			}
			return true;
		}
	},
	/*---------------Dang Ky Email Khuyen Mai Submit------------*/
	RegisterAdsEmail: function(){
		mt24h.customer.RegisterAdsEmail_form.submit();
		return false;
	},
	/*---------------Dang ky Email khuyen mai Form------------*/
	RegisterAdsEmail_form:{
		submit: function(){
			var email = mt24h.util_trim(jQuery('#txt_ads_email').val());
			
			if(email == '' || email == 'Nhập Email của bạn...')
			{
				mt24h.show_popup_message("Vui lòng nhập địa chỉ Email của bạn", "Lỗi đăng ký", -1);
				return false;
			}else if(!mt24h.is_email(email))
			{
				mt24h.show_popup_message("Địa chỉ Email không hợp lệ", "Lỗi đăng ký", -1);
				return false;
			}else
			{
				mt24h.ajax_popup('ajax_register_ads_mail',"POST",{email: email},
				function (j) {
					if (j.err == 0 && j.msg == 'success') {
						mt24h.show_popup_message("Đăng ký email thành công", "Đăng ký thành công", 1);
					}else{
						switch(j.msg){
								case 'email_error': j.msg = 'Email của bạn không có thật'; break;
								case 'reg_error'  : j.msg = 'Đăng ký thất bại'; break;
								case 'email_exist'   : j.msg = 'Email của bạn đã được đăng ký trước đó'; break;
							}
							mt24h.show_popup_message(j.msg, "Đăng nhập thất bại", -1);
					}
				});
				
			}
			return true;
		}
	},
	
	/*------------------Choose Provinces--------------*/
	province:{
		show:function(){
			mt24h.show_overlay_popup('cungmua24h-province-choose', '',
			mt24h.customer.province.theme.show('cungmua24h-province-choose'),
			{
				background: {'background-color' : 'transparent'},
				border: {
					'background-color' : 'transparent',
					'padding' : '0px'
				},
				title: {'display' : 'none'},
				content: {
					'padding' : '0px',
					'width' : '583px'
				},
				release:function(){
					//mt24h.enter('#forgot_email', mt24h.header.password.submit);
				}
			});
		},		
		changeClass:function(id){
			jQuery("#1").removeClass('active');
			jQuery("#2").removeClass('active');
			jQuery("#"+id).addClass('active');
		},
		theme:{
			show:function(id){
				return mt24h.join
				('<div class="popup_provinces" style="margin:0 auto">')
					('<a href="javascript:void(0)" class="popup_provinces-close" title="Đóng" onclick="mt24h.hide_overlay_popup(\''+id+'\')"><img src="templates/cungmua24h/images/popup/icon_close.png"></a>')
					('<div class="popup_content">')						
						('<ul>')
							('<li id="1" class="active"><a onclick="mt24h.customer.setCity(\'2\',\'Can-Tho\')" onmouseover="javascript:mt24h.customer.province.changeClass(1)" href="javascript:void(0);">TP. CẦN THƠ</a></li>')
							('<li id="2"><a onclick="mt24h.customer.setCity(\'15\',\'TP-Ho-Chi-Minh\')" onmouseover="javascript:mt24h.customer.province.changeClass(2)" href="javascript:void(0);">TP. HỒ CHÍ MINH</a></li>')
						('</ul>')
					('</div>')
				('</div>')();
			}
		}
	},
	
	/*------------------Resend Password--------------*/
	password:{
		resendPassword:function(email){
			mt24h.show_overlay_popup('mua24h-resend-password', '',
			mt24h.customer.password.theme.resendPassword('mua24h-resend-password', email),
			{
				background: {'background-color' : 'transparent'},
				border: {
					'background-color' : 'transparent',
					'padding' : '0px'
				},
				title: {'display' : 'none'},
				content: {
					'padding' : '0px',
					'width' : '400px'
				},
				release:function(){
					//mt24h.enter('#forgot_email', mt24h.header.password.submit);
				}
			});
		},
		submit:function(){
			var email = mt24h.util_trim(jQuery('#forgot_email').val());
			if(email == ''){
				mt24h.error.set('#forgot_email', 'Chưa nhập email', 230, '.resendPassword');
				return false;
			}else if(!mt24h.is_email(email)){
				mt24h.error.set('#forgot_email', 'Email không hợp lệ', 230, '.resendPassword');
				return false;
			}else{
				mt24h.error.close('#forgot_email', '.forgot_pasword');
			}
			mt24h.ajax_popup('ajax_forgetpass',"POST",{email: email},
			function (j) {
				if (j.err == 0 && j.msg == 'success') {
					mt24h.hide_overlay_popup('mua24h-resend-password');
					mt24h.show_popup_message("Thông tin hỗ trợ đã được gửi vào "+email+"\nQuý khách vui lòng làm theo hướng dẫn trong email", "Xác nhận email", 1);
				}else{
					mt24h.error.set('#forgot_email', j.msg, 230, '.resendPassword');
				}
			});
			return true;
		},		
		theme:{
			resendPassword:function(id, email){
				return mt24h.join
				('<div class="cungmua24h-popup" style="margin:0 auto">')
					('<div class="cungmua24h-sub-popup" style="margin:0 auto">')
					('<div class="cungmua24h-sub-popup_1" style="margin:0 auto">')
					('<div class="cungmua24h-popup-title"><div class="bgl"><div class="bg">')
						('<div class="fl"><div class="text_fl">Quên mật khẩu<a href="javascript:void(0)" class="cungmua24h-popup-close" title="Đóng" onclick="mt24h.hide_overlay_popup(\''+id+'\')"><img src="templates/cungmua24h/images/icons/cungmua24h_close_icons.png"></a></div></div>')
					('</div></div></div>')
					('<div class="content resendPassword">')
						('<div id="cError"></div>')
						('<div class="label mTop10">Email:</div>')
						('<div class="input-txt">')
							('<input type="text" id="forgot_email" name="email" class="txt" value="'+(email?email:'')+'" />')
							('<a id="fr" class="blueButton" onclick="mt24h.customer.password.submit()" href="javascript:void(0)"><span><span>Gửi đi</span></span></a>')
						('</div>')
						('<div class="f11">Vui lòng nhập đúng email đã đăng kí để nhận thông tin hỗ trợ lấy lại mật khẩu từ <span style="color:#f57d20;font-weight:bold;">CungMua24h</span></div>')
					('</div>')
					('<div class="bottom"><div class="bgl"><div class="bg"></div></div></div>')
					('</div>')
					('</div>')
				('</div>')();
			}
		}
	},
	/*------------------------------*/
	
	/* gui binh luan */
	comment:{
		send_comment:function(SPID, parentid){
			var noidung = mt24h.util_trim(jQuery('#noidung_lb').val());
			if(noidung == '' || noidung == "Bình luận của bạn"){
				mt24h.show_popup_message("Chưa nhập nội dung lời bình", "Bình luận thất bại", -1);
				return false;
			} else {
				mt24h.ajax_popup('ajax_guibinhluan',"POST",{noi_dung: noidung, sp_id:SPID, parent_id:parentid},
				function (j) {
					if (j.err == 0 && j.msg == 'success') {
						// load lai comment sau khi post du lieu ok
						mt24h.ajax_load_data('ajax_showbinhluan',"POST",{sp_id:SPID, parent_id:parentid, page:1},
						function (j) {
							if (j != "") {
								jQuery('.content_comment').html(j).hide().fadeIn(1000);
								mt24h.util_trim(jQuery('#noidung_lb').val("Bình luận của bạn"));
							}else{
								mt24h.show_popup_message(j.msg, "Tải bình luận thất bại", -1);
							}
						});

					}else{
						mt24h.show_popup_message(j.msg, "Bình luận thất bại", -1);
					}
				});
			}
			return true;
		},
		
		send_sub_comment:function(SPID, parentid){
			var noidung = mt24h.util_trim(jQuery('#sub_noidung_lb'+parentid).val());
			if(noidung == ''){
				mt24h.show_popup_message("Chưa nhập nội dung lời bình", "Bình luận thất bại", -1);
				return false;
			} else {
				mt24h.ajax_popup('ajax_guibinhluan',"POST",{noi_dung: noidung, sp_id:SPID, parent_id:parentid},
				function (j) {
					if (j.err == 0 && j.msg == 'success') {
						// load lai comment sau khi post du lieu ok
						mt24h.ajax_load_data('ajax_showsubbinhluan',"POST",{sp_id:SPID, parent_id:parentid},
						function (j) {
							if (j != "") {
								jQuery('.other_content_sub_comment'+parentid).append(j).hide().fadeIn(1000);
								jQuery('#sub_noidung_lb'+parentid).val("");
							}else{
								mt24h.show_popup_message(j.msg, "Tải bình luận thất bại", -1);
							}
						});						
					}else{
						mt24h.show_popup_message(j.msg, "Bình luận thất bại", -1);
					}
				});
			}
			return true;
		},
		
		del_comment:function(SPID, id, page){
			if(confirm("Bạn có chắc chắn muốn xóa bình luận này không ?")){
				mt24h.ajax_popup('ajax_delbinhluan',"POST",{id: id},
				function (j) {
					if (j.err == 0 && j.msg == 'success') {
						mt24h.ajax_load_data('ajax_showbinhluan',"POST",{sp_id:SPID, parent_id:id, page:page},
						function (j) {
							if (j != "") {
								jQuery('.content_comment').html(j).hide().fadeIn(1000);
							}else{
								mt24h.show_popup_message(j.msg, "Tải bình luận thất bại", -1);
							}
						});
					}else{
						mt24h.show_popup_message(j.msg, "Xóa bình luận thất bại", -1);
					}
				});
			}								
		},
		
		del_sub_comment:function(id){
			if(confirm("Bạn có chắc chắn muốn xóa trả lời này không ?")){
				mt24h.ajax_popup('ajax_del_sub_binhluan',"POST",{id: id},
				function (j) {
					if (j.err == 0 && j.msg == 'success') {
						 $('.other_content_sub_comment'+id).fadeOut(800);
					}else{
						mt24h.show_popup_message(j.msg, "Xóa trả lời thất bại", -1);
					}
				});
			}								
		},
		
		edit_comment:function(id){
			var comment = mt24h.util_trim(jQuery('#txt_comment').val());
			mt24h.ajax_popup('ajax_edit_binhluan',"POST",{id: id, comment:comment},
			function (j) {
				if (j.err == 0 && j.msg == 'success') {
					mt24h.hide_overlay_popup('cungmua24h-edit_comment');
					location.reload();
				}else{
					mt24h.show_popup_message(j.msg, "Edit bình luận thất bại", -1);
				}
			});
		},
		
		show_edit_comment:function(comment_id, page){
			comment = jQuery("#comment"+comment_id).html()
			mt24h.show_overlay_popup('cungmua24h-edit_comment', '',
			mt24h.customer.comment.theme.edit('cungmua24h-edit_comment', comment_id, comment),
			{
				background: {'background-color' : 'transparent'},
				border: {
					'background-color' : 'transparent',
					'padding' : '0px'
				},
				title: {'display' : 'none'},
				content: {
					'padding' : '0px',
					'width' : '520px'
				},
				release:function(){
					//mt24h.enter('#forgot_email', mt24h.header.password.submit);
				}
			});
		},
		
		theme:{			
			edit:function(frm_name, comment_id, comment){
				return mt24h.join
				('<div class="cungmua24h-popup" style="margin:0 auto">')
					('<div class="cungmua24h-sub-popup" style="margin:0 auto">')
					('<div class="cungmua24h-sub-popup_1" style="margin:0 auto">')
					('<div class="cungmua24h-popup-title"><div class="bgl"><div class="bg">')
						('<div class="fl"><div class="text_fl">Edit comment<a href="javascript:void(0)" class="cungmua24h-popup-close" title="Đóng" onclick="mt24h.hide_overlay_popup(\''+frm_name+'\')"><img src="templates/cungmua24h/images/icons/cungmua24h_close_icons.png"></a></div></div>')
					('</div></div></div>')
					('<div class="content resendPassword">')
						('<div id="cError"></div>')
							('<div class="ct_left">')
								('<div class="label mTop10">Nội dung comment:</div>')
								('<div class="input-txt" style="height:100px;width:440px;">')
									('<textarea id="txt_comment" name="txt_comment" class="sub_text_nd_binhluan" style="width:100%;height:100%">'+comment+'</textarea>')
								('</div>')							
							('</div>')
							('<div class="clear"></div>')
							('<div class="mTop10">')
								('<div style="width:200px;">')
									('<a id="fr" class="blueButton mLeft10" onclick="mt24h.hide_overlay_popup(\''+frm_name+'\')" href="javascript:void(0)"><span><span>Hủy bỏ</span></span></a>')
									('<a id="fr" class="blueButton" onclick="mt24h.customer.comment.edit_comment('+comment_id+')" href="javascript:void(0)"><span><span>Chấp nhận</span></span></a>')
									('<div class="c"></div>')
								('</div>')
							('</div>')
					('</div>')
					('<div class="bottom"><div class="bgl"><div class="bg"></div></div></div>')
					('</div>')
					('</div>')
				('</div>')();
			}
		}		
	},
	
	/*------------Dang ky------------------*/
	Register:{
		RegisterForm:function(email){
			mt24h.show_overlay_popup('mua24h-register', '',
			mt24h.customer.Register.theme.RegisterForm('mua24h-register'),
			{
				background: {'background-color' : 'transparent'},
				border: {
					'background-color' : 'transparent',
					'padding' : '0px'
				},
				title: {'display' : 'none'},
				content: {
					'padding' : '0px',
					'width' : '640px'
				},
				release:function(){
					//mt24h.enter('#forgot_email', mt24h.header.password.submit);
				}
			});
		},
		submit:function(){
			
			var email = mt24h.util_trim(jQuery('#email').val());
			var password = mt24h.util_trim(jQuery('#password').val());
			var repassword = mt24h.util_trim(jQuery('#repassword').val());
			var fullname = mt24h.util_trim(jQuery('#fullname').val());
			var gender = mt24h.util_trim(jQuery('#gender').val());
			var phone = mt24h.util_trim(jQuery('#phone').val());
			
			
			if(email == ''){
				mt24h.error.set('#email', 'Chưa nhập email', 230, '.resendPassword');
				return false;
			}else if(!mt24h.is_email(email)){
				mt24h.error.set('#email', 'Email không hợp lệ', 230, '.resendPassword');
				return false;
			}else if(password == ''){
				mt24h.error.set('#password', 'Chưa nhập Mật khẩu', 230, '.resendPassword');
				return false;
			}else if(password.length < 6){
				mt24h.error.set('#password', 'Mật khẩu phải từ 6 ký tự trở lên', 230, '.resendPassword');
				return false;
			}else if(repassword != password){
				mt24h.error.set('#repassword', 'Mật khẩu và nhắc lại Mật khẩu phải khớp với nhau', 230, '.resendPassword');
				return false;
			}else if(fullname == ''){
				mt24h.error.set('#fullname', 'Chưa nhập họ tên', 230, '.resendPassword');
				return false;
			}else if(phone == ''){
				mt24h.error.set('#phone', 'Chưa nhập số điện thoại', 230, '.resendPassword');
				return false;
			}else if(!mt24h.is_phone(phone)){
				mt24h.error.set('#phone', 'Số điện thoại không hợp lệ', 230, '.resendPassword');
				return false;
			}else{
				mt24h.error.close('#email', '.resendPassword');
			}
			mt24h.ajax_popup('ajax_Register',"POST",{email: email,password: password,fullname: fullname,gender: gender,phone: phone},
			function (j) {
				if (j.err == 0 && j.msg == 'success') {
					mt24h.hide_overlay_popup('mua24h-register');
					alert("Đăng ký thành công!");
					location.reload();
				}else{
					mt24h.error.set('#email', j.msg, 230, '.resendPassword');
				}
			});
			return true;
		},		
		theme:{
			RegisterForm:function(id){
				return mt24h.join
				('<div class="cungmua24h-popup" style="margin:0 auto">')
					('<div class="cungmua24h-sub-popup" style="margin:0 auto">')
					('<div class="cungmua24h-sub-popup_1" style="margin:0 auto">')
					('<div class="cungmua24h-popup-title"><div class="bgl"><div class="bg">')
						('<div class="fl"><div class="text_fl">Đăng ký<a href="javascript:void(0)" class="cungmua24h-popup-close" title="Đóng" onclick="mt24h.hide_overlay_popup(\''+id+'\')"><img src="templates/cungmua24h/images/icons/cungmua24h_close_icons.png"></a></div></div>')
					('</div></div></div>')
					('<div class="content resendPassword">')
						('<div id="cError"></div>')
							('<div class="ct_left">')
								('<div class="label mTop10">Email:</div>')
								('<div class="input-txt">')
									('<input type="text" id="email" name="email" class="txt" />')
								('</div>')
								('<div class="label mTop10">Mật Khẩu:</div>')
								('<div class="input-txt">')
									('<input type="password" id="password" name="password" class="txt" value="" />')
								('</div>')
								('<div class="label mTop10">Nhắc lại Mật Khẩu:</div>')
								('<div class="input-txt">')
									('<input type="password" id="repassword" name="repassword" class="txt" value="" />')
								('</div>')
							('</div>')
							('<div class="ct_right">')
								('<div class="label mTop10">Họ Tên:</div>')
								('<div class="input-txt">')
									('<input type="text" id="fullname" name="fullname" class="txt" value="" />')
								('</div>')
								('<div class="label mTop10">Giới tính:</div>')
								('<div class="input-txt">')
									('<input type="radio" id="gender" name="gender" class="radioBtn" value="1" checked /> Nam')
									('<input type="radio" id="gender" name="gender" class="radioBtn" value="0" /> Nữ')
								('</div>')
								('<div class="label mTop10">Số điện thoại:</div>')
								('<div class="input-txt">')
									('<input type="text" id="phone" name="phone" class="txt" value="" />')
								('</div>')
							('</div>')
							('<div class="clear"></div>')
							('<div class="mTop10">')
								('<div style="width:329px;">')
									('<a id="fr" class="blueButton mLeft10" onclick="mt24h.hide_overlay_popup(\''+id+'\')" href="javascript:void(0)"><span><span>Hủy bỏ</span></span></a>')
									('<a id="fr" class="blueButton" onclick="mt24h.customer.Register.submit()" href="javascript:void(0)"><span><span>Đăng Ký</span></span></a>')
									('<input type="submit" onclick="mt24h.customer.Register.submit();" value="" class="hidden" />')
									('<div class="c"></div>')
								('</div>')
							('</div>')
						('<div class="f11">Vui lòng nhập đầy đủ toàn bộ thông tin trên để đăng ký thành viên của <span style="color:#f57d20;font-weight:bold;">CungMua24h</span></div>')
					('</div>')
					('<div class="bottom"><div class="bgl"><div class="bg"></div></div></div>')
					('</div>')
					('</div>')
				('</div>')();
			}
		}
	},
	/*------------------------------------------------------------*/
	
	/*------------Dang Nhap------------------*/
	Login:{
		LoginForm:function(email){
			mt24h.show_overlay_popup('mua24h-Login', '',
			mt24h.customer.Login.theme.LoginForm('mua24h-Login'),
			{
				background: {'background-color' : 'transparent'},
				border: {
					'background-color' : 'transparent',
					'padding' : '0px'
				},
				title: {'display' : 'none'},
				content: {
					'padding' : '0px',
					'width' : '340px'
				},
				release:function(){
					//mt24h.enter('#forgot_email', mt24h.header.password.submit);
				}
			});
		},
		submit:function(){
			
			var email = mt24h.util_trim(jQuery('#email_Login').val());
			var password = mt24h.util_trim(jQuery('#password_Login').val());
			
			
			if(email == ''){
				mt24h.error.set('#email_Login', 'Chưa nhập email', 230, '.resendPassword');
				return false;
			}else if(!mt24h.is_email(email)){
				mt24h.error.set('#email_Login', 'Email không hợp lệ', 230, '.resendPassword');
				return false;
			}else if(password == ''){
				mt24h.error.set('#password_Login', 'Chưa nhập Mật khẩu', 230, '.resendPassword');
				return false;
			}else if(password.length < 6){
				mt24h.error.set('#password_Login', 'Mật khẩu phải từ 6 ký tự trở lên', 230, '.resendPassword');
				return false;
			}else{
				mt24h.error.close('#email_Login', '.resendPassword');
			}
			mt24h.ajax_popup('ajax_Login',"POST",{email: email,password: password},
			function (j) {
				if (j.err == 0 && j.msg == 'success') {
					mt24h.hide_overlay_popup('mua24h-Login');
					location.reload();
					//alert("Đăng ký thành công!");
				}else{
					mt24h.error.set('#email_Login', j.msg, 230, '.resendPassword');
				}
			});
			return true;
		},		
		theme:{
			LoginForm:function(id){
				return mt24h.join
				('<div class="cungmua24h-popup" style="margin:0 auto">')
					('<div class="cungmua24h-sub-popup" style="margin:0 auto">')
					('<div class="cungmua24h-sub-popup_1" style="margin:0 auto">')
					('<div class="cungmua24h-popup-title"><div class="bgl"><div class="bg">')
						('<div class="fl"><div class="text_fl">Đăng Nhập<a href="javascript:void(0)" class="cungmua24h-popup-close" title="Đóng" onclick="mt24h.hide_overlay_popup(\''+id+'\')"><img src="templates/cungmua24h/images/icons/cungmua24h_close_icons.png"></a></div></div>')
					('</div></div></div>')
					('<div class="content resendPassword">')
						('<div id="cError"></div>')
								('<div class="label mTop10">Email:</div>')
								('<div class="input-txt">')
									('<input type="text" id="email_Login" name="email_Login" class="txt" />')
								('</div>')
								('<div class="label mTop10">Mật Khẩu:</div>')
								('<div class="input-txt">')
									('<input type="password" id="password_Login" name="password_Login" class="txt" value="" />')
								('</div>')
							('<div class="clear"></div>')
							('<div class="mTop10">')
								('<div style="width:190px;">')
									('<a id="fr" class="blueButton mLeft10" onclick="mt24h.hide_overlay_popup(\''+id+'\')" href="javascript:void(0)"><span><span>Hủy bỏ</span></span></a>')
									('<a id="fr" class="blueButton" onclick="mt24h.customer.Login.submit()" href="javascript:void(0)"><span><span>Đăng Nhập</span></span></a>')
									('<input type="submit" onclick="mt24h.customer.Login.submit();" value="" class="hidden" />')
									('<div class="c"></div>')
								('</div>')
							('</div>')
						
					('</div>')
					('<div class="bottom"><div class="bgl"><div class="bg"></div></div></div>')
					('</div>')
					('</div>')
				('</div>')();
			}
		}
	}

};