/*
   * cấu hình plugin xác thực form
   * */
$.validate({
    modules: 'html5, security',
    addValidClassOnAll: true
});
/*khi click vào row của table sẽ tự show thông tin chi tiết*/
$('table.table > tbody > tr').not('tr.info').on('click', function (e) {

    if ($(e.target).is('td:last-child, button')) {
        e.preventDefault();
        return;
    }

    $('.info').not($(this).next('tr.info')).hide();
    $(this).next('tr.info').toggle();
});


/*limit chọn ngày khi chọn từ ngày thì ngày cuối thúc sẽ không cho lùi về quá khứ*/
$('#txtStartDate,#txtEndDate,#txtDate,.datepicker').datepicker({
    beforeShow: function (input) {
        if (input.id === "txtEndDate") {
            return {
                minDate: $("#txtStartDate").datepicker("getDate"),
            };
        }
    },

    dateFormat: "dd-mm-yy",
    showOn: 'both',
    buttonText: "<i class=\"icon-calendar\"></i>"
});

/*Chỉ nhập định dạng số*/
$('.isNumber').on("keypress", function (evt) {
    var keycode = evt.charCode || evt.keyCode;

    if (keycode === 8 || (keycode >= 37 && keycode <= 40) || (keycode >= 48 && keycode <= 57)) {
        return true;
    }
    return false;
});

function focustimkiem() {
    setTimeout(function () {
        $("#txtsearch").focus();
    }, 200);
}

function close_menu() {
    $("#content").attr("style", "width:100%");
    $("#menu").attr("style", "margin-left:-400px");
    $("#ht_btn_close_open_menu").html('<button onClick="open_menu()" type="button" class="btn-navbar"> <div class="icon_open_menu"></div> </button>');
    if($('#setting').length>0) {
        $('#setting').css('width','100%');
        $('.divtable').css('width','100%').css('float','right');
    }
}

function open_menu() {
    $("#content").removeAttr("style");
    $("#menu").removeAttr("style");
    $("#ht_btn_close_open_menu").html('<button onClick="close_menu()" type="button" class="btn-navbar"> <div class="icon_close_menu"></div> </button>');
    if($('.divtable').length>0)
        $('.divtable').css('width',(screen.width-$('#menu').width()-20)+'px').css('float','right');

}

/** Active menu default**/
activeMenu('');

/** Active menu **/
function activeMenu(url) {
    var link = url != '' ? url : location.href;
    $('.check_active_link a').each(function (key, item) {
        if ($(item).attr('href') != undefined) {
            var link_check = $(item).attr('href').split('?')[0];
            var patt = new RegExp(link_check + '$');

            if (patt.test(link.split('?')[0])) {
                $(item).parents('li').addClass('active');
            }
        }
    });

}

function showInfo(_this) {
    $('tr.info').not($(_this).parents('tr').next('tr.info')).hide();
    $(_this).parents('tr').next('tr.info').toggle();
}

/*Show notyfytion*/
function Message(title, content, class_name) {
    if (class_name == '') {
        class_name = 'gritter-primary';
    }
    $('.gritter-item-wrapper').remove();
    $.gritter.add({
        title: title,
        text: content,
        class_name: class_name
    });

    return true;

}
Date.prototype.customFormat = function(formatString){
    var YYYY,YY,MMMM,MMM,MM,M,DDDD,DDD,DD,D,hhhh,hhh,hh,h,mm,m,ss,s,ampm,AMPM,dMod,th;
    YY = ((YYYY=this.getFullYear())+"").slice(-2);
    MM = (M=this.getMonth()+1)<10?('0'+M):M;
    MMM = (MMMM=["January","February","March","April","May","June","July","August","September","October","November","December"][M-1]).substring(0,3);
    DD = (D=this.getDate())<10?('0'+D):D;
    DDD = (DDDD=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][this.getDay()]).substring(0,3);
    th=(D>=10&&D<=20)?'th':((dMod=D%10)==1)?'st':(dMod==2)?'nd':(dMod==3)?'rd':'th';
    formatString = formatString.replace("#YYYY#",YYYY).replace("#YY#",YY).replace("#MMMM#",MMMM).replace("#MMM#",MMM).replace("#MM#",MM).replace("#M#",M).replace("#DDDD#",DDDD).replace("#DDD#",DDD).replace("#DD#",DD).replace("#D#",D).replace("#th#",th);
    h=(hhh=this.getHours());
    if (h==0) h=24;
    if (h>12) h-=12;
    hh = h<10?('0'+h):h;
    hhhh = hhh<10?('0'+hhh):hhh;
    AMPM=(ampm=hhh<12?'am':'pm').toUpperCase();
    mm=(m=this.getMinutes())<10?('0'+m):m;
    ss=(s=this.getSeconds())<10?('0'+s):s;
    return formatString.replace("#hhhh#",hhhh).replace("#hhh#",hhh).replace("#hh#",hh).replace("#h#",h).replace("#mm#",mm).replace("#m#",m).replace("#ss#",ss).replace("#s#",s).replace("#ampm#",ampm).replace("#AMPM#",AMPM);
};

function getFormattedDate(unix_timestamp, methor) {
    var date = new Date(unix_timestamp);
    str = '';
    if(methor === 'date'){
        str = $.format.date(date.getTime(), 'dd-MM-yyyy')
    }else if (methor === 'time') {
        str = $.format.date(date.getTime(), 'HH:mm')
    } else {
        str = $.format.date(date.getTime(), 'HH:mm, dd/MM/yyyy')
    }

    return str;
}

function moneyFormat(money) {
    return $.number(money) + ' (VNĐ)';
}

function splitString(string) {
    var result = (typeof string != 'undefined' && string.split(',').length <= 1 ? string : (typeof string != 'undefined' ? (string.split(',')[0] + ',' + string.split(',')[1]) : ''));
    return result;
}