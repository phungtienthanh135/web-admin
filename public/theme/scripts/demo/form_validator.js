$.validator.setDefaults(
{
	submitHandler: function() { alert("submitted!"); },
	showErrors: function(map, list) 
	{
		this.currentElements.parents('label:first, .controls:first').find('.error').remove();
		this.currentElements.parents('.control-group:first').removeClass('error');
		
		$.each(list, function(index, error) 
		{
			var ee = $(error.element);
			var eep = ee.parents('label:first').length ? ee.parents('label:first') : ee.parents('.controls:first');
			
			ee.parents('.control-group:first').addClass('error');
			eep.find('.error').remove();
			eep.append('<p class="error help-block"><span class="label label-important">' + error.message + '</span></p>');
		});
		//refreshScrollers();
	}
});


$(function() {
	// validate the comment form when it is submitted
	// validate signup form on keyup and submit
	jQuery.validator.addMethod("checkPhoneNumber",
		function(value, element) 
		{
			n = value.length;
			count = 0;
			
			for (i = 0; i < n; ++i)
			{
				if( !(value.charAt(i) == ' ' || (value.charAt(i) >= '0' && value.charAt(i) <= '9')) )
				{
					return false;
				}
				else if( value.charAt(i) >= '0' && value.charAt(i) <= '9')
				{
					++count;
				}
			}
			
			if (count < 7)
			{
				return false;
			}

			return true;
		})
			});
$(function()
{
	// validate signup form on keyup and submit
	$("#validateSubmitForm").validate({
		rules: {
			fullname: "required",
			lastname: "required",
			username: {
				required: true,
				minlength: 2
			},
			password: {
				required: true,
				minlength: 5
			},
			confirm_password: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			},
			phone: {
				required: true,
				checkPhoneNumber: true
			},
			textfill: "required",
			textfill_2: "required"
		},
		messages: {
			fullname: "Nhập tên đầy đủ",
			lastname: "Nhập tên ngắn gọn",
			username: {
				required: "Nhập tài khoản",
				minlength: "Your username must consist of at least 2 characters"
			},
			password: {
				required: "Vui lòng nhập mật khẩu",
				minlength: "Mật khẩu của bạn phải có ít nhất 5 ký tự"
			},
			confirm_password: {
				required: "Vui lòng nhập mật khẩu",
				minlength: "Mật khẩu của bạn phải có ít nhất 5 ký tự",
				equalTo: "Xác nhận mật khẩu chưa đúng"
			},
			phone: {
				required: "Vui lòng nhập số điện thoại",
				checkPhoneNumber: "Số điện thoại chưa đúng"
			},
			textfill: "Vui lòng nhập giá trị",
			textfill_2: "Vui lòng nhập giá trị"
		}
	});

	// propose username by combining first- and lastname
	/*$("#username").focus(function() {
		var fullname = $("#firstname").val();
		var lastname = $("#lastname").val();
		if(fullname && lastname && !this.value) {
			this.value = fullname + "." + lastname;
		}
	});*/

	//code to hide topic selection, disable for demo
	//var newsletter = $("#newsletter");
	// newsletter topics are optional, hide at first
	/*var inital = newsletter.is(":checked");
	var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
	var topicInputs = topics.find("input").attr("disabled", !inital);*/
	// show when newsletter is checked
	/*newsletter.click(function() {
		topics[this.checked ? "removeClass" : "addClass"]("gray");
		topicInputs.attr("disabled", !this.checked);
	});*/
});