FROM cuongysl/laravel:latest

# copy apache config file
COPY laravel.conf /etc/apache2/sites-available/
RUN a2ensite laravel.conf && a2dissite default-ssl.conf 000-default.conf\
    && a2enmod rewrite

# Copy existing application directory permissions
COPY --chown=www-data:www-data . /var/www/html

RUN chown www-data:www-data /var/www

# Set working directory
WORKDIR /var/www/html

USER www-data

RUN composer install && npm install &&\
    php artisan config:clear &&\
    php artisan view:clear &&\
    npm run dev

USER root
CMD ["/usr/sbin/apache2ctl", "-DFOREGROUND"]