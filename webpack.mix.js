let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/Login/Agency/login.js', 'public/js/loginAgency.js')
    .js('resources/assets/js/Login/Employee/index.js', 'public/js/loginEmployee.js')
    .js('resources/assets/js/ElectronicContract/app.js', 'public/js/electronic-contract.js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('resources/assets/js/Login/Employee/index.sass', 'public/css/LoginEmployee.css')
    .options({
    processCssUrls: false
}).version();
mix.copyDirectory(['public/images/vendor/jquery-ui-bundle'], 'public/css/images');
mix.copyDirectory(['node_modules/element-ui/lib/theme-chalk/fonts/element-icons.woff','node_modules/element-ui/lib/theme-chalk/fonts/element-icons.ttf'], 'public/css/fonts');
