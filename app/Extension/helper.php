<?php

function generateUrl($url, $param_array)
{
    return $url.'?'.http_build_query($param_array);
}

function array_to_json($string)
{
    return str_replace('null', 0, json_encode($string));
}

function roleToString(Array $array=[]){
    return 'role:'.implode(',',$array);
}

function hasAnyRole($functionCode){
//    if(session('userLogin')['userInfo']['userType'] == ORGANIZATION_ADMIN)
//        return true;
    if(is_array($functionCode)){
        foreach ($functionCode as $item){
            if(hasRole($item))
                return true;
        }
    }else{
        return hasRole($functionCode);
    }
    return false;
}

function hasRole($functionCode){
    if(in_array($functionCode,session('listFunctionCode',[])))
        return true;

    return false;
}

function slugName($string)
{

    $string_array = explode(' ',  removeSignVN($string));
    $slugName = '';
    foreach ($string_array as $item) {
        $slugName .= substr($item, 0, 1);
    }

    return str_slug(strtolower($slugName));
}

function strMilisecond($string)
{
    $time = explode(":", $string);

    $hour = intval($time[0]) * 60 * 60 * 1000;

    $minute = intval($time[1]) * 60 * 1000;

    //$second = explode(",", $time[2]);
    //$sec    = $second[0] * 1000;
    //$milisec= $second[1];
//$result = $hour + $minute + $sec + $milisec;

    $result = $hour + $minute;

    return $result;
}

function rebuildDate($string)
{
    $date = str_replace('/', '-', $string);
    return $date;
}

function date_to_milisecond($string,$beginOrEnd = null)
{
    $date = strtotime(rebuildDate($string)) * 1000;
    if($beginOrEnd=='begin')
    {
        $defaultTimeZone = date_default_timezone_get();
        date_default_timezone_set('GMT');
        $beginOfDay = new DateTime(rebuildDate($string).' 00:00:00');
        $date = $beginOfDay->getTimestamp() * 1000;
        date_default_timezone_set($defaultTimeZone);
    }
    if ($beginOrEnd=='end'){
        $defaultTimeZone = date_default_timezone_get();
        date_default_timezone_set('GMT');
        $endOfDay = new DateTime(rebuildDate($string).' 23:59:59');
        $date = $endOfDay->getTimestamp() * 1000;
        date_default_timezone_set($defaultTimeZone);
    }

    return $date;
}

function listDateThisWeek()
{
    $defaultTimeZone = date_default_timezone_get();
    date_default_timezone_set('GMT');
    $listDateThisWeek = [
        'Monday' => strtotime('this monday'),
        'Tuesday' => strtotime('this tuesday'),
        'Wednesday' => strtotime('this wednesday'),
        'Thursday' => strtotime('this thursday'),
        'Friday' => strtotime('this friday'),
        'Saturday' => strtotime('this saturday'),
        'Sunday' => strtotime('this sunday')
    ];
    date_default_timezone_set($defaultTimeZone);
    return $listDateThisWeek;
}
function removeSignVN ($str){

    $unicode = array(

        'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',

        'd'=>'đ',

        'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',

        'i'=>'í|ì|ỉ|ĩ|ị',

        'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',

        'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',

        'y'=>'ý|ỳ|ỷ|ỹ|ỵ',

        'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

        'D'=>'Đ',

        'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

        'I'=>'Í|Ì|Ỉ|Ĩ|Ị',

        'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

        'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

        'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',

    );

    foreach($unicode as $nonUnicode=>$uni){

        $str = preg_replace("/($uni)/i", $nonUnicode, $str);

    }
    //$str = str_replace(' ','_',$str);

    return $str;

}
/*hàm tạo thông báo javascipt*/
function MessageJS($message){
    return "Message('Thông báo','" . $message. "','');";
}

function checkMessage($code)
{
    return defined($code) ? constant($code) : $code;
}

function dev($param, $exit = true)
{
    echo "<pre>";
    var_dump($param);
    echo "</pre>";
    if($exit)
    {
        exit();
    }
}