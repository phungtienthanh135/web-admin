<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ReportController extends Controller
{

    /*public function reconciliation(Request $request)
    {
        $startDate = $request->has('startDate') ? date_to_milisecond($request->startDate) : strtotime('-1 month', time()) * 1000;
        $endDate = $request->has('endDate') ? date_to_milisecond($request->endDate) : time() * 1000;


        $result = $this->makeRequest('web_report/create-reconciliation-report',
            [
                'timeZone' => 7,
                'companyId' => session('companyId'),
                'startDate' => $startDate,
                'endDate' => $endDate
            ], ['DOBODY6969' => $this->TOKEN_SUPPER]);

        return view('cpanel.Report.reconciliation-report')->with([
            'result' => array_get($result['results'], 'report', [])
        ]);
    }*/

    public function balanceReport(Request $request)
    {
        $startDate = $request->has('startDate') ? date_to_milisecond($request->startDate, 'begin') : date_to_milisecond(date('d-m-Y'), 'begin');
        $endDate = $request->has('endDate') ? date_to_milisecond($request->endDate, 'end') : date_to_milisecond(date('d-m-Y'), 'end');

        $getItem = $this->makeRequestWithJson('items/getlist', [
            'page' => 0,
            'count' => 1000,
        ]);
        $params = [
            'timeZone' => 7,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ];
        if (!empty($request->itemId)) {
            $params = array_merge($params, [
                'itemId' => $request->itemId,
            ]);
        }

        $result = $this->makeRequestWithJson('report/create-summary-receipt-payment', $params);
        return view('cpanel.Report.balance-report')->with([
            'result' => array_get($result['results'], 'result', []),
            'getItem' => $getItem['results']['result'],
        ]);
    }

    public function profit(Request $request)
    {
        $startDate = $request->has('startDate') ? date_to_milisecond($request->startDate, 'begin') : date_to_milisecond(date('d-m-Y'), 'begin');
        $endDate = $request->has('endDate') ? date_to_milisecond($request->endDate, 'end') : date_to_milisecond(date('d-m-Y'), 'end');

        $reportRowProfit = [
            'routeId' => 'Tuyến',
            'date' => 'Ngày',
            'numberPlate' => 'Biển số',
            'sumPriceBeforePromotion' => 'Tiền vé',
            'increase' => 'Phát sinh',
            'ticketMoney' => 'Tiền sau khuyến mại',
            'paidMoney' => 'Đã thu',
            'needPaidMoney' => 'Phải thu',
            "totalPayment" => 'Tổng chi',
            "profit" => 'Lợi nhuận'
        ];

        $listRoute = $this->makeRequestWithJson('web_route/getlist', ['page'=>0,'count'=>1000]);
//        $params = [
//            'timeZone' => 7,
//            'startDate' => $startDate,
//            'endDate' => $endDate,
//            'option' => 1
//        ];
//        if ($request->routeId != null) {
//            $params['routeId'] = $request->routeId;
//        }
//        if ($request->driver != null) {
//            $params['driverId'] = $request->driver;
//        }
//        if ($request->numberPlate != null) {
//            $params['numberPlate'] = $request->numberPlate;
//        }
//        if ($request->has('ticketStatus')) {
//            $params['ticketStatus'] = array_to_json(explode(" ", $request->ticketStatus));
//        }
//        $result = $this->makeRequestWithJson('report/reconciliation-staff', $params);
//        dev($result);
        $listDriver = $this->makeRequestWithJson('user/getlist', ['page' => 0, 'count' => 100, 'listUserType' => [2]]);
        $listVehicle = $this->makeRequestWithJson('vehicle/getlist', ['page' => 0, 'count' => 100, 'vehicleStatus' => [2]]);
        if (isset($result['results']['listReport'][0]['note'])) $reportRowProfit['note'] = 'Ghi chú';
//        dev($result);
        return view('cpanel.Report.report-profit')->with([
//            'result' => array_get($result['results'], 'listReport', []),
            'reportRowProfit' => $reportRowProfit,
            'listRoute' => head(array_get($listRoute, 'results', [])),
            'listDriver' => $listDriver['results']['result'],
            'listVehicle' => $listVehicle['results']['result']
        ]);
    }

    public function detail(Request $request)
    {
        $listAllUser = [];
        $listDriver = [];
        $listAssistant = [];
        date_default_timezone_set('GMT');
        $startDate = $request->has('startDate') ? date_to_milisecond($request->startDate, 'begin') : date_to_milisecond(date('d-m-Y'), 'begin');
        $endDate = $request->has('endDate') ? date_to_milisecond($request->endDate, 'end') : date_to_milisecond(date('d-m-Y'), 'end');
        $response_route = $this->makeRequestWithJson('route/getlist', null);
        $response_Vehicle = $this->makeRequestWithJson('vehicle/getlist', [
            'page' => 0,
            'count' => 1000
        ]);
        $response = $this->makeRequestWithJson('user/getlist', [
            'page' => 0,
            'count' =>200,
            'listUserType'=>[2,3,4,5,6]
        ]);
        $listDepartment = $this->makeRequest('department/getlist', []);
        return view('cpanel.Report.report-detail')->with(
            [
                'listRoute' => head(array_get($response_route, 'results', [])),
                'listVehicle' => head(array_get($response_Vehicle, 'results', [])),
                'listDriver' => $listDriver,
                'listAssistant' => $listAssistant,
                'listDepartment' => $listDepartment['results']['listDepartment'],
                'listAllUser' => $listAllUser,
                'cashierUser'=>isset($response['results']['result'])?$response['results']['result']:[]
            ]);

    }

    public function summaryByDate(Request $request)
    {
        date_default_timezone_set('GMT');
        $startDate = $request->has('startDate') ? date_to_milisecond($request->startDate) / 1000 : time();
        $endDate = $request->has('endDate') ? date_to_milisecond($request->endDate) / 1000 : time();

        $response_report = $this->makeRequest('web_report/create-summary-report', [
            'timeZone' => 7,
            'companyId' => session('companyId'),
            'startDate' => mktime(0, 0, 0, date("m", $startDate), date("d", $startDate), date("Y", $startDate)) * 1000,
            'endDate' => mktime(23, 59, 59, date("m", $endDate), date("d", $endDate), date("Y", $endDate)) * 1000
        ]);
        return view('cpanel.Report.report-summary-by-date')->with(['report' => array_get($response_report['results'], 'summaryReport', [])]);
    }

    public function summaryByMonth(Request $request)
    {
        $month = $request->has('month') ? $request->month : getdate()['mon'];
        $year = $request->has('year') ? $request->year : getdate()['year'];
        $numberPlate = $request->numberPlate;
        $routeId = $request->routeId;
        $response_route = $this->makeRequest('web_route/getlist', []);
        $response_Vehicle = $this->makeRequestWithJson('vehicle/getlist', [
            'page' => 0,
            'count' => 1000,
        ]);
        if ($request->month == 'all') {
            $all_month = [];
            for ($i = 1; $i < 13; $i++) {
                $response_all = $this->makeRequest('web_report/create-summary-report-by-month', [
                    'timeZone' => 7,
                    'companyId' => session('companyId'),
                    'month' => $i,
                    'year' => $year,
                    'numberPlate' => $numberPlate,
                    'routeId' => $routeId
                ]);
                array_push($all_month, head($response_all['results']));
            }
            $result = $all_month;
        } else {
            $response_report = $this->makeRequest('web_report/create-summary-report-by-month', [
                'timeZone' => 7,
                'companyId' => session('companyId'),
                'month' => $month,
                'year' => $year,
                'numberPlate' => $numberPlate,
                'routeId' => $routeId
            ]);
            $result = array_get($response_report['results'], 'report', []);
        }

        return view('cpanel.Report.report-summary-by-month')->with([
            'result' => $result,
            'listVehicle' => head(array_get($response_Vehicle, 'results', [])),
            'listRoute' => head(array_get($response_route, 'results', []))
        ]);
    }

    private function _getAllUser()
    {
        $response = $this->makeRequestWithJson('user/getlist', [
            'page' => 0,
            'count' => 1000,
//            'userType'=> "".DRIVER.",".ASSISTANT."",
            //'verifyStatus'=>'',
            //'userStatus'=>1,
        ]);

        return $response['results'];
    }

    public function listAgency(Request $request)
    {
//        $report = $this->makeRequest('web_admin/get-list-agency',[
//            'page'=>0,
//            'count'=>1000,
//            'agencyCompanyId'=>session('companyId')
//        ]);
        $route = $this->makeRequest('web_route/getlist', [
            'page' => 0,
            'count' => 1000
        ]);
        $vehicle = $this->makeRequestWithJson('vehicle/getlist', [
            'page' => 0,
            'count' => 1000
        ]);
//        dev($route,false);
//        dev($vehicle);
        return view('cpanel.Report.listAgency')->with(['routes' => $route['results']['result'], 'vehicles' => $vehicle['results']['result']]);
    }

    public function accountantReport(Request $request)
    {
        $startDate = $request->has('startDate') ? date_to_milisecond($request->startDate, 'begin') : date_to_milisecond(date('d-m-Y'), 'begin');;
        $endDate = $request->has('endDate') ? date_to_milisecond($request->endDate, 'end') : date_to_milisecond(date('d-m-Y'), 'end');;
        $params = [
            'timeZone' => 7,
            'companyId' => session('companyId'),
            'startDate' => $startDate,
            'endDate' => $endDate
        ];
        $result = $this->makeRequestWithJson('report/export', $params);
        return view('cpanel.Report.accountant-report')->with([
            'report' => $result['results']['report']
        ]);
    }

    public function onlineReport()
    {
        $result = $this->makeRequestWithJson('admin/get-online', [
            'companyId' => session('companyId')
        ]);
        $data = array_get($result['results'], 'results', []);
        $newArray = [];
        for ($i = 0; $i < count($data); $i++) {
            $newArray = array_merge($newArray, array_values($data)[$i]);
        }
        $newArray = array_filter($newArray, function ($v) {
            return ((date_to_milisecond('8/3/2018', 'begin') <= $v['createdDate']) && (date_to_milisecond('9/3/2018', 'end') >= $v['createdDate']));
        });
        dev($newArray);
        return view('cpanel.Report.get-online')->with([
            'result' => $newArray
        ]);
    }

    public function messageReport(Request $request)
    {
        $startDate = $request->startDate ? date("Y-m-d", strtotime($request->startDate)) : date("Y-m-d");
        $endDate = $request->endDate ? date("Y-m-d", strtotime($request->endDate)) : date("Y-m-d");

        $startDate = (str_replace("-", "", $startDate));
        $endDate = (str_replace("-", "", $endDate));
        $result = $this->makeRequestWithJson('/report/sms', [
            'companyId' => session('companyId'),
            'startDate' => $startDate,
            'endDate' => $endDate,
            'page' => 0,
            'count' => 10000
        ]);
        $money = $this->makeRequest('ePay/getSMSMegaV', null);
//        dev($money);
        if ($money['code'] == 200 && count($money['results']) > 0) {
            $money = $money['results']['megaVSMS'];
        } else {
            $money = null;
        }
        return view('cpanel.Report.report_message')->with(['result' => $result['results'], 'money' => $money, 'msg' => '']);
    }

    public function topUp(Request $request)
    {//nạp tiền tin nhắn
        $param = [
            'objectId' => $request->objectId,
            'paymentType' => "1",
            'amount' => $request->amount
        ];
        $res = $this->makeRequestWithJson('/ePay/createTransactionSMSMegaV', $param);
        $res['param'] = $param;
        if ($res['code'] == 200) {
            return Redirect::to($res['results']['redirect']);
        } else {
            return redirect()->back()->with(['msg' => 'Có lỗi xảy ra vui lòng tải lại trang']);
        }
    }

    public function debtorsReport(Request $request)
    {
        $startDate = $request->has('startDate') ? date_to_milisecond($request->startDate, 'begin') : date_to_milisecond(date('d-m-Y'), 'begin');;
        $endDate = $request->has('endDate') ? date_to_milisecond($request->endDate, 'end') : date_to_milisecond(date('d-m-Y'), 'end');;
        $page = $request->has('page') ? ($request->page) : 0;
        $agencyId = ($request->agencyId && $request->agencyId != -1 && $request->agencyId != 'Tất cả') ? $request->agencyId : '';
        $param = [
            'page' => $page,
            'count' => 10,
            'timeZone' => 7,
            'companyId' => session('companyId'),
            'startDate' => $startDate,
            'endDate' => $endDate,
            'sourceId' => $agencyId
        ];
        if ($request->routeId) $param = array_merge($param, [
            'routeId' => $request->routeId
        ]);
        //dev($param);
        $result = $this->makeRequestWithJson('/report/create_report_agency', $param);
        $listRoute = $this->makeRequestWithJson('route/getlist', null);
        //dev($listRoute);
        return view('cpanel.Report.report_debtors', ['results' => (isset($result['results']['reportAgency']) ? $result['results']['reportAgency'] : $result), 'page' => $page, 'listRoute' => $listRoute['results']['result']]);
    }

    public function depreciationReport(Request $request)
    {

        $result = $this->makeRequest('/report/create_report_depreciation');
        //dev($result);
        return view('cpanel.Report.report-depreciation', ['results' => (isset($result['results']['depreciationReport']) ? $result['results']['depreciationReport'] : $result)]);
    }

    public function getCustomer(Request $request)
    {
        if(is_null($request->startDate) xor is_null($request->endDate)) {
            $date = date_to_milisecond($request->startDate) + date_to_milisecond($request->endDate);
            $param['startDate'] = $date;
            $param['endDate'] = $date+24*3600*1000;
        } else {
            $param['startDate'] = $request->startDate ? date_to_milisecond($request->startDate) : date_to_milisecond(date('d-m-Y'), 'begin');
            $param['endDate'] = $request->endDate ? date_to_milisecond($request->endDate) : date_to_milisecond(date('d-m-Y'), 'end');
        }
        if ($request->phoneNumber) $param['phoneNumber'] = $request->phoneNumber;
        if ($request->status) $param['ticketStatus'] = $request->status;
        if ($request->paymentType) $param['paymentType'] = $request->paymentType;
        if ($request->routeId) $param['routeId'] = $request->routeId;
        $result = $this->makeRequestWithJson('/report/summaryCustomer', $param);
        $route = $this->makeRequestWithJson('route/getlist', null);
        return view('cpanel.Report.report-customer',
            ['results' => (isset($result['results']['customerReport']) ? $result['results']['customerReport'] : $result['code']),
                'route' => (isset($route['results']['result']) ? $route['results']['result'] : $route['code'])]);


    }

    public function getPoint(Request $request)
    {
        $listRoute = $this->makeRequestWithJson('route/getlist', null);
        $startDate = $request->has('startDate') ? date_to_milisecond($request->startDate, 'begin') : date_to_milisecond(date('d-m-Y'), 'begin');;
        $endDate = $request->has('endDate') ? date_to_milisecond($request->endDate, 'end') : date_to_milisecond(date('d-m-Y'), 'end');;

        $param = [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'routeId' => ($request->routeId ? $request->routeId : $listRoute['results']['result'][0]['routeId'])
        ];
        if (!$request->routeId) $route = $listRoute['results']['result'][0];
        else
            foreach ($listRoute['results']['result'] as $row) {
                if ($row['routeId'] == $request->routeId) $route = $row;
            }
        if ($listRoute['results']['result'][0]['routeId'])
            $result = $this->makeRequestWithJson('report/summaryByRoute', $param);
        return view('cpanel.Report.report-point',
            ['listRoute' => $listRoute['results']['result'], 'result' => $result['results']['reportByRoute'], 'route' => $route]);
    }

    public function chartByMonth()
    {
        $response_route = $this->makeRequest('web_route/getlist', []);
        return view('cpanel.Report.charByMonth')->with([
            'listRoute' => head(array_get($response_route, 'results', []))
        ]);
    }

    public function chartByMonthAll()
    {
        $response_route = $this->makeRequest('web_route/getlist', []);
        return view('cpanel.Report.charByMonthAll')->with([
            'listRoute' => head(array_get($response_route, 'results', []))
        ]);
    }
}
