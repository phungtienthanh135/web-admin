<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RouteController extends Controller
{

    public function show(Request $request)
    {

        $page = $request->has('page') ? $request->page : 1;

        $response = $this->makeRequest('web_route/getlist', [
            'page' => $page - 1,
            'count' => 100,
            'companyId'=>session('companyId'),
            'startPoint' => $request->startPoint,
            'endPoint' => $request->endPoint,
            'routeName' => $request->routeName
        ]);
//     dev($response['results']['result'][0]);
        return view('cpanel.Route.show', [
            'result' => $response['results']['result'],
            'page' => $page
        ]);

    }

    public function search(Request $request)
    {
        $result = [];
        if ($request->has('routeName')) {
            $response = $this->makeRequest('web_route/getlist', [
                'page' => 0,
                'count' => 100,
                'routeName' => $request->routeName
            ]);
            $result = $response['results']['result'];
        }

        if ($request->has('routeId')) {
            $response = $this->makeRequest('web_route/view-all', ['routeId' => $request->routeId]);
            $result = $response['results'];
        }

        return response()->json($result, 200, [], JSON_PRETTY_PRINT);
    }

    public function getEdit($id)
    {
        $response_vehicleType = $this->makeRequest('web_vehicletype/getlist', [
            'page' => 0,
            'count' => 100,
            'companyId' => session('companyId')
        ]);
        $vehicleTypeSorted = array_sort($response_vehicleType['results']['result'], function ($item) {
            return $item['vehicleTypeName'];
        });
        $listRound = $this->makeRequest('web_route/getlist');
        $listRound['results']['result'] = array_filter($listRound['results']['result'], function ($val) use ($id) {
            return $val['routeId'] != $id;
        });
        $listRound['results']['result'] = array_values($listRound['results']['result']);

//        dev($response['results']);

        $response = $this->makeRequest('web_route/view-all', ['routeId' => $id]);
        $listImage = (isset($response['results']['route']['listImages']) && !empty($response['results']['route']['listImages'])) ? $response['results']['route']['listImages'][0] : '';
        return view('cpanel.Route.edit')->with([
            'resultRoute' => $response['results'],
            'listVehicleType' => $vehicleTypeSorted,
            'listImage' => $listImage,
            'listRound' => $listRound
        ]);
    }

    public function postEdit(Request $request)
    {

        $listPoint = json_decode($request->listPoint, true);

        //$listPriceByVehicleType = $request->listPriceByVehicleType;
        //$listPriceWeightAndPoint = $request->listPriceWeightAndPoint;

        $listMealPrice = [];
        $listPickUpHome = [];
        $listTimeIntend = [];
        $listPointId = [];/*echo "<pre>";var_dump($listPoint);echo "</pre>";exit();*/

        foreach ($listPoint as $index => $item) {
            $listMealPrice[$index] = isset($item['mealprice']) ? $item['mealprice'] : 0;
            $listPickUpHome[$index] = isset($item['pickUpHome']) ? $item['pickUpHome'] : -1;
            $listTimeIntend[$index] = $item['TimeIntend'];
            $listPointId[$index] = $item['id'];
        }

        $routeName = $request->routeName;
//        $routeNameShort = slugName($routeName);
        $routeNameShort = $request->routeNameShort;
        $listVehicleType = array_to_json($request->listVehicleType);
        $childrenTicketRatio = $request->childrenTicketRatio / 100;
        $listPriceByVehicleType = $request->listPrice;


        $listMealPrice = array_to_json($listMealPrice);
        $listPickUpHome = array_to_json($listPickUpHome);
        $listPointId = array_to_json($listPointId);
        $listTimeIntend = array_to_json($listTimeIntend);

        $listImages = array_to_json(array($request->listImage));
        $phoneNumber = '';
        $displayPrice = 0;
        if (!is_null($request->displayPrice)) {
            $displayPrice = $request->displayPrice;
        }

        //$listPriceWeightAndPoint = array_to_json($listPriceWeightAndPoint);
        //$listDimention = '[]';
        //$listPriceDimentionAndPoint = '[]';
        //$listWeight = array_to_json($request->listWeight);
        $listTransshipment = $request->listTransshipment;
        $listTransshipment = explode(',', $listTransshipment);
        $param = [
            'listLockRoute' => $request->listNotSell,
            'goodsPriceRatio' => ($request->displayPriceShip / 100),
            'routeBack' => $request->roundId,
            'routeId' => $request->routeId,
            'routeName' => $routeName,
            'routeNameShort' => $routeNameShort,
            'listPointId' => $listPointId,
            'listPriceByVehicleType' => $listPriceByVehicleType,
            'listMealPrice' => $listMealPrice,
            'listTimeIntend' => $listTimeIntend,
            'displayPrice' => $displayPrice,
            'displayPriceIndex' => $request->displayPriceIndex,
            //'listVehicleType' => $listVehicleType,
            'childrenTicketRatio' => $childrenTicketRatio,
            'listImages' => $listImages,
            'listPickUpHome' => $listPickUpHome,
            'phoneNumber' => $phoneNumber,
            'style' => 0
            //'listWeight' => $listWeight,
            //'listPriceWeightAndPoint' => $listPriceWeightAndPoint,
            // 'listPriceDimentionAndPoint' => $listPriceDimentionAndPoint,
            //'listDimention' => $listDimention,
        ];
        if ($request->listTransshipment != '0' && $request->listTransshipment != null) $param = array_merge($param, [
            'listTransshipmentPoint'=> $request->listTransshipment
        ]);

//         dev($request->listTransshipment);
        $response = $this->makeRequest('web_route/update', $param);

        if ($response['status'] == 'success') {

            return redirect()->action('RouteController@show')->with(['msg' => "Message('Thông báo','Cập nhật tuyến thành công','');"]);
        } else {
            return redirect()->back()->withInput()->with(['msg' => "Message('Thông báo','Cập nhật tuyến thất bại<br>" . checkMessage($response['results']['error']['propertyName']) . "','');"]);
        }
    }


    public function getadd()
    {
        $response_vehicleType = $this->makeRequest('web_vehicletype/getlist', [
            'page' => 0,
            'count' => 100,
            'companyId' => session('companyId')
        ]);
        $vehicleTypeSorted = array_sort($response_vehicleType['results']['result'], function ($item) {
            return $item['vehicleTypeName'];
        });
        $listRound = $this->makeRequest('web_route/getlist');
        return view('cpanel.Route.add')->with([
            'listVehicleType' => $vehicleTypeSorted,
            'listRound' => $listRound
        ]);
    }

    public function postadd(Request $request)
    {
        $listPoint = json_decode($request->listPoint, true);

        //$listPriceByVehicleType = $request->listPriceByVehicleType;
        //$listPriceWeightAndPoint = $request->listPriceWeightAndPoint;

        $listMealPrice = [];
        $listPickUpHome = [];
        $listTimeIntend = [];
        $listPointId = [];
        $displayPrice = 0;
        if (!is_null($request->displayPrice)) {
            $displayPrice = $request->displayPrice;
        }

        foreach ($listPoint as $index => $item) {
            $listMealPrice[$index] = $item['mealprice'];
            $listPickUpHome[$index] = $item['pickUpHome'];
            $listTimeIntend[$index] = $item['TimeIntend'];
            $listPointId[$index] = $item['id'];
        }


        $routeName = $request->routeName;
//        $routeNameShort = slugName($routeName);
        $routeNameShort = $request->routeNameShort;
        $listVehicleType = array_to_json($request->listVehicleType);
        $childrenTicketRatio = $request->childrenTicketRatio / 100;
        $listPriceByVehicleType = $request->listPrice;


        $listMealPrice = array_to_json($listMealPrice);
        $listPickUpHome = array_to_json($listPickUpHome);
        $listPointId = array_to_json($listPointId);
        $listTimeIntend = array_to_json($listTimeIntend);

        $listImages = array_to_json(array($request->listImage));

        $phoneNumber = '';

        //$listPriceWeightAndPoint = array_to_json($listPriceWeightAndPoint);
        //$listDimention = '[]';
        //$listPriceDimentionAndPoint = '[]';
        //$listWeight = array_to_json($request->listWeight);
        $listTransshipment = $request->listTransshipment;
        $listTransshipment = explode(',', $listTransshipment);
        $param = [
            'listLockRoute' => $request->listNotSell,
            'goodsPriceRatio' => ($request->displayPriceShip / 100),
            'routeBack' => $request->roundId,
            'routeName' => $routeName,
            'routeNameShort' => $routeNameShort,
            'listPointId' => $listPointId,
            'listPriceByVehicleType' => $listPriceByVehicleType,
            'listMealPrice' => $listMealPrice,
            'listTimeIntend' => $listTimeIntend,
            'displayPrice' => $displayPrice,
            'displayPriceIndex' => $request->displayPriceIndex,
            'childrenTicketRatio' => $childrenTicketRatio,
            'listImages' => $listImages,
            'listPickUpHome' => $listPickUpHome,
            'phoneNumber' => $phoneNumber,
            'style' => 0
            //'listVehicleType' => $listVehicleType,
            //'listWeight' => $listWeight,
            //'listPriceWeightAndPoint' => $listPriceWeightAndPoint,
            // 'listPriceDimentionAndPoint' => $listPriceDimentionAndPoint,
            //'listDimention' => $listDimention,
        ];
        if ($request->listTransshipment !='0' && $request->listTransshipment != null) $param = array_merge($param, [
            'listTransshipmentPoint' => $request->listTransshipment
        ]);
       //dev($param);
        $response = $this->makeRequest('web_route/create', $param);
        //dev($response);
        if ($response['status'] == 'success') {
            return redirect()->action('RouteController@show')->with(['msg' => "Message('Thông báo','Thêm tuyến thành công','');"]);
        } else {
            return redirect()->back()->withInput()->with(['msg' => "Message('Thông báo','Thêm tuyến thất bại<br>" . checkMessage($response['results']['error']['propertyName']) . "','');"]);
        }

    }

    function delete(Request $request)
    {
        $routeId = $request->routeId;
        $response = $this->makeRequest('web_route/delete', ['routeId' => $routeId]);

        if ($response['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Xoá tuyến thành công','');"]);
        } else {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Xoá tuyến thất bại<br>" . checkMessage($response['results']['error']['propertyName']) . "','');"]);
        }

    }

    function sort(){
        $res = $this->makeRequest('web_route/getlist', [
            'page' => 0,
            'count' => 200,
            'companyId'=>session('companyId')
        ]);
        $routes = [];
        if($res['status']=="success"){
            $routes = $res['results']['result'];
        }
        return view('cpanel.Route.sort')->with(['routes'=>$routes]);
    }
}
