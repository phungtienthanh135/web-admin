<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FeedbackController extends Controller
{

    public function show(Request $request){
        $page = $request->has('page') ? $request->page : 1;

        $response = $this->makeRequest(generateUrl('web_feedback_company/getList_feedback',[
            'page'=>$page - 1,
            'count'=>10,
            'companyId'=>session('companyId')
        ]));
        return view('cpanel.Feedback.show')->with([
            'result' =>array_get($response['results'],'result',[]),
            'page' => $page
        ]);
    }

    public  function  postAdd(Request $request){
        $response = $this->makeRequest('web_admin/staff-register',
            [
                'feedbackType' => $request->feedbackType,
                'content' => $request->_content,
                'value' => $request->value,
                'companyId' => session('companyId'),
                'tripId' => $request->tripId,
                'ticketId' => $request->ticketId,
                'title' => $request->title
            ]);

        if ($response['status']=='success')
        {
            return redirect()->back()->with(['msg'=>"Message('Thành công','Tạo phản hồi thành công','');"]);
        }else
        {

            return redirect()->back()->withInput()->with(['msg'=>"Message('Thất bại','Tạo phản hồi thành công thất bại <br>Mã lỗi: ".checkMessage($response['results']['error']['propertyName'])."','');"]);
        }
    }
}
