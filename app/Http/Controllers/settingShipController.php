<?php
/**
 * Created by PhpStorm.
 * User: My Laptop
 * Date: 5/29/2018
 * Time: 5:21 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class settingShipController extends Controller
{


    public function setup()
    {
        $results = DB::table('config_table_ship')
            ->rightJoin('attribute_table_ship', function ($join) {
                $join->on('config_table_ship.idAttribute', '=', 'attribute_table_ship.id')
                    ->where('config_table_ship.companyId', '=', session('companyId'));
            })->orderBy('id', 'asc')
            ->get();
       $results= json_decode(json_encode($results), True);
       foreach ($results as $k=>$v){
           if (in_array($v['id'],[2,5,9,14,23,24,25])) {
              $results[$k]['status']=1;
              $results[$k]['alias']='';
           }
       }
        return view('cpanel/System/settingShip')->with(['result' => $results]);
    }

    public function updateSetting(Request $request)
    {
        $listAttr = json_decode($request->listAttribute);
        $listAttr = json_decode(json_encode($listAttr), True);
        foreach ($listAttr as $node) {
            foreach ($node as $row) {
                $listUse[]=$row['idAttribute'];
            }}
        $listDelete = DB::table('config_table_ship')
            ->where('companyId', '=', session('companyId'))
            ->whereNotIn('idAttribute',$listUse)
            ->get();
        if($listDelete!=null){
            $listDelete=json_decode(json_encode($listDelete),true);
            foreach ($listDelete as $delete){
                DB::table('config_table_ship')
                    ->where([['companyId', '=', session('companyId')],['idAttribute','=',$delete['idAttribute']]])
                    ->delete();
            }
        }
        foreach ($listAttr as $node) {
            foreach ($node as $row) {
                $checkExist = DB::table('config_table_ship')->where([['companyId', '=', session('companyId')], ['idAttribute', '=', $row['idAttribute']]])->count();
                if ($checkExist != null) {
                    DB::table('config_table_ship')
                        ->where([['companyId', '=', session('companyId')], ['idAttribute', '=', $row['idAttribute']]])
                        ->update(
                            ['status' => 1,
                                'size' => (int)$row['size'],
                                'bold' => (int)$row['bold'],
//                                'width' => (int)$row['width'],
//                                'height' => (int)$row['height'],
                                'orderDisplay' => (int)$row['orderDisplay'],
                                'alias' => $row['alias']]
                        );
                } else {
                    DB::table('config_table_ship')
                        ->insert([
                            'companyId' => session('companyId'),
                            'idAttribute' => $row['idAttribute'],
                            'status' => 1,
                            'size' => (int)$row['size'],
                            'bold' => $row['bold'],
//                            'width' => (int)$row['width'],
//                            'height' => (int)$row['height'],
                            'orderDisplay' => (int)$row['orderDisplay'],
                            'alias' => $row['alias']
                        ]);

                }
            }
        }
        return redirect()->back()->with(['msg' => "Message('Thành công','Cập nhật cài đặt thành công','');"]);
    }
}