<?php

namespace App\Http\Controllers;

use App\Http\Requests\ScheduleRequest;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
     const ONE_DATE = 24*60*60*1000;

    function show(Request $request)
    {
        $page = $request->has('page') ? $request->page : 1;

        $startDate = $request->has('startDate') ? date_to_milisecond($request->startDate,'beign') : 0;
        $endDate = $request->has('endDate') ? date_to_milisecond($request->endDate,'end') : strtotime('+1 month') * 1000;
        $routeId = $request->routeId;
        $type = $request->type;
        $numberPlate = $request->numberPlate;
        $response_route = $this->makeRequest('web_route/getlist',[
            'page'=>0,
            'count'=>100,
        ]);
        $response_Vehicle =  $this->makeRequestWithJson('vehicle/getlist',[
            'page'=>0,
            'count'=>1000,
        ]);
        $result = $this->makeRequest('web_schedule/getlist', [
            'count' => 10,
            'page' => $page - 1,
            'timeZone' => 7,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'routeId'=>$routeId,
            'type'=>$type,
            'numberPlate'=>$numberPlate,
        ]);

        return view('cpanel.Schedule.show')->with([
            'result' =>array_get($result['results'],'result',[]),
            'page' => $page,
            'listVehicle'=>head($response_Vehicle['results']),
            'listRoute'=>head($response_route['results'])
        ]);
    }

    function getAdd()
    {

        $listDriver = $this->makeRequestWithJson('user/getlist',['page'=>0,'count'=>100,'listUserType'=>[2]]);
        $listAssistant = $this->makeRequestWithJson('user/getlist',['page'=>0,'count'=>100,'listUserType'=>[3]]);
        $listVehicle = $this->makeRequestWithJson('vehicle/getlist',['page'=>0,'count'=>100,'vehicleStatus'=>[2]]);
        $listVehicleType = $this->makeRequest('web_vehicletype/getlist',['page'=>0,'count'=>100]);
        $listRoute = $this->makeRequest('web_route/getlist',['page'=>0, 'count'=>100]);

        return view('cpanel.Schedule.add')
            ->with([
                'listDriver' => $listDriver['results']['result'],
                'listAssistant' => $listAssistant['results']['result'],
                'listVehicle' => $listVehicle['results']['result'],
                'listVehicleType' => $listVehicleType['results']['result'],
                'listRoute' => $listRoute['results']['result'],
//                'listDateThisWeek' => listDateThisWeek()
            ]);
    }

    function postAdd(ScheduleRequest $request)
    {
        $timeZone = 7;
        $scheduleType = $request->scheduleType;
        $cycle = $request->cycle;
        $numberOfCycle = $request->numberOfCycle;
        $startDate = date_to_milisecond($request->startDate, 'begin');
        $endDate = date_to_milisecond($request->endDate, 'begin');
        $listAssistantId = !is_null($request->listAssistantId) && count($request->listAssistantId)>0 ? array_to_json($request->listAssistantId) : '';
        $listDriverId = !is_null($request->listDriverId) && count($request->listDriverId)>0 ? array_to_json($request->listDriverId) : '';
        $routeId = $request->routeId;
        $vehicleId = $request->vehicleId;
//        $seatMapId = $request->seatMapId;
        $listDate = $request->listDate;
        $startTime = strMilisecond($request->startTime)-($timeZone*60*60*1000);
//        if($startTime<0)
//        {
////            $startTime+=self::ONE_DATE;
//            foreach ($listDate as $key => $date)
//            {
//                $listDate[$key] = $date - self::ONE_DATE;
//            }
//        }
        $listDate = array_to_json($listDate);
        switch ($request->scheduleType) {
            case 1:
                {
                    $name = 'Lịch không lặp';
                    $listDate = array_to_json([$startDate]);
                    break;
                }
            case 2:
                {
                    $name = 'Lịch lặp hữu hạn';
                    break;
                }
            case 3:
                {
                    $name = 'Lịch lặp vô hạn';
                    break;
                }
            default:
                {
                    $name = 'Lịch không xác định';
                    break;
                }
        }
        $params = [
            'timeZone' => $timeZone,
            'name' => $name,
            'scheduleType' => $scheduleType,
            'cycle' => $cycle,//k bắt buộc
            'numberOfCycle' => $numberOfCycle,//k bắt buộc
            'startDate' => $startDate,
            'endDate' => $endDate,//k bắt buộc
            'listAssistantId' => $listAssistantId,//k bắt buộc
            'listDriverId'=>$listDriverId,//k bắt buộc
            'listDate' => $listDate,
            'routeId' => $routeId,
            //'seatMapId' => $seatMapId,
            'startTime' => $startTime,
            'vehicleId' => $vehicleId,
        ];
//        dev($params);

        $result = $this->makeRequest('web_schedule/create', $params);

        if($request->ajax()){
            return response()->json($result);
        }else{
            if ($result['status'] == 'success') {
                return redirect()->action('ScheduleController@show')->with(['msg' => "Message('Thông báo','Thêm lịch thành công','');"]);
            } else {
                echo generateUrl('web_schedule/create', [
                    'timeZone' => $timeZone,
                    'name' => $name,
                    'scheduleType' => $scheduleType,
                    'cycle' => $cycle,
                    'numberOfCycle' => $numberOfCycle,
                    'startDate' => $startDate,
                    'endDate' => $endDate,
//                'listAssistantId' => $listAssistantId,
//                'listDriverId'=>$listDriverId,
                    'listDate' => $listDate,
                    'routeId' => $routeId,
                    'vehicleId' => $vehicleId,
                    //'seatMapId' => $seatMapId,
                    'startTime' => $startTime,

                ]);
                return redirect()->back()->withInput()->with(['msg' => "Message('Thông báo','Thêm lịch thất bại <br>" . checkMessage($result['results']['error']['propertyName']) . "','');"]);
            }
        }
    }

    function getEdit($id)
    {
        $schedule = $this->makeRequest('web_schedule/view',['scheduleId'=>$id]);
        $listDriver = $this->makeRequestWithJson('user/getlist',['page'=>0,'count'=>100,'listUserType'=>[2]]);
        $listAssistant = $this->makeRequestWithJson('user/getlist',['page'=>0,'count'=>100,'listUserType'=>[3]]);
        $listVehicle = $this->makeRequestWithJson('vehicle/getlist',['page'=>0,'count'=>100,'vehicleStatus'=>[2]]);
        $listVehicleType = $this->makeRequest('web_vehicletype/getlist',['page'=>0,'count'=>100]);
        $listRoute = $this->makeRequest('web_route/getlist',['page'=>0, 'count'=>100]);
        $startTime = $schedule['results']['schedule']['startTime'] - (7 * 60 * 60 * 1000);
        $schedule['results']['schedule']['listDate'] = array_map(
            function ($item) use ($startTime) {
                return $item += $startTime;
            },
            $schedule['results']['schedule']['listDate']);
//        dev($schedule);
        return view('cpanel.Schedule.edit')
            ->with([
                'schedule' => $schedule['results']['schedule'],
                'listDriver' => $listDriver['results']['result'],
                'listAssistant' => $listAssistant['results']['result'],
                'listVehicle' => $listVehicle['results']['result'],
                'listVehicleType' => $listVehicleType['results']['result'],
                'listRoute' => $listRoute['results']['result'],
//                'listDateThisWeek' => listDateThisWeek(),
                // 'listDriverId' => $listDriverId,
            ]);
    }

    function postEdit(Request $request,$id)
    {
        $timeZone = 7;
        $scheduleType = $request->scheduleType;
        $cycle = $request->cycle;
        $numberOfCycle = $request->numberOfCycle;
        $startDate = date_to_milisecond($request->startDate, 'begin');
        $endDate = date_to_milisecond($request->endDate, 'begin');
        $listAssistantId = !is_null($request->listAssistantId)&&count($request->listAssistantId)>0 ? array_to_json($request->listAssistantId) : '';
        $listDriverId = !is_null($request->listDriverId)&&count($request->listDriverId)>0 ? array_to_json($request->listDriverId) : '';
        $routeId = $request->routeId;
        $vehicleId = $request->vehicleId;
        $seatMapId = $request->seatMapId;

        $listDate = $request->listDate;
        $startTime = strMilisecond($request->startTime)-($timeZone*60*60*1000);

//        if($startTime<0)
//        {
//            $startTime+=self::ONE_DATE;
//            foreach ($listDate as $key => $date)
//            {
//                $listDate[$key] = $date - self::ONE_DATE;
//            }
//        }
        $listDate = array_to_json($listDate);
        switch ($request->scheduleType) {
            case 1:
                {
                    $name = 'Lịch không lặp';
                    $listDate = array_to_json([$startDate]);
                    break;
                }
            case 2:
                {
                    $name = 'Lịch lặp hữu hạn';
                    break;
                }
            case 3:
                {
                    $name = 'Lịch lặp vô hạn';
                    break;
                }
            default:
                {
                    $name = 'Lịch không xác định';
                    break;
                }
        }

        $result = $this->makeRequest('web_schedule/update', [
            'timeZone' => $timeZone,
            'name' => $name,
            'scheduleType' => $scheduleType,
            'cycle' => $cycle,
            'numberOfCycle' => $numberOfCycle,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'listAssistantId' => $listAssistantId,
            'listDriverId'=>$listDriverId,
            'listDate' => $listDate,
            'routeId' => $routeId,
            'vehicleId' => $vehicleId,
            'seatMapId' => $seatMapId,
            'startTime' => $startTime,
            'scheduleId'=>$id

        ]);

        if ($result['status'] == 'success') {
            return redirect()->action('ScheduleController@show')->with(['msg' => "Message('Thông báo','Cập nhật lịch thành công','');"]);
        } else {

            return redirect()->back()->withInput()->with(['msg' => "Message('Thông báo','Cập nhật lịch thất bại <br>" . $result['results']['error']['propertyName'] . "','');"]);
        }
    }

    function delete(Request $request)
    {
        $page = $request->has('page') ? $request->page : 1;
        $result = $this->makeRequest('web_schedule/delete',['scheduleId'=> $request->scheduleId]);
        if ($result['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Xoá lịch thành công','');", 'page' => $page]);
        } else {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Xoá lịch thất bại','');", 'page' => $page]);
        }
    }

    /*Ty xoa*/
    public function listDateThisWeek(Request $request)
    {
        $defaultTimeZone = date_default_timezone_get();
        date_default_timezone_set('GMT');
        $listDateThisWeek = [
            'Monday' => strtotime('monday '.$request->date)*1000,
            'Tuesday' => strtotime('tuesday '.$request->date)*1000,
            'Wednesday' => strtotime('wednesday '.$request->date)*1000,
            'Thursday' => strtotime('thursday '.$request->date)*1000,
            'Friday' => strtotime('friday '.$request->date)*1000,
            'Saturday' => strtotime('saturday '.$request->date)*1000,
            'Sunday' => strtotime('sunday '.$request->date)*1000
        ];
        date_default_timezone_set($defaultTimeZone);
        return json_encode($listDateThisWeek);
    }
}
