<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustomerController extends Controller
{

    public function show(Request $request)
    {
        $page = $request->has('page') ? $request->page : 1;
        $phoneNumber = $request->phoneNumber != null ? $request->phoneNumber :'';
        $params = [
            'timeZone' => 7,
            'page' => $page - 1,
            'count' => 10,
            'companyId' => session('companyId'),
            'phoneNumber' => $phoneNumber,
        ];

        if (!empty($request->status)) {
            $params = array_merge($params, [
                'status' => $request->status,
            ]);
        }

        $result = $this->makeRequestWithJson('company_customer/get-list', $params);
        return view('cpanel.Customer.show')->with([
            'listCustomer' => $result['results']['result'],
            'page' => $page
        ]);
    }

    public function getInfoByPhoneNumber(Request $request)
    {
        $response = $this->makeRequest('web_ticket/get_list_user_for_company', [
            'timeZone' => 7,
            'page' => 0,
            'count' => 10,
            'startDate' => 0,
            'endDate' => strtotime('+ 1 year') * 1000,
            'phoneNumber' => $request->phoneNumber,

//            'routeId' => $request->routeId,
        ]);

        return response()->json(last(array_get($response['results'], 'listUser', [])));
    }

    public function getViewPolicy()
    {
        return view('cpanel.Customer.policyCustomer');
    }
}
