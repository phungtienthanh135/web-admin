<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ItemController extends Controller
{

    public function show(Request $request)
    {

        $page = $request->has('page') ? $request->page : 1;

        $response = $this->makeRequestWithJson('items/getlist',[
            'page'=>$page - 1,
            'count'=>10,
            'itemName'=> $request->itemName ,
            'itemType' => $request->itemType ,
            'itemId'=> $request->itemId
        ]);

        return view('cpanel.Item.show', [
            'result' => $response['results']['result'],
            'page' => $page
        ]);
    }
    public function postAdd(Request $request)
    {
        $param = [
            'itemName'=>$request->itemName,
            'itemType'=>$request->itemType,
            'itemReceiptPayment' => $request->itemReceiptPayment
        ];

        $result = $this->makeRequestWithJson('items/create', $param);

        if ($result['status']=='success')
        {
            return redirect()->back()->with(['msg'=>"Message('Thông báo','Thêm khoản mục thành công','');"]);
        }else
        {
            return redirect()->back()->withInput()->with(['msg'=>"Message('Thông báo','Thêm khoản mục thất bại.<br>Mã lỗi: " . checkMessage($result['results']['error']['code']) . "','');"]);
        }
    }

    public function postEdit(Request $request)
    {

        $result = $this->makeRequestWithJson('items/update',[
            'itemName'=>$request->itemName,
            'itemType'=>$request->itemType,
            'itemId'=>$request->itemId
        ]);

        if ($result['status']=='success')
        {
            return redirect()->back()->with(['msg'=>"Message('Thông báo','Cập nhật khoản mục thành công.','');"]);
        }else {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Cập nhật khoản mục thất bại.<br>Mã lỗi: " . checkMessage($result['results']['error']['code']) . "','');"]);
        }

    }

    public function delete(Request $request)
    {
        $page = $request->has('page')?$request->page:1;
        $result = $this->makeRequestWithJson('items/delete',['itemId'=>$request->itemId]);
        if ($result['status']=='success')
        {
            return redirect()->back()->with(['msg'=>"Message('Thông báo','Xoá khoản mục thành công','');",'page'=>$page]);
        }else
        {
            return redirect()->back()->with(['msg'=>"Message('Thông báo','Xoá khoản mục thất bại.<br>Mã lỗi: " . checkMessage($result['results']['error']['code']) . "','');",'page'=>$page]);
        }
    }

}
