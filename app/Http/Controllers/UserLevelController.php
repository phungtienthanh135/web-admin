<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserLevelController extends Controller
{

    public function show(Request $request)
    {
        $page = $request->has('page') ? $request->page : 1;

        $params = [
            'page' => $page - 1,
            'count' => 10,
        ];

        if (!empty($request->groupName)) {
            $params = array_merge($params, [
                'groupPermissionName' => $request->groupName,
            ]);
        }

//        $responses = $this->makeRequestWithJson('group-permission/getlist', [
//            'page'=>$page - 1,
//            'count'=>10,
//            'groupPermissionName' => $request->groupName,
//        ]);

        $responses = $this->makeRequestWithJson('group-permission/getlist', $params);

        $allPermission = [];

        foreach ($responses['results']['listGroupPermission'] as $response) {
            foreach ($response['listFunction'] as $permission) {
                $allPermission [] = $permission;
            }
        }

        return view('cpanel.User.Level.show')->with([
            'result' => $responses['results']['listGroupPermission'],
            'reponse_list_permit' => $this->getAllPermission()['reponse_list_permit'],
            'labelCategory' => $this->getAllPermission()['labelCategory'],
            'page' => $page,
        ]);
    }

    public function getadd()
    {

        return view('cpanel.User.Level.add')->with([
            'reponse_list_permit' => $this->getAllPermission()['reponse_list_permit'],
            'labelCategory' => $this->getAllPermission()['labelCategory'],
        ]);

    }

    public function postadd(Request $request)
    {
        $response = $this->makeRequestWithJson('group-permission/create', [

            'groupPermissionName' => $request->groupPermissionName,
            'groupPermissionDescription' => $request->groupPermissionDescription,
            'listFuncCode' => json_encode(array_map('intval', $request->listFuncCode)),
        ]);

        if ($response['status'] == 'success') {
            return redirect()->action('UserLevelController@show')->with(['msg' => "Message('Thành công','Tạo nhóm quyền thành công','');"]);
        } else {

            return redirect()->back()->withInput()->with(['msg' => "Message('Thất bại','Tạo nhóm quyền thất bại <br>Mã lỗi: " . $response['results']['error']['propertyName'] . "','');"]);
        }
    }

    public function delete(Request $request)
    {

        $result = $this->makeRequestWithJson('group-permission/delete', ['groupPermissionId' => $request->groupPermissionId]);

        if ($result['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Xoá nhóm quyền thành công','');"]);
        } else {
            return redirect()->back()->with(['msg' => "Message('Thất bại','Xoá nhóm quyền thất bại.<br>Mã lỗi: " . $result['results']['error']['propertyName'] . "','');"]);
        }
    }

    public function updatePermission(Request $request)
    {
        $response = $this->makeRequestWithJson('group-permission/update', [
            'groupPermissionId' => $request->groupPermissionId,
//            'groupPermissionName'=>$request->groupPermissionName,
//            'groupPermissionDescription'=>$request->groupPermissionDescription,
            'listFuncCode' => json_encode(array_map('intval', $request->listFuncCode)),
        ]);

        if ($response['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Sửa thành công nhóm quyền: " . $request->groupPermissionName . "','');"]);
        } else {

            return redirect()->back()->withInput()->with(['msg' => "Message('Thất bại','Sửa nhóm quyền thất bại <br>Mã lỗi: " . $response['results']['error']['propertyName'] . "','');"]);
        }
    }

    private function getAllPermission()
    {

        $results = [];

        $results['reponse_list_permit'] = $this->makeRequestWithJson('anvui_function/getAll', null)['results']['listAnvuiFunction'];

        $results['labelCategory'] = [
            10000 => 'Quản lý hệ thống',
            20000 => 'Quản lý nhân sự',
            30000 => 'Quản lý danh mục',
            40000 => 'Quản lý vé',
            50000 => 'Quản lý điều hành xe',
            60000 => 'Quản lý khách hàng',
            70000 => 'Quản lý báo cáo',
            80000 => 'Quản lý thu chi',
            90000 => 'Quản lý chung',
        ];

        return $results;
    }

    public function getPermissionByGroupId(Request $request)
    {
        $response = $this->makeRequestWithJson('group-permission/getlist', [
            'groupPermissionId' => $request->groupId,
        ]);
        return $response['results']['listGroupPermission'][0];
    }
}
