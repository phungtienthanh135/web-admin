<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationController extends Controller
{

    public function user(){
        return view('cpanel.Notification.notification_user');
    }
    public function customer(){
        return view('cpanel.Notification.notification_customer');
    }

    public function notify(){
        $listPopup = $this->makeRequestWithJson('/popup/get-list',[
            'page'=>0,
            'count'=>20,
            'companyId'=>session('companyId')
        ]);
        $lp = [];
        if($listPopup['code']==200){
            $lp = $listPopup['results']['popUps'];
        }
        return view('cpanel.Notification.notify')->with([
            'listPopup'=>$lp
        ]);
    }
}
