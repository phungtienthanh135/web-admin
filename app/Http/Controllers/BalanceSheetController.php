<?php

namespace App\Http\Controllers;

use App\Http\Requests\BalanceSheetRequest;
use Exception;
use Illuminate\Http\Request;

class BalanceSheetController extends Controller
{

    /*
     * Thu
     * */
    public function income(Request $request)
    {
        $tripId = $request->tripId;
        $page = $request->has('page') ? $request->page : 1;

        $startDate = date_to_milisecond($request->startDate, 'begin');
        $endDate = $request->has('endDate') ? date_to_milisecond($request->endDate, 'end') : strtotime('+1 month') * 1000;
//        $numberOfDocuments = $request->numberOfDocuments;
//        $companyId = $request->companyId;

        $nhanvien = $this->makeRequestWithJson('user/getlist', [
            'page' => 0,
            'count' => 1000,
            'listUserType' => [2,3,5]
        ]);

        $listItem = $this->makeRequestWithJson('items/getlist', ['page' => $page - 1, 'count' => 100, 'option' => 1]);

        // sửa type => itemType, thêm trong request
        $param= [
            'page' => $page - 1,
            'count' => 10,
            'timeZone' => 7,
//            'numberOfDocuments' => $numberOfDocuments,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'companyId' => session('companyId'),
            'type'=>1
        ];

        if(!is_null($request->Agency)){
            $param['agencyUserId']=$request->Agency;
        }

        if($request->itemId) $param['itemId']=$request->itemId;
        $response = $this->makeRequestWithJson('receipt_and_payment/getlist',$param);
        return view('cpanel.BalanceSheet.Income', [
            'result' => isset($response['results']['result']) ?$response['results']['result']:[] ,
            'page' => $page,
            'listItem' => isset($listItem['results']['result'])?$listItem['results']['result']:[],
            'nhanvien' => isset($nhanvien['results']['result'])?$nhanvien['results']['result']:[],
            'tripId' => $tripId
        ]);
    }

    /*
     * Chi
     * */
    public function expenses(Request $request)
    {
        $tripId = $request->tripId;
        $page = $request->has('page') ? $request->page : 1;

        $startDate = date_to_milisecond($request->startDate, 'begin');
        $endDate = session()->has('endDate') ? date_to_milisecond($request->endDate, 'end') : time() * 1000;
        $numberOfDocuments = $request->numberOfDocuments;
        $companyId = $request->companyId;

        // sửa type => itemType, thêm trong request

        $listItem = $this->makeRequestWithJson('items/getlist', ['page' => $page - 1, 'count' => 100, 'option' => 2]);

        $nhanvien = $this->makeRequestWithJson('user/getlist', [
            'page' => 0,
            'count' => 1000,
            'listUserType' => [2,3,5]
        ]);
        $param= [
            'page' => $page - 1,
            'count' => 10,
            'timeZone' => 7,
//            'numberOfDocuments' => $numberOfDocuments,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'companyId' => session('companyId'),
            'type'=>2
        ];

        if($request->itemId) $param['itemId']=$request->itemId;
        $response = $this->makeRequestWithJson('receipt_and_payment/getlist',$param);
        return view('cpanel.BalanceSheet.Expenses', [
            'result' => isset($response['results']['result']) ?$response['results']['result']:[] ,
            'page' => $page,
            'listItem' => isset($listItem['results']['result'])?$listItem['results']['result']:[],
            'nhanvien' => isset($nhanvien['results']['result'])?$nhanvien['results']['result']:[],
            'tripId' => $tripId
        ]);
    }

    //Thêm nhiều phiếu thu/chi theo chuyến
    public function addMultil(Request $request)
    {
        $itemId = $request->itemId;
        $amountSpent = $request->amountSpent;
        $url = 'BalanceSheetController@expenses';
        if($request->type == 1){
            $url = 'BalanceSheetController@income';
        }
        try {
            for($i=0; $i<count($itemId); $i++)
            {
                $numberOfDocuments = ($request->type == 1) ? 'PT'.($i+1).'-'.strtotime('now') : 'PC'.($i+1).'-'.strtotime('now');
                $param = [
                    'reason' => $request->reason,
                    'numberOfDocuments' => $numberOfDocuments,
                    'amountSpent' => $amountSpent[$i],
                    'date' => date_to_milisecond($request->date),
                    'itemId' => $itemId[$i],
                    'moneyReceiverId' => ($request->type == 2) ? $request->moneyReceiverId : array_get(session('userLogin'),'userInfo.userId'),
                    'spendingMoneyId' => ($request->type == 1) ? $request->spendingMoneyId : array_get(session('userLogin'),'userInfo.userId'),
//                    'havingIncreased' => $request->havingIncreased,
//                    'havingDecrease' => $request->havingDecrease,
                    'type'=>$request->type
                ];

                if(!empty($request->tripId))
                {
                    $param = array_merge($param, [
                        'tripId'=>$request->tripId, //tripId
                    ]);
                }

                if(!empty($request->receiver))
                {
                    $param = array_merge($param, [
                        'receiver'=>$request->receiver, //Người thu
                    ]);
                }

                if(!empty($request->approverId))
                {
                    $param = array_merge($param, [
                        'receiver'=>$request->approverId,
                    ]);
                }

                if(!empty($request->agencyUserId)) {
                    $param = array_merge($param, [
                        'agencyUserId'=>$request->agencyUserId,
                    ]);
                }

                if(!empty($request->sender))
                {
                    $param = array_merge($param, [
                        'sender'=>$request->sender,
                    ]);
                }

                $result = $this->makeRequestWithJson('receipt_and_payment/create', $param);
                if ($result['status'] != 'success') {
                    throw new Exception();
                }
            }
        } catch (Exception $e) {
            return redirect()->back()->withInput()->with(['msg' => "Message('Thông báo','Thêm phiếu thu/chi thất bại.','');"]);
        }
        return redirect()->action($url)->with(['msg' => "Message('Thông báo','Thêm phiếu thu/chi thành công','');"]);
    }

    public function add(Request $request)
    {
         // thu 1
         // chi 2
        $numberOfDocuments = ($request->type == 1) ? 'PT-'.strtotime('now') : 'PC-'.strtotime('now');

        $param = [
            'reason' => $request->reason,
            'numberOfDocuments' => $numberOfDocuments,
            'amountSpent' => $request->amountSpent,
            'date' =>$request->date? date_to_milisecond($request->date):(time()*1000),
            'itemId' => $request->itemId,
            'moneyReceiverId' => ($request->type == 2) ? $request->moneyReceiverId : array_get(session('userLogin'),'userInfo.userId'),
            'spendingMoneyId' => ($request->type == 1) ? $request->spendingMoneyId : array_get(session('userLogin'),'userInfo.userId'),
//            'havingIncreased' => $request->havingIncreased,
//            'havingDecrease' => $request->havingDecrease,
            'type'=>$request->type
        ];

        if(!empty($request->receiver))
        {
            $param = array_merge($param, [
                'receiver'=>$request->receiver, //Người thu
            ]);
        }

        if(!empty($request->approverId))
        {
            $param = array_merge($param, [
                'receiver'=>$request->approverId,
            ]);
        }

        if(!empty($request->agencyUserId)) {
            $param = array_merge($param, [
                'agencyUserId'=>$request->agencyUserId,
            ]);
        }

        if(!empty($request->sender))
        {
            $param = array_merge($param, [
                'sender'=>$request->sender,
            ]);
        }

        $result = $this->makeRequestWithJson('receipt_and_payment/create', $param);

        if ($result['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Thêm phiếu thu/chi thành công','');"]);
        } else {
            return redirect()->back()->withInput()->with(['msg' => "Message('Thông báo','Thêm  phiếu thu/chi thất bại.<br>Mã lỗi: " . checkMessage($result['results']['error']['propertyName']). "','');"]);
        }
    }

    /*public function report(Request $request)
    {
        $startDate =  $request->has('startDate') ?date_to_milisecond($request->startDate):strtotime('-1 month')*1000;
        $endDate = $request->has('endDate') ? date_to_milisecond($request->endDate) : time() * 1000;


        $response_in = $this->makeRequest('web_receipt_and_payment/getlist', [
            'page' => 0,
            'count' => 10,
            'timeZone' => 7,
            'startDate' => 0,
            'endDate' => $endDate,
            'type'=>1
        ]);
        $response_exp = $this->makeRequest('web_receipt_and_payment/getlist', [
            'page' => 0,
            'count' => 10,
            'timeZone' => 7,
            'startDate' => 0,
            'endDate' => $endDate,
            'type'=>2
        ]);
        return view('cpanel.BalanceSheet.report')->with([
            'in'=>head($response_in['results']),
            'exp'=>head($response_exp['results'])
        ]);

    }*/

    public function delete(Request $request)
    {
        $result = $this->makeRequestWithJson('receipt_and_payment/delete', [
            'receiptAndPaymentId' => $request->receiptAndPaymentId
        ]);
        $response['param']=$request->receiptAndPaymentId;
        dev($response);
        if ($result['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Xóa phiếu thu/chi thành công','');"]);
        } else {
            return redirect()->back()->withInput()->with(['msg' => "Message('Thông báo','Xóa phiếu thu/chi thất bại.<br>Mã lỗi: " . checkMessage($result['results']['error']['propertyName']) . "','');"]);
        }
    }
}
