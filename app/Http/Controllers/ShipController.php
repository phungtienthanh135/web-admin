<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Picqer\Barcode\BarcodeGeneratorSVG;

class ShipController extends Controller
{
    private $listTicketStatus = [//trạng thái thực tế là trừ 1 đơn vị vd: 0 là đã hủy
        1 => 'Đã Hủy',
        3 => 'Đã gửi yêu cầu',
        5 => 'Đã lên xe',
        6 => 'Đã Trả',
    ];

    public function show(Request $request)
    {
        //$dateDefault = strtotime(date("yyyy-mm-dd"));
        $today = (new \DateTime())->setTime(0, 0);

        $startDate = $request->has('startDate') ? date_to_milisecond($request->startDate) : (date_to_milisecond($today->format('d-m-Y')));
        $endDate = $request->has('endDate') ? (date_to_milisecond($request->endDate) + (24 * 60 * 60 * 1000)) : ($startDate + (24 * 60 * 60 * 1000));
        $startDate += 7 * 60 * 60 * 1000;
        $endDate += (7 * 60 * 60 * 1000 - 1);
        if ($request->phoneNumberReceiver == null) $request->phoneNumberReceiver = '';
        $ticketStatus = !empty($request->status) ? array_to_json(explode(" ", $request->status - 1)) : null;
        $params = [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'routeId' => $request->routeId,
            'ticketId' => $request->ticketId,
            'phoneNumberReceiver' => $request->phoneNumberReceiver,
        ];
        //dev($params);
        if ($ticketStatus != null) {
            $params['ticketStatus'] = $ticketStatus;
        }
        //dev($params);
        $result = $this->makeRequest('web_ticket/get_list_goods_ticket', $params);
        $listRoute = $this->makeRequest('web_route/getlist',
            [
                'page' => 0,
                'count' => 10
            ]);
        //dev($result);
        return view('cpanel.Ship.show.list-ship')->with([
            'listRoute' => head($listRoute['results']),
            'listShip' => head($result['results']),
            'ticketStatus' => $this->listTicketStatus
        ]);

    }

    public function postGoodInTrip(request $request)
    {
        $listTicket = explode(',', $request->listTicketIds);
        $results = $this->makeRequest('web_ticket/good-in-trip', [
            'listTicketIds' => array_to_json($listTicket),
            'timeZone' => 7,
            'tripId' => $request->tripId,
            'getInPointId' => $request->getInPointId,
            'getOffPointId' => $request->getOffPointId
        ]);
        if ($results['status'] == 'success') {
            return redirect()->route('cpanel.ship.show')->with(["msg" => MessageJS("Đăng kí chuyển hàng Thành Công")]);
        } else {
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Chuyển hàng thất bại <br>" . checkMessage($results['results']['error']['propertyName']))]);
        }
    }

    public function sell(Request $request)
    {
        /*$page = $request->has('page') ? $request->page : 1;
         $startDate = $request->has('startDate') ? date_to_milisecond($request->startDate) : 0;
        $endDate = $request->has('endDate') ? date_to_milisecond($request->endDate) : time() * 1000;
        $result = $this->makeRequest('web_ticket/get_list_ticket_for_company', [
            'timeZone' => 7,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'type' => 3,
            'page' => $page - 1,
            'count' => 100,
           'paymentType' => '1,2,3,4,5,6',
        ]);*/


        $response = $this->makeRequestWithJson('user/getlist', [
            'fullName' => $request->fullName,
            'page' => 0,
            'count' => 100000000,
            'listUserType' => [2,3,4,5,6],
        ]);

        $listRoute = $this->makeRequest('web_route/getlist', []);
        $resultList = $this->makeRequestWithJson('/goods_type/list',
            [
                'companyId' => session('companyId'),
                'goodsTypeName' => '',
                'createdUserId' => '',
                'page' => 0,
                'count' => 100

            ]
        );
        return view("cpanel.Ship.sell")->with([
            'listRoute' => $listRoute['results']['result'],
            'result' => $response['results']['result'],
            'listTypeShip' => $resultList['results']['list']
        ]);

    }


    public function getPriceTicketGoods(Request $request)
    {
        $pickUpAddress = $request->pickUpAddress;
        $dropOffAddress = $request->dropOffAddress;
        $promotionId = $request->promotionId;
        $tripId = $request->tripId;
        $routeId = $request->routeId;
        $fullNameEmployee = $request->fullNameEmployee;
        $weight = $request->weight;
        $dimension = $request->dimension;
        $getInTimePlan = date_to_milisecond($request->getInTimePlan);
        $getOffTimePlan = date_to_milisecond($request->getOffTimePlan);


        $responsePrice = $this->makeRequest('web/get-price-ticket-goods', [
            'timeZone' => 7,
            'pickUpAddress' => $pickUpAddress,
            'dropOffAddress' => $dropOffAddress,
            'tripId' => $tripId,
            'routeId' => $routeId,
            'promotionId' => $promotionId,
            'weight' => $weight,
            'dimension' => $dimension,
            'getInTimePlan' => $getInTimePlan,
            'getOffTimePlan' => $getOffTimePlan,
            'fullNameEmployee' => $fullNameEmployee
        ]);
        $resultPrice = $responsePrice['results'];
        if (array_has($resultPrice, 'price')) {
            $resultPrice = $responsePrice['results']['price'];
        }
        return response()->json($resultPrice);
    }

    public function postSellTicketGood(Request $request)
    {
        $pickUpAddress = $request->pickUpAddress;
        $dropOffAddress = $request->dropOffAddress;
        $tripId = $request->tripId;
        $routeId = $request->routeId;
        $promotionId = $request->promotionId;
        $weight = $request->weight;
        $dimension = empty($request->dimension) ? 0 : $request->dimension;
        $getInTimePlan = date_to_milisecond($request->getInTimePlan) + (7 * 60 * 60 * 1000);
        $getOffTimePlan = date_to_milisecond($request->getOffTimePlan) + (7 * 60 * 60 * 1000);
        $fullNameEmployee = $request->fullNameEmployee;

        $sendSMS = $request->has('sendSMS') ? 'true' : 'false';
        //dev( $getInTimePlan);


        //thực hiện đặt vé
        $result = $this->makeRequest('web_ticket/sale_ticket_goods', [
            'timeZone' => 7,
//            'pickUpAddress' => $pickUpAddress,
//            'dropOffAddress' => $dropOffAddress,
            'getInTimePlan' => $getInTimePlan,
            'getOffTimePlan' => $getOffTimePlan,
            'tripId' => $tripId,
            'routeId' => $routeId,
            'fullNameEmployee' => $request->fullNameEmployee,
            'startDate' => ceil($request->startDate / 86400000) * 86400000,
            'promotionId' => $promotionId,
            'fullName' => empty($request->fullName) ? 'Khách vãng lai' : $request->fullName,
            'phoneNumber' => $request->phoneNumber,
            'fullNameReceiver' => $request->fullNameReceiver,
            'phoneNumberReceiver' => $request->phoneNumberReceiver,
            'paymentTicketPrice' => $request->paymentTicketPrice,
            'ticketName' => $request->ticketName,
            'weight' => $weight,
            'dimension' => $dimension,
            'paidMoney' => is_null($request->paidMoney) ? "0" : $request->paymentTicketPrice,
            'paymentType' => $request->paymentType,
            'listImages' => array_to_json($request->listImages),
            'sendSMS' => $sendSMS


        ]);
        $resultList = $this->makeRequestWithJson('/goods_type/list',
            [
                'companyId' => session('companyId'),
                'goodsTypeName' => '',
                'createdUserId' => '',
                'page' => 0,
                'count' => 100

            ]
        );
        $response = $this->makeRequestWithJson('user/getlist', [
            'fullName' => $request->fullNames,
            'page' => 0,
            'count' => 100000000,
            'listUserType' => [2,3,4,5,6],
        ]);
        $listRoute = $this->makeRequest('web_route/getlist', []);
        if ($result['status'] == 'success') {

            return view("cpanel.Ship.sell")->with([
                'listRoute' => head($listRoute['results']),
                'result' => $response['results']['result'],
                'msg' => ($result['results']['message'] . 'Mã vé: ' . $result['results']['ticketId']),
                'listTypeShip' => $resultList['results']['list'],
                'code' => 200
            ]);
        } else {
            return view("cpanel.Ship.sell")->with([
                'msg' => ("Đặt vé gửi đồ thất bại" . $result['results']['error']['propertyName']),
                'listTypeShip' => $resultList['results']['list'],
                'listRoute' => head($listRoute['results']),
                'result' => $response['results']['result'],
                'code' => 1
            ]);
        }
    }


    public function updateStatusTicket(Request $request)
    {
        $result = $this->makeRequest('web_ticket/update_by_staff', [
            'timeZone' => 7,
            'ticketId' => $request->ticketId,
            'option' => $request->option,
            'phoneNumber' => $request->phoneNumber,
            'fullName' => $request->fullName,
            'phoneNumberReceiver' => $request->phoneNumberReceiver,
            'fullNameReceiver' => $request->fullNameReceiver,
        ]);
        if ($result['status'] == 'success') {
            $message = $result['results']['message'] . '<br>Mã vé: ' . $result['results']['ticketId'];

            return redirect()->back()->withInput()->with(['msg' => "Message('Thành công','" . $message . "','');"]);
        } else {
            $message = '';
            if ($request->option == 1)
                $message = "Message('Thất bại','Thanh toán vé thất bại<br>" . $result['results']['error']['propertyName'] . "','');";
            if ($request->option == 2)
                $message = "Message('Thất bại','Huỷ vé thất bại<br>" . $result['results']['error']['propertyName'] . "','');";
            if ($request->option == 3)
                $message = "Message('Thất bại','Cập nhật thông tin vé thất bại<br>" . $result['results']['error']['propertyName'] . "','');";
            if ($request->option == 4)
                $message = "Message('Thất bại','Trả đồ thất bại<br>" . $result['results']['error']['propertyName'] . "','');";
            return redirect()->back()->withInput()->with(['msg' => $message]);
        }
    }

    public function printTicket($ticketId)
    {
        $result = $this->makeRequest('web_ticket/view', [
            //'ticketCode' => $ticketId,
            'ticketId' => $ticketId
        ]);
        //dev($result);
        $generator = new BarcodeGeneratorSVG();
        $Barcode = $generator->getBarcode(array_get($result['results'], 'ticket.ticketCode'), $generator::TYPE_CODE_128, 1.2);

        return view('cpanel.Print.printTicket')->with(['result' => array_get($result['results'], 'ticket'), 'barcode' => $Barcode]);

    }

    public function typeShip(Request $request)
{  //dev($request->goodsTypeId);
    $name = $request->shipType ? $request->shipType : '';
    $param = [
        'companyId' => session('companyId'),
        'goodsTypeName' => $name,
        'createdUserId' => '',
        'page' => 0,
        'count' => 100

    ];
    if ($request->goodsTypeId)
        $param['goodsTypeId'] = $request->goodsTypeId;
    $resultList = $this->makeRequestWithJson('/goods_type/list', $param);
    return view('cpanel.Ship.Type.listType')->with(['result' => $resultList['results']['list']]);
}
    public function typeShipNew(Request $request)
    {  //dev($request->goodsTypeId);
        $name = $request->shipType ? $request->shipType : '';
        $param = [
            'companyId' => session('companyId'),
            'goodsTypeName' => $name,
            'createdUserId' => '',
            'page' => 0,
            'count' => 100

        ];
        if ($request->goodsTypeId)
            $param['goodsTypeId'] = $request->goodsTypeId;
        $resultList = $this->makeRequestWithJson('/goods_type/list', $param);
        return $resultList['results']['list'];
    }

    public function addTypeShip(Request $request)
    {
        $resultPost = $this->makeRequestWithJson('/goods_type/create',
            [
                'goodsTypeName' => $request->goodsTypeName,
                'goodsTypePrice' => $request->goodsTypePrice
            ]
        );
        $resultList = $this->makeRequestWithJson('/goods_type/list',
            [
                'companyId' => session('companyId'),
                'goodsTypeName' => '',
                'createdUserId' => '',
                'page' => 0,
                'count' => 100

            ]
        );
        if ($resultPost['code'] == 200)
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Thêm thành công loại đồ mới"), 'result' => $resultList['results']['list']]);
        else
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Chưa thành công")]);

    }

    public function repairTypeShip(Request $request)
    {
        $resultRepair = $this->makeRequestWithJson('/goods_type/update',
            [
                'goodsTypeName' => $request->goodsTypeName,
                'goodsTypePrice' => $request->goodsTypePrice,
                'goodsTypeId' => $request->goodsTypeId
            ]
        );
        $resultList = $this->makeRequestWithJson('/goods_type/list',
            [
                'companyId' => session('companyId'),
                'goodsTypeName' => '',
                'createdUserId' => '',
                'page' => 0,
                'count' => 100

            ]
        );
        if ($resultRepair['code'] == 200)
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Sửa thành công "), 'result' => $resultList['results']['list']]);
        else
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Chưa thành công"), 'result' => $resultList['results']['list']]);
    }

    public function deleteTypeShip(Request $request)
    {
        $resultDelete = $this->makeRequestWithJson('/goods_type/delete',
            [
                'goodsTypeId' => $request->goodsTypeId
            ]
        );
        $resultList = $this->makeRequestWithJson('/goods_type/list',
            [
                'companyId' => session('companyId'),
                'goodsTypeName' => '',
                'createdUserId' => '',
                'page' => 0,
                'count' => 100

            ]
        );
        if ($resultDelete['code'] == 200)
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Xóa thành công "), 'result' => $resultList['results']['list']]);
        else
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Chưa thành công"), 'result' => $resultList['results']['list']]);

    }

    public function getPriceShip(Request $request)
    {
        $param = [];
        if ($request->goodTypeId != '') {
            $param['goodsTypeId'] = $request->goodTypeId;
        } else {
            $param['routeId'] = $request->routeId;
            $param['getInPointId'] = $request->getInPointId;
            $param['getOffPointId'] = $request->getOffPointId;
        }
        $resultList = $this->makeRequestWithJson('/ticket/get_goods_price_by_point', $param);
        return $resultList;
    }


}

?>