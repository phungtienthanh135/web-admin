<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    public function show(Request $request)
    {
        $response = $this->makeRequest('web_foreign_currency/getlist'); 
        // dev($response);
        return view('cpanel.Currency.show', [
            'result' => array_get($response['results'],'listForeignCurrency',[])
            ]);
    }
    public function postAdd(Request $request)
    {
        $result = $this->makeRequest('web_foreign_currency/create',[
            'foreignCurrencyType'=>$request->foreignCurrencyType,
            'exchangeRate'=>$request->exchangeRate,
            'note'=>$request->note
        ]);

        if ($result['status']=='success')
        {
            return redirect()->back()->with(['msg'=>MessageJS('Thêm tiền tệ thành công')]);
        }else
        {
            return redirect()->back()->withInput()->with(['msg'=>MessageJS('Thêm tiền tệ thất bại <br>Mã lỗi: ' . checkMessage($result['results']['error']['code']))]);
        }
    }

    public function postEdit(Request $request)
    {

        $result = $this->makeRequest('web_items/update',[
            'itemName'=>$request->itemName,
            'itemType'=>$request->itemType,
            'itemId'=>$request->itemId
        ]);

        if ($result['status']=='success')
        {
            return redirect()->back()->with(['msg'=>MessageJS('Cập nhật tiền tệ thành công')]);
        }else {
            return redirect()->back()->with(['msg' => MessageJS('Cập nhật tiền tệ thất bại <br>Mã lỗi: ' . checkMessage($result['results']['error']['code']))]);
        }

    }

    public function delete(Request $request)
    {
        $page = $request->has('page')?$request->page:1;
        $result = $this->makeRequest('web_foreign_currency/delete',['foreignCurrencyId'=>$request->foreignCurrencyId]);
        if ($result['status']=='success')
        {
            return redirect()->back()->with(['msg'=>MessageJS('Xoá tiền tệ thành công'),'page'=>$page]);
        }else
        {
            return redirect()->back()->with(['msg'=>MessageJS('Xoá tiền tệ bại <br>Mã lỗi: ' . checkMessage($result['results']['error']['code'])),'page'=>$page]);
        }
    }
}
