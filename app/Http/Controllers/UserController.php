<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;

class UserController extends Controller
{


    public function show(Request $request)
    {

        $page = $request->has('page') ? $request->page : 1;
        $response = $this->makeRequestWithJson('user/getlist', [
            'page' => $page - 1,
            'count' => RECORD_PER_PAGE,
            'listUserType'=>[2,3,4,5,6],
            //'verifyStatus'=>'',
            //'userStatus'=>1,
            'phoneNumber' => $request->phoneNumber,
            //'companyName'=>'',
            'fullName' => $request->fullName,
            'homeTown' => $request->homeTown,
            'identityCardNumber' => $request->identityCardNumber,
        ]);
//        dev($response);

        $groupPermissions = [];
        if(hasAnyRole(GET_LIST_GROUP_PERMISSION))
        {
            $groupPermissions = $this->makeRequest('group-permission/getlist', [])['results']['listGroupPermission'];
        }

        $departments = $this->makeRequest('department/getlist',[]);
        return view('cpanel.User.show')->with([
            'result' => $response['results']['result'],
            'page' => $page,
            'countUser' => count($response['results']['result']),
            'groupPermissions' => $groupPermissions,
            'listDepartment'=>$departments['results']['listDepartment'],
        ]);
    }

    public function add()
    {
        $response_role = array(
//            [
//                "id" => 1,
//                "value" => "Người dùng thường"
//            ],
            [
                "id" => 2,
                "value" => "Lái xe"
            ],
            [
                "id" => 3,
                "value" => "Phụ xe"
            ],
            [
                "id" => 4,
                "value" => "Kế toán"
            ],
            [
                "id" => 5,
                "value" => "Nhân viên hành chính"
            ],
            [
                "id" => 6,
                "value" => "Thanh tra"
            ]
        );

        $departments = $this->makeRequest('department/getlist',[]);

        $groupPermissions = [];
        if(hasAnyRole(GET_LIST_GROUP_PERMISSION))
        {
            $groupPermissions = $this->makeRequest('group-permission/getlist', [])['results']['listGroupPermission'];
        }



        return view('cpanel.User.add')->with([
            'response_role' => $response_role,
            'groupPermissions' => $groupPermissions,
            'departments' => $departments['results']['listDepartment'],
        ]);
    }

    public function postadd(UserRequest $request)
    {
        $params = [
            'userType' => $request->userType,
            'phoneNumber' => $request->phoneNumber,
            'userName' => $request->userName,
            'fullName' => $request->fullName,
            'password' => $request->password,
            'contacterFullName' => $request->contacterFullName,
            'contacterPhoneNumber' => $request->contacterPhoneNumber,
            'homeTown' => $request->homeTown,
            'contacterEmail' => $request->contacterEmail,
            'identityCardNumber' => $request->identityCardNumber,
            'departmentId' => $request->departmentId,
            'code' => $request->code,
            'licenceLevel' => $request->licenceLevel,
            'licenceCode' => $request->licenceCode,
            'licenceExpired' => date_to_milisecond($request->licenceExpired),
            'issuedPlace' => $request->issuedPlace,
            'issuedDate' => date_to_milisecond($request->issuedDate),
            'birthDay' => date_to_milisecond($request->birthDay),
            'deviceId' => $request->deviceId,
            'education' => $request->education,
            'groupPermissionId' => $request->groupPermissionId,
            'avatar' => $request->avatar,
            'sex' => $request->sex_employee,
            'joinDate' => date_to_milisecond($request->joinDate),
            'deviceType' => $request->deviceType,
            'email' => $request->email,
            'phoneNumberReferrer' => $request->phoneNumberReferrer,
            'isAdmin' => $request->has('isAdmin') ? 'true' : '',
            'stateCode' => $request->stateCode,
            'listWhiteMAC' => explode(',',$request->listWhiteMAC),
        ];
        $response = $this->makeRequest('web_admin/staff-register',$params);

        if ($response['status'] == 'success') {
            return redirect()->action('UserController@show')->with(['msg' => "Message('Thành công','Tạo tài khoản thành công','');"]);
        } else {

            return redirect()->back()->withInput()->with(['msg' => "Message('Thất bại','Tạo tài khoản thất bại <br>Mã lỗi: " . checkMessage($response['results']['error']['propertyName']) . "','');"]);
        }

    }

    public function getEdit($id)
    {
        $response_role = array(
            [
                "id" => 1,
                "value" => "Người dùng thường"
            ],
            [
                "id" => 2,
                "value" => "Lái xe"
            ],
            [
                "id" => 3,
                "value" => "Phụ xe"
            ],
            [
                "id" => 4,
                "value" => "Kế toán"
            ],
            [
                "id" => 5,
                "value" => "NV Hành chánh"
            ],
            [
                "id" => 6,
                "value" => "Thanh tra"
            ]
        );
        $Sex = array(
            [
                "id" => 1,
                "value" => "Nam" 
            ],
            [
                "id" => 2,
                "value" => "Nữ"
            ]
        );

        $licenceLevels = array(
            [
                'id' => 'B1',
                'value' => 'Hạng B1'
            ],
            [
                'id' => 'B2',
                'value' => 'Hạng B2'
            ],
            [
                'id' => 'C',
                'value' => 'Hạng C'
            ],
            [
                'id' => 'D',
                'value' => 'Hạng D'
            ],
            [
                'id' => 'E',
                'value' => 'Hạng E'
            ]

        );

        $education_employees = array(
            [
                'id' => 'Trung học cơ sở',
                'value' => 'Trung học cơ sở'
            ],
            [
                'id' => 'Trung học phổ thông',
                'value' => 'Trung học phổ thông'
            ],
            [
                'id' => 'Đại học',
                'value' => 'Đại học'
            ],
            [
                'id' => 'Cao đẳng',
                'value' => 'Cao đẳng'
            ],
            [
                'id' => 'Thạc sĩ',
                'value' => 'Thạc sĩ'
            ],
            [
                'id' => 'Tiến sĩ',
                'value' => 'Tiến sĩ'
            ]
        );


        $response = $this->makeRequestWithJson('user/getuserinfo', ['userId' => $id]);
        $departments = $this->makeRequest('department/getlist',[]);

        $groupPermissions = [];
        if(hasAnyRole(GET_LIST_GROUP_PERMISSION))
        {
            $groupPermissions = $this->makeRequest('group-permission/getlist', [])['results']['listGroupPermission'];
        }

        return view('cpanel.User.edit')->with([
            'Sex' => $Sex,
            'licenceLevels' => $licenceLevels,
            'education_employees' => $education_employees,
            'departments' => $departments['results']['listDepartment'],
            'response_role' => $response_role,
            'row' => array_get($response['results'], 'userInfo'),
            'groupPermissions' => $groupPermissions,
        ]);
    }

    public function postEdit(UserRequest $request)
    {
        $params = [
            'userId' => $userId = $request->userId,
            'userType' => $request->userType,
            'phoneNumber' => $request->phoneNumber,
            'userName' => $request->userName,
            'fullName' => $request->fullName,
            'sex' => $request->sex_employee,
            'contacterFullName' => $request->contacterFullName,
            'contacterPhoneNumber' => $request->contacterPhoneNumber,
            'homeTown' => $request->homeTown,
            'code' => $request->code,
            'contacterEmail' => $request->contacterEmail,
            'identityCardNumber' => $request->identityCardNumber,
            'departmentId' => $request->departmentId,
            'licenceLevel' => $request->licenceLevel,
            'licenceCode' => $request->licenceCode,
            'licenceExpired' => date_to_milisecond($request->licenceExpired),
            'issuedPlace' => $request->issuedPlace,
            'issuedDate' => date_to_milisecond($request->issuedDate),
            'birthDay' => date_to_milisecond($request->birthDay),
            'education' => $request->education,
            'avatar' => $request->avatar,
            'joinDate' => date_to_milisecond($request->joinDate),
            'groupPermissionId' => $request->groupPermissionId,
            'email' => $request->email,
            'phoneNumberReferrer' => $request->phoneNumberReferrer,
            'isAdmin' => $request->has('isAdmin') ? 'true' : false,
            'stateCode' => $request->stateCode,
            'listWhiteMAC' => explode(',',$request->listWhiteMAC),
        ];
        $response = $this->makeRequest('web_admin/updateinfo',$params);
        if ($response['status'] == 'success') {
            return redirect()->action('UserController@show')->with(['msg' => "Message('Thành công','Cập nhật tài khoản thành công','');"]);
        } else {
            return redirect()->back()->with(['msg' => "Message('Thất bại','Cập nhật tài khoản thất bại <br>Mã lỗi: " . checkMessage($response['results']['error']['propertyName']) . "','');"]);
        }

    }

    public function delete(Request $request)
    {
        $result = $this->makeRequestWithJson('user/delete', ['userId' => $request->userId]);

        if ($result['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Xoá tài khoản thành công','');"]);
        } else {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Xoá tài khoản thất bại.<br>Mã lỗi: " . checkMessage($result['results']['error']['propertyName']) . "','');"]);
        }
    }

    public function lock(Request $request)
    {
        $result = $this->makeRequestWithJson('user/lock', ['userId' => $request->userId]);

        if ($result['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Khóa tài khoản thành công','');"]);
        } else {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Khóa tài khoản thất bại.<br>Mã lỗi: " . checkMessage($result['results']['error']['propertyName']) . "','');"]);
        }
    }

    public function changePassword(Request $request)
    {
        $result = $this->makeRequestWithJson('user/changepassword', [
            'oldPassword' => $request->oldPassword,
            'newPassword' => $request->newPassword
        ]);

        if ($result['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Đổi mật khẩu thành công','');"]);
        } else {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Đổi mật khẩu thất bại.<br>Mã lỗi: " . checkMessage($result['results']['error']['code']) . "','');"]);
        }
    }
}
