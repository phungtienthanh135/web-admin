<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TripController extends Controller
{

    public function show(Request $request)
    {
        $listDriverAndAssistant = $this->makeRequestWithJson('user/getlist', ['page' => 0, 'count' => 1000, 'listUserType' => [2,3]]);
        $listVehicle = $this->makeRequestWithJson('vehicle/getlist', ['page' => 0, 'count' => 100, 'vehicleStatus' => [2]]);
        $listRoute = $this->makeRequest('web_route/getlist', null);

        return view('cpanel.Trip.show')->with([
            'listDriverAndAssistant' => $listDriverAndAssistant['results']['result'],
            'listVehicle' => $listVehicle['results']['result'],
            'listRoute' => head(array_get($listRoute,'results',[])),
        ]);

    }

    public function getListTrip(Request $request){
        $page = $request->has('page') ? $request->page : 1;
        $startDate = $request->has('startDate') ? date_to_milisecond($request->startDate, 'begin')-7*60*60*1000 : time() * 1000;
        $endDate = $request->has('startDate') ? date_to_milisecond($request->startDate, 'end')-7*60*60*1000 : strtotime('+1 day') * 1000;
        $params = [
            'page' => $page - 1,
            'count' => 600,
            'timeZone' => '7',
            'companyId' => session('companyId'),
            'startDate' => $startDate,
            'endDate' => $endDate,
        ];

        if (!empty($request->routeId)) {
            $params = array_merge($params, [
                'routeId' => $request->routeId,
            ]);
        }

        $result = $this->makeRequest('web/get-schedule-by-company', $params, ['DOBODY6969' => $this->TOKEN_SUPPER], 'GET');

        return response()->json($result);
    }

    function postEdit(Request $request)
    {
        $listDriver = $request->listDriverId;
        $listAssistant = $request->listAssistantId;
        $listLockTrip = $request->listLockTrip;
        $note = '';
        if(!is_null($request->note)){
            $note = $request->note;
        }
        $listUpdateSchedules = [
            'startTime'=>strMilisecond($request->startTime)-7*60*60*1000,
            'vehicleId' => $request->vehicleId,
            'dateRun' => $request->dateRun,
            'note' => $note,
        ];
        if(is_array($listLockTrip)&&count($listLockTrip)>0){
            $listUpdateSchedules['listLockTrip']=$listLockTrip;
        }
        if(is_array($listDriver)&&count($listDriver)>0){
            $listUpdateSchedules['listDriver']=$listDriver;
        }
        if(is_array($listAssistant)&&count($listAssistant)>0){
            $listUpdateSchedules['listAssistant']=$listAssistant;
        }
        $listUpdateSchedules = array_to_json([(object)$listUpdateSchedules]);

        $param = [
            'timeZone' => 7,
            'scheduleId' => $request->scheduleId,
            'listUpdateSchedules' => $listUpdateSchedules,
        ];
        $result = $this->makeRequest('web_schedule/update-user-for-schedule', $param);
        $result['param']=$param;

        if($request->ajax()){
            return response()->json($result);
        }
        if ($result['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Cập nhật chuyến thành công','');"]);

        } else {
            return redirect()->back()->withInput()->with(['msg' => "Message('Thông báo','Cập nhật chuyến thất bại <br>" . checkMessage($result['results']['error']['code']) . "','');"]);
        }
    }

    function postEditTrip(Request $request)
    {
        $dateRun = $request->dateRun;
        $newDate = $request->newDate;
        $day = $request->day;

        $listUpdateTrips = array_to_json([(object)[
            'tripId' => $request->tripId,
            'dateRun' => $dateRun,
            'newDate' => strMilisecond($newDate) + date_to_milisecond($day),
        ]]);

        $param = [
            'timeZone' => 7,
            'scheduleId' => $request->scheduleId,
            'listUpdateTrips' => $listUpdateTrips,
        ];

        $result = $this->makeRequest('web_trip/update-time', $param);
        if ($result['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Cập nhật thời gian xuất bến thành công','');"]);

        } else {
            return redirect()->back()->withInput()->with(['msg' => "Message('Thông báo','Cập nhật chuyến thất bại <br>" . checkMessage($result['results']['error']['code']) . "','');"]);
        }
    }

    public function search(Request $request)
    {
        $startDate = $request->has('startDate') ? date_to_milisecond($request->startDate) : 0;
        $endDate = $request->has('endDate') ? date_to_milisecond($request->endDate) : strtotime('+1 month') * 1000;

        if($request->new)
        {
            $url = 'web/find-schedule-like-for-admin-new';
        } else {
            $url = 'web/find-schedule-like-for-admin';
        }


        if ($request->has('startPointId') && $request->has('endPointId')) {
            $response = $this->makeRequestWithJson($url,
                [
                    'page' => 0,
                    'count' => 100,
                    'timeZone' => 7,
                    'start' => $request->startPointId,
                    'end' => $request->endPointId,
                    'date' => date_to_milisecond($request->date),
                    'companyId' => session('companyId'),
                    'routeId' => $request->routeId
                ],
                ['DOBODY6969' => session('userLogin')['token']['tokenKey']],
                'POST');
            $result = array_get($response, 'results.result', head($response['results']));
        } else {
            $response = $this->makeRequest('web/get-schedule-by-company',
                [
                    'page' => 0,
                    'count' => 1000,
                    'timeZone' => 7,
                    'companyId' => session('companyId'),
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                    'listRouteId' => $request->routeId
                ], ['DOBODY6969' => $this->TOKEN_SUPPER],
                'GET');
            $result = array_get($response, 'results', head($response['results']));

        }
        return response()->json($result, 200, [], JSON_PRETTY_PRINT);
    }

    public function searchByTripId(Request $request)
    {
        $response = $this->makeRequest('web_trip/view-trip',
            [
                'tripId' => $request->tripId
            ]);
        $result = head($response['results']);

        if ($request->getCustomer) {
            $listGuestPerPoint = array_collapse(array_divide($result['listGuestPerPoint'])[1]);
            return response()->json($listGuestPerPoint, 200, [], JSON_PRETTY_PRINT);
        }


        return response()->json($result, 200, [], JSON_PRETTY_PRINT);
    }

    public function cancelTrip(Request $request)
    {
        $result = $this->makeRequest('web_trip/cancel-trip', [
            'tripId' => $request->tripId,
            'scheduleId'=>$request->scheduleId,
            'startTime' =>$request->startTime,
        ]);

        if($request->ajax()){
            return response()->json($result);
        }
        if ($result['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Huỷ chuyến thành công','');"]);
        } else {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Huỷ chuyến thất bại.<br>Mã lỗi: " . checkMessage($result['results']['error']['propertyName']) . "','');"]);
        }

    }

    public function startTrip(Request $request)
    {
        $listDriverRealityId = !is_null($request->listDriverRealityId) && count((array)$request->listDriverRealityId)>0 ? array_to_json((array)$request->listDriverRealityId) : '';
        $listAssistantRealityId = !is_null($request->listAssistantRealityId) && count((array)$request->listAssistantRealityId)>0 ? array_to_json((array)$request->listAssistantRealityId) : '';
        $result = $this->makeRequest('web_trip/start-trip', [
            'tripId' => $request->tripId,
            'listDriverRealityId' => $listDriverRealityId,
            'listAssistantRealityId' => $listAssistantRealityId,
            'sendSMS' => $request->sendSMS,
            'note' => $request->note,
        ]);

        if($request->ajax()){
            return response()->json($result);
        }
        if ($result['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Xuất bến thành công','');"]);
        } else {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Xuất bến thất bại.<br>Mã lỗi: " . checkMessage($result['results']['error']['propertyName']) . "','');"]);
        }
    }

    public function openStartTrip(Request $request){
        $result = $this->makeRequestWithJson('trip/open', ['tripId' => $request->tripId]);

        if($request->ajax()){
            return response()->json($result);
        }
        if ($result['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Xuất bến thành công','');"]);
        } else {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Xuất bến thất bại.<br>Mã lỗi: " . checkMessage($result['results']['error']['propertyName']) . "','');"]);
        }
    }

    public function endTrip($id)
    {
        $result = $this->makeRequest('web_trip/end-trip', ['tripId' => $id]);

        if ($result['status'] == 'success') {
            return redirect()->action('BalanceSheetController@expenses', ['tripId' => $id])->with(['msg' => "Message('Thông báo','Kết thúc chuyến thành công','');"]);
        } else {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Kết thúc chuyến.<br>Mã lỗi: " . checkMessage($result['results']['error']['propertyName']) . "','');"]);
        }
    }

    public function control(Request $request)
    {
        $page = $request->has('page') ? $request->page : 1;

        $response = $this->makeRequest('web_trip/getlist', [
            'page' => $page - 1,
            'count' => 10,
            'routeId' => $request->routeId,
            'numberPlate' => $request->numberPlate,
            'option' => $request->option
        ]);

        $listRoute = $this->makeRequest('web_route/getlist', []);

        $nhanvien = $this->makeRequestWithJson('user/getlist', [
            'page' => 0,
            'count' => 1000
        ]);

        $listItem = $this->makeRequestWithJson('items/getlist', ['page' => $page - 1, 'count' => 100]);


        return view('cpanel.Trip.control')->with([
            'listTrip' => array_get($response['results'], 'listTrip', []),
            'listRoute' => head($listRoute['results']),
            'page' => $page,
            'listItem' => $listItem['results']['result'],
            'nhanvien' => $nhanvien['results']['result']
        ]);
    }

    public function transfer(Request $request)
    {
        if ($request->has('tripSourceId')) {
            $tripSourceId = $request->tripSourceId;
            $tripDestinationId = $request->tripDestinationId;

            $response_tripSource = $this->makeRequest('web_trip/view-trip',
                [
                    'tripId' => $tripSourceId
                ]);
            $response_tripDestination = $this->makeRequest('web_trip/view-trip',
                [
                    'tripId' => $tripDestinationId
                ]);

            $listRoute = $this->makeRequest('web_route/getlist', ['page' => 0, 'count' => 100]);

            return view('cpanel.Trip.transfer')->with([
                'tripSource' => $response_tripSource['results'],
                'listRoute' => $listRoute['results']['result'],
                'tripDestination' => $response_tripDestination['results']
            ]);
        }
        return redirect()->action('TripController@control');
    }

    public function postTransfer(Request $request)
    {
//        dev($request->listSeatSourceId, false);
        $listSeat = $this->removeItemNull($request->listSeatSourceId, $request->listSeatDestinationId);
        $param = [
            'timeZone'=>7,
            'tripSourceId' => $request->tripSourceId,
            'tripDestinationId' => $request->tripDestinationId,
            'numberOfGuests' => $request->numberOfGuests,
            'timeChange' => date_to_milisecond($request->timeChange),
            'place' => $request->place,
            'scheduleId'=>$request->scheduleId,
            'listSeatSourceId' => array_to_json($this->convertIntToStrArr(array_keys($listSeat))), // Danh sách ghế cũ
            'listSeatDestinationId' => array_to_json($this->convertIntToStrArr(array_values($listSeat))), // Danh sách ghế chuyển
            'destinationInPointId' =>$request->destinationInPointId,
            'destinationOffPointId' =>$request->destinationOffPointId,
        ];

        if($request->has('ticketId')&&$request->option=="khoiphuc"){
            $param['ticketId']=$request->ticketId;
        }
        if($request->has('listDriverId')){
            $param['listDriverId']=array_to_json(explode(',',$request->listDriverId));
        }
        if($request->has('listAssistantId')){
            $param['listAssistantId']=array_to_json(explode(',',$request->listAssistantId));
        }

        if(isset($request->vehicleId) && $request->vehicleId != '')
        {
            $param = array_merge($param, [
                'vehicleId' => $request->vehicleId
            ]);
        }

        $result = $this->makeRequestWithJson('/general_ticket/transfer', $param);
        $result['param']=$param;
        $result['option']=$request->option;
        if($request->ajax()){
            return response()->json(['result' => $result]);
        }
        if ($result['status'] == 'success') {
                return redirect()->back()->with(['msg' => "Message('Thông báo','Chuyển thành công','');"]);

        }else{
            return redirect()->back()->withInput()->with(['msg' => "Message('Thông báo','Chuyển thất bại<br>" . checkMessage($result['results']['error']['propertyName']) . "','');"]);
        }
    }

    private function convertIntToStrArr($arr){
        $arr = array_map('strval', $arr);
        return $arr;
    }

    /*
     * Loại bỏ giá trị Null
     * Truyền 2 mảng, 1 mảng làm key, 1 mảng làm value
     * return về mảng mới
     * */
    private function removeItemNull($key, $value){
        $key = !is_array($key) ? explode(",", $key) : $key;
        $value = !is_array($value) ? explode(",", $value) : $value;
        foreach ($value as $k=>$val){
            if($val == null){
                $value[$k]="";
            }
        }
//        dev($key,false);
//        dev($value);
        $newArray =array_combine($key, $value);//gộp 2 mảng lại
        $newArray = array_diff($newArray, array(''));//xóa các phần tử rỗng trong mảng
        return $newArray;
    }


    public function log(Request $request)
    {
        $page = $request->has('page') ? $request->page : 1;
        $startDate = $request->has('startDate') ? date_to_milisecond($request->startDate, 'begin') : 0;
        $endDate = $request->has('endDate') ? date_to_milisecond($request->endDate, 'end') : strtotime('+1 month') * 1000;
        $result = $this->makeRequest('web_trip/get-list-trip-activity',
            [
                'page' => $page - 1,
                'count' => 10,
                'isActive' => 'true',
                'timeZone' => '7',
                'startDate' => $startDate,
                'endDate' => $endDate,
                'numberPlate' => $request->numberPlate,
//                'tripId' => $request->tripId,
                'content' => $request->_content
            ]);
        if (empty($request->tripId)) {
            return view('cpanel.Trip.log')->with([
                'result' => head($result['results']),
                'page' => $page
            ]);

        } else {
            return redirect()->action('TripController@logdetail', [
                'TripId' => $request->tripId
            ]);
        }

    }

    public function logdetail($TripId)
    {
        // $page = $request->has('page') ? $request->page : 1;
        $result = $this->makeRequest('web_trip/get-list-trip-activity',
            [
                'page' => 0,
                'count' => 100,
                'isActive' => 'true',
                'tripId' => $TripId,

            ]);
        return view('cpanel.Trip.log_detail')->with([
            'tripId' => $TripId,
            'result' => head($result['results'])
        ]);
    }

    function printVoidTicket(Request $request)
    {
        $response = $this->makeRequest('web_trip/view-trip',
            [
                'tripId' => $request->tripId
            ]);
        $result = head($response['results']);

        $listGuestPerPoint = array_collapse(array_divide($result['listGuestPerPoint'])[1]);
//        dev($result,false);
//        dev($listGuestPerPoint);
        return view('cpanel.Print.printTrip')->with([
            'result' => $result,
            'listGuestPerPoint' => $listGuestPerPoint
        ]);
    }

    function sortFunction($a, $b)
    {
        return strtotime($a["date"]) - strtotime($b["date"]);
    }

    public function getAddPriceHoliday(Request $request)
    {
        $response_seatMap = $this->makeRequest('web_seatmap/getlist', [
            'page' => 0,
            'count' => 100,
            //'companyId'=>session('companyId')
        ]);
        $response_vehicletype = $this->makeRequest('web_vehicletype/getlist', [
            'page' => 0,
            'count' => 1000,
            'companyId' => session('companyId')
        ]);
        $listRoute = $this->makeRequest('web_route/getlist', []);
        return view('cpanel.Trip.HolidayPrice.add')->with([
            'listSeatMap' => $response_seatMap['results']['result'],
            'listVehicleType' => $response_vehicletype['results']['result'],
            'listRoute' => $listRoute['results']['result']
        ]);
    }

    public function postAddPriceHoliday(Request $request)
    {
        $startDate = $request->has('startDate') ? date_to_milisecond($request->startDate, 'begin') : time() * 1000;
        $endDate = $request->has('endDate') ? date_to_milisecond($request->endDate, 'end') : strtotime('+1 day') * 1000;
        $routeId = $request->routeId;
        $seatMapId = $request->seatMapId;
        $seatList = $request->seatList;
//        dev([
//            'timeZone'=>7,
//            'startDate'=>$startDate,
//            'endDate'=>$endDate,
//            'routeId'=>$routeId,
//            'seatMapId'=>$seatMapId,
//        ]);
        $result = $this->makeRequestWithJson('price_table/create',[
           'timeZone'=>7,
           'startDate'=>$startDate,
           'endDate'=>$endDate,
           'routeId'=>$routeId,
           'seatMapId'=>$seatMapId,
            'tripId'=>'',
            'scheduleId'=>'',
            'seatList'=>$seatList,
        ]);
        //dev($request->seatList);
//        dev([
//            'timeZone'=>7,
//            'startDate'=>$startDate,
//            'endDate'=>$endDate,
//            'routeId'=>$routeId,
//            'seatMapId'=>$seatMapId,
//            'tripId'=>'',
//            'scheduleId'=>''
//        ],false);
//        dev($result);
        if ($result['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Tạo bảng giá thành công','');"]);
        } else {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Tạo bảng giá thất bại.<br>Mã lỗi: " . checkMessage($result['results']['error']['propertyName']) . "','');"]);
        }
    }

    public function getSeatMapByTripId(Request $request)
    {
        $responseSeatMap = $this->makeRequestWithJson('trip/view_detail_seat_map_in_trip', [
            'tripId' => $request->tripId,
            'scheduleId' => $request->scheduleId,
            'date' => $request->date,
        ],null,'POST');
        $result = head($responseSeatMap['results']);
        return response()->json($result, 200, [], JSON_PRETTY_PRINT);
    }

    public function updateLockTrip(Request $request)
    {
        if ($request->listOptionCancel) {
            $respon = $this->makeRequestWithJson('trip/lock_and_unlock',
                [
                    'timeZone' => 7,
                    'listLockTrip' => $request->listOptionCancel,
                    'tripId' => $request->tripId,
                    'scheduleId' => $request->scheduleId,
                    'date' => $request->dateRun
                ]);

        }
        if ($respon) return response()->json($respon);
    }
}
