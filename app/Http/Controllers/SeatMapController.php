<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SeatMapController extends Controller
{

    public function detail($id)
    {
        $response = $this->makeRequest('web_seatmap/getlist-all');
        $array = [];
        foreach ($response['results']['result'] as $key => $value) {
            if ($value['seatMapId'] == $id) {
                $array = $response['results']['result'][$key];
            }
        }

        echo json_encode($array);
    }

    public function show(Request $request)
    {
        $response = $this->makeRequest('web_seatmap/getlist', [
            'page' => 0,
            'count' => 100,
            'numberOfRows' => $request->numberOfRows,
            'numberOfColumns' => $request->numberOfColumns,
            'numberOfFloors' => $request->numberOfFloors,
            'vehicleTypeId' => $request->vehicleTypeId,
            'numberOfSeats' => $request->numberOfseats
        ]);
        echo json_encode(array_get($response['results'], 'result', []));
    }

    public function postAdd(Request $request)
    {
        $vehicleTypeId = $request->vehicleTypeId;
        $numberOfRows = $request->numberOfRows;
        $numberOfColumns = $request->numberOfColumns;
        $numberOfFloors = $request->numberOfFloors;
        $seatList = $request->seatList;
        $seatMapName = $request->seatMapName;
        $result = $this->makeRequest('web_seatmap/create', [
            'numberOfRows' => $numberOfRows,
            'numberOfColumns' => $numberOfColumns,
            'numberOfFloors' => $numberOfFloors,
            'vehicleTypeId' => $vehicleTypeId,
            'seatList' => $seatList,
            'seatMapName' => $seatMapName
        ]);
        return  $result;

    }

    public function getPaternSeatMap(){
        $response = $this->makeRequestWithJson('web_seatmap/getlist-sample',[]);
        return $response['results']['result'] ?$response['results']['result']:[];
    }
    public function edit(Request $request)
    {

        $vehicleTypeId = $request->vehicleTypeId;
        $numberOfRows = $request->numberOfRows;
        $numberOfColumns = $request->numberOfColumns;
        $numberOfFloors = $request->numberOfFloors;
        $seatList = $request->seatList;
        $seatMapId = $request->seatMapId;
        $seatMapName=$request->seatMapName;

        $result = $this->makeRequest('web_seatmap/update', [
            'seatMapId' => $seatMapId,
            'numberOfRows' => $numberOfRows,
            'numberOfColumns' => $numberOfColumns,
            'numberOfFloors' => $numberOfFloors,
            'vehicleTypeId' => $vehicleTypeId,
            'seatList' => $seatList,
            'seatMapName'=>$seatMapName
        ]);

        echo json_encode($result);

    }

    public function delete(Request $request)
    {

        $result = $this->makeRequest('web_seatmap/delete', [
            'seatMapId' => $request->seatMapId
        ]);
        if ($result['status'] == 'success') {
            return redirect()->back()->withInput()->with(['msg' => "Message('Thành công','Xoá sơ đồ thành công','');"]);
        } else {
            return redirect()->back()->withInput()->with(['msg' => "Message('Thất bại','Xoá sơ đồ thất bại','');"]);
        }
    }
}
