<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $bash_url = CONFIG_AV['dev']['url'];
    public $TOKEN_SUPPER = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ2IjowLCJkIjp7InVpZCI6IkFETTExMDk3Nzg4NTI0MTQ2MjIiLCJmdWxsTmFtZSI6IkFkbWluIHdlYiIsImF2YXRhciI6Imh0dHBzOi8vc3RvcmFnZS5nb29nbGVhcGlzLmNvbS9kb2JvZHktZ29ub3cuYXBwc3BvdC5jb20vZGVmYXVsdC9pbWdwc2hfZnVsbHNpemUucG5nIn0sImlhdCI6MTQ5MjQ5MjA3NX0.PLipjLQLBZ-vfIWOFw1QAcGLPAXxAjpy4pRTPUozBpw';

    public function __construct() {
        $this->bash_url = CONFIG_AV[env('APP_ENV', 'dev')]['url'];
    }

    /*
     *Hàm lấy data từ api
     *
     *@param đường dẫn lấy data $url
     *
     *@return  trả về kế quả là chuỗi json
     *
     *  */
    public function makeRequest($url, $params = [], $_headers_info = null, $method = 'POST', $otherAPI = true)
    {
        $headers_info = null;

        if(session('userLogin')) {
            $headers_info = ['DOBODY6969' => session('userLogin')['token']['tokenKey']];
        }

        $client = new Client(['headers' => $_headers_info != null ? $_headers_info : $headers_info]);
        if($otherAPI)
        {
            $path = $this->bash_url . $url;
        }else{
            $path = $url;
        }
        if ($method == 'POST') {
            $res = $client->request($method, $path, [
                'http_errors' => false,
                'form_params' => $params
            ]);
        } else {
            $res = $client->request($method, $path, [
                'http_errors' => false,
                'query' => $params
            ]);
        }


        $result = json_decode($res->getBody(), true);
        if ($res->getStatusCode() == '200') {

            return $result;
        } else {
            if($result['results']!=null)
            if (array_get(head($result['results']), 'propertyName') == 'USR_ERROR_000000000' || array_get(head($result['results']), 'propertyName') == 'USER_NOT_LOGGED_IN_YET') {
                session()->forget('userLogin');
            }
            if (isset($result['results']['error'])) {
                $result['results']['result'] = [];
                return $result;
            } else {
                return array('code' => $res->getStatusCode(), 'status' => 'error', 'results' => ['result' => [], 'error' => ['code' => 'UNKNOWN', 'propertyName' => 'UNKNOWN']]);
            }

        }

    }

    public function makeRequestWithJson($url, $params = [], $_headers_info = null, $method = 'POST')
    {
        $headers_info = [
            'DOBODY6969' => session('userLogin')['token']['tokenKey'],
            'Content-Type' => 'application/json'
        ];

        $client = new Client(['headers' => $_headers_info != null ? $_headers_info : $headers_info]);

        $path = $this->bash_url . $url;
        $res = $client->request($method, $path, [
            'http_errors' => false,
            'json' => $params
        ]);

        $result = json_decode($res->getBody(), true);
        if ($res->getStatusCode() == '200') {

            return $result;
        } else {
            if($result['results']!=null)
            if (array_get(head($result['results']), 'propertyName') == 'USR_ERROR_000000000' || array_get(head($result['results']), 'propertyName') == 'USER_NOT_LOGGED_IN_YET') {
                session()->forget('userLogin');
            }
            if (isset($result['results']['error'])) {
                $result['results']['result'] = [];
                return $result;
            } else {
                return array('code' => $res->getStatusCode(), 'status' => 'error', 'results' => ['result' => [], 'error' => ['code' => 'UNKNOWN', 'propertyName' => 'UNKNOWN']]);
            }
        }
    }

    public function makeRequestUploadImages($url, $file, $_headers_info = null)
    {
        $headers_info = ['DOBODY6969' => session('userLogin')['token']['tokenKey']];

        $client = new Client(['headers' => $_headers_info != null ? $_headers_info : $headers_info]);

        $path = $this->bash_url . $url;
        $res = $client->request('POST', $path, [
            'http_errors' => false,
            'multipart' =>
                [
                    [
                        'Content-type' => 'multipart/form-data',
                        'name' => 'upload Image',
                        'contents' => $file
                    ]
                ]
        ]);


        $result = json_decode($res->getBody(), true);

        if ($res->getStatusCode() == '200') {
            return $result;
        } else {
            if (isset($result['results']['error'])) {
                $result['results']['result'] = [];
                return $result;
            } else {
                return array('code' => $res->getStatusCode(), 'status' => 'error', 'results' => ['result' => [], 'error' => ['code' => 'UNKNOWN', 'propertyName' => 'UNKNOWN']]);
            }

        }

    }

}