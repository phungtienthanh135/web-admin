<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    //
    public function show(Request $request){
    	$result = $this->makeRequest('department/getlist',[]);
    	return view('cpanel.Department.show')->with(['data'=>$result]);
    }

    public function postAdd(Request $request){
    	$result = $this->makeRequestWithJson('department/create',[
    		'departmentName'=>$request->departmentName
    	]); 
    	if($result['status'] == 'success'){
            return redirect()->route('department.show')->with(["msg"=>MessageJS("Thêm Phòng Thành Công")]);
        }else{
            return redirect()->route('department.show')->with(['msg' => MessageJS("Thêm Phòng thất bại <br>" . checkMessage($result['results']['error']['propertyName']))]);
        }
    }

    public function postEdit(Request $request){
    	$result = $this->makeRequestWithJson('department/update',[
    		'departmentId' => $request->departmentId,
    		'departmentName'=>$request->departmentName
    	]);
    	if($result['status'] == 'success'){
            return redirect()->route('department.show')->with(["msg"=>MessageJS("Sửa Phòng Thành Công")]);
        }else{
            return redirect()->route('department.show')->with(['msg' => MessageJS("Sửa Phòng thất bại <br>" . checkMessage($result['results']['error']['propertyName']))]);
        }
    }

    public function delete($departmentId){
    	$result = $this->makeRequestWithJson('department/delete',[
    		'departmentId'=>$departmentId
    	]);
    	if($result['status'] == 'success'){
            return redirect()->route('department.show')->with(["msg"=>MessageJS("Xóa Phòng Thành Công")]);
        }else{
            return redirect()->route('department.show')->with(['msg' => MessageJS("Xóa Phòng thất bại <br>" . checkMessage($result['results']['error']['propertyName']))]);
        }
    }
}
