<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PackageServiceController extends Controller
{
    //
    public  function getViewListPackage(){
        $listPackage = $this->makeRequestWithJson('anvui_service_pack/getlist',['serviceType'=>1]);
        if(session()->has('userBuyPack')||session()->has('userLogin')){
            return view('cpanel.PackageService.show')->with(['listPackage'=>$listPackage['results']['result'],'msg'=>'']);
        }else{
            return redirect()->back();
        }
    }
    public function buyPackage(Request $request){
        $userName = '';
        if(session()->has('userBuyPack')){
            $userName = session('userBuyPack');
        }else if(session()->has('userLogin')){
            $userName = session('userLogin')['userInfo']['userName'];
        }
        $param = [
          'ticketId' => $request->ticketId,
          'paymentType'=>"1",
          'userName' => $userName
        ];
        $res = $this->makeRequestWithJson('ePay/payment-for-service-pack',$param);
        $res['param']=$param;
        if($res['code']==200){
            return Redirect::to($res['results']['redirect']);
        }else{
            return redirect()->back()->with(['msg'=>'Có lỗi xảy ra khi gia hạn']);
        }
    }
}
