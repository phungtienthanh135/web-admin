<?php
/**
 * Created by PhpStorm.
 * User: My Laptop
 * Date: 5/29/2018
 * Time: 5:21 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class settingPrintTicketController extends Controller
{


    public function setup()
    {

        $results = DB::table('configprintticket')
            ->rightJoin('attributeprintticket', function ($join) {
                $join->on('configprintticket.idAttribute', '=', 'attributeprintticket.id')
                    ->where('configprintticket.companyId', '=', session('companyId'));
            })->orderBy('id','asc')->get();
//        dev(json_decode(json_encode($results), True));
        return view('cpanel/System/settingPrintTicket')->with(['result' => json_decode(json_encode($results), True)]);
    }

    public function updateSetting(Request $request)
    {
        $listAttr = json_decode($request->listAttribute);
        $listAttr = json_decode(json_encode($listAttr), True);
        foreach ($listAttr as $node) {
            foreach ($node as $row) {
                $listUse[]=$row['idAttribute'];
            }}
        $listDelete = DB::table('configprintticket')
            ->where('companyId', '=', session('companyId'))
            ->whereNotIn('idAttribute',$listUse)
            ->get();
        if($listDelete!=null){
            $listDelete=json_decode(json_encode($listDelete),true);
            foreach ($listDelete as $delete){
                DB::table('configprintticket')
                    ->where([['companyId', '=', session('companyId')],['idAttribute','=',$delete['idAttribute']]])
                    ->delete();
            }
        }
        foreach ($listAttr as $node) {
            foreach ($node as $row) {
                $checkExist = DB::table('configprintticket')->where([['companyId', '=', session('companyId')], ['idAttribute', '=', $row['idAttribute']]])->count();
                if ($checkExist != null) {
                    DB::table('configprintticket')
                        ->where([['companyId', '=', session('companyId')], ['idAttribute', '=', $row['idAttribute']]])
                        ->update(
                            ['status' => 1,
                                'size' => (int)$row['size'],
                                'bold' => (int)$row['bold'],
                                'fontfamily' => (int)$row['fontfamily'],
                                'orderDisplay' => (int)$row['orderDisplay'],
                                'alias' => $row['alias']]
                        );
                } else {
                    DB::table('configprintticket')
                        ->insert([
                            'companyId' => session('companyId'),
                            'idAttribute' => $row['idAttribute'],
                            'status' => 1,
                            'size' => (int)$row['size'],
                            'bold' => $row['bold'],
                            'fontfamily' => (int)$row['fontfamily'],
                            'orderDisplay' => (int)$row['orderDisplay'],
                            'alias' => $row['alias']
                        ]);

                }
            }
        }
        return redirect()->back()->with(['msg' => "Message('Thành công','Cập nhật cài đặt thành công','');"]);
    }
}