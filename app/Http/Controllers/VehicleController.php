<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\VehicleRequest;

class VehicleController extends Controller
{
    /*
    * hiển thị danh sách
     * vehicleStatus
     * INVALID(0), // Không xác định
        NEED_CENSORED(1), // Cần xác thực
        AVAILABLE(2), // Sẵn sàng nhận chuyến
        TRAVELING(3), // Đang chạy
        PENDING(4), // Đang nghỉ
        DELETED(5); // Đã xóa
    * */
    public function show(Request $request)
    {
        $response_vehicletype = $this->makeRequest('web_vehicletype/getlist', [
            'page' => 0,
            'count' => 1000,
            'companyId' => session('companyId')
        ]);
        $vehicleType = $this->makeRequest('web_vehicletype/getlist',
            [
                "page"=>0,
                "count"=>200,
                'companyId'=>session('companyId')
            ]);
        $insuranceEndTime = $request->insuranceEndTime ? date_to_milisecond($request->insuranceEndTime) : 0;
        $page = $request->has('page') ? $request->page : 1;
        $vehicleStatus = $request->has('vehicleStatus') ? [$request->vehicleStatus] : [1, 2, 3, 4];
        $response = $this->makeRequestWithJson('vehicle/getlist', [
            'timeZone'=>7,
            'page' => $page - 1,
            'count' => 10,
            'vehicleStatus' => $vehicleStatus,
            'numberPlate' => $request->numberPlate,
            'numberOfSeats' => $request->numberOfSeats,
            'searchKeyWord' => $request->searchKeyWord,
//            'seatMapType' => $request->seatMapType,
            'vehicleTypeId' => $request->vehicleTypeId,
            'vehicleName' => $request->vehicleName,
            'insuranceEndTime' => $insuranceEndTime
        ]);
        $response['pr'] = [
            'timeZone'=>7,
            'page' => $page - 1,
            'count' => 10,
            'vehicleStatus' => $vehicleStatus,
            'numberPlate' => $request->numberPlate,
            'numberOfSeats' => $request->numberOfSeats,
            'searchKeyWord' => $request->searchKeyWord,
//            'seatMapType' => $request->seatMapType,
            'vehicleTypeId' => $request->vehicleTypeId,
            'vehicleName' => $request->vehicleName,
            'insuranceEndTime' => $insuranceEndTime
        ];

        return view('cpanel.Vehicle.show')->with([
            'result' => $response['results']['result'],
            'page' => $page,
            'listVehicleType' => array_get($response_vehicletype['results'], 'result', []),
            "vehicleType" => $vehicleType['results']['result'],
        ]);
    }


    /*
     * Thêm
     * */

    public function getadd()
    {

        $response_seatMap = $this->makeRequest('web_seatmap/getlist', [
            'page' => 0,
            'count' => 100,
            //'companyId'=>session('companyId')
        ]);
        $response_vehicletype = $this->makeRequest('web_vehicletype/getlist', [
            'page' => 0,
            'count' => 1000,
            'companyId' => session('companyId')
        ]);

        return view('cpanel.Vehicle.add')->with([
            'listSeatMap' => $response_seatMap['results']['result'],
            'listVehicleType' => $response_vehicletype['results']['result']
        ]);
    }

    public function postadd(VehicleRequest $request)
    {
        $insuranceEndTime = $request->insuranceEndTime ? date_to_milisecond($request->insuranceEndTime) : 0;
        $result = $this->makeRequest('web_vehicle/create', [
            'timeZone'=>7,
            'insuranceEndTime'=>$insuranceEndTime,
            'numberPlate' => $request->numberPlate,
            'originPrice' => $request->originPrice,
            'origin' => $request->origin,
            'registrationDeadline' => date_to_milisecond($request->registrationDeadline),
            'estimatedFuelConsumption' => date_to_milisecond($request->estimatedFuelConsumption),
            'listImages' => '[]',
            'seatMapId' => $request->seatMapId,
            'vehicleTypeId' => $request->vehicleTypeId_pattern?$request->vehicleTypeId_pattern: $request->vehicleTypeId,
            'remain' => $request->remain,
            'phoneNumber' => $request->phoneNumber,
            //'productedYear'=>$request->productedYear,
            'reduceTime' => $request->reduceTime,
            'chasisNumber' => $request->chasisNumber,//số khung
            'engineNumber' => $request->engineNumber,// số máy
            'gpsId' => $request->gpsId,// mã thiết bị
        ]);
        if ($result['status'] == 'success') {
            return redirect()->action('VehicleController@show')->with(['msg' => "Message('Thành công','Thêm xe thành công','');"]);
        } else {
            return redirect()->back()->withInput()->with(['msg' => "Message('Thất bại','Thêm xe thất bại<br/>Mã lỗi: " . checkMessage($result['results']['error']['code']) . "','');"]);
        }
    }

    /*
     * Xoá
     *
     * */
    public function delete(Request $request)
    {

        $result = $this->makeRequest('web_vehicle/delete', ['vehicleId' => $request->vehicleId]);

        if ($result['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thành công','Xoá xe thành công','');"]);
        } else {
            return redirect()->back()->with(['msg' => "Message('Thất bại','Xoá xe thất bại<br/>Mã lỗi: " . checkMessage($result['results']['error']['propertyName']) . "','');"]);
        }
    }

    public function getEdit($id)
    {

        $response_seatMap = $this->makeRequest('web_seatmap/getlist', [
            'page' => 0,
            'count' => 1000,
            'companyId' => session('companyId')
        ]);
        $response_vehicletype = $this->makeRequest('web_vehicletype/getlist', [
            'page' => 0,
            'count' => 1000,
            'companyId' => session('companyId')
        ]);
        $response_vehicle_info = $this->makeRequest('web_vehicle/view', ['vehicleId' => $id]);

        return view('cpanel.Vehicle.edit')->with([
            'id' => $id, 'vehicleInfo' => $response_vehicle_info['results']['vehicle'],
            'listSeatMap' => $response_seatMap['results']['result'],
            'listVehicleType' => $response_vehicletype['results']['result']]);
    }

    public function postEdit(VehicleRequest $request)
    {
        $insuranceEndTime = $request->insuranceEndTime ? date_to_milisecond($request->insuranceEndTime) : 0;
        $result = $this->makeRequest('web_vehicle/update',[
            'timeZone'=>7,
            'insuranceEndTime'=>$insuranceEndTime,
            'numberPlate' => $request->numberPlate,
            'originPrice' => $request->originPrice,
            'reduceTime' => $request->reduceTime,
            'registrationDeadline' => date_to_milisecond($request->registrationDeadline),
            'estimatedFuelConsumption' => $request->estimatedFuelConsumption,
            'listImages' => json_encode(array()),
            'seatMapId' => $request->seatMapId,
            'vehicleName' => $request->vehicleName,
            'vehicleId' => $request->vehicleId,
            'vehicleTypeId' => $request->vehicleTypeId,
            'remain' => $request->remain,
            'phoneNumber' => $request->phoneNumber,
            //'productedYear'=>$request->productedYear,
            //'origin' => $request->origin,
            'chasisNumber' => $request->chasisNumber,
            'engineNumber' => $request->engineNumber,
            'gpsId' => $request->gpsId,
        ]);
        if ($result['status'] == 'success') {
            return redirect()->action('VehicleController@show')->with(['msg' => "Message('Thành công','Cập nhật xe thành công','');"]);
        } else {
            return redirect()->back()->withInput()->with(['msg' => "Message('Thất bại','Cập nhật xe thất bại<br/>Mã Lổi: " . checkMessage($result['results']['error']['propertyName']) . "','');"]);
        }
    }

    public function getSeatMapByVehicleId(Request $request)
    {
        $seatMap = $this->makeRequest('web_vehicle/view', ['vehicleId' => $request->id]);

        return response()->json($seatMap['results']['vehicle']['seatMap'], 200, [], JSON_PRETTY_PRINT);
    }
    public function locationOnline(Request $request){
        return view('cpanel.Vehicle.locationOnline');
    }
    public function getLocationOnlineData(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"http://tracking.skysoft.vn/login");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "username=pxb_bgt&password=phucxuyendk663366");
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name');  //could be empty, but cause problems on some hosts
        curl_setopt($ch, CURLOPT_COOKIEFILE, 'test');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
// receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

//

// further processing ....
//        echo $server_output;
//        while (true == true)
//        {
            curl_setopt($ch, CURLOPT_URL, "http://tracking.skysoft.vn/onlinedata");
            $filecontent = curl_exec($ch);
//            echo " ";
            echo $filecontent;
//            sleep(5);
//        }
        curl_close ($ch);
//        return response()->json($filecontent);
    }
}
