<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CouponController extends Controller
{

    public function show(Request $request)
    {
        $page = $request->has('page') ? $request->page : 1;
        $response_web_promotion = $this->makeRequest('web_promotion/getlist', [
            'page' => $page - 1,
            'count' => 10,
            'promotionCode' => $request->promotionCode,
            'userId' => $request->userId,
        ]);
        $listschedule = $this->makeRequest('web_schedule/getlist', [
            'count' => 10,
            'page' => $page - 1,
            'timeZone' => 7,
            'startDate' => 0,
            'endDate' => 2503964800000
        ]);
        $listVehicle = $this->makeRequestWithJson('vehicle/getlist', ['page' => 0, 'count' => 1000, 'vehicleStatus' => [2]]);
        $listRoute = $this->makeRequest('web_route/getlist', ['page' => 0, 'count' => 1000]);
        return view('cpanel.Coupon.show')->with([
            'listPromtion' => $response_web_promotion['results']['result'],
            'listVehicle' => $listVehicle['results']['result'],
            'listRoute' => $listRoute['results']['result'],
            'listschedule' => $listschedule['results']['result'],
            'page' => $page
        ]);
    }

    public function postAdd(Request $request)
    {
        if ($request->has('subjects')) {
            $listRoute = [];
            $listVehicle = [];
            $listSchedule = [];
            foreach ($request->subjects as $item) {
                switch (substr($item, 0, 2)) {
                    case 'RT':
                        array_push($listRoute, $item);
                        break;
                    case 'VE':
                        array_push($listVehicle, $item);
                        break;
                    case 'SC':
                        array_push($listSchedule, $item);
                        break;
                }
            }
            //array_push($listSchedule, []);
            //array_push($listVehicle, $listSchedule);
            //array_push($listRoute, $listVehicle);

            $subjects = array_to_json( (object)array_flatten($listRoute));
        }
        $startDate = date_to_milisecond($request->startDate);
        $endDate = $request->endDate == null ? strtotime('+1 year')*1000 : date_to_milisecond($request->endDate);

        /*$startDate = date('Ymd',strtotime($request->startDate));
        $endDate = $request->endDate == null ? date('Ymd',strtotime('+1 year')) : date('Ymd',strtotime($request->endDate));*/
        $mang=($request->subjects);$list='';
        if($mang != null && count($mang)>=1)
        for($i=0;$i<count($mang);$i++) {
            $list .= '"' . $mang[$i] . '":{"abc' . $i . '":[]}';
            if($i<(count($mang)-1) && count($mang)>1) $list.=',';
        }
        $subjects='{'.$list.'}';
        $param= [
            'timeZone' => 7,
            'promotionCode' => $request->promotionCode,
            'percent' =>$request->has('percent')?$request->percent / 100:'',
            'price' => $request->price,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'number' => $request->number,
            'promotionType' => $request->promotionType,
            'note' => $request->note,
            'subjects' =>$subjects,
            'minPriceApply' => $request->minPriceApply,
        ];
        if($request->listDate!=null)
            $param = array_merge($param, [
                'daysOfWeekApplied' => implode(',',$request->listDate)
            ]);
//       dev($request->listDate);
        $response = $this->makeRequest('web_promotion/create',$param);
        if ($response['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thành công','Thêm mã khuyến mãi thành công','');"]);
        } else {
            return redirect()->back()->withInput()->with(['msg' => "Message('Thất bại','Thêm mã khuyến mãi thất bại<br/>Mã lỗi: " . checkMessage($response['results']['error']['propertyName']) . "','');"]);
        }
    }
    public function postEdit(Request $request)
    {
        $startDate = date_to_milisecond($request->startDate);
        $endDate = $request->endDate == null ? strtotime('+1 year')*1000 : date_to_milisecond($request->endDate);
        $mang=($request->subjects);$list='';
        if($mang != null && count($mang)>=1)
            for($i=0;$i<count($mang);$i++) {
                $list .= '"' . $mang[$i] . '":{"abc' . $i . '":[]}';
                if($i<(count($mang)-1) && count($mang)>1) $list.=',';
            }
        $subjects='{'.$list.'}';
        $param=[
            'timeZone' => 7,
            'promotionId' => $request->promotionId,
            'percent' =>$request->has('percent')?$request->percent / 100:'',
            'price' => $request->price,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'number' => $request->number,
            'promotionType' => $request->promotionType,
            'note' => $request->note,
            'subjects' =>$subjects,
            'minPriceApply' => $request->minPriceApply,
        ];
        if($request->listDate!=null)
        $param = array_merge($param, [
            'daysOfWeekApplied' => implode(',',$request->listDate)
        ]);
        $response = $this->makeRequest('web_promotion/update', $param);
        if ($response['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thành công','Sửa mã khuyến mãi thành công','');"]);
        } else {
            return redirect()->back()->withInput()->with(['msg' => "Message('Thất bại','Sửa mã khuyến mãi thất bại<br/>Mã lỗi: " . checkMessage($response['results']['error']['propertyName']) . "','');"]);
        }
    }
    public function check(Request $request)
    {

        $response_check_promotion = $this->makeRequest('web_promotion/check', [
            'timeZone' => 7,
            'promotionCode' => $request->promotionCode,
            'scheduleId' => $request->scheduleId,
            'vehicleId' => $request->vehicleId,
            'routeId' => $request->routeId,
            'listSeatId' => $request->listSeatId,
            'getInTimePlan'=>$request->getInTimePlan,
            'pricePerSeat' => $request->pricePerSeat
        ]);
        return response()->json($response_check_promotion['results']);
    }

    public function delete(Request $request)
    {

        $response = $this->makeRequest('web_promotion/delete', ['promotionId' => $request->promotionId]);

        if ($response['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thành công','Xoá mã khuyến mãi thành công','');"]);
        } else {
            return redirect()->back()->with(['msg' => "Message('Thất bại','Xoá mã khuyến mãi thất bại<br/>Mã lỗi: " . checkMessage($response['results']['error']['propertyName']) . "','');"]);
        }

    }
}
