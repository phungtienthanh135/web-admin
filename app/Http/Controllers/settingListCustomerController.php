<?php
/**
 * Created by PhpStorm.
 * User: My Laptop
 * Date: 5/29/2018
 * Time: 5:21 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class settingListCustomerController extends Controller
{


    public function setup()
    {

        $results = DB::table('configlistcustomer')
            ->rightJoin('attribute', function ($join) {
                $join->on('configlistcustomer.idAttribute', '=', 'attribute.id')
                    ->where('configlistcustomer.companyId', '=', session('companyId'));
            })->orderBy('id', 'asc')
            ->get();
        $results = json_decode(json_encode($results), True);
        foreach ($results as $k => $row) {
            if ($row['id'] == 99) {
                $blankRow = $row;
                $key = $k;
            }
        }
        if ($blankRow != null && $key != null && count($blankRow) > 0)
            unset($results[$key]);
        return view('cpanel/System/setting')->with(['result' => $results, 'blankRow' => $blankRow]);
    }

    public function updateSetting(Request $request)
    {
        $listAttribute = [
            'index' => 'Số thứ tự',
            'ticketCode' => 'Mã vé',
            'numberSeat' => 'Số ghế',
            'listSeat' => 'Danh sách ghế',
            'customer' => 'Tên hành khách',
            'phone' => 'Số điện thoại hành khách',
            'pickup' => 'Điểm lên',
            'dropOff' => 'Điểm xuống',
            'agency' => 'Đại lý',
            'ticketStatus' => 'Trạng thái vé',
            'agencyPrice' => 'Giá thực thu',
            'paidMoney' => 'Tiền đã trả',
            'unPaidMoney' => 'Tiền còn nợ',
            'note' => 'Ghi chú'
        ];
        $listAttr = json_decode($request->listAttribute);
        $listAttr = json_decode(json_encode($listAttr), True);
        foreach ($listAttr as $node) {
            foreach ($node as $row) {
                $listUse[] = $row['idAttribute'];
            }
        }
        $listDelete = DB::table('configlistcustomer')
            ->where('companyId', '=', session('companyId'))
            ->whereNotIn('idAttribute', $listUse)
            ->get();
        if ($listDelete != null) {
            $listDelete = json_decode(json_encode($listDelete), true);
            foreach ($listDelete as $delete) {
                DB::table('configlistcustomer')
                    ->where([['companyId', '=', session('companyId')], ['idAttribute', '=', $delete['idAttribute']]])
                    ->delete();
            }
        }
        foreach ($listAttr as $node) {
            foreach ($node as $row) {
                $checkExist = DB::table('configlistcustomer')->where([['companyId', '=', session('companyId')], ['idAttribute', '=', $row['idAttribute']]])->count();
                if ($checkExist != null) {
                    DB::table('configlistcustomer')
                        ->where([['companyId', '=', session('companyId')], ['idAttribute', '=', $row['idAttribute']]])
                        ->update(
                            ['status' => 1,
                                'size' => (int)$row['size'],
                                'bold' => (int)$row['bold'],
                                'width' => (int)$row['width'],
                                'height' => (int)$row['height'],
                                'alias' => $row['alias'],
                                'quantity'=>(int)$row['quantity']
                                ]
                        );
                } else {
                    DB::table('configlistcustomer')
                        ->insert([
                            'companyId' => session('companyId'),
                            'idAttribute' => $row['idAttribute'],
                            'status' => 1,
                            'size' => (int)$row['size'],
                            'bold' => $row['bold'],
                            'width' => (int)$row['width'],
                            'height' => (int)$row['height'],
                            'alias' => $row['alias'],
                            'quantity'=>(int)$row['quantity']
                        ]);

                }
            }
        }
        return redirect()->back()->with(['msg' => "Message('Thành công','Cập nhật cài đặt thành công','');"]);
    }
}