<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SystemController extends Controller
{

    public function config()
    {
        $result = $this->makeRequest('web_company/view', [
            'companyId' => session('companyId')
        ]);
        $telecomCompanys = $this->makeRequest('/telecom/list',null);
        $emails = array_get($result['results'], 'company', [])['userContact']['email'];
        $emails = explode(",", $emails);
        return view('cpanel.System.config')->with([
            'result' => array_get($result['results'], 'company', []),
            'emails' => $emails,
            'telecomCompany' => $telecomCompanys['results']['result']
        ]);
    }

    public function postConfig(Request $request)
    {
        $allowAutoStartTrip = $request->has('allowAutoStartTrip') ? 'true' : 'false';
        $timeRangeAutoStartTrip = $request->has('timeRangeAutoStartTrip') ? $request->timeRangeAutoStartTrip : '';
        if($allowAutoStartTrip == 'false'){
            $timeRangeAutoStartTrip = 0;
        }
        $emails = $request->email;
        $emails = implode(',', $emails);
        $result = $this->makeRequest('web_company/update', [
            'companyId' => session('companyId'),
            'sellingTicketEndTime' => $request->sellingTicketEndTime,
            'timeBookHolder' => $request->timeBookHolder,
            'address' => $request->address,
            "telecomConfig"=>$request->telecomConfig,
            'telecomCompanyId' =>$request->telecomCompanyId,
            'telecomPhoneNumber' => $request->telecomPhoneNumber,
            'pastDaysToSellTicket' => $request->pastDaysToSellTicket,
            'sendMail' => $request->has('sendMail') ? 'true' : 'false',
            'sendSms' => $request->has('sendSms') ? 'true' : 'false',
            'email' => !empty($emails) ? $emails : '',
            'phoneNumber' => !empty($request->phoneNumber) ? $request->phoneNumber : '',
            'isEditTicketPrice' => $request->has('isEditTicketPrice') ? 'true' : 'false',
            'allowAutoStartTrip' => $allowAutoStartTrip,
            'isSellTicketWithoutSeat' => $request->has('isSellTicketWithoutSeat') ? 'true' : 'false',
            'futureDaysToSellTicket' => $request->has('futureDaysToSellTicket') ? $request->futureDaysToSellTicket : '',
            'timeRangeAutoStartTrip' => $timeRangeAutoStartTrip,
        ]);
        if ($result['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Cập nhật thông tin hệ thống thành công','');"]);
        } else {

            return redirect()->back()->withInput()->with(['msg' => "Message('Thông báo','Cập nhật thông tin hệ thống thất bại <br>" . checkMessage($result['results']['error']['propertyName']) . "','');"]);
        }

    }

    public function postImg(Request $request)
    {
        $result = $this->makeRequest('web_company/update', [
            'companyId' => session('companyId'),
            'companyLogo' => $request->img]);
        return $result;
    }

    public function info()
    {
        $result = $this->makeRequest('web_company/view', [
            'companyId' => session('companyId')
        ], ['DOBODY6969' => $this->TOKEN_SUPPER]);
        return view('cpanel.System.info')->with(['result' => array_get($result['results'], 'company', [])]);
    }

    public function UserActivity()
    {
        return view('cpanel.System.UserActivity');
    }

    public function postUpload(Request $request)
    {
        $result = $this->makeRequestUploadImages('image/upload', fopen($request->file('img')->getRealPath(), 'r'));
        return response()->json(['url' => array_get($result['results'], 'listImages.0.gcsServingUrl')]);
    }

    public function Upload(Request $request)
    {
        $result = $this->makeRequestUploadImages('image/upload', fopen($request->file('img')->getRealPath(), 'r'));
        return $result['results'];
    }

    public function listPhoneNumber(Request $request)
    {
        $page = $request->has('page') ? $request->page : 1;
        $telecomCompanys = $this->makeRequest('/telecom/list',null);
        $listUser = $this->makeRequestWithJson('user/getlist', [
            'page' => 0, 'count' => 1000]);

        $result = $this->makeRequestWithJson('branch_telecom/get-list', [
            'page' => $page - 1,
            'count' => RECORD_PER_PAGE
        ]);
        return view('cpanel.System.list-phone')->with([
            'page' => $page,
            'result' => array_get($result['results'], 'result'),
            'listUser' => array_get($listUser['results'], 'result'),
            'telecomCompany' => $telecomCompanys['results']['result']
        ]);
    }

    public function addOrUpdatePhoneNumber(Request $request)
    {

        if ($request->type == 1) {
            $result = $this->makeRequestWithJson('branch_telecom/create', [
                'number' => $request->phoneNumber,
                'userId' => $request->userId,
                "telecomConfig"=>$request->telecomConfig,
                'telecomCompanyId' =>$request->telecomCompanyId,
            ]);
            if ($result['status'] == 'success') {
                session()->put('telecomNumber', $request->phoneNumber);
                return redirect()->back()->with(['msg' => "Message('Thông báo','Thêm mới số máy lẻ thành công','');"]);
            } else {

                return redirect()->back()->withInput()->with(['msg' => "Message('Thông báo','Thêm mới số máy lẻ thất bại <br>" . checkMessage($result['results']['error']['propertyName']) . "','');"]);
            }
        } elseif ($request->type == 2) {
            $result = $this->makeRequestWithJson('branch_telecom/update', [
                'id' => $request->phoneId,
                'number' => $request->phoneNumber,
                'userId' => $request->userId,
                "telecomConfig"=>$request->telecomConfig,
                'telecomCompanyId' =>$request->telecomCompanyId,
            ]);
            if ($result['status'] == 'success') {

                session()->put('telecomNumber', $request->phoneNumber);
                return redirect()->back()->with(['msg' => "Message('Thông báo','Cập nhật thông tin số máy lẻ thành công','');"]);
            } else {

                return redirect()->back()->withInput()->with(['msg' => "Message('Thông báo','Cập nhật thông tin số máy lẻ thất bại <br>" . checkMessage($result['results']['error']['propertyName']) . "','');"]);
            }
        }

        return redirect()->back()->withInput()->with(['msg' => "Message('Thông báo','Có lỗi xảy ra','');"]);
    }

    public function deletePhone(Request $request)
    {
        $response = $this->makeRequest('branch_telecom/delete', ['id' => $request->phoneId]);

        if ($response['status'] == 'success') {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Xoá số máy lẻ thành công','');"]);
        } else {
            return redirect()->back()->with(['msg' => "Message('Thông báo','Xoá số máy lẻ thất bại<br>" . checkMessage($response['results']['error']['propertyName']) . "','');"]);
        }
    }

    public function setContentMessage(Request $request)
    {
        $param = [
            'nativeContent' => $request->nativeContent,
            'messageType' => $request->messageType,
            'code' => "MESSAGE_SAOVANG"
        ];
        $url = 'message/create';
        if($request->id){
            $param['id']=$request->id;
            $url = 'message/update';
        }
        $response = $this->makeRequestWithJson($url, $param);
        return $response;
    }

    public function getContentsMessage()
    {
        $response = $this->makeRequestWithJson('message/getList', ['page' => 0, 'count' => 5, 'messageType ' => [1, 2, 3]]);
        return view('cpanel.System.settingMessage')->with(['setting' => isset($response['results']['listMessage']) ? $response['results']['listMessage'] : []]);
    }
}