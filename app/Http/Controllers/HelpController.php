<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Picqer\Barcode\BarcodeGeneratorSVG;

class HelpController extends Controller
{


    public function show($string,Request $req)
    {
        if ($string == "home")  return view('cpanel/help/Home');
        if ($string == "agency") return view('cpanel.help.agency');
        if ($string == "ticket" ||$string=="coupon") return view('cpanel.help.ticket');
        if ($string == "ship") return view('cpanel.help.ship');
        if ($string == "control") return view('cpanel.help.trip');
        if ($string == "trip" || $string=="schedule") return view('cpanel.help.tripes');
        if ($string == "route" || $string=="point") return view('cpanel.help.route');
        if ($string == "vehicle" || $string == "vehicle-type") return view('cpanel.help.vehicle');
        if ($string == "user" || $string=="group-permission" || $string=="department") return view('cpanel.help.user');
        if ($string == "customer") return view('cpanel.help.customer');
        if ($string == "balance-sheet") return view('cpanel.help.balance-sheet');
        if ($string == "item" || $string == "Currency") return view('cpanel.help.item');
        if ($string == "report") return view('cpanel.help.report');
        if ($string == "system") return view('cpanel.help.system');

}
}
