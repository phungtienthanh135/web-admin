<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Picqer\Barcode\BarcodeGeneratorSVG;
use Illuminate\Support\Facades\Cookie;

class TicketController extends Controller
{
    private $_newForm = [
        'TC4003002759250837',
        'TC1OHmbCcxRS',
        'TC01gWSmr8A9Qx'
    ];

    private $ticketStatus = [
        1 => 'Hết hạn giữ chỗ',
        2 => 'Đang giữ chỗ',
        3 => 'Đã thanh toán',
        4 => 'Đã lên xe',
        5 => 'Đã hoàn thành',
        6 => 'Quá giờ giữ chỗ',
        7 => 'Ưu tiên giữ chỗ'
    ];

    public function show(Request $request)
    {
        date_default_timezone_set('GMT');
        $page = $request->has('page') ? $request->page : 1;
        $tickeStatus = !empty($request->status) ? array_to_json(explode(" ", $request->status)) : null;
        $params = [
            'timeZone' => 7,
            'page' => $page - 1,
            'count' => 10,
            'phoneNumber' => $request->phoneNumber,
            'phoneNumberReceiver' => $request->phoneNumberReceiver,
            'numberPlate' => $request->numberPlate,
            'type' => $request->type,
            'ticketCode' => $request->ticketCode,
            'ticketId' => $request->ticketId,
            'paymentType' => '1,2,3,4,5,6',
            'ticketStatus' => $tickeStatus
        ];
        if (!empty($request->date)) {
            $params = array_merge($params, [
                'date' => date_to_milisecond($request->date)
            ]);
        }
        $result = $this->makeRequest('web_ticket/get_list_ticket_for_company', $params);
        //dev($result);
        return view('cpanel.Ticket.show')->with([
            'result' => array_get($result['results'], 'ticket', []),
            'page' => $page,
            'ticketStatus' => $this->ticketStatus
        ]);

    }

    public function getListTicketCancelWithTripId(Request $request)
    {
        $params = [
            'timeZone' => 7,
            'page' => 0,
            'count' => 100,
            'tripId' => $request->tripId,
            'ticketStatus' => array_to_json(explode(',', $request->ticketStatus)),
        ];

        $result = $this->makeRequest('web_ticket/get_list_ticket_for_company', $params);
        $result = array_get($result, 'results', head($result['results']));
        return response()->json($result, 200, [], JSON_PRETTY_PRINT);
    }

    public function showTicketCancel(Request $request)
    {
        return view('cpanel.ticket.show.listTicketCancel');
    }

    public function sell()
    {
        /*Nếu là Interbuslines thì dùng giao diện mới
        * Tài khoản thaiyen vẫn dùng cũ
        */
        //dev(!in_array(session('companyId'), $this->_newForm));
//        dev(session('userLogin'));
        if (!in_array(session('companyId'), $this->_newForm)) {
            return redirect()->action('TicketController@sellVer1');
        }

        $listRoute = $this->makeRequest('web_route/getlist',
            [
                'page' => 0,
                'count' => 10,
            ]);


        return view('cpanel.Ticket.sell')->with([
            'listRoute' => head($listRoute['results']),
//            'config' => array_get($config['results'], 'company', [])
        ]);

    }

    public function sellVer1(Request $request)
    {
//        dev(session('userLogin'));
        $sml = $request->cookie('somayle');
        if($request->has('SoMayLe')){
            Cookie::queue('somayle', $request->SoMayLe, 42000);
            $sml = $request->SoMayLe;
        }
        $soMayLe = '';
//        dev(session('userLogin')['telecomNumber']);
        if(isset(session('userLogin')['telecomNumber'])){
            foreach (session('userLogin')['telecomNumber'] as $tel){
                if($sml!=null&&$sml==$tel['number']){
                    $soMayLe = $tel;
                    break;
                }
            }
        }
//        dev($soMayLe);
        $listRoute = $this->makeRequest('web_route/getlist',
            [
                'page' => 0,
                'count' => 100,
            ]);
        if($listRoute['code']!=200){
            return view('cpanel.Ticket.sell.module.errorInternet');
        }
        if(is_array($listRoute['results']['result'])&&count($listRoute['results']['result'])==0){
            return view('cpanel.Ticket.sell.module.emptyRoute');
        }
        $hanmuc = $this->makeRequestWithJson('/user/get_exist_allow_money', ['companyId' => session('userLogin')['userInfo']['companyId']]);
//        $listDriver = $this->makeRequest('web_admin/getlist', ['page' => 0, 'count' => 100, 'userType' => 2]);
//        $listAss = $this->makeRequest('web_admin/getlist', ['page' => 0, 'count' => 100, 'userType' => 3]);
//        $listVehicle = $this->makeRequest('web_vehicle/getlist', ['page' => 0, 'count' => 100, 'vehicleStatus' => [2]]);
        $listSetting = DB::table('configlistcustomer')
            ->where('configlistcustomer.companyId', '=', session('companyId'))
            ->join('attribute', 'idAttribute', '=', 'id')
            ->orderBy('id', 'asc')
            ->get();
        $listSetting=json_decode(json_encode($listSetting),true);
        return view('cpanel.Ticket.sell.sell-ver1.sell')->with([
            'listRoute' => head($listRoute['results']),
            'hanmuc' => $hanmuc,
            'listDriver' => [],
            'listVehicle' => [],
            'listAss' => [],
            'listSetting'=>$listSetting,
            'soMayLe'=>$soMayLe,
        ]);
    }

    public function postSell(Request $request)
    {
        // param cho dat ve đi
        $getInPointId = $request->getInPointId;
        $getOffPointId = $request->getOffPointId;
        $getInTimePlan = $request->getInTimePlan;
        $getOffTimePlan = $request->getOffTimePlan;
        $tripId = $request->tripId;
        $scheduleId = $request->scheduleId;
        $fullName = empty($request->fullName) ? 'Khách vãng lai' : $request->fullName;
        $phoneNumber = $request->phoneNumber;
        $listSeatId = array_to_json(explode(",", $request->listSeat));
        $numberOfAdults = $request->has('numberOfAdults') ? (int) $request->numberOfAdults : 0;
        $numberOfChildren = $request->has('numberOfChildren') ? (int) $request->numberOfChildren : 0;
        $promotionId = $request->promotionId;
        $promotionCode = $request->promotionCode;
        $startDate = $request->startDate;
        $isMeal = $request->has('isMeal') ? 'true' : 'false';
        $isPickUpHome = $request->has('isPickUpHome') ? 'true' : 'false';
        $needPickUpHome = $request->has('needPickUpHome') ? 'true' : 'false';
        $isInsurrance = $request->has('isInsurrance') ? 'true' : 'false';
        $mealPrice = $request->has('isMeal') ? $request->isMeal : 0;//giá ăn
        $priceInsurrance = $request->has('isInsurrance') ? (int)$request->isInsurrance : 0;//giá bảo hiểm
        $sendSMS = $request->has('sendSMS') ? 'true' : 'false';
        $paymentType = $request->paymentType;//hình thức thanh toán


        $totalPrice = is_null($request->totalPrice)?($mealPrice+$priceInsurrance+$request->originalPrice):$request->totalPrice;
        $paidMoney = is_null($request->paidMoney) ? 0 : $request->paidMoney;
        $timeBookHolder = 0;
        if (!is_null($request->timeBookHolder)) {
            $timeBookHolder = $paidMoney < $totalPrice && $paidMoney != 0 ? 0 : $request->timeBookHolder;
        }
        $pickUpAddress = ($request->goUp == 'pickup' || $request->goUp == 'transshipment') && !is_null($request->pickUpAddress) ? $request->pickUpAddress : "";
        $alongTheWayAddress = $request->goUp == 'alongTheWay' && !is_null($request->pickUpAddress) ? $request->pickUpAddress : "";

        $dropOffAddress = ($request->goDown == 'dropOff' || $request->goDown == 'transshipment') && !is_null($request->dropOffAddress) ? $request->dropOffAddress : "";
        $dropAlongTheWayAddress = $request->goDown == 'alongTheWay' && !is_null($request->dropOffAddress) ? $request->dropOffAddress : "";

//        $pickUpAddress = $request->pickUpAddress;
        $latitudeUp = $request->pickUpLat;
        $longitudeUp = $request->pickUpLong;
//        $dropOffAddress = $request->dropOffAddress;
        $latitudeDown = $request->dropOffLat;
        $longitudeDown = $request->dropOffLong;
        $note = is_null($request->note)?'':$request->note;
        $agencyUserId = null;
        if ($request->agencyUserId) {
            $agencyUserId = $request->agencyUserId;
        } else {
            //nếu không chọn ĐẠI lí thì tài khoản đăng nhập là đại lí sẽ là agencyuserid
            if (count(session("userLogin")["userInfo"]['listAgency']) > 0) {
                $agencyUserId = session("userLogin")["userInfo"]['userId'];
            }
        }

        $agencyPrice = is_null($request->realPrice) ? $totalPrice : $request->realPrice;

//        if($agencyUserId){
//            $numberSeat = $numberOfAdults + $numberOfChildren;
//            $commissionType = $request->commissionType;
//            $commissionValue = $request->commissionValue;
//            $oneSeatPrice = $request->oneSeatPrice;
//            if($commissionType==1){
//                $agencyPrice = $totalPrice-$commissionValue*$numberSeat;
//            }else{
//                $agencyPrice = $totalPrice - $oneSeatPrice*$commissionValue*$numberSeat;
//            }
//        }
        $createdUser = empty($request->createdUser) ? session('userLogin')['userInfo']['userId'] : $request->createdUser;
        $foreignKey = empty($request->foreignKey) ? "" : $request->foreignKey;
        /*
         * giá sale nếu là tiền thì không cần tính, nếu là %(bé hơn 1) thì tính bằng
         * (giá tiền phải trả(không bao gồm giá ăn và giá bảo hiểm))*phần trăm chia cho (1 - phần trăm)
         * */
        $startDate = date('d-m-Y', $request->startDate / 1000);
        $startDate = date_to_milisecond($startDate, 'begin');
        if ($request->submit == 'THANHTOAN') {
            $paidMoney = $agencyPrice;
        }
        $unPaidMoney = $agencyPrice - $paidMoney;
        $param =[
            "timeZone"=>7,
            "tripId"=>$tripId,
            "scheduleId"=>$scheduleId,
            "getInPointId"=>$getInPointId,
            "getOffPointId"=>$getOffPointId,
            'getInTimePlan' => $getInTimePlan,
            'getOffTimePlan' => $getOffTimePlan,
            "numberOfAdults"=>$numberOfAdults,
            "numberOfChildren"=>$numberOfChildren,
            "listSeatId"=>$listSeatId,
            "email"=>is_null($request->email)?'':$request->email,
            "sendSMS"=>$sendSMS,
            "agencyUserId"=>$agencyUserId,
            "paymentTicketPrice"=>$request->paymentTicketPrice,
            "paidMoney"=>$paidMoney,
            "agencyPrice"=>$agencyPrice,
            "note"=>$note,
            "foreignKey"=>$foreignKey,
            "mealPrice"=>$mealPrice,
            'priceInsurrance' => $priceInsurrance,
            'timeBookHolder' => $timeBookHolder,
            'paymentType' => $paymentType,
            'pickUpAddress' => $pickUpAddress,
            'latitudeUp' => $latitudeUp,
            'longitudeUp' => $longitudeUp,
            'dropOffAddress' => $dropOffAddress,
            'latitudeDown' => $latitudeDown,
            'longitudeDown' => $longitudeDown,
            'promotionCode' => $request->promotionCode,
            'alongTheWayAddress' => $alongTheWayAddress,
            'dropAlongTheWayAddress' => $dropAlongTheWayAddress,
            'platform'=>1,
            'extraPrice' => $request->extraPrice,
            'collectMoneyForAgency' => $request->collectMoneyForAgency,
            'inTransshipmentPointId'=> $request->transshipmentInPointId,
            'offTransshipmentPointId'=> $request->transshipmentOffPointId,
            'inTransshipmentPrice' => $request->transshipmentInPointPrice * ($numberOfAdults + $numberOfChildren),
            'offTransshipmentPrice' => $request->transshipmentOffPointPrice * ($numberOfAdults + $numberOfChildren),
        ];
        if(!is_null($phoneNumber)){
            $param["phoneNumber"]=$phoneNumber;
        }
        if(!is_null($fullName)){
            $param["fullName"]=$fullName;
        }
        if(!is_null($request->email)){
            $param["email"]=$request->email;
        }
        if (!is_null($pickUpAddress)) {
            $param = array_merge($param, [
                'needPickUpHome' => 'true'
            ]);
        }
        $url = '/general_ticket/create';
        if($request->ticket_secondary_seat == 'true'){
            $url = 'ticket_secondary_seat/create';
        }
        if ($request->submit == 'ADDSEAT') {
            $paramsAdd=[
                "ticketId"=>$request->ticketId,
                "listSeatId"=>$listSeatId,
            ];
            $result = $this->makeRequestWithJson('/general_ticket/addSeat', $paramsAdd);
            if ($request->ajax()) {
                return response()->json(['start'=>$result]);
            } else {
                if ($result['status'] == 'success') {
                    return redirect()->back()->with(['msg' => "Message('Thành công','Cập nhật thêm ghế thành công','');"]);
                } else {
                    $message_err = defined($result['results']['error']['propertyName']) ? constant($result['results']['error']['propertyName']) : $result['results']['error']['propertyName'];
                    if ($request->ajax()) {
                        return response()->json(['msg' => $message_err]);
                    }
                    return redirect()->back()->withInput()->with(['msg' => MessageJS($message_err)]);
                }
            }
        }
//        $url = $request->submit == 'GIUCHO' ? 'web/ticket-order' : 'web_ticket/sale_ticket';

        if ($request->total_Price_Back > 0) {
            $paymentCode = $request->paymentCode;
            $param = array_merge($param, [
                'paymentCode' => $paymentCode,
            ]);
        }
        $param = $this->deleteNullParam($param);
        $result = $this->makeRequestWithJson($url, $param);
        $result['param'] = $param;
//        dev($result);
        // thực hiện đặt vé khứ hồi
        if ($request->total_Price_Back > 0) {
            //param cho khứ hồi
            $list_Seat = array_to_json(explode(",", $request->list_Seat));
            $total_Price_Back = $request->total_Price_Back;
            $trip_Id = $request->trip_Id;
            $getInPointIdRound = $request->getInPointIdRound;
            $getOffPointIdRound = $request->getOffPointIdRound;
            $getInTimePlanRound = $request->getInTimePlanRound;
            $getOffTimePlanRound = $request->getOffTimePlanRound;
            $scheduleIdRound = $request->scheduleIdRound;
            $fullName = empty($request->fullName) ? 'Khách vãng lai' : $request->fullName;
            $phoneNumber = $request->phoneNumber;
            $numberOfAdults = ($request->adultsRound != null && $request->adultsRound > 0) ? $request->adultsRound : 1;
            $numberOfChildren = 0;
            $promotionId = $request->promotionId;
            $startDateRound = $request->startDateRound;
            $isMeal = $request->has('mealPriceRound') ? 'true' : 'false';
            $isPickUpHome = $request->has('isPickUpHome') ? 'true' : 'false';
            $isInsurrance = $request->has('isInsurrance') ? 'true' : 'false';
            $mealPrice = $request->mealPriceRound?$request->mealPriceRound:0;
            $priceInsurrance = $request->priceInsurranceRound? $request->priceInsurranceRound:0;
            $sendSMS = $request->has('sendSMS') ? 'true' : 'false';
            $paymentType = $request->paymentType;
            $timeBookHolder = $paidMoney < $total_Price_Back && $paidMoney != 0 ? 0 : $request->timeBookHolder;
            $pickUpAddressRound = $request->pickUpAddressRound;
            $latitudeUpRound = $request->pickUpLatRound;
            $longitudeUpRound = $request->pickUpLongRound;
            $dropOffAddressRound = $request->dropOffAddressRound;
            $latitudeDownRound = $request->dropOffLatRound;
            $longitudeDownRound = $request->dropOffLongRound;
            $note = $request->note;
            $agencyUserIdRound = $agencyUserId;
            $createdUser = empty($request->createdUser) ? session('userLogin')['userInfo']['userId'] : $request->createdUser;
            $foreignKey = empty($request->foreignKey) ? "" : $request->foreignKey;

            /*
             * giá sale nếu là tiền thì không cần tính, nếu là %(bé hơn 1) thì tính bằng
             * (giá tiền phải trả(không bao gồm giá ăn và giá bảo hiểm))*phần trăm chia cho (1 - phần trăm)
             * */
            $salePrice = 0;
            if (!is_null($request->salePrice)) {
                $salePrice = $request->salePrice < 1 ? ($total_Price_Back - $priceInsurrance * count($list_Seat) * $request->salePrice) * $request->salePrice : $request->salePrice;
            }
            $paidMoneyRound = $request->paidMoneyRound;
            $startDateRound = date('d-m-Y', $request->startDateRound / 1000);
            $startDateRound = date_to_milisecond($startDateRound, 'begin');
            if ($request->submit != 'GIUCHO') {
                $paidMoneyRound = $request->realPriceRound;
            }

            $agencyPriceRound =$request->realPriceRound;
            $unPaidMoneyRound = $request->realPriceRound-$paidMoneyRound;

            $param_Round_Trip = [
                'timeZone' => 7,
                'getInPointId' => $getInPointIdRound,
                'getOffPointId' => $getOffPointIdRound,
                'getInTimePlan' => $getInTimePlanRound,
                'getOffTimePlan' => $getOffTimePlanRound,
                'tripId' => $trip_Id,
                'scheduleId' => $scheduleIdRound,
                'listSeatId' => $list_Seat,
                'originalTicketPrice' => $total_Price_Back,
                'paymentTicketPrice' => $total_Price_Back,
                'paidMoney' => $paidMoneyRound,
                'promotionCode' => $promotionCode,
                'fullName' => $fullName,
                'phoneNumber' => $phoneNumber,
                'numberOfAdults' => $numberOfAdults,
                'numberOfChildren' => $numberOfChildren,
                'mealPrice' => $mealPrice,
                'priceInsurrance' => $priceInsurrance,
                'startDate' => floor($startDateRound / 86400000) * 86400000,
                'sendSMS' => $sendSMS,
                'paymentType' => $paymentType,
                'timeBookHolder' => $timeBookHolder,
                'pickUpAddress' => $pickUpAddressRound,
                'latitudeUp' => $latitudeUpRound,
                'longitudeUp' => $longitudeUpRound,
                'dropOffAddress' => $dropOffAddressRound,
                'latitudeDown' => $latitudeDownRound,
                'longitudeDown' => $longitudeDownRound,
                'createdUser' => $createdUser,
                'agencyUserId' => $agencyUserIdRound,
                'note' => $note,
                'foreignKey' => $foreignKey,
                'agencyPrice' => $agencyPriceRound,
                'unPaidMoney' => $unPaidMoneyRound,
                'inTransshipmentPrice' => $request->transshipmentInPointPrice * ($numberOfAdults + $numberOfChildren),
                'offTransshipmentPrice' => $request->transshipmentOffPointPrice * ($numberOfAdults + $numberOfChildren),
                'paymentCode' => $request->paymentCode,
            ];

            if (!is_null($pickUpAddress)) {
                $param_Round_Trip = array_merge($param_Round_Trip, [
                    'needPickUpHome' => 'true'
                ]);
            }

            $url = $request->submit == 'GIUCHO' ? 'web/ticket-order' : 'web_ticket/sale_ticket';
            //thực hiện đặt ve khứ hồi
            $result_Round = $this->makeRequest($url, $param_Round_Trip);


        } else {
            $result_Round = [];
        }

        if ($request->ajax()) {
            $rs = [
                'start' => $result,
                'end' => $result_Round
            ];
            return response()->json($rs);
        } else {
            if($result['code']==200){
                if(count($result_Round) > 0){
                    if($result_Round['code']==200){
                        $previousUrl = explode('?', app('url')->previous())[0];
                        return redirect()
                            ->to($previousUrl . '?' . http_build_query([
                                    'ticketId' => $result['results']['ticketId'],
                                    'tripId' => $result['results']['tripId'],
                                    'scheduleId' => $scheduleId,
                                    'routeId' => $request->routeId,
                                    'index' => $request->index,
                                    'date' => date('d-m-Y', $getInTimePlan / 1000),
                                    'startPointId' => $getInPointId,
                                    'endPointId' => $getOffPointId,

                                    'ticketIdRound' => $result_Round['results']['ticketId'],
                                    'trip_Id' => $trip_Id,
                                    'scheduleIdRound' => $scheduleIdRound,
                                    'route_Id' => $request->routeIdRound,
                                    'indexRound' => $request->index,
                                    'dateRound' => date('d-m-Y', $getInTimePlanRound / 1000),
                                    'startPointIdRound' => $getInPointIdRound,
                                    'endPointIdRound' => $getOffPointIdRound,
                                    'action' => $request->has('print') ? 'print' : 'show',
                                    'type' => 'ticket'
                                ]))
                            ->with([
                                    'msg' => MessageJS($result_Round['results']['message'] . '<br>Đặt thành công 2 mã vé: ' . $result_Round['results']['ticketId'] . ' và ' . $result['results']['ticketId']
                                    )]
                            );
                    }else{
                        return redirect()->back()->withInput()->with(['msg' => MessageJS("Đặt vé thành công,vé về thất bại")]);
                    }
                }else{
                    return redirect()->back()->withInput()->with(['msg' => MessageJS("Đặt vé thành công")]);
                }
            }else{
                if(count($result_Round) > 0){
                    if($result_Round['code']==200) {
                        return redirect()->back()->withInput()->with(['msg' => MessageJS("Đặt vé đi thất bại,Đặt vé khứ hồi thành công")]);
                    }else{
                        return redirect()->back()->withInput()->with(['msg' => MessageJS("Đặt vé thất bại")]);
                    }
                }else{
                    return redirect()->back()->withInput()->with(['msg' => MessageJS("Đặt vé thất bại")]);
                }
            }
//            if ($result['status'] == 'success') {
//                if (count($result_Round) > 0) {
//                    if ($result_Round['status'] == 'success') {
//                        $previousUrl = explode('?', app('url')->previous())[0];
//                        if ($request->ajax()) {
//                            if ($request->has('getResult')) {
//                                return response()->json(['msg' => $result_Round['results']['message'], 'status' => $result_Round['status'], 'ticketId' => $result_Round['results']['ticketId']]);
//                            } else {
//                                return response()->json(['msg' => $result_Round['results']['message'] . '<br>Đặt thành công 2 mã vé: ' . $result_Round['results']['ticketId'] . ' và ' . $result['results']['ticketId']]);
//                            }
//                        } else {
//                            return redirect()
//                                ->to($previousUrl . '?' . http_build_query([
//                                        'ticketId' => $result['results']['ticketId'],
//                                        'tripId' => $result['results']['tripId'],
//                                        'scheduleId' => $scheduleId,
//                                        'routeId' => $request->routeId,
//                                        'index' => $request->index,
//                                        'date' => date('d-m-Y', $getInTimePlan / 1000),
//                                        'startPointId' => $getInPointId,
//                                        'endPointId' => $getOffPointId,
//
//
//                                        'ticketIdRound' => $result_Round['results']['ticketId'],
//                                        'trip_Id' => $trip_Id,
//                                        'scheduleIdRound' => $scheduleIdRound,
//                                        'route_Id' => $request->routeIdRound,
//                                        'indexRound' => $request->index,
//                                        'dateRound' => date('d-m-Y', $getInTimePlanRound / 1000),
//                                        'startPointIdRound' => $getInPointIdRound,
//                                        'endPointIdRound' => $getOffPointIdRound,
//                                        'action' => $request->has('print') ? 'print' : 'show',
//                                        'type' => 'ticket'
//                                    ]))
//                                ->with([
//                                        'msg' => MessageJS($result_Round['results']['message'] . '<br>Đặt thành công 2 mã vé: ' . $result_Round['results']['ticketId'] . ' và ' . $result['results']['ticketId']
//                                        )]
//                                );
//                        }
//
//                    } else {
//                        if ($request->ajax()) {
//                            return response()->json(['msg' => "Đặt vé khứ hồi thất bại<br>" . $result_Round['results']['error']['code']]);
//                        }
//                        return redirect()->back()->withInput()->with(['msg' => MessageJS("Đặt vé khứ hồi thất bại")]);
//                    }
//                } else {
//                    $previousUrl = explode('?', app('url')->previous())[0];
//                    if ($request->ajax()) {
//                        if ($request->has('getResult')) {
//                            return response()->json(['msg' => $result['results']['message'], 'status' => $result['status'], 'ticketId' => $result['results']['ticketId']]);
//                        } else {
//                            return response()->json(['msg' => $result['results']['message'] . '<br>Đặt thành công: ' . $result['results']['ticketId']]);
//                        }
//
//                    } else {
//                        return redirect()
//                            ->to($previousUrl . '?' . http_build_query([
//                                    'ticketId' => $result['results']['ticketId'],
//                                    'tripId' => $result['results']['tripId'],
//                                    'scheduleId' => $scheduleId,
//                                    'routeId' => $request->routeId,
//                                    'index' => $request->index,
//                                    'date' => date('d-m-Y', $getInTimePlan / 1000),
//                                    'startPointId' => $getInPointId,
//                                    'endPointId' => $getOffPointId,
//                                    'action' => $request->has('print') ? 'print' : 'show',
//                                    'type' => 'ticket'
//                                ]))
//                            ->with([
//                                    'msg' => MessageJS($result['results']['message'] . '<br>Đặt thành công: ' . $result['results']['ticketId']
//                                    )]
//                            );
//                    }
//                }
//            } else {
//                $message_err = defined($result['results']['error']['propertyName']) ? constant($result['results']['error']['propertyName']) : $result['results']['error']['propertyName'];
//                if ($request->ajax()) {
//                    return response()->json(['msg' => $message_err]);
//                }
//                return redirect()->back()->withInput()->with(['msg' => MessageJS($message_err)]);
//            }
        }
    }

    public function getPriceTicketGoods(Request $request)
    {
        $getInPointId = $request->getInPointId;
        $getOffPointId = $request->getOffPointId;
        $promotionId = $request->promotionId;
        $tripId = $request->tripId;
        $scheduleId = $request->scheduleId;
        $weight = $request->weight;
        $dimension = $request->dimension;

        $responsePrice = $this->makeRequest(generateUrl('web/get-price-ticket-goods', [
            'timeZone' => 7,
            'getInPointId' => $getInPointId,
            'getOffPointId' => $getOffPointId,
            'tripId' => $tripId,
            'scheduleId' => $scheduleId,
            'promotionId' => $promotionId,
            'weight' => $weight,
            'dimension' => $dimension
        ]));
        $resultPrice = $responsePrice['results'];
        if (array_has($resultPrice, 'price')) {
            $resultPrice = $responsePrice['results']['price'];
        }
        return response()->json($resultPrice);

    }

    public function postSellTicketGood(Request $request)
    {
        $getInPointId = $request->getInPointId;
        $getOffPointId = $request->getOffPointId;
        $tripId = $request->tripId;
        $scheduleId = $request->scheduleId;
        $promotionId = $request->promotionId;
        $weight = $request->weight;
        $dimension = empty($request->dimension) ? 0 : $request->dimension;

        /*$sendSMS = $request->has('sendSMS') ? 'true' : 'false';*/

        //lấy giá

        $responsePrice = $this->makeRequest(generateUrl('web/get-price-ticket-goods', [
            'timeZone' => 7,
            'getInPointId' => $getInPointId,
            'getOffPointId' => $getOffPointId,
            'tripId' => $tripId,
            'scheduleId' => $scheduleId,
            'promotionId' => $promotionId,
            'weight' => $weight,
            'dimension' => $dimension
        ]));
        $resultPrice = $responsePrice['results']['price'];
        $paidMoney = $request->has('paidMoney') ? '0' : $resultPrice['pricePayment'];

        $startDate = date('d-m-Y', $request->startDate / 1000);
        $startDate = date_to_milisecond($startDate, 'begin');

        //thực hiện đặt ve
        $result = $this->makeRequest('web_ticket/sale_ticket_goods', [
            'timeZone' => 7,
            'getInPointId' => $getInPointId,
            'getOffPointId' => $getOffPointId,
            'getInTimePlan' => $request->getInTimePlan,
            'getOffTimePlan' => $request->getOffTimePlan,
            'tripId' => $tripId,
            'scheduleId' => $scheduleId,
            'startDate' => $startDate,
            'promotionId' => $promotionId,
            'fullName' => empty($request->fullName) ? 'Khách vãng lai' : $request->fullName,
            'phoneNumber' => $request->phoneNumber,
            'fullNameReceiver' => $request->fullNameReceiver,
            'phoneNumberReceiver' => $request->phoneNumberReceiver,
            'paymentTicketPrice' => $resultPrice['pricePayment'],
            'ticketName' => $request->ticketName,
            'weight' => $weight,
            'dimension' => $dimension,
            'paidMoney' => $paidMoney,
            'paymentType' => $request->paymentType,
            'listImages' => array_to_json($request->listImages)
        ]);


        if ($result['status'] == 'success') {
            $previousUrl = explode('?', app('url')->previous())[0];
            return redirect()
                ->to($previousUrl . '?' . http_build_query([
                        'ticketId' => $result['results']['ticketId'],
                        'tripId' => $tripId,
                        'routeId' => $request->routeId,
                        'date' => date('d-m-Y', $request->getOffTimePlan / 1000),
                        'startPointId' => $getInPointId,
                        'endPointId' => $getOffPointId,
                        'action' => $request->has('print') ? 'print' : 'show',
                        'type' => 'goods'
                    ]))
                ->with([
                    'msg' => MessageJS($result['results']['message'] . '<br>Mã vé: ' . $result['results']['ticketId'])
                ]);
        } else {
            return redirect()
                ->back()
                ->withInput()
                ->with([
                    'msg' => MessageJS("Đặt vé gửi đồ thất bại <br>" . checkMessage($result['results']['error']['propertyName']))
                ]);
        }
    }

    public function orderOnline(Request $request)
    {
        $page = $request->has('page') ? $request->page : 1;
        $startDate = $request->has('startDate') ? date_to_milisecond($request->startDate, 'begin') : date_to_milisecond(date('d-m-Y'), 'begin');
        $endDate = $request->has('endDate') ? date_to_milisecond($request->endDate, 'end') : date_to_milisecond(date('d-m-Y'), 'end');
        $params = [
            'timeZone' => 7,
            'page' => $page - 1,
            'count' => 100,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'paymentType' => '1',
            'getByCreatedDate' => "true"
        ];
        if (!is_null($request->ticketCode)) {
            $params = array_merge($params, [
                'ticketCode' => $request->ticketCode
            ]);
        }
        if (!is_null($request->phoneNumber)) {
            $params = array_merge($params, [
                'phoneNumber' => $request->phoneNumber
            ]);
        }
        if (!is_null($request->transCode)) {
            $params = array_merge($params, [
                'transCode' => $request->transCode,
            ]);
        }
        $result = $this->makeRequestWithJson('report/onlinePayment', $params);
        return view('cpanel.Report.orderOnline')->with([
            'result' =>$result['results']['listOnlinePaymentReport'],
            'page' => $page
        ]);
    }

    public function getListTicket(Request $request)
    {
        $startDate = $request->has('startDate') ? date_to_milisecond($request->startDate) : 0;
        $endDate = $request->has('endDate') ? date_to_milisecond($request->endDate) : time() * 1000;
        $result = $this->makeRequest('web_ticket/get_list_ticket_for_company', [
            'timeZone' => 7,
            'page' => 0,
            'count' => 100,
            'startDate' => $startDate,
            'endDate' => $endDate
        ]);

        return response()->json($result, 200, [], JSON_PRETTY_PRINT);
    }

    public function updateStatusTicket(Request $request)
    {
//        $option = $request->totalPrice > $request->paidMoney ? 3 : 1 ;
//        $option = $request->option == 2?2:$option;
        $option = $request->option;
        $pickUpAddress = ($request->goUp == 'pickup' || $request->goUp == 'transshipment') && !is_null($request->pickUpAddress) ? $request->pickUpAddress : "";
        $alongTheWayAddress = $request->goUp == 'alongTheWay' && !is_null($request->pickUpAddress) ? $request->pickUpAddress : "";

        $dropOffAddress = ($request->goDown == 'dropOff' || $request->goDown == 'transshipment') && !is_null($request->dropOffAddress) ? $request->dropOffAddress : "";
        $dropAlongTheWayAddress = $request->goDown == 'alongTheWay' && !is_null($request->dropOffAddress) ? $request->dropOffAddress : "";
        $latitudeUp = $request->pickUpLat;
        $longitudeUp = $request->pickUpLong;
        $latitudeDown = $request->dropOffLat;
        $longitudeDown = $request->dropOffLong;
        $foreignKey = empty($request->foreignKey) ? "" : $request->foreignKey;
        $totalPrice = $request->totalPrice;
        if ($pickUpAddress == null) $pickUpAddress = '';
        if ($dropOffAddress == null) $dropOffAddress = '';
        $unpaidMoney = $request->realPrice - $request->paidMoney;
        $agencyUserId = null;
        if (!is_null($request->agencyUserId)&&$request->agencyUserId!='') {
            $agencyUserId = $request->agencyUserId;
        } else {
            //nếu không chọn ĐẠI lí thì tài khoản đăng nhập là đại lí sẽ là agencyuserid
            if (count(session("userLogin")["userInfo"]['listAgency']) > 0) {
                $agencyUserId = session("userLogin")["userInfo"]['userId'];
            }
        }


        $params = [
            'ticketId' => $request->ticketId,
            'phoneNumber' => $request->phoneNumber,
            'fullName' => $request->fullName,
            'email' => $request->email,
            'priceInsurrance' => $request->priceInsurrance,
            'promotionCode' => $request->promotionCode,
            'agencyUserId' => $agencyUserId,
            'pickUpAddress' => $pickUpAddress,
            'latitudeUp' => $latitudeUp,
            'longitudeUp' => $longitudeUp,
            'dropOffAddress' => $dropOffAddress,
            'latitudeDown' => $latitudeDown,
            'longitudeDown' => $longitudeDown,
            'totalPrice' => $totalPrice,
            'foreignKey' => $foreignKey,
            'alongTheWayAddress' => $alongTheWayAddress,
            'dropAlongTheWayAddress' => $dropAlongTheWayAddress,
            'unPaidMoney' => $unpaidMoney,
            "extraPrice"=>$request->extraPrice,
            "collectMoneyForAgency"=>$request->collectMoneyForAgency,
            'inTransshipmentPointId'=>$request->transshipmentInPointId,
            'offTransshipmentPointId'=>$request->transshipmentOffPointId,
            'inTransshipmentPrice' => $request->transshipmentInPointPrice * (count(explode(",", $request->listExtraPrice))),
            'offTransshipmentPrice' => $request->transshipmentOffPointPrice * (count(explode(",", $request->listExtraPrice))),
            'platform'=>1
        ];
        if($request->has('paidMoney')&&$request->paidMoney!=''){
            $params['paidMoney'] = $request->paidMoney;
        }
        if($request->has('realPrice')&&$request->realPrice!=''){
            $params['agencyPrice'] = $request->realPrice;
        }
        if($request->has('paymentTicketPrice')&&$request->paymentTicketPrice!=''){
            $params['paymentTicketPrice'] = $request->paymentTicketPrice;
        }
        if ($request->listSeat != '') {
            $params['listSeatId'] = array_to_json(explode(",", $request->listSeat));
        }
        if ($request->has('sendSMS')&&$request->sendSMS=="true") {
            $params['sendSMS'] = "true";
        }
        if ($request->cancelReason) {
            $params['cancelReason'] = $request->cancelReason;
        }
        if (!empty($request->promotionCode)) {
            $params['paymentTicketPriceAfterPromotion'] = $request->totalPrice + $request->transshipmentInPointPrice + $request->transshipmentOffPointPrice;
        }
        if (!is_null($pickUpAddress)) {
            $params = array_merge($params, [
                'needPickUpHome' => 'true'
            ]);
        }
        if (!is_null($dropOffAddress)) {
            $params = array_merge($params, [
                'needDropOffHome' => 'true'
            ]);
        }
        $params = $this->deleteNullParam($params);
        $params['note']=$request->note;
        if(is_null($request->note)){
            $params['note']='';
        }
        if($option==2){
            $paramCancel = [
                "ticketId"=>$request->ticketId
            ];
            if ($request->listSeat != '') {
                $paramCancel['listSeatId'] = array_to_json(explode(",", $request->listSeat));
            }
            if ($request->cancelReason) {
                $paramCancel['cancelReason'] = $request->cancelReason;
            }
            $result = $this->makeRequestWithJson('/general_ticket/cancel', $paramCancel);
        }else{
            if($option==1){
                $params['isPayment'] = "true";
                $result = $this->makeRequestWithJson('/general_ticket/updateInfo', $params);
            }else{
                $result = $this->makeRequestWithJson('/general_ticket/updateInfo', $params);
            }
        }
        $result['parrams']=$params;

//        dev($result);
        if ($request->ajax()) {
            return response()->json($result);
        } else {
            if ($result['status'] == 'success') {
                $message = '<br>Mã vé: ' . $result['results']['ticket']['ticketId'];
                return redirect()->back()->withInput()->with(['msg' => "Message('Thành công','" . $message . "','');"]);
            } else {
                $message = '';
                if ($request->option == 1)
                    $message = "Message('Thất bại','Thanh toán vé thất bại<br>" . checkMessage($result['results']['error']['propertyName']) . "','');";
                if ($request->option == 2)
                    $message = "Message('Thất bại','Huỷ vé thất bại<br>" . checkMessage($result['results']['error']['propertyName']) . "','');";
                if ($request->option == 3)
                    $message = "Message('Thất bại','Cập nhật thông tin vé thất bại<br>" . checkMessage($result['results']['error']['propertyName']) . "','');";
                if ($request->option == 4)
                    $message = "Message('Thất bại','Trả đồ thất bại<br>" . checkMessage($result['results']['error']['propertyName']) . "','');";
                return redirect()->back()->withInput()->with(['msg' => $message]);
            }
        }
    }

    function updateCheckTicket(Request $request)
    {
        $check = $request->isChecked;
        $tripId = $request->tripId;
        $ticketId = $request->ticketId;
        $response = $this->makeRequestWithJson('/general_ticket/updateInfo', ['isChecked' => $check, 'tripId' => $tripId, 'timeZone' => 7, 'ticketId' => $ticketId, 'option' => 3]);
        if ($request->ajax()) {
            return response()->json($response);
        } else {
            return redirect()->back();
        }
    }

    private function removeNullParam($param)
    {
        foreach ($param as $key => $value) {
            if (is_null($value)) {
                $param[$key] = '';
            }
        }
        return $param;
    }

    private function deleteNullParam($param){
        foreach ($param as $key => $value) {
            if (is_null($value)) {
                unset($param[$key]);
            }
        }
        return $param;
    }

    public function printTicket(Request $request)
    {
        $result = $this->makeRequest('web_ticket/view', [
            //'ticketCode' => $ticketId,
            'ticketId' => $request->ticketId
        ]);
        $generator = new BarcodeGeneratorSVG();
        $Barcode = $generator->getBarcode(array_get($result['results'], 'ticket.ticketCode'), $generator::TYPE_CODE_128, 1.2);
        $listSettingPrintTicket = DB::table('configprintticket')
            ->where('configprintticket.companyId', '=', session('companyId'))
            ->join('attributeprintticket', 'idAttribute', '=', 'id')
            ->orderBy('id', 'asc')
            ->get();
        $listSettingPrintTicket=json_decode(json_encode($listSettingPrintTicket),true);
        return [
            'result' => array_get($result['results'], 'ticket'),
            'listSettingPrintTicket'=>$listSettingPrintTicket,
            'barcode' => $Barcode];
    }

    public function getTicketInfo(Request $request)
    {
        $response = $this->makeRequest('web_ticket/view', [
            'ticketId' => $request->ticketId,
        ]);
        $result = array_get($response, 'results', head($response['results']));
        return response()->json($result, 200, [], JSON_PRETTY_PRINT);
    }

    /*
    * Tạo cuộc gọi đi
    */
    public function makeCall(Request $request)
    {
        $token = $this->makeRequest('https://me.taduphone.vn/api/v1/auth/token', [
            'apiId' => session('apiId'), //API Id
            'apiKey' => session("telecomAPIKey"), //API Key
        ], [], 'GET', false);

        $response = $this->makeRequest('https://me.taduphone.vn/api/v1/phone/call', [
            'callNumber' => session('telecomNumber'),//So tong dai
            'receiveNumber' => $request->phoneNumber, //So goi di
        ], [
            'Authorization' => $token['data']
        ], 'POST', false);
        return $response;
    }

    /*
     * Lấy lịch sử vé
     * */
    public function getTicketHistory(Request $request)
    {
        $response = $this->makeRequest('web_ticket/logs', [
            'ticketId' => $request->ticketId,
        ]);
        $result = array_get($response, 'listTicketLog', head($response['results']));
        return response()->json($result, 200, [], JSON_PRETTY_PRINT);
    }

    public function addtransshipment(Request $request)
    {
        $list = $request->list;
        foreach ($list as $key => $value)
            foreach ($value as $k => $v) {
                if ($v == NULL) $list[$key][$k] = "";
            }
        $result = $this->makeRequestWithJson('/ticket/add_transshipment_driver', ['transshipmentDriver' => $list]);

    }

    public function getHistoryCall(Request $request){
        $sml = $request->cookie('somayle');
        if($request->has('SoMayLe')){
            Cookie::queue('somayle', $request->SoMayLe, 42000);
            $sml = $request->SoMayLe;
        }
        $soMayLe = '';
        if(isset(session('userLogin')['telecomNumber'])){
            foreach (session('userLogin')['telecomNumber'] as $tel){
                if($sml!=null&&$sml==$tel['number']){
                    $soMayLe = $tel;
                    break;
                }
            }
        }
        if($soMayLe!==''){
            $telecomConfig = $soMayLe["telecomConfig"];
            if(gettype($telecomConfig)==="string"){
                $telecomConfig = json_decode($telecomConfig);
            }
            $param = $request->all();
            $param = http_build_query($param);
            $res = [];
            if($soMayLe['telecomCompanyId']== "TC06I2JslMujIwm"){//tri anh
                $res = file_get_contents((string)$telecomConfig->urlHttpService."webservice/getcallslog?".$param);
            }
            if($soMayLe['telecomCompanyId']== "TC06I2JslovcpeG"){//ntt
//                $res = file_get_contents((string)$telecomConfig->urlReport."?".$param);
                $res = $this->makeRequest($telecomConfig->urlReport,$param,'','GET',false);
            }

            return response($res);
        }
        return response([]);
    }

    public function payVnp($ticketId){
        $banks = $this->makeRequestWithJson('banks',null,null,"GET");
        return view('cpanel.Ticket.sell.module.payVnp')->with([
            'ticketId' => $ticketId,
            'banks' => $banks['results']['data'],
        ]);
    }
}
