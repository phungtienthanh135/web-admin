<?php

namespace App\Http\Controllers;

use Auth;
use http\Env\Response;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index()
    {
        return view('cpanel.Vue.LoginEmployee');
    }

    public function logout(Request $rq)
    {
        if(isset($rq->check)){
            $ck = $this->makeRequestWithJson('display-settings/get',['type'=>'PRINT_GOODS_TEM']);
            if($ck['status'] == 'success'){
                return redirect()->back();
            }
        }
//        session()->forget('userLogin');
        $res = $this->makeRequestWithJson('user/logout',[
            'platform' => 1
        ]);
        session()->flush();
        return redirect('/');
    }

    public function CheckLogin(Request $request)
    {
        $uid = $request->uid;
        $pass = $request->pass;
        $accessCode = $request->accessCode;
//        $deviceId = $request->deviceToken;

//        if($request->get('g-recaptcha-response')==''){
//            return redirect()->back()->with(['msg' => 'Captcha không hợp lệ']);
//        }

        $res = $this->makeRequest('web_admin/rlogin', [
                'userName' => $uid,
                'password' => $pass,
//                'deviceId' => $deviceId,
                'deviceType' => DEVICE_TYPE,
                'deviceId' => $request->deviceId,
                'accessCode' => $accessCode,
            ]
        );
        $denied = [
            NEW_USER,
            LOCKED,
            NEED_CENSORED,
            DELETED
        ];
//        dev($res);
        if ($res['status'] == 'success') {
            if (in_array($res['results']['userInfo']['userStatus'], $denied)) {
                switch ($res['results']['userInfo']['userStatus']) {
                    case NEW_USER:
                        $msg = 'Tài khoản chưa được kích hoạt! Hãy liên hệ tổng đài để được hỗ trợ.';
                        break;
                    case LOCKED:
                        $msg = 'Tài khoản đã bị khóa! Hãy liên hệ tổng đài để được hỗ trợ.';
                        break;
                    case NEED_CENSORED:
                        $msg = 'Tài khoản cần được kiểm duyệt! Hãy liên hệ tổng đài để được hỗ trợ.';
                        break;
                    case DELETED:
                        $msg = 'Tài khoản đã bị xóa! Hãy liên hệ tổng đài để được hỗ trợ.';
                        break;
                    default:
                        $msg = 'Có lỗi xảy ra!';
                        break;
                }
                $res['message'] = $msg;
                if($request->ajax()){
                    return $res;
                }
                return redirect()->back()->withInput()->with(['msg' => $msg]);
            }else
            {
                session()->put('userLogin', $res['results']);
                session()->put('companyId', $res['results']['userInfo']['companyId']);
                session()->put('isEditTicketPrice',$res['results']['isEditTicketPrice']);

                session()->put('apiId', $res['results']['apiId']);
                session()->put('telecomAPIKey', $res['results']['telecomAPIKey']);
                session()->put('companyconfigurations', $res['results']['companyconfigurations']);
                session()->put('pastDaysToSellTicket', isset($res['results']['pastDaysToSellTicket'])?$res['results']['pastDaysToSellTicket']:-1);

                if(!empty($res['results']['telecomNumber']))
                {
                    session()->put('telecomNumber', $res['results']['telecomNumber'][0]['number']);
                }

                $result = $this->makeRequest('web_company/view', [
                    'companyId' => session('companyId')
                ], ['DOBODY6969' => $this->TOKEN_SUPPER]);
                session()->put('companyInfo', array_get($result['results'], 'company', []));
                if($request->ajax()){
                    return $res;
                }
                return redirect('cpanel/vue/home');
//                if(strpos($_SERVER['HTTP_USER_AGENT'], 'Android') || strpos($_SERVER['HTTP_USER_AGENT'], 'Mac OS')) {
//                    if(hasAnyRole(SELL_TICKET)) {
//                        return redirect()->action('TicketController@sell');
//                    }
//                    return redirect()->route('cpanel.home');
//                } else {
//                    return redirect()->route('cpanel.home');
//                }
            }

        } else {
            if($request->ajax()){
                return $res;
            }
            if(isset($res['results']['error']['code'])&&$res['results']['error']['code']=='191'){
                return redirect()->back()->withInput()->with(['msg' => 'Bạn đang đăng nhập ở một máy tính khác, vui lòng gửi yêu cầu để được Đăng nhập!', 'accept' => 1]);
            }
            if(isset($res['results']['error']['code'])&&$res['results']['error']['code']=='192'){
                return redirect()->back()->withInput()->with(['msg' => 'Trạng thái đăng nhập của bạn: Đang chờ duyệt Vui lòng liên hệ với quản lý nếu có thắc mắc', 'accept' => 2]);
            }
            if(isset($res['results']['error']['code'])&&$res['results']['error']['code']=='193'){
                return redirect()->back()->withInput()->with(['msg' => 'Trạng thái đăng nhập của bạn: Đã bị từ chối Vui lòng liên hệ với quản lý nếu có thắc mắc.', 'accept' => 3]);
            }

            $msg = 'Đăng nhập thất bại. Vui lòng thử lại';
            if(isset($res['results']['error']['code'])&&$res['results']['error']['code']=='122'){
                $msg = "Tài khoản hoặc mật khẩu không đúng";
            }
            if(isset($res['results']['error']['code'])&&$res['results']['error']['code']=='141'){
                $msg = "Nhà xe bị khóa";
            }
            return redirect()->back()->withInput()->with(['msg' => $msg]);
        }
    }

    public function ForgotPassword($buoc, Request $request)
    {
        switch ($buoc) {
            case '0':
                {
                    return view('cpanel.login.ForgotPassword');
                }
            case '1':
                {
                    $uid = $request->uid;
                    $res = $this->makeRequestWithJson('user/sendverify', [
                            'userName' => $uid
                        ]
                    );

                    if($request->ajax()){
                        return response()->json($res);
                    }

                    if ($res['status'] == 'success') {
                        return view('cpanel.login.SendCodeResetPassword', [
                            'username' => $uid,
                            'msg' => ''
                        ]);
                    } elseif ($res['results']['error']['propertyName'] === 'USER_NAME_INVALID') {
                        $msg = 'Tài khoản không tồn tại';
                        return redirect()->back()->withInput()->with(['msg' => $msg]);
                    } else {
                        $msg = 'Tạm thời không gửi được tin nhắn. Vui lòng chờ 5 phút';
                        return redirect()->back()->withInput()->with(['msg' => $msg]);
                    }
                }
            case '2':
                {
                    $otpCode = $request->otpCode;
                    $userName = $request->userName;
                    $newPassword = $request->newPassword;

                    $msg = '';

                    $param=[
                        'phoneNumber'=>$request->phoneNumber,
                        'userName' => $userName,
                        'otpCode' => $otpCode,
                        'newPassword' => $newPassword,
                        'stateCode' => '84',
                    ];

                    $res = $this->makeRequestWithJson('user/forgetpassword',$param);
                    $res['param']=$param;

                    if($request->ajax()){
                        return response()->json($res);
                    }
                    if ($res['status'] == 'success') {
                        $msg = 'Đổi mật khẩu thành công';
                        return redirect()->route('cpanel.login.index')->withInput()->with(['msg' => $msg]);
                    } elseif ($res['results']['error']['propertyName'] === 'USER_OTP_CODE_NOT_MATCH') {
                        $msg = 'Mã xác thực không đúng';
                        return view('cpanel.login.SendCodeResetPassword', [
                            'username' => $userName,
                            'msg' => $msg
                        ]);
                    } else {
                        $msg = 'Đổi mật khẩu thất bại';
                        return view('cpanel.login.SendCodeResetPassword', [
                            'username' => $userName,
                            'msg' => $msg
                        ]);
                    }
                }
            default:
                {
                    return view('cpanel.login.ForgotPassword');
                }
        }

    }

    public function ForgotPasswordDefaul()
    {
        return view('cpanel.login.ForgotPassword');
    }
    public function agencyLogin()
    {
        return view('cpanel.Vue.LoginAgency');
    }


}
