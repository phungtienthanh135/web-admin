<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ShipNewController extends Controller
{
    /*
     * ds hàng hóa đã phân loại theo trạng thái, key tương ứng với trạng thái:
     * vd: $listGoodsClassified[0]:ds hàng hóa đã hủy
     *     $listGoodsClassified[1]:ds hàng hóa đã nhận
     */
    public $listGoodsClassified = array([], [], [], [], [], []);

    public function show(Request $request)
    {
        $array_idFillter = [2, 3, 4, 5, 8, 11, 12, 13, 14, 15, 17, 18, 19, 20];  // defined in show.blade
        $today = (new \DateTime())->setTime(0, 0);
        $sendDate = $request->has('sendDate') ? date_to_milisecond($request->sendDate) : ((int)((time() * 1000) / (24 * 3600000)) * (24 * 3600000) - 7 * 3600000);
        $toDate = $request->has('toDate') ? date_to_milisecond($request->toDate) : 0;
        $goodsCode = $request->goodsCode;
        $senderPhone = $request->senderPhone;
        $receiverPhone = $request->receiverPhone;
        $isIncludeCancel = (bool)$request->has('cancel');
        $params = [
            'timeZone' => 7,
            'page' => 0,
            'count' => 1000,
            'companyId' => session('companyId'),
            'goodsCode' => $goodsCode,
            'senderPhone' => $senderPhone,
            'receiverPhone' => $receiverPhone,
            'sendDate' => $sendDate,
            'cancel' => $isIncludeCancel,
        ];
        if ($toDate && $toDate > $sendDate) $params['toDate'] = $toDate + 24 * 3600000;
        if ($request->routeId) $params['routeId'] = $request->routeId;
        $listGoods = $this->makeRequestWithJson('goods/list', array_filter($params));
//        dev($listGoods["results"]['Goods']);
        $listTypeGoods = $this->makeRequestWithJson('goods_type/list',
            ['page' => 0,
                'count' => 1000,
                'companyId' => session('companyId')
            ]
        );
        $listRoute = $this->makeRequest('web_route/getlist',
            ['page' => 0, 'count' => 10,]
        );
        $listPoint = $this->makeRequest('web_point/getlist', [
            'count' => 1000
        ]);
        $listSetting = DB::table('config_table_ship')
            ->where('config_table_ship.companyId', '=', session('companyId'))
            ->join('attribute_table_ship', 'idAttribute', '=', 'id')
            ->orderBy('id', 'asc')
            ->get();
        $listSetting=json_decode(json_encode($listSetting),true);
        if( count($listSetting)==0) {
            $listSetting = DB::table('config_table_ship')
                ->where('config_table_ship.companyId', '=', 'DEFAULT_COMPANY')
                ->join('attribute_table_ship', 'idAttribute', '=', 'id')
                ->orderBy('id', 'asc')
                ->get();
            $listSetting = json_decode(json_encode($listSetting), true);
        }
        return view('cpanel.Ship_new.show')->with([
            'listGoods' => isset($listGoods["results"]['Goods']) ? $listGoods["results"]['Goods'] : [],
            'listTypeGoods' => isset($listTypeGoods["results"]['list']) ? $listTypeGoods["results"]['list'] : [],
            'listGoodsClassified' => isset($listGoods["results"]['Goods']) ? $listGoods["results"]['Goods'] : [],
            'listRoute' => head($listRoute['results']),
            'listPoint' => $listPoint['results']['result'],
            'listSetting'=>$listSetting,
            'array_idFillter'=>$array_idFillter
        ]);
    }

    function classifyGoods(array $listGoods)
    {
        foreach ($listGoods as $goods) {
            $status = $goods['goodsStatus'] ? $goods['goodsStatus'] : 0;
            $this->listGoodsClassified[$status][] = $goods;
        }
        //dd($this->listGoodsClassified);
        return $this->listGoodsClassified;
    }

    public function createGoods(Request $request)
    {
//        dev($request->all());
//        $sendDate = $request->sendDate ? date_to_milisecond($request->sendDate) : time() * 1000;
        $thousand=$request->has('thousand') ? $request->thousand[0]:1;
        foreach ($request->goodsName as $key => $goods) {
            $goodsValueKey=$request->goodsValue[$key]?$request->goodsValue[$key]:0;
            $goodsValueKey=preg_replace('/[^0-9]/', '', $goodsValueKey);
            $params = [
                'goodsName' => $request->goodsName[$key] ? $request->goodsName[$key] : '',
                'goodsValue'=>$goodsValueKey,
                'imageURL' => $request->listImage[$key] ? $request->listImage[$key] : '',
                'quantity' => $request->quantity[$key] ? $request->quantity[$key] : 1,
                'dropOffPoint' => $request->dropOffPoint[$key] ? $request->dropOffPoint[$key] : '',
                'receiver' => $request->receiver[$key] ? $request->receiver[$key] : '',
                'receiverImageURL' => $request->listUserImage[$key] ? $request->listUserImage[$key] : '',
                'dropPointId' => $request->dropOffSavePoint[$key] ? $request->dropOffSavePoint[$key] : '',
                'receiverPhone' => $request->receiverPhone[$key] ? $request->receiverPhone[$key] : '',
                'pickUpPoint' => $request->pickUpPoint[$key] ? $request->pickUpPoint[$key] : '',
                'sender' => $request->sender[$key] ? $request->sender[$key] : '',
                'pickPointId' => $request->pickUpSavePoint[$key] ? $request->pickUpSavePoint[$key] : '',
                'senderPhone' => $request->senderPhone[$key] ? $request->senderPhone[$key] : '',
                'price' => $request->price[$key] ? $request->price[$key] * $thousand : 0,
                'extraPrice' => $request->extraPrice[$key] ? $request->extraPrice[$key] *$thousand : 0,
                'extraPriceNote' => $request->extraPriceNote[$key] ? $request->extraPriceNote[$key] : '',
                'totalPrice' => $request->totalPrice[$key] ? $request->totalPrice[$key] *$thousand : 0,
                'paid' => $request->paid[$key] ? $request->paid[$key] *$thousand: 0,
                'unpaid' => $request->unpaid[$key] ? $request->unpaid[$key] *$thousand : 0,
                'notice' => $request->notice[$key] ? $request->notice[$key] : '',
                'sendDate' => $request->sendDate[$key] ? date_to_milisecond($request->sendDate[$key]) : time() * 1000,
//                'sendDate' => $sendDate,
                'goodsStatus' => 1,
                'sendSMS' => true,
                'timeZone' => 7,
                'dropMidway' => $request->dropMidway[$key] == 1 ? true : false
            ];
            if ($request->goodsCode[$key]) {
                $params['goodsCode'] = $request->goodsCode[$key];
            }
            if ($request->goodsTypeId[$key]) {
                $params['goodsTypeId'] = $request->goodsTypeId[$key];
            }
            $resultCreate = $this->makeRequestWithJson('goods/create', $params);
        }
        $resultCreate['params']=$params;
//        dev($resultCreate);
        if ($resultCreate['code'] == 200) {
            session()->put('print', $resultCreate['results']["goods"]);//dev(session('result'));
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Thêm thành công "), 'result' => $resultCreate['results']]);
        } else {
            session()->forget('print');
//            dev(session('result'));
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Thêm thất bại"), 'result' => $resultCreate['results']]);
        }
    }

    public function getNameFromPhone(Request $request)
    {
        $params = [
            'companyId' => session('companyId'),
            'count' => 1
        ];
        if ($request->receiverPhone) {
            $params['receiverPhone'] = $request->receiverPhone;
        }
        if ($request->senderPhone) {
            $params['senderPhone'] = $request->senderPhone;
        }

        $listGoods = $this->makeRequestWithJson('goods/list', array_filter($params));
        return (isset($listGoods["results"]['Goods']) ? $listGoods["results"]['Goods'] : []);
    }

    public function updateGoods(Request $request)
    {
        $params = [
            'goodsId' => $request->codeEdit,
            'companyId' => session('companyId'),
//            'goodsCode' => $request->goodsCode,
            'goodsTypeId' => $request->goodType,
            'goodsName' => $request->editName,
            'goodsValue' => $request->goodsValue?$request->goodsValue:0,
            'imageURL' => $request->listImage,
            'dropOffPoint' => $request->editPlaceReceive,
            'receiver' => $request->editReceive,
            'receiverPhone' => $request->editPhoneReceive,
            'pickUpPoint' => $request->editPlaceSend,
            'sender' => $request->editSender,
            'senderPhone' => $request->editPhoneSend,
            'quantity' => $request->number,
            'price' => $request->editPrice ? $request->editPrice : 0,
            'extraPrice' => $request->editExtraPeice ? $request->editExtraPeice : 0,
            'extraPriceNote' => $request->editNoteExtra,
            'totalPrice' => $request->editTotal ? $request->editTotal : 0,
            'paid' => $request->editPaid ? $request->editPaid : 0,
            'unpaid' => $request->editUnPaid ? $request->editUnPaid : 0,
            'notice' => $request->editNote ? $request->editNote : '',
        ];
        if ($request->editDate) $params['sendDate'] = date_to_milisecond($request->editDate);
        if ($request->editSavePick != 'Chọn bến') $params['pickPointId'] = $request->editSavePick;
        if ($request->editSaveDrop != 'Chọn bến') $params['dropPointId'] = $request->editSaveDrop;
        if ($request->senDate != 0 && $request->senDate !=null ) $params['sendDate'] = $request->sendDate;
        if (!$request->editTotal) $params = [
            'receiverImageURL' => $request->receiverImageURL,
            'goodsId' => $request->codeEdit,
            'companyId' => session('companyId'),
        ];
        $params = array_filter($params, 'strlen');
        $params['dropMidway'] = $request->editDropMidway == 'on' ? true : false;
        $resultCreate = $this->makeRequestWithJson('goods/update', $params);
        if ($resultCreate['code'] == 200)
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Sửa thành công ")]);
        else
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Sửa thất bại")]);
    }

    public function createPackage(Request $request)
    {
        $goodsIdList = [];
        if (is_array($request->goodsIdList)) {
            $goodsIdList = $request->goodsIdList;
        } elseif (is_string($request->goodsIdList)) {
            $goodsIdList = json_decode($request->goodsIdList, true);
        }
        $params = [
            'goodsIdList' => $goodsIdList,
            'timeZone' => 7,
            'sendDate' => round(microtime(true) * 1000)
        ];
        if ($request->goodsPackageName && $request->goodsPackageName != '') $params['goodsPackageName'] = $request->goodsPackageName;
//        if ($request->goodsPackageCode) $params['goodsPackageCode'] = $request->goodsPackageCode;
        if ($request->scheduleId)
            $params['scheduleId'] = $request->scheduleId;
        if ($request->tripId) {
            $params['tripId'] = $request->tripId;
            $params['timeCreate'] = $request->timeCreate ? date_to_milisecond($request->timeCreate) : (time() * 1000);
        }
        //dev($params);
        $resultCreate = $this->makeRequestWithJson('goods/create-package', $params);
        $resultCreate['params'] = $params;
//        dev($resultCreate);
        if ($resultCreate['code'] == 200)
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Gom thành công")]);
        else
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Gom thất bại"), 'result' => $resultCreate['results']['error']['code']]);
    }

    public function shipPackage(Request $request)
    {
        $params = [];
        if ($request->listGood) {
            $params['goodsCode'] = $request->listGood;
        }
        if ($request->listPackageGood != null) {
            $params['goodsPackageCode'] = $request->listPackageGood;
        }
        if ($request->scheduleId) {
            $params['scheduleId'] = $request->scheduleId;
        }
        $params['sendDate'] = $request->timeCreate ? date_to_milisecond($request->timeCreate) : time() * 1000;
        if (!$request->tripId) {
            if ($request->routeId) {
                $params['routeId'] = $request->routeId;
            }
        }
        if ($request->tripId == -1 || $request->notSendTripExist || $request->sendTripExist) $params['tripId'] = $request->tripId;
        //dev($params);
        $params['timeCreate'] = date_to_milisecond($request->timeCreate);
        $params['goodsStatus'] = 3;
        $resultCreate = $this->makeRequestWithJson('goods/package-arrive', $params);
        if ($resultCreate['code'] == 200)
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Đưa hàng lên xe thành công")]);
        else
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Đưa hàng lên xe thất bại")]);
    }

    public function arrivePackage(Request $request)
    {
        $params = [];
        if ($request->goodsCodeXuongxe) {
            $params['goodsCode'] = $request->goodsCodeXuongxe;
        }
        if ($request->goodsPackageCode) {
            $params['goodsPackageCode'] = $request->goodsPackageCode;
        }
        $params['goodsStatus'] = 4;
        //dev($params);
        $resultCreate = $this->makeRequestWithJson('goods/package-arrive', $params);
        if ($resultCreate['code'] == 200)
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Đưa hàng xuống xe thành công")]);
        else
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Đưa hàng xuống xe thất bại")]);
    }

    public function deliverPackage(Request $request)
    {
        $params = [];
        if ($request->listGiaohang) {
            $params['goodsCode'] = $request->listGiaohang;
        }
//        if ($request->goodsPackageCode) {
//            $params['goodsPackageCode'] = $request->goodsPackageCode;
//        }
        $params['goodsStatus'] = 5;
        // dev($params);
        $resultCreate = $this->makeRequestWithJson('goods/package-arrive', $params);
        if ($resultCreate['code'] == 200)
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Giao hàng thành công")]);
        else
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Giao hàng thất bại")]);
    }

    public function deleteGoods(Request $request)
    {
        $params = [
            'goodsStatus' => 0,
            'goodsCode' => $request->listCode
        ];
        // dev($params);
        $resultCreate = $this->makeRequestWithJson('goods/package-arrive', $params);
        if ($resultCreate['code'] == 200)
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Xóa thành công")]);
        else
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Xóa thất bại")]);
    }

    public function searchPoint(Request $request)
    {
        $response = $this->makeRequest('web_point/getlist', [
            'count' => 10,
            'pointName' => $request->searchWord
        ]);
        if ($request->ajax()) {
            return response()->json(['result' => $response['results']['result']]);
        } else {
            return redirect()->back()->with(['result' => $response['results']['result']]);
        }
    }

    public function updatePackage(Request $request)
    {
        if ($request->goodsPackageCode != null && $request->goodsCode != null) {
            $param = [
                'companyId' => session('companyId'),
                'goodsCode' => $request->goodsCode,
                'goodsPackageCode' => $request->goodsPackageCode,
                'delete' => true
            ];
            $message="Tách vận đơn";
        }else {
            $param = [
                'companyId' => session('companyId'),
                'goodsCode' => $request->listPoke,
                'goodsPackageCode' => $request->packagePoke,
                'delete' => false
            ];
            $message="Thêm vận đơn";
        }
        $response = $this->makeRequestWithJson('goods/update-package-goods', $param);
        if ($response['code'] == 200)
            return redirect()->back()->withInput()->with(['msg' => MessageJS($message." thành công")]);
        else
            return redirect()->back()->withInput()->with(['msg' => MessageJS($message." thất bại")]);
    }

    public function getRoute(Request $request)
    {
        $param = [];
//        if ($request->listPointId)  $param['listPointId']=$request->listPointId;
        $response = $this->makeRequestWithJson('route/listByPoint', $param);
        return isset($response['results']['listRoute']) ? $response['results']['listRoute'] : [];
    }

    public function returnPackage(Request $request)
    {
        $param = [];
        if ($request->goodsCodeReturn) $param['goodsCode'] = $request->goodsCodeReturn;
        if ($request->goodsPackageCodeReturn) $param['goodsPackageCode'] = $request->goodsPackageCodeReturn;
        //dev($param);
        $response = $this->makeRequestWithJson('goods/delete-trip-info', $param);
        if ($response['code'] == 200)
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Phục hồi thành công")]);
        else
            return redirect()->back()->withInput()->with(['msg' => MessageJS("Phục hồi thất bại")]);
    }
}
