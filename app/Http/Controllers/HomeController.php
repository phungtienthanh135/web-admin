<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{

    public function index(){
        //Log::info('[HomeController Index] - Start Time:' . microtime());
        $response_list_on_the_trip = [];
        $response_vehicle = [];
        $response_report = [];
        $report_month_detail = [];

        /*Lấy ds chuyến đang chạy*/
//        if(hasAnyRole(GET_LIST_OUT_OF_STATION_COMMAND)) {
//            $response_list_on_the_trip = $this->makeRequest('web_trip/getlist-on-the-trip',['page'=>0,'count'=>10]);
//        }
        //Log::info('[HomeController Index] - After hasAnyRole:' . microtime());
//        if(session('userLogin')['userInfo']['userType'] == 7) {
//            $response_vehicle =  $this->makeRequest('web_vehicle/getlist',[
//                'page'=>0,
//                'count'=>1000
//            ]);
            //Log::info('[HomeController Index] - After $response_vehicle Time:' . microtime());
            /*lấy report tổng quan trong tháng hiện tại*/
//            $response_report = $this->makeRequest('web_report/create-summary-report-by-month',[
//                'timeZone'=>7,
//                'companyId'=>session('companyId'),
//                'month'=>getdate()['mon'],
//                'year'=>'2017'
////                'year'=>getdate()['year']
//            ]);
            //Log::info('[HomeController Index] - After $response_report Time:' . microtime());
            /*lấy report chi tiết trong tháng hiện tại*/
//            $response_report_by_month_detail = $this->makeRequest('web_report/create-summary-report-by-month-detail',[
//                'timeZone'=>7,
//                'companyId'=>session('companyId'),
//                'month'=>getdate()['mon'],
//                'year'=>'2017'
////                'year'=>getdate()['year']
//            ]);
            //Log::info('[HomeController Index] - After $response_report_by_month_detail Time:' . microtime());
//            $report_month_detail['results'] = array_sort(head($response_report_by_month_detail['results']),function ($item,$key){
//                return $key;
//            });
//        }

        /*lấy report chi tiết trong tuần hiện tại
        $response_report_by_week_detail = $this->makeRequest('web_report/create-summary-report-by-week-detail',[
            'timeZone'=>7,
            'companyId'=>session('companyId'),
            'week'=>strtotime('monday this week')*1000
        ]);*/

        return view('cpanel.Home.Index')->with([
            'listOnTheTrip'=> !empty($response_list_on_the_trip) ? head($response_list_on_the_trip['results']) : [],
            'report'=> !empty($response_report) ? head($response_report['results']) : [],
            'report_detail'=> !empty($report_month_detail) ? head($report_month_detail) : [],
            //'report_detail_week'=>head($response_report_by_week_detail['results']),
            'listVehicle'=>!empty($response_vehicle) ? head($response_vehicle['results']) : []
        ]);
    }

    public function account(){
        return view('cpanel.Home.account');
    }

    public function userLog(){
        return view('cpanel.Home.userLog');
    }

    public function quickSearch(Request $request)
    {
        date_default_timezone_set('GMT');

        $params = [
            'page' => 0,
            'count' => isset($request->count) ? $request->count : '10',
            'keyword' => $request->term
        ];
        $result = $this->makeRequest('web_ticket/search_by_keyword', $params);
        $result = array_get($result['results'], 'ticket', []);
        if(empty($result))
        {
            $result = [
                [
                    'empty' => true
                ],
            ];
        }

        return response()->json($result, 200, [], JSON_PRETTY_PRINT);
    }

    public function searchPhone(Request $request)
    {
        date_default_timezone_set('GMT');

        $params = [
            'page' => 0,
            'count' => isset($request->count) ? $request->count : '10',
            'phoneNumber' => $request->term
        ];

        $result = $this->makeRequestWithJson('company_customer/find_info_by_phone_number', $params);
        $result = array_get($result['results'], 'lastTicketInfo', []);
        if(empty($result))
        {
            $result = [
                [
                    'empty' => true
                ],
            ];
        }

        return response()->json($result, 200, [], JSON_PRETTY_PRINT);
    }
}
