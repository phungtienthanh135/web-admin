<?php

namespace App\Http\Controllers;

use App\Http\Requests\AgencyRequest;
use Validator;
use Illuminate\Http\Request;

class AgencyController extends Controller
{
    protected $typeLevel = [
      1 => 'Số ghế bán',
      2 => 'Tiền bán vé'
    ];
    private $response = [
      'status' => 'error',
      'code' => 'CLIENT',
    ];
    public function show(Request $request){
        $groupPermissions = [];
        $page = $request->has('page') ? $request->page : 1;
        $response = $this->makeRequest('web_admin/get-list-agency',[
            'page'=>$page - 1,
            'count'=>10,
            'phoneNumber'=>$request->phoneNumber,
            'fullName'=>$request->fullName,
            'address'=>$request->address,
            'agencyCompanyId'=>session('companyId')
        ]);

        $listLevel = $this->makeRequest('/level_agency/get_list',[]);
        //dev($listLevel);

        if(hasAnyRole(GET_LIST_GROUP_PERMISSION))
        {
            $groupPermissions = $this->makeRequest('group-permission/getlist', []);
            $groupPermissions = $groupPermissions['results']['listGroupPermission'];
        }
        //dev($response);
        return view('cpanel.Agency.show')->with([
            'result' => array_get($response['results'],'result'),
            'page' => $page,
            'GroupPermissions' => $groupPermissions,
            'listLevel' => $listLevel['results']['listLevelOfAgency'],
        ]);
    }

    public function postAdd(Request $request)
    {
        $response = $this->makeRequest('web_admin/create-agency',
            [
                'phoneNumber' => $request->phoneNumber,
                'userName' => $request->userName,
                'agencyCode'=>$request->agencyCode,
                'level' => $request->level,
                'fullName' => $request->fullName,
                'password' => $request->password,
                'commission' => 10/100,
                'debtLimit' => $request->debtLimit,
                'joinDate' => time()*1000,
                'email' => $request->email,
                'address' => $request->address,
                'stateCode' =>84,
                'companyId'=>session('companyId'),
                'groupPermissionId'=>$request->groupPermissionId,
            ]);
        if ($response['status']=='success')
        {
            return redirect()->back()->with(['msg'=>"Message('Thành công','Thêm đại lý thành công','');"]);
        }else
        {

            return redirect()->back()->withInput()->with(['msg'=>"Message('Thất bại','Thêm đại lý thất bại <br>Mã lỗi: ".checkMessage($response['results']['error']['propertyName'])."','');"]);
        }
    }

    /*
     * xoá và sửa cùng một api
     * 1. update
     * 2. xóa
     * */

    public function updateOrDelete(Request $request){
        $result = $this->makeRequest('web_admin/update-agency', [
            'userId'=>$request->userId,
            'option'=>$request->option,
            'agencyCode' => $request->agencyCode,
            'level' => $request->level,
            'companyId'=>session('companyId'),
            'phoneNumber' => $request->phoneNumber,
            'fullName' => $request->fullName,
            'debtLimit' => $request->debtLimit,
            'joinDate' => time()*1000,
            'email' => $request->email,
            'address' => $request->address,
            'stateCode' => 84,
            'groupPermissionId'=>$request->groupPermissionId,
            'commission'=>$request->commission/100,
        ]);
        $type =$request->option==1?'Cập nhật':'Xoá';
        if ($result['status']=='success')
        {
            return redirect()->back()->with(['msg'=>"Message('Thông báo','$type đại lý thành công','');"]);
        }else
        {
            return redirect()->back()->with(['msg'=>"Message('Thông báo','$type đại lý thất bại.<br>Mã lỗi: " . checkMessage($result['results']['error']['propertyName']) . "','');"]);
        }
    }

    public function getListAgency(Request $request)
    {
        $agency = [];
        if(hasAnyRole(GET_LIST_AGENCY)) {
            $agency = $this->makeRequest('web_admin/agency/search_by_keyword', [
                'page' => 0,
                'count' => 50,
                'keyword' => $request->keyword
            ]);
            $agency = array_get($agency['results'], 'result');
        }
        return response()->json($agency, 200, [], JSON_PRETTY_PRINT);
    }

    public function getViewLevelAgency(){
//        $response = $this->makeRequest('/level_agency/get_list',[]);
//        dev($this->getListLevelAgency());
        return view('cpanel.Agency.level')->with(['levels'=>$this->typeLevel]);
    }

    public function getListLevelAgency(){
        $response = $this->makeRequest('/level_agency/get_list',[]);
        return response($response,201);
    }

    public function getListPolicyAgency(){
        $response = $this->makeRequest('/agency_price_policy/get_list',[]);
        return response($response,201);
    }

    public function postAddLevelAgency(Request $request){
        $validate = Validator::make($request->all(),[
            'level' => 'required|numeric',
            'conditionAchieveType' => 'required|numeric',
            'achieveValue' => 'required|numeric',
            'applyDate' =>'required|date'
        ],[
            'level.required' =>'{"field":"level","message":"Cấp không được bỏ trống"}',
            'level.numeric' => '{"field":"level","message":"Cấp phải là một số nguyên"}',
            'conditionAchieveType.required' =>'{"field":"conditionAchieveType","message":"Kiểu không được bỏ trống"}',
            'conditionAchieveType.numeric' => '{"field":"conditionAchieveType","message":"Kiểu không tồn tại"}',
            'achieveValue.required' =>'{"field":"achieveValue","message":"Định mức không được bỏ trống"}',
            'achieveValue.numeric' => '{"field":"achieveValue","message":"Định mức phải là một số nguyên"}',
            'applyDate.required' => '{"field":"applyDate","message":"Ngày áp dụng không được bỏ trống"}',
            'applyDate.date' => '{"field":"applyDate","message":"Ngày áp dụng không đúng định dạng"}',
        ]);
        if($validate->fails()){
            $this->response['results'] = json_decode($validate->errors()->all()[0]);
            return response($this->response,201);
        }
        $level = $request->level;
        $conditionAchieveType = $request->conditionAchieveType;
        $achieveValue = $request->achieveValue;
        $applyDate = date_to_milisecond($request->applyDate);

        $params = [
            'level' => $level,
            'conditionAchieveType' =>$conditionAchieveType,
            'achieveValue' => $achieveValue,
            'applyDate' => $applyDate,
        ];

        $this->response = $this->makeRequestWithJson('/level_agency/create',$params);

        return response($this->response,201);
    }

    public function postEditLevelAgency(Request $request){
        $validate = Validator::make($request->all(),[
            'level' => 'required|numeric',
            'conditionAchieveType' => 'required|numeric',
            'achieveValue' => 'required|numeric',
            'applyDate' =>'required|date'
        ],[
            'level.required' =>'{"field":"level","message":"Cấp không được bỏ trống"}',
            'level.numeric' => '{"field":"level","message":"Cấp phải là một số nguyên"}',
            'conditionAchieveType.required' =>'{"field":"conditionAchieveType","message":"Kiểu không được bỏ trống"}',
            'conditionAchieveType.numeric' => '{"field":"conditionAchieveType","message":"Kiểu không tồn tại"}',
            'achieveValue.required' =>'{"field":"achieveValue","message":"Định mức không được bỏ trống"}',
            'achieveValue.numeric' => '{"field":"achieveValue","message":"Định mức phải là một số nguyên"}',
            'applyDate.required' => '{"field":"applyDate","message":"Ngày áp dụng không được bỏ trống"}',
            'applyDate.date' => '{"field":"applyDate","message":"Ngày áp dụng không đúng định dạng"}',
        ]);
        if($validate->fails()){
            $this->response['results'] = json_decode($validate->errors()->all()[0]);
            return response($this->response,201);
        }
        $level = $request->level;
        $conditionAchieveType = $request->conditionAchieveType;
        $achieveValue = $request->achieveValue;
        $applyDate = date_to_milisecond($request->applyDate);
        $levelAgencyId = $request->levelAgencyId;

        $params = [
            'level' => $level,
            'conditionAchieveType' =>$conditionAchieveType,
            'achieveValue' => $achieveValue,
            'applyDate' => $applyDate,
            'levelAgencyId'=>$levelAgencyId
        ];

        $this->response = $this->makeRequestWithJson('/level_agency/update',$params);

        return response($this->response,201);
    }

    public function postAddPolicyAgency(Request $request){
        $params = [
            'routeId' => $request->routeId,
            'applyDate' =>date_to_milisecond($request->applyDate),
            'commission'=>$request->commission,
        ];
        $res = $this->makeRequestWithJson('/agency_price_policy/create',$params);
        return response()->json($res);
    }

    public function postUpdatePolicyAgency(Request $request){
        $params = [
            'policyId'=>$request->policyId,
            'routeId' => $request->routeId,
            'applyDate' =>date_to_milisecond($request->applyDate),
            'commission'=>$request->commission,
        ];
        $response = $this->makeRequestWithJson('/agency_price_policy/update',$params);
        return response($response);
    }

    public function getViewPolicyAgency(){
        return view('cpanel.Agency.policyAgency')->with(['levels'=>$this->typeLevel]);
    }
}
