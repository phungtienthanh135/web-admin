<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PointRequest;
class PointController extends Controller
{

    public function show(Request $request){

        $page = $request->has('page')?$request->page:1;

        $response =  $this->makeRequest('web_point/getlist',[
            'page'=>($page-1),
            'count'=>10,
            'pointName'=>$request->pointName
        ]);

        return view('cpanel.Point.show',['result'=>$response['results']['result'],'page'=>$page]);
    }

    public function search(Request $request){
        $page = $request->has('page')?$request->page:1;
        $response =  $this->makeRequest('web/point-getlist',[
            'page'=>0,
            'count'=>100,
            'searchWord'=>$request->searchWord,
            'listPointType'=>$request->listPointType
        ],null,'GET',true);
        if($request->ajax()){
            return response()->json(['result'=>$response['results']['result'],'page'=>$page]);
        }else{
            return redirect()->back()->with(['result'=>$response['results']['result'],'page'=>$page]);
        }
    }


    public function getadd(){
        $result = $this->makeRequest('point/get_province_district');
        return view('cpanel.Point.add',['result'=>$result['results']['result']]);
    }
    public function postadd(PointRequest $request){

        $result = $this->makeRequest(generateUrl('web_point/create',[
            'pointName'=>$request->pointName,
            'address'=>$request->address,
            'style'=>1,
            'province'=>$request->province,
            'district'=>$request->district,
            'longitude'=>$request->longitude,
            'latitude'=>$request->latitude,
            'pointType'=>$request->pointType,
            'isReceivedGoods'=>$request->has('isReceivedGoods')?'true':'false',
            'phoneNumber'=>$request->phoneNumber
        ]));
        if ($result['status']=='success')
        {
            return redirect()->action('PointController@show')->with(['msg'=>"Message('Thông báo','Thêm điểm thành công','');"]);
        }else
        {

            return redirect()->back()->withInput()->with(['msg'=>"Message('Thông báo','Thêm điểm thất bại<br>Mã lỗi: " . checkMessage($result['results']['error']['propertyName']) . "','');"]);
        }

    }
    public function getEdit(Request $request)
    {
        $data =$request->data;
        $result=$this->makeRequest('point/get_province_district');
        return view('cpanel.Point.edit',['data'=>$data,'result'=>$result['results']['result']]);
    }
    public function postEdit(PointRequest $request)
    {
        $result = $this->makeRequest('web_point/update',[
            'pointName'=>$request->pointName,
            'address'=>$request->address,
            'style'=>1,
            'province'=>$request->province,
            'district'=>$request->district,
            'longitude'=>$request->longitude,
            'latitude'=>$request->latitude,
            'pointType'=>$request->pointType,
            'pointId'=>$request->pointId,
            'isReceivedGoods'=>$request->has('isReceivedGoods')?'true':'false',
            'phoneNumber'=>$request->phoneNumber
        ]);

        if ($result['status']=='success')
        {
            return redirect()->action('PointController@show')->with(['msg'=>"Message('Thông báo','Cập nhật điểm thành công.','');"]);
        }else
        {
            return redirect()->back()->with(['msg'=>"Message('Thông báo','Cập nhật điểm thất bại.<br>Mã lỗi: " . checkMessage($result['results']['error']['propertyName']) . "','');"]);
        }
    }
    public function delete(Request $request){
        $page = $request->has('page')?$request->page:1;
        $result = $this->makeRequest('web_point/delete',['pointId'=>$request->pointId]);
        if ($result['status']=='success')
        {
            return redirect()->back()->with(['msg'=>"Message('Thông báo','Xoá điểm thành công','');",'page'=>$page]);
        }else
        {
            return redirect()->back()->with(['msg'=>"Message('Thông báo','Xoá điểm thất bại.<br>Mã lỗi: " . checkMessage($result['results']['error']['propertyName']) . "','');",'page'=>$page]);
        }
    }
}
