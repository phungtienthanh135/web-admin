<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VehicleTypeController extends Controller
{

    /*
     * hiển thị danh sách
     * */
    public function show(Request $request){

        $page = $request->has('page')?$request->page:1;

        $response =  $this->makeRequest('web_vehicletype/getlist',[
            'page'=>$page-1,
            'count'=>10,
            'numberOfSeats'=>$request->numberOfSeats,
            //'vehicleTypeName'=>$request->vehicleTypeName,
            'seatMapType'=>$request->seatMapType,
            'companyId'=>session('companyId')
        ]);

        return  view('cpanel.Vehicle.Type.show')->with(['result'=>array_get($response['results'],'result',[]),'page'=>$page]);
    }

    /*
     * Cập nhật
     * */
    public function edit(Request $request){
        $vehicleTypeName = $request->vehicleTypeName;
        $numberOfSeats = $request->numberOfSeats;
        $description = $request->description;
        $vehicleTypeId=$request->vehicleTypeId;


        $result = $this->makeRequest('web_vehicletype/update',[
            'vehicleTypeName'=>$vehicleTypeName,
            'numberOfSeats'=>$numberOfSeats,
            'description'=>$description,
            'vehicleTypeId'=>$vehicleTypeId,
        ]);
        if ($result['status']=='success')
        {
            return redirect(action('VehicleTypeController@show'))->with(['msg'=>"Message('Thành công','Cập nhật loại xe thành công','');"]);
        }else
        {
            return redirect(action('VehicleTypeController@show'))->with(['msg'=>"Message('Thất bại','Cập nhật loại xe thất bại<br/>Mã lỗi: ".checkMessage($result['results']['error']['propertyName'])."','');"]);
        }
    }
    /*
     * Thêm
     * */
    public function add(Request $request){
        $vehicleTypeName = $request->vehicleTypeName;
        $numberOfSeats = $request->numberOfSeats;
        $description = $request->description;

        $result = $this->makeRequest('web_vehicletype/create',[
            'vehicleTypeName'=>$vehicleTypeName,
            'numberOfSeats'=>$numberOfSeats,
            'description'=>$description,
        ]);


        if ($result['status']=='success')
        {
            return redirect(action('VehicleTypeController@show'))->with(['msg'=>"Message('Thành công','Thêm loại xe thành công','');"]);
        }else
        {
            return redirect()->back()->withInput()->with(['msg'=>"Message('Thất bại','Thêm loại xe thất bại<br/>Mã lỗi: ".checkMessage($result['results']['error']['propertyName'])."','');"]);
        }
    }

    /*
     * Xoá
     *
     * */
    public function delete(Request $request){

        $result = $this->makeRequest('web_vehicletype/delete',[
            'vehicleTypeId'=>$request->vehicleTypeId
        ]);
        if ($result['status']=='success')
        {
            return redirect()->back()->with(['msg'=>"Message('Thành công','Xoá loại xe thành công','');"]);
        }else
        {
            return redirect()->back()->with(['msg'=>"Message('Thất bại','Xoá loại xe thất bại<br/>Mã lỗi: ".checkMessage($result['results']['error']['propertyName'])."','');"]);
        }
    }
}
