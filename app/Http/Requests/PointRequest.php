<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PointRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pointName'=>'required',
            'longitude'=>'required',
            'latitude'=>'required',
            'address'=>'required',
            'province'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'pointName.required'=>'Vui lòng nhập tên của điểm',
            'longitude.required'=>'Vui lòng nhập kinh độ của điểm',
            'latitude.required'=>'Vui lòng nhập vĩ độ của điểm',
            'address.required'=>'Vui lòng nhập địa chỉ của điểm',
            'province.required'=>'Vui lòng nhập tỉnh của điểm'
        ];
    }
}
