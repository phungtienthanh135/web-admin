<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AgencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
//    public function rules()
//    {
//        return [
//            'userName' =>'required',
//            'fullName' =>'required',
//            'password' =>'required',
//            'commission' =>'required',
//        ];
//    }
//
//    public function messages()
//    {
//        return [
//            'fullName.required' =>'Vui lòng nhập họ tên',
//            'commission.required' =>'Vui lòng nhập hoa hồng',
//            'password.required' =>'Vui lòng nhập mật khẩu',
//            'phoneNumber.required' =>'Vui lòng nhập số điện thoại',
//        ];
//    }
}
