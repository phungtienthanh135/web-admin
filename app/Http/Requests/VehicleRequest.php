<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'numberPlate' =>'required',
            'seatMapId' =>'required',
            'productedYear' =>'numeric',
        ];
    }

    public function messages()
    {
        return [
            'numberPlate.required' =>'Vui lòng nhập biển số xe',
            'seatMapId.required' =>'Vui lòng chọn sơ đồ ghế',
            'productedYear.numeric' =>'Vui lòng nhập đúng số năm sản xuất',
        ];
    }
}
