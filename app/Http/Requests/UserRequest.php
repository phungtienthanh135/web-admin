<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullName' => 'required',
            'userName' => 'required',
//            'password' =>'required',
//            'phoneNumber' => 'required',
            'userType' => 'required',
            'stateCode' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'fullName.required' => 'Vui lòng nhập họ tên',
            'userName.required' => 'Vui lòng nhập tài khoản',
            'password.required' => 'Vui lòng nhập mật khẩu',
//            'phoneNumber.required' => 'Vui lòng nhập số điện thoại',
            'userType.required' => 'Vui lòng nhập chức vụ',
            'stateCode.required' => 'Vui lòng nhập mã vùng điện thoại'

        ];
    }
}
