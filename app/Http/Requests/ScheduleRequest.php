<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'name' => 'required',
            //'listAssistantId.0' => 'required',
            //'listDriverId.0' => 'required',
            'listDate.*' => 'required',
            'vehicleId' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhập tiêu đề lịch',
            'listAssistantId.*.required' => 'Vui lòng chọn phụ xe',
            'listDriverId.*.required' => 'Vui lòng chọn tài xế',
            'listDate.*.required' => 'Vui lòng chọn tài xế ngày',
            'vehicleId.required' => 'Vui lòng chọn xe',
        ];
    }
}
