<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RouteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'listPointId.0' => 'required',
            'listPointId.1' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'listPointId.0.required' => 'Vui lòng nhập điểm đến',
            'listPointId.1.required' => 'Vui lòng nhập điểm đi',
        ];
    }
}
