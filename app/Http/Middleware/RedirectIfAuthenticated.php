<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if(session()->has('userLogin'))
        {
            if($request->ajax()){
                return response(["message"=>"logged"]);
            }else{
                return redirect('cpanel');
            }
        }
        return $next($request);

    }
}
