<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // force https if APP_URL is https url
        if (str_contains(\Config::get('app.url'), 'https://')) {
            \URL::forceScheme('https');
        }

        Blade::directive('dateFormat', function ($expression) {
            return "<?php echo $expression<=0?'':date('d/m/Y',($expression / 1000)); ?>";
        });
        Blade::directive('timeFormat', function ($expression) {
            return "<?php
                if (preg_match('/^\d{0,10}$/', $expression)) {
                 echo $expression<=0?'':date('H\hi',($expression)); 
                } else {
                   echo $expression<=0?'':date('H\hi',($expression / 1000)); 
                }
            
             ?>";
        });
        Blade::directive('dateTime', function ($expression) {
            return "<?php 
                if (preg_match('/^\d{10}$/', $expression)) {
                 echo $expression<=0?'':date('H\hi d/m/Y',($expression)); 
                } else {
                   echo $expression<=0?'':date('H\hi d/m/Y',($expression / 1000)); 
                }
           
            ?>";
        });
        Blade::directive('moneyFormat', function ($expression) {
            return "<?php echo number_format($expression, 0, ',', '.').'(VNĐ)'; ?>";
        });
        Blade::directive('moneyFormatNoCurrency', function ($expression) {
            return "<?php echo number_format($expression, 0, ',', '.'); ?>";
        });

        Blade::directive('balance', function ($expression) {
            return "<?php 
                switch($expression){
                    case '1':
                    {
                     echo 'Phiếu thu';
                         break;
                    }
                    case '2':
                    {
                     echo 'Phiếu chi';
                         break;
                    }
                    default:
                       echo 'Chưa xác định';

                }
                

            ?>";
        });

        /*CUSTOMER(1), // Người dùng thường
        DRIVER(2), // Lái xe
        ASSISTANT(3), // Phụ xe
        ACCOUNTANT(4), // Kế toán
        ADMINISTRATIVE_STAFF(5), // Nhân viên hành chính
        INSPECTOR(6), // Thanh tra
        ORGANIZATION_ADMIN(7), // Admin của nhà vận tải
        ADMIN(8), // Admin hệ thống
        ULTIMATE_ADMIN(9), // Admin cao nhất
        CUSTOMER_ANVUI(10);
        */

        Blade::directive('ChucVu', function ($expression) {
            return "<?php
                                    switch($expression)
                                    {
                                        case '1':
                                        {
                                         echo 'Người dùng thường';
                                             break;
                                        }
                                        case '2':
                                        {
                                         echo 'Tài xế';
                                             break;
                                        }
                                        case '3':
                                        {
                                         echo 'Phụ xe';
                                             break;
                                        }
                                        case '4':
                                        {
                                         echo 'Kế toán';
                                             break;
                                        }
                                        case '5':
                                        {
                                         echo 'Nhân viên hành chính';
                                             break;
                                        }
                                        case '6':
                                        {
                                         echo 'Thanh tra';
                                             break;
                                        }
                                        case '7':
                                        {
                                         echo 'Admin nhà vận tải';
                                             break;
                                        }
                                        case '8':
                                        {
                                         echo 'Admin hệ thống';
                                             break;
                                        }
                                        case '9':
                                        {
                                         echo 'Admin hệ thống';
                                             break;
                                        }
                                        case '10':
                                        {
                                         echo 'Khách hàng ANVUI';
                                             break;
                                        }

                                        default:
                                           echo 'Chưa xác định';

                                    }

                                ?>";
        });

        Blade::directive('checkSex', function ($expression) {
            return "<?php
                                    switch($expression)
                                    {
                                        case '1':
                                        {
                                         echo 'Nam';
                                             break;
                                        }
                                        case '2':
                                        {
                                         echo 'Nữ';
                                             break;
                                        }
                                        default:
                                           echo 'Chưa xác định';

                                    }

                                ?>";
        });
        Blade::directive('checkItem', function ($expression) {
            return "<?php
                                    switch($expression)
                                    {
                                        case '1':
                                        {
                                         echo 'Tài sản';
                                             break;
                                        }
                                        case '2':
                                        {
                                         echo 'Nguồn vốn';
                                             break;
                                        }
                                        case '3':
                                        {
                                         echo 'Lưỡng tính';
                                             break;
                                        }
                                        default:
                                           echo 'Chưa xác định';

                                    }

                                ?>";
        });
        Blade::directive('checkTicketStatus', function ($expression) {
            return "<?php  
                                    
                    switch($expression)
                        {
                            case '0':
                            {
                                echo 'Đã huỹ';
                                break;
                            }
                            case '1':
                            {
                                echo 'Trống';
                                break;
                            }
                            case '2':
                            {
                                echo 'Đang giữ chỗ';
                                break;
                            }
                            case '3':
                            {
                                echo 'Đã thanh toán';
                                break;
                            }
                            case '4':
                            {
                                echo 'Đã lên xe';
                                break;
                            }
                            case '5':
                            {
                                echo 'Đã hoàn thành';
                                break;
                            }
                            case '6':
                            {
                                echo 'Quá giờ giữ chỗ';
                                break;
                            }
                            case '7':
                            {
                                echo 'Ưu tiên giữ chỗ';
                                break;
                            }
                            default:
                                echo 'Chưa xác định';
                
                        }

                       ?>";
        });

        Blade::directive('getWeekday', function ($time) {
            return "<?php
            switch(getdate($time)['wday'])
            {
                case '0':
                {
                    echo 'Chủ nhật';
                    break;
                }
                case '1':
                {
                    echo 'Thứ 2';
                    break;
                }
                case '2':
                {
                    echo 'Thứ 3';
                    break;
                }
                case '3':
                {
                    echo 'Thứ 4';
                    break;
                }
                case '4':
                {
                    echo 'Thứ 5';
                    break;
                }
                case '5':
                {
                    echo 'Thứ 6';
                    break;
                }
                case '6':
                {
                    echo 'Thứ 7';
                    break;
                }
                default:
                    echo 'Chưa xác định';
            }
            ?>";
        });

        /*
         * STAFF(0), //nhân viên của nhà xe
         * NORMALLY(1),// bình thường
         * FRIENDLY(2),// thân thiết
         * */
        Blade::directive('checkRank', function ($expression) {
            return "<?php
                switch($expression)
                {
                    case '0':
                    {
                        echo 'Nhân viên của nhà xe';
                        break;
                    }
                    case '1':
                    {
                        echo 'Bình thường';
                        break;
                    }
                    case '2':
                    {
                        echo 'Thân thiết';
                        break;
                    }                 
                    default:
                        echo 'Chưa xác định';
                }
            ?>";
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
